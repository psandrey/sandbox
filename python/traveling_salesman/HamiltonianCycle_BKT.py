from random import sample
import copy

"""
Two versions to find Hamiltonian Cycles in a graph.
1 .Using random generated paths (not all the paths are leading to a cycle).
2. Using the brute-force Backtracking
-------------------------------------------------------------------------------
"""

# Graph representation with adjacency matrix
class Graph:
	adjMatrix: list

	def __init__(self, adjMatrix):
		self.adjMatrix = adjMatrix

	def printAdjMatrix(self):
		for line in self.adjMatrix:
			print(line)

###############################################################################
# Hamiltonian cycle 
###############################################################################
#
# Version 1: seek a random hamiltonian path :), but mind that many paths stuck
# Version 2: brute-force:  backtracking...
class HamiltonianCycle:
	inf = 0xffffffff

	def randomHamiltonianCycle(self, g: Graph, s: int):
		adjMatrix = copy.deepcopy(g.adjMatrix)
		pathLength =  adjMatrix[0].__len__()

		u = s
		path = []
		path.append(u)
		while  (pathLength > 1):
			parent = u
			adjVertices = self.getAdjVertices(adjMatrix[u])
			if (adjVertices.__len__()  ==  0):
				# cannot go further, this path is blocked...
				print("> blocked: ", path)
				return False

			u =  sample(adjVertices, 1)[0]
			path.append(u)
			adjMatrix = self.cutVertexFromGraph(adjMatrix, parent)
			pathLength -= 1

		# close circle
		if (g.adjMatrix[u][s] > 0):
			path.append(s)
			cost = self.getHamiltonianCycleCost(g, path)
			print(" > Hamiltonian cycle:", path, " > cost:", cost)
			return True

		# cannot close the circle, because from the last vertex 
		# there is no edge to the source vertex, so return False
		print("> blocked: ", path)
		return False

	def getAdjVertices(self,neighbos: list):
		adjList = []
		vertex = 0
		for v in neighbos:
			if (v != 0):
				adjList.append(vertex)
			vertex += 1
		return adjList

	def cutVertexFromGraph(self, adjMatrix: list, v: int):
		# cut all outgoing edges from v
		adjMatrix[v] = [0]*adjMatrix[v].__len__()

		# cut all incoming edges to v
		for line in adjMatrix:
			line[v] =0

		return adjMatrix

	def backtrackingHamiltonianCycle(self, g: Graph):
		# hamiltonian cycle must visit all vertices once (and the starting vertex twice) 
		hamCycleLength = g.adjMatrix[0].__len__()
		stack = [0]*(hamCycleLength + 1)
		
		# vertices are labeled 0...n, so the last vertex label/index is:
		lastVertexIndex = hamCycleLength-1

		# the first vertex is labeled '0', and the current vertex is the next one: 1
		k = 1
		
		# count the hamilton cycles
		count = 0
		tsp = (0, self.inf)
		while (k > 0):
			if (k == hamCycleLength):
				count += 1
				cost = self.getHamiltonianCycleCost(g, stack)
				print(" > Hamiltonian cycle(", count,"):", stack, " > cost:", cost)
				if (tsp[1] > cost):
					tsp = (count, cost)

				# backtracks to find the next hamilton cycle
				stack[k] = 0
				k -= 1
			else :
				# set the next valid vertex and return True
				# if there is none, then return False
				rc = self.setNextValidVertex(g, stack, k, lastVertexIndex)

				# if the path is valid, then find the next vertex in the path
				if (rc == True):
					k += 1
					stack[k] = 0

				# if it is none, then backtracks
				else:
					stack[k] = 0
					k -= 1

		print("TSP: cycle index > ", tsp[0], " cost >", tsp[1])
		print("End...")

	def setNextValidVertex(self, g: Graph, stack: list, k: int, lastVertexIndex: int):
		while (stack[k] < lastVertexIndex):
			stack[k] +=1
			if (self.isValid(g, stack, k)):
				return True
		return False

	def isValid(self, g: Graph, stack: list, k):
		# if the stack has only one vertex, then it must be valid, so return True
		if (k == 0):
			return True

		# if the vertex K has already been visited, then is not valid
		u = stack[k]
		for v in stack[:k]:
			if (u == v):
				return False

		# has to be an incident edge from vertex K-1 to vertex K
		#  (i.e. vertex K is part of the hamiltonian path)
		v = stack[k-1]
		if (g.adjMatrix[v][u] == 0):
			return False

		# so the path is valid
		return True

	def getHamiltonianCycleCost(self, g: Graph, cycle: list):
		cost = 0
		for idx in range (0, cycle.__len__() - 1, 1):
			u = cycle[idx]
			v = cycle[idx+1]
			cost += g.adjMatrix[u][v]

		return cost

class testHamilton:
	def doTestRandomHamiltonCycle(self):
		adjMatrix = [
			# 0    1    2    3     4   5     6    7    8
			[  0, 28, 25, 12,   0, 11, 13,   0,   0], # 0
			[28,   0, 34,   0,   0,   0, 25,   0,   0], # 1
			[25, 34,   0, 35,   0,   0,   0,   0,   0], # 2
			[12,   0, 35,   0, 28,   0,   0, 47, 24], # 3
			[  0,   0,   0, 28,   0, 15,   0,   0, 38], # 4
			[11,   0,   0,   0, 15,   0, 24,   0,   0], # 5
			[19, 25,   0,   0,   0, 24,   0,   0,   0], # 6
			[  0,   0,   0, 47,   0,   0,   0,   0, 32], # 7
			[  0,   0,   0, 24, 38,   0,   0, 32,   0], # 8
			]
		g = Graph(adjMatrix)
		hp = HamiltonianCycle()
		while (hp.randomHamiltonianCycle(g, 0) != True):
			pass

	def doTestBacktrackingHamiltonCycle(self):
		adjMatrix = [
			# 0    1    2    3     4   5     6    7    8
			[  0, 28, 25, 12,   0, 11, 13,   0,   0], # 0
			[28,   0, 34,   0,   0,   0, 25,   0,   0], # 1
			[25, 34,   0, 35,   0,   0,   0,   0,   0], # 2
			[12,   0, 35,   0, 28,   0,   0, 47, 24], # 3
			[  0,   0,   0, 28,   0, 15,   0,   0, 38], # 4
			[11,   0,   0,   0, 15,   0, 24,   0,   0], # 5
			[19, 25,   0,   0,   0, 24,   0,   0,   0], # 6
			[  0,   0,   0, 47,   0,   0,   0,   0, 32], # 7
			[  0,   0,   0, 24, 38,   0,   0, 32,   0], # 8
			]
		g = Graph(adjMatrix)

		hp = HamiltonianCycle()
		hp.backtrackingHamiltonianCycle(g)

t  = testHamilton()
print("# Use randomize...")
t.doTestRandomHamiltonCycle()
print("# Use backtraking...")
t.doTestBacktrackingHamiltonCycle()

	