from random import random
from random import uniform
from random import sample
from random import shuffle
import math
import itertools
from time import time
import matplotlib.pyplot as plt

"""
Genetic Algorithm for Traveling Salesman problem.
-------------------------------------------------------------------------------

Note: Install matplotlib before running this.
	- python -m pip install -U pip
	- python -m pip install -U matplotlib

"""

class Gene:
	""" A gene is encoded as a vertex (a.k.a. city):
		- 2D coordinates : (x,y), where x,y > 0
		Note: Two different cities cannot have the same coordinate,
		thus each city (gene) is identified by an id.
	"""
	x: int
	y: int
	idx: int

	dist: list # euclidian distance to all the other genes (i.e. a complete graph of cities)

	def __init__(self, idx: int, x:int, y:int):
		self.idx = idx
		self.x = x
		self.y = y
		self.dist = []

	def setDistanceToGene(self, geneId: int, dist:int):
		if self.__isDistanceToGeneSet(geneId) == True:
			return False

		# cache the distance to another gene with the id: geneId
		self.dist.append((geneId, dist))
		return True

	def __isDistanceToGeneSet(self, geneId: int):
		for t in self.dist:
			idx = t[0]
			if idx == geneId:
				return True
		return False

	def getDistanceToGene(self, geneId):
		for t in self.dist:
			gId = t[0]
			gdist = t[1] 
			if gId == geneId:
				return gdist
		return None

	def printVerboseGene(self):
		print (self.idx, ":(",self.x,",",self.y,")", end="")

	def printGene(self):
		print ("(", self.idx,")", end="")

class Chromosome:
	""" A chromosome is encoded as a list of Genes.
		Basically, it is a hamiltonian-path (or a tour of cities).
	"""
	genes: list

	cost: float
	fitness: float
	#Note:
	# The cost (e.g. distance)  can be considered the fitness, and instead of maximizing the fitness function
	# in this case it must be minimized. However, the tests results looks better if the fitness is 1/cost... 
	# It might be related to the implementation of the pseudo random function.

	def __init__(self):
		self.genes = []
		self.cost = 0
		self.fitness = 0

	def addGenes(self, genes: list):
		self.genes = genes

	def getLength(self):
		return len(self.genes)
	
	def doCycleFitness(self):
		"""
		A low cost(distance) is a good fitness, so the actual fitness is the 1.0/cost
		(i.e. a high fitness is a good fitness).
		"""
		self.cost = self.__doCycleDistance()
		self.fitness = 1.0 / self.cost
		return self.fitness

	def __doCycleDistance(self):
		"""
		The cost of a cycle is the sum of the distances between the nodes/cities (genes)
		that compose this cycle (chromosome).
		"""
		idx = 0
		self.cost = 0
		while idx < len(self.genes)-1:
			g1 = self.genes[idx]
			g2 = self.genes[idx+1]
			self.cost += g1.getDistanceToGene(g2.idx)
			idx += 1

		# close cycle: add the cost from the last gene to the first gene
		firstGene = self.genes[0]
		lastGene = self.genes[len(self.genes)-1]
		self.cost += lastGene.getDistanceToGene(firstGene.idx)
		return self.cost	

	def printVerboseChromosome(self):
		print("<", end="")
		for g in self.genes:
			g.printVerboseGene()
			print(", ", end="")
		print("> :(",self.cost,"):", self.fitness)

	def printChromosome(self):
		print("<", end="")
		for g in self.genes:
			g.printGene()
			print(",", end="")
		print("> :(",self.cost,"):", self.fitness)

class Population:
	""" The population is encoded as a collection(list) of chromosomes (i.e. tours of cities). """
	collection: list
	totalFitness: float

	def __init__(self):
		self.collection = []
		self.totalFitness = 0
		
	def addChromosome(self, c: Chromosome):
		self.collection.append(c)
		return True

	def doFitness(self):
		for c in self.collection:
			self.totalFitness += c.doCycleFitness()

	def getChromosomeById(self, idx):
		if idx >= self.getPopulationSize():
			return None
		return self.collection[idx]

	def getPopulationSize(self):
		return len(self.collection)

	def printVerbosePopulation(self):
		print("# Population:")
		i = 0
		for c in self.collection:
			i += 1
			strLeadingZeroes = str(i).zfill(3)
			print("(CHR:" , strLeadingZeroes,"):", end="") 
			c.printVerboseChromosome()
		print("End population...");

	def printPopulation(self):
		print("# Population:")
		i = 0
		for c in self.collection:
			i += 1
			strLeadingZeroes = str(i).zfill(3)
			print("(CHR:" , strLeadingZeroes,"):", end="") 
			c.printChromosome()
		print("End population...");

class Utils:
	@staticmethod
	def doEuclidianDistance(g: Gene, toG: Gene):
		""" Do the euclidian distance between a gene 'g' to a gene 'toG' """
		xCub = math.pow(toG.x - g.x, 2)
		yCub = pow(toG.y - g.y, 2)
		d = math.sqrt(xCub + yCub) # euclidian distance to gene 'g'
		g.setDistanceToGene(toG.idx, d)
		return g

	@staticmethod
	def verticesToGenes(vertices: list):
		""" Transform a list of vertices (2D coordinates x,y) into a list of Genes """
		listGenes = []
		idx = 0
		for v in vertices:
			g = Gene(idx, v[0], v[1]) # gene = (idx , x, y)
			listGenes.append(g)
			idx += 1

		for g in listGenes:
			for toG in listGenes:
				if ( g != toG ):
					g = Utils.doEuclidianDistance(g, toG)

		return listGenes

	@staticmethod
	def getFitesstChromosome(listCh: list):
		ch = listCh[0]
		for i in range(1, len(listCh)):
			if ch.fitness < listCh[i].fitness:
				ch = listCh[i]
		return ch

	@staticmethod
	def buildStubChromosome(nGenes: int):
		Q = Chromosome()
		Q.genes = [Gene(-1,-1,-1)] * nGenes
		return Q

	@staticmethod
	def plotGenes(genes:list, xMax, yMax):
		plt.axis([-1, xMax, -1, yMax])
		for idx in range(0, len(genes)):
			plt.plot(
				genes[idx].x, genes[idx].y,
				color="brown", marker="o")
		plt.show()

	@staticmethod
	def drawCycleGenes(genes:list, xMax, yMax, longPause: bool ):
		plt.clf()
		plt.axis([-1, xMax, -1, yMax])
		# draw path
		for idx in range(0, len(genes)-1):
			x1, y1  = genes[idx].x, genes[idx].y
			x2, y2  = genes[idx+1].x, genes[idx+1].y
			plt.plot( [x1, x2], [y1, y2], color="brown", marker="o")

		# close path = cycle
		x1, y1  = genes[len(genes)-1].x, genes[len(genes)-1].y
		x2, y2  = genes[0].x, genes[0].y
		plt.plot( [x1, x2], [y1, y2], color="brown", marker="o")

		if longPause == True:
			plt.pause(5)
		else:
			plt.pause(0.0001)

	@staticmethod
	def  secondsToTime(d):
		delta = int(d)

		minutes, seconds = divmod(delta, 60)
		hours, minutes = divmod(minutes, 60)
		buf = "%d:%d:%d" % (hours, minutes, seconds)
		return buf

class Crossovers:
	""" Chromosomes crossovers"""

	@staticmethod
	def doCX(P1: Chromosome, P2: Chromosome):
		""" Cycle crossover: CX""" 
		nGenes = len(P1.genes)
		Q = Utils.buildStubChromosome(nGenes)

		idx = 0
		while (nGenes > 0):
			# find and set the next cycle
			noGenesInCycle = Crossovers.__setCycle( idx, P1, P2, Q)
			nGenes -= noGenesInCycle
			idx = Crossovers.__getIdxOfNextGeneToSet(Q)
			
			# switch parents
			C = P1
			P1 = P2
			P2 = C

		return Q

	@staticmethod
	def __getIndexOfGeneFromChromosome(C: Chromosome, G: Gene):
		idx = 0
		for g in C.genes:
			if (g.idx == G.idx):
				return idx
			idx += 1
		return -1

	@staticmethod
	def __setCycle(idx: int, P1: Chromosome, P2: Chromosome, Q: Chromosome):
		g: Gene
		g = P1.genes[idx]
		Q.genes[idx] = g
		noGenes = 1
		while (True):
			idx = Crossovers.__getIndexOfGeneFromChromosome(P2, g)
			if (idx != -1 and P1.genes[idx].idx != Q.genes[idx].idx):
				g = P1.genes[idx]
				Q.genes[idx] = g
				noGenes += 1
			else:
				break
		return noGenes

	@staticmethod
	def __getIdxOfNextGeneToSet(Q: Chromosome):
		idx = 0
		for g in Q.genes:
			if (g.idx == -1):
				return idx
			idx += 1
		return idx

class Selections:
	@staticmethod
	def tournamentSelection(pop: Population, tSize: int):
		chList = []
		for _ in range(0, tSize):
			idx = int (random.random() * (pop.getPopulationSize() - 1))
			ch = pop.getChromosomeById(idx)
			chList.append(ch)
		ch = Utils.getFitesstChromosome(chList)
		return ch

	@staticmethod
	def rouletteSelection(p: Population):
		limit = uniform(0.0, p.totalFitness)
		s = 0
		for ch in p.collection:
			s += ch.fitness
			if s > limit:
				return ch

class Mutation:
	@staticmethod
	def mutationSwapTwoGenesInChromosome(ch:Chromosome, mutationRate):
		for firstGene in range(len(ch.genes)):
			if random() < mutationRate:
				secondGene = (int) (random() * ch.getLength())
				ch.genes[firstGene], ch.genes[secondGene] = ch.genes[secondGene], ch.genes[firstGene]

class TSP:
	vertices: list

	population: Population
	mutationRate : float
	generations: int
	tournamentSize: int

	def __init__(self,
			vertices: list, populationSize:int, generations:int,
			mutationRate:int, tournamentSize:int):
		self.vertices = vertices
		self.generatePopulation(populationSize)
		self.generations = generations
		self.mutationRate  = mutationRate 
		self.tournamentSize = tournamentSize

	def generatePopulation(self, populationSize):
		listGenes = Utils.verticesToGenes(self.vertices)
		self.population = Population()
		for _ in range(populationSize):
			genesList = sample(listGenes, len(listGenes))
			c = Chromosome()
			c.addGenes(genesList)
			self.population.addChromosome(c)
		self.population.doFitness()
		self.population.printPopulation()

	def runGeneticAlgorithm(self):
		print("...Start...")

		start = time()
		generation = 0
		while generation < self.generations:
			newPopulation = Population()
			
			# Elitism: keep the fittest  chromosome in the new population
			chFittest = Utils.getFitesstChromosome(self.population.collection)
			newPopulation.addChromosome(chFittest)
			Utils.drawCycleGenes(chFittest.genes, 25, 25, False)
			
			while newPopulation.getPopulationSize() < self.population.getPopulationSize():
				#P1 = Selections.tournamentSelection(self.population, self.tournamentSize)
				#P2 = Selections.tournamentSelection(self.population, self.tournamentSize)
				P1 = Selections.rouletteSelection(self.population)
				P2 = Selections.rouletteSelection(self.population)
				
				Q = Crossovers.doCX(P1, P2)				
				newPopulation.addChromosome(Q)

			# shuffle in case of roulette selection
			shuffle(newPopulation.collection)

			for idx in range (1, newPopulation.getPopulationSize()):
				ch = newPopulation.getChromosomeById(idx)
				Mutation.mutationSwapTwoGenesInChromosome(ch, self.mutationRate )

			generation += 1
			self.population = newPopulation
			self.population.doFitness()

		chFittest = Utils.getFitesstChromosome(self.population.collection)

		print("Best chromosome found:")
		chFittest.printVerboseChromosome()
		print("Runtime:", Utils.secondsToTime(time() - start))
		Utils.drawCycleGenes(chFittest.genes, 25, 25, True)

		print("...End...")

class BruteForceTSP:
	def doHideousBruteForce(self, vertices: list):
		start = time()
		listGenes = Utils.verticesToGenes(vertices)

		time1 =  time()
		print(" Generate all combinations of genes:",  Utils.secondsToTime(time1-start))
		allGenesLists =  list(itertools.permutations(listGenes))
		pop = Population()

		time2 =  time()
		print(" do population of all chromosomes:",  Utils.secondsToTime(time2-time1))
		iterationsMinor = 0
		iterationsMajor = 0
		timer = time()
		for listGenes in allGenesLists:
			ch = Chromosome()
			ch.addGenes(listGenes)
			ch.doFitness()
			pop.addChromosome(ch)
			iterationsMinor += 1
			if iterationsMinor == 1000: 
				print("Chromosomes:", iterationsMajor*1000, " > Timer:", 
					Utils.secondsToTime(time()-timer), " > Total time:",
					Utils.secondsToTime(time()-start))
				ch.printChromosome()
				iterationsMajor += 1
				iterationsMinor = 0
				timer = time()

		time3 =  time()
		print("Sort chromosomes based on their fitness:", Utils.secondsToTime(time3-time2))
		pop.collection.sort(key = lambda x: float(x.fitness), reverse=False)
		time4 =  time()
		print("Done:", Utils.secondsToTime(time4-time3))
		pop.printPopulation()
		print("Total time:",  Utils.secondsToTime(time4-start))

class testTSP:
	def doUnitTestFitness(self):
		# Test fitness
		popVertices = [
			[(13, 2), (15, 15), (2,  10), (19,  6), (12,  5), (1, 12), (13, 2)],
			[(13, 2), (2,  10), (1,  12), (19,  6), (15, 15), (12, 5), (13, 2)],
			[(13, 2), (12,  5), (19,  6), (1,  12), (15, 15), (2, 10), (13, 2)],
			[(13, 2), (2,  10), (15, 15), (19,  6), (1,  12), (12, 5), (13, 2)],
			[(13, 2), (2,  10), (19,  6), (12,  5), (15, 15), (1, 12), (13, 2)],
			[(13, 2), (1,  12), (15, 15), (2,  10), (12,  5), (19, 6), (13, 2)],
			[(13, 2), (12,  5), (1,  12), (15, 15), (2,  10), (19, 6), (13, 2)],
			[(13, 2), (2,  10), (15, 15), (12,  5), (19,  6), (1, 12), (13, 2)],
			[(13, 2), (19,  6), (15, 15), (2,  10), (1,  12), (12, 5), (13, 2)],
			[(13, 2), (2,  10), (1,  12), (15, 15), (19,  6), (12, 5), (13, 2)]
			]
		pop = Population()
		for listVertices in popVertices:
			listGenes = Utils.verticesToGenes(listVertices)
			ch = Chromosome()
			ch.addGenes(listGenes)
			print("Cycle distance: ",  ch.doCycleDistance())
			print("Cycle fitness:", ch.doCycleFitness())
			pop.addChromosome(ch)

		pop.printVerbosePopulation()
		print("Total fitness:", pop.doTotalFitness()) # Should be: 0.15180012685914165

	def doTest_1(self):
		mutationRate = 0.015
		generations = 50
		populationSize = 10
		turnamentSize = 3
		vertices  = [(12,  1), (4, 12), (17,  6), (18,  4), (1, 10),
			     (16, 16), (5, 11), (17, 10), (10, 20), (15, 2)]

		TSP(
			vertices,
			populationSize,
			generations, 
			mutationRate,
			turnamentSize).runGeneticAlgorithm()

	def doTest_2(self):
		mutationRate = 0.015
		generations = 200
		populationSize = 50
		turnamentSize = 3
		vertices = [(1,   4), (2,   2), (5,  2), (10,  5), (16,  2),
			    (20,  5), (18,  6), (16, 8), (18, 12), (15, 15),
			    (18, 16), (10, 18), (8, 18), (5,  20), (1,  16), (4, 12)]

		TSP(
			vertices,
			populationSize,
			generations, 
			mutationRate,
			turnamentSize).runGeneticAlgorithm()
		
t  = testTSP()
#t.doUnitTestFitness()
#t.doTest_1()
t.doTest_2()