"""
Simulated Annealing technique for Traveling Salesman problem.
-------------------------------------------------------------------------------

Note: Install matplotlib before running this.
	- python -m pip install -U pip
	- python -m pip install -U matplotlib

"""
import copy
import math
from random import random
from random import shuffle
import matplotlib.pyplot as plt

class Vertex:
	""" A vertex (a.k.a. city):
		- 2D coordinates : (x,y), where x,y > 0
		Note: Two different vertices cannot have the same coordinate,
		thus each vertex is identified by an id.
	"""
	x: int
	y: int
	idx: int

	# euclidian distance to all the other vertices 
	# (i.e. a complete graph of cities)
	dist: list

	def __init__(self, idx: int, x:int, y:int):
		self.idx = idx
		self.x = x
		self.y = y
		self.dist = []

	def setDistanceToVertex(self, vertexId: int, dist:int):
		if self.__isDistanceToGeneSet(vertexId) == True:
			return False

		# cache the distance to another gene with the id: geneId
		self.dist.append((vertexId, dist))
		return True

	def __isDistanceToGeneSet(self, vertexId: int):
		for t in self.dist:
			idx = t[0]
			if idx == vertexId:
				return True
		return False

	def getDistanceToVertex(self, vertexId):
		for t in self.dist:
			vId = t[0]
			vdist =t[1]
			if vId == vertexId:
				return vdist
		return None

	def printVerboseVertex(self):
		print (self.idx, ":(",self.x,",",self.y,")", end="")

	def printVertex(self):
		print ("(", self.idx,")", end="")

class Tour:
	vertices: list
	def __init__(self, vertices: list):
		self.vertices = copy.deepcopy(vertices)

	def randomize(self):
		shuffle(self.vertices)

	def getEnergy(self):
		"""
		The energy of a cycle is the sum of the distances between its vertices.
		"""
		idx = 0
		energy = 0
		while idx < len(self.vertices)-1:
			v1 = self.vertices[idx]
			v2 = self.vertices[idx+1]
			energy += v1.getDistanceToVertex(v2.idx)
			idx += 1

		# close cycle: add the cost from the last Vertex to the first one
		firstVertex = self.vertices[0]
		lastVertex = self.vertices[len(self.vertices)-1]
		energy += lastVertex.getDistanceToVertex(firstVertex.idx)
		return energy

	def randomNeighbor(self):
		i = int((len(self.vertices) - 1) * random())
		j = int((len(self.vertices) - 1) * random())
		
		if (i != j):
			self.vertices[i], self.vertices[j] = self.vertices[j], self.vertices[i] 
		
class Utils:
	@staticmethod
	def doEuclidianDistance(v: Vertex, toV: Vertex):
		""" Do the euclidian distance between a vertex 'v' to a vertex 'toV' """
		xCub = math.pow(toV.x - v.x, 2)
		yCub = pow(toV.y - v.y, 2)
		d = math.sqrt(xCub + yCub) # euclidian distance to gene 'g'
		v.setDistanceToVertex(toV.idx, d)
		return v

	@staticmethod
	def coordinatesToVertices(coordinates: list):
		""" Transform a list of 2D coordinates (x,y) into a list of Vertices"""
		listVertices = []
		idx = 0
		for c in coordinates:
			v = Vertex(idx, c[0], c[1]) # Vertex = (idx , x, y)
			listVertices.append(v)
			idx += 1

		for v in listVertices:
			for toV in listVertices:
				if ( v != toV ):
					v = Utils.doEuclidianDistance(v, toV)

		return listVertices

	@staticmethod
	def drawTour(tour:Tour, xMax, yMax, longPause):
		plt.clf()
		plt.axis([-1, xMax, -1, yMax])
		vertices = tour.vertices
		# draw path
		for idx in range(0, len(vertices)-1):
			x1, y1  = vertices[idx].x, vertices[idx].y
			x2, y2  = vertices[idx+1].x, vertices[idx+1].y
			plt.plot( [x1, x2], [y1, y2], color="brown", marker="o")

		# close path = cycle
		x1, y1  = vertices[len(vertices)-1].x, vertices[len(vertices)-1].y
		x2, y2  = vertices[0].x, vertices[0].y
		plt.plot( [x1, x2], [y1, y2], color="brown", marker="o")

		if longPause == True:
			plt.pause(5)
		else:
			plt.pause(0.0001)

class TSP:
	vertices: list
	KMax: int

	def __init__(self, coordinates:list, KMax):
		self.vertices = Utils.coordinatesToVertices(coordinates)
		self.KMax = KMax
		
	def isAccepted(self, energy, energyNew, T):
		if (energyNew < energy):
			return True
		
		p = math.exp((energy-energyNew)/T)
		#print("e:",energy-energyNew, "  T:",T, "  P:" , p)
		if (p >= random()):
			return True

		return False

	def run(self):
		print("Run...")

		current = Tour(self.vertices)
		current.randomize()

		for k in range (self.KMax, 1, -1):
			T = k/self.KMax
			
			new = Tour(current.vertices)
			new.randomNeighbor()

			energy = current.getEnergy()
			energyNew = new.getEnergy()
			if (self.isAccepted(energy, energyNew, T)):
				current = Tour(new.vertices)
				Utils.drawTour(current, 25, 25, False)

		print("Terminated...")
		Utils.drawTour(current, 25, 25, True)

class testTSP:
	def doTest(self):
		print("Test...")
		TMax = 1000
		coordinates2D = [(1,   4), (2,   2), (5,  2), (10,  5), (16,  2),
				 (20,  5), (18,  6), (16, 8), (18, 12), (15, 15),
				 (18, 16), (10, 18), (8, 18), (5,  20), (1,  16), (4, 12)]
		tsp = TSP(coordinates2D, TMax)
		tsp.run()

t = testTSP()
t.doTest()
