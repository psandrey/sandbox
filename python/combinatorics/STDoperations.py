import copy

"""
###############################################################################
 - Permutation(n)
 - K-Permutation: Permutation(n, k)
 - Combinations(n, k)
 - Cartesian product of n sets of the same size
 - Subsets of a set between bottomLimit and topLimit
###############################################################################
"""

class Combinatorics:
	@staticmethod
	def doLexiPerm(n):
		""" Lexicographic permutations of n elements """
		maxIdx = n - 1
		p = [k for k in range(0, n)]
		perms = []
		perms.append(copy.deepcopy(p))
		
		while True:
			# Find the largest index k such that p[k] < p[k + 1]. 
			# If no such index exists, the permutation is the last permutation.
			k = maxIdx - 1
			while p[k] > p[k+1]:
				k -= 1

			# if k < 0 (basically -1) it means that we reached at the last permutation
			# the biggest in the lexicographic order
			if k >= 0:
				# Find the largest index l greater than k such that p[k] < p[l].
				l = maxIdx
				while p[k] > p[l]:
					l -= 1
		
				# Swap the value of p[k] with that of p[l].
				p = Combinatorics.swap(p, k, l)

				# Reverse the sequence from p[k + 1] up to and including p[n]
				i = k+1
				j = maxIdx
				while i < j:
					p = Combinatorics.swap(p, i, j)
					i += 1
					j -= 1
				perms.append(copy.deepcopy(p))
			else:
				break
		return perms

	@staticmethod
	def doLexiKPerm(n, k):
		""" Lexicographic permutations of k elements from a set of n elements, n >= k """
		kPerms = []
		
		# initialize the k-permutation and the availability array
		avlb = [0] * n
		kP = [i for i in range(0, k)]
		for i in range(k, n):
			avlb[i] = 1

		# save the first k-permutation
		kPerms.append(copy.deepcopy(kP))

		# generate all the other k-permutations
		i = k - 1
		while i>= 0:
			# swap the i-th element with the smallest available element
			#
			# ! if the next element (kP[i]+1) is available then swap with it,
			# otherwise it means that the i-th element is the maximal element available,
			# so, let's see what about i-1 element. If there is no element that can be
			# swapped then i = -1 and we just generate all the k-permutations.
			avlb[kP[i]] = 1
			j = kP[i] + 1

			foundNewKPerm = False
			while j < n and foundNewKPerm == False:
				if avlb[j] == 1: 
					# So the j-th element is available and it can be swapped
					# with the i-th element.
					kP[i] = j
					avlb[j] = 0

					# All the elements from i-th element to k-th element must be
					# lexicographically ordered with all the available
					# elements, so lets do that.
					t = 0
					for l in range (i+1, k):
						while avlb[t] == 0:
							t += 1
						kP[l] = t
						avlb[t] = 0
					foundNewKPerm = True
					break
				else:
					j += 1

				foundNewKPerm = False
			
			# We've just found the next available element to replace the kP[i], 
			# so that means we found a new k-permutation
			if foundNewKPerm == True:
				kPerms.append(copy.deepcopy(kP))
				i = k - 1
				continue

			# If there is no available element for i-th, then go to the previous one			# and check if there is an available element for it
			i -= 1

		# DONE, there is no other permutation that can be generate
		return kPerms

	@staticmethod
	def doCombinations(n: int, k:int):
		combs = []
		comb = [i for i in range(0, k)]
		combs.append(copy.deepcopy(comb))
		maxNIdx = n - 1
		maxKIdx = k - 1
		
		i = maxKIdx
		while i >= 0:
			if (comb[i] < maxNIdx - maxKIdx + i):
				comb[i] += 1
				for j in range(i+1, k):
					comb[j] = comb[j-1] + 1
				i = maxKIdx
				combs.append(copy.deepcopy(comb))
			else:
				i -= 1
		return combs

	@staticmethod
	def doCartesianProduct(nSets: int, sizeSet:int):
		""" Cartesian product of n sets of the same size """
		maxSetsIdx = nSets - 1
		maxSizeIdx = sizeSet - 1
		product = [0] * nSets
		products = []
		products.append(copy.deepcopy(product))
		
		i = maxSetsIdx
		flag = False
		while i >= 0:
			if product[i] < maxSizeIdx:
				product[i] = product[i] + 1
				flag = True
				if (i < maxSetsIdx):
					i = i + 1
			else:
				product[i] = 0
				i = i - 1

			if (flag == True):
				products.append(copy.deepcopy(product))
				flag = False
		return products

	@staticmethod
	def swap(L: list, i, j):
		aux = L[i]
		L[i] = L[j]
		L[j] = aux
		return L

	@staticmethod
	def soSubsets(bottomLimit: int, topLimit: int, subsetSize:int):
		S = []
		subsets = []
		Combinatorics.subset(
			S, subsets,
			bottomLimit, topLimit+1, subsetSize)
		return subsets

	@staticmethod
	def subset(
		S: list,
		subsets: list, 
		bottomLimit:int, topLimit: int, subsetSize:int):

		if (len(S) == subsetSize):
			subsets.append(copy.deepcopy(S))
			S.pop(len(S) - 1)
			return

		for i in range (bottomLimit, topLimit, 1):
			S.append(i)
			Combinatorics.subset(
				S,
				subsets,
				i+1, topLimit, subsetSize)

		if (len(S) > 0):
			S.pop(len(S) - 1)

class test:
	def doPermTest(self):
		n = 5
		perms = Combinatorics.doLexiPerm(n)
		for perm in perms:
			print(perm)

	def doKPermTest(self):
		n = 7
		k = 3
		kPerms = Combinatorics.doLexiKPerm(n, k)
		for kperm in kPerms:
			print(kperm)

	def doCombinTest(self):
		n = 5
		k = 3
		combs = Combinatorics.doCombinations(n, k)
		for comb in combs:
			print(comb)

	def doSubsets(self):
		bottomLimit = 1
		topLimit = 3
		sizeSubset = 2
		subsets = Combinatorics.soSubsets(bottomLimit, topLimit, sizeSubset)
		for subset in subsets:
			print(subset)

#t = test()
#t.doPermTest()
#t.doKPermTest()
#t.doCombinTest()
#t.doSubsets()