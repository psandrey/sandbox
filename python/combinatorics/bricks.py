"""
------------------------------------------------------------------------------- 
The algorithm below uses permutations and cartesian product to solve the
 following problem.

Time complexity: O((3n)!)
Note: this method is somewhat similar with backtracking...
===============================================================================

Problem:
 Given a set of N bricks. Each brick has three dimensions: Length, Width, Height. 
 What is the tallest tower that can be build using a sub-set from the N-brick set
 by putting one brick on top of the other such that the face of the bottom brick
 to include the face of top brick for every two consecutive bricks.

For example:
 - brick 1: 1, 2, 3
 - brick 2: 4, 6, 5

The tallest tower that can be built is by putting brick 1 over brick 2: the height is 9.
The side composed by 4 and 5 can include the side composed by 1 and 2, because 4>=1 and 5 >= 2.

	     |
	     3  /
	     | 2
	--1--/
	     |   
	     6  /
	     | 5
	--4--/ 
"""
import copy

class BRICKS:
	@staticmethod
	def doBricks(bricks: list):
		n = len(bricks)
		bricksPermutations = BRICKS.doLexicographicPermutations(n)

		# Each brick has 3 faces codified as follows:
		# -------------------------------------------------
		# face 0: (length , width)
		# face 1: (length, height) 
		# face 2: (width, height) 
		nFaces = 3
		cartesianProducts = BRICKS.doCartesianProduct(len(bricks), nFaces)

		solution = (0 , [])
		# For each permutation do the cartesian product
		for bricksPermutation in bricksPermutations:
			for cartesianProduct in cartesianProducts:
				partialSol = BRICKS.computeMaxHeight(bricksPermutation, cartesianProduct, bricks)
				if partialSol[0] > solution[0]:
					solution = copy.deepcopy(partialSol)
		return solution
		
	@staticmethod
	def computeMaxHeight(bricksPermutation: list, cartesianProduct:list , bricks: list):
		brickBottom = bricks[bricksPermutation[0]]
		faceIdxBottom =  cartesianProduct[0]
		faceBottom = BRICKS.getFace(faceIdxBottom)
		height = BRICKS.getHeight(brickBottom, faceBottom)

		solFaces = []
		solBricks = []
		solBricks.append(copy.deepcopy(brickBottom))
		solFaces.append(copy.deepcopy(faceBottom))
		for idx in range(0, len(bricksPermutation) - 1):
			idxBottom = bricksPermutation[idx]
			idxTop = bricksPermutation[idx + 1]
			brickBottom = bricks[idxBottom]
			brickTop = bricks[idxTop]

			faceIdxBottom =  cartesianProduct[idx]
			faceIdxTop =  cartesianProduct[idx+1]

			faceBottom = BRICKS.getFace(faceIdxBottom)
			faceTop = BRICKS.getFace(faceIdxTop)

			(L1bottom, L2bottom) = (brickBottom[faceBottom[0]], brickBottom[faceBottom[1]])
			(L1top, L2top) =   (brickTop[faceTop[0]], brickTop[faceTop[1]])

			if (	(L1bottom >= L1top and L2bottom >= L2top) or 
				(L1bottom >= L2top and L2bottom >= L1top)):
				height += BRICKS.getHeight(brickTop, faceTop)
				solBricks.append(copy.deepcopy(brickTop))
				solFaces.append(copy.deepcopy(faceTop))
			else:
				break

		return (height, solBricks, solFaces)

	@staticmethod
	def getHeight(brick: list, face):
		nFaces = 3
		hIdx =  nFaces - (face[0] + face[1])
		return  brick[hIdx]	
	
	@staticmethod
	def getFace(faceIdx):
		# Faces codification:
		# -------------------------------------------
		# LengthIdx = 0, widthIdx = 1, heightIdx = 2
		# faceIdx 0: (length , width)  -> (0, 1) 
		# faceIdx 1: (length, height) -> (0, 2)
		# faceIdx 2: (width, height) -> (1, 2)
		if faceIdx == 0:
			return (0, 1)
		elif faceIdx == 1:
			return (0, 2)
		else:
			return (1, 2)

	@staticmethod
	def doLexicographicPermutations(n):
		maxIdx = n - 1
		p = [k for k in range(0, n)]
		permutations = []
		permutations.append(copy.deepcopy(p))
		
		while True:
			# Find the largest index k such that p[k] < p[k + 1]. If no such index exists, the permutation is the last permutation.
			k = maxIdx - 1
			while p[k] > p[k+1]:
				k -= 1

			# if k < 0 (basically -1) it means that we reached at the last permutation
			# the biggest in the lexicographic order
			if k >= 0:
				# Find the largest index l greater than k such that p[k] < p[l].
				l = maxIdx
				while p[k] > p[l]:
					l -= 1
		
				# Swap the value of p[k] with that of p[l].
				p = BRICKS.swapElements(p, k, l)

				# Reverse the sequence from p[k + 1] up to and including p[n]
				i = k+1
				j = maxIdx
				while i < j:
					p = BRICKS.swapElements(p, i, j)
					i += 1
					j -= 1
				permutations.append(copy.deepcopy(p))
			else:
				break
		return permutations

	@staticmethod
	def doCartesianProduct(nSets: int, sizeSet:int):
		maxSetsIdx = nSets - 1
		maxSizeIdx = sizeSet - 1
		product = [0] * nSets
		products = []
		products.append(copy.deepcopy(product))
		
		i = maxSetsIdx
		flag = False
		while i >= 0:
			if product[i] < maxSizeIdx:
				product[i] = product[i] + 1
				flag = True
				if (i < maxSetsIdx):
					i = i + 1
			else:
				product[i] = 0
				i = i - 1

			if (flag == True):
				products.append(copy.deepcopy(product))
				flag = False
		return products

	@staticmethod
	def swapElements(L: list, i, j):
		aux = L[i]
		L[i] = L[j]
		L[j] = aux
		return L
	
	@staticmethod
	def printSolution(solution: list):
		maxHeight = solution[0]
		bricks = solution[1]
		faces = solution[2]
		print("Max height = ", maxHeight)
		for idx in range(0, len(bricks)):
			print("Brick(",idx,"):", bricks[idx])
			print("    - Face:", bricks[idx][faces[idx][0]], ",", bricks[idx][faces[idx][1]])

class test:
	def doUnitTest(self):
		p = BRICKS.doLexicographicPermutations(5)
		print(p)

	def doTest(self):
		bricks = [(1, 2, 3), (4, 5, 6), (7, 8, 9)] # (L, l, h)

		solution = BRICKS.doBricks(bricks)
		BRICKS.printSolution(solution)

t = test()
#t.doUnitTest()
t.doTest()