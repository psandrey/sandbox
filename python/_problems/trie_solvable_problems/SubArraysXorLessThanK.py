"""
Problem:
--------
Given an array of positive integers find the number of sub-arrays whose XOR is less than K
--------

Note: The problem is solved using a Trie structure.
"""

class TrieNode:
	""" Binary Trie Node """
	def __init__(self):
		self.left = None
		self.right = None
		self.lc = 0
		self.rc = 0

class Trie:
	""" Binary Trie """
	MAX_BIN = 4

	def __init__(self): 
		self.root = self.get_node() 

	def get_node(self):
		return TrieNode() 

	def insert(self, key):
		return self.insert_rec(self.root, key, self.MAX_BIN - 1)
		
	def insert_rec(self, node: TrieNode, key, level):
		if level == -1:
			return None

		b = self.get_nbit(key, level)
		if b == 1:
			node.rc += 1
			if not node.right:
				node.right = self.get_node()
			node.right = self.insert_rec(node.right, key, level-1)
		else:
			node.lc += 1
			if not node.left:
				node.left = self.get_node()
			node.left = self.insert_rec(node.left, key, level-1)
		return node

	def query(self, key, k):
		# how many sub-arrays already exist into structure which when taken xor with q
		#  return an integer less than k.
		return self.query_rec(self.root, key, k, self.MAX_BIN - 1)

	def query_rec(self, node: TrieNode, key, k, level):
		if not node or level == -1:
			return 0

		q = self.get_nbit(key, level)
		p = self.get_nbit(k, level)
		
		if (p == 1):
			if not q:
				return node.lc + self.query_rec(node.right, key, k, level-1)
			else:
				return node.rc + self.query_rec(node.left, key, k, level-1)
		else:
			if not q:
				return self.query_rec(node.left, key, k, level-1)
			else:
				return self.query_rec(node.right, key, k, level-1)

	def zero(self, c):
		if ord(c) == ord('0'):
			return True
		return False

	def get_child_index(self, c):
		if (self.zero(c) == True):
			return 0
		return 1

	def get_nbit(self, key, level):
		s = self.format_binary(key)
		#print(level)
		c = s[level]
		idx = self.get_child_index(c)
		return idx

	def format_binary(self, k):
		s = bin(k)
		s = s[2:len(s)]
		s = s.zfill(self.MAX_BIN)
		return s

class SubArraysXorLessThanK:
	@staticmethod
	def do(a: list, k):
		t = Trie()
		q = 0
		p = 0
		ans = 0
		t.insert(0)
		t.insert(0)
		for i in range(0, len(a)):
			q = p ^ a[i]
			ans += t.query(q, k)
			t.insert(q)
			p = q
		return ans
		
class test:
	def do_test(self):
		a = [4, 1, 3, 2, 7]
		k = 2
		no = SubArraysXorLessThanK.do(a, k)
		print("The array:", a)
		print("Number the sub-arrays with xor less than:", k, " : is:", no)

#t = test()
#t.do_test()
