"""
Problem:
--------
Given an array of integers, find the sub-array with maximum XOR.
 a. not contiguous sub-array
 b. contiguous sub-array
--------

Note: The problem is solved using a Trie structure.
"""

class TrieNode:
	""" Binary Trie Node """
	def __init__(self, idx):
		self.idx = idx
		self.children = [None]*2
		self.is_number = False

class Trie:
	""" Binary Trie """
	MAX_BIN = 4

	def __init__(self): 
		self.root = self.get_node() 

	def get_node(self):
		return TrieNode(0) 

	def insert(self, key, idx):
		s = self.format_binary(key)
		node = self.root
		length = len(s)
		for index in range(0, length):
			c = s[index]
			child_index = self.get_child_index(c)
			if not node.children[child_index]: 
				node.children[child_index] = self.get_node() 
			node = node.children[child_index]
		node.idx = idx
		node.is_number = True

	def query(self, key):
		s = self.format_binary(key)
		xor_max = 0
		node = self.root 
		length = len(s)
		for index in range(0, length):
			c = s[index]

			child_index = self.get_child_index(c)
			xor_to_child = child_index ^ 1

			if not node.children[xor_to_child]:
				xor_to_child ^= 1

			xor_max <<= 1
			xor_max |= (child_index ^ xor_to_child)

			node = node.children[xor_to_child] 
		return (xor_max, node.idx)

	def zero(self, c):
		if ord(c) == ord('0'):
			return True
		return False

	def get_child_index(self, c):
		if (self.zero(c) == True):
			return 0
		return 1
	
	def format_binary(self, k):
		s = bin(k)
		s = s[2:len(s)]
		s = s.zfill(self.MAX_BIN)
		return s

class MaxSubArrayXor:
	@staticmethod
	def do_max_notcontiguous(a: list):
		t = Trie()
		pre = 0
		ans = 0
		t.insert(a[0])
		idx = 0
		for i in range(1, len(a)):
			(pre,id) = t.query(a[i])
			t.insert(pre, i)
	
			if pre > ans:
				ans = pre
				idx = i
		return (ans,idx)

	@staticmethod
	def do_max_contiguous(a: list):
		t = Trie()
		pre = 0
		ans = 0
		idx_b = 0
		idx_e = 0
		t.insert(0, 0)
		for i in range(0, len(a)):
			pre = pre ^ a[i]
			t.insert(pre, i)
			(pre_xor, idx) = t.query(pre)
			if ans < pre_xor:
				ans = pre_xor
				idx_b = idx + 1
				idx_e = i
		return (ans, idx_b, idx_e)

class test:
	def do_unit_test_1(self):
		t = Trie()
		t.insert(7)
		t.insert(2)
		maxXor = t.query(4)
		print("Maximum XOR:", maxXor)

	def do_test_1(self):
		a = [1, 0, 5, 4, 2]
		(xor_max,idx) = MaxSubArrayXor.do_max_notcontiguous(a)
		print("The array:", a)
		print("Maximum XOR of the sub-array:", xor_max, " : id:", idx)

	def do_test_2(self):
		a = [1, 0, 5, 4, 2]
		(xor_max, idx_b, idx_e) = MaxSubArrayXor.do_max_contiguous(a)
		print("The array:", a)
		print("Maximum XOR of the sub-array:",idx_b, " - ", idx_e, " is:", xor_max)

#t = test()
#t.do_unit_test_1()
#t.do_test_1()
#t.do_test_2()
