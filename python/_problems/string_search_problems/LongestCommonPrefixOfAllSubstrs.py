"""
Problem:
--------
Given a string s contains lowercase alphabet,
find the length of the Longest common Prefix of all substrings in O(n). 
--------

For example 

s = 'ababac' 

Then substrings are as follow: 

1: s(1, 6) = ababac 
2: s(2, 6) = babac 
3: s(3, 6) = abac 
4: s(4, 6) = bac 
5: s(5, 6) = ac 
6: s(6, 6) = c 

Now, The lengths of LCP of all substrings are as follow 

1: len(LCP(s(1, 6), s)) = 6 
2: len(LCP(s(2, 6), s)) = 0 
3: len(LCP(s(3, 6), s)) = 3 
4: len(LCP(s(4, 6), s)) = 0 
5: len(LCP(s(5, 6), s)) = 1 
6: len(LCP(s(6, 6), s)) = 0 
"""

class LCP:
	@staticmethod
	def build_lps_asc_parent_tables(p:str):
		lps = [0] * len(p)
		asc = [-1] * len(p)
		parent = [i for i in range(0, len(p))]
		
		k = -1
		lps[0] = -1
		parent[0] = 0
		for i in range(1, len(p)):
			while (k >= 0 and p[k+1] != p[i]):
				k = lps[k]
			if p[k+1] == p[i]:
				k += 1
			
			# longest prefix which is also a suffix for p[0:i]
			lps[i] = k;
			parent[i] = parent[k]
			if (asc[i-1]<0 and k>=0):
				asc[i] = i
			elif (asc[i-1]>=0 and k>=0):
				asc[i] = asc[i-1]

		return (lps,asc,parent)

	@staticmethod
	def rotate_asc_subarr(a: list, asc:list):
		i = len(asc) - 1
		while i>=0:
			if asc[i] >= 0:
				# rotate
				end = i
				start = asc[i]
				while (start < end):
					a[start],a[end] = a[end],a[start]
					end -=1
					start +=1
				# jump to the next one
				i = asc[i] - 1
			else:
				i -= 1

	@staticmethod
	def filter_lsp(lsp, parent):
		for i in range(0, len(lsp)):
			if parent[i] != 0:
				lsp[i] = 0
			else:
				lsp[i] += 1
		lsp[0] = len(lsp)
		return lsp
			
class test:
	def do_unit_1(self):
		s = "ababac"
		(lsp,asc,parent) = LCP.build_lps_asc_parent_tables(s)
		LCP.rotate_asc_subarr(lsp, asc)
		lcp = LCP.filter_lsp(lsp, parent)
		print(lcp)

t = test()
t.do_test_1()