"""
=============================================================================== 
Convex Hull: Graham scan algorithm

Time complexity: O(nlog(n))
-------------------------------------------------------------------------------
Note 1: Some corner cases hasn't been considered, however this was made just as 
      reference (e.g. collinear points).
Note 2: Install matplotlib before running this.
	- python -m pip install -U pip
	- python -m pip install -U matplotlib
===============================================================================
"""
from random import randint
from mpl_toolkits.axisartist.clip_path import atan2
import matplotlib.pyplot as plt

class Point:
	x: int
	y: int
	def __init__(self,x,y):
		self.x = x
		self.y = y

class ConvexHull:
	points: list
	def __init__(self, points:list):
		self.points=points
	
	def __firstPointWithMinY(self):
		for i in range(1, len(self.points)):
			if ((self.points[i].y < self.points[0].y) or
			    (self.points[i].y == self.points[0].y and self.points[i].x < self.points[0].x)):
				self.points[0], self.points[i] = self.points[i] ,self.points[0]

	def __doPolarAngle(self, p:Point):
		dX = self.points[0].x - p.x
		dY = self.points[0].y - p.y
		return atan2(dY,dX)

	def __isConcav(self, p1:Point, p2:Point, p3:Point):
		"""
		Let 2 vectors that have counter-clockwise numbered points: P1, P2 and P3.
		The z-coordinate of the cross product of the two vectors P1P2 and P1P3 given by the
		expression (x2-x1)(y3-y1)-(y2-y1)(x3-x1) gives the right information about the turn.
			see:	https://en.wikipedia.org/wiki/Cross_product
				https://en.wikipedia.org/wiki/Graham_scan
			if Cz > 0 convex
			if Cz = 0 colinear
			if Cz < 0 concav
		"""
		z = (p2.x-p1.x)*(p3.y-p1.y) - (p2.y-p1.y)*(p3.x-p1.x)
		if z<=0:
			return True
		return False	

	def doGrahamScan(self):
		if (len(self.points) < 3):
			return self.points

		# first point (points[0]) will be the one with the lowest Y
		self.__firstPointWithMinY()

		# sort points by polar angle in counterclockwise order around points[0]
		self.points[1:] = sorted(self.points[1:], key=self.__doPolarAngle, reverse=False)

		S = []
		S.append(self.points[0])
		S.append(self.points[1])
		S.append(self.points[2])
		for i in range(3, len(self.points)):
			while (True):
				top = S[len(S)-2]
				nextToTop = S[len(S)-1]
				if self.__isConcav(top, nextToTop, self.points[i]) == True:
					S.pop()
				else:
					break
			S.append(self.points[i])
			self.plotHull(S, False)
		self.plotHull(S, True)
		return S
	
	def printPoints(self):
		for i in range(len(self.points)):
			print(self.points[i].x, ",", self.points[i].y)

	def plotHull(self, hull:list, longPause: bool, xMax=55, yMax=55):
		plt.clf()
		plt.axis([-1, xMax, -1, yMax])
		for idx in range(0, len(self.points)):
			plt.plot(
				self.points[idx].x, self.points[idx].y,
				color="black", marker="o")
		for i in range(0, len(hull)-1):
			plt.plot(
				[hull[i].x, hull[i+1].x], [hull[i].y,hull[i+1].y],
				color="red", marker="o")
		plt.plot(
			[hull[len(hull)-1].x, hull[0].x], [hull[len(hull)-1].y,hull[0].y],
			color="red", marker="o")

		if longPause == True:
			plt.pause(5)
		else:
			plt.pause(0.0001)

class testConvexHull:
	def generateRandomPoints(self, noPoints, minRand=0, maxRand=50):
		# generate a list of noPoints as [x,y] cartesian coordinates, where
		#  each coordinate is between minRand and maxRand
		points = []
		for _ in range(noPoints):
			p = Point(randint(minRand, maxRand), randint(minRand,maxRand))
			points.append(p)
		return points
	
	def testGrahamScan(self):
		points = self.generateRandomPoints(50)
		ch = ConvexHull(points)
		ch.doGrahamScan()
		print("test done...")

t = testConvexHull()
t.testGrahamScan()