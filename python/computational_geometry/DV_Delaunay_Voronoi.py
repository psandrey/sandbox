"""
=============================================================================== 
Triangulation: Naive, Delaunay -> Voronoi 

Time complexity: :) terrible for the moment
-------------------------------------------------------------------------------
Note 1:
	1. Naive Triangulation: Done
		Time complexity: O(n^2)
	2. Delaunay flipping version (from naive to Delaunay): Done
		Time complexity: terrible :)
	3. Delaunay iterrative version (based on super triangle): Not yet
	4. Delaunay -to-> Voronoi: Done
		Time complexity: O(n^2)
	5. Voronoi Diagram Bowyer-Watson version: Not yet
	6. Voronoi Diagram Fortune's version: Not yet

Note 2: Install matplotlib before running this.
	- python -m pip install -U pip
	- python -m pip install -U matplotlib
===============================================================================
"""
from math import atan2
from math import sqrt
from math import ceil
from random import uniform
from random import shuffle
import matplotlib.pyplot as plt

class Vertex:
	x:float
	y:float
	def __init__(self, x, y):
		self.x = x
		self.y = y

class Triangle:
	vIdx: list

	# vIdx1, vIdx2, vIdx3 are three indexes in a list of vertices
	def __init__(self, vIdx1: int, vIdx2:int, vIdx3:int):
		self.vIdx = [vIdx1, vIdx2, vIdx3]
		
class Circle:
	center: Vertex
	radius: float
	def __init__(self, center: Vertex, radius:float):
		self.center = center
		self.radius = radius

class CGUtils:
	@staticmethod
	def generateVertices(w: int, h: int, no: int):
		vertices = []
		for _ in range(0, no):
			x = uniform(1, w-1)
			y = uniform(1, h-1)
			#x = randint(1, w-1)
			#y = randint(1, h-1)
			p = Vertex(x, y)
			vertices.append(p)
		return vertices

	@staticmethod
	def sameCoordinates(a: Vertex, b:Vertex):
		if ((a.x == b.x) and (a.y == b.y)):
			return True
		return False

	@staticmethod
	def doGravityPoint(v1:Vertex, v2:Vertex, v3:Vertex):
		s_x = v1.x + v2.x + v3.x
		s_y = v1.y + v2.y + v3.y
		gx = s_x / 3
		gy = s_y / 3
		return Vertex(gx, gy)
	
	@staticmethod
	def doPolarAngle(v:Vertex, g:Vertex):
		dX = v.x - g.x
		dY = v.y - g.y
		return atan2(dY,dX)

	@staticmethod
	def ccw(a,b,c):
		return (c.y-a.y) * (b.x-a.x) > (b.y-a.y) * (c.x-a.x)
	
	@staticmethod
	def areIntersect(a:Vertex ,b:Vertex, c:Vertex, d:Vertex):
		""" True if [a,b] intersect with [c,d] """
		return CGUtils.ccw(a,c,d) != CGUtils.ccw(b,c,d) and CGUtils.ccw(a,b,c) != CGUtils.ccw(a,b,d)

	@staticmethod
	def getCommonEdge(t1: Triangle, t2: Triangle):
		edge1 = []
		edge2 = []
		n = 0
		for i in range(0, len(t1.vIdx)):
			for j in range(0, len(t2.vIdx)):
				if t1.vIdx[i] == t2.vIdx[j]:
					n += 1
					edge1.append(i)
					edge2.append(j)

		# has a common edge
		if n == 2:
			return (edge1[0], edge1[1]), (edge2[0], edge2[1]) 

		return None

	@staticmethod
	def isPointInCircumcircle(vertices, i1, i2, i3, i4):
		"""
		Let a,b,c three points having the coordinate .x, .y .
		These points are given in counterclockwise by their polar angle.
		And, the circle determined by a,b and c having the center coordinate
		C.x, C.y and radius r.
		
		Find if point d is inside circle C or outside.
		"""
		v1 = vertices[i1]
		v2 = vertices[i2]
		v3 = vertices[i3]

		# sort the three points in counter clockwise by their polar angle around their gravity point
		g = CGUtils.doGravityPoint(v1, v2, v3)
		cCw = [v1, v2, v3]
		cCw = sorted(cCw,  key = lambda v: CGUtils.doPolarAngle(v, g), reverse=False)
		a = cCw[0]
		b = cCw[1]
		c = cCw[2]
		d = vertices[i4]

		d_adx = a.x - d.x
		d_ady = a.y - d.y
		d_bdx = b.x - d.x
		d_bdy = b.y - d.y
		d_cdx = c.x - d.x
		d_cdy = c.y - d.y
		s_ad2 = d_adx**2 + d_ady**2
		s_bd2 = d_bdx**2 + d_bdy**2
		s_cd2 = d_cdx**2 + d_cdy**2
		
		det = d_adx*d_bdy*s_cd2 + d_ady*s_bd2*d_cdx + s_ad2*d_bdx*d_cdy \
			- d_cdx*d_bdy*s_ad2 - d_cdy*s_bd2*d_adx - s_cd2*d_bdx*d_ady

		r = False
		if det > 0:
			r = True

		return r

	@staticmethod
	def circleByThreePoints(a:Vertex, b:Vertex, c:Vertex):
		"""
		Let a,b,c three points having the coordinate .x, .y, and
		the circle having the center coordinate C.x, C.y and radius r.
		
		The circle coordinates arise by solving the following system: 
		(a.x-C.x)^2 + (a.y-Y.c)^2 - r^2 = 0
		(b.x-C.x)^2 + (b.y-Y.c)^2 - r^2 = 0
		(c.x-C.x)^2 + (c.y-Y.c)^2 - r^2 = 0
		
		then, find the radius by using any of the three equation.
		"""
		#if ((CGUtils.sameCoordinates(a, b) == True) or
		#    (CGUtils.sameCoordinates(a, c) == True) or
		#    (CGUtils.sameCoordinates(b, c) == True)):
		#	return None

		# corner cases not handled !!!
		# e.g. collinear points.
		d_acx = a.x - c.x
		d_acy = a.y - c.y
		d_abx = a.x - b.x
		d_aby = a.y - b.y
		s_a2  = a.x**2 + a.y**2
		s_b2  = b.x**2 + b.y**2
		s_c2  = c.x**2 + c.y**2
		
		Cx_toDivide = d_aby * (s_a2 - s_c2) - d_acy *(s_a2 - s_b2)
		Cx_divdeWith = 2*(d_acx * d_aby - d_abx * d_acy)
		Cx = Cx_toDivide / Cx_divdeWith
		
		Cy_toDivide = d_abx * (s_a2 - s_c2) - d_acx * (s_a2 - s_b2)
		Cy_divdeWith = 2*(d_acy *d_abx - d_acx * d_aby) 
		Cy = Cy_toDivide / Cy_divdeWith
		Cxy = Vertex(Cx, Cy)

		radius_2 = (Cxy.x - a.x)**2 + (Cxy.y-a.y)**2
		radius = sqrt(radius_2)

		C = Circle(Cxy, radius)
		return C
	
	@staticmethod
	def crossProduct(a: Vertex, v1: Vertex, v2: Vertex):
		return (v1.x-a.x)*(v2.y-a.y) - (v1.y-a.y)*(v2.x-a.x)
	
	@staticmethod
	def verticesOnSameSide(a:Vertex, b:Vertex, v1:Vertex, v2:Vertex):
		cp1 = CGUtils.crossProduct(a, b, v1)
		cp2 = CGUtils.crossProduct(a, b, v2)
		if cp1 * cp2 > 0: return True

		# Note: returns false if one vertex is colinear
		return False

	@staticmethod
	def vertexInTriangle(vertices: list, idx:int, t:Triangle):
		v0 = vertices[t.vIdx[0]]
		v1 = vertices[t.vIdx[1]]
		v2 = vertices[t.vIdx[2]]
		v = vertices[idx]

		if (CGUtils.verticesOnSameSide(v1, v2, v, v0) == True and
			CGUtils.verticesOnSameSide(v0, v2, v, v1) == True and
			CGUtils.verticesOnSameSide(v0, v1, v, v2) == True):
			return True
		return False

	@staticmethod
	def overlapTriangles(vertices: list, t1:list, t2: Triangle):
		t1v0 = vertices[t1.vIdx[0]]
		t1v1 = vertices[t1.vIdx[1]]
		t1v2 = vertices[t1.vIdx[2]]

		t2v0 = vertices[t2.vIdx[0]]
		t2v1 = vertices[t2.vIdx[1]]
		t2v2 = vertices[t2.vIdx[2]]
		# t1
		if (CGUtils.verticesOnSameSide(t1v0, t1v1, t1v2, t2v0) == False and 
			CGUtils.verticesOnSameSide(t1v0, t1v1, t1v2, t2v1) == False and
			CGUtils.verticesOnSameSide(t1v0, t1v1, t1v2, t2v2) == False):
			return False
		
		if (CGUtils.verticesOnSameSide(t1v0, t1v2, t1v1, t2v0) == False and 
			CGUtils.verticesOnSameSide(t1v0, t1v2, t1v1, t2v1) == False and
			CGUtils.verticesOnSameSide(t1v0, t1v2, t1v1, t2v2) == False):
			return False

		if (CGUtils.verticesOnSameSide(t1v1, t1v2, t1v0, t2v0) == False and 
			CGUtils.verticesOnSameSide(t1v1, t1v2, t1v0, t2v1) == False and
			CGUtils.verticesOnSameSide(t1v1, t1v2, t1v0, t2v2) == False):
			return False

		# t2
		if (CGUtils.verticesOnSameSide(t2v0, t2v1, t2v2, t1v0) == False and 
			CGUtils.verticesOnSameSide(t2v0, t2v1, t2v2, t1v1) == False and
			CGUtils.verticesOnSameSide(t2v0, t2v1, t2v2, t1v2) == False):
			return False
		
		if (CGUtils.verticesOnSameSide(t2v0, t2v2, t2v1, t1v0) == False and 
			CGUtils.verticesOnSameSide(t2v0, t2v2, t2v1, t1v1) == False and
			CGUtils.verticesOnSameSide(t2v0, t2v2, t2v1, t1v2) == False):
			return False

		if (CGUtils.verticesOnSameSide(t2v1, t2v2, t2v0, t1v0) == False and 
			CGUtils.verticesOnSameSide(t2v1, t2v2, t2v0, t1v1) == False and
			CGUtils.verticesOnSameSide(t2v1, t2v2, t2v0, t1v2) == False):
			return False

		return True

	@staticmethod
	def plotPoint(v: Vertex):
		plt.plot( v.x , v.y, color="brown", marker="o" )

	@staticmethod
	def plotPoints(vertices: list):
		for i in range(0, len(vertices)):
			v = vertices[i]
			CGUtils.plotPoint(v)

	@staticmethod
	def plotTriangle(vertices:list, t: Triangle, a, c):
		v0 = vertices[t.vIdx[0]]
		v1 = vertices[t.vIdx[1]]
		v2 = vertices[t.vIdx[2]]
		plt.plot([v0.x, v1.x], [v0.y,v1.y], color=c, marker="o", alpha=a)
		plt.plot([v0.x, v2.x], [v0.y,v2.y], color=c, marker="o", alpha=a )
		plt.plot([v1.x, v2.x], [v1.y,v2.y], color=c, marker="o", alpha=a)

	@staticmethod
	def plotCircle(C: Circle, a, c):
		ax = plt.gca()
		plt.plot(C.center.x , C.center.y, color="black", marker="+", alpha=0.1)
		Circ = plt.Circle((C.center.x , C.center.y), C.radius, edgecolor=c, fill=False, alpha=a)
		ax.add_patch(Circ)
	
	@staticmethod
	def plotMesh(vertices: list, mesh:list, alpha):
		plt.clf()
		for i in range(0, len(mesh)):
			t = mesh[i]
			CGUtils.plotTriangle(vertices, t, alpha, "blue")
		plt.axis('scaled')

class Triangulation:
	w: int
	h: int
	superT : Triangle

	def __init__ (self, w: int, h:int):
		self.w = w
		self.h = h
		self.triangles = []

	def doNaiveTriangulation(self, vertices: list):
		if len(vertices) < 3:
			return None

		# Note: for debug reasons only
		shuffle(vertices)

		mesh = [Triangle(0, 1, 2)]
		for i in range(3, len(vertices)):
			
			exterior = True
			t = None
			for j in range(0, len(mesh)):
				t = mesh[j]
				if (CGUtils.vertexInTriangle(vertices, i, t)):
					exterior = False
					break

			# vertex interior (found a triangle that contains vertex i)
			if (exterior == False):
				mesh += self.__splitTriangle(i, t)
				mesh.remove(t)
			# vertex exterior (no triangle contains vertex i)
			else:
				mesh = self.__addExteriorTriangles(vertices, mesh, i)

		return mesh
	
	def __splitTriangle(self, i:int, t:Triangle):
		i0 = t.vIdx[0]
		i1 = t.vIdx[1]
		i2 = t.vIdx[2]
		t1 = Triangle(i, i0, i1)
		t2 = Triangle(i, i1, i2)
		t3 = Triangle(i, i0, i2)
		return [t1, t2, t3]

	def __addExteriorTriangles(self, vertices: list, mesh:list, i: int):
		for j in range(0, len(mesh)):
			t = mesh[j]
			i0 = t.vIdx[0]
			i1 = t.vIdx[1]
			i2 = t.vIdx[2]
			candidate_t0 = Triangle(i, i0, i1)
			candidate_t1 = Triangle(i, i0, i2)
			candidate_t2 = Triangle(i, i1, i2)

			if (self.__viableCandidateTriangle(vertices, mesh, candidate_t0) == True):
				mesh.append(candidate_t0)
			
			if (self.__viableCandidateTriangle(vertices, mesh, candidate_t1) == True):
				mesh.append(candidate_t1)
			
			if (self.__viableCandidateTriangle(vertices, mesh, candidate_t2) == True):
				mesh.append(candidate_t2)

		return mesh

	def __viableCandidateTriangle(self, vertices:list, mesh:list, candidate: Triangle):
		for j in range(0, len(mesh)):
			t = mesh[j]
			if (CGUtils.overlapTriangles(vertices, candidate, t) == True):
				return False
		return True

	def doDelaunayTriangulationFlippingVersion(self, vertices: list, live: bool):
		if len(vertices) < 3:
			return None
		
		# construct an arbitrary mesh
		mesh = self.doNaiveTriangulation(vertices)

		if live == True:
			CGUtils.plotMesh(vertices, mesh, 0.25)
			plt.pause(5)

		# flip until triangulation becomes Delaunay
		flip = True
		while flip == True:
			flip = False

			for i in range(0 , len(mesh)):
				t1 = mesh[i]
				for j in range(i+1, len(mesh)):
					t2 = mesh[j]

					if live == True:
						CGUtils.plotMesh(vertices, mesh, 0.25)
						CGUtils.plotTriangle(vertices, mesh[i], 1, "red")
						CGUtils.plotTriangle(vertices, mesh[j], 1, "green")
						a=vertices[t1.vIdx[0]]
						b=vertices[t1.vIdx[1]]
						c=vertices[t1.vIdx[2]]
						C=CGUtils.circleByThreePoints(a, b, c)
						CGUtils.plotCircle(C, 0.25, "black")
						plt.pause(0.0001)
					
					# has t1 and t2 any common edge ?
					edge = CGUtils.getCommonEdge(t1, t2)
					if edge == None: continue

					# if t1 and t2 has a common edge, check if the 2 triangles are Delaunay and if not flip
					if (self.__doFlip(vertices, edge, mesh[i], mesh[j]) == True):
						if live == True:
							CGUtils.plotMesh(vertices, mesh, 0.25)
							CGUtils.plotTriangle(vertices, mesh[i], 1, "red")
							CGUtils.plotTriangle(vertices, mesh[j], 1, "red")
							plt.pause(0.0001)
						flip = True

		return mesh

	def __doFlip(self, vertices, edge, t1, t2):
		# edge is a tuple that contains ((edge1), (edge2)), edge1 from t1 and edge2 from t2
		idxForthT2 = self.__getForthPoint(edge, 2)
		if CGUtils.isPointInCircumcircle(vertices,
				t1.vIdx[0], t1.vIdx[1], t1.vIdx[2], t2.vIdx[idxForthT2]) == True:
			idxT1 = self.__getForthPoint(edge, 1)
			idxT2 = self.__getForthPoint(edge, 2)
			edge1 = edge[0]
			edge2 = edge[1]
			t1.vIdx[edge1[0]] = t2.vIdx[idxT2]
			t2.vIdx[edge2[1]] = t1.vIdx[idxT1]
			return True
		return False

	def __getForthPoint(self, edge, which):
		if which == 1: e = edge[0]
		else: e = edge[1]
		idx = 3 - e[0] - e[1]
		return idx

	def __doSuperTriangle(self, w:int, h:int):
		epsilon = 1
		if (w/2 > h) : delta = h
		else: delta = ceil(w/2)
		delta += epsilon
		
		# the height of the super triangle
		H = h + ceil((h * w) / (2 * delta))
		
		p1 = Vertex(-delta, -epsilon)
		p2 = Vertex(w+delta, -epsilon)
		p3 = Vertex(int(w/2), H)
		return (p1, p2, p3)

class Voronoi:
	def doDelaunayToVoronoi(self, vertices: list, mesh: list):
		circs = self.__computeTriangleCircumcirle(vertices, mesh)
		
		CGUtils.plotMesh(vertices, mesh, 0.25)
		plt.axis('scaled')
		for i in range(0, len(circs)):
			c = circs[i]
			CGUtils.plotCircle(c, 0.25, "green")
		plt.pause(0.0001)
		print()
		
		self.drawVoronoi(vertices, mesh, circs)

	def __computeTriangleCircumcirle(self, vertices:list, mesh:list):
		circumcircles = []
		for j in range(0, len(mesh)):
			t = mesh[j]
			v1 = vertices[t.vIdx[0]]
			v2 = vertices[t.vIdx[1]]
			v3 = vertices[t.vIdx[2]]
			C = CGUtils.circleByThreePoints(v1, v2, v3)
			circumcircles.append(C)
		return circumcircles
	
	def drawVoronoi(self, vertices:list, mesh:list, circs:list):
		for i in range(0, len(mesh)):
			t1 = mesh[i]
			for j in range(0, len(mesh)):
				t2 = mesh[j]
				if (CGUtils.getCommonEdge(t1, t2) != None):
					c1 = circs[i]
					c2 = circs[j]
					plt.plot([c1.center.x, c2.center.x], [c1.center.y, c2.center.y], color="red", marker="o", alpha=1) 
		plt.pause(10)
		return []

class testTriangulation:
	def doUnitTest_1(self):
		v1 = Vertex(33,40)
		v2 = Vertex(4,44)
		v3 = Vertex(14,22)
		v4 = Vertex(82,42)
		
		r = CGUtils.isPointInCircumcircle(v1, v2, v3, v4)
		print(r)

		C = CGUtils.circleByThreePoints(v1, v2, v3)
		CGUtils.plotCircle(C)
		CGUtils.plotPoint(v1)
		CGUtils.plotPoint(v2)
		CGUtils.plotPoint(v3)
		CGUtils.plotPoint(v4)
		plt.pause(3)
	
	def doUnitTest_2(self):
		v1 = Vertex(1,1)
		v2 = Vertex(2,2)
		v3 = Vertex(3,1)
		v4 = Vertex(10, 10) # outside
		v5 = Vertex(2, 1.5) # inside
		vertices = [v1, v2, v3, v4, v5]
		t = Triangle(0, 1, 2)
		
		r = CGUtils.vertexInTriangle(vertices, 3, t)
		print(r)
		r = CGUtils.vertexInTriangle(vertices, 4, t)
		print(r)


	def doTestNaiveTriangulation(self):
		w = 100
		h = 50
		vertices = CGUtils.generateVertices(w, h, 10)

		dt = Triangulation(w, h)
		mesh = dt.doNaiveTriangulation(vertices)
		CGUtils.plotMesh(vertices, mesh, 1)
		print()

	def doTestDelaunayTriangulation(self):
		w = 100
		h = 100
		vertices = CGUtils.generateVertices(w, h, 20)

		dt = Triangulation(w, h)
		mesh = dt.doDelaunayTriangulationFlippingVersion(vertices, True)

		CGUtils.plotMesh(vertices, mesh, 1)
		plt.pause(10)
	
	def doTestVoronoi(self):
		w = 100
		h = 100
		vertices = CGUtils.generateVertices(w, h, 20)
		dt = Triangulation(w, h)
		mesh = dt.doDelaunayTriangulationFlippingVersion(vertices, False)

		vd = Voronoi()
		vd.doDelaunayToVoronoi(vertices, mesh)

t = testTriangulation()
#t.doUnitTest_1()
#t.doUnitTest_2()
#t.doTestNaiveTriangulation()
#t.doTestDelaunayTriangulation()
t.doTestVoronoi()
print(" Test done... ")