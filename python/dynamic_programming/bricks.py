"""
------------------------------------------------------------------------------- 
The algorithm below is the dynamic programming version of the following problem

Time complexity: O(n^2)
===============================================================================

Problem:
 Given a set of N bricks. Each brick has three dimensions: Length, Width, Height. 
 What is the tallest tower that can be build using a sub-set from the N-brick set
 by putting one brick on top of the other such that the face of the bottom brick
 to include the face of top brick for every two consecutive bricks.

For example:
 - brick 1: 1, 2, 3
 - brick 2: 4, 6, 5

The tallest tower that can be built is by putting brick 1 over brick 2: the height is 9.
The side composed by 4 and 5 can include the side composed by 1 and 2, because 4>=1 and 5 >= 2.

	     |
	     3  /
	     | 2
	--1--/
	     |   
	     6  /
	     | 5
	--4--/ 

"""

class Brick:
	id: int
	d1: int
	d2: int
	d3: int
	def __init__(self, ID:int, d1:int, d2:int, d3:int):
		self.id = ID
		self.d1 = d1
		self.d2 = d2
		self.d3 = d3

class Bricks:
	@staticmethod
	def doBricks(bricks: list):
		"""
		Steps:
		1. build a set that contains all possible position of each brick (3 positions);
		2. sort the dimensions of each brick's face;
		3. sort the entire set by the first dimension to assure that when bricks
		    are stacked the first dimension of the bottom brick will always fit
		    the first dimension of the top brick;
		4. find the longest ascending subset by dimension 2 of each brick's face;
		5. for duplicate bricks (since each brick may appear at most 3 times) keep
		    the brick with the biggest third dimension (the highest).
		"""
		arr = Bricks.builSet(bricks)
		Bricks.sortBricksFaces(arr)
		Bricks.sortBricks(arr)
		lass = Bricks.doDPLASS(arr)
		heighest = Bricks.removeDuplicateAndKeepTheHeighest(lass)

		print(" The heighest: ")
		Bricks.printArray(heighest)

	@staticmethod
	def builSet(bricks: list):
		arr = []
		for idx in range(0, len(bricks)):
			# Each brick as 3 dimensions, so 3 faces
			# Each brick can be placed on any of the 3 faces, so make a set
			#  with all these possibilities...
			b1 = Brick(idx, bricks[idx].d1, bricks[idx].d2, bricks[idx].d3)
			b2 = Brick(idx, bricks[idx].d2, bricks[idx].d3, bricks[idx].d1)
			b3 = Brick(idx, bricks[idx].d1, bricks[idx].d3, bricks[idx].d2)
			arr.append( b1 )
			arr.append( b2 )
			arr.append( b3 )
		return arr

	@staticmethod
	def sortBricksFaces(arr: list):
		# each face is sorted such that the smallest dimension to be on the left side
		for b in arr:
			if b.d1 > b.d2:
				b.d1, b.d2 = b.d2, b.d1

	@staticmethod
	def sortBricks(arr: list):
		# Since the first dimension for each side is the smallest of the two,
		#  then by sorting the array will ensure that when bricks are stacked
		#  the first dimension of the bottom brick will always fit the first dimension
		#  of the top brick, so the only thing that remains to be done is to find
		#  the longest ascending subset based on the second dimension such that the 
		#  height is the maximum possible.
		for j in range (1, len(arr)):
			key = arr[j]
			i = j - 1
			while i >= 0 and ((arr[i].d1 < key.d1) or 
					  (arr[i].d1 == key.d1 and arr[i].d2 < key.d2)):
				arr[i], arr[i+1] = arr[i+1], arr[i]
				i = i-1

			arr[i+1] = key
		return arr
	
	@staticmethod
	def doDPLASS(arr:list):
		dp = [0] * len(arr)
		pred = [0] * len(arr)
		
		dp[0] = 1
		pred[0] = 0
		maxLen = 1
		maxId = 0
		for i in range(1, len(arr)):
			dp[i] = 1
			pred[i] = i
			for j in range(0, i):
				if arr[i].d2 <= arr[j].d2 and dp[i] < dp[j] + 1:
					dp[i] = dp[j] + 1
					pred[i] = j
					if (maxLen < dp[i]):
						maxLen = dp[i]
						maxId = i
		lass = []
		k = maxId		
		while True:
			lass.append(arr[k])
			if pred[k] == k:
				break
			k = pred[k]
			
		return lass

	@staticmethod
	def removeDuplicateAndKeepTheHeighest(arr: list):
		for i in range(0, len(arr)):
			k = i
			j = i + 1
			while (j < len(arr)):
				if (arr[k].id == arr[j].id):
					if (arr[k].d3 > arr[j].d3):
						arr.remove(arr[j])
						continue
					else:
						arr.remove(arr[k])
						k = j
				j += 1
		return arr

	@staticmethod
	def printArray(arr: list):
		k = 0
		for b in arr:
			print(k, ">", b.id, ":(", b.d1, ",", b.d2, ",", b.d3, ")")
			k += 1
		
class testBricks:
	def doUnitTest(self):
		arr = []
		b1 = Brick(0, 1, 1, 2)
		b2 = Brick(1, 2, 1, 1)
		b3 = Brick(2, 2, 1, 1)
		b4 = Brick(0, 2, 1, 1)
		arr.append( b1 )
		arr.append( b2 )
		arr.append( b3 )
		arr.append( b4 )
		Bricks.printArray(arr)
		
		Bricks.sortBricksFaces(arr)
		print(" -- ")
		Bricks.printArray(arr)

		Bricks.sortBricks(arr)
		print(" -- ")
		Bricks.printArray(arr)

		lass = Bricks.doDPLASS(arr)
		print(" -- ")
		Bricks.printArray(lass)
		
		heighest = Bricks.removeDuplicateAndKeepTheHeighest(lass)
		print(" -- ")
		Bricks.printArray(heighest)

	def doTest(self):
		#	  (id,L, l, h)
		b1 = Brick(0, 1, 2, 3)
		b2 = Brick(1, 4, 5, 6)
		b3 = Brick(2, 7, 8, 9)
		arr = []
		arr.append( b1 )
		arr.append( b2 )
		arr.append( b3 )
		Bricks.doBricks(arr)

t = testBricks()
#t.doUnitTest()
t.doTest()