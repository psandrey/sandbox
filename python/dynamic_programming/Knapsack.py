"""
-------------------------------------------------------------------------------
 The Knapsack problem:

 Given a set of n objects. Each object has a price and a weight (p[i],w[i]),
 where p[i], w[i] > 0. What is the maximum profit that can be achieved by 
 a collection so that the total weight of the collection is less than or equal
 to the given limit W.
 
 Solutions:
 1. Brute-force (backtracking based); Time complexity: O(2^n)
 2. Dynamic programming recursive (top-down approach): Time complexity: O(n*W)
 3. Dynamic programming iterative (bottom-up approach): Time complexity: O(n*W) 
-------------------------------------------------------------------------------
"""
class Knapsack:
	it = 0
	totit = 0
	dp = [[]]

	def init0(self, n, W):
		self.dp =  [[0] * (W+1) for _ in range(n)]

	@staticmethod
	def  doKnapsackBF(i: int, cap: int, w, p):
		# base case
		if ( i == 0 ):
			sol = 0
		# general case
		else:
			sol = Knapsack.doKnapsackBF ( i-1, cap, w, p )
			
			if ( cap - w[i] >= 0):
				t = Knapsack.doKnapsackBF ( i-1, cap - w[i], w, p ) + p[i]
				sol = max (sol, t)
		return sol

	@staticmethod
	def doKnapsackDPRecursive(i: int, cap: int, w, p, dp):
		if (dp[i-1][cap] != -1):
			return dp[i-1][cap];

		if ( i == 0 or cap == 0):
			sol = 0
		else:
			sol = Knapsack.doKnapsackDPRecursive ( i-1, cap, w, p, dp )
			if ( cap - w[i] >= 0):
				t  = Knapsack.doKnapsackDPRecursive ( i-1, cap - w[i], w, p, dp ) + p[i]
				sol = max (sol, t)

		dp[i][cap] = sol
		return sol

	@staticmethod
	def doKnapsackDPIterative(W, w, p):
		dp =  [[0] * (W+1) for _ in range(len(p))]

		n = len(w)
		for i in range (1, n, 1):
			for cap in range(1, W+1, 1):
				sol = dp[i-1][cap]
				if (cap-w[i]  >= 0):
					t = dp[i-1][cap-w[i]] + p[i]
					sol = max (sol, t)

				dp[i][cap] = sol

		return dp[n-1][W]

	def printDP(self):
		print(self.dp)

class testKnapsack:
	def doTestKnapsackBF(self):
		n = 5 # n objects
		w = [0, 3, 3, 1, 1, 2] # weights
		p = [0, 6, 3, 2, 8, 5] # prices
		W = 3 # weight limit
		r = Knapsack.doKnapsackBF(n, W, w, p)
		print("BF> Knapsack maximum profit is:", r)

	def doTestKnapsackDPRecursive(self):
		w = [0, 3, 3, 1, 1, 2]
		p  = [0, 6, 3, 2, 8, 5]
		n = 5
		W = 3
		dp = [[-1] * (W+1) for _ in range(n+1)]
		r = Knapsack.doKnapsackDPRecursive(n, W, w, p, dp)
		print("DP recursive> Knapsack maximum profit is:", r)

	def doTestKnapsackDPIterative(self):
		w = [0, 3, 3, 1, 1, 2]
		p  = [0, 6, 3, 2, 8, 5]
		W = 3
		r = Knapsack.doKnapsackDPIterative(W, w, p)
		print("DP iterative> Knapsack maximum profit is:", r)

t = testKnapsack()
#t.doTestKnapsackBF()
#t.doTestKnapsackDPRecursive()
#t.doTestKnapsackDPIterative()
