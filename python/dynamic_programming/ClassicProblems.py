"""
-------------------------------------------------------------------------------
1. Largest sum contiguous subset
2. Longest ascending subset
3. Longest common subset
-------------------------------------------------------------------------------
"""

#------------------------------------------------------------------------------
# 1. Largest sum contiguous subset
#------------------------------------------------------------------------------
class LSCSS:
	@staticmethod
	def doLSCSS(a: list):
		dp = [0] * len(a)
		pred = [0] * len(a)
		
		dp[0] = a[0]
		pred[0] = 0
		for i in range(1, len(a)):
			if dp[i-1] > 0:
				dp[i] = dp[i-1] + a[i]
				pred[i] = i-1
			else:
				dp[i] = a[i]
				pred[i] = i
		return (dp, pred);

	@staticmethod
	def printSolution(a, dp, pred):
		sol = dp[0] 
		k = 0
		for i in range(1, len(dp)):
			if sol < dp[i]:
				sol = dp[i]
				k = i

		print("Maximum sum is:", sol)
		i = k
		while True:
			print(a[i], ",", end="")
			if pred[i] == i:
				break
			else:
				i = pred[i]
		print("")

class testLSCSS:
	def doTest(self):
		a = [-10, 2, 3, -1, 2, -3]
		(dp, pred) = LSCSS.doLSCSS(a)
		LSCSS.printSolution(a, dp, pred)
#t = testLSCSS()
#t.doTest()

#------------------------------------------------------------------------------
# 2. Longest ascending subset
#------------------------------------------------------------------------------
class LASS:
	@staticmethod
	def doLASS(a:list):
		dp = [0] * len(a)
		pred = [0] * len(a)
		
		dp[0] = 1
		pred[0] = 0
		for i in range(1, len(a)):
			dp[i] = 1
			pred[i] = i
			for j in range(0, i):
				if a[i] > a[j] and dp[i] < dp[j] + 1:
					dp[i] = dp[j] + 1
					pred[i] = j
		return (dp, pred)	

	@staticmethod
	def printSolution(a, dp, pred):
		sol = dp[0] 
		k = 0
		for i in range(1, len(dp)):
			if sol < dp[i]:
				sol = dp[i]
				k = i

		print("Longest subset is:", sol)
		i = k
		while True:
			print(a[i], ",", end="")
			if pred[i] == i:
				break
			else:
				i = pred[i]
		print("")

class testLASS:
	def doTest(self):
		a = [100, 12, 13, -1, 15, -30]
		(dp, pred) = LASS.doLASS(a)
		LASS.printSolution(a, dp, pred)
#t = testLASS()
#t.doTest()

#------------------------------------------------------------------------------
# 3. Longest common subset
#------------------------------------------------------------------------------
class LCSS:
	@staticmethod
	def doLCSS(a: list, b: list):
		dp = [[0 for j in range(0, len(b))] for i in range(0, len(a))]
		predi = [i for i in range(0, len(a))]
		predj = [j for j in range(0, len(b))]
		
		previ = -1
		prevj = -1
		maxi = 0
		maxj = 0
		for i in range(0, len(a)):
			for j in range(0, len(b)):
				if (a[i] == b[j]):
					dp[i][j] = dp[i-1][j-1] + 1
					
					predi[i] = previ if previ != -1 else i
					previ = i
					predj[j] = prevj if prevj != -1 else j
					prevj = j
					maxi = i
					maxj = j
				else:
					dp[i][j] = max(dp[i][j-1], dp[i-1][j])

		return (dp, predi, predj, maxi, maxj)

	@staticmethod
	def printLCSS(a, b, dp, predi, predj, maxi, maxj):
		print(a)
		print(b)
		print("Maximum common subset is of length:", dp[len(a)-1][len(b)-1])
		print("a's predecessors are:", predi)
		print("b's predecessors are:", predj)
		i = maxi
		while True:
			print(a[i], ",", end="")
			if i == predi[i]:
				break
			else:
				i = predi[i]
		print("")
class testLCSS:
	def doTest(self):
		a = [6, -1, 9]
		b = [0, 6, 2, 9, 8]
		(dp, predi, predj, maxi, maxj) = LCSS.doLCSS(a, b)
		LCSS.printLCSS(a, b, dp, predi, predj, maxi, maxj)
		
#t = testLCSS()
#t.doTest()