"""
-------------------------------------------------------------------------------
 The coins problem:

 Given n types of coins, and an unlimited number of coins of each type.
 What is the minimum number of coins to pay the amount S.
 Note: if the problem doesn't have a solution, then return -1.
 
 Solutions:
 1. Brute-force (backtracking based); Time complexity: O(S^n)
 2. Dynamic programming recursive (top-down approach): Time complexity: O(S*n)
 3. Dynamic programming iterative (bottom-up approach): Time complexity: O(S*n)
-------------------------------------------------------------------------------
"""

class Coins:
	MAX_INT = 0xffffffff

	@staticmethod
	def doCoinsBF(i: int, s: int, c: list):
		if (s == 0):
			return 0

		n =  len(c)
		if ( i < n and s > 0):
			maxCi = int( s/c[i] )
			minNoCoins = Coins.MAX_INT
			for x in range (0,maxCi+1, 1):
				if ( s >= x*c[i] ):
					rest = Coins.doCoinsBF( i+1, s - x*c[i], c )
					if (rest != -1):
						minNoCoins = min ( minNoCoins, rest + x )
			
			if ( minNoCoins == Coins.MAX_INT):
				return -1
			return minNoCoins
		return -1

	@staticmethod
	def doCoinsDPRecursive(rem:int, c:list, dp:list):
		if rem < 0:
			return -1
		elif rem == 0:
			return 0
		if dp[rem - 1] != -1:
			return dp[rem - 1]
		
		minNoCoins = Coins.MAX_INT
		for i in range(0, len(c)):
			res = Coins.doCoinsDPRecursive(rem - c[i], c, dp)
			if res >= 0 and res < minNoCoins:
				minNoCoins = res + 1
		
		if minNoCoins == Coins.MAX_INT:
			dp[rem - 1] = -1
		else:
			dp[rem - 1] = minNoCoins
		
		return dp[rem - 1]

	@staticmethod
	def doCoinsDPIterative(s: int, c: list):
		dp = [Coins.MAX_INT] * (s+1)
		dp[0] = 0
		
		# values from 1 to s
		for v in range(1, s+1):
			# all types of coins
			for j in range(0, len(c)):
				# all coins smaller than remaining value
				if c[j] <= v:
					rem = dp[v - c[j]]
					if rem != Coins.MAX_INT and rem + 1 < dp[v]:
						dp[v] = rem + 1
		return dp[s]
		
class testCoins:
	def doTestCoinsBF(self):
		c = [1, 2, 5]
		S = 11
		r = Coins.doCoinsBF(0, S, c)
		print("The sum is:", S)
		print("Type of coins are:", c)
		print("Minimum no of coins is:", r)

	def doTestCoinsDPRecursive(self):
		c = [1, 2, 5]
		S = 11
		dp = [-1] * 11
		r = Coins.doCoinsDPRecursive(S, c, dp)
		print("The sum is:", S)
		print("Type of coins are:", c)
		print("Minimum no of coins is:", r)
		#print(dp)

	def doTestCoinsDPIterative(self):
		c = [1, 2, 5]
		S = 11
		r = Coins.doCoinsDPIterative(S, c)
		print("The sum is:", S)
		print("Type of coins are:", c)
		print("Minimum no of coins is:", r)
		#print(dp)

#t = testCoins()
#t.doTestCoinsBF()
#t.doTestCoinsDPRecursive()
#t.doTestCoinsDPIterative()