package graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UndirectedGraph {
	public HashMap<Integer, Vertex> vertices;
	public ArrayList<Edge> eList;
	public HashMap<Vertex, List<Vertex>> adjList;

	public UndirectedGraph(Integer[][] g) {
		if (g == null)
			return;

		vertices = new HashMap<Integer, Vertex>();
		adjList = new HashMap<Vertex, List<Vertex>>();
		eList = new ArrayList<Edge>();
		
		// un-weighted: { uId, vId }
		if (g[0].length == 2)
			buildUndirectedGraph(g);
		// weighted: { uId, vId, w(uId, vId) }
		else if (g[0].length == 3)
			buildUndirectedWeightedGraph(g);
		
	}

	public Integer getWeight(Vertex u, Vertex v) {
		Edge e = isEdge(u, v);
		return e.w;
	}
	
	private void buildUndirectedGraph(Integer[][] g) {
		for (int i = 0; i < g.length; i++) {
			Vertex u = getVertex(g[i][0]);
			Vertex v = getVertex(g[i][1]);
			updateEdgeList(u, v);
			updateAdjList(u, v);
			updateAdjList(v, u);
		}
	}
	
	private void buildUndirectedWeightedGraph(Integer[][] g) {
		for (int i = 0; i < g.length; i++) {
			Vertex u = getVertex(g[i][0]);
			Vertex v = getVertex(g[i][1]);
			updateEdgeList(u, v, g[i][2]);
			updateAdjList(u, v);
			updateAdjList(v, u);
		}
	}
	
	public Vertex getVertex(Integer id) {
		if (!vertices.containsKey(id)) {
			Vertex u = new Vertex(id);
			vertices.put(id, u);
			return u;
		}
	
		return vertices.get(id);
	}
	
	private void updateEdgeList(Vertex u, Vertex v) {
		if (isEdge(u, v) != null)
			return;

		eList.add(new Edge(u, v));
	}
	
	private void updateEdgeList(Vertex u, Vertex v, Integer w) {
		if (isEdge(u, v) != null)
			return;

		eList.add(new Edge(u, v, w));
	}
	
	private Edge isEdge(Vertex u, Vertex v) {
		for (Edge e : eList) {
			if ((e.from == u && e.to == v) || (e.from == v && e.to == u))
				return e;
		}
		return null;
	}
	
	private void updateAdjList(Vertex u, Vertex v) {
		List<Vertex> listU = null;
		if (!adjList.containsKey(u)) {
			listU = new ArrayList<Vertex>();
			adjList.put(u, listU);
		} else
			listU = adjList.get(u);

		listU.add(v);
	}
}
