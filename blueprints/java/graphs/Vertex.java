package graphs;

public class Vertex implements Comparable<Vertex> {
	public int id;

	public Vertex (int id) {
		this.id = id;
	}

	@Override
	public int compareTo(Vertex v) {
		// TODO Auto-generated method stub
		return (this == v) ? 0 : 1;
	}
}
