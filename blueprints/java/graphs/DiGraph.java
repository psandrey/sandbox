package graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DiGraph {
	public HashMap<Integer, Vertex> vertices;
	public ArrayList<Edge> eList;
	public HashMap<Vertex, HashMap<Vertex, Edge>> fastFromToEdge;
	public HashMap<Vertex, List<Vertex>> adjList;

	// the reversed graph
	public ArrayList<Edge> rEList;
	public HashMap<Vertex, List<Vertex>> rAdjList;

	public DiGraph(Integer[][] g) {
		if (g == null)
			return;
		
		vertices = new HashMap<Integer, Vertex>();
		adjList = new HashMap<Vertex, List<Vertex>>();
		eList = new ArrayList<Edge>();
		fastFromToEdge = new HashMap<Vertex, HashMap<Vertex, Edge>>();
		
		// the graph is weighted and undirected
		// each line of the matrix g is { uId, vId, w(uId, vId) }
		if (g[0].length == 3)
			buildWeightedDiGraph(g);
		else
			buildDiGraph(g);
	}

	public void buildReversedGraph() {
		generateReverseEdges();  //shallow copy
		generateReverseAdjList(); //shallow copy
	}
	
	private void generateReverseEdges() {
		rEList = new ArrayList<Edge>(this.eList.size());
		for (Edge e: this.eList)
			rEList.add(new Edge(e.to, e.from));
	}
	
	private void generateReverseAdjList() {
		rAdjList = new HashMap<Vertex, List<Vertex>>();
		for (Vertex u: adjList.keySet()) {
			List<Vertex> l = adjList.get(u);
			
			for (Vertex v: l) {
				List<Vertex> rl = null;
				if (!rAdjList.containsKey(v)) {
					rl = new ArrayList<Vertex>();
					rAdjList.put(v, rl);
				}
				else
					rl = rAdjList.get(v);
				
				rl.add(u);
			}
		}
	}
	
	private void buildDiGraph(Integer[][] g) {
		for (int i = 0; i < g.length; i++) {
			Vertex u = getVertex(g[i][0]);
			Vertex v = getVertex(g[i][1]);
			//System.out.println(" + from:" + u.id + " -> " + v.id);
			updatEdgeList(u, v);
			updateAdjList(u, v);
		}
	}
	
	private void buildWeightedDiGraph(Integer[][] g) {
		for (int i = 0; i < g.length; i++) {
			Vertex u = getVertex(g[i][0]);
			Vertex v = getVertex(g[i][1]);
			updateWeightedEdgeList(u, v, g[i][2]);
			updateAdjList(u, v);
		}
	}
	
	private Vertex getVertex(Integer id) {
		if (!vertices.containsKey(id)) {
			Vertex u = new Vertex(id);
			vertices.put(id, u);
			return u;
		}
	
		return vertices.get(id);
	}
	
	private void updatEdgeList(Vertex u, Vertex v) {
		Edge e = new Edge(u, v);
		eList.add(e);
	}

	private void updateWeightedEdgeList(Vertex u, Vertex v, Integer w) {
		Edge e = new Edge(u, v, w);
		eList.add(e);

		// fast access to edge given from and to vertices
		HashMap<Vertex, Edge> toEdge = null;
		if (!fastFromToEdge.containsKey(u)) {
			toEdge = new HashMap<Vertex, Edge>();
			fastFromToEdge.put(u, toEdge);
		} else
			toEdge = fastFromToEdge.get(u);
		
		toEdge.put(v, e);
	}

	public Integer getWeight(Vertex from, Vertex to) {
		return fastFromToEdge.get(from).get(to).w;
	}
	
	private void updateAdjList(Vertex u, Vertex v) {
		List<Vertex> listU = null;
		if (!adjList.containsKey(u)) {
			listU = new ArrayList<Vertex>();
			adjList.put(u, listU);
		} else
			listU = adjList.get(u);

		listU.add(v);
	}
}
