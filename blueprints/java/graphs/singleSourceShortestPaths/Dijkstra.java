package graphs.singleSourceShortestPaths;

import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;

import graphs.DiGraph;
import graphs.Vertex;

public class Dijkstra {

	public static HashMap<Vertex, Vertex> dijkstra(DiGraph G, Vertex start) {
		HashMap<Vertex, Vertex> pred = new HashMap<Vertex, Vertex>();
		HashMap<Vertex, Integer> dist = new HashMap<Vertex, Integer>();
		Comparator<Vertex> comp = new Comparator<Vertex>() {
			@Override
			public int compare(Vertex u, Vertex v) {
				Integer distU = dist.get(u);
				Integer distV = dist.get(v);
				return distU.compareTo(distV);
			}
		};
		PriorityQueue<Vertex> Q = new PriorityQueue<Vertex>(comp);
		
		for (Integer id: G.vertices.keySet()) {
			Vertex v = G.vertices.get(id);
			pred.put(v, v);
			dist.put(v, Integer.MAX_VALUE);
			if (v == start)
				dist.put(v, 0);
			Q.offer(v);
		}
		
		while (!Q.isEmpty()) {
			Vertex u = Q.poll();

			if (G.adjList.get(u) == null)
				continue;
			
			for (Vertex v: G.adjList.get(u)) {
				Integer distFrom = dist.get(u);
				if (distFrom == Integer.MAX_VALUE)
					continue;
				Integer distTo = dist.get(v);
				
				Integer w = G.getWeight(u, v);
				if (distTo > distFrom + w) {
					dist.put(v, distFrom + w);
					pred.put(v, u);
					
					// pity that PriorityQueue doesn't have decreaseKey
					// so, remove+offer acts like a decrease key 2.log(n) instead log(n)
					Q.remove(v);
					Q.offer(v);
				}
			}
		}
		return pred;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[][] g = new Integer[][]{
			{1, 2, 3},
			{1, 4, 2},
			{2, 3, 1},
			{3, 5, 1},
			{3, 6, 2},
			{4, 2, 2},
			{4, 3, 1},
			{5, 4, 1},
			{5, 6, 1}};
		DiGraph G = new DiGraph(g);
		Vertex start = G.vertices.get(1);
		
		HashMap<Vertex, Vertex> pred = dijkstra(G, start);

		System.out.println("Single-Source-Shortest-Path by Dijkstra:");
		for (Vertex u : pred.keySet()) {
			Vertex p = pred.get(u);
			System.out.println("[" + p.id + " -> " + u.id + "]");
		}
		System.out.println();
	}
}
