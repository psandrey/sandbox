package graphs.singleSourceShortestPaths;

import java.util.HashMap;

import graphs.DiGraph;
import graphs.Edge;
import graphs.Vertex;

public class BellmanFord {
	
	public static HashMap<Vertex, Vertex> bellmanFord(DiGraph G, Vertex start) {
		HashMap<Vertex, Vertex> pred = new HashMap<Vertex, Vertex>();
		HashMap<Vertex, Integer> dist = new HashMap<Vertex, Integer>();

		for (Integer id: G.vertices.keySet()) {
			Vertex v = G.vertices.get(id);
			dist.put(v, Integer.MAX_VALUE);
			if (v == start)
				dist.put(v, 0);
			pred.put(v, v);
		}
		
		int n = G.vertices.size();
		while (n-- > 0) {
			for (Edge e: G.eList) {
				Integer distFrom = dist.get(e.from);
				if (distFrom == Integer.MAX_VALUE)
					continue;
				Integer distTo = dist.get(e.to);

				if (distTo > distFrom + e.w) {
					//System.out.println("Releax:(" + e.from.id + "," + e.to.id + "):" + distTo + "->" + (distFrom + e.w));
					dist.put(e.to, distFrom + e.w);
					pred.put(e.to, e.from);
				}
			}
		}

		// if at least an edge can be relaxed, then it means we have at least
		//  one negative cycle
		boolean hasNegativeCycle = false;
		for (Edge e: G.eList) {
			if (dist.get(e.to) >  dist.get(e.from) + e.w) {
				hasNegativeCycle = true;
				break;
			}
		}
		
		if (hasNegativeCycle)
			System.out.println("Graph has negative cycles!!!");

		return pred;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[][] g = new Integer[][]{
			{1, 2, 3},
			{1, 3, -2},
			{2, 4, 1},
			{3, 2, 2},
			{3, 4, 1},
			{4, 5, -1},
			{4, 6, 2},
			{5, 3, 1},
			{5, 6, 1}};
		DiGraph G = new DiGraph(g);
		Vertex start = G.vertices.get(1);
		
		HashMap<Vertex, Vertex> pred = bellmanFord(G, start);

		System.out.println("Single-Source-Shortest-Path by Bellman-Ford:");
		for (Vertex u : pred.keySet()) {
			Vertex p = pred.get(u);
			System.out.println("[" + p.id + " -> " + u.id + "]");
		}
		System.out.println();
	}
}
