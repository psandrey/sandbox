package graphs.singleSourceShortestPaths;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import graphs.DiGraph;
import graphs.Vertex;

public class UnWightedDiGraphSSSP {

	public static HashMap<Vertex, Vertex> diGraphSSSP(DiGraph G, Vertex start) {
		HashMap<Vertex, Vertex> pred = new HashMap<Vertex, Vertex>();
		HashMap<Vertex, Integer> dist = new HashMap<Vertex, Integer>();
		HashSet<Vertex> marked = new HashSet<Vertex>();
		for (Integer id: G.vertices.keySet()) {
			Vertex u = G.vertices.get(id);
			dist.put(u, Integer.MAX_VALUE);
			if (u == start)
				dist.put(u, 0);
		}
		LinkedList<Vertex> Q = new LinkedList<Vertex>();
		Q.offer(start);
		marked.add(start);
		
		while (!Q.isEmpty()) {
			Vertex u = Q.poll();

			// if there no out edge, then continue
			if (!G.adjList.containsKey(u))
				continue;

			// relax all out edges
			for (Vertex v: G.adjList.get(u)) {
				if (marked.contains(v))
					continue;

				Integer distFrom = dist.get(u);
				Integer distTo = dist.get(v);
				if (distFrom < Integer.MAX_VALUE && distTo > distFrom + 1) {
					dist.put(v, distFrom + 1);
					pred.put(v, u);
				}
				
				Q.add(v);
				marked.add(v);
			}
		}
		
		return pred;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[][] g = new Integer[][]{
			{1, 2},
			{1, 3},
			{2, 3},
			{3, 4}};
		DiGraph G = new DiGraph(g);
		Vertex start = G.vertices.get(1);
		
		HashMap<Vertex, Vertex> pred = diGraphSSSP(G, start);

		System.out.println("Single-Source-Shortest-Path by BFS:");
		for (Vertex u : pred.keySet()) {
			Vertex p = pred.get(u);
			System.out.println("[" + p.id + " -> " + u.id + "]");
		}
		System.out.println();
	}
}
