package graphs.traversals;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import graphs.UndirectedGraph;
import graphs.Vertex;

public class BreadthFirstSearch {

	private static LinkedHashSet<Vertex> BFS(UndirectedGraph G, Vertex start) {
		LinkedHashSet<Vertex> marked = new LinkedHashSet<Vertex>();
		if (G == null || G.adjList == null)
			return marked;
		
		Queue<Vertex> Q = new LinkedList<>();
		Q.add(start);
		marked.add(start);
		while (!Q.isEmpty()) {
			Vertex u = Q.poll();

			List<Vertex> uAdjList;
			if (!G.adjList.containsKey(u) || (uAdjList = G.adjList.get(u)) == null)
				continue;

			for (Vertex v: uAdjList) {
				if (marked.contains(v))
					continue;

				marked.add(v);
				Q.add(v);
			}
		}

		return marked;
	}
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[][] g = new Integer[][]{
			{1, 2},
			{1, 4},
			{2, 1},
			{2, 3},
			{2, 4},
			{3, 2},
			{3, 5},
			{4, 1},
			{4, 2},
			{4, 5},
			{5, 4},
			{5, 3}};
		UndirectedGraph G = new UndirectedGraph(g);
		Vertex start = G.vertices.get(2);
		
		LinkedHashSet<Vertex> pred = BFS(G, start);
		System.out.println("BFS version:");
		for (Vertex u : pred) {
			System.out.println("[" + u.id + "]");
		}
		System.out.println();	
	}
}
