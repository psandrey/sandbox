package graphs.traversals;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Stack;

import graphs.UndirectedGraph;
import graphs.Vertex;

public class DepthFirstSearch {

	// The recursive version
	public static LinkedHashSet<Vertex> DFS(UndirectedGraph G, Vertex start) {
		LinkedHashSet<Vertex> marked = new LinkedHashSet<Vertex>();
		if (G == null || G.adjList == null)
			return marked;
		
		DFSHelper(G, start, marked);
		return marked;
	}
	
	public static void DFSHelper(UndirectedGraph G, Vertex u, LinkedHashSet<Vertex> marked) {
		marked.add(u);

		List<Vertex> uAdjList;
		if (!G.adjList.containsKey(u) || (uAdjList = G.adjList.get(u)) == null)
			return;
		
		for (Vertex v: uAdjList) {
			if (marked.contains(v))
				continue;
			
			DFSHelper(G, v, marked);
		}
	}
	
	// The iterative version
	public static LinkedHashSet<Vertex> DFS_(UndirectedGraph G, Vertex start) {
		LinkedHashSet<Vertex> marked = new LinkedHashSet<Vertex>();
		if (G == null || G.adjList == null)
			return marked;
		
		Stack<Vertex> st = new Stack<Vertex>();
		st.add(start);
		while (!st.isEmpty()) {
			Vertex u = st.pop();
			if (marked.contains(u))
				continue;

			marked.add(u);
			
			List<Vertex> uAdjList;
			if (!G.adjList.containsKey(u) || (uAdjList = G.adjList.get(u)) == null)
				continue;
			
			for (Vertex v: uAdjList) {
				if (marked.contains(v))
					continue;
				
				st.add(v);
			}
		}
		
		return marked;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[][] g = new Integer[][]{
			{1, 2},
			{1, 4},
			{2, 1},
			{2, 3},
			{2, 4},
			{3, 2},
			{3, 5},
			{4, 1},
			{4, 2},
			{4, 5},
			{5, 4},
			{5, 3}};
		UndirectedGraph G = new UndirectedGraph(g);
		Vertex start = G.vertices.get(2);
		
		LinkedHashSet<Vertex> pred = DFS(G, start);
		System.out.println("DFS the recursive version:");
		for (Vertex u : pred) {
			System.out.println("[" + u.id + "]");
		}
		System.out.println();

		pred = DFS_(G, start);
		System.out.println("DFS the iterative version:");
		for (Vertex u : pred) {
			System.out.println("[" + u.id + "]");
		}
		System.out.println();

	}
}
