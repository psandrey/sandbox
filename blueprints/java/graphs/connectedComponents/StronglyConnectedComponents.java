package graphs.connectedComponents;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Stack;

import graphs.DiGraph;
import graphs.Vertex;

public class StronglyConnectedComponents {

	public static LinkedHashMap<Vertex, Integer> SCC(DiGraph G) {
		LinkedHashMap<Vertex, Integer> cc = new LinkedHashMap<Vertex, Integer>();
		G.buildReversedGraph();

		// Stack vertices in order of completion time
		HashSet<Vertex> marked = new HashSet<Vertex>();
		Stack<Vertex> st = new Stack<Vertex>();
		for (Integer id: G.vertices.keySet()) {
			Vertex v = G.vertices.get(id);
			if (!marked.contains(v))
				stackOrderFinishTime(G, marked, v, st);
		}

		// discover all SSC
		int ccId = 0;
		marked = new HashSet<Vertex>();
		while (!st.isEmpty()) {
			Vertex u = st.pop();
			if (marked.contains(u))
				continue;
			
			discoverSCC(G, u, marked, ccId, cc);
			ccId++;
		}

		return cc;
	}
	
	// Similar with topological-sort
	private static void stackOrderFinishTime(DiGraph G, HashSet<Vertex> marked, Vertex u, Stack<Vertex> st) {
		marked.add(u);

		List<Vertex> uRAdjList;
		if (G.rAdjList.containsKey(u) && (uRAdjList = G.rAdjList.get(u)) != null) {		
			for (Vertex v: uRAdjList) {
				if (marked.contains(v))
					continue;
				
				stackOrderFinishTime(G, marked, v, st);
			}
		}
		
		st.add(u);
	}
	
	// discover all SCC
	private static void discoverSCC(DiGraph G, Vertex u, HashSet<Vertex> marked,
			int ccId, LinkedHashMap<Vertex, Integer> cc) {
		marked.add(u);
		cc.put(u, ccId);

		List<Vertex> uAdjList;
		if (!G.adjList.containsKey(u) || (uAdjList = G.adjList.get(u)) == null)
			return;
		
		for (Vertex v: uAdjList) {
			if (marked.contains(v))
				continue;
			
			discoverSCC(G, v, marked, ccId, cc);
		}		
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[][] g = new Integer[][]{
			{1, 2},
			{2, 3},
			{3, 1},

			{2, 4},
			{4, 5},
			{5, 4},

			{6, 7},
			{7, 8},
			{8, 6}};
		DiGraph G = new DiGraph(g);
		LinkedHashMap<Vertex, Integer>cc = SCC(G);
		
		System.out.println("Strongly connected components:");
		for (Vertex u : cc.keySet()) {
			int ccId = cc.get(u);
			System.out.println("[" + u.id + " : " + ccId + "]");
		}
		System.out.println();
	}
}
