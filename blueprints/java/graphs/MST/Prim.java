package graphs.MST;

import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;

import graphs.UndirectedGraph;
import graphs.Vertex;

public class Prim {

	public static HashMap<Vertex, Vertex> prim(UndirectedGraph G, Vertex start) {
		HashMap<Vertex, Integer> kW = new HashMap<Vertex, Integer>();
		Comparator<Vertex> comp = new Comparator<Vertex>() {
			@Override
			public int compare(Vertex u, Vertex v) {
				Integer kU = kW.get(u);
				Integer kV = kW.get(v);
				return kU.compareTo(kV);
			}
		};
		PriorityQueue<Vertex> Q = new PriorityQueue<Vertex>(comp);
		HashMap<Vertex, Boolean> S = new HashMap<Vertex, Boolean>();

		// initialize data
		HashMap<Vertex, Vertex> pred = new HashMap<Vertex, Vertex>();
		for (Integer label : G.vertices.keySet()) {
			Vertex v = G.vertices.get(label);
			S.put(v, false);
			pred.put(v, v);
			kW.put(v, Integer.MAX_VALUE);

			if (v == start)
				kW.put(v, 0);
			
			Q.offer(v);
		}
		
		// build MST by Prim
		while (!Q.isEmpty()) {
			Vertex u = Q.poll();
			S.put(u, true);

			for (Vertex v: G.adjList.get(u)) {
				Integer wUV = G.getWeight(u, v);

				if (S.get(v) == false && kW.get(v) > wUV) {
					kW.put(v, wUV);
					pred.put(v, u);
					
					// pity that PriorityQueue doesn't have decreaseKey
					// so, remove+offer acts like a decrease key 2.log(n) instead log(n)
					Q.remove(v);
					Q.offer(v);
				}
			}
		}
		
		return pred;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[][] g = new Integer[][]{
			{1, 2, 3},
			{1, 4, 2},
			{2, 4, 2},
			{2, 3, 1},
			{3, 4, 1},
			{3, 5, 3},
			{3, 6, 2},
			{4, 5, 1},
			{5, 6, 1}};
		UndirectedGraph G = new UndirectedGraph(g);
		Vertex start = G.vertices.get(1);

		HashMap<Vertex, Vertex> pred = prim(G, start);

		System.out.println("MST by Prim:");
		for (Vertex u : pred.keySet()) {
			Vertex p = pred.get(u);
			System.out.println("[" + p.id + " -> " + u.id + "]");
		}
		System.out.println();
	}
}
