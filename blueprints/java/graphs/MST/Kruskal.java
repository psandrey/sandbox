package graphs.MST;

import graphs.util.DisjointSet;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;

import graphs.UndirectedGraph;
import graphs.Vertex;
import graphs.Edge;

public class Kruskal {
	
	public static LinkedList<Edge> kruskal(UndirectedGraph G) {
		// init queue
		Comparator<Edge> comp = new Comparator<Edge>() {
			@Override
			public int compare(Edge e1, Edge e2) {
				return e1.w.compareTo(e2.w);
			}
		};
		PriorityQueue<Edge> Q = new PriorityQueue<Edge>(comp);
		for (Edge e: G.eList)
			Q.offer(e);
		
		// init disjoint-set
		DisjointSet<Vertex> ds = new DisjointSet<Vertex>();
		for (Integer label : G.vertices.keySet()) {
			Vertex u = G.vertices.get(label);
			ds.newSet(u);
		}
		
		// build MST by Kruskal
		LinkedList<Edge> mst = new LinkedList<Edge>();
		while (!Q.isEmpty()) {
			Edge e = Q.poll();
			if (ds.find(e.from) != ds.find(e.to)) {
				ds.union(e.from, e.to);
				mst.add(e);
			}
		}
		
		return mst;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[][] g = new Integer[][]{
			{1, 2, 3},
			{1, 4, 2},
			{2, 4, 2},
			{2, 3, 1},
			{3, 4, 1},
			{3, 5, 3},
			{3, 6, 2},
			{4, 5, 1},
			{5, 6, 1}};
		UndirectedGraph G = new UndirectedGraph(g);
		
		LinkedList<Edge> mst = kruskal(G);

		System.out.println("Mst by Kruskal:");
		for (Edge e: mst)
			System.out.println("[" + e.from.id + " -> " + e.to.id + "]");
		System.out.println();
	}
}
