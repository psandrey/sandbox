package graphs.other;

/*
 * Hierholzer's algorithm  to find Eulerian path.
 * Note: The graph must have a source and a sink node and all the other nodes must have the
 *       in-degree = out-degree, otherwise the graph doesn't have an Eulerian path.
 */

import java.util.LinkedList;
import java.util.List;

import graphs.DiGraph;
import graphs.Vertex;

public class EulerianPath {

	public static List<Vertex> eulerianPath(DiGraph G, Vertex start) {
		List<Vertex> ePath = new LinkedList<Vertex>();
		
		eulerianPath(G, start, ePath);
		return ePath;
	}

	public static void eulerianPath(DiGraph G, Vertex u, List<Vertex> ePath) {
		List<Vertex> adjList;
		if (G.adjList.containsKey(u) && (adjList = G.adjList.get(u)) != null) {			
			while (!adjList.isEmpty()) {
				Vertex v = adjList.remove(0);
				eulerianPath(G, v, ePath);
			}
		}
		ePath.add(0, u);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[][] g = new Integer[][]{
			{0, 1},
			{1, 2},
			{1, 3},
			{2, 1},
			{3, 4}};
		DiGraph G = new DiGraph(g);
		Vertex start = G.vertices.get(0);
		
		List<Vertex> ePath = eulerianPath(G, start);
		
		System.out.println("Eulerian path:");
		for (Vertex u : ePath) {
			System.out.println("[" + u.id + "]");
		}
		System.out.println();
	}
}
