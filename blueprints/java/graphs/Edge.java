package graphs;

import graphs.Vertex;

public class Edge {
	public Vertex from;
	public Vertex to;
	public Integer w;

	public Edge(Vertex from, Vertex to) {
		this.from = from;
		this.to = to;
	}
	public Edge(Vertex from, Vertex to, Integer w) {
		this.from = from;
		this.to = to;
		this.w = w;
	}
}
