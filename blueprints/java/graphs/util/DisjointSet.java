package graphs.util;

import java.util.Hashtable;

public class DisjointSet<E extends Comparable<E>> {
	public Hashtable<E, E> parent;
	public Hashtable<E, Integer> rank;

	public DisjointSet() {
		parent = new Hashtable<E, E>();
		rank = new Hashtable<E, Integer>();
	}
	
	public void newSet(E u) {
		parent.put(u, u);
		rank.put(u, 0);
	}
	
	public E find(E v) {
		if (parent.containsKey(v) == false)
			return null;

		E pV = parent.get(v);
		if (v.compareTo(pV) != 0) {
			E p = find(pV);
			parent.put(v, p);
		}

		return parent.get(v);
	}

	public void union(E u, E v) {
		E uId = find(u);
		E vId = find(v);
		Integer rU = rank.get(uId);
		Integer rV = rank.get(vId);
		
		if (rU > rV)
			parent.put(vId, uId);
		else {
			parent.put(uId, vId);
			if (rU == rV)
				rank.put(vId, rU + 1);
		}
	}
}