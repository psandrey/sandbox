package sort.sortByDistribution;

/*
 * The Bucket-sort:
 * 
 * Time complexity:
 *     - worst case:   T(n) = O(n^2)
 *     - average case: T(n) = O(n)
 */

import java.util.LinkedList;

public class BucketSort {

	private void sort(int[] a) {
		if (a == null || a.length < 2)
			return;
		
		int max = getMax(a);
		sort(a, max);
	}

	private void sort(int[] a, int m) {
		int n = a.length;
		@SuppressWarnings("unchecked") LinkedList<Integer>[] bucket = new LinkedList[n];
		for (int i = 0; i < n; i++)
			bucket[i] = new LinkedList<Integer>();
		
		// put each item in its designated bucket
		for (int i = 0; i < n; i++) {
			int b = (a[i]*(n-1))/m;
			bucket[b].add(a[i]);
		}
		
		// sort each bucket with insertion-sort, then
		//  concatenate.
		int idx = 0;
		for (int i = 0; i < n; i++) {
			sort(bucket[i]);
			for(int j = 0; j < bucket[i].size(); j++)
				a[idx++] = bucket[i].get(j);
		}			
	}
	
	private void sort(LinkedList<Integer> a) {
		if (a == null || a.size() < 2)
			return;
		
		int n = a.size();
		for (int i = 1; i < n; i++) {
			int key = a.get(i);
			int j = i;
			while (j > 0 && key < a.get(j-1)) {
				a.set(j, a.get(j-1));
				j--;
			}
			a.set(j, key);
		}
	}
	private int getMax(int[] a) {
		int max = Integer.MIN_VALUE;
		for(int i = 0; i < a.length; i++)
			max = Math.max(max, a[i]);
		return max;
	}
	
	private void printArray(int[] a) {
		int n = a.length;
		System.out.print("[ ");
		for (int i = 0; i < n; i++)
			System.out.print(a[i]+",");
		System.out.print(" ]");
		System.out.println();
	}
	
	// The MAIN ---------------------------------------------------------------------------------
	public static void main (String args[]) {
		int[] a = {9, 3, 8, 6, 1, 2};
		BucketSort t = new BucketSort();
		t.sort(a);
		t.printArray(a);
	}
}
