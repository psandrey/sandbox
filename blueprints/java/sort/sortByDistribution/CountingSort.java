package sort.sortByDistribution;

/*
 * The Counting-sort:
 * 
 * Time complexity: worst/average case: T(n) = O(n+k)
 */

public class CountingSort {
	
	private void sort(int[] a, int k) {
		int[] counter = new int[k+1];
		for (int i = 0; i < a.length; i++)
			counter[a[i]]++;
		
		int j = 0;
		for (int i = 0; i <= k; i++) {
			while (counter[i] > 0) {
				a[j++] = i;
				counter[i]--;
			}
		}
	}

	private void printArray(int[] a) {
		int n = a.length;
		System.out.print("[ ");
		for (int i = 0; i < n; i++)
			System.out.print(a[i]+",");
		System.out.print(" ]");
		System.out.println();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = {0, 3, 1, 4, 2};
		CountingSort t = new CountingSort();
		t.sort(a, 4);
		t.printArray(a);
	}
}
