package sort.sortByDistribution;

/*
 * The Radix-sort:
 * 
 * Time complexity: worst/average case: T(n) = O(d.(n+k))
 */

public class RadixSort {

	private void sort(int[] a) {
		if (a == null || a.length < 2)
			return;
		
		int max = getMax(a);
		int maxDigit = getMaxDigitBase10(max);
		int exp = 1; 
		for (int digit = 0; digit < maxDigit; digit++) {
			sort(a, exp);
			exp *= 10;
		}
	}
	
	private int getMax(int[] a) {
		int max = Integer.MIN_VALUE;
		for(int i = 0; i < a.length; i++)
			max = Math.max(max, a[i]);
		return max;
	}
	
	private int getMaxDigitBase10(int x) {
		int base = 10;
		int digits = 1;
		while (x/base != 0) {
			x = x/base;
			digits++;
		}

		return digits;
	}
	
	// sort array with respect to exp's digit
	// e.g. exp=10 (means digit 2) => 345/10 = 34%10 = 4
	private void sort(int[] a, int exp) {
		int n = a.length;
		int[] bucket = new int[10];
		
		// count how many items with every digit we have
		for (int i = n-1; i >=0; i--) {
			int digit = (a[i]/exp)%10;
			bucket[digit]++;
		}
		
		// offset current bucket with all previous positions
		// to make enough room all previous items 
		for (int i = 1; i < 10; i++) {
			bucket[i] += bucket[i-1];
		}

		// place each item in sorted fashion with respect to exp's digit
		int[] output = new int[n];
		for (int i = n - 1; i >=0; i--) {
			int digit = (a[i]/exp)%10;
			output[bucket[digit] - 1] = a[i];
			bucket[digit]--;
		}
		
		// copy sorted array back into the original input, so next time
		// it will be sorted with respect to next digit
		System.arraycopy(output, 0, a, 0, n);
	}
	
	private void printArray(int[] a) {
		int n = a.length;
		System.out.print("[ ");
		for (int i = 0; i < n; i++)
			System.out.print(a[i]+",");
		System.out.print(" ]");
		System.out.println();
	}
	
	// The MAIN ---------------------------------------------------------------------------------
	public static void main (String args[]) {
		int[] a = {345, 95, 71, 102};
		RadixSort t = new RadixSort();
		t.sort(a);
		t.printArray(a);
	}
}

