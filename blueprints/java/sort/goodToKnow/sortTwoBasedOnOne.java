package sort.goodToKnow;

import java.util.Arrays;
import java.util.Comparator;

public class sortTwoBasedOnOne {
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] x = {4, 3, 1, 2};
		int[] y = {1, 2, 4, 3};
		
		Integer[] idxs = new Integer[x.length];
		for(int i = 0; i < x.length; i++) idxs[i] = i;
		
		Comparator<Integer> comp = new Comparator<Integer>() {
			public int compare(Integer i, Integer j) {
				return x[i] - x[j];
			}
		};
		Arrays.sort(idxs, comp);
		
		for (int i = 0; i < x.length; i++)
			System.out.println(x[i] + " " + idxs[i] + " (" + x[idxs[i]] + "," + y[idxs[i]] + ")");
	}
}
