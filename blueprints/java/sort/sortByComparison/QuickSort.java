package sort.sortByComparison;

/*
 * The Quick-sort:
 * 
 * Time complexity:
 *     - worst case:   T(n) = O(n^2)
 *     - average case: T(n) = O(n.log(n))
 *     
 * Note: Quick-sort does not require extra space, so is faster than Merge-sort.
 */

public class QuickSort {

	private void sort(int[] a) {
		if ( a == null || a.length <2)
			return;
		
		sort(a, 0, a.length - 1);
	}
	
	private void sort(int[] a, int s, int e) {
		if ( s >= e)
			return;
		
		int pivot = partition(a, s, e);
		sort(a, s, pivot - 1);
		sort(a, pivot + 1, e);
	}
	
	private int partition(int[] a, int s, int e) {
		int j = s - 1;
		for (int i = s; i < e ; i++) {
			if (a[i] < a[e]) {
				j++;
				int t = a[j];
				a[j] = a[i];
				a[i] = t;
			}
		}
		
		int t = a[j+1];
		a[j+1] = a[e];
		a[e] = t;
		return j + 1;
	}
	
	private void printArray(int[] a) {
		int n = a.length;
		System.out.print("[ ");
		for (int i = 0; i < n; i++)
			System.out.print(a[i]+",");
		System.out.print(" ]");
		System.out.println();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = {0, 3, 1, 4, 2};
		QuickSort t = new QuickSort();
		t.sort(a);
		t.printArray(a);
	}
}
