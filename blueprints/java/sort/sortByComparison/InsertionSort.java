package sort.sortByComparison;

/*
 * The Insertion sort:
 * 
 * Time complexity: average/worst case: T(n) = O(n^2)
 * Space complexity: O(1)
 */

public class InsertionSort {
	
	private void sort(int[] a) {
		if (a == null || a.length < 2)
			return;
		
		int n = a.length;
		
		for (int i = 1; i < n; i++) {
			int key = a[i];
			int j = i;
			while (j > 0 && key < a[j-1]) {
				a[j] = a[j-1];
				j--;
			}
			a[j] = key;
		}
	}
	
	private void printArray(int[] a) {
		int n = a.length;
		System.out.print("[ ");
		for (int i = 0; i < n; i++)
			System.out.print(a[i]+",");
		System.out.print(" ]");
		System.out.println();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = {4, 3, 2, 1, 0};
		InsertionSort t = new InsertionSort();
		t.sort(a);
		t.printArray(a);
	}

}
