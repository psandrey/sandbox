package sort.sortByComparison;

/*
 * The Merge-sort:
 * 
 * Time complexity: average/worst case: T(n) = O(n.log(n))
 * 
 * Note: Merge-sort requires O(n) extra space, so is slower than quicksort.
 */

import java.util.Arrays;

public class MergeSort {
	
	private void sort(int[] a) {
		if ( a == null || a.length <2)
			return;

		sort(a, 0, a.length - 1);
	}
	
	private void sort(int[] a, int s, int e) {
		if (s >= e)
			return;
		int mid = (s+e)/2;
		sort(a, s, mid);
		sort(a, mid+1, e);
		merge(a, s, mid, e);
	}
	
	private void merge(int[] a, int s, int mid, int e) {
		int[] L;
		int[] R;
		int lenL = mid - s + 1;
		int lenR = e - mid;
		L = Arrays.copyOfRange(a, s, mid + 1);
		R = Arrays.copyOfRange(a, mid + 1, e + 1);
		
		int i = 0;
		int j = 0;
		for (int k = s; k <= e; k++) {
			if (((j == lenR) && (i < lenL)) ||
				((i < lenL) && (j < lenR) && (L[i] < R[j])))
				a[k] = L[i++];
			else
				a[k] = R[j++];
		}
	}
	
	private void printArray(int[] a) {
		int n = a.length;
		System.out.print("[ ");
		for (int i = 0; i < n; i++)
			System.out.print(a[i]+",");
		System.out.print(" ]");
		System.out.println();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = {4, 3, 2, 1, 0};
		MergeSort t = new MergeSort();
		t.sort(a);
		t.printArray(a);
	}
}
