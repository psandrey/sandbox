package sort.sortByComparison;

/*
 * The Heap-sort:
 * 
 * Time complexity: average/worst case: T(n) = O(n.log(n))
 */

public class HeapSort {
	
	private void sort(int[] a) {
		if (a == null || a.length < 2)
			return;

		int heapLimit = a.length;
		buildMaxHeap(a);
		for (int i = heapLimit-1; i >= 0; i--) {
			swap(a, 0, i);
			heapLimit--;
			maxHeapifySink(a, heapLimit, 0);
		}
	}
	
	private void buildMaxHeap(int[] heap) {
		int mid = heap.length / 2;
		for (int i = mid; i >= 0; i--) {
			maxHeapifySink(heap, heap.length, i);
		}
	}
	
	private void maxHeapifySink(int[] heap, int heapLimit, int idx) {
		int l = left(idx);
		int r = right(idx);
		
		int largest = idx;
		if (l < heapLimit && heap[l] > heap[largest])
			largest = l;
	
		if (r < heapLimit && heap[r] > heap[largest])
			largest = r;

		if (largest == idx)
			return;
		
		swap(heap, idx, largest);
		maxHeapifySink(heap, heapLimit, largest);		
	}
	
	private int left(int idx) {
		return 2*idx + 1;
	}

	private int right(int idx) {
		return 2*idx + 2;
	}
	
	private void swap(int[] heap, int i, int j) {
		int t = heap[i];
		heap[i] = heap[j];
		heap[j] = t;
	}
	
	private void printArray(int[] a) {
		int n = a.length;
		System.out.print("[ ");
		for (int i = 0; i < n; i++)
			System.out.print(a[i]+",");
		System.out.print(" ]");
		System.out.println();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = {2, 1, 4, 3};
		HeapSort t = new HeapSort();
		t.sort(a);
		t.printArray(a);
	}
}
