
#include <thread>
#include <chrono>
#include <vector>
#include <iostream>
using namespace std;

#include "_Visual.h"

namespace ns_visual {
	class DFS {
	private:
		vector<vector<int>> d = { { 0, 1 }, { 0, -1 }, { 1, 0 }, { -1, 0 }};
	
		void _do_DFS(int i, int j, int n, int m, vector<vector<bool>>& marked) {
			if (i < 0 || i >= n) return;
			if (j < 0 || j >= m) return;
			if (marked[i][j] == true) return;

			marked[i][j] = true;
			_Visual::print(marked);

			for (int k = 0; k < (int)d.size(); k++)
				_do_DFS(i + d[k][0], j + d[k][1], n, m, marked);
		}

	public:
		void do_DFS(int n, int m, int si, int sj) {
			vector<vector<bool>> marked(n, vector<bool>(m, false));
			_do_DFS(si, sj, n, m, marked);
		}
	};
};

#if 0
using namespace ns_visual;
int main(void) {
	int n = 11, m = 11;
	//int si = n / 2, sj = m / 2;
	int si = 0, sj = 0;

	DFS t;
	t.do_DFS(n, m, si, sj);

	return 0;
}
#endif