
/*
 * Single Source Shortest Paths: Dijkstra's algorithm.
 *
 * Note: Only for didactic purpose.
 *
 * Complexity: T(V,E) = O((V+E)*log(E)), the log(E) is not log(V) because STL does not offer interface
 *             to change the priority of an object.
 *             Alternatively we can obtain T(V,E) = O(V^2) if we use an unsroted list as priority-queue;
 *              this is better for dense graphs.
 *
 * CLRS: T(V,E) = O((V+E)*log(V)).
 */

#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class SSSP_Dijkstra {
public:
	pair< vector<int>, vector<int>> sssp(int n, vector< vector<pair<int, int>> >& adjL, int s) {

		// define min priority queue
		struct compare {
			bool operator()(const pair<int, int>& a, const pair<int, int>& b) {
				return (a.second > b.second);
			}
		};
		priority_queue<pair<int, int>, vector<pair<int, int>>, compare> pq;

		int min_cost = 0;
		vector<int> pred(n), dist(n, INT_MAX);
		for (int i = 0; i < n; i++) pred[i] = i;

		dist[s] = 0;
		pq.push(make_pair(s, dist[s]));
		vector<bool> marked(n, false);
		while (!pq.empty()) {
			pair<int, int> p = pq.top(); pq.pop();
			int u = p.first;

			if (marked[u] == true) continue;
			marked[u] = true;

			if (!adjL[u].empty()) {
				for (int j = 0; j < (int)adjL[u].size(); j++) {
					pair<int, int> entry = adjL[u][j];
					int v = entry.first;
					int w = entry.second;

					if (marked[v] == false && dist[v] > dist[u] + w) {
						dist[v] = dist[u] + w;
						pred[v] = u;
						
						pq.push(make_pair(v, dist[v]));
					}
				}
			}
		}

		return make_pair(dist, pred);
	}

	// build adjacent list
	vector< vector<pair<int, int>> > build_graph(vector<vector<int>>& edges, int n) {
		vector< vector<pair<int, int>> >adjL(n, vector<pair<int, int>>());
		for (int i = 0; i < (int)edges.size(); i++) {
			int u = edges[i][0];
			int v = edges[i][1];
			int w = edges[i][2];
			pair<int, int> e;

			// u -> v
			e = make_pair(v, w);
			adjL[u].push_back(e);
		}

		return adjL;
	}
};

#if 0
int main(void) {
	//                             u, v, w
	vector<vector<int>> edges = { { 0, 1, 3 }, { 0, 3, 2 }, { 1, 2, 1 }, { 2, 4, 1 }, {2, 5, 2},
								  { 3, 1, 2 }, { 3, 2, 1 }, {4, 3, 1}, { 4, 5, 1 } };
	int n = 6;

	SSSP_Dijkstra t;
	vector< vector<pair<int, int>> > adjL = t.build_graph(edges, n);
	pair<vector<int>, vector<int>> ans = t.sssp(n, adjL, 0);

	vector<int> dist = ans.first;
	vector<int> pred = ans.second;

	cout << "SSSP edges: ";
	for (int i = 0; i < (int)pred.size(); i++) {
		if (pred[i] == i) continue;

		cout << "(" << pred[i] << "," << i << "): " << dist[i] << ", ";
	}
	cout << endl;

	return 0;
}
#endif