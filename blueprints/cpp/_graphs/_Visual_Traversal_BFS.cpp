
#include <thread>
#include <chrono>
#include <queue>
#include <vector>
#include <iostream>
using namespace std;

#include "_Visual.h"

namespace ns_visual {
	class BFS {
	private:
		//vector<vector<int>> d = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0,  1 },
		//						  { -1, -1}, { 1, 1 }, { -1, 1 }, { 1, -1} };
		vector<vector<int>> d = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0,  1 } };

	public:
		void do_BFS(int n, int m, int si, int sj) {
			vector<vector<bool>> marked(n, vector<bool>(m, false));
			queue<pair<int, int>> q;

			q.push(make_pair(si, sj));
			marked[si][sj] = true;
			while (!q.empty()) {
				pair<int, int> pp = q.front(); q.pop();
				int i = pp.first, j = pp.second;

				for (int k = 0; k < (int)d.size(); k++) {
					int new_i = i + d[k][0];
					int new_j = j + d[k][1];
					if (new_i < 0 || new_i >= n) continue;
					if (new_j < 0 || new_j >= m) continue;

					if (marked[new_i][new_j] == false) {
						q.push(make_pair(new_i, new_j));
						marked[new_i][new_j] = true;

						_Visual::print(marked);
					}
				}
			}
		}
	};
};

#if 0
using namespace ns_visual;
int main(void) {
	int n = 25, m = 25;
	int si = n / 2, sj = m / 2;

	BFS t;
	t.do_BFS(n, m, si, sj);

	return 0;
}
#endif