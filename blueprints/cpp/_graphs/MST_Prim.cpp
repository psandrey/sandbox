
/*
 * Minimum spanning tree: Prim-Jarnik algorithm.
 * 
 * Note: Only for didactic purpose.
 *
 * Complexity v1: T(V,E) = O((V+E)*log(E)), the log(E) is because STL does not offer interface
 *                to change the priority of an object, thus we insert all cross edges that connects
 *                every vertex to MST and we let the priority queue to give us the lightest.
 *
 * Complexity v2: T(V,E) = O(V^2+E) alternatively, we can use unsorted list/vector
 *                to get the lightest corss edge - this is better for dense graphs. 
 *
 * Complexity v3: T(V,E) = O((V+E)*log(V)) it adaptable priority queue is used (i.e. the pq has
 *                an interface to change the priority of an object - as an example see
 *                min_pq_adaptable from _utils)
 */

#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class MST_Prim {
public:
	// T(V,E) = O((V+E)*log(E))
	pair<int, vector<int>> mst_v1(int n, vector< vector<pair<int, int>> >& adjL, int s) {
		// define min priority queue
		struct compare {
			bool operator()(const pair<int, int>& a, const pair<int, int>& b) {
				return (a.second > b.second);
			}
		};
		priority_queue<pair<int, int>, vector<pair<int, int>>, compare> pq;

		int min_cost = 0;
		vector<int> pred(n), key(n);
		for (int i = 0; i < n; i++) { key[i] = INT_MAX; pred[i] = i; }

		key[s] = 0;
		pq.push(make_pair(s, key[s]));
		vector<bool> mst_set(n, false);
		while (!pq.empty()) {
			pair<int, int> p = pq.top(); pq.pop();
			// dequeue the endpoint of the lightest edge and if it is not yet included in the MST,
			//  it means that this is the lightest cross edge, so we must include in the MST.
			int u = p.first;
			if (mst_set[u] == true) continue;

			// greedy: take the lightest cross edge
			mst_set[u] = true;
			min_cost += key[u];
			
			// explore vertex 'u' and for all endpoints that are not in MST do:
			//   enqueue endpoints and relax all incident edges
			if (!adjL[u].empty()) {
				for (int j = 0; j < (int)adjL[u].size(); j++) {
					pair<int, int> entry = adjL[u][j];
					int v = entry.first;
					int w = entry.second;

					// we just found a lighter cross edge that connects v to MST thru u
					if (mst_set[v] == false && key[v] > w) {
						key[v] = w;
						pred[v] = u;

						// since we do not have an interface to decrease priority, we just
						//  have to insert this edge into priority queue and let the queue choose
						//  the lightest cross edge
						pq.push(entry);
					}
				}
			}
		}

		return make_pair(min_cost, pred);
	}

	// T(V,E) = O(V^2+E)
	pair<int, vector<int>> mst_v2(int n, vector< vector<pair<int, int>> >& adjL, int s) {
		int min_cost = 0;
		vector<int> pred(n), key(n);
		for (int i = 0; i < n; i++) { key[i] = INT_MAX; pred[i] = i; }

		key[s] = 0;
		vector<bool> mst_set(n, false);
		for (int i = 0; i < n; i++) { // include every vertex in MST
			int u = get_min(n, key, mst_set);
			mst_set[u] = true;
			min_cost += key[u];

			for (int j = 0; j < (int)adjL[u].size(); j++) {
				pair<int, int> entry = adjL[u][j];
				int v = entry.first;
				int w = entry.second;

				if (mst_set[v] == false && key[v] > w) {
					key[v] = w;
					pred[v] = u;
				}
			}
		}

		return make_pair(min_cost, pred);
	}

	int get_min(int n, vector<int>& key, vector<bool>& mst_set) {
		int min_key = INT_MAX, v = -1;
		for (int u = 0; u < n; u++) {
			if (mst_set[u] == false && min_key > key[u]) {
				min_key = key[u];
				v = u;
			}
		}

		return v;
	}

	// build adjacent list
	vector< vector<pair<int, int>> > build_graph(vector<vector<int>>& edges, int n) {
		vector< vector<pair<int, int>> >adjL(n, vector<pair<int, int>>());
		for (int i = 0; i < (int)edges.size(); i++) {
			int u = edges[i][0];
			int v = edges[i][1];
			int w = edges[i][2];
			pair<int, int> e;

			// u -> v
			e = make_pair(v, w);
			adjL[u].push_back(e);

			// v -> u
			e = make_pair(u, w);
			adjL[v].push_back(e);
		}

		return adjL;
	}
};

#if 0
int main(void) {
	//                             u, v, w
	vector<vector<int>> edges = {{ 0, 1, 3 }, { 0, 2, 2 }, { 1, 2, 2 }, { 1, 3, 1 }, { 2, 3, 1 },
								{ 2, 4, 1 }, { 3, 4, 3 }, { 3, 5, 2 }, { 4, 5, 1 }};
	int n = 6;
	MST_Prim t;

	vector< vector<pair<int, int>> > adjL = t.build_graph(edges, n);
	pair<int, vector<int>> ans1 = t.mst_v1(n, adjL, 0);

	int min_cost = ans1.first;
	vector<int> pred = ans1.second;

	cout << "Min weight: " << min_cost << endl;
	cout << "MST edges: ";
	for (int i = 0; i < (int)pred.size(); i++) {
		if (pred[i] == i) continue;

		cout << "(" << pred[i] << "," << i << "), ";
	}
	cout << endl;


	pair<int, vector<int>> ans2 = t.mst_v2(n, adjL, 0);

	min_cost = ans2.first;
	pred = ans2.second;

	cout << "Min weight: " << min_cost << endl;
	cout << "MST edges: ";
	for (int i = 0; i < (int)pred.size(); i++) {
		if (pred[i] == i) continue;

		cout << "(" << pred[i] << "," << i << "), ";
	}
	cout << endl;

	return 0;
}
#endif