
/*
 * Single Source Shortest Paths: Bellman-Ford algorithm.
 *
 * Note: Only for didactic purpose.
 *
 * Complexity: T(V,E) = O(VE).
 *
 * Note: if adjacency list is used, then T(V,E) = O((E+V)E).
 * Note: see Bellman-ford version from dynamic-programming section for memoized version.
 */

#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class SSSP_Bellman_Ford {
public:
	pair< vector<int>, vector<int>> sssp(int n, vector<vector<int>>& edges, int s) {
		vector<int> pred(n), dist(n, INT_MAX);
		for (int i = 0; i < n; i++) pred[i] = i;

		dist[s] = 0;
		for (int i = 1; i <= n - 1; i++) {
			for (int j = 0; j < (int)edges.size(); j++) {
				int u = edges[j][0];
				int v = edges[j][1];
				int w = edges[j][2];

				if (dist[u] != INT_MAX && dist[v] > dist[u] + w) {
					dist[v] = dist[u] + w;
					pred[v] = u;
				}
			}
		}

		return make_pair(dist, pred);
	}
};

#if 0
int main(void) {
	//                             u, v, w
	//vector<vector<int>> edges = { { 0, 1, 3 }, { 0, 3, 2 }, { 1, 2, 1 }, { 2, 4, 1 }, {2, 5, 2},
	//							  { 3, 1, 2 }, { 3, 2, 1 }, {4, 3, 1}, { 4, 5, 1 } };
	//int n = 6;

	vector<vector<int>> edges = { { 0, 1, 1 }, { 0, 2, 1 }, { 0, 3, 1 },
								  { 1, 2, -1 },
								  { 2, 3, -1 } };
	int n = 4;

	SSSP_Bellman_Ford t;
	pair<vector<int>, vector<int>> ans = t.sssp(n, edges, 0);

	vector<int> dist = ans.first;
	vector<int> pred = ans.second;

	cout << "SSSP edges: ";
	for (int i = 0; i < (int)pred.size(); i++) {
		if (pred[i] == i) continue;

		cout << "(" << pred[i] << "," << i << "): " << dist[i] << ", ";
	}
	cout << endl;

	return 0;
}
#endif