
/*
 * Minimum spanning tree: Kruskal's algorithm.
 *
 * Note: Only for didactic purpose.
 *
 * Complexity: T(V,E) = O(Elog(E))
 */

#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class MST_Kruscal {
public:

	pair<int, vector<vector<int>>> mst(int n, vector<vector<int>>& edges) {
		// sort edges in reverse order of their weight
		struct {
			bool operator()(const vector<int>& a, const vector<int>& b) {
				return (a[2] < b[2]); // 0:u, 1:v, 2:w (weight)
			}
		} compare;
		sort(edges.begin(), edges.end(), compare);

		// init disjoint sets
		vector<int> ds_r(n, 0);
		vector<int> ds_p(n); for (int i = 0; i < n; i++) ds_p[i] = i;

		// handle edges in reverse order of their weights
		int min_cost = 0;
		vector< vector<int>> mst_set;
		for (int i = 0; i < (int)edges.size(); i++) {
			vector<int> e = edges[i];
			int u = e[0];
			int v = e[1];
			int w = e[2];
	
			// edpoints of the lightest edge are from different sets (i.e. lightest cross edge),
			//  then add the edge to the MST.
			if (ds_find(u, ds_p) != ds_find(v, ds_p)) {
				min_cost += w;
				mst_set.push_back(e);
				ds_union(u, v, ds_r, ds_p); 
			}
		}

		return (make_pair(min_cost, mst_set));
	}

	// Disjoint sets with contraction: find and union
	int ds_find(int u, vector<int>& ds_p) {
		if (ds_p[u] != u) ds_p[u] = ds_find(ds_p[u], ds_p); 

		return ds_p[u];
	}

	void ds_union(int u, int v, vector<int>& ds_r, vector<int>& ds_p) {
		int pu = ds_find(u, ds_p);
		int pv = ds_find(v, ds_p);
		if (ds_r[pu] > ds_r[pv]) ds_p[pv] = pu;
		else if (ds_r[pu] < ds_r[pv]) ds_p[pu] = pv;
		else { ds_p[pv] = pu; ds_r[pu]++; }
	}
};

#if 0
int main(void) {
	//                              u, v, w
	vector<vector<int>> edges = { { 0, 1, 3 }, { 0, 2, 2 }, { 1, 2, 2 }, { 1, 3, 1 }, { 2, 3, 1 },
								{ 2, 4, 1 }, { 3, 4, 3 }, { 3, 5, 2 }, { 4, 5, 1 } };
	int n = 6;

	MST_Kruscal t;
	pair<int, vector< vector<int>>> ans = t.mst(n, edges);
	int min_cost = ans.first;
	vector< vector<int>> edges_mst = ans.second;

	cout << "Min weight: " << min_cost << endl;
	cout << "MST edges: ";
	for (int i = 0; i < (int)edges_mst.size(); i++) {
		int u = edges_mst[i][0];
		int v = edges_mst[i][1];
		int w = edges_mst[i][2];

		cout << "(" << u << "," << v << "):" << w << ",  ";
	}
	cout << endl;

	return 0;
}
#endif