
/*
 * All Pairs Shortest Paths: Floyd-Warshall algorithm.
 * Note: DP tabulation with path reconstruction, plus DP memoization (just weights).
 *
 * Note: Only for didactic purpose.
 *
 * Complexity: T(V,E) = O(V^3)
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class APSP_FloydWarshall {
public:
	// Floyd-Warshall DP: tabulation
	pair<vector<vector<int>>, vector<vector<int>>> FWi(int n, vector<vector<int>>& w) {
		vector<vector<int>> apsp(n, vector<int>(n, INT_MAX));
		vector<vector<int>> pred(n, vector<int>(n, -1));

		for (int u = 0; u < n; u++)
			for (int v = 0; v < n; v++) {
				if (u == v) { apsp[u][v] = 0; pred[u][v] = u; }
				else if (w[u][v] != -1) { apsp[u][v] = w[u][v]; pred[u][v] = u; }
			}

		// the path considering all sets ending with each vertex
		for (int k = 0; k < n; k++) {
			for (int u = 0; u < n; u++) {
				for (int v = 0; v < n; v++) {
					if (apsp[u][k] != INT_MAX && apsp[k][v] != INT_MAX
						&& apsp[u][v] > apsp[u][k] + apsp[k][v]) {

						apsp[u][v] = apsp[u][k] + apsp[k][v];
						pred[u][v] = k;
					}
				}
			}
		}

		//print_matrix(apsp);
		return make_pair(apsp, pred);
	}

	// Floyd-Warshall DP: memoization
	vector<vector<int>> FWr(int n, vector<vector<int>>& w) {
		vector<vector<vector<int>>> memo(n, vector<vector<int>>(n + 1, vector<int>(n, INT_MIN)));
		for (int u = 0; u < n; u++)
			for (int v = 0; v < n; v++)
				FWr(n, w, u, v, n, memo);
	
		return memo[n];
	}

	int FWr(int n, vector<vector<int>>& w, int u, int v, int k, vector<vector<vector<int>>>& memo) {
		if (memo[k][u][v] != INT_MIN) return memo[k][u][v];

		if (u == v) memo[k][u][v] = 0;
		else if (k == 0) {
			if (w[u][v] == -1)  memo[k][u][v] = INT_MAX;
			else  memo[k][u][v] = w[u][v];
		} else {
			int uv = FWr(n, w, u    , v    , k - 1, memo);
			int uk = FWr(n, w, u    , k - 1, k - 1, memo);
			int kv = FWr(n, w, k - 1, v    , k - 1, memo);

			if (uk == INT_MAX || kv == INT_MAX) memo[k][u][v] = uv;
			else memo[k][u][v] = min(uv, uk + kv);
		}

		return memo[k][u][v];
	}

	void print_path(int u, int v, vector<vector<int>>& pred) {
		cout << u << "-->" << v << ":  ";
		if (pred[u][v] == -1) {
			cout << "no path!" << endl;
			return;
		}

		// the path is built in reverse
		vector<int> path;
		while (v != u && v != -1) {
			path.push_back(v);
			v = pred[u][v];
		}
		path.push_back(v);

		// print
		for (int i = (int)path.size() - 1; i >= 0; i--)
			cout << path[i] << " ";
		cout << endl;
	}

	void print_all_paths(vector<vector<int>>& pred) {
		for (int u = 0; u < (int)pred.size(); u++) {
			for (int v = 0; v < (int)pred[0].size(); v++) {
				print_path(u, v, pred);
			}
		}
	}

	void print_matrix(vector<vector<int>>& a) {
		for (int u = 0; u < (int) a.size(); u++) {
			for (int v = 0; v < (int) a[0].size(); v++) {
				if (a[u][v] == INT_MAX) cout << "- ";
				else cout << a[u][v] << " ";
			}
			cout << endl;
		}
		cout << endl;
	}
};

#if 0
int main(void) {
	vector<vector<int>> w = {
						{ -1,  1, -1, 10 },
						{ -1, -1,  1,  4 },
						{  1,  3, -1,  1 },
						{ -1, -1, -1, -1 } };
	int n = 4;

	APSP_FloydWarshall t;


	// tabulation version with paths
	pair<vector<vector<int>>, vector<vector<int>>> ans = t.FWi(n, w);
	vector<vector<int>> apsp = ans.first;
	vector<vector<int>> pred = ans.second;
	cout << "All weights (iterative): " << endl;
	t.print_matrix(apsp);
	cout << "All paths (iterative):" << endl;
	t.print_all_paths(pred);

	// memoization version... just weights w/o path
	// Note: is easy to add path reconstruction
	apsp = t.FWr(n, w);
	cout << "All weights (recursive): " << endl;
	t.print_matrix(apsp);

	return 0;
}
#endif