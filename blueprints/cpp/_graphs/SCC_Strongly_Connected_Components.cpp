
/*
 * Strongly Connected Components: Naive and Kosarju's algorithm.
 *
 * Note: Only for didactic purpose.
 *
 * Complexity:
 *    - naive: T(V,E) = O( V^3.(E+V) )
 *    - Kosarju: T(V,E) = O(E + V)
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class SCC {
public:

	vector<int> scc_naive(int n, vector<vector<int>>& adjL) {
		vector<int> scc(n, 0);
		
		int cc = -1;
		for (int u = 0; u < n; u++) {
			if (scc[u] <= cc) continue;

			cc++;
			for (int i = u; i < n - 1; i++)
				for (int j = i + 1; j < n; j++)
					if (connected(n, adjL, i, j) == false || connected(n, adjL, j, i) == false)
						scc[j] = cc + 1;
		}

		return scc;
	}

	bool connected(int n, vector<vector<int>>& adjL, int u, int v) {
		vector<bool> marked(n, false);
		return DFS_connected(u, v, adjL, marked);
	}

	bool DFS_connected(int u, int end, vector<vector<int>>& adjL, vector<bool>& marked) {
		if (marked[u] == true) return false;
		if (u == end) return true;
		
		marked[u] = true;
		if (adjL[u].empty() == false) {
			for (int i = 0; i < (int)adjL[u].size(); i++) {
				bool ret = DFS_connected(adjL[u][i], end, adjL, marked);
				if (ret == true) return true;
			}
		}

		return false;
	}

	// Kosaraju's algorithm
	vector<int> scc_kosaraju(int n, vector<vector<int>>& adjL) {
		// 1. Construct the transposed graph
		vector<vector<int>> adjLT(n, vector<int>());
		transposed_graph(n, adjL, adjLT);
		
		// 2. Reversed Depth First Order on the transposed graph
		vector<int> rdfo;
		vector<bool> discovered(n, false);
		for (int u = 0; u < n; u++)
			if (discovered[u] == false)
				DFS_rdfo(u, adjLT, discovered, rdfo);
		
		// 3. Discover all strongly connected components
		vector<int> scc(n, -1);
		int cc = -1;
		// we need Depth First Order in the transposed graph, so from end to begin since we have rdfo
		for (int j = n - 1; j >= 0; j--) {
			int u = rdfo[j];
			if (scc[u] == -1) {
				cc++;
				DFS_scc(u, adjL, scc, cc);
			}
		}

		return scc;
	}

	void transposed_graph(int n, vector<vector<int>>& adjL, vector<vector<int>>& adjLT) {
		for (int u = 0; u < n; u++) {
			if (adjL[u].empty() == true) continue;
			for (int j = 0; j < (int)adjL[u].size(); j++) {
				int v = adjL[u][j];
				adjLT[v].push_back(u);
			}
		}
	}

	void DFS_rdfo(int u, vector<vector<int>>& adjLT, vector<bool>& discovered, vector<int>& rdfo) {
		if (discovered[u] == true) return;
		discovered[u] = true;

		if (adjLT[u].empty() == false) {
			for (int j = 0; j < (int)adjLT[u].size(); j++)
				DFS_rdfo(adjLT[u][j], adjLT, discovered, rdfo);
		}

		// this is reversed depth first order
		// - depth first order would be (pre-append: insert(dfo.begin(), u))
		rdfo.push_back(u);
	}

	void DFS_scc(int u, vector<vector<int>>& adjL, vector<int>& scc, int cc) {
		if (scc[u] != -1) return;
		scc[u] = cc;

		if (adjL[u].empty() == false) {
			for (int j = 0; j < (int)adjL[u].size(); j++)
				DFS_scc(adjL[u][j], adjL, scc, cc);
		}
	}

	// for debug purposes
	vector<vector<int>> build_graph(int n, vector<vector<int>>& edges) {
		vector<vector<int>> adjL(n, vector<int>());
		for (int i = 0; i < (int)edges.size(); i++) {
			int u = edges[i][0];
			int v = edges[i][1];
			adjL[u].push_back(v);
		}

		return adjL;
	}

	void dbg_print_scc(vector<int>& a) {
		cout << "[ ";
		for (int i = 0; i < (int)a.size(); i++)
			cout << "(" << i << " : " << a[i] << ") ";
		cout << " ]" << endl;
	}
};

#if 0
int main(void) {
	vector<vector<int>> edges = {
		{0, 2},
		{1, 0},
		{2, 1}, {2, 4},
		{4, 3},
		{3, 5},
		{5, 4},
	};
	int n = 6;

	SCC t;
	vector<vector<int>> adjL = t.build_graph(n, edges);
	vector<int> scc_n = t.scc_naive(n, adjL);
	vector<int> scc_k = t.scc_kosaraju(n, adjL);

	cout << "SCC from naive algorithm:" << endl;
	t.dbg_print_scc(scc_n);
	cout << endl;
	cout << "SCC from Kosarju's algorithm:" << endl;
	t.dbg_print_scc(scc_k);
	cout << endl;

	return 0;
}
#endif