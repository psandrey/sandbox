
/*
 * Build topological sort and detect cycles using DFS and BFS (Kahn's algorithm).
 * Note: DFS (recursive and interative).
 *
 * Note: Only for didactic purpose.
 */

#include <stack>
#include <queue>
#include <vector>
#include <iostream>
using namespace std;

#define DISCOVERED 1
#define EXPLORED 2

class DetectCycles_TopoSort_DiGraph {
public:
	bool hasCycles_TopoSort(int n, vector<vector<int>>& adjL, vector<int>& sort) {
#if 0
		// detect cycles in a digraph using DFS (recursive)
		//return hasCycles_TopoSort_DFSr(n, adjL, sort);

		// detect cycles in a digraph using DFS (iterative)
		return hasCycles_TopoSort_DFSi(n, adjL, sort);
#else
		// detect cycles in a digraph using Kahn's algorithm
		return hasCycles_TopoSort_Kahn(n, adjL, sort); // BFS
#endif
	}

	bool hasCycles_TopoSort_DFSr(int n, vector<vector<int>>& adjL, vector<int>& sort) {
		vector<int> marked(n, 0);
		for (int i = 0; i < n; i++) {
			if (marked[i] != EXPLORED && hasCycles_TopoSort_DFSr(i, adjL, marked, sort) == true)
				return true;
		}

		return false;
	}

	bool hasCycles_TopoSort_DFSr(int u, vector<vector<int>>& adjL, vector<int>& marked, vector<int>& sort) {
		if (marked[u] == DISCOVERED) return true;
		if (marked[u] == EXPLORED) return false;

		marked[u] = DISCOVERED;
		if (!adjL[u].empty()) {
			for (int i = 0; i < (int)adjL[u].size(); i++) {
				int v = adjL[u][i];

				if (hasCycles_TopoSort_DFSr(v, adjL, marked, sort) == true)
					return true;
			}
		}

		marked[u] = EXPLORED;
		sort.insert(sort.begin(), u); // reverse-post-order
		return false;
	}

	bool hasCycles_TopoSort_DFSi(int n, vector<vector<int>>& adjL, vector<int>& sort) {
		vector<int> marked(n, 0);
		vector<int> it(n, 0); // iterators for neighbors of each vertex

		for (int i = 0; i < n; i++) {
			if (marked[i] != EXPLORED && hasCycles_TopoSort_DFSi(i, adjL, marked, it, sort) == true)
				return true;
		}

		return false;
	}

	bool hasCycles_TopoSort_DFSi(int s, vector<vector<int>>& adjL, vector<int>& marked, vector<int>& it, vector<int>& sort) {
		stack<int> st;
		st.push(s); marked[s] = DISCOVERED;
		while (!st.empty()) {
			int u = st.top();
			if (!adjL.empty() && it[u] != adjL[u].size()) {
				int v = adjL[u][it[u]]; it[u]++;
		
				if (marked[v] == DISCOVERED) return true;
				if (marked[v] == EXPLORED) continue;

				st.push(v); marked[v] = true;
				marked[v] = DISCOVERED;
			} else {
				st.pop();
				marked[u] = EXPLORED;

				sort.insert(sort.begin(), u);
			}
		}

		return false;
	}

	bool hasCycles_TopoSort_Kahn(int n, vector<vector<int>>& adjL, vector<int>& sort) {
		// build indegree vector
		vector<int> indegree(n, 0);
		for (int i = 0; i < n; i++) {
			if (adjL[i].empty()) continue;
			
			for (int j = 0; j < (int)adjL[i].size(); j++)
				indegree[adjL[i][j]]++;
		}

		queue<int> q;
		for (int i = 0; i < n; i++)
			if (indegree[i] == 0) q.push(i);

		while (!q.empty()) {
			int u = q.front(); q.pop();
			sort.push_back(u);

			if (adjL[u].empty()) continue;

			for (int i = 0; i < (int) adjL[u].size(); i++) {
				int v = adjL[u][i];
				indegree[v]--;

				if (indegree[v] == 0) q.push(v);
			}
		}

		if (sort.size() != n) return true;
		return false;
	}

	// for debug purposes
	vector<vector<int>> build_graph(vector<vector<int>>& edges, int n) {
		vector<vector<int>> adjL(n, vector<int>());
		for (int i = 0; i < (int)edges.size(); i++) {
			int u = edges[i][0];
			int v = edges[i][1];
			adjL[u].push_back(v);
		}

		return adjL;
	}

	void dbg_print_vector(vector<int>& a) {
		cout << "[ ";
		for (auto x : a) cout << x << " ";
		cout << " ]" << endl;
	}
};

#if 0
int main(void) {
#if 1
	// no cycle
	vector<vector<int>> edges = { {0, 1}, {0, 2}, {1, 2}, {1, 3}, {2, 3}, {3, 4} };
	int n = 5;
#else
	// cycle
	vector<vector<int>> edges = { {0, 2}, {0, 1}, {1, 2}, {1, 3}, {2, 3}, {3, 0} };
	int n = 4;
#endif

	DetectCycles_TopoSort_DiGraph t;
	vector<vector<int>> adjL = t.build_graph(edges, n);
	
	vector<int> sort;
	bool has_cycle = t.hasCycles_TopoSort(n, adjL, sort);
	if (has_cycle == true) cout << " Has cycle/s" << endl;
	else t.dbg_print_vector(sort);

	return 0;
}
#endif