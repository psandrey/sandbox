
/*
 * Eulerian path/cycle for undirected graphs (Hierholzer's and Fleury's algorithm).
 *
 * Note: Only for didactic purpose.
 *
 * Complexity:
 *    - Hierholzer's: T(V,E) = O(E)
 *    - Fleury's:     T(v,E) = O(E.(V+E))
 */

#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class Eulerian_Trail_UndirectedGraph {
public:
	// Hierholzer's algorithm: iterative version
	vector<int> hierholzer_i(int n, vector<vector<int>>& adjL) {
		vector<int> e_path;
		if (adjL.empty()) return e_path;

		vector<vector<bool>> marked(n, vector<bool>(n, false)); // marking used edges(u,v)+(v,u)
		vector<int> it(n, 0); // iterator for each vertex
		stack<int> st;
		st.push(0);

#if 1 // version 1
		while (!st.empty()) {

			int u = st.top();
			if (!adjL[u].empty() && it[u] < (int)adjL[u].size()) {
				// find next unused incident edge
				int v = adjL[u][it[u]]; it[u]++;
				if (marked[u][v] == true) continue;
				marked[u][v] = true;
				marked[v][u] = true;
				st.push(v);
			} else {
				st.pop();

				// reverse-postorder: pre-append vertex with no leaving unused edge
				e_path.insert(e_path.begin(), u);
			}
		}
#endif

#if 0 // version 2
		while (!st.empty()) {

			// reverse postorder
			int u = st.top(); st.pop();
			while (!adjL[u].empty() && it[u] < (int)adjL[u].size()) {
				// find next unused incident edge
				int v = adjL[u][it[u]]; it[u]++;
				if (marked[u][v] == true) continue;
				marked[u][v] = true;
				marked[v][u] = true;
				
				// pushed vertex again since we don't know if it still has unused incided edges,
				// then move to its child and go deeper
				st.push(u);
				u = v;
			}

			// reverse-postorder: pre-append vertex with no leaving unused edge
			e_path.insert(e_path.begin(), u);
		}
#endif
		return e_path;
	}

	// Hierholzer's algorithm: recursive version
	vector<int> hierholzer_r(int n, vector<vector<int>>& adjL) {
		vector<int> e_path;
		if (adjL.empty()) return e_path;

		vector<vector<bool>> marked(n, vector<bool>(n, false));
		preorder(0, adjL, marked, e_path);

		return e_path;
	}

	void preorder(int u, vector<vector<int>>& adjL, vector<vector<bool>>& marked, vector<int>& e_path) {
		if (adjL[u].empty() == false) {
			for (int i = 0; i < (int)adjL[u].size(); i++) {
				// find next unused incident edge
				int v = adjL[u][i];
				if (marked[u][v] == true) continue;
				marked[u][v] = true;
				marked[v][u] = true;
			
				preorder(v, adjL, marked, e_path);
			}
		}

		// pre-append vertex with no leaving unused edge
		e_path.insert(e_path.begin(), u);
	}

	// Fleury's algorithm
	vector<int> fleury_r(int n, vector<vector<int>>& adjL) {
		vector<int> e_path;
		if (adjL.empty()) return e_path;

		vector<vector<bool>> marked(n, vector<bool>(n, false)); // marking used edges(u,v)+(v,u)
		fluery_path(0, n, adjL, marked, e_path);
		return e_path;
	}

	void fluery_path(int u, int n, vector<vector<int>>& adjL, vector<vector<bool>>& marked, vector<int>& e_path) {
		bool last_first = true;
		for (int j = 0; j < (int)adjL[u].size(); j++) {
			int v = adjL[u][j];
			if (marked[u][v] == true) continue;
			last_first = false;

			// is edge(u,v) safe (not bridge, or the only incident edge to u)?
			if (safe(u, v, n, adjL, marked) == true) {
				marked[u][v] = true;
				marked[v][u] = true;

				e_path.push_back(u);
				fluery_path(v, n, adjL, marked, e_path);
			}
		}

		// when we reach at starting vertex, then there is no incident edge, so
		//  just add it to the e_path;
		if (last_first == true) e_path.push_back(u);
	}

	bool safe(int u, int v, int n, vector<vector<int>>& adjL, vector<vector<bool>>& marked) {
		// is (u,v) the only incident edge to u?
		int adjc = 0;
		for (int i = 0; i < (int)adjL[u].size(); i++) {
			int v = adjL[u][i];
			if (marked[u][v] == true) continue;
			adjc++;
		}
		if (adjc == 1) return true;

		// is (u,v) a bridge?
		marked[u][v] = true;
		marked[v][u] = true;
		int cu = DFS(u, n, adjL, marked); // # of vertices reacheable from u
		int cv = DFS(v, n, adjL, marked); // # of vertices reacheable from v
		marked[u][v] = false;
		marked[v][u] = false;

		return (cu == cv);
	}

	int DFS(int u, int n, vector<vector<int>>& adjL, vector<vector<bool>>& marked) {
		vector<bool> visited(n, false);
		stack<int> st;
		st.push(u); visited[u] = true;
		int count = 0;
		while (!st.empty()) {
			int x = st.top(); st.pop();
			count++;
			if (adjL[x].empty() == false) {
				for (int j = 0; j < (int)adjL[x].size(); j++) {
					int y = adjL[x][j];
					if (marked[x][y] == true) continue; // this edge has been deleted
					if (visited[y] == true) continue;

					visited[y] = true;
					st.push(y);
				}
			}
		}

		return count;
	}

	// for debugging purpose
	vector<vector<int>> build_adjL(int n, vector<vector<int>> &edges) {
		vector<vector<int>> adjL(n, vector<int>());
		for (int i = 0; i < (int)edges.size(); i++) {
			int u = edges[i][0];
			int v = edges[i][1];
			adjL[u].push_back(v);
			adjL[v].push_back(u);
		}

		return adjL;
	}

	void print_e_path(vector<int> &e_path) {
		if (e_path.empty() == false) {
			for (int i = 0; i < (int)e_path.size(); i++) {
				int u = e_path[i];
				cout << u << ", ";
			}
			cout << endl;
		}
	}
};

#if 0
int main(void) {
#if 1
	vector<vector<int>> edges = { {0, 1}, {0, 2}, {0, 5}, {0, 4},
								  {1, 2}, {1, 5}, {1, 4},
								  {2, 5}, {2, 3},
								  {3, 4},
								  {4, 5} };
	int n = 6;
#else
	vector<vector<int>> edges = { {0, 1}, {0, 3},
								  {1, 2},
								  {2, 3} };
	int n = 4;
#endif
	Eulerian_Trail_UndirectedGraph t;
	vector<vector<int>> adjL = t.build_adjL(n, edges);
	vector<int> e_pathi = t.hierholzer_i(n, adjL);
	t.print_e_path(e_pathi);

	vector<int> e_pathr = t.hierholzer_r(n, adjL);
	t.print_e_path(e_pathr);

	vector<int> e_fleury = t.fleury_r(n, adjL);
	t.print_e_path(e_fleury);

	return 0;
}
#endif