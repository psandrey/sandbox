
/* Held�Karp/Bellman�Held�Karp algorithm for travelling salesman.
 *
 * Note 1: memoization and tabulation.
 * Note 2: plus backtracking version.
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class TravellingSalesman{
public:
	
	int tsp(int n, vector<vector<int>>& edges) {
		int ans = INT_MAX;
#if 1
		// DP with memoization
		int all = (1 << n) - 1; // the set containing all verticex
		vector<vector<int>> memo(all + 1, vector<int>(n, -1));
		
		// since it is cycle, we can start from any node, so why not 0?
		ans = tsp_memo(all, 0, n, edges, memo);
#endif

#if 0
		// DP with tabulation
		ans = tsp_tab(n, edges);
#endif

#if 0
		// backtracking version
		vector<bool> marked(n, false);
		for (int k = 0; k < n; k++)
			tsp_back(n, edges, k, k, n, marked, 0, &ans);
#endif

		return ans;
	}

	int tsp_tab(int n, vector<vector<int>>& edges) {
		int all = (1 << n) - 1;
		vector<vector<int>> dp(all + 1, vector<int>(n, INT_MAX));
		vector<vector<int>> pred(all + 1, vector<int>(n, -1));

		// if there is an edge from 0 to vertex j, add it as initialization
		pred[0][0] = 0;
		for (int j = 1; j < n; j++)
			if (edges[0][j] != -1) {
				dp[1 << j][j] = edges[0][j];
				pred[1 << j][j] = 0;
			}

		// find the shortest hamiltonian cycle
		for (int mask = 3; mask < all; mask++) {
			if ((mask & 1) != 0) continue; // edge 0->j was added at initialization time

			for (int k = 1; k < n; k++) {
				if ((mask & (1 << k)) == 0) continue; // if k not in the mask, then continue
				
				int mask_nok = mask ^ (1 << k);
				for(int j = 1; j < n; j++) {
					if (((1 << j) & mask_nok) == 0) continue; // if j not in the next mask, then continue

					if (edges[j][k] != INT_MAX && dp[mask_nok][j] != INT_MAX
						&& dp[mask][k] > dp[mask_nok][j] + edges[j][k]) {
						
						dp[mask][k] = dp[mask_nok][j] + edges[j][k];
						pred[mask][k] = j;
					}
				}
			}
		}

		// add edge from last vertex to vertex 0
		int ans = INT_MAX, initial = -1;
		for (int j = 1; j < n; j++) {
			if (dp[all - 1][j] == INT_MAX || edges[j][0] == INT_MAX) continue;

			if (ans > dp[all - 1][j] + edges[j][0]) {
				ans = dp[all - 1][j] + edges[j][0];
				initial = j;
			}
		}
		print_matrix(dp);
		reconstruct_cycle(all, initial, pred);

		return ans;
	}

	void reconstruct_cycle(int all, int initial, vector<vector<int>>& pred) {
		int mask = all - 1;
		int j = initial;
		cout << "TS-cycle: ";
		while (mask != 0 && j != 0) {
			cout << j << ", ";

			int prev = pred[mask][j];
			mask = mask ^ (1 << j);
			j = prev;
		}
		cout << j << ", " << endl;
	}

	// DP with memoization
	int tsp_memo(int mask, int k, int n, vector<vector<int>>& edges, vector<vector<int>>& memo) {
		if (memo[mask][k] != -1) return memo[mask][k];
		
		int mask_nok = mask ^ (1 << k); // the current set without vertex k
		int ans = INT_MAX;
		if (mask_nok == 0)
			// k is the only vertex left in the set, so if there is an edge from 0 to k return that weight
			// (thus the cycle is closed), otherwise return INT_MAX
			ans = (edges[0][k] != -1) ? edges[0][k] : INT_MAX;
		else {
			// chose the cheepest path to k in the current set
			for (int j = 0; j < n; j++) {
				if ((mask_nok & (1 << j)) == 0) continue; // j not in the current set (without k)
				if (edges[j][k] == -1) continue; // no edge from j to k

				int candidate = tsp_memo(mask_nok, j, n, edges, memo);
				if (candidate != INT_MAX && ans > candidate + edges[j][k])
					ans = candidate + edges[j][k];
			}
		}

		memo[mask][k] = ans;
		return memo[mask][k];
	}

	// backtracking version
	void tsp_back(int n, vector<vector<int>>& edges, int head, int u, int rem, 
					vector<bool>& marked, int cost, int* ans) {
		if (rem == 1) {
			if (edges[u][head] != -1 && (*ans) > cost + edges[u][head])
				(*ans) = cost + edges[u][head];
			return;
		}

		marked[u] = true;
		for (int j = 0; j < n; j++) {
			if (marked[j] == true || edges[u][j] == -1) continue;

			tsp_back(n, edges, head, j, rem - 1, marked, cost + edges[u][j], ans);
		}
		marked[u] = false;
	}

	void print_matrix(vector<vector<int>>& a) {
		for (int u = 0; u < (int)a.size(); u++) {
			for (int v = 0; v < (int)a[0].size(); v++) {
				if (a[u][v] == INT_MAX) cout << "-- ";
				else cout << a[u][v] << " ";
			}
			cout << endl;
		}
		cout << endl;
	}
};

#if 0
int main(void) {
	vector<vector<int>> edges = {
		{ 0, 10, 15, 20 },
		{ 10, 0, 35, 25 },
		{ 15, 35, 0, 30 },
		{ 20, 25, 30, 0 }
	};
	int n = 4;

	TravellingSalesman t;
	cout << t.tsp(n, edges) << endl;

	return 0;
}
#endif