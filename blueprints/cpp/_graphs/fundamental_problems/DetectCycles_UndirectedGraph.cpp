
/*
 * Detect cycles in undirected graph using BFS and DFS (recursive and iterative versions).
 *
 * Note: Only for didactic purpose.
 */

#include <stack>
#include <queue>
#include <iostream>
using namespace std;

class DetectCycles_UndirectedGraph {
public:

	bool hasCycles(int n, vector<vector<int>>& adjL) {
		bool ans = false;

#if 1
		// detect cycles in a undirected graph using BFS
		ans = hasCycles_BFS(n, adjL);
#else
		// detect cycles in a undirected graph using DFS (recursive)
		//ans = hasCycles_DFSr(n, adjL);

		// detect cycles in a undirected graph using DFS (iterative)
		ans = hasCycles_DFSi(n, adjL);
#endif

		return ans;
	}

	bool hasCycles_BFS(int n, vector<vector<int>>& adjL) {
		vector<int> p(n, -1);
		for (int i = 0; i < n; i++)
			if (p[i] == -1 && hasCycles_BFS(i, adjL, p) == true)
				return true;

		return false;
	}

	bool hasCycles_BFS(int s, vector<vector<int>>& adjL, vector<int>& p) {
		queue<int> q; q.push(s); p[s] = s;
		while (!q.empty()) {
			int u = q.front(); q.pop();

			if (!adjL[u].empty()) {
				for (int i = 0; i < (int)adjL[u].size(); i++) {
					int v = adjL[u][i];
					if (v == p[u]) continue; // edge to parent is not considered a loop
					// cycle detected: v has already a parent, thus u is the second parent, thus is a cycle
					if (p[v] != -1) return true;

					p[v] = u;
					q.push(v);
				}
			}
		}

		return false;
	}

	bool hasCycles_DFSr(int n, vector<vector<int>>& adjL) {
		vector<bool> discovered(n, false);
		for (int i = 0; i < n; i++)
			if (discovered[i] == false && hasCycles_DFSr(adjL, i, -1, discovered) == true)
				return true;

		return false;
	}

	bool hasCycles_DFSr(vector<vector<int>>& adjL, int u, int p, vector<bool>& discovered) {
		if (discovered[u] == true) return true;
		discovered[u] = true;

		if (!adjL[u].empty()) {
			for (int i = 0; i < (int) adjL[u].size(); i++) {
				int v = adjL[u][i];
				if (v == p) continue; // edge to parent is not considered a loop
			
				if (hasCycles_DFSr(adjL, v, u, discovered) == true)
					return true;
			}
		}

		return false;
	}

	bool hasCycles_DFSi(int n, vector<vector<int>>& adjL) {
		vector<int> p(n, -1);
		for (int i = 0; i < n; i++)
			if (p[i] == -1 && hasCycles_DFSi(i, adjL, p) == true)
				return true;

		return false;
	}

	bool hasCycles_DFSi(int s, vector<vector<int>>& adjL, vector<int>& p) {
		stack<int> st;
		st.push(s); p[s] = s;
		while (!st.empty()) {
			int u = st.top(); st.pop();

			if (adjL[u].empty() == false) {
				for (int j = 0; j < (int)adjL[u].size(); j++) {
					int v = adjL[u][j];
					if (v == p[u]) continue; // edge to parent is not considered a loop
					if (p[v] != -1) return true; // cycle detected
					
					p[v] = u;
					st.push(v);
				}
			}
		}

		return false;
	}

	// for debug purposes
	vector<vector<int>> build_graph(vector<vector<int>>& edges, int n) {
		vector<vector<int>> adjL(n, vector<int>());
		for (int i = 0; i < (int)edges.size(); i++) {
			int u = edges[i][0];
			int v = edges[i][1];
			adjL[u].push_back(v);
			adjL[v].push_back(u);
		}

		return adjL;
	}
};

#if 0
int main(void) {
	vector<vector<int>> edges = {
		{0, 1}, {1, 4},
		{1, 5}, {2, 3},
		{1, 3}, {2, 4}};
	int  n = 6;

	DetectCycles_UndirectedGraph t;
	vector<vector<int>> adjL = t.build_graph(edges, n);
	cout << t.hasCycles(n, adjL) << endl;

	return 0;
}
#endif