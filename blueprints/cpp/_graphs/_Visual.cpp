
#include <thread>
#include <chrono>
#include <vector>
#include <iostream>
using namespace std;

#include "_Visual.h"
namespace ns_visual {
	void _Visual::print_x() { cout << YELLOW << "X " << RESET; }
	void _Visual::print_o() { cout << WHITE << "O " << RESET; }

	void _Visual::print(vector<vector<bool>>& marked) {
		int n = marked.size(), m = marked[0].size();
		system("cls");

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if (marked[i][j] == true) print_x();
				else print_o();
			}
			cout << endl;
		}

		cout << endl << endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}