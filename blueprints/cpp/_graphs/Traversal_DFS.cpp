
/*
 * DFS traversal methods and DFS orders: pre-order, post-order, reverse-post-order.
 * Note: Contains recursive and iterative versions.
 *
 * Note: Only for didactic purpose.
 */

#include <stack>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class Traversal_DFS {
public:
	// Building predecesor DFS tree -------------------------------------------
	// ------------------------------------------------------------------------

	// DFS: the recursive version
	vector<int> DFSr(int s, int n, vector<vector<int>>& adjL) {
		vector<bool> marked(n, false);
		vector<int> p(n, -1); // predecesor vector for DFS tree

		p[s] = s;
		DFSr(s, adjL, p, marked);

		return p;
	}

	void DFSr(int u, vector<vector<int>>& adjL, vector<int>& p, vector<bool>& marked) {
		marked[u] = true;

		for (int i = 0; i < (int)adjL[u].size(); i++) {
			int v = adjL[u][i];
			if (marked[v] == false) {
				p[v] = u;
				DFSr(v, adjL, p, marked);
			}
		}
	}

	// DFS: iterative version
	vector<int> DFSi(int s, int n, vector<vector<int>>& adjL) {
		vector<int> p(n, -1); // predecesor vector for DFS tree
		vector<int> it(n, 0); // iterators for neighbors of each vertex
		vector<bool> marked(n, false);

		stack<int> st;
		st.push(s); marked[s] = true;
		p[s] = s;
		while (!st.empty()) {
			int u = st.top();
			if (!adjL.empty() && it[u] != adjL[u].size()) {
				int v = adjL[u][it[u]]; it[u]++;
				if (marked[v] == true) continue;

				st.push(v); marked[v] = true;
				p[v] = u;
			}
			else st.pop();
		}

		return p;
	}

	vector<int> DFSi2(int s, int n, vector<vector<int>>& adjL) {
		vector<int> p(n, -1);
		vector<bool> marked(n, false);

		vector<vector<int>::iterator> it(adjL.size());
		for (int u = 0; u < n; u++) it[u] = adjL[u].begin();

		stack<int> st;
		st.push(s); marked[s] = true;
		p[s] = s;
		while (!st.empty()) {
			int u = st.top();

			if (it[u] != adjL[u].end()) {
				int v = *it[u]; it[u]++;

				if (marked[v] == false) {
					st.push(v); marked[v] = true;
					p[v] = u;
				}
			}
			else st.pop();
		}

		return p;
	}

	// DFS dfs_orders: pre, post and reverse-post -----------------------------
	// ------------------------------------------------------------------------
	struct dfs_orders { vector<int> pre, post, rpost; };

	// iterative dfs orders
	struct dfs_orders DFS_orderi(int s, int n, vector<vector<int>>& adjL) {
		vector<int> pre, post, rpost;
		vector<int> it(n, 0); // iterators for neighbors of each vertex
		vector<bool> marked(n, false);

		stack<int> st;
		st.push(s); marked[s] = true;
		pre.push_back(s);
		while (!st.empty()) {
			int u = st.top();
			if (!adjL.empty() && it[u] != adjL[u].size()) {
				int v = adjL[u][it[u]]; it[u]++;
				if (marked[v] == false) {
					st.push(v); marked[v] = true;
				
					pre.push_back(v);
				}
			}
			else {
				st.pop();

				post.push_back(u);
				rpost.insert(rpost.begin(), u);
			}
		}

		struct dfs_orders o;
		o.pre = pre;
		o.post = post;
		o.rpost = rpost;
		return o;
	}

	// recursive dfs orders
	// ------------------------------------------------------------------------
	struct dfs_orders DFS_orderr(int s, int n, vector<vector<int>>& adjL) {
		vector<bool> marked(n, false);
		return DFS_orderr(0, marked, adjL);
	}

	struct dfs_orders DFS_orderr(int u, vector<bool>& marked, vector<vector<int>>& adjL) {
		struct dfs_orders o;

		o.pre.push_back(u);
		marked[u] = true;

		for (int i = 0; i < (int)adjL[u].size(); i++) {
			int v = adjL[u][i];
			if (marked[v] == true) continue;

			struct dfs_orders add_o = DFS_orderr(v, marked, adjL);
			if (!add_o.pre.empty()) {
				o.pre.insert(o.pre.end(), add_o.pre.begin(), add_o.pre.end());
				o.post.insert(o.post.end(), add_o.post.begin(), add_o.post.end()); // insert at end
				o.rpost.insert(o.rpost.begin(), add_o.rpost.begin(), add_o.rpost.end()); // insert at begin
			}
		}

		o.post.push_back(u); // insert at end
		o.rpost.insert(o.rpost.begin(), u); // insert at begin
		return o;
	}

	// for debugging purpose
	vector<vector<int>> build_adjL(int n, vector<vector<int>> &edges) {
		vector<vector<int>> adjL(n, vector<int>());
		for (int i = 0; i < (int)edges.size(); i++) {
			int u = edges[i][0];
			int v = edges[i][1];
			adjL[u].push_back(v);
			adjL[v].push_back(u);
		}

		return adjL;
	}

	void print_pred(int n, vector<int>& p) {
		if (!p.empty())
			for (int u = 0; u < (int)n; u++)
				cout << "(" << p[u] << "," << u << ")," << " ";
		cout << endl;
	}

	void print_dfs_orders(struct dfs_orders& o) {
		cout << " Pre-order         :";
		for (auto u : o.pre) cout << u << ", ";
		cout << endl;

		cout << " Post-order        :";
		for (auto u : o.post) cout << u << ", ";
		cout << endl;

		cout << " Reverse-post-order:";
		for (auto u : o.rpost) cout << u << ", ";
		cout << endl;
	}
};

#if 0
int main(void) {
/*
	vector<vector<int>> edges = {
		{0, 1}, {0, 3},
		{1, 2}, {1, 4},
		{2, 5},
		{3, 4}, {3, 6},
		{4, 5}, {4, 7},
		{5, 8},
		{6, 7},
		{7, 8} };
*/

	vector<vector<int>> edges = {
		{4, 5}, {3, 6},
		{0, 3}, {0, 1},
		{1, 2}, {1, 4},
		{3, 4}, {4, 7},
		{6, 7},
		{7, 8},
		{5, 8},
		{2, 5},
	};
	int n = 9;

	Traversal_DFS t;

	vector<vector<int>> adjL = t.build_adjL(n, edges);
	vector<int> p;

	p = t.DFSr(0, n, adjL);
	cout << "r: "; t.print_pred(n, p);

	p = t.DFSi(0, n, adjL);
	cout << "i1: "; t.print_pred(n, p);

	p = t.DFSi2(0, n, adjL);
	cout << "i2: "; t.print_pred(n, p);

	Traversal_DFS::dfs_orders o;
	o = t.DFS_orderi(0, n, adjL);
	cout << "DFS orders i:" << endl;
	t.print_dfs_orders(o);

	o = t.DFS_orderr(0, n, adjL);
	cout << "DFS orders r:" << endl;
	t.print_dfs_orders(o);

	return 0;
}
#endif