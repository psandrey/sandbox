
/*
 * BFS traversal method.
 *
 * Note: Only for didactic purpose.
 */

#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class Traversal_BFS {
public:
	vector<int> BFSi(int s, int n, vector<vector<int>>& adjL) {
		vector<int> ans;

		vector<bool> marked(n, false);
		queue<int> q;
		q.push(s); marked[s] = true;

		while (!q.empty()) {
			int u = q.front(); q.pop();
			ans.push_back(u);

			for (int i = 0; i < (int)adjL[u].size(); i++) {
				int v = adjL[u][i];
				if (marked[v] == true) continue;

				q.push(v);
				marked[v] = true;
			}
		}

		return ans;
	}

};

#if 0
int main(void) {
	int n = 6;
	vector<vector<int>> adjL(n, vector<int>());
	adjL[0] = { 1, 5 };
	adjL[1] = { 0, 2, 3 };
	adjL[2] = { 1, 4 };
	adjL[3] = { 5, 1, 4 };
	adjL[4] = { 2, 3 };
	adjL[5] = { 0, 3 };

	Traversal_BFS t;
	vector<int> ans = t.BFSi(0, n, adjL);
	if (!ans.empty()) for (int u : ans) cout << u << " ";
	cout << endl;

	return 0;
}
#endif