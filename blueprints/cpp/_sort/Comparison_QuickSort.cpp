
/*
 * Comparison Sort: QuickSort (in-place)
 *
 * Note: Only for didactic purpose.
 * Note: Recursive/Iterative postorder traversal of full binary tree.
 * Note: For Randomized QuickSort, chose a random pivot between i and j in the partition method and
 *       swap it with a[j]. Then, pivot method remains unchanged.
 *
 * Complexity:
 *    - worst case:   T(n) = O(n^2)
 *    - average case: T(n) = O(n.logn)
 */

#include <stack>
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class QuickSort {
public:
	void sort_inplace(vector<int>& a) {
		if (a.empty() || a.size() == 1) return;
#if 0
		// iterative version
		return sort_i(a);
#else
		// recursive version
		return sort_r(a, 0, a.size() - 1);
#endif
	}

	void sort_r(vector<int>& a, int i, int j) {
		if (i < j) {
			int pivot = partition(a, i, j);
			sort_r(a, i, pivot - 1);
			sort_r(a, pivot + 1, j);
		}
	}

	void sort_i(vector<int>& a) {
		stack<pair<int, int>> st;
		st.push(make_pair(0, a.size() - 1));

		// preorder traversal on indexes
		while (!st.empty()) {
			pair<int, int> p = st.top(); st.pop();
			int i = p.first, j = p.second;
			if (i < j) {
				int pivot = partition(a, i, j);
				st.push(make_pair(pivot + 1, j));
				st.push(make_pair(i, pivot - 1));
			}
		}
	}

	// partition subvector i::j around a[j] as pivot such that a[i..pivot-1] < a[pivot] <= a[pivot+1..j]
	int partition(vector<int>& a, int lo, int hi) {
		/* Note: For Randomized QuickSort, chose a random pivot between i and j and
		 *       swap it with a[j]. */
#if 0
		int p = lo;
		for (int i = lo + 1; i <= hi; i++) {
			if (a[i] <= a[lo]) { p++; swap(a[p], a[i]); }
		}

		swap(a[p], a[lo]);
		return p;

#else
		int p = lo;
		for (int i = lo; i < hi; i++) {
			if (a[i] < a[hi]) {
				swap(a[p], a[i]);
				p++;
			}
		}

		swap(a[p], a[hi]);
		return p;
#endif
	}
};

#if 0
int main(void) {
	vector<int> a = { 7, 3, 0, 4, 9, 5, 2, 6, 1 };
	//vector<int> a = { 2,1 ,1, 3, 3, 4, 0, 0, 3, 3 };

	QuickSort t;
	t.sort_inplace(a);
	for (auto x : a) cout << x << " ";

	return 0;
}
#endif