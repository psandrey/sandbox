
/*
 * Distribution Sort: Bucket-sort.
 *
 * Note: This is a generic Bucket-sort.
 * Note: Only for didactic purpose.
 *
 * Complexity:
 *    - worst case time complexity: T(n) = O(n^2)
 *    - average case time complexity: T(n) = O(n + k)
 *    - space complexity: S(n) = O(n)
 */

#include <list>
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class BucketSort {
public:
	void sort(vector<int>& a) {
		int n = (int)a.size();
		vector<list<int>> bucket(n, list<int>());

		// hash to buckets
		int M = get_max(a); // the maximum number will go to bucket k
		for (int i = 0; i < n; i++) {
			// hash function (divide first then multiply to avoid overflow)
			int b = (int)floor(((double)a[i] / M) * ((double)n - 1.0));
			bucket[b].push_back(a[i]); 
		}

		// sort each bucket
		for (int i = 0; i < n; i++)
			if (!bucket[i].empty())
				bucket[i].sort();

		// reconstruct a from in-order buckets
		int j = 0;
		for (int i = 0; i < n; i++)
			if (!bucket[i].empty()) {
				list<int>::iterator it_i;
				for (it_i = bucket[i].begin(); it_i != bucket[i].end(); it_i++)
					a[j++] = *it_i;
			}
	}

	int get_max(vector<int>& a) {
		int m = INT_MIN;
		for (auto x : a) m = max(m, x);
		return m;
	}

#if 0
	void insertion_sort(list<int>& a) {
		if (a.empty() || a.size() == 1) return;

		list<int>::iterator it_i;
		for (it_i = next(a.begin()); it_i != a.end(); it_i++) {
			int key = *it_i;

			// search for the key's right position
			list<int>::iterator it_j;
			for (it_j = prev(it_i); it_j != a.begin(); it_j--)
				if (*it_j > key) *next(it_j) = *it_j;
				else break;

			// insert key in its right position
			*next(it_j) = key;
		}
	}
#endif

};

#if 0
int main(void) {
	vector<int> a = {9, 2, 3, 8, 6, 1, 5, 7, 8, 4};

	BucketSort t;
	t.sort(a);
	for (auto x : a) cout << x << " ";

	return 0;
}
#endif