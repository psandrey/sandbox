
/*
 * Comparison Sort: Insertion-Sort (in-place)
 *
 * Note: Only for didactic purpose.
 *
 * Complexity:
 *    - worst/average case: T(n) = O(n^2)
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class InsertionSort {
public:
	void sort_inplace(vector<int> &a) {
		if (a.empty()) return;

		for (int i = 1; i < (int)a.size(); i++) {
			int key = a[i];

			// search for the key's right position
			int j = i - 1;
			for (; j >= 0; j--)
				if (a[j] > key) a[j + 1] = a[j];
				else break;

			// insert key in its right position
			a[j + 1] = key;
		}
	}
};

#if 0
int main(void) {
	vector<int> a = { 3, 7, 4, 9, 5, 2, 6, 1 };

	InsertionSort t;
	t.sort_inplace(a);

	for (auto x : a) cout << x << " ";
	return 0;
}
#endif