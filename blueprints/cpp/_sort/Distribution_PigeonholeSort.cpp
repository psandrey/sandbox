
/*
 * Distribution Sort: Pigeonhole-Sort (in place).
 *
 * Note: Only for didactic purpose.
 *
 * Complexity:
 *    - worst/average case:   T(n) = O(n + k)
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class PigeonholeSort {
public :
	void sort(vector<int>& a) {
		if (a.empty()) return;
		int m = get_min(a), M = get_max(a);
		int range = M - m + 1;
		vector<vector<int>> holes(range, vector<int>());

		// items to holes
		for (int i = 0; i < (int)a.size(); i++)
			holes[a[i] - m].push_back(a[i]);

		// distribute backwards to be stable sort
		int j = (int)a.size() - 1;
		for (int i = range - 1; i >= 0; i--) {
			while (!holes[i].empty()) {
				a[j--] = holes[i].back();
				holes[i].pop_back();
			}
		}
	}

	int get_min(vector<int>& a) {
		int m = INT_MAX;
		for (int i = 0; i < (int)a.size(); i++)
			m = min(m, a[i]);

		return m;
	}

	int get_max(vector<int>& a) {
		int m = INT_MIN;
		for (int i = 0; i < (int)a.size(); i++)
			m = max(m, a[i]);

		return m;
	}

};

#if 0
int main(void) {
	vector<int> a = { 3, 1, 5, 1, 7, 3 };
	int k = 9;

	PigeonholeSort t;
	t.sort(a);
	for (auto x : a) cout << x << " ";

	return 0;
}
#endif