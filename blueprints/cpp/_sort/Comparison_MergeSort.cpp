
/*
 * Comparison Sort: Merge-Sort (in-place)
 *
 * Note: Only for didactic purpose.
 * Note: Recursive/Iterative postorder traversal of full binary tree.
 *
 * Complexity:
 *    - worst/average case: T(n) = O(n.logn)
 */

#include <stack>
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class MergeSort {
public:
	void sort_inplace(vector<int>& a) {
		if (a.empty() || a.size() == 1) return;
#if 1
		// iterative version
		return sort_i(a);
#else
		// recursive version
		return sort_r(a, 0, a.size() - 1);
#endif
	}

	// iterative version
	void sort_i(vector<int>& a) {
		stack<pair<int, int>> st; st.push(make_pair(0, a.size() - 1));
		pair<int, int> prev = make_pair(-1, -1);

		// postorder traversal on indexes
		while (!st.empty()) {
			pair<int, int> p = st.top();
			int i = p.first, j = p.second;

			if (i < j) {
				int m = (i + j) / 2;
				int ip = prev.first, jp = prev.second;
				if (ip == (m + 1) && jp == j) {
					merge(a, i, m, j);
					st.pop();
				} else {
					st.push(make_pair(m + 1, j));
					st.push(make_pair(i, m));
				}
			} else st.pop();
			
			prev = p;
		}
	}
	
	// recursive version
	void sort_r(vector<int>& a, int i, int j) {
		if (i >= j) return;

		int m = (i + j) / 2;
		sort_r(a, i, m);
		sort_r(a, m + 1, j);
		merge(a, i, m, j);
	}

	void merge(vector<int>& a, int i, int m, int j) {
		vector<int> L(a.begin() + i, a.begin() + m + 1);
		vector<int> R(a.begin() + m + 1, a.begin() + j+ 1);

		// merge L and R in a so that a from i to j is sorted
		int l = 0, r = 0, k = i;
		while (l < (int)L.size() && r < (int)R.size())
			if (L[l] < R[r]) a[k++] = L[l++];
			else a[k++] = R[r++];
		
		// add tail of L or R (if items remains)
		while (l < (int)L.size()) a[k++] = L[l++];
		while (r < (int)R.size()) a[k++] = R[r++];
	}

};

#if 0
int main(void) {
	vector<int> a = { 3, 7, 4, 9, 5, 2, 6, 1 };

	MergeSort t;
	t.sort_inplace(a);
	for (auto x : a) cout << x << " ";
	return 0;
}
#endif