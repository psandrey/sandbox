
/*
 * Distribution Sort: Counting-Sort.
 *
 * Note: Only for didactic purpose.
 *
 * Complexity:
 *    - worst/average case:   T(n) = O(n + k)
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class CountingSort {
public:
	vector<int> sort(vector<int>& a, int k) {
		vector<int> counter(k + 1, 0);
		vector<int> b(a.size());

		// compute frequencies
		for (int i = 0; i < (int)a.size(); i++)
			counter[a[i]]++;

		// transform counts to indexes
		for (int i = 1; i <= k; i++)
			counter[i] += counter[i - 1];

		// distribute (backwards to conserve relative position of identical items in the original array)
		for (int i = (int)a.size() - 1; i >= 0; i--) {
			b[counter[a[i]] - 1] = a[i]; // -1 because vector is from 0
			counter[a[i]]--;
		}

		return b;
	}
};

#if 0
int main(void) {
	vector<int> a = {3, 1, 5, 1, 7, 3};
	int k = 9;

	CountingSort t;
	vector<int> b = t.sort(a, k);
	for (auto x : b) cout << x << " ";
	
	return 0;
}
#endif