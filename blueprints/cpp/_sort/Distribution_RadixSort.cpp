
/*
 * Distribution Sort: Radix-Sort.
 *
 * Note: Only for didactic purpose.
 *
 * Complexity:
 *    - worst/average case time complexity: T(n) = O(w.n), where w is the no of bits of the maximum element in the array
 *    - space complexity: S(n) = O(n)
 */

#include <vector>
#include <list>
#include <algorithm>
#include <iostream>
using namespace std;

class RadixSort {
public:
	vector<int> sort(vector<int>& a, int radix) {
		vector<int> sorted(a);
		int msb = log_floor(get_max(a), radix);
		
		// - sort the vector relative to digit d using a stable sort algorithm to preserve
		// the relative position of items with identical previous digit
		// - stable sorting algorithms: counting sort, pigeonhole sort... etc).
#if 0
		// counting sort
		for (int d = 0; d <= msb; d++)
			sorted = radix_sort_v1(sorted, radix, d);

		return sorted;
#else
		// pigeonhole sort
		for (int d = 0; d <= msb; d++)
			radix_sort_v2(a, radix, d);

		return a;
#endif
	}

	// v1: use counting sort as stable sort
	vector<int> radix_sort_v1(vector<int>& a, int radix, int d) {
		int n = (int)a.size();
		int D = (int)pow(radix, d);
		
		// compute frequencies
		vector<int> count(radix, 0);
		for (int i = 0; i < n; i++) {
			int digit = (a[i] / D) % radix;
			count[digit]++;
		}

		// transform counts into indexes
		for (int i = 1; i < radix; i++)
			count[i] += count[i - 1];

		// distribute
		vector<int> sorted(n);
		for (int i = n - 1; i >= 0; i--) {
			int digit = (a[i] / D) % radix;
			sorted[count[digit] - 1] = a[i];
			count[digit]--;
		}

		return sorted;
	}

	// v2: use pigeonhole sort as stable sort
	void radix_sort_v2(vector<int>& a, int radix, int d) {
		int n = (int)a.size();
		int D = (int)pow(radix, d);

		// items to holes
		vector<list<int>> holes(radix, list<int>());
		for (int i = 0; i < (int)a.size(); i++) {
			int b = (a[i] / D) % radix;
			holes[b].push_back(a[i]);
		}

		// distribute backwards to be stable sort
		int j = n - 1;
		for (int i = radix - 1; i >= 0; i--) {
			while (!holes[i].empty()) {
				a[j--] = holes[i].back();
				holes[i].pop_back();
			}
		}
	}

	int get_min(vector<int>& a) {
		int m = INT_MAX;
		for (int i = 0; i < (int)a.size(); i++)
			m = min(m, a[i]);

		return m;
	}

	int get_max(vector<int>& a) {
		int m = INT_MIN;
		for (int i = 0; i < (int)a.size(); i++)
			m = max(m, a[i]);

		return m;
	}

	int log_floor(int x, int base) {
		return (int)(log(x) / log(base));
	}
};

#if 0
int main(void) {
	vector<int> a = { 345, 95, 71, 342, 351, 103, 104 };
	int radix = 10;

	RadixSort t;
	vector<int> b = t.sort(a, radix);
	for (auto x : b) cout << x << " ";

	return 0;

}
#endif