
/* 
 * Adaptable min-priority-queue based on heap.
 *
 * Note: Only for didactic purpose.
 */

#include <unordered_map>
#include <vector>
#include <iostream>
using namespace std;

class min_pq_adaptable {
private:
	// vector of objects
	vector<int> pq;
	// key = the object, value = pair(its priority, its index in pq)
	unordered_map<int, pair<int, int>> hm; 

	void _swap(int i, int j) {
		// swap in the priority queue
		swap(pq[i], pq[j]);

		// update hash map
		pair<int, int> pp = hm[pq[i]];
		pp.second = i; hm[pq[i]] = pp;

		pp = hm[pq[j]];
		pp.second = j; hm[pq[j]] = pp;
	}

	void _rise(int i) {
		if (i == 0) return;
		int p = (i - 1) / 2;

		// we need to swap if the parent has a smaller priority
		if (hm[pq[p]].first < hm[pq[i]].first) {
			_swap(p, i);

			// rise until max heap property is satisfied
			_rise(p);
		}
	}

	void _sink(int i) {
		int l = 2 * i + 1, r = 2 * i + 2;
		int big = i;

		if (l < pq.size() && hm[pq[big]].first < hm[pq[l]].first) big = l;
		if (r < pq.size() && hm[pq[big]].first < hm[pq[r]].first) big = r;

		// do we need to swap ?
		if (big != i) {
			_swap(i, big);

			// sink until max heap property is satisfied
			_sink(big);
		}
	}

public:
	void push(int x, int p) {
		if (hm.find(x) != hm.end()) return; // only unique objects as key

		int idx = pq.size();
		hm[x] = make_pair(p, idx);
		pq.push_back(x);

		// restore max heap property
		_rise(idx);
	}

	void pop() {
		if (hm.empty()) return;
		_swap(0, pq.size() - 1);

		// erase it from hash
		hm.erase(hm.find(pq[pq.size() - 1]));

		// erase it from pq
		pq.pop_back();

		// restore max heap property
		_sink(0);
	}

	void increase(int x, int p) {
		if (hm.empty()) return;
		if (hm.find(x) == hm.end()) return;

		// increase priority
		pair<int, int> pp = hm[pq[x]];
		if (pp.first >= p) return;
		pp.first = p; hm[x] = pp;

		// restore max heap property
		_rise(pp.second);
	}

	void decrease(int x, int p) {
		if (hm.empty()) return;
		if (hm.find(x) == hm.end()) return;

		// decrease priority
		pair<int, int> pp = hm[pq[x]];
		if (pp.first <= p) return;
		pp.first = p; hm[x] = pp;

		// restore max heap property
		_sink(pp.second);
	}

	int top() {
		if (hm.empty()) return -1;

		return pq[0];
	}

	int get_priority(int x) {
		if (hm.empty()) return -1;
		if (hm.find(x) == hm.end()) return -1;

		return hm[x].first;
	}

#if 0
private:
	void heapify_rise(int i) {
		while (i > 0) {
			int p = (i - 1) / 2;

			if (map[heap[i]].first >= map[heap[p]].first) break;

			swap(p, i);
			i = p;
		}
	}

	void heapify_sink(int i) {
		int n = (int)heap.size();
		while (i < (n / 2)) {
			int l = 2 * i + 1;
			int r = 2 * i + 2;

			int smallest = i;
			if (l < n && map[heap[smallest]].first > map[heap[l]].first) smallest = l;
			if (r < n && map[heap[smallest]].first > map[heap[r]].first) smallest = r;
			if (smallest == i) break;

			swap(i, smallest);
			i = smallest;
		}
	}
#endif
};

#if 0
int main(void) {
	min_pq_adaptable pq;

	pq.push(1, 7);
	pq.push(2, 8);
	pq.push(3, 15);
	pq.push(4, 5);
	pq.push(5, 3);
	pq.push(6, 1);
	cout << pq.top() << " : " << pq.get_priority(pq.top()) << endl;

	//pq.decrease_key(1, 0);
	//cout << pq.top() << endl;

	//pq.increase_key(6, 20);
	//cout << pq.top() << endl;


# if 1
	pq.pop();
	cout << pq.top() << " : " << pq.get_priority(pq.top()) << endl;
	pq.pop();
	cout << pq.top() << " : " << pq.get_priority(pq.top()) << endl;
	pq.pop();
	cout << pq.top() << " : " << pq.get_priority(pq.top()) << endl;
	pq.pop();
	cout << pq.top() << " : " << pq.get_priority(pq.top()) << endl;
	pq.pop();
	cout << pq.top() << " : " << pq.get_priority(pq.top()) << endl;
	pq.pop();
	cout << pq.top() << " : " << pq.get_priority(pq.top()) << endl;
#endif
	return 0;
}
#endif