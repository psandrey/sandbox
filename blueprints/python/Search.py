###############################################################################
## 1. Binary Search BSTrees
## 2. Self-Balancing Search Trees
## 	2.1. AVL Trees
##	2.2. Red-Black Trees
###############################################################################
import math
import enum

###############################################################################
## 1. Binary Search Trees
###############################################################################
class BSTNode:
	key: int
	left: object
	right: object
	p: object
	def __init__(self, key: int):
		self.key = key

		self.left = None
		self.right = None
		self.p = None

class BSTree:
	root: BSTNode
	sentinel: object
	def __init__(self, root: BSTNode):
		self.sentinel = BSTNode(-1)

		# init root's attributes
		self.root = root
		self.root.p = self.sentinel
		self.root.left = self.sentinel
		self.root.right = self.sentinel

class BSTreeOps:
	@staticmethod
	def BSTInorderTraversal(T:BSTree, x:BSTNode):
		if x != T.sentinel:
			BSTreeOps.BSTInorderTraversal(T, x.left)
			print(x.key, " ")
			BSTreeOps.BSTInorderTraversal(T, x.right)

	@staticmethod
	def BSTPreorderTraversal(T:BSTree, x:BSTNode):
		if x != T.sentinel:#
			print(x.key, " ")
			BSTreeOps.BSTPreorderTraversal(T, x.left)
			BSTreeOps.BSTPreorderTraversal(T, x.right)
		
	@staticmethod
	def BSTPostorderTraversal(T:BSTree, x:BSTNode):
		if x != T.sentinel:#
			BSTreeOps.BSTPostorderTraversal(T, x.left)
			BSTreeOps.BSTPostorderTraversal(T, x.right)
			print(x.key, " ")

	@staticmethod
	def BSTInsert(T: BSTree, z: BSTNode):
		y = T.sentinel
		x = T.root
		while x != T.sentinel:
			y = x
			if z.key <= x.key:
				x = x.left
			else:
				x = x.right
		z.p = y
		if y == T.sentinel:
			T.root = z
		elif z.key <= y.key:
			y.left = z
		else:
			y.right = z
		z.left = T.sentinel
		z.right = T.sentinel

	@staticmethod
	def BSTDelete(T: BSTree, z: BSTNode):
		# do not let to delete the sentinel
		if z == None or z == T.sentinel:
			return

		# y has at least one T.sentinel as a child
		if (z.left == T.sentinel or z.right == T.sentinel):
			y = z
		else:
		# find the z's successor
			y = BSTreeOps.BSTMinValue(T, z.right)

		# x is y's only child
		if y.left != T.sentinel:
			x = y.left
		else:
			x = y.right
		
		# remove y from the parent chain
		x.p = y.p
		if y.p != T.sentinel:
			if y == y.p.left:
				y.p.left = x
			else:
				y.p.right = x
		else:
			T.root = x
		
		if y != z:
			z.key = y.key

	@staticmethod
	def BSTMinValue(T, x):
		while x.left != T.sentinel:
			x = x.left
		return x

	@staticmethod
	def BSTTransplant(T, u, v):
		if u.p == T.sentinel:
			T.root = v
		elif u == u.p.left:
			u.p.left = v
		else:
			u.p.right = v
		if v != T.sentinel:
			v.p = u.p

class testBST:
	def doTestInsert(self):
		T = BSTree(BSTNode(10))
		BSTreeOps.BSTInsert(T, BSTNode(5))
		BSTreeOps.BSTInsert(T, BSTNode(15))
		BSTreeOps.BSTInsert(T, BSTNode(3))
		BSTreeOps.BSTInsert(T, BSTNode(7))
		BSTreeOps.BSTInsert(T, BSTNode(12))
		BSTreeOps.BSTInsert(T, BSTNode(17))

		print("Inorder Walk:")
		BSTreeOps.BSTInorderTraversal(T, T.root)
		print("Preorder Walk:")
		BSTreeOps.BSTPreorderTraversal(T, T.root)
		print("Postorder Walk:")
		BSTreeOps.BSTPostorderTraversal(T, T.root)

	def doTestDelete(self):
		# Case 1
		T = BSTree(BSTNode(5))
		z = BSTNode(3)
		BSTreeOps.BSTInsert(T, z)
		BSTreeOps.BSTInsert(T, BSTNode(7))

		print("# Delete case 1:")
		print("  before delete:")
		BSTreeOps.BSTInorderTraversal(T, T.root)
		
		BSTreeOps.BSTDelete(T, z)
		print("  after delete:")
		BSTreeOps.BSTInorderTraversal(T, T.root)

		# Case 2
		T = BSTree(BSTNode(10))
		z = BSTNode(5)
		BSTreeOps.BSTInsert(T, z)
		BSTreeOps.BSTInsert(T, BSTNode(7))

		print("# Delete case 2:")
		print("  before delete:")
		BSTreeOps.BSTInorderTraversal(T, T.root)
		
		BSTreeOps.BSTDelete(T, z)
		print("  after delete:")
		BSTreeOps.BSTInorderTraversal(T, T.root)

		# Case 3
		z = BSTNode(10)
		T = BSTree(z)
		BSTreeOps.BSTInsert(T, BSTNode(5))
		BSTreeOps.BSTInsert(T, BSTNode(15))
		BSTreeOps.BSTInsert(T, BSTNode(3))
		BSTreeOps.BSTInsert(T, BSTNode(7))
		BSTreeOps.BSTInsert(T, BSTNode(12))
		BSTreeOps.BSTInsert(T, BSTNode(17))
		BSTreeOps.BSTInsert(T, BSTNode(14))

		print("# Delete case 3:")
		print("  before delete:")
		BSTreeOps.BSTInorderTraversal(T, T.root)
		
		BSTreeOps.BSTDelete(T, z)
		print("  after delete:")
		BSTreeOps.BSTInorderTraversal(T, T.root)

#t = testBST()
#t.doTestInsert()
#t.doTestDelete()

###############################################################################
## 2. Self-Balancing Search Trees
##-----------------------------------------------------------------------------
## 2.1. AVL Trees
###############################################################################
class AVLNode:
	key: int
	height: int
	left: object
	right: object
	p: object
	def __init__(self, key: int):
		self.key = key
		self.height = 0

		self.left = None
		self.right = None
		self.p = None

class AVLTree:
	root: AVLNode
	sentinel: None
	def __init__(self, root: AVLNode):
		self.root = root
		self.sentinel = AVLNode(-1)

		# init root's attributes
		self.root = root
		self.root.p = self.sentinel
		self.root.left = self.sentinel
		self.root.right = self.sentinel

class AVLTreeOps:
	@staticmethod
	def AVLInorderTraversal(T:AVLTree, x:AVLNode):
		if x != T.sentinel:
			AVLTreeOps.AVLInorderTraversal(T, x.left)
			print("k:", x.key, ", h:", x.height)
			AVLTreeOps.AVLInorderTraversal(T, x.right)

	@staticmethod
	def AVLSearchKey(T:AVLTree, x: AVLNode, key: int):
		if x != T.sentinel:
			if x.key == key:
				return x
			elif key < x.key:
				return AVLTreeOps.AVLSearchKey(T, x.left, key)
			else:
				return AVLTreeOps.AVLSearchKey(T, x.right, key)

	@staticmethod
	def AVLInsert(T: AVLTree, z: AVLNode):
		y = T.sentinel
		x = T.root
		while x != T.sentinel:
			y = x
			if z.key <= x.key:
				x = x.left
			else:
				x = x.right
		z.p = y
		if y == T.sentinel:
			T.root = z
		elif z.key <= y.key:
			y.left = z
		else:
			y.right = z

		z.left = T.sentinel
		z.right = T.sentinel
		z.height = 0
		AVLTreeOps.AVLInsertFixup(T, z)
	
	@staticmethod
	def AVLInsertFixup(T: AVLTree, z: AVLNode):
		x = z
		while x != T.sentinel:
			AVLTreeOps.AVLHeightFixup(T, x)
			bf = AVLTreeOps.AVLBalanceFactor(T, x)
			#Case 1 and 2
			if bf == 2:
				if AVLTreeOps.AVLBalanceFactor(T, x.left) < 0:
					AVLTreeOps.AVLRotateLeft(T, x.left)
				AVLTreeOps.AVLRotateRight(T, x)
				break
			elif bf == -2:
				if AVLTreeOps.AVLBalanceFactor(T, x.right) > 0:
					AVLTreeOps.AVLRotateRight(T, x.right)
				AVLTreeOps.AVLRotateLeft(T, x)
				break
			x = x.p

	@staticmethod
	def AVLDelete(T: AVLTree, z: AVLNode):
		# do not let to delete the sentinel
		if z == None or z == T.sentinel:
			return

		# y has at least one T.sentinel as a child
		if (z.left == T.sentinel or z.right == T.sentinel):
			y = z
		else:
		# find the z's successor
			y = BSTreeOps.BSTMinValue(T, z.right)

		# x is y's only child
		if y.left != T.sentinel:
			x = y.left
		else:
			x = y.right
		
		# remove y from the parent chain
		x.p = y.p
		if y.p != T.sentinel:
			if y == y.p.left:
				y.p.left = x
			else:
				y.p.right = x
		else:
			T.root = x
		
		if y != z:
			z.key = y.key
		AVLTreeOps.AVLIDeleteFixup(T, y.p)

	@staticmethod
	def AVLIDeleteFixup(T, z):
		x = z
		while x != T.sentinel:
			AVLTreeOps.AVLHeightFixup(T, x)
			bf = AVLTreeOps.AVLBalanceFactor(T, x)
			#Case 1 and 2
			if bf == 2:
				if AVLTreeOps.AVLBalanceFactor(T, x.left) < 0:
					AVLTreeOps.AVLRotateLeft(T, x.left)
				AVLTreeOps.AVLRotateRight(T, x)
			elif bf == -2:
				if AVLTreeOps.AVLBalanceFactor(T, x.right) > 0:
					AVLTreeOps.AVLRotateRight(T, x.right)
				AVLTreeOps.AVLRotateLeft(T, x)
			x = x.p

	@staticmethod	
	def AVLGetHeight(T: AVLTree, x: AVLNode):
		if x != T.sentinel:
			return x.height
		return -1
	
	@staticmethod	
	def AVLBalanceFactor(T: AVLTree, x: AVLNode):
		lh = AVLTreeOps.AVLGetHeight(T, x.left)
		rh = AVLTreeOps.AVLGetHeight(T, x.right)
		return (lh - rh)
	
	@staticmethod
	def AVLHeightFixup(T: AVLTree, x: AVLNode):
		if x != T.sentinel:
			lh = AVLTreeOps.AVLGetHeight(T, x.left)
			rh = AVLTreeOps.AVLGetHeight(T, x.right)
			x.height = max(lh, rh) + 1

	@staticmethod
	def AVLRotateLeft(T: AVLTree, x: AVLNode):
		y = x.right
		x.right = y.left
		if y.left != T.sentinel:
			y.left.p = x
		y.p = x.p
		if x.p == T.sentinel:
			T.root = y
		elif x == x.p.left:
			x.p.left = y
		else:
			x.p.right = y
		y.left = x
		x.p = y

		AVLTreeOps.AVLHeightFixup(T, x)
		AVLTreeOps.AVLHeightFixup(T, y)
		
	@staticmethod
	def AVLRotateRight(T: AVLTree, x: AVLNode):
		y = x.left
		x.left = y.right
		if y.right != T.sentinel:
			y.right.p = x
		y.p = x.p
		if x.p == T.sentinel:
			T.root = y
		elif x == x.p.left:
			x.p.left = y
		else:
			x.p.right = y
		y.right = x
		x.p = y

		AVLTreeOps.AVLHeightFixup(T, x)
		AVLTreeOps.AVLHeightFixup(T, y)
		
class testAVL:
	def doTestInsert(self):
		T = AVLTree(AVLNode(10))
		AVLTreeOps.AVLInsert(T, AVLNode(5))
		AVLTreeOps.AVLInsert(T, AVLNode(3))
		AVLTreeOps.AVLInsert(T, AVLNode(12))
		AVLTreeOps.AVLInsert(T, AVLNode(15))
		AVLTreeOps.AVLInsert(T, BSTNode(17))

		print("Insert > Inorder Walk:")
		AVLTreeOps.AVLInorderTraversal(T, T.root)
		print("Delete(17) > Inorder Walk:")

	def doTestDelete(self):
		T = AVLTree(AVLNode(10))
		AVLTreeOps.AVLInsert(T, AVLNode(5))
		AVLTreeOps.AVLInsert(T, AVLNode(3))
		AVLTreeOps.AVLInsert(T, AVLNode(12))
		AVLTreeOps.AVLInsert(T, AVLNode(15))
		AVLTreeOps.AVLInsert(T, BSTNode(17))
		AVLTreeOps.AVLInsert(T, BSTNode(14))
		AVLTreeOps.AVLInsert(T, BSTNode(7))
		AVLTreeOps.AVLInsert(T, BSTNode(11))
		AVLTreeOps.AVLInsert(T, BSTNode(13))
		AVLTreeOps.AVLInsert(T, BSTNode(18))
		AVLTreeOps.AVLInsert(T, BSTNode(8))
		AVLTreeOps.AVLInsert(T, BSTNode(19))
		AVLTreeOps.AVLInsert(T, BSTNode(16))
		AVLTreeOps.AVLInsert(T, BSTNode(9))	

		print("# Before delete:")
		AVLTreeOps.AVLInorderTraversal(T, T.root)

		z = AVLTreeOps.AVLSearchKey(T, T.root, 7)
		AVLTreeOps.AVLDelete(T, z)
		z = AVLTreeOps.AVLSearchKey(T, T.root, 12)
		AVLTreeOps.AVLDelete(T, z)

		AVLTreeOps.AVLInorderTraversal(T, T.root)

#t = testAVL()
#t.doTestInsert()
#t.doTestDelete()

###############################################################################
## 2.2. Red-Black Trees
###############################################################################
class COLOR(enum.Enum):
	UNDEF = 0
	BLACK = 1
	RED = 2

class RBSTNode:
	key: int
	color: int

	left: object
	right: object
	p: object
	def __init__(self, key: int):
		self.key = key
		self.color = COLOR.UNDEF

		self.left = None
		self.right = None
		self.p = None

class RBSTree:
	root: RBSTNode
	sentinel: None
	def __init__(self, root: RBSTNode):
		# init sentinel node
		self.sentinel = RBSTNode(-1)
		self.sentinel.color = COLOR.BLACK

		# init root's attributes
		self.root = root
		self.root.p = self.sentinel
		self.root.left = self.sentinel
		self.root.right = self.sentinel
		self.root.color = COLOR.BLACK
		

class RBSTreeOps:
	@staticmethod
	def RBSTInorderTraversal(T:RBSTree, x:RBSTNode):
		if x != T.sentinel:
			RBSTreeOps.RBSTInorderTraversal(T, x.left)
			print("k:", x.key, ", c:", x.color)
			RBSTreeOps.RBSTInorderTraversal(T, x.right)

	@staticmethod
	def RBSTSearchKey(T:RBSTree, x: RBSTNode, key: int):
		if x != T.sentinel:
			if x.key == key:
				return x
			elif key < x.key:
				return RBSTreeOps.RBSTSearchKey(T, x.left, key)
			else:
				return RBSTreeOps.RBSTSearchKey(T, x.right, key)

	@staticmethod
	def RBSTInsert(T: RBSTree, z: RBSTNode):
		# init z's attributes
		z.p = T.sentinel
		z.left = T.sentinel
		z.right = T.sentinel
		z.color = COLOR.RED

		# insert as it would be inserted in a typical BST
		y = T.sentinel
		x = T.root
		while x != T.sentinel:
			y = x
			if z.key <= x.key:
				x = x.left
			else:
				x = x.right
		z.p = y
		if y == T.sentinel:
			T.root = z
		elif z.key <= y.key:
			y.left = z
		else:
			y.right = z
		RBSTreeOps.RBSTInsertFixup(T, z)
	
	@staticmethod
	def RBSTInsertFixup(T: RBSTree, z: RBSTNode):
		while ((z.p != T.sentinel) and (z.p.color == COLOR.RED)):
			if z.p == z.p.p.left:
				y = z.p.p.right
				if y.color == COLOR.RED:
					z.p.color = COLOR.BLACK
					y.color = COLOR.BLACK
					z.p.p.color = COLOR.RED
					z = z.p.p
				else:
					if z == z.p.right:
						z = z.p
						RBSTreeOps.RBSTRotateLeft(T, z)
					z.p.color = COLOR.BLACK
					z.p.p.color = COLOR.RED
					RBSTreeOps.RBSTRotateRight(T, z.p.p)
			else:
				y = z.p.p.left
				if y.color == COLOR.RED:
					z.p.color = COLOR.BLACK
					y.color = COLOR.BLACK
					z.p.p.color = COLOR.RED
					z = z.p.p
				else:
					if z == z.p.left:
						z = z.p
						RBSTreeOps.RBSTRotateRight(T, z)
					z.p.color = COLOR.BLACK
					z.p.p.color = COLOR.RED
					RBSTreeOps.RBSTRotateLeft(T, z.p.p)
		# make the root always black
		T.root.color = COLOR.BLACK

	@staticmethod
	def RBSTDelete(T: RBSTree, z: RBSTNode):
		# do not let to delete the sentinel
		if z == None or z == T.sentinel:
			return

		# y has at least one T.sentinel as a child
		if (z.left == T.sentinel or z.right == T.sentinel):
			y = z
		else:
		# find the z's successor
			y = RBSTreeOps.RBSTMinValue(T, z.right)

		# x is y's only child
		if y.left != T.sentinel:
			x = y.left
		else:
			x = y.right
		
		# remove y from the parent chain
		x.p = y.p
		if y.p != T.sentinel:
			if y == y.p.left:
				y.p.left = x
			else:
				y.p.right = x
		else:
			T.root = x
		
		if y != z:
			z.key = y.key

		# if the successor's original color was BLACK, then
		#  solve the doubly-black cases
		if y.color == COLOR.BLACK:
			RBSTreeOps.RBSTDeleteFixup(T, x)

	@staticmethod
	def RBSTDeleteFixup(T, x):
		while ((x != T.root) and (x.color == COLOR.BLACK)):
			if x == x.p.left:
				w = x.p.right
				#case A : adjustment
				if w.color == COLOR.RED:
					w.color = COLOR.BLACK
					x.p.color = COLOR.RED
					RBSTreeOps.RBSTRotateRight(T, x.p)
					w = x.p.right
				#case B.1 : demotion
				elif w.left.color == COLOR.BLACK and w.right.color == COLOR.BLACK:
					w.color = COLOR.RED
					x = x.p
				else:
					#case B.2 : double-rotation
					if w.right.color == COLOR.BLACK:
						w.left.color = COLOR.BLACK
						w.color = COLOR.RED
						RBSTreeOps.RBSTRotateRight(T, w)
						w = x.p.right
					#case B.3 : single-rotation
					w.color = x.p.color
					x.p.color = COLOR.BLACK
					w.right.color = COLOR.BLACK
					RBSTreeOps.RBSTRotateLeft(T, x.p)
					x = T.root
			else:
				w = x.p.left
				#case A' : adjustment
				if w.color == COLOR.RED:
					w.color = COLOR.BLACK
					x.p.color = COLOR.RED
					RBSTreeOps.RBSTRotateLeft(T, x.p)
					w = x.p.left
				#case B'.1 : demotion
				elif w.right.color == COLOR.BLACK and w.left.color == COLOR.BLACK:
					w.color = COLOR.RED
					x = x.p
				else:
					#case B'.2 : double-rotation
					if w.left.color == COLOR.BLACK:
						w.ritgh.color = COLOR.BLACK
						w.color = COLOR.RED
						RBSTreeOps.RBSTRotateLeft(T, w)
						w = x.p.left
					#case B'.3 : single-rotation
					w.color = x.p.color
					x.p.color = COLOR.BLACK
					w.left.color = COLOR.BLACK
					RBSTreeOps.RBSTRotateRight(T, x.p)
					x = T.root
		# 1. if y.p == z and z.color == RED and y.color == BLACK
		#    you end up with Red-Red combination solved if x becomes RED
		# 2. x is root and is RED, so make it black
		x.color = COLOR.BLACK

	@staticmethod
	def RBSTMinValue(T, x):
		while x.left != T.sentinel:
			x = x.left
		return x

	@staticmethod
	def RBSTRotateLeft(T: RBSTree, x: RBSTNode):
		y = x.right
		x.right = y.left
		if y.left != T.sentinel:
			y.left.p = x
		y.p = x.p
		if x.p == T.sentinel:
			T.root = y
		elif x == x.p.left:
			x.p.left = y
		else:
			x.p.right = y
		y.left = x
		x.p = y
		
	@staticmethod
	def RBSTRotateRight(T: RBSTree, x: RBSTNode):
		y = x.left
		x.left = y.right
		if y.right != T.sentinel:
			y.right.p = x
		y.p = x.p
		if x.p == T.sentinel:
			T.root = y
		elif x == x.p.left:
			x.p.left = y
		else:
			x.p.right = y
		y.right = x
		x.p = y

class tesRBST:
	def doTestInsert(self, case: int):
		if case == 1:
			# CASE A
			T = RBSTree(RBSTNode(11))
			RBSTreeOps.RBSTInsert(T, RBSTNode(9))
			RBSTreeOps.RBSTInsert(T, RBSTNode(14))
			RBSTreeOps.RBSTInsert(T, RBSTNode(3)) # CASE A
			RBSTreeOps.RBSTInsert(T, RBSTNode(10))
			RBSTreeOps.RBSTInsert(T, RBSTNode(12))
			RBSTreeOps.RBSTInsert(T, RBSTNode(16))
			RBSTreeOps.RBSTInsert(T, RBSTNode(2))
			RBSTreeOps.RBSTInsert(T, RBSTNode(4))
			RBSTreeOps.RBSTInsert(T, RBSTNode(1))
	
		elif case == 2:
			# CASE B ( 1&2 )
			T = RBSTree(RBSTNode(3))
			RBSTreeOps.RBSTInsert(T, RBSTNode(1))
			RBSTreeOps.RBSTInsert(T, RBSTNode(2))
				
	def doTestDelete(self, testNo: int):
		if testNo == 1:
			T = RBSTree(RBSTNode(2))
			RBSTreeOps.RBSTInsert(T, RBSTNode(1))
			RBSTreeOps.RBSTInsert(T, RBSTNode(4))
			RBSTreeOps.RBSTInsert(T, RBSTNode(5))
			#for i in range (2, 10):
			#	RBSTreeOps.RBSTInsert(T, RBSTNode(i))
	
			print("Removing all...")
			z = RBSTreeOps.RBSTSearchKey(T, T.root, 4)
			RBSTreeOps.RBSTDelete(T, z)
			z = RBSTreeOps.RBSTSearchKey(T, T.root, 5)
			RBSTreeOps.RBSTDelete(T, z)
			z = RBSTreeOps.RBSTSearchKey(T, T.root, 2)
			RBSTreeOps.RBSTDelete(T, z)
			z = RBSTreeOps.RBSTSearchKey(T, T.root, 1)
			RBSTreeOps.RBSTDelete(T, z)
			print("Adding 3,2,1...")
			#T.root.left.color = COLOR.RED
			RBSTreeOps.RBSTInsert(T, RBSTNode(3))
			RBSTreeOps.RBSTInsert(T, RBSTNode(2))
			RBSTreeOps.RBSTInsert(T, RBSTNode(1))
			print("Done test 1")
		elif testNo == 2:
			T = RBSTree(RBSTNode(1))
			for i in range (2, 12):
				RBSTreeOps.RBSTInsert(T, RBSTNode(i))

			z = RBSTreeOps.RBSTSearchKey(T, T.root, 2)
			RBSTreeOps.RBSTDelete(T, z)
			print("Done test 2")
#t = tesRBST()
#t.doTestInsert(1)
#t.doTestInsert(2)
#t.doTestDelete(1)
#t.doTestDelete(2)
