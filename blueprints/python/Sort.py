###############################################################################
## 1 Comparison Sort 
##	1.1 Insertion-Sort
##	1.2 Merge-Sort
##	1.3 Quicksort
##	1.4 Heapsort
## 2 Distribution Sort 
## 	2.1 Counting-Sort
##	2.2 Radix-Sort
##	2.3 Bucket-Sort
###############################################################################
import enum
import math

###############################################################################
## 1 Sorting Algorithms
##-----------------------------------------------------------------------------
## 1.1 Insertion-Sort
###############################################################################
class InsertionSort:
	@staticmethod
	def sort(a: list):
		for j in range (1, len(a)):
			key = a[j]
			i = j - 1
			while i >= 0 and a[i] > key:
				a[i+1] = a[i]
				i = i-1
			a[i+1] = key
		return a

class testInsertionSort:
	def doTest(self):
		a = [4, 3, 2, 1, 5]
		b = InsertionSort.sort(a)
		print(b)

#t = testInsertionSort()
#t.doTest()

###############################################################################
## 1.2 Merge-Sort
###############################################################################
class MergeSort:
	@staticmethod
	def sort(a: list, low: int, high: int):
		if low < high:
			middle = int( math.floor((low+high)/2) )
			MergeSort.sort(a, low, middle)
			MergeSort.sort(a, middle + 1, high)
			MergeSort.merge(a, low, middle, high)


	@staticmethod
	def merge(a: list, low: int, middle: int, high: int):
		L = a[low : middle + 1]
		R = a[middle + 1 : high + 1]
		i = 0
		j = 0
		for k in range(low, high+1):
			if ((j == len(R)) or
				(i < len(L) and j < len(L) and L[i] < R[j])):
				a[k] = L[i]
				i += 1
			else:
				a[k] = R[j]
				j += 1

	@staticmethod
	def debug(a, prefix, low, high):
		print(prefix, end="")
		for i in range (low, high + 1):
			print(a[i], ", ", end="")
		print()

class testMergeSort:
	def doTest1(self):
		a = [3, 2, 1]
		MergeSort.sort(a, 0, len(a)-1)
		print(a)

	def doTest2(self):
		a = [7, 8, 1, 5, 2, 3]
		MergeSort.sort(a, 0, len(a)-1)
		print(a)

#t = testMergeSort()
#t.doTest1()
#t.doTest2()

###############################################################################
## 1.3 Quicksort
###############################################################################
class Quicksort:
	@staticmethod
	def sort(a: list, low: int, high: int):
		if low < high:
			pivot = Quicksort.partition(a, low, high)
			Quicksort.sort(a, low, pivot - 1)
			Quicksort.sort(a, pivot + 1, high)

	@staticmethod
	def partition(a: list, low: int, high: int):
		pivot = a[high]
		i = low - 1
		for j in range(low, high):
			if a[j] >= pivot:
				i += 1
				a[i], a[j] = a[j], a[i]
		a[i+1], a[high] = a[high], a[i+1]
		return (i + 1)

class testQuicksort:
	def doTest0(self):
		a = [1, 2, 3]
		Quicksort.sort(a, 0, len(a)-1)
		print(a)
	
	def doTest1(self):
		a = [3, 1, 2]
		Quicksort.sort(a, 0, len(a)-1)
		print(a)

	def doTest2(self):
		a = [7, 8, 1, 5, 2, 3]
		Quicksort.sort(a, 0, len(a)-1)
		print(a)

t = testQuicksort()
#t.doTest0()
#t.doTest1()
#t.doTest2()


###############################################################################
## 1.4 Heapsort
###############################################################################
class heapType(enum.Enum):
	MIN = 0
	MAX = 1

class Heap:
	'The heap'
	type: heapType
	heap: list
	size: int
	
	def __init__(self, heapType: type, heap: list):
		self.type = heapType
		self.heap = heap
		self.size = self.heap.__len__()

	def __left(self, idx: int):
		return 2*idx + 1

	def __right(self, idx: int):
		return 2*idx + 2

	def __parent(self, idx: int):
		return int(idx/2 + 0.5) - 1

	def swap(self, i: int, j: int):
		auxItem = self.heap[i]
		self.heap[i] = self.heap[j]
		self.heap[j] = auxItem

	def heapifyRise(self, idx: int):
		p = self.__parent(idx)
		if (p < 0):
			return;

		if ((type == heapType.MIN and # min heap
			 self.heap[p] > self.heap[idx]) or
			(type == heapType.MAX and # max heap
			 self.heap[p] < self.heap[idx])):

			# continue Heapify...
			self.swap(idx, p)
			self.heapifyRise(p)


	def heapifySink(self, idx: int):
		l = self.__left(idx)
		r = self.__right(idx)
		swIdx = idx

		if (type == heapType.MIN): # min heap
			# smallest: swIdx
			if (l < self.size and self.heap[l] < self.heap[swIdx]):
				swIdx = l
		
			if (r < self.size and self.heap[r] < self.heap[swIdx]):
				swIdx = r

		else: # max heap
			# biggest: swIdx
			if (l < self.size and self.heap[l] > self.heap[swIdx]):
				swIdx = l
		
			if (r < self.size and self.heap[r] > self.heap[swIdx]):
				swIdx = r

		# continue Heapify...
		if (swIdx != idx):
			self.swap(idx, swIdx)
			self.heapifySink(swIdx)
	
	def insertHeap(self, item):
		self.heap.append(item)
		self.size = self.size + 1
		self.heapifyRise(self.size)
		
	def buildHeap(self):
		for idx in range (self.size//2, -1, -1):
			self.heapifySink(idx)

	def printHeap(self):
		for idx in range (0, self.size+1, 1):
			print("(", self.heap[idx], ",", self.heapPrios[idx],")", end="")
		print("")

	def printHeapTree(self):
		for idx in range (0, self.size, 1):
			l = self.__left(idx)
			r = self.__right(idx)
			print("(", self.heap[idx], ")")
			if (l < self.size):
				print("    (", self.heap[l],")", end="")
			if (r < self.size):
				print("    (", self.heap[r], ")", end="")
			print("")

class heapSort:
	'The Heapsort'
	def doSort(self, items: list):
		h = Heap(heapType.MAX, items)
		h.buildHeap()
		for idx in range (h.size-1, 0, -1):
			h.swap(0, idx)
			h.size -= 1
			h.heapifySink(0)
		return h.heap

class TestHeap:
	def doTestHeap(self):
		items = [ 1, 3, 5, 15, 16, 8, 7]
		h = Heap(heapType.MAX, items)
		h.buildHeap()
		h.printHeapTree()

#t = TestHeap()
#t.doTestHeap()

class TestHeapsort:
	def doTest(self):
		items = [ 1, 3, 5, 15, 16, 8, 7]
		print(" Initial set:")
		for item in items:
			print(item, ",", end="")

		s = heapSort()
		sortedItems = s.doSort(items)

		print()
		print(" Sorted set:")
		for item in sortedItems:
			print(item, ",", end="")
		print("")

#t = TestHeapsort()
#t.doTest()

###############################################################################
## 2 Distribution Sort 
##-----------------------------------------------------------------------------
## 2.1 Counting-Sort
###############################################################################
class CountingSort:
	@staticmethod
	def sort(a: list, k: int):
		counter = [0] * k
		# count how many values of a[i] are
		# Note: a[i] > 0, otherwise it will fail
		for i in range(0, len(a)):
			counter[a[i]] +=1

		idx = 0
		for i in range(0, len(counter)):
			while counter[i] > 0:
				a[idx] = i
				idx +=1
				counter[i] -= 1

class TestCountingSort:
	def doTest(self):
		a = [ 3, 1, 5, 1, 7]
		CountingSort.sort(a, 10)
		print(a)

#t = TestCountingSort()
#t.doTest()

###############################################################################
## 2.2 Radix-Sort
###############################################################################
class RadixSort:
	@staticmethod
	def sort(a: list, msd: int):
		sortedA = [0] * len(a)
		radix = 10
		dIdx = 0
		exp = 1
		while dIdx <= msd:
			bucket = [0] * radix
			
			for i in range(0, len(a)):
				digit = int(a[i]/exp % radix)
				bucket[digit] += 1
				
			for i in range(1, radix):
				bucket[i] = bucket[i] + bucket[i-1]
		
			for i in range(len(a)-1, -1, -1):
				digit = int(a[i]/exp % radix)
				sortedA[bucket[digit] - 1] = a[i]
				bucket[digit] -= 1
			
			for i in range(0, len(a)):
				a[i] = sortedA[i]
			
			exp *= radix
			dIdx += 1

class TestRadixSort:
	def doTest(self):
		a = [ 345, 95, 71, 102 ]
		RadixSort.sort(a, 2)
		print(a)

#t = TestRadixSort()
#t.doTest()

###############################################################################
## 2.3 Bucket-Sort
###############################################################################
class BucketSort:
	@staticmethod
	def sort(a: list, m: int):
		n = len(a)
		bucket = [[] for _ in range(n)]
		maxIdx = n - 1
		for i in range(n):
			idx = int( math.floor((maxIdx*a[i])/m) )
			bucket[idx].append(a[i])
		
		idx = 0
		for i in range (0, n):
			InsertionSort.sort(bucket[i])
			
			for j in range(0, len(bucket[i])):
				a[idx] = bucket[i][j]
				idx +=1

class TestBucketSort:
	def doTest(self):
		a = [ 9, 3, 8, 6, 1 ]
		BucketSort.sort(a, 10)
		print(a)

#t = TestBucketSort()
#t.doTest()
