###############################################################################
## 1. Prerequisite: Stacks, Queues, Priority Queues, Disjoint Sets
## 2. Representations of Graphs based on linked lists
## 3. Methods for Traversing Graphs
##	BFS and DFS
## 4. Connected Components
##	Connected Components in undirected graphs
##	Kosaraju\'s algorithm for Strongly Connected Components
## 5. Minimum Spanning Trees
##	Prim's and Kruskal algorithms
## 6. Shortest-Paths
##	Bellman-Ford and Dijkastra	
## 7. Maximum Flow
##	Edmonds-Karp
###############################################################################
from builtins import int, dict

###############################################################################
## 1. Prerequisite: Stacks, Queues, Priority Queues, Disjoint Sets
###############################################################################

class Stack:
	'Standard Stack'
	stack: list

	def __init__(self):
		self.stack = []
	
	def push(self, item):
		self.stack.append(item)

	def pop(self):
		return self.stack.pop()

	def isEmpty(self):
		if len(self.stack) == 0:
			return True
		return False

class Queue:
	'Standard Queue'
	queue: list

	def __init__(self):
		self.queue = []

	def enqueue(self, item):
		self.queue.append(item)

	def dequeue(self):
		return self.queue.pop(0)
	
	def clear(self):
		self.queue.clear()

	def isEmpty(self):
		if len(self.queue) == 0:
				return True
		return False

class MinPrioQueue:
	'Minimum-Priority-Queue implemented with heap'
	heapItems: list
	heapPrios: list
	heapSize: int
	
	def __init__(self):
		self.heapItems = []
		self.heapPrios = []
		self.heapSize = - 1

	def __left(self, idx: int):
		return 2*idx + 1

	def __right(self, idx: int):
		return 2*idx + 2

	def __parent(self, idx: int):
		return int(idx/2 + 0.5) - 1

	def __swap(self, i: int, j: int):
		auxPrio = self.heapPrios[i]
		auxItem = self.heapItems[i]

		self.heapPrios[i] = self.heapPrios[j]
		self.heapItems[i] = self.heapItems[j]
		
		self.heapPrios[j] = auxPrio
		self.heapItems[j] = auxItem

	def __minHeapifyRise(self, idx: int):
		p = self.__parent(idx)

		smallest = idx	   
		if (p >= 0 and self.heapPrios[p] > self.heapPrios[smallest]):
			smallest = p
		
			if (smallest != idx):
				self.__swap(idx, smallest)
				self.__minHeapifyRise(smallest)
			
	def __minHeapifySink(self, idx: int):
		l = self.__left(idx)
		r = self.__right(idx)
		
		smallest = idx
		if (l <= self.heapSize and self.heapPrios[l] < self.heapPrios[smallest]):
			smallest = l
	
		if (r <= self.heapSize and self.heapPrios[r] < self.heapPrios[smallest]):
			smallest = r

		if (smallest != idx):
			self.__swap(idx, smallest)
			self.__minHeapifySink(smallest)
	
	def insert(self, item, p: int):
		self.heapItems.append(item)
		self.heapPrios.append(p)
		self.heapSize = self.heapSize + 1
		self.__minHeapifyRise(self.heapSize)

	def extractMin(self):
		minPrios = self.heapPrios[0]
		minItem = self.heapItems[0]

		lastLeafPrio = self.heapPrios.pop()
		lastLeafItem = self.heapItems.pop()
		self.heapSize = self.heapSize - 1
		
		if self.heapSize >= 0:
			self.heapPrios[0] = lastLeafPrio
			self.heapItems[0] = lastLeafItem
			self.__minHeapifySink(0)
		
		return (minItem, minPrios)

	def decreasePrio(self, item, p: int):
		idxItem = self.heapItems.index(item)
		self.heapPrios[idxItem] = p
		
		self.__minHeapifyRise(idxItem)

	def isEmpty(self):
		if len(self.heapItems) == 0:
			return True
		return False

	def printHeap(self):
		for idx in range (0, self.heapSize+1, 1):
			print("(", self.heapItems[idx], ",", self.heapPrios[idx],")", end="")
		print("")

	def printHeapTree(self):
		for idx in range (0, self.heapSize + 1, 1):
			l = self.__left(idx)
			r = self.__right(idx)
			print("(", self.heapItems[idx], ",", self.heapPrios[idx],")")
			if (l <= self.heapSize):
				print("	(", self.heapItems[l], ",", self.heapPrios[l],")", end="")
			if (r <= self.heapSize):
				print("	(", self.heapItems[r], ",", self.heapPrios[r],")", end="")
			print("")

class testMinPrioQueue:
	def doTestInsertRemove(self):
		pQ = MinPrioQueue()
		pQ.insert(0, 3)
		pQ.printHeap()
		pQ.insert(1, 7)
		pQ.printHeap()
		pQ.insert(2, 12)
		pQ.printHeap()
		pQ.insert(3, 1)
		pQ.printHeap()
		pQ.insert(4, 15)
		pQ.printHeap()
		pQ.insert(5, 8)
		pQ.printHeap()
		pQ.extractMin()
		pQ.printHeap()
		pQ.extractMin()
		pQ.printHeap()
		pQ.extractMin()
		pQ.printHeap()
		pQ.extractMin()
		pQ.printHeap()
		pQ.extractMin()
		pQ.printHeap()

	def doTestDecreasePrio(self):
		pQ = MinPrioQueue()
		pQ.insert(0, 3)
		pQ.insert(1, 7)
		pQ.insert(2, 12)
		pQ.insert(3, 1)
		pQ.insert(4, 15)
		pQ.insert(5, 8)
		pQ.printHeap()
		pQ.decreasePrio(4, 0)
		pQ.printHeap()
		pQ.printHeapTree()

#t = testMinPrioQueue()
#t.doTestInsertRemove()
#t.doTestDecreasePrio()

class DisjointSet:
	'Disjoint-set with path compression: union-by-rank'
	items: list
	parent: dict
	rank: dict

	def __init__(self, items: list):
		self.parent = dict()
		self.rank = dict()
		self.itemsLists = items
		for item in self.itemsLists:
			self.parent[item] = item
			self.rank[item] = 0
		
	def find(self, v):
		if self.parent[v] != v:
			self.parent[v] = self.find(self.parent[v])
		return self.parent[v]

	def union(self, u, v):
		uid = self.find(u)
		vid = self.find(v)
		if self.rank[uid] > self.rank[vid]:
			self.parent[vid] = vid
		else:
			self.parent[uid] = vid
			if self.rank[uid] == self.rank[vid]:
				self.rank[vid] = self.rank[vid]+1
	
	def printDisjointSet(self):
		for item in self.itemsLists:
			print("item(", item, "): parent(", 
				  self.parent[item],"), rank(", self.rank[item], ")")

###############################################################################
## 2. Representations of Graphs 
###############################################################################
class Graph:
	'Graph representation with adjacency list'
	# Note: labels of the graph's vertices must be different

	vList: list
	eList: list
	adjList: list

	eListR: list

	# edges are given as a list of tuples: [ (u1,u2),(u3,u4), ... ]
	# where u1, u2, u3, ... are labels (int)
	def __init__(self, vList: list, eList: list):
		self.adjList = []

		self.vList = vList
		self.__initAdjLists(vList)

		self.eList = eList
		self.__populateAdjList(eList)

		self.eListR = []
		self.__buildReverseEdgesList()

	def __initAdjLists(self, vList: list):
		for u in vList:
			self.adjList.append((u, []))

	def __populateAdjList(self, eList: list): 
		for uv in eList:
			u = uv[0]
			v = uv[1]
			uList = self.getVertexAdjList(u)
			uList.append(v)
	
	def __buildReverseEdgesList(self):
		for uv in self.eList:
			vu = (uv[1], uv[0])
			self.eListR.append(vu)
	
	def getVertexAdjList(self, u: int):
		for adjTuple in self.adjList:
			v = adjTuple[0]
			vList = adjTuple[1]
			if v == u:
				return vList
		return None

	def addEdge(self, e):
		self.eList.append(e)
		u = e[0]
		v = e[1]
		uList = self.getVertexAdjList(u)
		uList.append(v)

		self.eListR.append((v,u))

	def removeEdge(self, e):
		self.eList.remove(e)
		u = e[0]
		v = e[1]
		uList = self.getVertexAdjList(u)
		uList.remove(v)

		self.eListR.remove((v,u))

	def printAdjList(self):
		for adjTuple in self.adjList:
			u = adjTuple[0]
			uList = adjTuple[1]
			
			print("vertex: ", u, ": ", end="")
			for v in uList:
				print(v, " ,", end="")
			print()
		print("-------------------------------")

###############################################################################
## 3. Methods for Traversing Graphs
###############################################################################
class GraphTraversal:
	'BFS and DFS recursive and iterative implementations'
	def BFS(self, g: Graph, s):
		marked = dict()
		pred = dict()
		for v in g.vList:
			marked[v] = False
			pred[v] = None
		Q = Queue()
		
		Q.enqueue(s)
		pred[s] = s
		marked[s] = True

		while (Q.isEmpty() == False):
			u = Q.dequeue()
			uList = g.getVertexAdjList(u)
			for v in uList:
				if marked[v] == False:
					marked[v] = True
					pred[v] = u
					Q.enqueue(v)
		return pred

	def DFSR(self, g: Graph, s):
		marked = dict()
		pred = dict()
		for v in g.vList:
			marked[v] = False
			pred[v] = None
		
		self.DFSRecursive(g, s, marked, pred)

		return pred

	def DFSRecursive(self, g: Graph, u, marked: dict, pred: dict):
		marked[u] = True
		uList = g.getVertexAdjList(u)
		for v in uList:
			if marked[v] == False:
				pred[v] = u
				self.DFSRecursive(g, v, marked, pred)

	def DFSI(self, g:Graph, s: int):
		marked = dict()
		pred = dict()
		for v in g.vList:
			marked[v] = False
			pred[v] = None
		S = Stack()

		pred[s] = s
		S.push(s)
		while (S.isEmpty() == False):
			u = S.pop()
			if marked[u] == False:
				marked[u] = True
				uList = g.getVertexAdjList(u)
				for v in uList:
					if marked[v] == False:
						S.push(v)
						pred[v] = u

		return pred

	def printPredecessorTree(self, pred: dict):
		print("The predecessor tree:")
		for u, p in pred.items() :
			print("(" , u, ", pred: ", p, ")") 

class testTraversal:
	def doTestBFS(self):
		verticesList = [1, 2, 3, 4, 5]
		edgesList = [
			(1,2), (1,4),
			(2,1), (2,3), (2,4),
			(3,2), (3,5),
			(4,1), (4,2), (4,5),
			(5,3), (5,4),
			]
		g = Graph(verticesList, edgesList)
		t = GraphTraversal()
		pred = t.BFS(g, 2)
		t.printPredecessorTree(pred)

	def doTestDFSRecursive(self):
		verticesList = [1, 2, 3, 4, 5]
		edgesList = [
			(1,2), (1,4),
			(2,1), (2,3), (2,4),
			(3,2), (3,5),
			(4,1), (4,2), (4,5),
			(5,3), (5,4),
			]
		g = Graph(verticesList, edgesList)
		t = GraphTraversal()
		pred = t.DFSR(g, 2)
		t.printPredecessorTree(pred)

	def doTestDFSIterrative(self):
		verticesList = [1, 2, 3, 4, 5]
		edgesList = [
			(1,4), (1,2),
			(2,4), (2,3), (2,1),
			(3,5), (3,2),
			(4,5), (4,2), (4,1),
			(5,4), (5,3),
			]
		
		g = Graph(verticesList, edgesList)
		t = GraphTraversal()
		pred = t.DFSI(g, 2)
		t.printPredecessorTree(pred)

#t = testTraversal()
#t.doTestBFS()
#t.doTestDFSRecursive()
#t.doTestDFSIterrative()

###############################################################################
## 4. Connected Components
###############################################################################
class CC:
	g: Graph
	Q: Queue
	marked: dict
	cc: dict

	def BFS(self, s: int, ccId: int):
		self.Q.enqueue(s)
		self.marked[s] = True
		self.cc[s] = ccId

		while (self.Q.isEmpty() == False):
			u = self.Q.dequeue()
			uList = self.g.getVertexAdjList(u)
			for v in uList:
				if self.marked[v] == False:
					self.marked[v] = True
					self.cc[v] = ccId
					self.Q.enqueue(v)

	def ccBFS(self, g):
		self.g = g
		self.Q = Queue()
		self.cc = dict()

		self.marked = dict()
		for v in self.g.vList:
			self.marked[v] = False
		
		ccId = 1
		for adjTuple in self.g.adjList:
			v = adjTuple[0]
			if self.marked[v] == False:
				self.BFS(v, ccId)
				ccId = ccId + 1

		return self.cc

	def ccUF(self, g: Graph):
		S = DisjointSet(g.vList)
		for e in g.eList:
			u = e[0]
			v = e[1]
			if (S.find(u) != S.find(v)):
				S.union(u, v)

		return S

	def printCCBFS(self):
		for adjTuple in self.g.adjList:
			v = adjTuple[0]
			print("vertex:", v, " - cc :", self.cc[v])

class testCC:
	def doTestCCBFS(self):
		verticesList = [1, 2, 3, 4, 5, 6, 7]
		edgesList = [
			(1,2), (1,4),
			(2,1), (2,3), (2,4),
			(3,2), (3,5),
			(4,1), (4,2), (4,5),
			(5,3), (5,4),
			(6,7),
			(7,6),
			]

		g = Graph(verticesList, edgesList)
		
		ccUG = CC()
		ccUG.ccBFS(g)
		ccUG.printCCBFS()

	def doTestCCUF(self):
		verticesList = [1, 2, 3, 4, 5, 6, 7]
		edgesList = [
			(1,2), (1,4),
			(2,1), (2,3), (2,4),
			(3,2), (3,5),
			(4,1), (4,2), (4,5),
			(5,3), (5,4),
			(6,7),
			(7,6),
			]

		g = Graph(verticesList, edgesList)
		
		ccUG = CC()
		S = ccUG.ccUF(g)
		S.printDisjointSet()

#t = testCC()
#t.doTestCCBFS()
#t.doTestCCUF()

class SCC:
	'Kosaraju\'s algorithm for Strongly Connected Components'
	S: Stack
	cc = dict

	markedTS: dict
	def __DFSTS(self, g:Graph, u: int):
		self.markedTS[u] = True
		uList = g.getVertexAdjList(u)
		for v in uList:
			if self.markedTS[v] == False:
				self.__DFSTS(g, v)

		# The stack will contain the vertices ordered
		# by the finishing time.
		self.S.push(u)

	def topologicalSort(self, g: Graph):
		self.markedTS = dict()
		for v in g.vList:
			self.markedTS[v] = False

		for v in g.vList:
			if self.markedTS[v] == False:
				self.__DFSTS(g, v)

	markedSCC:dict
	def __DFSSCC(self, g:Graph, u: int, ccId: int):
		self.markedSCC[u] = True
		self.cc[u] = ccId

		uList = g.getVertexAdjList(u)
		for v in uList:
			if self.markedSCC[v] == False:
				self.__DFSSCC(g, v, ccId)

	def scc(self, g: Graph):
		self.S = Stack()
		self.cc = dict()

		self.markedSCC = dict()
		for v in g.vList:
			self.markedSCC[v] = False

		gr = Graph(g.vList, g.eListR)
		self.topologicalSort(gr)
		
		ccId = 1
		while self.S.isEmpty() == False:
			v = self.S  .pop()
			
			if self.markedSCC[v] == False:
				self.__DFSSCC(g, v, ccId)
				ccId = ccId + 1
		return self.cc

	def printSCCs(self, cc: dict):
		for v in cc:
			print("vertex:", v, " - scc:", cc[v])

class testSCC:
	def doTest(self):
		verticesList = [1, 2, 3, 4, 5, 6, 7, 8]
		edgesList = [
			(1,2),
			(2,3), (2,4),
			(3,1),
			(4,5),
			(5,4),
			(6,7),
			(7,8),
			(8,6),
			]
		g = Graph(verticesList, edgesList)
		
		scc = SCC()
		cc = scc.scc(g)
		scc.printSCCs(cc)

#t = testSCC()
#t.doTest()

###############################################################################
## 5. Minimum Spanning Trees 
###############################################################################
class MST:
	MAX_INT = 0xffffffff

	def prim(self, g: Graph, w: dict, s):
		Q = MinPrioQueue()
		sMst = dict()
		pred = dict()
		kw = dict()

		for v in g.vList:
			sMst[v] = False
			pred[v] = v			
			kw[v] = 0 if v == s else self.MAX_INT
			Q.insert(v, kw[v])
		
		while Q.isEmpty() == False:
			t = Q.extractMin()
			u = t[0] # item = t[0], priority = t[1]
			sMst[u] = True
			for v in g.getVertexAdjList(u):
				if sMst[v] == False and kw[v] > w[(u,v)]:
					kw[v] = w[(u,v)]
					Q.decreasePrio(v, kw[v])
					pred[v] = u
		return (pred, kw)

	def kruscal(self,g :Graph, w: dict):
		T = []
		S = DisjointSet(g.vList)
		Q = MinPrioQueue()
		for e in g.eList:
			Q.insert(e, w[e])
		
		while Q.isEmpty() == False:
			t = Q.extractMin() # queue's tuple : (e, p) -> e=(u,v), p=w[e]
			e = t[0]
			u = e[0]
			v = e[1]
			if S.find(u) != S.find(v):
				S.union(u, v)
				T.append((e , w[e]))

		return T

	def printPrimMST(self, g: Graph, pred:dict, kw: dict):
		print("The MST:")
		for v in g.vList:
			print("(" , v, ", pred: ", pred[v], "):", kw[v])

	def printKruskalMST(self, T: list):
		print("The MST:")
		for t in T:
			e = t[0]
			w = t[1]
			print("(" , e[0], ",", e[1], "):", w)

class testMST:
	def doTestPrim(self):
		verticesList = ['s', 'a', 'b', 'c', 'd' ,'t']
		edgesList = [
			('s','a'), ('s','c') ,
			('a','s'), ('a','b'), ('a','c'),
			('b','a'), ('b','c'), ('b','c'), ('b','t'),
			('c','s'), ('c','a'), ('c','b'), ('c','d'),
			('d','b'), ('d','c'), ('d','t'),
			('t','b'), ('t','d') ,
			]
		w = {
			('s','a'):3, ('s','c'):2 ,
			('a','s'):3, ('a','b'):1, ('a','c'):2,
			('b','a'):1, ('b','c'):1, ('b','c'):3, ('b','t'):2,
			('c','s'):2, ('c','a'):2, ('c','b'):1, ('c','d'):1,
			('d','b'):3, ('d','c'):1, ('d','t'):1,
			('t','b'):2, ('t','d'):3
			}
		
		g = Graph(verticesList, edgesList)
		g.printAdjList()
		mstP = MST()
		r = mstP.prim(g, w, 's')
		mstP.printPrimMST(g, r[0], r[1])

	def doTestKruskal(self):
		verticesList = ['s', 'a', 'b', 'c', 'd' ,'t']
		edgesList = [
			('s','a'), ('s','c') ,
			('a','s'), ('a','b'), ('a','c'),
			('b','a'), ('b','c'), ('b','c'), ('b','t'),
			('c','s'), ('c','a'), ('c','b'), ('c','d'),
			('d','b'), ('d','c'), ('d','t'),
			('t','b'), ('t','d') ,
			]
		w = {
			('s','a'):3, ('s','c'):2 ,
			('a','s'):3, ('a','b'):1, ('a','c'):2,
			('b','a'):1, ('b','c'):1, ('b','c'):3, ('b','t'):2,
			('c','s'):2, ('c','a'):2, ('c','b'):1, ('c','d'):1,
			('d','b'):3, ('d','c'):1, ('d','t'):1,
			('t','b'):2, ('t','d'):3
			}
		
		g = Graph(verticesList, edgesList)
		g.printAdjList()
		mstP = MST()
		T = mstP.kruscal(g, w)
		mstP.printKruskalMST(T)

#t = testMST()
#t.doTestPrim()
#t.doTestKruskal()

###############################################################################
## 6. Shortest-Paths
###############################################################################
class SSSPUndirected:
	MAX_INT = 0xffffffff

	def BFS(self, g: Graph, s):
		marked = dict()
		pred = dict()
		dist = dict()
		for v in g.vList:
			marked[v] = False
			pred[v] = None
			dist[v] = self.MAX_INT
		Q = Queue()
		
		Q.enqueue(s)
		pred[s] = s
		marked[s] = True
		dist[s] = 0

		while (Q.isEmpty() == False):
			u = Q.dequeue()
			uList = g.getVertexAdjList(u)
			for v in uList:
				if marked[v] == False:
					marked[v] = True
					pred[v] = u
					dist[v] = dist[u] + 1
					Q.enqueue(v)
		return pred

	def printSSSP(self, g: Graph, pred:dict,  s):
		print("The Single-Source Shortest-Paths for undirected graphs:")
		for v in g.vList:
			if pred[v] != v:
				print("(" , pred[v], ",", v, ")")

class testSSSPUndirected:
	def doTest(self):
		verticesList = ['1', '2', '3', '4', '5']
		edgesList = [
				('1','2'), ('1','3'),
				('2','1'), ('2','3'), ('2','4'),
				('3','2'), ('3','5'),
				('4','1'), ('4','2'), ('4','5'),
				('5','3'), ('5','4'),
				]
			
		g = Graph(verticesList, edgesList)
		sssp = SSSPUndirected()
		pred = sssp.BFS(g, '2')
		sssp.printSSSP(g, pred, '2')

#t = testSSSPUndirected()
#t.doTest()

class SSSPDirected:
	MAX_INT = 0xffffffff

	def relaxEdge(self, e, w, dist, pred):
		u = e[0]
		v = e[1]
		if dist[v] > dist [u] + w[e]:
			dist[v] = dist[u] + w[e]
			pred[v] = u
			return True
		return False

	def bellmanFord(self, g: Graph, w: dict, s):
		pred = dict()
		dist = dict()

		for v in g.vList:
			pred[v] = v			
			dist[v] = self.MAX_INT
		dist[s] = 0

		for i in range (0, g.vList.__len__(), 1):
			for e in g.eList:
				self.relaxEdge(e, w, dist, pred)
		
		for e in g.eList:
			if self.relaxEdge(e, w, dist, pred) == True:
				print("Error: G contains negative cycles !")

		return pred

	def dijkstra(self, g: Graph, w: dict, s):
		ssspInclude = dict()
		Q = MinPrioQueue()
		pred = dict()
		dist = dict()

		for v in g.vList:
			ssspInclude[v] = False
			pred[v] = v			
			dist[v] = 0 if v == s else self.MAX_INT
			Q.insert(v, dist[v])

		while Q.isEmpty() == False:
			t = Q.extractMin()
			u = t[0] # item = t[0], priority = t[1]
			ssspInclude[u] = True
			for v in g.getVertexAdjList(u):
				if ssspInclude[v] == False:
					e = (u,v)
					if self.relaxEdge(e, w, dist, pred) == True:
						Q.decreasePrio(v, dist[v])
	
		return pred

	def printSSSP(self, g: Graph, pred:dict, w: dict, s):
		print("The Bellman-Ford Single-Source Shortest-Paths:")
		for v in g.vList:
			if pred[v] != v:
				print("(" , pred[v], ",", v, "):", w[(pred[v],v)])


class testSSSP:
	def doTestBellmanFord(self):
		verticesList = ['s', 'a', 'b', 'c', 'd' ,'t']
		edgesList = [
			('s','a'), ('s','c'),
			('a','b'),
			('b','d'), ('b','t'),
			('c','a'), ('c','b'),
			('d','c'), ('d','t'),
			]
		w = {
			('s','a'):3 , ('s','c'):-2,
			('a','b'):1 ,
			('b','d'):-1, ('b','t'):2,
			('c','a'):2 , ('c','b'):1,
			('d','c'):1 , ('d','t'):1,
			}
		
		g = Graph(verticesList, edgesList)
		sssp = SSSPDirected()
		pred = sssp.bellmanFord(g, w, 's')
		sssp.printSSSP(g, pred, w, 's')

	def doTestDijkastra(self):
		verticesList = ['s', 'a', 'b', 'c', 'd' ,'t']
		edgesList = [
			('s','a'), ('s','c'),
			('a','b'),
			('b','d'), ('b','t'),
			('c','a'), ('c','b'),
			('d','c'), ('d','t'),
			]
		w = {
			('s','a'):3, ('s','c'):2,
			('a','b'):1,
			('b','d'):1, ('b','t'):2,
			('c','a'):2, ('c','b'):1,
			('d','c'):1, ('d','t'):1,
			}
		
		g = Graph(verticesList, edgesList)
		sssp = SSSPDirected()
		pred = sssp.dijkstra(g, w, 's')
		sssp.printSSSP(g, pred, w, 's')

	def doTestDijkastra2(self):
		verticesList = ['1', '2', '3', '4', '5']
		edgesList = [
				('1','2'), ('1','3'),
				('2','1'), ('2','3'), ('2','4'),
				('3','2'), ('3','5'),
				('4','1'), ('4','2'), ('4','5'),
				('5','3'), ('5','4'),
				]
		w = {
				('1','2'):1, ('1','3'):1,
				('2','1'):1, ('2','3'):1, ('2','4'):1,
				('3','2'):1, ('3','5'):1,
				('4','1'):1, ('4','2'):1, ('4','5'):1,
				('5','3'):1, ('5','4'):1,
			}
		
		g = Graph(verticesList, edgesList)
		sssp = SSSPDirected()
		pred = sssp.dijkstra(g, w, '2')
		sssp.printSSSP(g, pred, w, '2')

#t = testSSSP()
#t.doTestBellmanFord()
#t.doTestDijkastra()
#t.doTestDijkastra2()

###############################################################################
## 7. Maximum Flow
###############################################################################
class flowNetwork:
	g: Graph
	c: dict
	def __init__(self, g: Graph, s, t, c: dict):
		self.g = g
		self.c = c
		self.s = s
		self.t = t

class maximumFlow:
	'Maximum Flow: Edmonds-Karp'

	def __residualCapacity(self, R: flowNetwork, path: dict):
		v = R.t
		crPath = R.c[(path[v], v)]
		while v != R.s:
			if crPath > R.c[(path[v], v)]:
				crPath = R.c[(path[v], v)]
			v = path[v]
		return crPath

	def __findAugmentingPath(self, R: flowNetwork):
		t = GraphTraversal()
		path = t.BFS(R.g, R.s)

		if path[R.t] == None:
			return None
		return path

	def __augmentFlow(self, N: flowNetwork, R: flowNetwork,
					  path: dict, flow: dict):
		crPath = self.__residualCapacity(R, path)

		v = R.t
		while v != R.s:
			u = path[v]
			eD = (u,v)
			eR = (v,u)

			# augment flow along the path
			if eD in N.g.eList: # direct edge (increase flow along G's edges)
				if eD not in flow:
					flow[eD] = 0
				flow[eD] += crPath
			else:			   # reverse edge (otherwise decrease flow)
				flow[eD] -= crPath
				if flow[eD] == 0:
					del flow[eD]

			# update the residual network: R.G (Gf), R.c (cr)
			R.c[eD] -= crPath
			if R.c[eD] == 0:
				R.g.removeEdge(eD)
				del R.c[eD]

			if eR not in R.g.eList:
				R.g.addEdge(eR)
				R.c[eR] = crPath
			else:
				R.c[eR] += crPath

			v = path[v]

		return flow

	def findMaximumFlow(self,  N: flowNetwork):
		R = N
		flow = dict()

		path = self.__findAugmentingPath(R)
		while path != None:
			flow = self.__augmentFlow(N, R, path ,flow)
			path = self.__findAugmentingPath(R)

		return flow

	def printFlow(self, flow: dict):
		for e in flow:
			print(e, ':', flow[e])

class testMaximumFlow:
	def doTest(self):
		verticesList = ['s', 'a', 'b', 't']
		edgesList = [
			('s','a'), ('s','b'),
			('a','b'), ('a','t'),
			('b','t'),
			]
		c = {
			('s','a'):2, ('s','b'):2,
			('a','b'):2, ('a','t'):4,
			('b','t'):3,
			}
		g = Graph(verticesList, edgesList)
		g.printAdjList()

		N = flowNetwork(g, 's', 't', c)
		fN = maximumFlow()
		flow = fN.findMaximumFlow(N)
		fN.printFlow(flow)

#t = testMaximumFlow()
#t.doTest()
