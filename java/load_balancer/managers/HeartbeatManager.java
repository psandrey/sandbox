package load_balancer.managers;

import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import load_balancer.balancers.LoadBalancer;
import load_balancer.providers.Provider;

public class HeartbeatManager {
	private LoadBalancer lb;

	private int heartBeatTime;
	private Timer timer;
	private TimerTask hearBeatTask;
	
	public HeartbeatManager(LoadBalancer lb, int heartBeatTime) {
		this.lb = lb;
		this.heartBeatTime = heartBeatTime;
		assert heartBeatTime > 0:"Heart Beat time must be > 0";
		
		this.hearBeatTask = new TimerTask() {
			public void run() { check(); }
		};
		this.timer = new Timer();
	}
	
	public void startLoadBalancer() {
		 timer.schedule(hearBeatTask, 0, heartBeatTime);
	}
	
	public void stopLoadBalancer() {
		timer.cancel();
		timer.purge();
	}

	private void check() {
		Map<String, Provider> registered = lb.getRegisteredProviders();
		
		Iterator<?> it = registered.entrySet().iterator();
		while (it.hasNext()) {
			@SuppressWarnings("rawtypes")
			Map.Entry pair = (Map.Entry)it.next();
			String URI = (String) pair.getKey();
			Provider p = (Provider) pair.getValue();

			p.simulateChangeStatus();
			if (p.isAlive()) {
				// if the URI is added, then alive = 1
				// if the URI is not added, then alive = 2, thus add it to balancer
				if (lb.checkAndAddAlive(URI) == false) lb.addToBalancer(URI);
			} else {
				lb.removeAlive(URI);
				lb.removeFromBalancer(URI);
			}
		}
	}
}
