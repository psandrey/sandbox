package load_balancer.managers;

import load_balancer.balancers.LoadBalancer;
import load_balancer.providers.Provider;

public class ManualManager {
	private LoadBalancer lb;
	public ManualManager(LoadBalancer lb) { this.lb = lb; }
	
	/** Add a registered provider to the balancer */
	public boolean add(String URI) {	
		Provider p = lb.getProvider(URI);
		if (p == null) return false;
		
		if (!p.isAlive()) {
			lb.removeAlive(URI);
			return false;
		}
		
		lb.addAlive(URI);
		lb.addToBalancer(URI);
		return true;
	}

	/** Remove a registered provider from the balancer */
	public boolean remove(String URI) {
		Provider p = lb.getProvider(URI);
		if (p == null) return false;

		return lb.removeFromBalancer(URI);
	}
}
