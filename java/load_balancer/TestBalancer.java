package load_balancer;

import static java.lang.System.out;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import load_balancer.balancers.LoadBalancer;
import load_balancer.balancers.SlotManager;
import load_balancer.managers.HeartbeatManager;
import load_balancer.managers.ManualManager;
import load_balancer.providers.Provider;
import load_balancer.schedulers.Scheduler;
import load_balancer.schedulers.SchedulerRandom;
import load_balancer.schedulers.SchedulerRoundRobin;

public class TestBalancer {
	/*
	 * Test Description: Random Scheduler (requirement 3: random invocation)
	 * ---------------------------------------------------------------------
	 * There are 2 scheduling algorithms: random and round robin. This test uses the
	 * random algorithm for scheduler.
	 */
	public static void testAutomaticManagerWithRandom() {
		SlotManager slMng = new SlotManager(3);
		Scheduler sch = new SchedulerRandom();
		LoadBalancer lb = new LoadBalancer(sch, slMng);
		HeartbeatManager lbMng = new HeartbeatManager(lb, 1);

		// add providers
		ArrayList<String> pURIs = new ArrayList<String>();
		for (int i = 0; i < 10; i++)
			pURIs.add(new Provider(lb, slMng).getURI());
		
		lbMng.startLoadBalancer();
		for (int i = 0; i < 2000; i++) {
			Provider p = lb.get();
			
			if (p != null)
				out.println("REQ ( " + i + " ) : got response: " + p.get());
			
			try { Thread.sleep(1); }
			catch (InterruptedException e) { e.printStackTrace(); }
		}
		lbMng.stopLoadBalancer();
		
		out.println("\n\n!!! DONE !!! ");		
	}
	/*
	 * Test Description: manual manager (requirement 5: manual node inclusion/exclusion)
	 * ---------------------------------------------------------------------------------
	 * Manual manager adds/removes nodes into balancer list.
	 * Note: If when load balancer get is issued the provider is dead it is removed from
	 *       balancer list. If the provider becomes alive it must be added manually into
	 *       the balancer list. 
	 */
	public static void testManualManagerWithRR() {
		SlotManager slMng = new SlotManager(3);	
		Scheduler sch = new SchedulerRoundRobin();
		LoadBalancer lb = new LoadBalancer(sch, slMng);
		ManualManager lbMng = new ManualManager(lb);

		// add providers
		ArrayList<String> pURIs = new ArrayList<String>();
		for (int i = 0; i < 3; i++)
			pURIs.add(new Provider(lb, slMng).get());
		
		for (int i = 0; i < 3; i++)
			lbMng.add(pURIs.get(i));

		for (int i = 0; i < 100; i++) {
			if (i % 3 == 0)
				lbMng.remove(pURIs.get(2));
			else if (i % 7 == 0)
				lbMng.add(pURIs.get(2));
			
			Provider p = lb.get();
			if (p != null)
				out.println("REQ ( " + i + " ) : got response: " + p.get());
		}
		
		out.println("\n\n!!! DONE !!!");
	}

	/*
	 * Test Description: automatic manager (requirement 6 and 7: Hear beat checker)
	 * ----------------------------------------------------------------------------
	 * Automatic manager adds and removes providers from balancer list based on their status.
	 * A provider can be alive or dead. If it is dead, then it is removed from the list and
	 * it will be added back if two hear beats are successful.
	 */
	public static void testAutomaticManagerWithRR() {
		SlotManager slMng = new SlotManager(3);
		Scheduler sch = new SchedulerRoundRobin();
		LoadBalancer lb = new LoadBalancer(sch, slMng);
		HeartbeatManager lbMng = new HeartbeatManager(lb, 1);

		// add providers
		ArrayList<String> pURIs = new ArrayList<String>();
		for (int i = 0; i < 3; i++)
			pURIs.add(new Provider(lb, slMng).getURI());
		
		lbMng.startLoadBalancer();
		for (int i = 0; i < 100; i++) {
			Provider p = lb.get();
			
			if (p != null)
				out.println("REQ ( " + i + " ) : got response: " + p.get());
			
			try { Thread.sleep(1); }
			catch (InterruptedException e) { e.printStackTrace(); }
		}
		lbMng.stopLoadBalancer();
		out.println("\n\n!!! DONE !!!");
	}

	/*
	 * Test Description: slot availability (requirement 8: cluster capacity limit)
	 * ---------------------------------------------------------------------------
	 * 3 clients, 2 providers with 1 slot each. Each client asks for a provider. 2 clients delays
	 *   releasing the the acquired slots, thus there is no slot for the third client. After one
	 *   of the two clients releases a slot, the third client will get its slot and the test ends. 
	 */
	public static void testSlotAvailability() throws InterruptedException {
		class ThreadClient implements Runnable {
			private int id;
			private CountDownLatch latch;
			
			private LoadBalancer lb;
			public ThreadClient(CountDownLatch latch, int id, LoadBalancer lb) {
				this.latch = latch;
				this.id = id;
				this.lb = lb;
			}
			
			public void run() {
				latch.countDown();
				Provider p = null;
				while (p == null) {
					p = lb.get();
					if (id < 2)
						try { Thread.sleep(1); }
						catch (InterruptedException e) { e.printStackTrace(); }
					
					String str = "#TH( " + id + " ) : got provider URI : ";
					if (p == null) str += " slot unavailable!!!";
					else str += p.get();
					out.println(str);
				}
			}
		}
		
		SlotManager slMng = new SlotManager(1);
		Scheduler sch = new SchedulerRoundRobin();
		LoadBalancer lb = new LoadBalancer(sch, slMng);
		//HeartbeatManager lbMng = new HeartbeatManager(lb, 1);
		ManualManager lbMng = new ManualManager(lb);
		
		int nCliets = 3;
		int nProviders = 2;
		ArrayList<String> pURIs = new ArrayList<String>();
		for (int i = 0; i < nProviders; i++)
			pURIs.add(new Provider(lb, slMng).getURI());
		
		for (int i = 0; i < nProviders; i++)
			lbMng.add(pURIs.get(i));
		
		ArrayList<Thread> thClients = new ArrayList<Thread>();
		CountDownLatch latch = new CountDownLatch(nCliets);
		for (int i = 0; i < nCliets; i++) {
			ThreadClient c = new ThreadClient(latch, i, lb);
			Thread th = new Thread(c);
			thClients.add(th);
		}
		
		//lbMng.startLoadBalancer();
		for (int i = 0; i < nCliets; i++)
			thClients.get(i).start();
		
		for (int i = 0; i < nCliets; i++)
			thClients.get(i).join();
		//lbMng.stopLoadBalancer();
		out.println("\n\n!!! DONE !!!");
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) throws InterruptedException {
		testAutomaticManagerWithRandom();
		//testManualManagerWithRR();
		//testAutomaticManagerWithRR();
		//testSlotAvailability();
	}
}
