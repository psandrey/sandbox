package load_balancer.balancers;

/*
 *  This slot manager is implemented as a counter, however in real application is implemented
 *  (for example) based on lease management.
 */
public class SlotManager {
	private int providerCapacity;
	private int totalSlots;
	private int slots;
	
	public SlotManager(int providerCapacity) {
		this.providerCapacity = providerCapacity;
		this.totalSlots = 0;
		this.slots = 0;
	}
	
	public void aquireSlot() {
		slots++;
	}
	
	public void releaseSlot() {
		slots--;
	}
	
	public boolean isAvailableSlot() {
		return (slots < totalSlots);
	}
	
	public void addProviderSlots() {
		totalSlots += providerCapacity;
	}
	
	public void removeProviderSlots() {
		totalSlots -= providerCapacity;
	}
}
