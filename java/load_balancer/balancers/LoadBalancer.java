package load_balancer.balancers;

import static java.lang.System.out;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import load_balancer.providers.Provider;
import load_balancer.schedulers.Scheduler;

public class LoadBalancer {
	private int maxProviders = 10;

	private Map<String, Provider> registered;  // (URI, Provider): manage registered providers
	private Set<String> alive;                 // (URI): manage alive providers
	private ArrayList<String> balance;         // (URI): keep a list of providers subject to schedulers
	
	private ReentrantLock malive;              // alive set is subject to multi-threaded manipulation
	private ReentrantLock mbalance;            // balance list is subject to multi-threaded manipulation
	
	private Scheduler scheduler;	           // The scheduler: Round-Robin, Random, etc...
	private SlotManager slMng;                 // Slot management: acquire/release slot by clients
	
	public LoadBalancer(Scheduler scheduler, SlotManager slMng) {
		this.scheduler = scheduler;
		this.slMng = slMng;
		
		// registered is subject to multi-threaded manipulation
		this.registered = new ConcurrentHashMap<>();

		this.alive = new HashSet<>();
		this.malive = new ReentrantLock();
		
		this.balance = new ArrayList<String>();
		this.mbalance = new ReentrantLock();
	}

	/** Register a Provider in the load balancer */
	public boolean registerProvider(Provider p) {
		if (registered.size() == maxProviders) return false;
		
		registered.put(p.getURI(), p); // register the provider
		slMng.addProviderSlots();      // add the provider's slots
		return true;
	}
	
	/** Unregister a Provider from the load balancer */
	public boolean unregisterProvider(String URI) {
		if (!registered.containsKey(URI)) return false;

		// TODO: gracefully shutdown the provider
		registered.remove(URI);      // unregister the provider
		slMng.removeProviderSlots(); // remove the provider's slots
		return true;
	}
	
	/** Get the registered provider specified by URI */
	public Provider getProvider(String URI) {
		if (!registered.containsKey(URI)) return null;
		
		return registered.get(URI);
	}
	
	/** Get the registered providers */
	public Map<String, Provider> getRegisteredProviders() {
		return registered;
	}

	/** Is the Provider with the given URI registered ? */
	public boolean isRegistered(String URI) {
		return registered.containsKey(URI);
	}
	
	/** Get result from the scheduled provider. */
	public Provider get() {
		Provider p = null;
		mbalance.lock();
		if (slMng.isAvailableSlot()) { // do we have slots ?
			while (!balance.isEmpty() && p == null) { // do we have alive providers ?
				
				String URI = scheduler.doSchedule(balance);
				p = registered.get(URI);
				assert p != null : "Provider unknown";
				
				// if the provider is dead remove it from balancer
				if (!p.isAlive()) {
					p = null;
	
					removeAlive(URI);
					_checkAndRemoveFromBalancer(URI);
				} else
					slMng.aquireSlot();
			}
		} else out.println("!!! Load Balancer: slot unavailable !!!");
		mbalance.unlock();
		
		return p;
	}

	/** Add a registered provider to the balancer list (subject for scheduler) */
	public boolean addToBalancer(String URI) {
		if (!isRegistered(URI)) return false;

		mbalance.lock();
		boolean rc = _checkAndAddToBalancer(URI);
		mbalance.unlock();
		
		return rc;
	}

	/** Remove a registered provider from the balancer list (subject for scheduler) */
	public boolean removeFromBalancer(String URI) {
		if (!isRegistered(URI)) return false;
		
		mbalance.lock();
		boolean rc = _checkAndRemoveFromBalancer(URI);
		mbalance.unlock();
		
		return rc;
	}
	
	private boolean _isInBalancer(String URI) {
		for(String uri: balance)
			if (URI.equals(uri)) return true;
		
		return false;
	}
	
	private boolean _checkAndAddToBalancer(String URI) {
		if (_isInBalancer(URI)) return false;
		balance.add(URI);

		out.println("  ++ URI ( " + URI  + " ) - ALIVE * 2 : added to the pool.");
		return true;
	}
	
	private boolean _checkAndRemoveFromBalancer(String URI) {
		if (_isInBalancer(URI)) {
			balance.remove(URI);
			
			out.println("  -- URI ( " + URI  + " ) - DEAD : remove from the pool.");
			return true;
		}
		
		return false;
	}
	
	/** Add the provider in alive group */
	public void addAlive(String URI) {
		malive.lock();
		if (!_isAlive(URI)) alive.add(URI);
		malive.unlock();
	}

	/** Check and Add the provider in alive group */
	public boolean checkAndAddAlive(String URI) {
		boolean rc = false;
		
		malive.lock();
		if (!_isAlive(URI)) {
			addAlive(URI);
			rc = true;
		}
		malive.unlock();
		
		return rc;
	}
	
	/** Remove the provider from alive group */
	public void removeAlive(String URI) {
		malive.lock();
		if (_isAlive(URI)) alive.remove(URI);
		malive.unlock();
	}
	
	private boolean _isAlive(String URI) {
		return alive.contains(URI); 
	}
}
