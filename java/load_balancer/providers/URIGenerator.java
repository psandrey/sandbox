package load_balancer.providers;

public enum URIGenerator {
	INSTANCE;
	
	private int uri = 0;
	public String getURI() {
		uri++;
		return String.valueOf(uri);
	}
}
