package load_balancer.providers;

import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

import load_balancer.balancers.LoadBalancer;
import load_balancer.balancers.SlotManager;

public class Provider {
	private String URI;
	private boolean alive;
	private ReentrantLock mLock;
	
	SlotManager slMng;
	
	public Provider (LoadBalancer lb, SlotManager slMng) {
		this.URI = URIGenerator.INSTANCE.getURI();
		this.slMng = slMng;
		
		this.mLock = new ReentrantLock();
		this.alive = true;
		
		lb.registerProvider(this);
	}
	
	public boolean isAlive() {
		mLock.lock();
		boolean rc = alive;
		mLock.unlock();
		
		return rc;
	}
	
	public String getURI() { return URI; }
	
	public String get() {
		//TODO: The provider can be DEAD when get is issued, however because there is no lease
		//      algorithm implemented, get must work every time a provider is scheduled
		//      (given to a client), otherwise the slot manager fails.
		//if (!isAlive()) return "";
		
		// tell to load balancer that we have a free slot
		slMng.releaseSlot();
		return URI;
	}
	
	public void simulateChangeStatus() {
		Random rand = new Random();
		int x = rand.nextInt(2);
		
		mLock.lock();
		alive = true;
		if (x == 0) alive = false;
		mLock.unlock();
	}
}
