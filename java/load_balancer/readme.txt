LOAD BALANCING:
---------------
	A load balancer is a component that, once 
	invoked, it distributes incoming requests to 
	a list of registered providers and return 
	the value obtained from one of the 
	registered providers to the original caller. 
	For simplicity we will consider both the 
	load balancer and the provider having a 
	public method named get().

	Step 1 - Generate provider  
		Generate a Provider that, once invoked 
		on his get() method, retrieve an unique 
		identifier (string) of the provider 
		instance.

	Step 2 - Register a list of providers  
		Register a list of provider instances to 
		the Load Balancer - the max number 
		of accepted providers from the load 
		balancer is 10.

	Step 3 - Random invocation  
		Develop an algorithm that, when invoking 
		multiple times the Load Balancer on its 
		get() method, should cause the random 
		invocation of the get() method of any 
		registered provider instance.

	Step 4 - Round Robin invocation  
		Develop an algorithm that, when invoking 
		multiple times the Load Balancer on its 
		get() method, should cause the 
		round-robin (sequential) invocation of the 
		get() method of the registered providers.  

	Step 5 - Manual node exclusion / inclusion  
		Develop the possibility to exclude / 
		include a specific provider into the 
		balancer.

	Step 6 - Heart beat checker  
		The load balancer should invoke every X 
		seconds each of its registered providers 
		on a special method called check() to 
		discover if they are alive - if not, it should 
		exclude the provider node from load 
		balancing.  

	Step 7 - Improving Heart beat checker 
		If a node has been previously excluded 
		from the balancing it should be 
		re-included if it has successfully been 
		"heartbeat checked" for 2 consecutive 
		times.

	Step 8 - Cluster Capacity Limit  
		Assuming that each provider can handle a 
		maximum number of Y parallel requests, 
		the Balancer should not accept any further 
		request when it has (Y​*aliveproviders) 
		incoming requests running simultaneously.

Questions:
----------
	Step 3. Random invocation: What if the provider is not alive ?
	Step 4. Round Robin invocation: Same as above, what if is not alive ?
		Note: The scheduler chooses only providers that are alive.
	Step 5. Manual inclusion/exclusion:
		Q1. Include only alive providers?
		Q2. How Heart-beat checker and manual inclusion/exclusion works together?
		Note: Q1. If the provider is dead, then it will not be included in the
		balancer pool.
		Q2. Manual manager vs Heart-beat manager. There are implemented as
		mutual exclusive managers.
	Step 8. Cluster capacity limit: This can be implemented in different ways:
		1. Based on lease management. The LB gives a lease (time frame) within which
		the client uses the resource and then can either extend the lease or release
		the resource. If the resource dies within this time frame, the LB should check
		and remove it from the balancer pool of resources. The cluster availability is
		the number of remaining leases.
		2. Based on queues. The LB keeps input queue/s and output queue/s. The cluster
		availability is the difference between the max available slots and the size of
		the input queue.
		3. Based on callbacks. "similar with above"
		... etc ...
		Note: The current implementation uses just a counter for slots. Each time a
		provider is scheduled a slot is acquired, and each time a client is served
		a slot is released. This works perfectly if the provider do not die while
		it is used by a client. If it dies and the slot is never released tjen the
		slot management fails.

Description:
------------
	1. balancers:
		- LoadBalancer.java
			- keeps track of registered and alive providers
			- handle requests by using a given scheduler (RR or Random)
		- SlotManager.java
			- manage cluster capacity limit
	2. managers:
		- HeartbeatManager.java
			- automatic administration of providers
			- include/exclude providers from pool based on thier state (i.e. alive)
		- ManualManager.java
			- manual administration of providers
			- a provider will be included if and only if it is registered and alive
	3. providers:
		- Provider.java
			- the resource
		- URIGenerator.java
			- slingleton that generates unique ids
	4. schedulers:
		- Scheduler.java
			- the interface
		- SchedulerRandom.java
			- schedule a random provider
		- SchedulerRoundRobin.java
			- schedule the next provider based on round-robin mechanism
	- TestBalancer.java
		- several test cases







