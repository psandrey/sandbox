package load_balancer.schedulers;

import java.util.ArrayList;

public interface Scheduler {
	public String doSchedule(ArrayList<String> p);
}
