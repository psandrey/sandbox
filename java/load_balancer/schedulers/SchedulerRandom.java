package load_balancer.schedulers;

import java.util.ArrayList;
import java.util.Random;

public class SchedulerRandom implements Scheduler{
	private Random rand;
	public SchedulerRandom() { rand = new Random(); }
	
	@Override
	public String doSchedule(ArrayList<String> p) {
		if (p.isEmpty()) return null;
		int idx = rand.nextInt(p.size());
		
		return p.get(idx);
	}

}
