package load_balancer.schedulers;

import java.util.ArrayList;

public class SchedulerRoundRobin implements Scheduler {
	private int idx;
	public SchedulerRoundRobin() { idx = 0; }
	
	@Override
	public String doSchedule(ArrayList<String> p) {
		if (p.isEmpty()) return null;
		if (idx >= p.size()) idx = 0;
		
		String ret = p.get(idx);
		idx++;

		return ret;
	}

}
