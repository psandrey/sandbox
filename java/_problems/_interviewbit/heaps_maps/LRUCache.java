package _problems._interviewbit.heaps_maps;

/*
 * Design and implement a data structure for Least Recently Used (LRU) cache.
 * It should support the following operations: get and set. 
 * get(key) - Get the value (will always be positive) of the key if the key exists in the cache,
 *  otherwise return -1.
 * set(key, value) - Set or insert the value if the key is not already present.
 *  When the cache reached its capacity, it should invalidate the least recently used item
 *  before inserting a new item.
 * 
 * Note: Definition of �least recently used� : An access to an item is defined as a get or a set
 *       operation of the item. �Least recently used� item is the one with the oldest access time.
 * 
 * Note: LinkedHashMap can be configured in the constructor to behave as LRU: 
 * 			linkedMap = new LinkedHashMap<T1,T2>(16, 0.75f, true);
 */

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class LRUCache {

	// this version uses LinkedHashMap as a cache
	private int cap;
	private LinkedHashMap<Integer, Integer> cache = null;
	
	@SuppressWarnings("serial")
	public LRUCache(int capacity) {
		if (capacity < 1) return;
		cap = capacity;
		cache = new LinkedHashMap<Integer, Integer>(cap, 0.75f, true) {
			@Override
            protected boolean removeEldestEntry(Map.Entry<Integer, Integer> eldest) {
                return size() > cap;
            }
		};
	}
	
	int get(int k) {
		if (cache.containsKey(k))
			return cache.get(k);
	
		return -1;
	}

	void set(int k, int v) {
		cache.put(k, v);
	}
	
/*
	// version using custom doubly linked list
	private class ListNode {
		ListNode next, prev;
		int k, v;
		public ListNode(int key, int val) {
			k = key; v = val; next = this; prev = this; }
	};
	private ListNode head = null;

	ListNode ListRemoveNode(ListNode m) {
		if (m.next == m && m.prev == m) head = null;
		else {
			if (head == m) head = m.next;
			ListNode p = m.prev, n = m.next;
			p.next = n; n.prev = p;
		}

		return m;
	}

	void ListInsertFrontNode(ListNode m) {
		if (head == null) head = m;
		else {
			ListNode p = head.prev;
			p.next = m;
			m.next = head; m.prev = p;
			head.prev = m;
			head = m;
		}
	}

	private int cap;
	private HashMap<Integer, ListNode> hm = null;
	
	public LRUCache(int capacity) {
		if (capacity < 1) return;
		cap = capacity;
		head = null;
		hm = new HashMap<Integer, ListNode>(capacity);
	}
	
	int get(int k) {
		if (!hm.containsKey(k)) return -1;

		ListNode m = hm.get(k);
		ListRemoveNode(m);
		ListInsertFrontNode(m);
		return m.v;
	}

	void set(int k, int v) {
		ListNode m = null;
		if (hm.containsKey(k)) {
			m = hm.get(k);
			ListRemoveNode(m);
			m.v = v;
		} else {
			if (hm.size() == cap)
				hm.remove( ListRemoveNode(head.prev).k );

			m = new ListNode(k, v);
			hm.put(k, m);
		}

		ListInsertFrontNode(m);
	}
	
	// debug
	private void printList() {
		ListNode n = head;
		if (n == null) return;
		
		System.out.print(" [ ");
		do {
			System.out.print(n.v + "  ");
			n = n.next;
		} while (n != head);
		
		System.out.print(" ] ");
		System.out.println("");
	}
*/
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int cap = 2;
		LRUCache t = new LRUCache(cap);

		t.set(2, 1);
		t.set(2, 2);
		System.out.println(t.get(2));
		t.set(1, 1);
		t.set(4, 1);
		System.out.println(t.get(2));
	}
}
