package _problems._interviewbit.heaps_maps;

/*
 * Merge k sorted linked lists and return it as one sorted list.
 */

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class MergeKSortedLists {

	private class ListNode {
		public int val;
		public ListNode next;
		ListNode(int x) { val = x; next = null; }
	}
	
	public ListNode mergeKLists(ArrayList<ListNode> lists) {
		Comparator<ListNode> comp= new Comparator<ListNode>() {
			@Override
			public int compare(ListNode n, ListNode m) {
				return (n.val - m.val);
			}
		};
		PriorityQueue<ListNode> pq = new PriorityQueue<ListNode>(comp);
		for (int j = 0; j < lists.size(); j++) pq.add(lists.get(j));
		
		ListNode ans = null, head = null;
		while(!pq.isEmpty()) {
			ListNode n = pq.poll();
			ListNode m = n.next;
			if (m != null) pq.add(m);
			
			if (ans == null) { ans = n; head = n; }
			else { head.next = n; head = n; }
		}
	
		return ans;
	}
	
	// debug
	private ListNode arrayToList(int[] arr) {
		if (arr == null || arr.length == 0)
			return null;

		ListNode L = new ListNode(arr[0]);
		ListNode prev = L;
		for (int i = 1; i < arr.length; i++) {
			ListNode n = new ListNode(arr[i]);
			prev.next = n;
			prev = n;
		}

		return L;
	}

	private void printList(ListNode L) {
		ListNode n = L;
		while (n != null) {
			System.out.print(n.val + ",");
			n = n.next;
		}
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[][] arrs = {
				{ 1, 3, 5, 7 },
				{ 2, 4, 6, 8 },
				{ 0, 9, 10, 11 }
		};
		
		MergeKSortedLists t = new MergeKSortedLists();

		ArrayList<ListNode> lists = new ArrayList<>();
		for (int i = 0; i < arrs.length; i++)
			lists.add(t.arrayToList(arrs[i]));
		
		ListNode L = t.mergeKLists(lists);
		t.printList(L);
	}
}
