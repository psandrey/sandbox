package _problems._interviewbit.heaps_maps;

/*
 * You are given an array of N integers, A1, A2 ,�, AN and an integer K.
 * Return the of count of distinct numbers in all windows of size K.
 */

import java.util.HashMap;

public class DistinctNumbersWindow {
	
	// T(n) = O(n) - assuming the hash table is O(n) in the worst case
	public int[] dNums(int[] a, int k) {
		if (a == null || (a.length > 0 && k > a.length) || (a.length == 0 && k == 0))
			return new int[] {};

		int n = a.length, count = 0;
		int[] ans = new int[n-k+1];
		HashMap<Integer, Integer> hm = new HashMap<>();
		
		for (int i = 0; i < n; i++) {
			// add next element
			if (hm.containsKey(a[i])) hm.put(a[i], hm.get(a[i])+1);
			else { hm.put(a[i], 1); count++; }
			
			// if we passed the first window, remove the last element
			if (i >= k) {
				if (hm.get(a[i-k]) == 1) { hm.remove(a[i-k]); count--; }
				else hm.put(a[i-k], hm.get(a[i-k])-1);
			}
			
			// the array of distinct elements in window
			if (i >= k-1) ans[i-k+1] = count;
		}
		
		return ans;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = new int[] {1, 2, 1, 3, 4, 3};
		int k = 3;
		
		DistinctNumbersWindow t = new DistinctNumbersWindow();
		int[] ans = t.dNums(a, k);
		for (int i = 0; i < ans.length; i++) System.out.print(ans[i] + ", ");
	}
}
