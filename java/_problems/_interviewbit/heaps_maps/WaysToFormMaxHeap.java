package _problems._interviewbit.heaps_maps;

/*
 * Max Heap is a special kind of complete binary tree in which for every node the value
 *  present in that node is greater than the value present in it�s children nodes.
 * 
 * So now the problem statement for this question is:
 *  How many distinct Max Heap can be made from n distinct integers
 *  In short, you have to ensure the following properties for the max heap :
 *  1. Heap has to be a complete binary tree (A complete binary tree is a binary tree
 *   in which every level, except possibly the last, is completely filled, and all nodes
 *   are as far left as possible. )
 *  2. Every node is greater than all its children
 */

import java.util.HashMap;

public class WaysToFormMaxHeap {
		
	private long choose(long[][] nck, int n, int k) {
		if (k > n) return 0L;
		if (n <= 1) return 1L;
		if (k == 0) return 1L;
		k = Math.min(k, n-k);
		
		if (nck[n][k] != -1) return nck[n][k];

		long ans = choose(nck, n-1, k-1) + choose(nck, n-1, k);

		long m = 1000000007;
		nck[n][k] = ans%m;
		return nck[n][k];
	}
	
	private int floorLog2(int x) {
		int v = x, y = -1;
		while (v > 0) { v >>= 1; y++; }
		return y;
	}
	
	public int solve(int n) {
		int MAX = 101;
		long[][] nck = new long[MAX][MAX];
		for(int i = 0; i < MAX; i++)
			for (int j = 0; j < MAX; j++) nck[i][j] = -1;
		
		HashMap<Integer, Long> anss = new HashMap<>();
		return (int) (helper(nck, anss, n));
	}
	
	public long helper(long[][] nck, HashMap<Integer, Long> anss, int n) {
		if (n <= 2) return 1;
		if (anss.containsKey(n)) return anss.get(n);

		int l, r;
		int h = floorLog2(n);
		int _2h = (1<<h); // 2^h
		int last_level_full = _2h;
		int last_level = n+1-_2h;
		
		// compute L and R
		if (last_level >= (last_level_full>>1)) l = _2h-1;
		else l = n-(_2h>>1);
		r = n-1-l;

		
		long m = 1000000007;
		long ans = (helper(nck, anss, l)*helper(nck, anss, r))%m;
		ans = (choose(nck, n-1, l)*ans)%m;

		anss.put(n, ans);
		return ans;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		WaysToFormMaxHeap t = new WaysToFormMaxHeap();
		System.out.print(t.solve(100));
	}
}
