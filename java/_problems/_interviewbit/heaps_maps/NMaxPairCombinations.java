package _problems._interviewbit.heaps_maps;

/*
 * Given two arrays A & B of size N each.
 * Find the maximum n elements from the sum combinations (Ai + Bj) formed from elements
 * in array A and B.
 * 
 * For example if A = [1,2], B = [3,4], then possible pair sums can be 1+3 = 4,
 *  1+4=5 , 2+3=5 , 2+4=6 and maximum 2 elements are 6, 5
 *  
 *  Example:
 *  N = 4
 *  a[]={1,4,2,3}
 *  b[]={2,5,1,6}
 *  
 *  Maximum 4 elements of combinations sum are
 *	10 (4+6), 9 (3+6), 9 (4+5), 8 (2+6)
 */

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.PriorityQueue;

public class NMaxPairCombinations {

	// T(n) = O(n.logn)
	// S(N) = O(n) - the priority Q and the hash-set
	public int[] solve(int[] a, int[] b) {
		if (a == null || b == null || a.length == 0 || b.length == 0 || a.length != b.length)
			return new int[] {};
		
		class Element {
			int i, j;
			public Element(int ii, int jj) { i = ii; j = jj; }
			
			@Override
			public int hashCode() {
				int hash = 17;
				hash = hash * 31 + i;
				hash = hash * 31 + j;
				return (int)hash;
			}

			@Override
			public boolean equals(Object obj){
				if (obj instanceof Element) {
					Element e = (Element) obj;
					return (e.i == i && e.j == j);
				}
				return false;
			}
		}
		
		Arrays.sort(a); // O(n.logn)
		Arrays.sort(b); // O(n.logn)

		int n = a.length;
		int[] ans = new int[n];

		Comparator<Element> comp = new Comparator<Element> () {
			@Override
			public int compare(Element e1, Element e2) {
				return ((a[e2.i] + b[e2.j])-(a[e1.i] + b[e1.j]));
			}
		};
		PriorityQueue<Element> pq = new PriorityQueue<>(comp);
		HashSet<Element> hs = new HashSet<>();
		Element e = new Element(n-1, n-1);
		hs.add(e);
		pq.add(e);

		// T(n) - O(n.logn)
		int k = 0;
		while (k < n) {
			Element cur = pq.poll();
			ans[k++] = a[cur.i] + b[cur.j];
			
			e = new Element(cur.i - 1, cur.j);
			if (cur.i - 1 >= 0 && !hs.contains(e)) { hs.add(e); pq.add(e); }
			
			e = new Element(cur.i, cur.j - 1);
			if (cur.j - 1 >= 0 && !hs.contains(e)) { hs.add(e); pq.add(e); }
		}
		
		return ans;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//int[] a = {1,4,2,3};
		//int[] b = {2,5,1,6};
		//int[] a = {4, 2, 5, 1};
		//int[] b = {8, 0, 3, 5};
		//int[] a = {1};
		//int[] b = {1};
		int[] a = { 3, 2, 4, 2 };
		int[] b = { 4, 3, 1, 2 };
		
		NMaxPairCombinations t = new NMaxPairCombinations();
		int[] ans = t.solve(a, b);
		System.out.print("[");
		for (int i = 0; i < ans.length; i++) System.out.print(ans[i] + ", ");
		System.out.print("]");
		System.out.println();
	}
}
