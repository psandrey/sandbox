package _problems._interviewbit.heaps_maps;

/*
 * Given N bags, each bag contains Ai chocolates. There is a kid and a magician.
 * In one unit of time, kid chooses a random bag i, eats Ai chocolates, then
 * the magician fills the ith bag with floor(Ai/2) chocolates.
 * 
 * Given Ai for 1 <= i <= N, find the maximum number of chocolates kid can eat in K units of time.
 */

import java.util.Comparator;
import java.util.PriorityQueue;

public class MagicianAndChocolates {

	public int nchoc(int k, int[] a) {
		if (a == null || a.length == 0 || k == 0) return 0;
		long ans = 0;
		long m = 1000000007;
		
		Comparator<Integer> comp = new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o2.intValue() - o1.intValue();
			}
		};
		PriorityQueue<Integer> pq = new PriorityQueue<>(comp);
		for (int i = 0; i < a.length; i++) pq.add(a[i]);
		
		while (k > 0 && !pq.isEmpty()) {
			long bag = (long)pq.poll();
			ans = (ans + (bag%m))%m;
			k--;
			
			bag /= 2L;
			if (bag != 0) pq.offer((int)bag);
		}
		
		return (int)(ans%m);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		MagicianAndChocolates t = new MagicianAndChocolates();
		int k = 3;
		int[] a = new int[] {6, 5};
		
		System.out.println(t.nchoc(k, a));
	}
}
