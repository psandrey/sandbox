package _problems._interviewbit.strings;

/*
 * Given an array of words and a length L, format the text such that each line has exactly L characters
 *  and is fully (left and right) justified. You should pack your words in a greedy approach; that is,
 *  pack as many words as you can in each line.
 *  
 *  Pad extra spaces � � when necessary so that each line has exactly L characters.
 *  Extra spaces between words should be distributed as evenly as possible.
 *  If the number of spaces on a line do not divide evenly between words, the empty slots on the left
 *  will be assigned more spaces than the slots on the right.
 *  For the last line of text, it should be left justified and no extra space is inserted between words.
 *  Your program should return a list of strings, where each string represents a single line.
 */

import java.util.LinkedList;

public class JustifiedText {

	public String[] fullJustify(String[] strs, int L) {
		LinkedList<String> ans = new LinkedList<>();
		if (strs == null || strs.length == 0 || L == 0) return new String[] {};
		
		int n = strs.length;
		int start = 0, end = 0;
		while (end < n) {
			start = end; end++;
			int count = strs[start].length();
			while (end < n && count + 1 + strs[end].length() <= L) {
					count += 1 + strs[end].length();
					end++;
			}
			
			int words = end - start;
			StringBuilder sb = new StringBuilder(count);
			if (words > 1) {
				int spaces = words-1;
				int totalSpaces = spaces + (L - count);
				int q = 1, r = 0;
				if (end < n) {
					q = totalSpaces/spaces;
					r = totalSpaces%spaces;
				}

				for (int i = start; i < end-1; i++) {
					sb.append(strs[i]);
					int space = q;
					if (r > 0) {space++; r--; }
					String format = "%"+space+"s";
					sb.append(String.format(String.format(format," ")));
				}
			}
			
			sb.append(strs[end-1]);
			
			// add the fucking spaces at the end (this cost me 3 stupid submits)
			if (sb.length() < L) {
				int space = L - sb.length(); 
				String format = "%"+space+"s";
				sb.append(String.format(String.format(format," ")));
			}

			ans.add(sb.toString());
		}
		
		String[] toString = new String[ans.size()];
		toString = ans.toArray(toString);
		return toString;
	}
	
	private void print(String[] l) {
		for(String s: l) {
			System.out.println(s);
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//String[] strs = {"This", "is", "an", "examples", "of", "text", "justification."};
		//int L = 16;
				
		String[] strs = {"am", "fasgoprn", "lvqsrjylg", "rzuslwan", "xlaui", "tnzegzuzn", "kuiwdc", "fofjkkkm", "ssqjig", "tcmejefj", "uixgzm", "lyuxeaxsg", "iqiyip", "msv", "uurcazjc", "earsrvrq", "qlq", "lxrtzkjpg", "jkxymjus", "mvornwza", "zty", "q", "nsecqphjy"};
		int L = 14;
		
		//String[] strs = {"a", "b", "c", "xyz"};
		//int L = 5;
		JustifiedText t = new JustifiedText();
		String[] l = t.fullJustify(strs, L);
		t.print(l);
	}
}
