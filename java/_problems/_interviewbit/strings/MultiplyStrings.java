package _problems._interviewbit.strings;

/*
 * Given two numbers represented as strings, return multiplication of the numbers as a string.
 */

public class MultiplyStrings {

	public String multiply(String a, String b) {
		// handle special cases
		if (a == null || b == null || a.length() == 0 || b.length() == 0) return "";
		if ((a.length() == 1 && a.charAt(0)-'0' == 0) ||
			(b.length() == 1 && b.charAt(0)-'0' == 0)) return "0";

		int ea = 0, eb = 0, i, j, pos, c = 0;
		// cut the leading zeros (if they are)
		while (a.charAt(ea) - '0' == 0) ea++;
		while (b.charAt(eb) - '0' == 0) eb++;

		// do the multiply
		int n = a.length(); int m = b.length(); int nm = n+m-ea-eb;		
		int[] ans = new int [nm];
		for (j = m-1; j >= eb; j--) {
			c = 0;
			for (i = n-1; i >= ea; i--) {
				int p = (a.charAt(i) - '0')*(b.charAt(j)-'0');
				pos = i+j+1-ea-eb;
				int s = ans[pos] + p + c;

				ans[pos] = s%10;
				c = s/10;
			}

			pos = i+j+1-ea-eb;
			ans[pos] = c;
		}
		
		StringBuilder sb = new StringBuilder();
		for (i = (c == 0? 1 : 0); i < nm; i++) sb.append(ans[i]);
			
		return sb.toString();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String a = "0006020453667958309279424408570378228292268488402";
		String b = "0021473700594524297017810575200827941459805716642468749607585313713214621412";
		
		MultiplyStrings t = new MultiplyStrings();
		String ans = t.multiply(a, b);
		System.out.println(ans);
	}
}
