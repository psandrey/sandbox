package _problems._interviewbit.strings;

/*
 * Write a function to find the longest common prefix string amongst an array of strings.
 * Longest common prefix for a pair of strings S1 and S2 is the longest string S which is the
 *  prefix of both S1 and S2.
 *  
 * As an example, longest common prefix of "abcdefgh" and "abcefgh" is "abc".
 * Given the array of strings, you need to find the longest S which is the prefix of ALL
 *  the strings in the array.
 */

public class LongestCommonPrefix {

	public String longestCommonPrefix(String[] strs) {
		if (strs == null || strs.length == 0) return "";
		if (strs.length == 1) return strs[0];
		
		int n = strs.length;
		int minLen = getMinLength(strs);
		if (minLen == 0) return "";
		
		int len = 0;
		for (int pos = 0; pos < minLen; pos++) {
			char c = strs[0].charAt(pos);
			for (int i = 1; i < n; i++)
				if (strs[i].charAt(pos) != c)
					return (len == 0 ? "" : strs[0].substring(0, len));
			len++;
		}
		
		return (len == 0 ? "" : strs[0].substring(0, len));
	}
	
	private int getMinLength(String[] strs) {
		int minLen = Integer.MAX_VALUE;
		for (String s: strs) minLen = Math.min(minLen, s.length());
		
		return minLen;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		String[] strs = {"abcd", "abcd", "efgh"};
		LongestCommonPrefix t = new LongestCommonPrefix();
		String s = t.longestCommonPrefix(strs);
		System.out.println(s);
	}
}
