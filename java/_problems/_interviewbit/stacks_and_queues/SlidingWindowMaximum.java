package _problems._interviewbit.stacks_and_queues;

/*
 * A long array A[] is given to you. There is a sliding window of size w which is moving
 * from the very left of the array to the very right. You can only see the w numbers in the window.
 *  Each time the sliding window moves rightwards by one position. You have to find the maximum
 *  for each window. 
 * 
 * The following example will give you more clarity:
 * Input:  [1 3 -1 -3 5 3 6 7], and w is 3
 * Output: [3 3 5 5 6 7]
 */

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;

public class SlidingWindowMaximum {

	// Version T(n) = O(n)
	public int[] slidingMaximum(final int[] a, int w) {
		if (a == null || a.length == 0) return new int[] {};

		int n = a.length;
		int m = (n < w ? 1 : n-w+1);
		int[] ans = new int[m];
	
		LinkedList<Integer> q = new LinkedList<Integer>();
		int i = 0;
		while (i<n) {
			// we make sure that the q is always in descending order
			while (!q.isEmpty() && a[i] > a[q.peekLast()]) q.removeLast();
			
			q.add(i);
			
			if (i+1 >= w) ans[i+1-w] = a[q.peekFirst()];
			
			// remove if it drops outside the window
			if (q.peekFirst() == i+1-w) q.removeFirst();
			i++;
		}

		return ans;
	}
	
	// Version T(n) = O(n.logk) - using heap (priority queue)
	public int[] slideWindowV2(int[] a, int w) {
		int n = a.length;
		int m = (n < w ? 1 : n-w+1);
		int[] ans = new int[m];
		
		Comparator<Integer> comp = new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o2.intValue()-o1.intValue();
			}
			
		};
		PriorityQueue<Integer> pq = new PriorityQueue<Integer>(comp);
		int i = 0, limit = Math.min(w, n);
		for (i = 0; i < limit; i++) pq.add(a[i]);
		
		while (i < n) {
			int max = pq.peek();
			ans[i-w] = max;
			pq.remove(a[i-w]);
			pq.offer(a[i]);
			i++;
		}
		ans[n<w ? 0 : i-w] = pq.peek();
		
		return ans;
	}
	
	private void printArray(int[] a) {
		System.out.print("[");
		for (int i = 0; i < a.length; i++)
			System.out.print(a[i] + ", ");
		System.out.println("]");
		System.out.println();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		//int[] a = new int[] {1, 3, -1, -3, 5, 3, 6, 7};
		//int w = 3;

		int[] a = new int[] {648, 614, 490, 138, 657, 544, 745, 582, 738, 229, 775, 665, 876, 448, 4, 81, 807, 578, 712, 951, 867, 328, 308, 440, 542, 178, 637, 446, 882, 760, 354, 523, 935, 277, 158, 698, 536, 165, 892, 327, 574, 516, 36, 705, 900, 482, 558, 937, 207, 368};
		int w = 9;

		SlidingWindowMaximum t = new SlidingWindowMaximum();
		int[] ans;
		ans = t.slidingMaximum(a, w);
		System.out.println("Linear version:");
		t.printArray(ans);

		
		ans = t.slideWindowV2(a, w);
		System.out.println("Log version:");
		t.printArray(ans);
	}
}
