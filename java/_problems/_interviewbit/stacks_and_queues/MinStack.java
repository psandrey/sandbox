package _problems._interviewbit.stacks_and_queues;

/*
 * Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.
 * 
 * push(x) -- Push element x onto stack.
 * pop() -- Removes the element on top of the stack.
 * top() -- Get the top element.
 * getMin() -- Retrieve the minimum element in the stack.
 */

public class MinStack {

	// check the version from CPP!!!
	private class Elem {
		public int value;
		public Elem prev;
		public int min;
		
		public Elem(int value, Elem prev, int min) {
			this.value = value;
			this.prev = prev;
			this.min = min;
		}
	}
	
	Elem stack = null;
	private void push(int value) {
		if (stack == null)
			stack = new Elem(value, null, value);
		else {
			int min = Math.min(value, stack.value);
			Elem e = new Elem(value, stack, min);
			stack = e;
		}
	}
	
	private Elem pop() {
		if (stack == null)
			return null;
		
		Elem topElem = stack;
		topElem = null;
		stack = stack.prev;
		return topElem;
	}

	private int top() {
		if (stack == null)
			return -1;
		
		return stack.value;
	}
	
	private int getMin () {
		if (stack == null)
			return -1;

		return stack.min;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		MinStack s = new MinStack();
		s.push(10);
		s.push(5);
		s.push(12);
		s.push(1);
		while (s.top() != -1) {
			System.out.println(s.getMin());
			s.pop();
		}
	}
}
