package _problems._interviewbit.stacks_and_queues;

/*
 * Evaluate the value of an arithmetic expression in Reverse Polish Notation.
 * Valid operators are +, -, *, /. Each operand may be an integer or another expression.
 * 
 * Examples:
 *   ["2", "1", "+", "3", "*"] -> ((2 + 1) * 3) -> 9
 *   ["4", "13", "5", "/", "+"] -> (4 + (13 / 5)) -> 6
 */

import java.util.HashSet;
import java.util.Stack;

public class EvaluateExpression {

	public int evalRPN(String[] expr) {
		if (expr == null || expr.length == 0) return 0;

		String[] ops = {"+", "-", "*", "/"};
		HashSet<String> hs = new HashSet<>();
		for (String op: ops) hs.add(op);
		
		Stack<Integer> st = new Stack<>();
		for (String item: expr) {
			if (!hs.contains(item)) { st.push(Integer.valueOf(item)); continue; }

			String op = item;
			int operand2 = st.pop();
			int operand1 = st.pop();
			int eval = 0;
			
			/* + */ if (op.equals(ops[0])) eval = operand1 + operand2;
			/* - */ else if (op.equals(ops[1])) eval = operand1 - operand2;
			/* * */ else if (op.equals(ops[2])) eval = operand1 * operand2;
			/* / */ else if (op.equals(ops[3])) eval = operand1 / operand2;
			st.push(eval);
		}
		
		return st.pop();
	}
	
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		String[] expr = {"15", "7", "1", "1", "+", "-", "/", "3", "*", "2", "1", "1", "+", "+", "-"};

		EvaluateExpression t = new EvaluateExpression();
		System.out.println(t.evalRPN(expr));
	}
}
