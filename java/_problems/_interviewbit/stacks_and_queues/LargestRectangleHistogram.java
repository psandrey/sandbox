package _problems._interviewbit.stacks_and_queues;

/*
 * Given n non-negative integers representing the histogramís bar height where the width of each bar is 1,
 * find the area of largest rectangle in the histogram.
 */

import java.util.Stack;

public class LargestRectangleHistogram {

	// version 1:
	//  T(n) = O(n)
	//  S(n) = O(1)
	public int largestRectangleArea(int[] hist) {
		if (hist == null || hist.length == 0) return 0;
		int n = hist.length;
		if (n == 1) return hist[0];
		
		Stack<Integer> st = new Stack<Integer>();
		int max = 0, i = 0;
		while (i < n) {
			if (st.isEmpty() || hist[st.peek()] <= hist[i]) {
				st.push(i++);
				continue;
			}
			 
			int top = st.pop();
			int h = hist[top];
			
			// when we compute width we need i - "first previous smaller than top => st.peek()", 
			// width is not (i - top), is (i - prev) and -1 because we are one step further
			// (first after top that is smaller).
			int w = (st.isEmpty() ? i : i - st.peek() - 1);
			int area = h*w;
			max = Math.max(max, area);
		}
		
		// compute leftovers rectangles
		while (!st.isEmpty()) {
			int top = st.pop(); 
			int h = hist[top];
			int w = (st.isEmpty() ? i : i - st.peek() - 1);
			int area = h*w;
			max = Math.max(max, area);			
		}
		
		return max;
	}

 	// version 2:
	//  T(n) = O(n)
	//  S(n) = O(n)
	public int largestRectangleAreaV2(int[] hist) {
		if (hist == null || hist.length == 0) return 0;
		int n = hist.length;
		if (n == 1) return hist[0];
		
		int[] toRight = toRight(hist);
		int[] toLeft  = toLeft(hist);
		
		int max = 0;
		for (int i = 0; i < n; i++)
			max = Math.max(max, toRight[i] + toLeft[i] + hist[i]);
		
		return max;
	}

	private int[] toRight(int[] hist) {
		int n = hist.length;
		int[] toRight = new int[n];

		Stack<Integer> st = new Stack<Integer>();
		int i = 0;
		while (i < n) {
			// push index as long as we can expand
			if (st.isEmpty() || hist[st.peek()] <= hist[i]) {
				st.push(i++);
				continue;
			}
			
			// if we cannot expand anymore, then compute the area
			int top = st.pop();
			int height = hist[top];
			int width = i - top - 1;
			int area = height * width;
			toRight[top] = area;
		}
		
		// compute leftovers rectangles
		while (!st.isEmpty()) {
			int top = st.pop();
			int height = hist[top];
			int width = i - top - 1;
			int area = height * width;

			toRight[top] = area;			
		}
		
		return toRight;
	}
	
	private int[] toLeft(int[] hist) {
		int n = hist.length;
		int[] toLeft = new int[n];

		Stack<Integer> st = new Stack<Integer>();
		int i = n - 1;
		while (i >= 0) {
			// push index as long as we can expand
			if (st.isEmpty() || hist[st.peek()] <= hist[i]) {
				st.push(i--);
				continue;
			}
			
			// if we cannot expand anymore, then compute the area
			int top = st.pop();
			int height = hist[top];
			int width = top - i - 1;
			int area = height * width;
			toLeft[top] = area;
		}
		
		// compute leftovers rectangles
		while (!st.isEmpty()) {
			int top = st.pop();
			int height = hist[top];
			int width = top - i - 1;
			int area = height * width;
			toLeft[top] = area;		
		}
		
		return toLeft;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		//int[] hist = {2,2,5,6,2,3};
		//int[] hist = {6, 2, 5, 4, 5, 1, 6 };
		int[] hist = new int[]{1, 5, 4, 5, 1};
		LargestRectangleHistogram t = new LargestRectangleHistogram();
		int r = t.largestRectangleArea(hist);
		System.out.println(r);
	}
}
