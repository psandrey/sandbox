package _problems._interviewbit.backtracking;

/*
 * Given a collection of candidate numbers (C) and a target number (T),
 * find all unique combinations in C where the candidate numbers sums to T.
 *
 * Each number in C may only be used once in the combination.
 *
 *  Note:
 * All numbers (including target) will be positive integers.
 * Elements in a combination (a1, a2, � , ak) must be in non-descending order. (ie, a1 <= a2 <= ... <= ak).
 * The solution set must not contain duplicate combinations.
 *
 * Example :
 * Given candidate set 10,1,2,7,6,1,5 and target 8,
 * A solution set is:
 * [1, 7]
 * [1, 2, 5]
 * [2, 6]
 * [1, 1, 6]
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Stack;

public class CombinationSumII {
	public ArrayList<ArrayList<Integer>> combinationSumII(ArrayList<Integer> a, int t) {
		ArrayList<ArrayList<Integer>> ans = new ArrayList<ArrayList<Integer>>();
		if (a.isEmpty()) return ans;
		
		Collections.sort(a);
		Stack<Integer> st = new Stack<>();
		builfCombSum(st, a, t, 0, ans);
		return ans;
	}

	void builfCombSum(Stack<Integer> st, ArrayList<Integer> a, int t, int j, ArrayList<ArrayList<Integer>> ans) {
		if (t == 0) ans.add(new ArrayList<Integer>(st));
		else if (j < a.size()) {
			
			// take a[j] if and only if it hasn't already been taken
			if (t >= a.get(j)) {
				st.push(a.get(j));
				builfCombSum(st, a, t - a.get(j), j + 1, ans);
				st.pop();
			}
			
			// don't take a[j] and avoid duplicates
			j++;
			while (j< a.size() && a.get(j) == a.get(j - 1)) j++;
			builfCombSum(st, a, t, j, ans);
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[] a = { 10,1,2,7,6,1,5 };
		int target = 8;
		
		ArrayList<Integer> A = new ArrayList<Integer>(Arrays.asList(a));
		
		CombinationSumII t = new CombinationSumII();
		ArrayList<ArrayList<Integer>> ans = t.combinationSumII(A, target);
		for (int j = 0; j < ans.size(); j++) {
			for (Integer x: ans.get(j))
				System.out.print(x + " ");
			System.out.println();
		}
	}
}
