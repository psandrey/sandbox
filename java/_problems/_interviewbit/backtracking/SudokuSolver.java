package _problems._interviewbit.backtracking;

import java.util.Stack;

/*
 * Write a program to solve a Sudoku puzzle by filling the empty cells.
 * 
 * Note: Backtracking version.
 */

public class SudokuSolver {
	
	// iterative backtracking (classic)
	private class Item {
		int[] cell;
		int k;
		public Item(int[] cell, int k) {
			this.cell = cell;
			this.k = k;
		}
	}

	private void solverBck(int[][] board){
		boolean isSucc = false;
		Stack<Item> st = new Stack<Item>();
		
		initStack(board, st);
		while (!st.empty()) {
			// find a valid successor
			do {
				isSucc = successor(board, st);
			} while (isSucc == true && !isValid(board, st));
			
			if (isSucc == true) {
				// if the successor is valid, then check if we have a solution
				if (isSolution(board))
					break;
				else
					// if not a solution, it means that there are empty cells, so
					//  lets find a valid number for the empty cell
					st.add(nextMove(board));
			} else {
				// no valid successor for the current cell, so backtrack and check
				//  next valid configuration
				Item top = st.pop();
				board[top.cell[0]][top.cell[1]] = 0;
			}
		}
	}

	private void initStack(int[][] board, Stack<Item> st) {
		// add the first empty cell in the board
		int[] cell = new int[2];
		findNextEmptyCell(board, cell);
		st.add(new Item(cell, 0));
	}
	
	private boolean successor(int[][] board, Stack<Item> st) {
		Item top = st.peek();
		if (top.k < 9) {
			top.k++;
			st.pop();
			st.add(top);
			return true;
		}
		
		return false;
	}

	private boolean isValid(int[][] board, Stack<Item> st) {
		Item top = st.peek();
		boolean flag = valid(board, top.cell, top.k);
		if(flag)
				board[top.cell[0]][top.cell[1]] = top.k;
		return flag;
	}
	
	private boolean isSolution(int[][] board) {
		for(int i = 0; i < board.length; i++)
			for(int j = 0; j < board[0].length; j++) {
				if (board[i][j] == 0)
					return false;
			}
		
		return true;
	}
	
	private Item nextMove(int[][] board) {
		int[] cell = new int[2];
		findNextEmptyCell(board, cell);
		return new Item(cell, 0);
	}

	// recursive backtracking
	private boolean solver(int[][] board) {
		int[] cell = new int[2];

		// find an empty cell
		//  if none is found, then we found the solution
		if (!findNextEmptyCell(board, cell))
			return true;

		// find a valid successor
		for(int k = 1; k <= 9; k++) {

			if (valid(board, cell, k)) { // is the successor valid?
				board[cell[0]][cell[1]] = k;

				if (solver(board)) // if the successor is valid, then move FW
					return true;
			}
			
			// backtracks
			board[cell[0]][cell[1]] = 0;
		}
		
		return false;
	}

	private boolean findNextEmptyCell(int[][] board, int[] cell) {
		for(int i = 0; i < board.length; i++)
			for(int j = 0; j < board[0].length; j++) {
				if (board[i][j] == 0) {
					cell[0] = i;
					cell[1] = j;
					return true;
				}
			}
		
		return false;
	}
	
	private boolean valid(int[][] board, int[] cell, int k) {
		return  validColumn(board, cell, k) &&
				validRow(board, cell, k) &&
				validCell(board, cell, k);
	}
	
	private boolean validColumn(int[][] board, int[] cell, int k) {
		for (int i = 0; i < 9; i++) {
			if (board[i][cell[1]] == k)
				return false;
		}

		return true;
	}

	private boolean validRow(int[][] board, int[] cell, int k) {
		for (int i = 0; i < 9; i++) {
			if (board[cell[0]][i] == k)
				return false;
		}

		return true;
	}
	
	private boolean validCell(int[][] board, int[] cell, int k) {
		int offLine = cell[0] / 3;
		int offCol = cell[1] / 3;
		
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				if (board[3*offLine+i][3*offCol+j] == k)
					return false;
			}
		}
		
		return true;
	}
	
	private void printMatrix(int[][] a) {
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				System.out.print(a[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[][] board = new int[][] {
			{3, 0, 6,   5, 0, 8,   4, 0, 0},
			{5, 2, 0,   0, 0, 0,   0, 0, 0},
			{0, 8, 7,   0, 0, 0,   0, 3, 1},
			
			{0, 0, 3,   0, 1, 0,   0, 8, 0},
			{9, 0, 0,   8, 6, 3,   0, 0, 5},
			{0, 5, 0,   0, 9, 0,   6, 0, 0},
			
			{1, 3, 0,   0, 0, 0,   2, 5, 0},
			{0, 0, 0,   0, 0, 0,   0, 7, 4},
			{0, 0, 5,   2, 0, 6,   3, 0, 0}};

		SudokuSolver t = new SudokuSolver();
		t.solver(board);
		//t.solverBck(board);
		t.printMatrix(board);
	}
}
