package _problems._interviewbit.backtracking;

/*
 * Given a set of distinct integers, S, return all possible subsets.
 *
 *  Note:
 * Elements in a subset must be in non-descending order.
 * The solution set must not contain duplicate subsets.
 * Also, the subsets should be sorted in ascending ( lexicographic ) order.
 * The list is not necessarily sorted.
 *
 *  Example : If S = [1,2,3], a solution is:
 *
 * [
 *   [],
 *   [1],
 *   [1, 2],
 *   [1, 2, 3],
 *   [1, 3],
 *   [2],
 *   [2, 3],
 *   [3],
 * ]
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Stack;

public class Subset {

	public ArrayList<ArrayList<Integer>> subsets(ArrayList<Integer> a) {
		ArrayList<ArrayList<Integer>> ans = new ArrayList<ArrayList<Integer>>();
		ans.add(new ArrayList<Integer>()); // empty subset
		if (a.isEmpty()) return ans;

		Collections.sort(a);
		
		Stack<Integer> st = new Stack<Integer>();
		for (int j = 0; j < a.size(); j++)
			buildSubsets(st, j, a, ans);
		
		return ans;
	}
	
	public void buildSubsets(Stack<Integer> st, int j, ArrayList<Integer> a, ArrayList<ArrayList<Integer>> ans) {
		if (j == a.size()) return;
		
		st.push(a.get(j));
		ans.add(new ArrayList<Integer>(st));
		
		for (int i = j + 1; i < a.size(); i++)
			buildSubsets(st, i, a, ans);
		
		st.pop();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[] a = {1, 2, 3};
		ArrayList<Integer> A = new ArrayList<Integer>(Arrays.asList(a));
		
		Subset t = new Subset();
		ArrayList<ArrayList<Integer>> ans = t.subsets(A);
		for (int j = 0; j < ans.size(); j++) {
			for (Integer x: ans.get(j))
				System.out.print(x + " ");
			System.out.println();
		}
	}
}
