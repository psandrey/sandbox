package _problems._interviewbit.backtracking;

/*
 * Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses of length 2*n.
 *
 * For example, given n = 3, a solution set is:
 *
 * "((()))", "(()())", "(())()", "()(())", "()()()"
 * Make sure the returned list of strings are sorted.
 */

import java.util.ArrayList;

public class GenerateAllParenthesesII {
	public ArrayList<String> generateParenthesis(int n) {
		ArrayList<String> ans = new ArrayList<String>();
		if (n <= 0) return ans;
		
		StringBuilder st = new StringBuilder();
		generateParenthesis(st, n, n, ans);
		
		return ans;
	}

	private void generateParenthesis(StringBuilder st, int n, int m, ArrayList<String> ans) {
		if (n == 0 && m == 0) ans.add(st.toString());
		else {
			if (n > 0) {
				st.append('(');
				generateParenthesis(st, n - 1, m, ans);
				st.deleteCharAt(st.length() - 1);
			}
			
			if (m > n) {
				st.append(')');
				generateParenthesis(st, n, m - 1, ans);
				st.deleteCharAt(st.length() - 1);
			}
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		GenerateAllParenthesesII t = new GenerateAllParenthesesII();
		ArrayList<String> ans = t.generateParenthesis(3);

		for (String s : ans) System.out.println(s);
	}
}
