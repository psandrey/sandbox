package _problems._interviewbit.backtracking;

/*
 * The set [1,2,3,...,n] contains a total of n! unique permutations.
 *
 * By listing and labeling all of the permutations in order,
 * We get the following sequence (ie, for n = 3 ) :
 *
 * 1. "123"
 * 2. "132"
 * 3. "213"
 * 4. "231"
 * 5. "312"
 * 6. "321"
 * Given n and k, return the kth permutation sequence.
 *
 * For example, given n = 3, k = 4, ans = "231"
 *
 *  Good questions to ask the interviewer :
 *
 *  What if n is greater than 10. How should multiple digit numbers be represented in string?
 *    In this case, just concatenate the number to the answer.
 *    so if n = 11, k = 1, ans = "1234567891011"
 * 
 *  Whats the maximum value of n and k?
 *    In this case, k will be a positive integer thats less than INT_MAX.
 *    n is reasonable enough to make sure the answer does not bloat up a lot.
*/

public class KthPermutationSequence {

	public String getPermutation(int n, int k) {
		boolean[] digit = new boolean[n + 1];
		return buildPermutation(n, k - 1, digit);
	}
	
	public String buildPermutation(int n, int k, boolean[] digit) {
		if (n == 0) return "";
		
		int group = k / factorial(n - 1);
		int offset = k % factorial(n - 1);
		
		int leading = getDigit(digit, group) + 1;
		
		return Integer.toString(leading) + buildPermutation(n - 1, offset, digit);
	}
	
	private int factorial(int n) {
		if (n > 12)  return Integer.MAX_VALUE;

		int fact = 1;
		for (int i = 2; i <= n; i++) fact *= i;
		return fact;
	}
	
	private int getDigit(boolean[] digit, int j) {
		int i = 0, k = 0;
		while (i <= j) {
			if ( digit[k] == false ) i++;
			k++;
		}
		digit[k - 1] = true;
		return (k - 1);
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int n = 11;
		int k = 34;
		
		KthPermutationSequence t = new KthPermutationSequence();
		String ans = t.getPermutation(n, k);
		System.out.println(ans);
	}
}
