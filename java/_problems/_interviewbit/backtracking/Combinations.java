package _problems._interviewbit.backtracking;

/*
 * Given two integers n and k, return all possible combinations of k numbers out of 1 2 3 ... n.
 *
 * Make sure the combinations are sorted.
 *
 * To elaborate,
 * Within every entry, elements should be sorted. [1, 4] is a valid entry while [4, 1] is not.
 * Entries should be sorted within themselves.
 * Example :
 * If n = 4 and k = 2, a solution is:
 *
 * [
 *   [1,2],
 *   [1,3],
 *   [1,4],
 *   [2,3],
 *   [2,4],
 *   [3,4],
 * ]
 */

import java.util.ArrayList;
import java.util.Stack;

public class Combinations {
	
	public ArrayList<ArrayList<Integer>> combine(int n, int k) {
		ArrayList<ArrayList<Integer>> ans = new ArrayList<ArrayList<Integer>>();
		if (k > n) return ans;
		
		Stack<Integer> st = new Stack<Integer>();
		buildCombinations(st, 1, n, k, ans);
		
		return ans;
	}
	
	public void buildCombinations(Stack<Integer> st, int j, int n, int k, ArrayList<ArrayList<Integer>> ans) {
		if (st.size() == k) {
			ans.add(new ArrayList<Integer>(st));
			return;
		}
				
		for (int i = j; i <= n; i++) {
			st.push(i);
			buildCombinations(st, i + 1, n, k, ans);
			st.pop();
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int n = 4;
		int k = 2;

		Combinations t = new Combinations();
		ArrayList<ArrayList<Integer>> ans = t.combine(n, k);
		for (int j = 0; j < ans.size(); j++) {
			for (Integer x: ans.get(j))
				System.out.print(x + " ");
			System.out.println();
		}
	}
}
