package _problems._interviewbit.backtracking;

/*
 * Generate permutations.
 */

public class Permutations {
	
	private int factorial(int n) {
		// 1*2*3*4*...*n
		int f = 1;
		for (int i = 2; i <= n; i++) f *= i;
		
		return f;
	}
	
	private void setPermutation(int[] perm, int[]a, int[] p, int n) {
		for (int i = 0; i < n; i++)
			perm[i] = a[p[i]];
	}

	public int[][] permute(int[] a) {
		if (a == null || a.length == 0)
			return new int[][] {};

		int n = a.length;
		int m = factorial(n);
		int[][] ans = new int[m][n];
		
		int[] p = new int[n];
		for (int i = 0; i < n; i++) p[i] = i;
		
		int next = 0;
		setPermutation(ans[next], a, p, n); // set first permutation
		while (true) {
			// 1. find the largest k, such that a[k] < a[k+1];
			int k = n-2;
			while (k >= 0 && p[k] > p[k+1]) k--;

			// all permutations has been generated
			if (k < 0) break;
			
			// 2. find the largest l, such that a[k] < a[l]
			int l = n-1;
			while (l > k && p[l] < p[k]) l--;
			
			// 3. switch a[k] <-> a[l]
			int tmp = p[k]; p[k] = p[l]; p[l] = tmp;
			
			// 4. since all values are in decreasing order, now when the leading element
			//    has been increased, we need to inverse the order and make it increasing, thus
			//    the lexicographic order is preserved.
			k++; l = n-1;
			while (k < l) { tmp = p[k]; p[k] = p[l]; p[l] = tmp; k++; l--; }
	
			// 5. generated permutation
			setPermutation(ans[++next], a, p, n);
		}
		
		return ans;
	}
	
	// using standard backtracking method
	public int[][] permuteBck(int[] a) {
		if (a == null || a.length == 0)
			return new int[][] {};

		int n = a.length;
		int m = factorial(n);
		int[][] ans = new int[m][n];
		
		int next = -1;
		permutationsBck(ans, a, next);	
		return ans;
	}

	private void permutationsBck(int[][] ans, int[] a, int next) {
		int n = a.length;
		int[] st = new int[n+1];

		int k = 0; st[k] = -1;
		while (k >= 0) {
			boolean isSucc = false;
			
			// loop until we have a valid successor
			do {isSucc = isSuccessor(st, k, n);
			} while (isSucc && !isValid(st, k));

			// if the successor is valid, we check if we have a solution to process
			if (isSucc == true) {
				if (k == n - 1)
					setPermutation(ans[++next], a, st, a.length);
				else { k++; st[k] = -1; }
				
			// so, we do not have any successor on this level, thus backtracks
			} else k--;
		}
	}
	
	private boolean isSuccessor(int[] st, int k, int limit) {
		if (st[k] >= limit - 1)
			return false;

		st[k]++;
		return true;
	}
	
	private boolean isValid(int[] st, int k) {
		for (int i = 0; i < k; i++)
			if (st[k] == st[i]) return false;

		return true;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = new int[] {1, 2, 3};
		
		Permutations t = new Permutations();
		int[][] r = t.permuteBck(a);

		for (int i = 0; i < r.length; i++) {
			System.out.println();
			System.out.print("[");
			
			for (int j = 0; j < r[0].length; j++)
				System.out.print(r[i][j] + ", ");
			
			System.out.print("]");
		}
	}
}
