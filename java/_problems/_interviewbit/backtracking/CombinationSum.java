package _problems._interviewbit.backtracking;

/*
 * Given a set of candidate numbers (C) and a target number (T), find all unique combinations in C where
 * the candidate numbers sums to T.
 *
 * The same repeated number may be chosen from C unlimited number of times.
 *
 *  Note:
 * All numbers (including target) will be positive integers.
 * Elements in a combination (a1, a2, � , ak) must be in non-descending order. (ie, a1 <= a2 <= � <=ak).
 * The combinations themselves must be sorted in ascending order.
 * CombinationA > CombinationB iff (a1 > b1) OR (a1 = b1 AND a2 > b2) OR ... 
 *   (a1 = b1 AND a2 = b2 AND ... ai = bi AND ai+1 > bi+1)
 * The solution set must not contain duplicate combinations.
 * Example,
 * Given candidate set 2,3,6,7 and target 7,
 * A solution set is:
 *
 * [2, 2, 3]
 * [7]
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Stack;

public class CombinationSum {

	public ArrayList<ArrayList<Integer>> combinationSum(ArrayList<Integer> a, int t) {
		ArrayList<ArrayList<Integer>> ans = new ArrayList<ArrayList<Integer>>();
		if (a.isEmpty()) return ans;
		
		Collections.sort(a);
		Stack<Integer> st = new Stack<>();
		builfCombSum(st, a, t, 0, ans);
		return ans;
	}

	void builfCombSum(Stack<Integer> st, ArrayList<Integer> a, int t, int j, ArrayList<ArrayList<Integer>> ans) {
		if (t == 0) ans.add(new ArrayList<Integer>(st));
		else if (j < a.size()) {
			
			// take a[j], t/a[j] times if and only if it hasn't already been taken
			if (j == 0 || a.get(j) != a.get(j - 1)) {
				int times = t/a.get(j);
				for (int i = times; i >= 1; i--) st.push(a.get(j));
				
				for (int i = times; i >= 1; i--) {
					builfCombSum(st, a, t - i*a.get(j), j + 1, ans);
					st.pop();
				}
			}
			
			// don't take a[j]
			builfCombSum(st, a, t, j + 1, ans);
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//Integer a[] = {1, 1, 1};
		//int target = 3;
		Integer[] a = { 8, 10, 6, 11, 1, 16, 8  };
		int target = 28;
		
		ArrayList<Integer> A = new ArrayList<Integer>(Arrays.asList(a));
		
		CombinationSum t = new CombinationSum();
		ArrayList<ArrayList<Integer>> ans = t.combinationSum(A, target);
		for (int j = 0; j < ans.size(); j++) {
			for (Integer x: ans.get(j))
				System.out.print(x + " ");
			System.out.println();
		}
	}
}
