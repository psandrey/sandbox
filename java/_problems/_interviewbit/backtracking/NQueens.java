package _problems._interviewbit.backtracking;

/*
 * The n-queens puzzle is the problem of placing n queens on an n�n chessboard
 *  such that no two queens attack each other.
 */

import java.util.ArrayList;

public class NQueens {

	public ArrayList<ArrayList<String>> solveNQueens(int n) {
		ArrayList<ArrayList<String>> ans = new ArrayList<ArrayList<String>>();
		if (n == 1) {
			ArrayList<String> sol = new ArrayList<String>();
			sol.add("Q");
			ans.add(sol);
			return ans;
		}
		if (n < 4)
			return ans;

		StringBuilder sb = new StringBuilder(n);
		for (int i = 0; i < n; i++) sb.append('.');
		String blankLine = sb.toString();
		
		int[] st = new int[n];
		int k = 0;
		st[k] = -1;
		while (k >= 0) {
			boolean isSucc = false;
			do {
				isSucc = successor(st, k, n);
			} while (isSucc && !valid(st, k, n));

			if (isSucc) {
				if (k == n - 1) // solution if we have a valid Q on the last row
					processSolution(st, n, ans, blankLine);
				else { k++; st[k] = -1;}
			} else k--;
		}
		
		return ans;
	}

	private boolean successor(int[] st, int k, int n) {
		if (st[k] < n-1) {
			st[k]++;
			return true;
		}

		return false;
	}
	
	private boolean valid(int[] st, int k, int n) {
		for (int l = 0; l < k; l++) {
			int c = st[l];
			if (st[k] == c || (k - l) == Math.abs(st[k] - c))
				return false;
		}

		return true;
	}
	
	private void processSolution(int[] st, int n, ArrayList<ArrayList<String>> ans, String blank) {
		ArrayList<String> sol = new ArrayList<String>(n);
		for (int l = 0; l < n; l++) {
			// build line l
			StringBuilder sb = new StringBuilder(blank);
			sb.setCharAt(st[l], 'Q');
			 sol.add(sb.toString());
		}
		ans.add(sol);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int n = 4;
		NQueens t = new NQueens();
		ArrayList<ArrayList<String>> r = t.solveNQueens(n);
		for (ArrayList<String> board: r) {
			for (int l = 0; l < board.size(); l++)
				System.out.println(board.get(l));
			System.out.println();
		}
	}
}
