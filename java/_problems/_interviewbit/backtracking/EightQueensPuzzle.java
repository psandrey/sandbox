package _problems._interviewbit.backtracking;

import java.util.Arrays;

/*
 * The eight queens problem is the problem of placing eight queens on an 8�8 chessboard
 *  such that none of them attack one another (no two are in the same row,
 *  column, or diagonal). More generally, the n queens problem places n queens on
 *  an n�n chessboard.
 *  
 * Solutions:
 *     - 2 iterative backtracking
 *     - 1 recursive backtracking
 */

import java.util.Stack;

public class EightQueensPuzzle {
	
	// recursive version
	private int[] doQueenPuzzleBackRcr(int n) {
		int[] chessboard = new int[n];
		Arrays.fill(chessboard, -1);
		
		if (findQueenPuzzleBackRcr(chessboard, 0))
			return chessboard;

		return null;
	}
	
	private boolean findQueenPuzzleBackRcr(int[] chessboard, int row) {
		int n = chessboard.length;
		if (solutionBackRcr(row, n))
			return true;
		
		// try all the positions (i.e. cols) for the queen on the current row
		for (int col = 0; col < n; col++) {
			chessboard[row] = col;
			
			// if on this col the queen is free from attack, then go to the next queen
			if (validBackRcr(chessboard, row)) {
				
				//if we found a solution, then return from recursion
				if (findQueenPuzzleBackRcr(chessboard, row + 1))
					return true;
				
				// if putting the queen on this col doesn't generate a solution, try next position 
				chessboard[row] = -1;
			}
		}

		return false;
	}
	
	private boolean solutionBackRcr(int row, int n) {
		// if all the queens are on the chessboard, then it must be a solutions
		if (row == n)
			return true;
		return false;
	}
	
	private boolean validBackRcr(int[] chessboard, int row) {
		// check if candidate position of the queen in the current row is valid
		int candidate = chessboard[row];
		for (int r = 0; r < row; r++) {
			int d = chessboard[r];
			if ((d == candidate) || Math.abs(d - candidate) == (row - r))
				return false;
		}

		return true;
	}
	
	// iterative: classic
	private Integer[] doQueenPuzzleBackV2(int n) {
		if (n < 4)
			return null;

		boolean isSucc = false;
		Stack<Integer> st = new Stack<Integer>();
		st.add(-1);
		while (!st.empty()) {
			do {
				isSucc = successorV2(st, n);
			} while (isSucc == true && !isValidV2(st));
			
			if (isSucc == true) {
				if (isSolutionV2(st, n))
					break;
				else
					st.add(-1);
			} else
				st.pop();
		}
		
		return st.toArray(new Integer[st.size()]);
	}

	private boolean successorV2(Stack<Integer> st, int n) {
		int head = st.peek();
		if (head < n - 1) {
			st.set(st.size() - 1, head + 1);
			return true;
		}
		
		return false;
	}

	private boolean isValidV2(Stack<Integer> st) {
		int k = st.size();
		
		int candidate = st.get(k - 1);
		for (int i = 0; i < k - 1; i++) {
			int d = st.get(i);
			if ((d == candidate) || Math.abs(d - candidate) == (k - 1 - i))
				return false;
		}

		return true;
	}
	
	private boolean isSolutionV2(Stack<Integer> st, int n) {
		if (st.size() == n)
			return true;

		return false;
	}

	// iterative: ad-hoc version
	private Integer[] doQueenPuzzleBackV1(int n) {
		if (n < 4)
			return null;
		Stack<Integer> st = new Stack<Integer>();

		st.add(0);
		while (!st.isEmpty()) {
			if (isSolution(st, n))
				break;
			else {
				if (st.size() < n)
					st.add(0);
				else {
					while (!st.isEmpty()) {
						if (successor(st, n))
							break;
						else
							st.pop();
					}
				}
			}
		}

		return st.toArray(new Integer[st.size()]);
	}
	
	private boolean successor(Stack<Integer> st, int n) {
		int head = st.peek();
		if (head < n - 1) {
			st.set(st.size() - 1, head + 1);
			return true;
		}
		
		return false;
	}
	
	private boolean isSolution(Stack<Integer> st, int n) {
		if (st.size() < n)
			return false;

		for (int i = 0; i < n; i++) {
			int pos = st.get(i);
			for (int j = i + 1; j < n; j ++) {
				int next = st.get(j);

				// verify attack on cols
				if (pos == next)
					return false;
				
				//verify on diagonals
				if (Math.abs(next - pos) == j - i)
					return false;
			}
		}

		return true;
	}
	
	private void printSolution(Integer[] sol) {
		System.out.print("[");
		for (int i = 0; i < sol.length; i++)
			System.out.print(sol[i] + ",");
		System.out.print("]");
		System.out.println();
	}
	
	private void printSolution(int[] sol) {
		System.out.print("[");
		for (int i = 0; i < sol.length; i++)
			System.out.print(sol[i] + ",");
		System.out.print("]");
		System.out.println();
	}
	
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		EightQueensPuzzle t = new EightQueensPuzzle();
		Integer[] sol;
		int n = 8;
		
		sol = t.doQueenPuzzleBackV1(n);
		t.printSolution(sol);

		sol = t.doQueenPuzzleBackV2(n);
		t.printSolution(sol);
		
		int[] solrcr = t.doQueenPuzzleBackRcr(n);
		t.printSolution(solrcr);

	}
}
