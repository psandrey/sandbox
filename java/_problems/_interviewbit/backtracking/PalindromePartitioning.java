package _problems._interviewbit.backtracking;

/*
 * Given a string s, partition s such that every string of the partition is a palindrome.
 * Return all possible palindrome partitioning of s.
 */

import java.util.ArrayList;
import java.util.Stack;

public class PalindromePartitioning {

	public ArrayList<ArrayList<String>> partition(String a) {
		ArrayList<ArrayList<String>> ans = new ArrayList<ArrayList<String>>();
		int n = 0;
		if (a == null || (n = a.length()) == 0)
			return ans;
		else if (n == 1) {
			ArrayList<String> sol = new ArrayList<String>(); sol.add(a);
			ans.add(sol);
			return ans;
		}

		Stack<String> st =  new Stack<String>();
		helper(a, n, 0, st, ans);
		return ans;
	}
	
	private void helper(String a, int n, int i, Stack<String> st, ArrayList<ArrayList<String>> ans) {
		if (i == n) {
			ArrayList<String> sol = new ArrayList<String>(st);
			ans.add(sol);
			return;
		}
		
		for (int j = i; j < n; j++) {
			String c = a.substring(i, j + 1);
			if (!isPalindrome(c)) continue;
			
			st.push(c);
			helper(a, n, j+1, st, ans);
			st.pop();
		}
	}
	
	private boolean isPalindrome(String s) {
		char[] ss = s.toCharArray();
		int i = 0, j = ss.length - 1;
		while (i < j) {
			if (ss[i] != ss[j])
				return false;
			i++; j--;
		}

		return true;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String s = "aab";
		
		PalindromePartitioning t = new PalindromePartitioning();
		ArrayList<ArrayList<String>> ans = t.partition(s);
		for (ArrayList<String> l: ans) {
			System.out.println(l);
		}
	}
}
