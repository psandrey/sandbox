package _problems._interviewbit.backtracking;

import java.util.ArrayList;

public class GrayCode {

	public ArrayList<Integer> grayCode(int n) {
		ArrayList<Integer> gc = new ArrayList<Integer>();
		if (n == 0) return gc;
		
		gc.add(0); gc.add(1);
		if (n > 1) { 
			for (int k = 2; k <= n; k++) {
				int msb = 1 << (k - 1);
				for (int j = gc.size() - 1; j >= 0; j--) gc.add((msb | gc.get(j)));
			}
		}
		
		return gc;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int n = 3;
		GrayCode t = new GrayCode();
		ArrayList<Integer> gc = t.grayCode(n);
		
		for (Integer x: gc) System.out.print(x + " ");
		System.out.println();
	}
}
