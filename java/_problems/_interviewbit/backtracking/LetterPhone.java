package _problems._interviewbit.backtracking;

/*
 * Given a digit string, return all possible letter combinations that the number could represent.
 *
 * A mapping of digit to letters (just like on the telephone buttons) is given below.
 * {'0'},{'1'},
 *   2              3             4                 5
 * {'a', 'b', 'c'},{'d','e','f'}, {'g', 'h', 'i'}, {'j','k','l'},
 *   6              7                8               9
 * {'m','n','o'}, {'p','q','r','s'}, {'t','u','v'}, {'w','x','y','z'} };
 *
 * The digit 0 maps to 0 itself.
 * The digit 1 maps to 1 itself.
 *
 * Input: Digit string "23"
 * Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
 * Make sure the returned strings are lexicographically sorted.
 */

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class LetterPhone {

	public ArrayList<String> letterCombinations(String a) {
		ArrayList<String> ans = new ArrayList<String>();
		if (a.isEmpty()) return ans;
	
		ArrayList<List<Character>> map = buildLetterMap();
		StringBuilder st = new StringBuilder();
		
		letterCombinations (st, a, 0, ans, map);
		return ans;
	}

	private ArrayList<List<Character>> buildLetterMap() {
		ArrayList<List<Character>> map = new ArrayList<List<Character>>();
		Character[] zero = {'0'}; map.add(Arrays.asList(zero));
		Character[] one = {'1'}; map.add(Arrays.asList(one));
		Character[] two = {'a','b','c'}; map.add(Arrays.asList(two));
		Character[] three = {'d','e','f'}; map.add(Arrays.asList(three));
		Character[] four = {'g','h','i'}; map.add(Arrays.asList(four));
		Character[] five = {'j','k','l'}; map.add(Arrays.asList(five));
		Character[] six = {'m','n','o'}; map.add(Arrays.asList(six));
		Character[] seven = {'p','q','r','s'}; map.add(Arrays.asList(seven));
		Character[] eight = {'t','u','v'}; map.add(Arrays.asList(eight));
		Character[] nine = {'w','x','y','z'}; map.add(Arrays.asList(nine));
		return map;
	}
	
	private void letterCombinations (StringBuilder st, String a, int k, ArrayList<String> ans, ArrayList<List<Character>> map) {
		if (k == a.length()) ans.add(st.toString());
		else {
			List<Character> digit = map.get(a.charAt(k) - '0');
			for (Character c: digit) {
				st.append(c);
				letterCombinations(st, a, k + 1, ans, map);
				st.deleteCharAt(st.length() - 1);
			}
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String a = "23";
		LetterPhone t = new LetterPhone();
		ArrayList<String> ans = t.letterCombinations(a);
		for (String s: ans) System.out.println(s);
	}
}
