package _problems._interviewbit.backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Stack;

public class SubsetsII {

	public ArrayList<ArrayList<Integer>> subsetsWithDup(ArrayList<Integer> a) {
		ArrayList<ArrayList<Integer>> ans = new ArrayList<ArrayList<Integer>>();
		
		Collections.sort(a);
		
		Stack<Integer> st = new Stack<Integer>();
		buildSubsets(st, 0, a, ans);
		
		return ans;
	}
	
	public void buildSubsets(Stack<Integer> st, int i, ArrayList<Integer> a, ArrayList<ArrayList<Integer>> ans) {
		ans.add(new ArrayList<Integer>(st));
		
		for (int j = i; j < a.size(); j++) {
			if (j > i && a.get(j) == a.get(j - 1)) continue;
			
			st.push(a.get(j));
			buildSubsets(st,j + 1, a, ans);
			st.pop();
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[] a = {1, 2, 2};
		ArrayList<Integer> A = new ArrayList<Integer>(Arrays.asList(a));
		
		SubsetsII t = new SubsetsII();
		ArrayList<ArrayList<Integer>> ans = t.subsetsWithDup(A);
		for (int j = 0; j < ans.size(); j++) {
			for (Integer x: ans.get(j))
				System.out.print(x + " ");
			System.out.println();
		}
	}
}
