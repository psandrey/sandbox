package _problems._interviewbit.dynamic_programming;

/*
 * Find the superstring of a set of strings.
 * Note: Greedy, bad version.
 */

import java.util.Arrays;
import java.util.HashSet;

public class ShortestCommonSuperstringII {

	public String solve(String[] str) {
		if (str == null || str.length == 0)
			return "";

		// eliminate duplicates
		HashSet<String> hs = new HashSet<>(Arrays.asList(str));
		String[] a = new String[hs.size()];
		a = hs.toArray(a);
		Arrays.sort(a);
		int n = a.length;

		// initialize LCSP (longest common suffix-prefix) table
		int m = n;
		int[][] t = new int[n][n];
		for (int i = 0; i < n; i++) m -= LCSP(a, t, i);
		
		// update until one string remains (the SCS)
		while (m > 1) {
			int[] max = getMax(t, a);
			int i = max[0];
			int j = max[1];
			
			a[i] +=  a[j].substring(t[i][j]);
			a[j] = null; m--;

			m -= LCSP(a, t, i);
		}
				
		for (int i = 0; i < a.length; i++)
			if (a[i] != null) return a[i];
		
		return "";
	}
	
	private int LCSP(String[] a, int[][] t, int i) {
		if (a[i] == null) return 0;

		// update line i and col i
		int n = a.length;
		int deltions = 0;
		for (int j = 0; j < n; j++) {
			if (a[j] == null || j == i) continue;
			if (a[i].contains(a[j])) {  a[j] = null; deltions++;
										continue;}

			t[i][j] = LC(a[i], a[j]);
			t[j][i] = LC(a[j], a[i]);
		}
		
		return deltions;
	}
	
	private int LC(String s1, String s2) {
		int n = s1.length();
		int m = s2.length();
		int len = Math.min(n, m);

		int max = 0;
		for (int i = 1; i < len; i++)
			if (s1.regionMatches(n-i, s2, 0, i)) max = i;

		return max;
	}
	
	private int[] getMax(int[][] t, String[] a) {
		int n = t.length;
		int max = -1;
		int[] m = new int[2];
		for (int i = 0; i < n; i++) {
			if (a[i] == null) continue;
			
			for (int j = 0; j < n; j++) {
				if (i == j || a[j] == null) continue;
				
				if (max < t[i][j]) {
					max = t[i][j];
					m[0] = i; m[1] = j;
				}
			}
		}

		return m;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		//String[] a = {"abcd", "cdef", "fgh", "de"};
		//String[] a = {"abc", "de"};
		String[] a = {"xabc", "abc"};
		ShortestCommonSuperstringII t = new ShortestCommonSuperstringII();
		String ans = t.solve(a);
		System.out.println(ans);
	}
}
