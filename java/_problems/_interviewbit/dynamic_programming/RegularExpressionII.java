package _problems._interviewbit.dynamic_programming;

/*
 * Implement regular expression matching with support for '.' and '*'.
 * '.' Matches any single character.
 * '*' Matches zero or more of the preceding element.
 * 
 * The matching should cover the entire input string (not partial).
 */

public class RegularExpressionII {
	
	// the DP version: T(n) = O(n*m)
	public int isMatchDP(final String s, final String p) {
		int n = s.length();
		int m = p.length();
		boolean[][] t = new boolean [n+1][m+1];
		t[0][0] = true;

		// pattern of type a*b* can match to empty string;
		for(int j = 1; j < m; j++)
			if (p.charAt(j-1) == '*') t[0][j] = t[0][j-2];
		
		// i - string, j - pattern
		// t[i][j] == true if pattern[0:j] is a VALID regex for string[0:i]
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++){
				if (s.charAt(i-1) == p.charAt(j-1) || p.charAt(j-1) == '.')
					t[i][j] = t[i-1][j-1];

				else if(p.charAt(j-1) == '*'){
					// case 1: 0 matches
					t[i][j] = t[i][j-2];

					// case 2: multiple matches
					if(s.charAt(i-1) == p.charAt(j-2) || p.charAt(j-2) == '.')
						t[i][j] = (t[i][j] || t[i-1][j]);
				} else
					// pattern[0:j] is NOT a VALID regex for string[0:i]
					t[i][j] = false;
			}
		}
/*
		for (int i = 0; i <= n; i++){
			for (int j = 0; j <= m; j++) {
				int x = (t[i][j] == true ? 1 : 0);
				System.out.print(x + " ");
			}
			System.out.println();
		}
*/
		return (t[n][m]) ? 1 : 0; 
	}
	
	// the brute-force version: T(n) = O(m*2^n)
	public int isMatch(final String s, String p) {
		if ((s == null && p == null) || (s.length() == 0 && p.length() == 0))
			return 1;
		return isMatchBF(s, 0, p, 0);
	}

	private int isMatchBF(String s, int i, String p, int j) {
		int n = s.length();
		int m = p.length();
		if (i == n) {
			if (j == m || (j+1 == m-1 && p.charAt(j+1) == '*')) return 1;
			return 0;
		}
		
		if (j == m) return 0;
		
		// p[j+1] == '*'
		if (j+1 < m && (p.charAt(j+1) == '*')) {
			// case 1: 0 matches
			if (isMatchBF(s, i, p, j+2) == 1)
				return 1;
			
			// case 2: multiple matches: try all possible matches
			int k = 0;
			while (i+k<n && (s.charAt(i+k) == p.charAt(j) || p.charAt(j)=='.')){
				if (isMatchBF(s, i+k+1, p, j+2) == 1) return 1;
				k++;
			}
			
			return 0;
		} else {
			if (s.charAt(i) != p.charAt(j) && p.charAt(j) != '.') return 0;
			return isMatchBF(s, i+1, p, j+1);
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		//String s = "abbc";
		//String p = "ab*bbc";
		//String s = "bbb";
		//String p = "b*";
		String s = "abcbcd";
		String p = "a.*c.*d"
				;
		RegularExpressionII t = new RegularExpressionII();
		System.out.println("Mine-BF:" + t.isMatch(s, p));
		System.out.println("Their-DP:" + t.isMatchDP(s, p));
	}
}
