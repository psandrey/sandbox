package _problems._interviewbit.dynamic_programming;

/*
 * Say you have an array for which the ith element is the price of a given stock on day i.
 * If you were only permitted to complete at most one transaction (ie, buy one and sell
 * one share of the stock), design an algorithm to find the maximum profit.
 */

public class BestTimeToBuyAndSellStocks_I {

	public int maxProfit(final int[] a) {
		if (a == null || a.length < 2) return 0;
		
		int minSoFar = a[0];
		int ans = 0;
		for (int i = 1; i < a.length; i++) {
			ans = Math.max(ans, a[i] - minSoFar);
			minSoFar = Math.min(minSoFar, a[i]);
		}
		
		return ans;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = {50, 60, 80, 70, 60};
		BestTimeToBuyAndSellStocks_I t = new BestTimeToBuyAndSellStocks_I();
		System.out.println(t.maxProfit(a));
	}
}
