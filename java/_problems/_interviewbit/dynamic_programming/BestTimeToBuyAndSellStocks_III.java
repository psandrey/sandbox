package _problems._interviewbit.dynamic_programming;

/*
 * Say you have an array for which the ith element is the price of a given stock on day i.
 * Design an algorithm to find the maximum profit. You may complete at most two transactions.
 * 
 * Note:
 *   A transaction is a buy & a sell. You may not engage in multiple transactions at the same time
 *   (ie, you must sell the stock before you buy again).
 */

public class BestTimeToBuyAndSellStocks_III {

	public int maxProfit(final int[] a) {
		if (a == null || a.length < 2) return 0;
		
		int n = a.length;
		int[] left = new int[n];
		int[] right = new int[n];

		// scan from left to right and compute the maximum profit
		//  as if we do only one transaction
		int minSoFar = a[0];
		left[0] = 0;
		for (int i = 1; i < a.length; i++) {
			left[i] = Math.max(left[i-1], a[i] - minSoFar);
			minSoFar = Math.min(minSoFar, a[i]);
		}

		// scan from right to left and compute the maximum profit
		//  as if we do only one transaction
		int maxSoFar = a[n-1];
		right[0] = 0;
		for (int i = n-2; i >= 0; i--) {
			maxSoFar = Math.max(maxSoFar, a[i]);
			right[i] = Math.max(right[i+1], maxSoFar - a[i]);
		}

		// now, we know the maximum profit at each step, so we divide and conquer
		//  and just add the maximum profit from each part
		int ans = left[n-1];
		for (int i = 1; i < n; i++) {
			ans = Math.max(ans, left[i-1]+right[i]);
		}
		
		return ans;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = new int[]{1, 2, 7, 5, 10, 1, 10};
		
		BestTimeToBuyAndSellStocks_III t = new BestTimeToBuyAndSellStocks_III();
		System.out.println(t.maxProfit(a));
	}
}
