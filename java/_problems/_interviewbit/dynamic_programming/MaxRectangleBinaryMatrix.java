package _problems._interviewbit.dynamic_programming;

/*
 * Given a 2D binary matrix filled with 0�s and 1�s, find the largest rectangle containing
 * all ones and return its area.
 */

import java.util.Stack;

public class MaxRectangleBinaryMatrix {
	
	public int maximalRectangle(int[][] a) {
		if (a == null) return 0;

		int n = a.length;
		int m = a[0].length;
		int[] hist = new int[m];
		int max = 0;
		for (int i = 0; i < n; i++) {
			prepareHistogram(hist, a[i]);
			int candidate = largestRectangleArea(hist);
			max = Math.max(max, candidate);
		}

		return max;
	}
		
	private void prepareHistogram(int[] hist, int[] a) {
		for (int i = 0; i < hist.length; i++)
			hist[i] = (a[i] == 0) ? 0 : hist[i] + a[i];
	}

	private int largestRectangleArea(int[] hist) {
		Stack<Integer> st = new Stack<Integer>();
		int n = hist.length;
		int max = 0, i = 0;
		while (i < n) {
			if (st.isEmpty() || hist[st.peek()] <= hist[i]) {
				st.push(i++);
				continue;
			}
			 
			int top = st.pop();
			int h = hist[top];
			// when we compute width we need i - "first previous smaller than top = st.peek()", 
			// width is not i - top, is i - prev and -1 because we are one step further
			// (first after top that is smaller).
			int w = (st.isEmpty() ? i : i - st.peek() - 1);
			int area = h*w;
			max = Math.max(max, area);
		}
		
		// compute leftovers rectangles
		while (!st.isEmpty()) {
			int top = st.pop(); 
			int h = hist[top];
			int w = (st.isEmpty() ? i : i - st.peek() - 1);
			int area = h*w;
			max = Math.max(max, area);			
		}
		
		return max;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
/*
		int[][] a = new int[][] {
			{0, 0, 0, 1, 1},
			{0, 1, 1, 1, 1},
			{0, 1, 1, 1, 1},
			{0, 1, 0, 1, 0},
			{0, 0, 0, 0, 1}};
*/
		int[][] a = new int[][] {
		  {0, 0},
		};

		MaxRectangleBinaryMatrix t = new MaxRectangleBinaryMatrix();
		int r = t.maximalRectangle(a);
		System.out.println("DP:" + r);
	}
}
