package _problems._interviewbit.dynamic_programming;

/*
 * Given a 2D matrix, find the number non-empty sub matrices, such that the sum of the elements
 *  inside the sub matrix is equal to 0. (note: elements might be negative).
 */

import java.util.ArrayList;
import java.util.HashMap;

public class SubMatricesSumZero {

	// My (kind of DP) version... passed on line judge (T(n) = O(n^4))
	public int solve(int[][] a) {
		if (a == null || a.length == 0 || a[0].length == 0)
			return 0;

		int n = a.length;
		int count = 0;
		for (int lstart = 0; lstart < n; lstart++)
			for (int lend = lstart; lend < n; lend++) {
				int[] b = sumLines(a, lstart, lend);
				count += zeroes(b);
			}
		
		return count;
	}
	
	private int[] sumLines(int[][] a, int lstart, int lend) {
		int[] b = new int[a[0].length];
		for (int k = 0; k < a[0].length; k++) {
			for (int l = lstart; l <= lend; l++)
				b[k] += a[l][k];
		}

		return b;
	}

	private int zeroes(int[] b) {
		int count = 0;
		for (int i = 0; i < b.length; i++) {
			int s = 0;
			for (int j = i; j < b.length; j++) {
				s += b[j];
				if (s == 0) count++;
			}
				
		}

		return count;
	}

	// brute-force version: didn't pass the on-line judge
	public int solveBF(int[][] a) {
		if (a == null || a.length == 0 || a[0].length == 0)
			return 0;

		HashMap<String, Integer> hm = new HashMap<>();
		int n = a.length;
		int m = a[0].length;
		int count = 0;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				for (int k = i; k >= 0; k--)
					for (int l = j; l >= 0; l--) {					
						int s = sum(a, k, l, i, j, hm);
						if (s == 0) {
							count++;
							//printSubMatrix(a, k, l, i, j);
						}
					}
		
		return count;
	}

	private int sum(int[][] a, int k, int l, int i, int j, HashMap<String, Integer> hm) {
		int s = 0;
		if (k == i && l == j)
			s = a[i][j];
		else if (k == i) {
			String key1 = Integer.toString(i) + "_" + Integer.toString(l) + "_"
					+ Integer.toString(i) + "_" + Integer.toString(j-1);
			int s1 = hm.get(key1);

			s = s1 + a[i][j];	
		} else if (l == j) {
			String key1 = Integer.toString(k) + "_" + Integer.toString(j) + "_"
					+ Integer.toString(i-1) + "_" + Integer.toString(j);
			int s1 = hm.get(key1);

			s = s1 + a[i][j];
		} else {
			String key1 = Integer.toString(k) + "_" + Integer.toString(l) + "_"
					+ Integer.toString(i) + "_" + Integer.toString(j-1);
			int s1 = hm.get(key1);

			String key2 = Integer.toString(k) + "_" + Integer.toString(j) + "_"
					+ Integer.toString(i-1) + "_" + Integer.toString(j);
			int s2 = hm.get(key2);
			s = s1 + s2 + a[i][j];			
		}

		String key = Integer.toString(k) + "_" + Integer.toString(l) + "_"
				+ Integer.toString(i) + "_" + Integer.toString(j);
		hm.put(key, s);
		return s;
	}

	/*
	private void printSubMatrix(int[][] a,int k, int l, int i, int j) {
		for (int g = k; g <= i; g++) {
			System.out.print("[");
			for (int h = l; h <= j; h++) {
				System.out.print(a[g][h] + ", ");
			}
			System.out.print("]");
			System.out.println("");
		}
		
		System.out.println("");
	}
	*/

	// Their DP version (editorial) ...
	public int solveTheirDP(int[][] a) {
		if (a == null || a.length == 0 || a[0].length == 0) return 0;

		int rows = a.length;
		int columns = a[0].length;
		int count = 0;

		for (int leftColumn = 0; leftColumn < columns; leftColumn++) {
			int[] rowSums = new int[rows];

			for (int rightColumn = leftColumn; rightColumn < columns; rightColumn++) {
				for (int i=0;i<rows;i++)
					rowSums[i] += a[i][rightColumn];

				HashMap<Integer, ArrayList<Integer>> map = new HashMap<>();
				int curSum = 0;
				for (int i=0;i<rows;i++) {
					curSum += rowSums[i];
					if (curSum == 0) count++;

					ArrayList<Integer> indexes = map.get(curSum);
					if (indexes != null) count += indexes.size();
					else {
						indexes = new ArrayList<>();
						map.put(curSum, indexes);
					}
					
					indexes.add(i);
				}
			}
		}

		return count;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//int[][] a = new int[][]{
		//		{-8,  5,  7},
		//		{ 3,  7, -8},
		//		{ 5, -8,  9}};
		int[][] a = new int[][] {{-2, -1, 1, 2}};
		SubMatricesSumZero t = new SubMatricesSumZero();
		int r = t.solveBF(a);
		System.out.println("BF:" + r);
		
		r = t.solve(a);
		System.out.println("My DP:" + r);

		r = t.solveTheirDP(a);
		System.out.println("Their DP:" + r);

	}
}
