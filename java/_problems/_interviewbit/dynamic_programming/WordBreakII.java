package _problems._interviewbit.dynamic_programming;

/*
 * Given a string s and a dictionary of words dict, add spaces in s to construct a sentence
 * where each word is a valid dictionary word. Return all such possible sentences.
 * 
 * Note: Make sure the strings are sorted in your result.
 */

import java.util.Arrays;
import java.util.LinkedHashSet;

public class WordBreakII {

	public String[] wordBreak(String s, String[] dict) {
		if (s == null || s.length() == 0 || dict == null || dict.length == 0)
			return new String[] {};
		
		LinkedHashSet<String> ansHS = new LinkedHashSet<String>();
		
		Arrays.sort(dict);
		
		StringBuilder sb = new StringBuilder();
		helper(s, 0, dict, sb, ansHS);
		
		String[] ans = new String[ansHS.size()]; 
		ansHS.toArray(ans);
		return ans;
	}
	
	private void helper(String s, int i, String[] dict,
			StringBuilder sb, LinkedHashSet<String> ansHS) {

		if (i == s.length()) {
			ansHS.add(sb.toString());
			return;
		}
		
		for (int j = 0; j < dict.length; j++) {
			int limit = i + dict[j].length();
			if (limit > s.length()) continue;
			
			if (s.substring(i, limit).equals(dict[j])) {
				int idx = sb.length();
				
				if (i == 0) sb.append(dict[j]);
				else sb.append(" " + dict[j]);
				helper(s, limit, dict, sb, ansHS);
				if (i == 0) sb.delete(idx, idx + dict[j].length());
				else sb.delete(idx, idx + 1 + dict[j].length());
			}
		}
		
	}
	
	private void print(String[] l) {
		for (String s: l)
			System.out.println("[" +s + "]");
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String s = "aabbbabaaabbbabaabaab";
		String[] dict = new String[] {"bababbbb", "bbbabaa", "abbb", "a", 
				"aabbaab", "b", "babaabbbb", "aa", "bb"};

		WordBreakII t = new WordBreakII();
		String[] ans = t.wordBreak(s, dict);
		t.print(ans);
	}
}
