package _problems._interviewbit.dynamic_programming;

/*
 * Given a set of strings. Find the length of smallest string which has all the strings
 * in the set as substring.
 * 
 * Constraints:
 *  1) 1 <= Number of strings <= 18
 *  2) Length of any string in the set will not exceed 100.
 * 
 * Example:
 * Input: [�abcd�, �cdef�, �fgh�, �de�]
 * Output: 8 (Shortest string: �abcdefgh�)
 */

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;

public class ShortestCommonSuperstring {

	private class Element {
		int len;
		int last;
		public Element(int l, int i) {
			len = l;
			last = i;
		}
 	};

 	// T(n) = O(n.logn) + O(n^2) + O(n.2^n) =  O(n.2^n)
	public int solve(String[] str) {
		if (str == null || str.length == 0) return 0;

		// 1. eliminate duplicates: T(n) = O(nlogn)
		HashSet<String> hs = new HashSet<>(Arrays.asList(str));
		String[] a = new String[hs.size()];
		a = hs.toArray(a);
		
		// 2. eliminate substrings: : T(n) = O(n^2)
		Comparator<String> comp = new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return (o2.length() - o1.length());
			}
		};
		Arrays.sort(a, comp);
		int n = a.length;
		for (int i = 0; i < n; i++) {
			for (int j = i+1; j < n; j++)
				if (a[i].contains(a[j])) { swap(a, j, n-1); j--; n--; }
		}
		
		// 3. compute the length of the SCS for the remaining strings T(n) = O(n.2^n)
		int[][] overlaps = generateOverlaps(a);
		
		// init dp table.
		int combinations = 1<<n;
		Element[] dp = new Element[combinations];
		for (int i = 0; i < dp.length; i++) dp[i] = new Element(Integer.MAX_VALUE, -1);
		dp[0].len = 0;

		// ... do the work ...
		for(int mask = 0; mask < combinations; mask++) {
			for (int i = 0; i < n; i++) {
				// exclude self combinations
				if( (mask & (1 << i)) != 0) continue;

				int last = dp[mask].last;
				int with = mask | (1 << i);
				int new_len = dp[mask].len + (last == -1 ? a[i].length() : overlaps[last][i]);
				int old_len = dp[with].len;
				
				// update dp[with] with the smallest value
				if (new_len <= old_len) { dp[with].len = new_len; dp[with].last = i; }
			}
		}
		
		return dp[combinations-1].len;
	}
	
	private void swap(String[] a, int i, int j) {
		String temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}

	private int[][] generateOverlaps(String[] a) {
		int n = a.length;
		int[][] overlaps = new int[n][n];
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++) {
				if (i==j) {overlaps[i][j] = -1; continue; }
				overlaps[i][j] = a[j].length() - maxOverlap(a[i], a[j]);
			}

		return overlaps;
	}

	private int maxOverlap(String s1, String s2) {
		int n = s1.length();
		int m = s2.length();
		int len = Math.min(n, m);

		int max = 0;
		for (int i = 1; i < len; i++)
			if (s1.regionMatches(n-i, s2, 0, i)) max = i;

		return max;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		//String[] a = {"abcd", "cdef", "fgh", "de"};
		//String[] a = {"ab", "cd", "bc"};
		String[] a = {"xabc", "abc"};

		ShortestCommonSuperstring t = new ShortestCommonSuperstring();
		int ans = t.solve(a);
		System.out.println(ans);
	}
}
