package _problems._interviewbit.dynamic_programming;

/*
 * Given an array with non negative numbers, divide the array into two parts such that the average of both the parts is equal.
 * Return both parts (If exist).
 * If there is no solution. return an empty list.
 *
 * Input:
 *   [1 7 15 29 11 9]
 *
 * Output:
 *   [9 15] [1 7 11 29]
 */

import java.util.*;
public class EqualAveragePartition {
	
	// The brute-force version:
	// T(n) = O(n.S^n)
	private boolean isPossible_bf (
			ArrayList<Integer> a, int idx, int c_sum, int c_size, boolean[] sol) {

		if (c_size == 0 && c_sum == 0) return true;
		if (c_size == 0) return false;
		if (idx == a.size()) return false;

		boolean take = false, notake = false;
		if (c_sum - a.get(idx).intValue() >= 0) {
			sol[idx] = true;
			take = isPossible_bf(a, idx + 1, c_sum-a.get(idx).intValue(), c_size-1, sol);
		}
		if (!take) {
			sol[idx] = false;
			notake = isPossible_bf(a, idx + 1, c_sum, c_size, sol);
		}
		
		return (take||notake);
	}
	
	public ArrayList<ArrayList<Integer>> avgset_bf(ArrayList<Integer> a) {
		ArrayList<ArrayList<Integer>> ans = new ArrayList<ArrayList<Integer>>();
		if (a == null || a.size() < 2) return ans;
		Collections.sort(a);
		
		int n = a.size();
		int sum = 0;
		for (int i = 0; i < n; i++) sum += a.get(i).intValue();
		boolean[] sol = new boolean[n];

		int max_size = n/2;
		for (int c_size = 1; c_size <= max_size; c_size++) {
			if (sum * c_size % n != 0) continue;
			int c_sum = sum * c_size / n;

			if (isPossible_bf(a, 0, c_sum, c_size, sol) == true) {

				ArrayList<Integer> p1 = new ArrayList<>();
				ArrayList<Integer> p2 = new ArrayList<>();
				for (int i = 0; i < a.size(); i++) {
					if (sol[i] == true) p1.add(a.get(i).intValue());
					else p2.add(a.get(i).intValue());
				}
				
				ans.add(p1);
				ans.add(p2);
				break;
			} else Arrays.fill(sol, false);
		}
		
		return ans;
	}
	
	// The dynamic programming version:
	// T(n) = O(S.n^3)
	// S(n) = O(s.n^2)
	private Boolean isPossible_memo (ArrayList<Integer> a, int idx, int c_sum, int c_size,
									ArrayList<Integer> set1, Boolean[][][] memo) {
		
		if (c_size == 0) return (c_sum == 0);
		if (idx == a.size()) return false;
		if (memo[idx][c_size][c_sum] == false) return false;

		int x = a.get(idx).intValue();
		if (c_sum - x >= 0) {
			set1.add(x);
			
			if (isPossible_memo(a, idx + 1, c_sum-a.get(idx).intValue(), c_size-1, set1, memo) == true)
				return true;
			
			set1.remove(set1.size()-1);
		}
		
		if (isPossible_memo(a, idx + 1, c_sum, c_size, set1, memo) == true)
			return true;
		
		memo[idx][c_size][c_sum] = false;
		return memo[idx][c_size][c_sum];
	}
	
	private ArrayList<Integer> buildSet2(ArrayList<Integer> a, ArrayList<Integer> set1) {
		int n = a.size();
		int m = set1.size();
		ArrayList<Integer> set2 = new ArrayList<Integer>(n-m);
		int j = 0;
		for (int i = 0; i < n; i++) {
			int x = a.get(i);
			if (j < m && x == set1.get(j)) { j++; continue; }
			
			set2.add(x);
		}
		
		return set2;
	}
		
	public ArrayList<ArrayList<Integer>> avgset_memo(ArrayList<Integer> a) {
		ArrayList<ArrayList<Integer>> ans = new ArrayList<ArrayList<Integer>>();
		if (a == null || a.size() < 2) return ans;
		Collections.sort(a);
		
		int n = a.size();
		int sum = 0;
		for (int i = 0; i < n; i++) sum += a.get(i).intValue();

		Boolean[][][] memo = new Boolean[n+1][n+1][sum+1];
		for (int i = 0; i <= n; i++)
			for (int j = 0; j <= n; j++)
				for (int k = 0; k <= sum; k++)
					memo[i][j][k] = true;
			
		int half = (n+1)/2;
		for (int c_size = 1; c_size <= half; c_size++) {
			if (sum * c_size % n != 0) continue;
			
			int c_sum = sum * c_size / n;
			
			System.out.println(sum + "   " + c_sum + "   " + c_size);
			
			ArrayList<Integer> set1 = new ArrayList<Integer>(n);
			if (isPossible_memo(a, 0, c_sum, c_size, set1, memo) == false) continue;
			
			ArrayList<Integer> set2 = buildSet2(a, set1);
			if (!set2.isEmpty()) {
				ans.add(set1);
				ans.add(set2);
				break;
			}
		}
		
		return ans;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//Integer[] a = { 0, 0, 12, 0, 5, 5, 5, 5 };
		Integer[] a = { 0, 1, 11, 0, 2, 2, 2, 2, 2, 2, 2, 2 };
		//Integer[] a = { 0, 0, 0, 0, 0, 0};
		//Integer[] a = { 1, 7, 15, 29, 11, 9 };
		
		ArrayList<Integer> A = new ArrayList<Integer>(Arrays.asList(a));
		EqualAveragePartition t = new EqualAveragePartition();
		
		long start = System.nanoTime();
		System.out.println(t.avgset_bf(A));
		long end = System.nanoTime();
		
		System.out.println("Execution time: " + (end - start) + "   ns");
		System.out.println("Execution time: " + ((end - start)/1000000) + "   ms");
		
		start = System.nanoTime();
		System.out.println(t.avgset_memo(A));
		end = System.nanoTime();
		
		System.out.println("Execution time: " + (end - start) + "   ns");
		System.out.println("Execution time: " + ((end - start)/1000000) + "   ms");
	}
}
