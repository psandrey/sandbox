package _problems._interviewbit.dynamic_programming;

/*
 * Say you have an array for which the ith element is the price of a given stock on day i.
 * Design an algorithm to find the maximum profit.
 * You may complete as many transactions as you like (ie, buy one and sell one share
 * of the stock multiple times). However, you may not engage in multiple transactions
 * at the same time (ie, you must sell the stock before you buy again).
 */

public class BestTimeToBuyAndSellStocks_II {

	public int maxProfit(final int[] a) {
		if (a == null || a.length < 2) return 0;
		
		int n = a.length;
	    int profit = 0;
	    for (int j = 1; j < n; j++)
	        if (a[j] > a[j - 1]) profit += a[j] - a[j - 1];
	        
	    return profit;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = new int[]{5, 1, 2, 3, 4};
		BestTimeToBuyAndSellStocks_II t = new BestTimeToBuyAndSellStocks_II();
		System.out.println(t.maxProfit(a));
	}
}
