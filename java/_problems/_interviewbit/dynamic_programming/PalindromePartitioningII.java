package _problems._interviewbit.dynamic_programming;

/*
 * Given a string s, partition s such that every substring of the partition is a palindrome.
 * Return the minimum cuts needed for a palindrome partitioning of s.
 */

import java.util.Arrays;

public class PalindromePartitioningII {

	// DP: version
	public int minCut(String s) {
		if (s == null || s.length() < 2) return 0;
		
		int n = s.length();
		int[][] dp = new int[n][n];

		for (int sz = 1; sz <= n; sz++)
			for (int i = 0; i < n; i++) {
				if (n - i < sz) break;
				int j = i + sz - 1;
				
				// id the previous substring was palindrome (dp[i+1][j-1] have 0 curs)
				// and if the edges correspond, then the substring[i:j]
				// is also a palindrome.
				if (s.charAt(i) == s.charAt(j) && (j-i <= 2 || dp[i+1][j-1] == 0)) dp[i][j] = 0;
				else {
					// if it is not a palindrome, then take the minimum cuts from previous
					// substrings (of smaller lengths) + 1 cut.
					dp[i][j] = sz;
					for (int k = i; k < j; k++)
						dp[i][j] = Math.min(dp[i][j], 1 + dp[i][k] + dp[k+1][j]);
				}
			}
		
		return dp[0][n-1]; // the minimum number of cuts for the entire string
	}

	// brute-force version
	public int minCutBF(String s) {
		return minCutBF(s, 0, 0);
	}

	private int minCutBF(String s, int j, int cut) {
		if (j == s.length()) return cut-1;
		
		int min = Integer.MAX_VALUE;
		for (int i = j; i < s.length(); i++) {
			if (isPalindrome(s.substring(j, i+1))) {
				int cuts = minCutBF(s, i+1, cut+1);
				min = Math.min(min, cuts);
			}
		}
		
		return min;
	}
	
	private boolean isPalindrome(String s) {
		int i = 0, j = s.length()-1;
		
		while ( i < j) {
			if (s.charAt(i) != s.charAt(j)) return false;
			i++; j--;
		}
		
		return true;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		//String s = "aab";
		String s = "abcbm";
		
		PalindromePartitioningII t = new PalindromePartitioningII();
		int ans;
		ans = t.minCutBF(s);
		System.out.println("BF:" + ans);

		ans = t.minCut(s);
		System.out.println("DP:" + ans);
	}
}
