package _problems._interviewbit.dynamic_programming;

/*
 * Given a square matrix (n*m) which contains only 1 and 0, find the largest sub-square matrix
 * which only contains 1s. 
 */

public class MaxSquareBinaryMatrix {

	// DP approach
	public int maxSquareDP(int[][] a) {
		int n = a.length;
		int m = a[0].length;
		int[][]dp = new int[n][m];

		for (int i = 1; i < n; i++) dp[i][0] = a[i][0];
		for (int j = 1; j < m; j++) dp[0][j] = a[0][j];

		int max = 0;
		for (int i = 1; i < n; i++)
			for (int j = 1; j < m; j++) {
				if (a[i][j] == 1) {
					dp[i][j] = (1 + Math.min(dp[i-1][j-1], Math.min(dp[i-1][j], dp[i][j-1])));
					max = Math.max(max, dp[i][j]);
				}
			}

		return max;
	}

	// brute-force approach
	public int maxSquareBF(int[][] a) {
		int n = a.length;
		int m = a[0].length;
		int max = 0;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++) {
				if (a[i][j] == 1) {
					int candidateMax = helperBF(a, n, m, i, j);
					max = Math.max(max, candidateMax);
				}
			}

		return max;
	}
	
	private int helperBF(int[][] a, int n, int m, int i, int j) {
		if (i+1 < n && j+1 < m && a[i+1][j] == 1 && a[i][j+1] == 1 && a[i+1][j+1] == 1) {

			int r = helperBF(a, n, m, i, j+1);
			int d = helperBF(a, n, m, i+1, j);
			int s = helperBF(a, n, m, i+1, j+1);
			
			return 1 + Math.min(r, Math.min(d, s));
		}
		
		// we don't get here unless a[i][j] == 1, so we return 1;
		return 1;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[][] a = new int[][] {
			{0, 0, 0, 1, 1},
			{0, 1, 1, 1, 1},
			{0, 1, 1, 1, 1},
			{0, 1, 1, 1, 0},
			{0, 0, 0, 0, 1}};
			
		MaxSquareBinaryMatrix t = new MaxSquareBinaryMatrix();
		int r;
		
		r = t.maxSquareBF(a);
		System.out.println("BF:" + r);

		r = t.maxSquareDP(a);
		System.out.println("DP:" + r);

	}
}
