package _problems._interviewbit.dynamic_programming;

/*
 * A subsequence is a sequence that can be derived from another sequence by deleting
 *  some elements without changing the order of the remaining elements.
 *  Longest common subsequence (LCS) of 2 sequences is a subsequence, with maximal length, 
 *  which is common to both the sequences. 
 */

public class LongestCommonSubsequence {
	
	// DP version
	public int LCS(String A, String B) {
		char[] a = A.toCharArray();
		char[] b = B.toCharArray();
		int n = a.length;
		int m = b.length;
		int[][] dp = new int [n+1][m+1];
		
		for (int i = 0; i <= n; i++)
			for (int j = 0; j <= m; j++) {
				if (i == 0 || j == 0) { dp[i][j] = 0; continue; }

				if (a[i-1] == b[j-1]) dp[i][j] = dp[i-1][j-1] + 1;
				else dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]);
			}
	
		return dp[n][m];
	}

	// brute-force version
	public int LCSBF(String A, String B) {
		char[] a = A.toCharArray();
		char[] b = B.toCharArray();
		
		return helperBF(a, 0, b, 0);
	}
	
	private int helperBF(char[] a, int i, char[] b, int j) {
		if (i == a.length || j == b.length) return 0;
		
		if (a[i] == b[j]) return (1+ helperBF(a, i+1, b, j+1));
		
		int len1 = helperBF(a, i+1, b, j);
		int len2 = helperBF(a, i, b, j+1);
		return Math.max(len1, len2 );
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//String A = "ABCDGH";
		//String B = "AEDFHR";
		String A = "AGGTAB";
		String B = "GTAB";

		LongestCommonSubsequence t = new LongestCommonSubsequence();
		int r = t.LCSBF(A, B);
		System.out.println("BF:" + r);
		r = t.LCS(A, B);
		System.out.println("DP:" + r);
	}
}
