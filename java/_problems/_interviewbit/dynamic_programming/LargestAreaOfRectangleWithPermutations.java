package _problems._interviewbit.dynamic_programming;

/*
 * Given a binary grid i.e. a 2D grid only consisting of 0�s and 1�s,
 * find the area of the largest rectangle inside the grid such that all the cells
 * inside the chosen rectangle should have 1 in them.
 * You are allowed to permutate the columns matrix i.e. you can arrange
 * each of the column in any order in the final grid.
 * 
 * Please follow the below example for more clarity.
 * Lets say we are given a binary grid of 3 * 3 size.
 *   1 0 1
 *   0 1 0
 *   1 0 0
 *  
 * At present we can see that max rectangle satisfying the criteria mentioned
 * in the problem is of 1 * 1 = 1 area i.e either of the 4 cells which contain 1 in it.
 * Now since we are allowed to permutate the columns of the given matrix, we can take
 * column 1 and column 3 and make them neighbours. One of the possible configuration
 * of the grid can be:
 *   1 1 0
 *   0 0 1
 *   1 0 0
 * 
 * Now In this grid, first column is column 1, second column is column 3 and third
 * column is column 2 from the original given grid. Now, we can see that if we calculate
 * the max area rectangle, we get max area as 1 * 2 = 2 which is bigger than the earlier case.
 * Hence 2 will be the answer in this case.
 */

import java.util.Arrays;
import java.util.Stack;

public class LargestAreaOfRectangleWithPermutations {
	
	public int solve(int[][] a) {
		if (a == null || a.length == 0) return 0;

		int n = a.length;
		int m = a[0].length;
		int[][] b = new int[n][m];
		prepareHistogram(b, a);
		int max = 0;
		for (int i = 0; i < n; i++) {
			int[] hist = b[i];
			Arrays.sort(hist);
			int candidate = largestRectangleArea(hist);
			max = Math.max(max, candidate);
		}

		return max;
	}
	
	private void prepareHistogram(int[][] b, int[][] a) {
		int n = a.length;
		int m = a[0].length;
		for (int j = 0; j < m; j++) b[0][j] = a[0][j];

		for (int i = 1; i < n; i++)
			for (int j = 0; j < m; j++) {
				b[i][j] = (a[i][j] == 0) ? 0 : b[i-1][j] + a[i][j];
			}
	}
	
	private int largestRectangleArea(int[] hist) {
		int ans = 0;
		
		Stack<Integer> st = new Stack<Integer>();
		int i = 0, n = hist.length;
		while (i < n) {
			if (st.isEmpty() || hist[i] > hist[st.peek()]) {
				st.push(i); i++; continue; }
			
			int j = st.pop();
			int h = hist[j];
			int w = (st.isEmpty() ? i : i-st.peek()-1);
			int area = w*h;
			ans = Math.max(ans, area);
		}
		
		while (!st.isEmpty()) {
			int j = st.pop();
			int h = hist[j];
			int w = (st.isEmpty() ? i : i-st.peek()-1);
			int area = w*h;
			ans = Math.max(ans, area);			
		}
		
		return ans;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		LargestAreaOfRectangleWithPermutations t = new LargestAreaOfRectangleWithPermutations();
		int[][] a =  new int[][]
				{{0, 0, 1, 1, 0},
		     	 {0, 0, 1, 1, 1},
				 {1, 0, 1, 1, 0}};
		System.out.println(t.solve(a));
		//int[] hist = new int[]{1, 5, 4, 5, 1};
		//System.out.println(t.largestRectangleArea(hist));
	}
}
