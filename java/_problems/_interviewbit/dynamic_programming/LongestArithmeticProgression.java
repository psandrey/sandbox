package _problems._interviewbit.dynamic_programming;

/*
 * Find longest Arithmetic Progression in an integer array and return its length.
 * More formally, find longest sequence of indeces, 0 < i1 < i2 < � < ik < ArraySize(0-indexed)
 * such that sequence A[i1], A[i2], �, A[ik] is an Arithmetic Progression.
 * 
 * Note: The order matters... however there is a solution when the array is sorted...
 *		I guess it is more likely to be in an interview the one with the sorted array.
 *
 * Note: There are 4 different solutions presented for this problem:
 *       - brute-force
 *       - dynamic programming when the array is not sorted
 *       - dynamic programming when the array is sorted
 *       - dynamic programming bit-mask (just as an exercise)
 */

import java.util.HashMap;

public class LongestArithmeticProgression {
	
	// Version 1: brute-force
	// T(n) = O(n^3)
	// S(n) = O(1)
	public int solveV1(int[] a) {
		if (a == null || a.length == 0) return 0;

		int n = a.length;
		int maxLen = 1;
		for (int i = 0; i < n-1; i++)
			for (int j = i+1; j < n; j++) {
				int r = a[j]-a[i];
				int len = 2;
				int prev = a[j];
				for (int k = j+1; k < n; k++)
					if (a[k]-prev == r) { len++; prev = a[k]; }
				
				maxLen = Math.max(maxLen, len);
			}

		return maxLen;
	}

	// Version 2: dynamic programming
	// T(n) = O(n^2)
	// S(n) = O(n^2)
	public int solveV2(final int[] a) {
		if (a == null || a.length == 0) return 0;
		
		int n = a.length;
		if (n < 3) return n;

		// the dp table contains maximum length of the AP that ends with elements (i,j) 
		// hashMap tell us if there is a previous element/AP that can be attached
		//  to the current progression
		int[][] dp = new int[n][n];
		HashMap<Integer, Integer> pos = new HashMap<>();

		int ans = 2;
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				dp[i][j] = 2;
				
				int need = 2 * a[i] - a[j];

				if (!pos.containsKey(need)) continue;

				// the length of the AP that has the last two elements i and j is updated (enlarged)
				//  with the AP that ends with the element previous element i and element i
				dp[i][j] = Math.max(dp[i][j], dp[pos.get(need)][i] + 1);
				ans = Math.max(ans, dp[i][j]);
			}

			pos.put(a[i], i);
		}

		return ans;
	}
	
	// Version 3: dynamic programming - sorted array
	// T(n) = O(n^2)
	// S(n) = O(n^2)
	public int solveV3(final int[] a) {
		if (a == null || a.length == 0) return 0;
		
		int n = a.length;
		if (n < 3) return n;

		int[][] dp = new int[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++)
				dp[i][j] = (i == j ? 1 : 2);
		}
		  
		int ans = 2;
		for (int k = 1; k < n-1; k++) {
			int i = k-1;
			int j = k+1;

			// search for i k j, such that a[i] + a[j] = 2*a[k]
			while (i >= 0 && j <= n-1) {
				// 2 pointer trick to search in a sorted array
				if (a[i] + a[j] > 2*a[k]) i--;
				else if (a[i] + a[j] < 2*a[k]) j++;
				else {		
					dp[k][j] = dp[i][k] + 1; // extend AP (..i, k) with element j
					
					// keep track of the maximum length of AP
					ans = Math.max(ans, dp[k][j]);
					i--;j++;
				}
			}
		}
		  
		return ans; 
	}

	// Version 4: dynamic programming - bit mask (just as an exercise)
	// T(n) = O(n.2^n) - DP using bit mask
	// S(n) = O(2^n)
	private class Element {
		int last;
		int ratio;
		int len;
		public Element(int l, int r, int size) { last = l; ratio = r; len = size; }
	}
	
	public int solveV4(int[] a) {
		if (a == null || a.length == 0) return 0;
		
		int n = a.length;
		int size = (1<<n);
		Element[] dp = new Element[size];
		for (int mask = 0; mask < size; mask++) dp[mask] = new Element(-1, Integer.MAX_VALUE, -1); 
		for (int i = 0; i < n; i++) {
			int mask = 1<<i;
			dp[mask].len = 1;
			dp[mask].last = i;
			dp[mask].ratio = 0;
		}

		int maxLen = 1;
		int[][] ratio = computeRatio(a);
		for (int mask = 1; mask < size; mask++) {
			for (int i = 0; i < n; i++) {
				int last = dp[mask].last;
				if (i <= last) continue;
				
				int r = dp[mask].ratio;
				int len = dp[mask].len;
				
				int take = mask | (1 << i);
				if (len == 1) {
					dp[take].ratio = ratio[last][i];
					dp[take].len = len + 1;
					dp[take].last = i;
				} else if (ratio[last][i] == r) {
					dp[take].ratio = r;
					dp[take].len = len + 1;
					dp[take].last = i;
				} else {
					dp[take].ratio = r;
					dp[take].len = len;
					dp[take].last = last;
				}

				maxLen = Math.max(maxLen, dp[take].len);
			}
		}

		return maxLen;
	}
	
	private int[][] computeRatio(int[] a) {
		int n = a.length;
		int[][] ratio = new int[n][n];
		
		for (int i = 0; i < n-1; i++)
			for (int j = i; j < n; j++)
				ratio[i][j] = a[j] - a[i];
		
		return ratio;
	}

	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		int[] a = new int[] {2, 4, 5, 3};
		
		LongestArithmeticProgression t = new LongestArithmeticProgression();
		System.out.println("Longest arithmetic progression V1: " + t.solveV1(a));
		System.out.println("Longest arithmetic progression V2: " + t.solveV2(a));
		System.out.println("Longest arithmetic progression V3: " + t.solveV3(a));
		System.out.println("Longest arithmetic progression V4: " + t.solveV4(a));
	}
}
