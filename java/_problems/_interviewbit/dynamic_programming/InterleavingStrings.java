package _problems._interviewbit.dynamic_programming;

/*
 * Given s1, s2, s3, find whether s3 is formed by the interleaving of s1 and s2.
 */

public class InterleavingStrings {

	// DP version
	public int isInterleave(String S1, String S2, String S3) {
		if (S3 == null || (S1 == null && S2 == null))
			return 0;

		char[] s1 = S1.toCharArray();
		char[] s2 = S2.toCharArray();
		char[] s3 = S3.toCharArray();
		if (s3.length != s1.length + s2.length)
			return 0;

		int n = s1.length;
		int m = s2.length;
		boolean[][] dp = new boolean[n + 1][m + 1];
		dp[0][0] = true; // this doesn't matter
		
		// case where s2 is empty
		for (int i = 1; i <= n; i++ )
			if (s1[i - 1] == s3[i - 1]) dp[i][0] = true;
			else break;

		// case where s1 is empty
		for (int j = 1; j <= m; j++ )
			if (s2[j - 1] == s3[j - 1]) dp[0][j] = true;
			else break;
		
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++) {				
				int k = i + j - 1;
				dp[i][j] = false;
				
				// either s1 or s2
				if (s1[i-1] == s3[k] && s2[j-1] == s3[k])
					dp[i][j] = dp[i-1][j] || dp[i][j-1];
				// s1
				else if (s1[i-1] == s3[k])
					dp[i][j] = dp[i-1][j];
				// s2
				else if (s2[j-1] == s3[k])
						dp[i][j] = dp[i][j-1];
			}
		
		return dp[n][m] == true ? 1 : 0;
	}
	
	// divide&conquer version
	public boolean interStrBF(String S1, String S2, String S3) {
		char[] s1 = S1.toCharArray();
		char[] s2 = S2.toCharArray();
		char[] s3 = S3.toCharArray();
		if (s3.length != s1.length + s2.length)
			return false;
		
		return interStrBF(s1, 0, s2, 0, s3, 0);
	}
	
	private boolean interStrBF(char[] s1, int i, char[] s2, int j, char[] s3, int k) {
		if (k == s3.length) {
			if (i == s1.length && j == s2.length) return true;
			else return false;
		}
		
		if (i < s1.length && j < s2.length && s1[i] == s3[k] && s2[j] == s3[k])
			return interStrBF(s1, i+1, s2, j, s3, k+1) || interStrBF(s1, i, s2, j+1, s3, k+1);
		else if (i < s1.length && s1[i] == s3[k])
			return interStrBF(s1, i+1, s2, j, s3, k+1);
		else if (j < s2.length && s2[j] == s3[k])
			return interStrBF(s1, i, s2, j+1, s3, k+1);

		return false;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String s1 = "aabcc";
		String s2 = "dbbca";
		String s3 = "xadbbcbcac";
		//String s3 = "aadbbbaccc";
		
		//String s1 = "a";
		//String s2 = "a";
		//String s3 = "aa";
		
		
		InterleavingStrings t = new InterleavingStrings();
		boolean r = t.interStrBF(s1, s2, s3);
		System.out.println("BF:" + r);
		
		int rr = t.isInterleave(s1, s2, s3);
		System.out.println("DP:" + rr);
	}
}
