package _problems._interviewbit.dynamic_programming;

/*
 * Given two sequences S, T, count number of unique ways in sequence S, to form a subsequence
 *  that is identical to the sequence T.
 *  
 * Note: Edit-Distance variation.
 */

public class DistinctSubsequences {

	// DP version
	public int numDistinct(String S, String T) {
		if (S == null || T == null)
			return 0;

		char[] s = S.toCharArray();
		char[] t = T.toCharArray();

		if (s.length < t.length) return 0;
		else if (s.length == 1 && t.length == 1)
			if(s[0] == t[0]) return 1;
			else return 0;
		
		return distinct(s, t);
	}
	
	private int distinct(char[] s, char[] t) {
		int n = s.length;
		int m = t.length;
		
		int[][] dp = new int[n+1][m+1];
		for (int i = 0; i <= n; i++) dp[i][0] = 1;
		
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++) {
				// if s[i-1] = t[j-1] we see in how many ways s[0..i-2] => t[0..j-2] and in
				//  how many ways we can change s[0.i-2] => t[0..j-1] (delete s[i-1])
				// Note: the above line is taken into consideration if and only if s[i-1] = t[j-1]
				//       otherwise is zero
				if (s[i-1] == t[j-1]) dp[i][j] = dp[i-1][j-1] + dp[i-1][j];
				else dp[i][j] = dp[i-1][j];
			}
		
		return dp[n][m];
	}
	
	// divide&conquer version
	public int distinctBF(String S, String T) {
		char[] s = S.toCharArray();
		char[] t = T.toCharArray();
		
		return distinctBF(s, 0, t, 0);
	}
	
	private int distinctBF(char[] s, int i, char[] t, int j) {
		if (j == t.length) return 1;
		if (i == s.length) return 0;
		
		// if we have a match, then we pursuit two options:
		if (s[i] == t[j])
			return (distinctBF(s, i+1, t, j+1) + // take   s[i]
					distinctBF(s, i+1, t, j));   // delete s[i]
		
		return distinctBF(s, i+1, t, j); // no match, so only delete
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String S = "rabbbit";
		String T = "rabbit";
		//String S = "b";
		//String T = "a";

		
		DistinctSubsequences t = new DistinctSubsequences();
		int r = t.distinctBF(S, T);
		System.out.println("BF:" + r);
		
		r = t.numDistinct(S, T);
		System.out.println("DP:" + r);
	}
}
