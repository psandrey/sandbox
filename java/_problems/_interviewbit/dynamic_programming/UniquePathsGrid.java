package _problems._interviewbit.dynamic_programming;

/*
 * Given a grid of size m * n, lets assume you are starting at (1,1) and your goal
 * is to reach (m,n). At any instance, if you are on (x,y), you can either go to
 * (x, y + 1) or (x + 1, y). Now consider if some obstacles are added to the grids.
 * How many unique paths would there be?
 * An obstacle and empty space is marked as 1 and 0 respectively in the grid.
 */

public class UniquePathsGrid {

	public int uniquePathsWithObstacles(int[][] grid) {
		if (grid == null || grid.length == 0) return 0;
		int n = grid.length;
		int m = grid[0].length;
		if (grid[n-1][m-1] == 1) return 0;

		int[][] dp = new int[n][m];
		for (int i = 0; i < n; i++)
			if (grid[i][0] == 0) dp[i][0] = 1;
			else break;
		for (int j = 0; j < m; j++)
			if (grid[0][j] == 0) dp[0][j] = 1;
			else break;
		
		for (int i = 1; i < n; i++)
			for (int j = 1; j < m; j++) {
				if (grid[i-1][j] == 1 && grid[i][j-1] == 1) dp[i][j] = 0;
				else if (grid[i-1][j] == 1) dp[i][j] = dp[i][j-1];
				else if (grid[i][j-1] == 1) dp[i][j] = dp[i-1][j];
				else dp[i][j] = dp[i][j] = dp[i][j-1] + dp[i-1][j];
			}
		return dp[n-1][m-1];
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		int[][] grid = new int[][] {
			{0,0,0},
			{0,0,0},
			{1,0,0}
		};
		
		UniquePathsGrid t = new UniquePathsGrid();
		int ans = t.uniquePathsWithObstacles(grid);
		System.out.println(ans);
	}
}
