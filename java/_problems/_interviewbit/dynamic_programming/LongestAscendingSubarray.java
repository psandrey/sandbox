package _problems._interviewbit.dynamic_programming;

public class LongestAscendingSubarray {

	private int lenOfLongestAscendingSubarray(int[] a) {
		if (a == null || a.length == 0)
			return 0;
		int n = a.length;
		int[] dp = new int[n];
		dp[0] = 1;
		int lenMax = dp[0];
		for (int i = 0; i < n; i++) {
			dp[i] = 1;
			for (int j = 0; j < i; j++)
				if (a[i] > a[j])
					dp[i] = Math.max(dp[i], dp[j] + 1);
			
			lenMax = Math.max(lenMax, dp[i]);
		}

		return lenMax;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		int[] a = {1, 5, 5, 7, 4};
		LongestAscendingSubarray t = new LongestAscendingSubarray();
		int max = t.lenOfLongestAscendingSubarray(a);
		System.out.println(max);
	}
}
