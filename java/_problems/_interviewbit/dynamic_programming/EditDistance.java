package _problems._interviewbit.dynamic_programming;

/*
 * Given two words A and B, find the minimum number of steps required to convert A to B.
 * (each operation is counted as 1 step.)
 * 
 * You have the following 3 operations permitted on a word:
 * Insert a character
 * Delete a character
 * Replace a character
 */

public class EditDistance {
	
	// the naive approach
	private int LevDistNaive(String s1, String s2) {
		if (s1.isEmpty())
			return s2.length(); // only insertions
		if (s2.isEmpty())
			return s1.length(); // only deletions

		if (s1.charAt(0) == s2.charAt(0))
			return LevDistNaive(s1.substring(1), s2.substring(1)); // this is the same as substitution, but kept for clarity
		else {
			int delete = LevDistNaive(s1.substring(1), s2) + 1;  //delete
			int insert = LevDistNaive(s1, s2.substring(1)) + 1;  // insert
			int substitution = LevDistNaive(s1.substring(1), s2.substring(1)) +
					(s1.charAt(0) == s2.charAt(0) ? 0 : 1); // substitution
			
			return min(delete, insert, substitution);
		}
	}


	private int min(int... numbers) {
		int min = Integer.MAX_VALUE;
		for (int num: numbers)
			min = Math.min(min, num);
		return min;
	}
	
	// the dynamic programming approach
	private int LevDistDP(String s1, String s2) {
		if (s1 == null || s2 == null)
			return 0;
		
		int n = s1.length();
		int m = s2.length();
		if (n == 0 || m == 0)
			return 0;

		int[][] dp = new int[m + 1][n + 1];
		for (int i = 0; i <= n; i++) dp[0][i] = i;
		for (int i = 0; i <= m; i++) dp[i][0] = i;

		for (int i = 1; i <= m; i++)
			for (int j = 1; j <= n; j++) {
				if (s1.charAt(j-1) == s2.charAt(i-1))
					dp[i][j] = dp[i-1][j-1];
				else
								        // sub      del         ins
					dp[i][j] = 1 +  min(dp[i][j-1], dp[i-1][j], dp[i-1][j-1]);
			}

		return dp[m][n];
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//String s1 = "b";
		//String s2 = "a";
		
		String s1 = "abc";
		String s2 = "axcy";
		
		EditDistance t = new EditDistance();
		int min = t.LevDistNaive(s1, s2);
		System.out.println(min);
		
		min = t.LevDistDP(s1, s2);
		System.out.println(min);
	}
}
