package _problems._interviewbit.dynamic_programming;

/*
 * Given a string s and a dictionary of words dict, determine if s can be segmented
 *  into a space-separated sequence of one or more dictionary words.
 */

public class WordBreak {

	public int wordBreak(String s, String[] dict) {
		if (s == null || s.length() == 0 || dict == null || dict.length == 0)
			return 0;
	
		int n = s.length();
		boolean[] dp = new boolean[n+1];
		dp[0] = true;

		for (int i = 0; i < n; i++) {
			if (dp[i] == false) continue;
			
			for (int j = 0; j < dict.length; j++) {
				int limit = i+dict[j].length();
				if (limit > s.length()) continue;
				
				if (s.substring(i, limit).equals(dict[j])) dp[limit] = true;
			}
		}
		
		return dp[n] == true ? 1 : 0;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String s = "aaabc";
		String[] dict = new String[] {"aa", "a", "abc"};
		//String s = "a";
		//String[] dict = new String[] {"x"};
		
		WordBreak t = new WordBreak();
		int r = t.wordBreak(s, dict);
		System.out.println(r);
	}
}
