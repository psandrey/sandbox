package _problems._interviewbit.dynamic_programming;

/*
 * There is a rod of length N lying on x-axis with its left end at x = 0 and right end at x = N.
 * Now, there are M weak points on this rod denoted by positive integer values(all less than N)
 * A1, A2, �, AM. You have to cut rod at all these weak points. You can perform these cuts in any order.
 * After a cut, rod gets divided into two smaller sub-rods. Cost of making a cut is the length of
 * the sub-rod in which you are making a cut.
 * 
 * Your aim is to minimize this cost. Return an array denoting the sequence in which you will make cuts.
 * If two different sequences of cuts give same cost, return the lexicographically smallest.
 */

import java.util.ArrayList;
import java.util.HashMap;

public class RodCutting {

	// DP: bottom-up approach
	ArrayList<Integer> result;
	int[] cuts;
	int[][] parent;
	public ArrayList<Integer> rodCutBU(int rod, int[] scores) {
		int n = scores.length + 2;
		cuts = new int[n];
		cuts[0] = 0;
		for (int i = 0; i < scores.length; i++)
			cuts[i + 1] = scores[i];
		cuts[n - 1] = rod;

		long[][] dp = new long[n][n];
		parent = new int[n][n];
		for (int len = 1; len <= n; len++) {
			for (int s = 0; s < n - len; s++) {
				int e = s + len;
				for (int k = s + 1; k < e; k++) {
					long sum = cuts[e] - cuts[s] + dp[s][k] + dp[k][e];
					if (dp[s][e] == 0 || sum < dp[s][e]) {
						dp[s][e] = sum;
						parent[s][e] = k;
					}
				}
			}
		}
		
		result = new ArrayList<>();
		backTrack(0, n - 1);
		
		return result;
	}
	
	private void backTrack(int s, int e) {
		if (s + 1 >= e) {
			return;
		}		
		
		result.add(cuts[parent[s][e]]);
		backTrack(s, parent[s][e]);
		backTrack(parent[s][e], e);
	}

	// DP: top-down approach (memorization)
	public int[] rodCutTD(int n, int[] a) {
		helper(a, 0, n);
		
		int[] r = new int[a.length];
		buildResponse(0 , n, r, -1);
		return r;
	}
	
	private class Piece {
		long cost;
		int cut;
		public Piece(int c, int k) {
			cost = c;
			cut = k;
		}
	}
	private HashMap<String, Piece> mem = new HashMap<String, Piece>();

	private Piece helper(int[] a, int start, int end) {
		String key = Integer.toString(start) + "_" + Integer.toString(end);
		if (mem.containsKey(key))
			return mem.get(key);
		
		int k = 0;
		long minCut = Long.MAX_VALUE;
		for (int i = 0; i < a.length; i++) { // O(m)
			if (a[i] <= start) continue;
			if (a[i] >= end) break;
	
			Piece pieceLeft  = helper(a, start, a[i]);
			Piece pieceRight = helper(a, a[i] , end );
			if (minCut > pieceLeft.cost + pieceRight.cost) {
				minCut = pieceLeft.cost + pieceRight.cost;
				k = a[i];
			}
		}

		Piece piece = new Piece(Integer.MAX_VALUE, 0);

		if (minCut != Integer.MAX_VALUE)
			piece.cost = (end - start) + minCut;
		else
			piece.cost = 0;
		piece.cut = k;
		
		mem.put(key, piece);
		return piece;
	}
	
	private int buildResponse(int s, int e, int[] r, int depth) {
		String key = Integer.toString(s) + "_" + Integer.toString(e);
		Piece p = mem.get(key);
		if (p.cut == 0)
			return depth;
		
		r[++depth] = p.cut;
		int nextDepth = buildResponse(s, p.cut, r, depth);
		nextDepth = buildResponse(p.cut, e, r, nextDepth);
		return nextDepth;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//int n = 7;
		//int[] a = { 1, 2, 3, 6 };
		
		int n = 500;
		int[]  a = { 1, 2, 13, 17, 28, 29, 31, 40, 56, 57, 67, 72, 96, 103, 128, 136, 151, 174, 202, 220, 221, 232, 262, 273, 278, 305, 314, 315, 319, 332, 333, 344, 346, 361, 367, 376, 380, 387, 397, 403, 404, 408, 414, 438, 450, 452, 469, 485 };
		
		RodCutting t = new RodCutting();

		int[] r = t.rodCutTD(n, a);
		System.out.print("[");
		for (int i = 0; i < r.length; i++)
			System.out.print(r[i] + ", ");
		System.out.print("]");
		System.out.println();

		ArrayList<Integer> r2 = t.rodCutBU(n, a);
		System.out.println(r2);
	}
}
