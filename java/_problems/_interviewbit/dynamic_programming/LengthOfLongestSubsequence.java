package _problems._interviewbit.dynamic_programming;

/*
 * Given an array of integers, find the length of longest subsequence
 * which is first increasing then decreasing.
 * 
 * Example:
 *   For the given array [1 11 2 10 4 5 2 1]
 *   Longest subsequence is [1 2 10 4 2 1]
 *   
 *   Return value 6
 */

public class LengthOfLongestSubsequence {

	public int longestSubsequenceLength(final int[] a) {
		if (a == null || a.length == 0) return 0;
		int n = a.length;		
		
		int[] dpLeft = new int[n];
		dpLeft[0] = 1;
		int ans = 1;
		for (int i = 1; i < n; i++) {
			dpLeft[i] = 1;
			for (int j = 0; j < i; j++) {
				if (a[i] > a[j]) dpLeft[i] = Math.max(dpLeft[i], dpLeft[j] + 1);
			}
		}

		int[] dpRight = new int[n];
		dpRight[n-1] = 1;
		for (int i = n-2; i >= 0; i--) {
			dpRight[i] = 1;
			for (int j = i+1; j < n; j++) {
				if (a[i] > a[j]) dpRight[i] = Math.max(dpRight[i], dpRight[j] + 1);
			}
		}
		
		for (int i = 1; i < n; i++)
			ans = Math.max(ans, dpLeft[i]+dpRight[i]-1);

		ans = Math.max(ans, dpLeft[n-1]);
		ans = Math.max(ans, dpRight[0]);
		return ans;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//int [] a = new int[] {1, 11, 2, 10, 4, 5, 2, 1};
		int [] a = new int[] {80, 60, 30, 40, 20, 10};
		LengthOfLongestSubsequence t = new LengthOfLongestSubsequence();
		System.out.println(t.longestSubsequenceLength(a));
	}
}
