package _problems._interviewbit.dynamic_programming;

/*
 * Given a string containing just the characters '(' and ')', find the length of the longest valid
 * (well-formed) parentheses substring.
 * 
 * For "(()", the longest valid parentheses substring is "()", which has length = 2.
 * Another example is ")()())", where the longest valid parentheses substring is "()()",
 * which has length = 4.
 */

import java.util.Stack;

public class LongestValidParentheses {
	
	public int longestValidParentheses(String s) {
		if (s == null || s.length() == 0) return 0;

		Stack<int[]> st = new Stack<>();
		int max = 0;
		
		// codification: 0->'(', 1->')'
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c == ')') {
				if (!st.isEmpty() && st.peek()[0] == 0) {
					st.pop();
					// the index of the start before the last mistake in the parentheses flow 
					int j = (st.isEmpty() ? -1 : st.peek()[1]);
					
					// update the maximum length
					max = Math.max(max, i - j);
				} else
					// this is done to solve situation like: ")()"
					st.push(new int[] {1, i});
			} else
				st.push(new int[] {0, i});
		}

		return max;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String s = "))()()";
		
		LongestValidParentheses t = new LongestValidParentheses();
		int r = t.longestValidParentheses(s);
		System.out.println(r);
	}
}
