package _problems._interviewbit.dynamic_programming;

/*
 * Given an array of non-negative integers, you are initially positioned at the first index
 *  of the array. Each element in the array represents your maximum jump length at that position.
 *  Your goal is to reach the last index in the minimum number of jumps.
 */

public class MinJumpsArray {

	// Greedy version: T(n) = O(n)
	public int jump(int[] a) {
		if (a == null) return -1;
		else if (a.length == 1)	return 0;
		else if (a[0] == 0) return -1;
		
		int limit = a[0];
		int steps = a[0];
		int jumps = 1;
		for (int i = 1; i < a.length; i++) {
			if (i == a.length - 1) return jumps;
			
			limit = Math.max(limit, i+a[i]);
			
			// we consume steps as we go
			if (--steps > 0) continue;
			
			// if we pass the quantum of maximum steps (i.e. limit)
			// it means we cannot jump to the end)
			if (i >= limit) return -1;
			
			// we just consumed the current steps, so one more jump
			jumps++;
			
			// maximum number of steps we can make from here are the remaining limit after the
			// steps we've just consumed.
			steps = limit - i;
			
			// if limit is beyond length of the array we are sure we can reach at the end.
			if (limit >= a.length - 1) return jumps;
		}

		return -1;
	}

	// DP v1: T(n) = O(n^2)
	public int jumpV1(int[] a) {
		if (a == null)
			return -1;
		
		int n = a.length;
		if (n == 1)	return 0;
		if (a[0] == 0) return -1;
		
		int[] dp = new int[n];
		dp[0] = 1;
		for (int i = 1; i < n; i++) {
			int min = Integer.MAX_VALUE;
			for (int j = 0; j < i; j ++) {
				if (dp[j] > 0 && i <= j + a[j])
					min = Math.min(min, dp[j] + 1);
			}
			dp[i] = (min == Integer.MAX_VALUE) ? 0 : min;
		}
		
		return (dp[n-1] == 0 ? -1 : dp[n-1] - 1);
	}
	
	// DP v2: T(n) = O(n^2)
	public int jumpV2(int[] a) {
		if (a == null) return -1;
		
		int n = a.length;
		if (n == 1)	return 0;
		if (a[0] == 0) return -1;
		
		int[] dp = new int[n];
		
		dp[0] = 1;
		for (int i = 0; i < n-1; i++) {
			if (a[i] == 0 || dp[i] == 0) continue;
			
			for (int j = i+1; j <= Math.min(n-1, i + a[i]); j++) {
				dp[j] = (dp[j] == 0 ? dp[i] + 1 : Math.min(dp[j], dp[i]+1));
			}
		}
		
		return (dp[n - 1] == 0 ? -1 : dp[n-1] - 1);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = {3,2,2,2,0,0,0};

/*
		int[] a = new int[] {
				23, 18, 19, 17, 41, 0, 22, 0, 15, 48,
				5, 0, 25, 29, 33, 16, 35, 0, 45, 46,
				37, 46, 13, 0, 9, 0, 47, 26, 0, 46,
				33, 0, 40, 20, 42, 18, 7, 0, 20, 0,
				31, 17, 34, 0, 0, 18, 0, 6, 16, 7,
				23, 0, 13, 46, 49, 33, 43, 15, 18, 29,
				40, 42, 50, 2, 37, 0, 13, 0, 49, 4,
				34, 47, 34, 48, 0, 0, 0, 49, 22, 0,
				1, 35, 47, 33, 3, 19, 23, 39, 0, 14,
				9, 0, 4, 7, 26, 0, 6, 31, 0, 13, 44,
				0, 1, 49, 5, 40, 50, 4, 0, 13, 38, 35,
				0, 0, 48, 0, 35, 26, 0, 47, 38, 35, 0,
				0, 0, 16, 0, 33, 9, 29, 39, 38, 44, 25,
				0, 0, 50, 13, 0, 44, 11, 1, 0, 45, 0,
				11, 37, 0, 28, 23, 4, 7, 0, 0, 29, 44,
				0, 29, 0, 0, 25, 40, 0, 0, 47, 12, 4,
				35, 46, 40, 5, 47, 35, 3, 27, 0, 44,
				22, 48, 45, 32, 30, 0, 13, 0, 0, 4, 9,
				0, 20, 43, 37, 0, 39, 46, 29, 33, 21,
				50, 0, 19, 36, 0, 6, 45, 21, 0, 40, 0,
				0, 50, 4, 0, 32, 0, 28, 0, 44, 18, 30,
				32, 0, 12, 10, 0, 0, 8, 43, 0, 37, 48,
				0, 14, 11, 23, 27, 29, 44, 21, 1, 0, 0,
				0, 30, 24, 0, 21, 10, 41, 35, 49, 25, 0,
				43, 39, 15, 48, 0, 19, 28, 40, 0, 16, 30,
				43, 6, 19, 5, 32, 35, 15, 26, 47, 0, 37,
				40, 41, 49, 0, 0, 20, 11, 0, 46, 0, 29,
				0, 22, 0, 0, 0, 25, 0, 43, 32, 0, 0, 42,
				0, 10, 31, 32, 3, 0, 45, 49, 33, 50, 13,
				16, 40, 46, 19, 35, 20, 16, 5, 32, 0, 0,
				29, 16, 14, 0, 44, 23, 0, 2, 15, 15, 12,
				48, 50, 20, 7, 0, 30, 32, 45, 0, 40, 0, 1,
				44, 0, 13, 0, 21, 47, 0, 0, 9, 0, 3, 44,
				28, 50, 0, 48, 27, 4, 31, 6, 43, 0, 2, 0,
				15, 41, 20, 0, 0, 5, 22, 0, 48, 25, 30,
				21, 27, 14, 5, 7, 45, 21, 20, 0, 13, 0,
				13, 0, 14, 36, 36, 21, 0, 29, 30, 0, 18,
				18, 12, 0, 0, 17, 39, 44, 21, 14, 0, 26,
				45, 11, 11, 43, 35, 30, 5, 0, 30, 1, 0,
				34, 0, 13, 21, 0, 0, 45, 48, 32, 41, 5,
				0, 0, 1, 34, 50, 30, 25, 12, 26, 0, 43,
				0, 34, 21, 0, 23, 48, 0, 0, 0, 0, 0, 45,
				43, 5, 11, 34, 0, 42, 9, 44, 38, 0, 3, 
				42, 50, 0, 0, 26, 50, 17, 27, 23, 13, 3,
				17, 4, 8, 3, 32, 47, 36, 13, 10, 0, 9, 
				43, 19, 32, 0, 43, 40, 0, 16, 0, 0, 24, 
				43, 28, 49, 33, 49, 23, 8, 48, 42, 18,
				35, 1, 13, 18, 5, 27, 10, 35, 3, 45, 2,
				1, 7, 40, 40, 0, 0, 44, 6, 0, 29, 0, 8,
				34, 28, 0, 23, 20, 24, 1, 2, 47, 20, 5,
				16, 0, 37, 25, 0, 0, 26, 7, 0, 46, 40, 
				3, 34, 6, 2, 0, 22, 0, 46, 1, 37, 29, 3,
				1, 0, 0, 0, 0, 6, 19, 0, 0, 38, 23, 18, 
				38, 26, 28, 40, 49, 9, 47, 26, 0, 0, 2, 
				0, 0, 43, 0, 3, 18, 1, 48 };
*/
		MinJumpsArray t = new MinJumpsArray();
		int r;
		r = t.jumpV1(a);
		System.out.println(r);

		r = t.jumpV2(a);
		System.out.println(r);
		
		r = t.jump(a);
		System.out.println(r);

	}
}
