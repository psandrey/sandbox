package _problems._interviewbit.math;

/*
 * There are A cities numbered from 1 to A. You have already visited M cities, 
 * the indices of which are given in an array B of M integers.
 *
 * If a city with index i is visited, you can visit either the city with index i-1 (i >= 2) or
 * the city with index i+1 (i < A) if they are not already visited.
 * Eg: if N = 5 and array M consists of [3, 4], then in the first level of moves, you can either visit 2 or 5.
 *
 * You keep visiting cities in this fashion until all the cities are not visited.
 * Find the number of ways in which you can visit all the cities modulo 10^9+7.
 *
 * Constraints:
 * 1 <= A <= 1000
 * 1 <= M <= A
 * 1 <= B[i] <= A
 */

public class CityTour {

	public int solve(int n, int[] b) {
		int m = b.length;
		
		// pre-compute binomial coefficients
		int[][] bcoef = new int[n + 1][n + 1];
		bcoef[0][0] = 1;
		for (int i = 1; i <= n; i++) {
			bcoef[i][0] = 1;
			for (int j = 1; j < i; j++) bcoef[i][j] = bcoef[i - 1][j - 1] + bcoef[i - 1][j];
			bcoef[i][i] = 1;
		}

		// pre-compute power of 2
		int[] pow2 = new int[n + 1];
		pow2[0] = 1;
		for (int j = 1; j <= n; j++) pow2[j] = pow2[j - 1] * 2;
		
		int size = b[0] - 1; // length of tail
		int ways = 1; // tail has only one way to be visited
		for(int j = 1; j < m; j++) {
			if (b[j] == b[j - 1] + 1) continue;
			
			int csize = b[j] - b[j - 1] - 1; // size of current segment
			int cways = pow2[csize - 1]; // ways to visit a segment with both heads visited
			ways = ways * cways * bcoef[size + csize][csize];
			
			size += csize;
		}
		
		// include the last cities (they have only one head visited)
		int csize = n - b[m - 1]; // size of head
		if (csize > 0) ways = ways * bcoef[size + csize][csize];
		
		return ways;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int n = 14;
		int[] b = {3, 7, 11};
		
		CityTour t = new CityTour();
		System.out.println(t.solve(n, b));	
	}
}
