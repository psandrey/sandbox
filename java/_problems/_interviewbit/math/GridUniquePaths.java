package _problems._interviewbit.math;

/*
 * A robot is located at the top-left corner of an A x B grid.
 * 
 * The robot can only move either down or right at any point in time.
 * The robot is trying to reach the bottom-right corner of the grid (marked �Finish�
 * in the diagram below).
 * How many possible unique paths are there? 
 */

public class GridUniquePaths {

	public int uniquePaths(int rows, int cols) {
		if (rows == 0 && cols == 0) return 1;
		if (rows == 1 || cols == 1) return 1;
		
		return binCoef(rows+cols-2, cols-1);
	}

	private int binCoef(int n, int k) {
		if (n == k) return 1;

		k = Math.min(k, n-k);
		long ans = 1L;
		for (int i = 1; i <= k; i++) {
			ans *= (long)(n - i + 1);
			ans /= (long)(i);
		}

		return (int)(ans);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
	
		int n = 2,
			m = 3;
		GridUniquePaths t = new GridUniquePaths();
		int ans = t.uniquePaths(n, m);
		System.out.println(ans);
	}
}
