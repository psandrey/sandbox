package _problems._interviewbit.math;

/*
 * Given an array of N non-negative integers, find the sum of hamming distances
 *  of all pairs of integers in the array.
 * 
 *  Return the answer modulo 1000000007.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SumPairwiseHammingDistance {
	
	public int hammingDistance(final List<Integer> A) {
		if (A == null || A.size() == 0) return 0;

		long m = 1000000007;
		long n = A.size();
		long c = 0;
		for (int i = 0; i < 32; i++) {
			int mask = (1<<i);
			
			long bitCount = 0;
			for (Integer a: A)
				if ((a & mask) == mask) bitCount++;
			
			c = (c%m) + ((2L * bitCount * (n-bitCount))%m);
		}

		return (int)(c%m);
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[] a = new Integer[] {81, 13, 2, 7, 96};
		List<Integer> l = new ArrayList<Integer>(Arrays.asList(a));
		
		SumPairwiseHammingDistance t = new SumPairwiseHammingDistance();
		int ans = t.hammingDistance(l);
		System.out.println(ans);
	}
}
