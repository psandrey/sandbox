package _problems._interviewbit.math;

/*
 * Given 2 non negative integers m and n, find gcd(m, n).
 */

public class GreatestCommonDivisor {

	public int gcd(int a, int b) {
		if (a == 0 && b == 0) return -1;
		if (a == 0) return b;
		if (b == 0 || a == b) return a;
		
		if (a < b) { int t = a; a = b; b = t;}
		
		while (b != 0) {
			int r = a % b;
			a = b;
			b = r;
		}
		
		return a;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int a = 128;
		int b = 244448;
		GreatestCommonDivisor t = new GreatestCommonDivisor();
		int ans = t.gcd(a, b);
		System.out.println(ans);
	}
}
