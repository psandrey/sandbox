package _problems._interviewbit.math;

/*
 * Determine whether an integer is a palindrome. Do this without extra space.
 *
 * A palindrome integer is an integer x for which reverse(x) = x where reverse(x) is x with its digit reversed.
 * Negative numbers are not palindromic.
 *
 * Example :
 *   Input : 12121
 *   Output : True
 *
 *   Input : 123
 *   Output : False
 */

public class PalindromeInteger {

	public int isPalindrome(int a) {
		if (a == 0) return 1;
		if (a < 0) return 0;

        int rev = 0, b = a;
        while (b != 0) {
            rev = rev*10 + b%10;
            b /= 10;
        }
        
        if (rev != a) return 0;
        return 1;
/*
		int n = (int) Math.floor(Math.log10((double)a));
		if (n == 0) return 1;
		
		int d = (int) Math.pow(10, n);
		int h = a / d;
		for (int j = 0; j < (n + 1)/2; j++) {
			if (h != a%10) return 0;
			
			h = (a - (a/d) * d) / (d / 10);
			a /= 10;
			d /= 100;
		}		

		return 1;
*/
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int a = 1221;
		System.out.println(((-123)%10));
		PalindromeInteger t = new PalindromeInteger();
		System.out.println(t.isPalindrome(a));
	}
}
