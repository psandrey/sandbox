package _problems._interviewbit.math;

/*
 * Given a string, find the rank of the string amongst its permutations sorted lexicographically.
 * Note: that the characters might be repeated. If the characters are repeated, we need to look at the rank
 * in unique permutations.
 * Look at the example for more details.
 *
 * Example :
 *   Input : 'aba'
 *   Output : 2
 *
 * The order permutations with letters 'a', 'a', and 'b' :
 *   aab
 *   aba
 *   baa
 * The answer might not fit in an integer, so return your answer % 1000003
 *
 *  Note: 1000003 is a prime number
 */

public class SortedPermutationRankWithRepeats {
	
	public int findRank(String a) {
		if (a.isEmpty()) return 0;
		int n = a.length();
		
		// pre-compute factorial
		int[] fact = new int[n]; fact[0] = 1;
		for (int j = 1; j < n; j++) fact[j] = fact[j - 1] * j;
		
		// counter (duplicates) and less right
		int[] less_right = new int[256];
		int[] counter = new int[256];
		for (int j = 0; j < 256; j++) { counter[j] = 0; less_right[j] = 0; }
		for (int j = 0; j < n; j++) { counter[a.charAt(j)]++; less_right[a.charAt(j)]++; }
		for (int j = 1; j < 256; j++) less_right[j] += less_right[j - 1];

		// compute rank
		int rank = 1;
		for (int j = 0; j < n; j++) {
			int ord = a.charAt(j);

			// compute duplicate permutations
			int dup_perm = 1;
			for (int i = 0; i <= ord; i++) {
				if (counter[i] <= 1) continue;
				dup_perm *= fact[counter[i]];
			}
			
			// SUM OF: permutations of less right divided by duplicate permutations
			rank += (less_right[ord - 1] * fact[n - 1 - j]) / dup_perm;

			// update counter and less_right
			counter[ord]--;
			for (int i = ord; i < 256; i++) less_right[i]--;

		}
		
		return rank;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String a = "bbdaaa";
		
		SortedPermutationRankWithRepeats t = new SortedPermutationRankWithRepeats();
		System.out.println(t.findRank(a));
	}
}
