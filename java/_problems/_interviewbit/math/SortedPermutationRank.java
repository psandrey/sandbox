package _problems._interviewbit.math;

/*
 * Given a string, find the rank of the string amongst its permutations sorted lexicographically.
 * Assume that no characters are repeated.
 *
 * Example :
 *   Input : 'acb'
 *   Output : 2
 *
 * The order permutations with letters 'a', 'c', and 'b' :
 * abc
 * acb
 * bac
 * bca
 * cab
 * cba
 * The answer might not fit in an integer, so return your answer % 1000003
 */

public class SortedPermutationRank {

	public int findRank(String a) {
		if (a.isEmpty()) return 0;
		
		// letters rank
		int[] letters_rank = new int[256];
		for (int j = 0; j < 256; j++) letters_rank[j] = 0;
		for (int j = 0; j < a.length(); j++) letters_rank[a.charAt(j)] = 1;
		for (int j = 1; j < 256; j++) letters_rank[j] += letters_rank[j - 1];
		
		// how many chars in front of current char have smaller rank
		int[] less = new int[a.length()]; less[0] = 0;
		for (int j = 1; j < a.length(); j++) {
			int c = 0;
			for (int i = 0; i < j; i++) if (a.charAt(i) < a.charAt(j)) c++;
			less[j] = c;
		}
		
		// pre-compute factorial
		int[] fact = new int[a.length()];
		fact[0] = 1;
		for (int j = 1; j < a.length(); j++) fact[j] = (fact[j - 1] * j) % 1000003;
		
		// compute rank
		int rank = 1;
		for (int j = 0; j < a.length(); j++)
			rank = (rank + 
					((letters_rank[a.charAt(j)] - 1 - less[j]) * fact[a.length() - 1 - j]) % 1000003)
					% 1000003;
		
		return rank;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String s = "DTNGJPURFHYEW";
		
		SortedPermutationRank t = new SortedPermutationRank();
		System.out.println(t.findRank(s));
	}
}
