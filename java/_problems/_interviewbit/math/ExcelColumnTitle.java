package _problems._interviewbit.math;

/*
 * Given a positive integer, return its corresponding column title as appear in an Excel sheet.
 *
 * For example:
 *
 * 	1 -> A
 * 	2 -> B
 * 	3 -> C
 * 	...
 * 	26 -> Z
 * 	27 -> AA
 * 	28 -> AB
 */

public class ExcelColumnTitle {

	public String convertToTitle(int a) {
		StringBuilder sb = new StringBuilder();

		while (a > 0) {
			a = a - 1; // shit one to the left and make A=0 ... Z=25
			
			int digit = a % 26;
			char c = (char)('A' + digit);
			sb.insert(0, c);
			
			a /= 26;
		}
		
		return sb.toString();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//int a = 943566;
		//int a = 468096;
		//int a = 27;
		int a = 474668;
		
		ExcelColumnTitle t = new ExcelColumnTitle();
		System.out.println(t.convertToTitle(a));
	}
}
