package _problems._interviewbit.math;

/*
 * Rearrange a given array so that Arr[i] becomes Arr[Arr[i]] with O(1) extra space.
 * Note: let n = size(a), then
 *    All elements in the array are in the range [0, N-1]
 *    N^2 does not overflow for a signed integer
 */

import java.util.ArrayList;
import java.util.Arrays;

public class RearrangeArray {

	public void arrange(ArrayList<Integer> a) {
		if (a.isEmpty()) return ;
		
		int n = a.size();
		/* 
			for (int j = 0; j < n; j++) a.set(j, a.get(j) + n);
			for (int j = 0; j < n; j++) {
				int t = a.get(a.get(j) % n) % n; 
				a.set(j, (t * n) + (a.get(j) % n));
			}
		*/
		for (int j = 0; j < n; j++) {
			int t = a.get(a.get(j)) % n;
			a.set(j, a.get(j) + t * n);
		}
		for (int j = 0; j < n; j++) a.set(j, a.get(j) / n);
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[] a = { 4, 0, 2, 1, 3 };
		ArrayList<Integer> A = new ArrayList<Integer>(Arrays.asList(a));
		
		RearrangeArray t = new RearrangeArray();
		t.arrange(A);
		
		for (int x : A) System.out.print(x + " ");
	}
}
