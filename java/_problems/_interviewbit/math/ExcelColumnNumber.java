package _problems._interviewbit.math;

public class ExcelColumnNumber {

	public int titleToNumber(String a) {
		if (a.isEmpty()) return 0;

		int x = 0;
		for (int j = a.length() - 1, p = 1; j >= 0; j--, p *= 26)
			x += (a.charAt(j) - 'A' + 1) * p;
		
		return x;
	}
    
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//String a = "BAQTZ";
		String a = "ZZ";

		ExcelColumnNumber t = new ExcelColumnNumber();
		System.out.println(t.titleToNumber(a));
	}

}
