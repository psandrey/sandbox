package _problems._interviewbit.hashing;

/*
 * You are given a string, S, and a list of words, L, that are all of the same length.
 * Find all starting indices of substring(s) in S that is a concatenation of each word in
 * L exactly once and without any intervening characters.
 *  
 * Example :
 *    S: "barfoothefoobarman"
 *    L: ["foo", "bar"]
 *    
 *  You should return the indices: [0,9].
 *  Note: the order of indices does not matter.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SubstringConcatenation {

	// T(n) = O(mn)
	public ArrayList<Integer> findSubstring_V2(String s, final List<String> words) {
		ArrayList<Integer> ans = new ArrayList<Integer>();
		if (s == null || s.length() == 0 || words == null || words.size() == 0) return ans;
		
		// compute words frequencies, since in s only the freq matters not the order
		HashMap<String, Integer> wFreq = new HashMap<>();
		int m = words.size();
		for (int i = 0; i < m; i++)
			if (wFreq.containsKey(words.get(i))) wFreq.put(words.get(i), wFreq.get(words.get(i)) + 1);
			else wFreq.put(words.get(i), 1);
		
		// since all the words has the same length, we can look at each word as 'char',
		//   and from now on we will call it meta-word
		// also, we can start checking for a new set of words concatenation from 0 to metaCharLen
		// Note: write on paper... you will figure it out :)
		int metaCharLen = words.get(0).length();
		int offset = 0;
		int n = s.length();
		while (offset < metaCharLen) {
			
			int i = offset; // the start of the current series
			int count = 0;  // count the words in the current series
			HashMap<String, Integer> hm = new HashMap<>(); // current frequencies

			for (int j = offset; j <= n-metaCharLen; j+=metaCharLen) {

				String metaChar = s.substring(j, j+metaCharLen);
				if (wFreq.containsKey(metaChar)) {
					if (hm.containsKey(metaChar)) hm.put(metaChar, hm.get(metaChar) + 1);
					else hm.put(metaChar, 1);
					
					// we just added new word
					count++;
					
					// now, if the freq of the new added word is to big... we are cutting
					//  meta-chars from the start of the series until it meets
					//  the required frequency
					while (hm.get(metaChar) > wFreq.get(metaChar)) {
						String leftMetaChar = s.substring(i, i+metaCharLen);
						hm.put(leftMetaChar, hm.get(leftMetaChar) - 1);
						count--;
						i+=metaCharLen;
					}
					
					// new, lets see if we have a match (the current series contains all words)
					if (count == m) {
						ans.add(i);
						
						// shift right one position
						String leftMetaChar = s.substring(i, i+metaCharLen);
						hm.put(leftMetaChar, hm.get(leftMetaChar) - 1);
						count--;
						i+=metaCharLen;
					}
				} else {
					// we just met a metaChar that is not in the words list, so rest
					//  everything...
					hm.clear();
					i = j+metaCharLen; // next meta word is the new start
					count = 0;
				}
			}
			
			// check series starting of the next offset...
			offset++;
		}
		
		// done...
		return ans;
	}
	
	// T(n) = O(n^2) : 1/m(1+2+3+..+n) = 1/m(n(n+1)/2) = O(n^2)
	// S(n) = O(m)
	public ArrayList<Integer> findSubstring_V1(String s, final List<String> words) {
		ArrayList<Integer> ans = new ArrayList<Integer>();
		if (s == null || s.length() == 0 || words == null || words.size() == 0) return ans;
		
		// compute words frequencies, since in s only the freq matters not the order
		HashMap<String, Integer> wFreq = new HashMap<>();
		for (int i = 0; i < words.size(); i++)
			if (wFreq.containsKey(words.get(i)))
				wFreq.put(words.get(i), wFreq.get(words.get(i)) + 1);
			else
				wFreq.put(words.get(i), 1);
	
		int lSize = words.size();
		int wSize = words.get(0).length();
		int i = 0;
		int seriesLen = wSize*lSize;

		// continue as long as we have room for one more series
		while((i + seriesLen - 1) < s.length()) {

			// start new series
			int j = 0;
			HashMap<String, Integer> hm = new HashMap<>();
			while (i + j*wSize+wSize-1 < s.length()) {
				String w = s.substring(i + j*wSize, (i + j*wSize) + wSize);
				
				// if this word is not in the list of words, start next series
				if(!wFreq.containsKey(w)) break;

				// add the new word to the series
				if (!hm.containsKey(w)) hm.put(w, 1);
				else hm.put(w, hm.get(w) + 1);
				
				// too many... next series
				if (hm.get(w) > wFreq.get(w)) break;

				// do we have a match?
				j++;
				if (j == lSize) ans.add(i);
			}

			// move offset
			i++;
		}
		
		return ans;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//String  s="barfoothefoobarman" ;
		//String[] words= {"foo", "bar"};
		String s = "aaaaaaaaaaa";
		String[] words = {"aaa", "aaa"};
		
		SubstringConcatenation t = new SubstringConcatenation();
		ArrayList<Integer> ans = t.findSubstring_V1(s, new ArrayList<String>(Arrays.asList(words)));
		System.out.println(ans);
		
		ans = t.findSubstring_V2(s, new ArrayList<String>(Arrays.asList(words)));
		System.out.println(ans);
	}
}
