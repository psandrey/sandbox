package _problems._interviewbit.hashing;

/*
 * Given an array A of integers and another non negative integer k,
 * find if there exists 2 indices i and j such that A[i] - A[j] = k, i != j.
 */

import java.util.HashSet;

public class DiffkII {
	
	public int diffPossible(final int[] a, int k) {
		if (a == null || a.length == 0) return 0;
		int n = a.length;
		
		HashSet<Integer> hs = new HashSet<>();
		for (int i = 0; i < n; i++) {
			if (hs.contains(a[i]+k)|| hs.contains(a[i]-k)) return 1;
			else hs.add(a[i]);
		}

		return 0;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = new int[] {2, 7, 5};
		int k = 2;
		
		DiffkII t = new DiffkII();
		System.out.println(t.diffPossible(a, k));
	}
}
