package _problems._interviewbit.hashing;

/*
 * Given a string S and a string T, find the minimum window in S which will contain all the characters
 *  in T in linear time complexity.
 *  Note that when the count of a character C in T is N, then the count of C in minimum window in S
 *  should be at least N.
 */

public class WindowString {

	int hSize = 256;
	public String minWindow(String S, String T) {
		if (T == null || S == null || T.length() == 0 || S.length() == 0 || S.length() < T.length())
			return "";
		
		char[] t = T.toCharArray();
		char[] s = S.toCharArray();
		int n = t.length;
		int m = s.length;
		
		int[] hs = new int[hSize];
		int[] ht = new int[hSize];
		
		for (int i = 0; i < n; i++) ht[t[i]]++;
		
		int minStart = -1, minLen = Integer.MAX_VALUE;
		int start = 0;
		int count = 0;
		for (int i = 0; i < m; i++) {
			hs[s[i]]++;
			if (ht[s[i]] > 0 && hs[s[i]] <= ht[s[i]]) count++;
			
			if (count == n) {
				
				// we found a match, now minimize window
				while (ht[s[start]] == 0 || hs[s[start]] > ht[s[start]]) {
					hs[s[start]]--;
					start++;
				}
				
				// check if the window is smaller
				int lenWindow = i - start + 1;
				if (minLen > lenWindow) {
					minStart = start;
					minLen = i - start + 1;
				}
			}
		}

		return (minStart == -1) ? "" : S.substring(minStart, minStart + minLen);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String s = "ADOBECODEBANC";
		String t = "ABC";
		
		WindowString ws = new WindowString();
		String r = ws.minWindow(s, t);
		System.out.println(r);
	}
}
