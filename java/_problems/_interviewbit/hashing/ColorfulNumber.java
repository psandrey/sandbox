package _problems._interviewbit.hashing;

/*
 * For Given Number N find if its COLORFUL number or not.
 * 
 * Example: 
 * A number can be broken into different contiguous sub-subsequence parts. 
 * Suppose, a number 3245 can be broken into parts like 3 2 4 5 32 24 45 324 245 4245. 
 * And this number is a COLORFUL number, since product of every digit of a
 * contiguous subsequence is different
 */

import java.util.ArrayList;
import java.util.HashSet;

public class ColorfulNumber {
	
	public int colorful(int n) {
		ArrayList<Integer> digits = getDigits(n);

		HashSet<Long> hs = new HashSet<Long>();
		int m = digits.size();
		int len = digits.size();
		for (int l = 1; l <= len; l++) {
			for (int i = 0; i <= m-l; i++) {
				long p = doProd(digits, i, l);
				if (hs.contains(p)) return 0;
				else hs.add(p);
			}
		}
		
		return 1;
	}
	
	private long doProd(ArrayList<Integer> digits, int i, int len) {
		long p = 1;
		for (int j = i; j < i+len; j++) p *= digits.get(j);
		
		return p;
	}
	
	private ArrayList<Integer> getDigits(int n) {
		ArrayList<Integer> digits = new ArrayList<Integer>();
		int Q = n, R;
		do {
			R = Q%10 ;
			digits.add(R);
			Q /= 10;
		} while (Q != 0);
		
		return digits;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		ColorfulNumber t = new ColorfulNumber();
		System.out.println(t.colorful(23));
	}
}
