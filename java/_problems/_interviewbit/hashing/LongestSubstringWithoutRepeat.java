package _problems._interviewbit.hashing;

/*
 * Given a string, find the length of the longest substring without repeating characters.
 * 
 * Example:
 *  The longest substring without repeating letters for "abcabcbb" is "abc", which the length is 3.
 *  For "bbbbb" the longest substring is "b", with the length of 1.
 */

import java.util.HashSet;

public class LongestSubstringWithoutRepeat {
	
	public int lengthOfLongestSubstring(String s) {
		if (s == null || s.length() == 0) return 0;
		int n = s.length();
		
		HashSet<Character> hs = new HashSet<>();
		int j, i = 0, max = 1;
		for (j = 0; j < n; j++) {
			char c = s.charAt(j);
			if (!hs.contains(c)) hs.add(c);
			else {
				max = Math.max(max, j-i);
				while(i < j && s.charAt(i) != c) { hs.remove(s.charAt(i)); i++; }
				i++;
			}
		}
		max = Math.max(max, j-i);
		
		return max;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String s = "dadbc";
		LongestSubstringWithoutRepeat t = new LongestSubstringWithoutRepeat();
		System.out.println(t.lengthOfLongestSubstring(s));
	}

}
