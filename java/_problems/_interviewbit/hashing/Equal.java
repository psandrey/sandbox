package _problems._interviewbit.hashing;

/*
 * Given an array A of integers, find the index of values that satisfy A + B = C + D,
 *  where A,B,C & D are integers values in the array
 * 
 * Note:
 * 
 * 1) Return the indices `A1 B1 C1 D1`, so that 
 *   A[A1] + A[B1] = A[C1] + A[D1]
 *   A1 < B1, C1 < D1
 *   A1 < C1, B1 != D1, B1 != C1 
 * 
 * 2) If there are more than one solutions, 
 *    then return the tuple of values which are lexicographical smallest. 
 * 
 * Assume we have two solutions
 * S1 : A1 B1 C1 D1 ( these are values of indices int the array )  
 * S2 : A2 B2 C2 D2
 * 
 * S1 is lexicographically smaller than S2 iff
 *   A1 < A2 OR
 *   A1 = A2 AND B1 < B2 OR
 *   A1 = A2 AND B1 = B2 AND C1 < C2 OR 
 *   A1 = A2 AND B1 = B2 AND C1 = C2 AND D1 < D2
 * Example:
 * 
 * Input: [3, 4, 7, 1, 2, 9, 8]
 * Output: [0, 2, 3, 5] (O index)
 * If no solution is possible, return an empty list.
 */

import java.util.ArrayList;
import java.util.HashMap;

public class Equal {
	
	private class Pair {
		int i, j;
		public Pair(int ii, int jj) { i = ii; j = jj; }
	};

	// T(n) = O(n^3)
	public int[] equal(int[] a) {
		if (a == null || a.length < 4) return new int[] {};
		int[] ans = null;
		
		int n = a.length;
		HashMap<Integer, ArrayList<Pair>> hm = new HashMap<>();
		
		for (int i = 0; i < n-1; i++)
			for (int j = i+1; j < n; j++) { // O(n^2)
				
				int key = a[i]+a[j];
				if (!hm.containsKey(key)) {
					ArrayList<Pair> l = new ArrayList<>();
					l.add(new Pair(i, j));
					hm.put(key, l);
				} else {
					ArrayList<Pair> l = hm.get(key);
					int[] sol = solution(l, i, j); // O(n)

					if (sol != null) {
						if(ans != null)
							ans = lexicographic(sol, ans);
						else ans = sol;
					}
					
					l.add(new Pair(i, j));
				}
			}
			
		return (ans == null ? new int[] {} : ans);
	}
	
	// check if this is a solution
	private int[] solution (ArrayList<Pair> l, int ii, int jj) {
		for (Pair p : l)
			if (p.i < ii && p.j != jj && p.j != ii )
				return new int[] {p.i, p.j, ii, jj};
		
		return null;
	}
	
	// we need the smallest (lexicographic) solution
	private int[] lexicographic(int[] sol, int[] ans) {
		for (int i = 0; i < sol.length; i++) {
			if (ans[i] == sol[i]) continue;
			if (ans[i] > sol[i]) return sol;
			if (ans[i] < sol[i]) return ans;
		}
		
		return ans;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//int[] a = new int[] {3, 4, 7, 1, 2, 9, 8};
		int[] a = new int[] {1, 5, 2, 4, 2, 0, 2, 5, 1, 0, 5, 0 };
		Equal t = new Equal();
		int[] ans = t.equal(a);

		System.out.println();
		if (ans != null && ans.length == 4) {
			for (int i = 0; i < ans.length; i++)
				System.out.print(ans[i] + " ");
		}
	}
}
