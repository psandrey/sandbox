package _problems._interviewbit.hashing;

/*
 * Given an array of strings, return all groups of strings that are anagrams.
 * Represent a group by a list of integers representing the index in the original list.
 * Look at the sample case for clarification.
 * 
 * Anagram : a word, phrase, or name formed by rearranging the letters of another,
 * such as 'spar', formed from 'rasp' 
 * 
 * Note: All inputs will be in lower-case.
 * Example :
 *   Input : cat dog god tca
 *   Output : [[1, 4], [2, 3]]
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

public class Anagrams {
	
	// version with String.hashCode()
	public ArrayList<ArrayList<Integer>> anagrams_v1(final List<String> s) {
		ArrayList<ArrayList<Integer>> ans = new ArrayList<ArrayList<Integer>>();
		if (s == null || s.size() == 0) return ans;

		int n = s.size();
		LinkedHashMap<String, ArrayList<Integer>> hm = new LinkedHashMap<>();
		for (int i = 0; i < n; i++) {
			char[] w = s.get(i).toCharArray();
			
			// sort to get the same hash key regardless of chars ordering
			Arrays.sort(w);
			String key = new String(w);
			
			ArrayList<Integer> l;
			if (hm.containsKey(key)) l = hm.get(key);
			else l = new ArrayList<>();

			// Note: the hash for a string is h = 31 * h + val[i];
			l.add(i+1);
			hm.put(key, l);
		}
		
		for (String key: hm.keySet()) {
			ArrayList<Integer> l = hm.get(key);
			if (l.size() > 1) ans.add(l);
		}
		
		return ans;
	}
	
	// version with custom hashcode
	public ArrayList<ArrayList<Integer>> anagrams_v2(final List<String> s) {
		ArrayList<ArrayList<Integer>> ans = new ArrayList<ArrayList<Integer>>();
		if (s == null || s.size() == 0) return ans;

		int n = s.size();
		LinkedHashMap<Integer, ArrayList<Integer>> hm = new LinkedHashMap<>();
		for (int i = 0; i < n; i++) {
			int key = hashCode(s.get(i));

			ArrayList<Integer> l;
			if (hm.containsKey(key)) l = hm.get(key);
			else l = new ArrayList<>();

			l.add(i+1);
			hm.put(key, l);
		}
		
		for (Integer key: hm.keySet()) ans.add(hm.get(key));
		
		return ans;
	}

	public int hashCode(String s) {
		if (s.length() == 0) return 0;

		long h = 17;
		long m = 1000000007;
		char val[] = s.toCharArray();
		for (int i = 0; i < val.length; i++) h = (h*(long)val[i])%m;
		
		return (int)h;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Anagrams t = new Anagrams();
		String[] s = {"cat", "dog", "god", "tca"};
		System.out.println(t.anagrams_v1(new ArrayList<String>(Arrays.asList(s))));
		System.out.println(t.anagrams_v2(new ArrayList<String>(Arrays.asList(s))));	
	}
}
