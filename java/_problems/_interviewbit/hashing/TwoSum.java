package _problems._interviewbit.hashing;

import java.util.HashMap;

/*
 * Given an array of integers, find two numbers such that they add up
 *  to a specific target number. The function twoSum should return indices
 *  of the two numbers such that they add up to the target, where index1 < index2.
 *  Please note that your returned answers (both index1 and index2 ) are not zero-based. 
 *  Put both these numbers in order in an array and return the array from your function
 *  
 *  Note that, if no pair exists, return empty list.
 *  If multiple solutions exist, output the one where index2 is minimum.
 *  If there are multiple solutions with the minimum index2,
 *  choose the one with minimum index1 out of them.
 */

public class TwoSum {
	
	public int[] twoSum(final int[] a, int t) {
		if (a == null || a.length < 2)
			return new int[] {};

		int n = a.length;
		HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
		for (int i = 0; i < n; i++) {
			int c = t - a[i];
			if (hm.containsKey(c))
				return new int[] {hm.get(c)+1 , i+1};
			else if (!hm.containsKey(a[i]))
				hm.put(a[i], i);
		}
		
		return new int[] {};
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//int[] a = {3, 7, 3, 11, 15};
		//int t = 14;
		
		int[] a = {3, 7, 11, 3, 15};
		int t = 14;
		
		TwoSum s = new TwoSum();
		int[] r = s.twoSum(a, t);
		for (int i = 0; i < r.length; i++)
			System.out.print(r[i] + ", ");
	}
}
