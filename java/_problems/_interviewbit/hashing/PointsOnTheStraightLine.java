package _problems._interviewbit.hashing;

/*
 * Given n points on a 2D plane, find the maximum number of points that lie on the same straight line.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class PointsOnTheStraightLine {
/*
	private int __gcd(int a, int b) {
		int k = Math.max(a,b);
		int m = Math.min(a,b);
		while (m != 0) {
			int r = k % m;
			k = m;
			m = r;
		}
		
		return k;
	}
*/
	public int maxPoints(ArrayList<Integer> x, ArrayList<Integer> y) {
		if (x == null || y == null || x.size() != y.size())
			return 0;
		
		int n = x.size();
		if (n == 1)	return 1;
		else if (n == 2) return 2;
		
		int max = 0;
		for (int i = 0; i < n-1; i++) {
			int x0 = x.get(i);
			int y0 = y.get(i);
			
			int vertical = 0;
			int overlap = 0;
			int maxPoint = 0;
			
			HashMap<Double, Integer> hm = new HashMap<>();
			for (int j = i+1; j < n; j++) {
				int x1 = x.get(j);
				int y1 = y.get(j);
				
				if (x0 == x1) {
					if (y0 == y1) overlap++; 
					else vertical++;
					continue;
				}
				
				double slope = (y1 == y0) ? 0.0 : ((double) (y1 - y0)) / ((double)(x1 - x0));

				int k = 1;
				if (hm.containsKey(slope)) k = hm.get(slope) + 1;
				hm.put(slope, k);

				maxPoint = Math.max(maxPoint, k);
			}
			maxPoint = Math.max(maxPoint, vertical);
			
			max = Math.max(max, maxPoint + overlap + 1);
		}
	
		return max;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//Integer[] x = new Integer[] {-1, 0, 1, 2, 3, 3};
		//Integer[] y = new Integer[] { 1, 0, 1, 2, 3, 4};
		
		Integer[] x = new Integer[] { 1, -1};
		Integer[] y = new Integer[] {-1, -1};
		
		//int[] x = new int[] { 1, 2};
		//int[] y = new int[] { 1, 2};
		PointsOnTheStraightLine t = new PointsOnTheStraightLine();
		ArrayList<Integer> X = new ArrayList<Integer>(Arrays.asList(x));
		ArrayList<Integer> Y = new ArrayList<Integer>(Arrays.asList(y));
		int r = t.maxPoints(X, Y);
		System.out.println(r);
		
	}
}
