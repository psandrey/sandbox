package _problems._interviewbit.hashing;

/*
 * Given an array S of n integers, are there elements a, b, c, and d in S such that
 * a + b + c + d = target? Find all unique quadruplets in the array which gives the sum of target.
 * 
 * Elements in a quadruplet (a,b,c,d) must be in non-descending order. (i.e. a <= b <= c <= d).
 * The solution set must not contain duplicate quadruplets.
 * Also make sure that the solution set is lexicographically sorted.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class FourSum {

	// version accepted
	public int[][] fourSumV1(int[] a, int t) {
		if (a == null || a.length < 4)
			return new int[][] {};
		
		Arrays.sort(a);

		LinkedList<int[]> ans = new LinkedList<int[]>();
		for (int i = 0; i < a.length-3; i++) {
			// if there was a solution with a[i] = a[i+1] it was find when a[i] = a[j]
			// so, if we would allow this, then we would have a duplicate solution.
			if(i!=0 && a[i]==a[i-1])
				continue;

			for (int j = i + 1; j < a.length-2; j++) {
				// if there was a solution with a[j] = a[j+1] it was find when a[l] = a[j]
				// so, if we would allow this, then we would have a duplicate solution
				if (j != i+1 && a[j] == a[j-1])
					continue;

				int k = j+1;
				int l = a.length-1;
				while (k < l) {
					if (a[i] + a[j] + a[k] + a[l] < t) k++; // first to preserve lexicographic order
					else if (a[i] + a[j] + a[k] + a[l] > t) l--;
					else {
						int[] sol = new int[4];
						sol[0] = a[i]; sol[1] = a[j];
						sol[2] = a[k]; sol[3] = a[l];
						ans.add(sol);
						
						// go to next
						k++;l--;

						// advance over duplicates
						while (k < l && a[l] == a[l+1]) l--;
						while (k < l && a[k] == a[k-1]) k++;
					}
				}
			}
		}
 
		int[][] result = new int[ans.size()][4];
		ans.toArray(result);
		return result;
	}
	

	// Version not accepted !!!
	private class Pair {
		public int a, b;
		public Pair(int aa, int bb) { a = aa; b = bb; }
	}

	public int[][] fourSumV2(int[] a, int t) {
		if (a == null || a.length < 4)
			return new int[][] {};
		
		Arrays.sort(a);
		
		HashMap<Integer, List<Pair>> hm = new HashMap<>();
		int n = a.length;
		for (int i = 0; i < n-1; i++) {
			for (int j = i+1; j < n; j++) {
				int s = a[i] + a[j];

				List<Pair> l;
				if (hm.containsKey(s)) l = hm.get(s);
				else { l = new ArrayList<Pair>(); hm.put(s, l); }
				
				l.add(new Pair(i, j));
			}
		}

		HashSet<ArrayList<Integer>> ans = new HashSet<ArrayList<Integer>>();
		for (int i = 0; i < n-1; i++) {
			for (int j = i+1; j < n; j++) {
				int s = a[i] + a[j];
				int c = t - s;
				
				if (!hm.containsKey(c))
					continue;
				
				List<Pair> l = hm.get(c);
				for (Pair p: l) {
					if (i != p.a && i != p.b && j != p.a && j != p.b) {
						ArrayList<Integer> sol = new ArrayList<Integer>(4);
						
						sol.add(a[i]); sol.add(a[j]);
						sol.add(a[p.a]); sol.add(a[p.b]);
						Collections.sort(sol);
						ans.add(sol);
					}
				}
			}
		}

		int i = 0;
		int[][] result = new int[ans.size()][4];
		for (ArrayList<Integer> list: ans) {
			for (int j = 0; j < list.size(); j++)
				result[i][j] = list.get(j);
			i++;
		}
		return result;
	}
	
	public void printSums(int[][] r) {
		for (int i = 0; i < r.length; i++) {
			System.out.println();
			System.out.print("[");
			for (int j = 0; j < r[0].length; j++) {
				System.out.print(r[i][j] + ",");
			}
			System.out.print("]");
		}		
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = new int[]{1, 1, 1, 1, 0, 0, 0, -1, -1, -1, 0, -2, -2, 2, 0, 2, 2, 2};
		int t = 0;

		//int[] a = new int[]{1, 2, 3, 4};
		//int t = 10;
		FourSum s = new FourSum();
		
		int[][] r = s.fourSumV1(a, t);
		s.printSums(r);
		System.out.println();
		
		r = s.fourSumV2(a, t);
		s.printSums(r);
	}
}
