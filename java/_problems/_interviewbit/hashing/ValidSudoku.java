package _problems._interviewbit.hashing;

/*
 * Determine if a Sudoku is valid, according to: http://sudoku.com.au/TheRules.aspx
 * The Sudoku board could be partially filled, where empty cells are filled with the character �.�.
 * 
 * E.g. ["53..7....", "6..195...", ".98....6.", "8...6...3", "4..8.3..1",
 *       "7...2...6", ".6....28.", "...419..5", "....8..79"]
 *       Answer: 1
 */

public class ValidSudoku {

	public int isValidSudoku(final String[] sBoard) {
		if (sBoard == null || sBoard.length != 9 || sBoard[0].length() != 9)
			return 0;

		int n = sBoard.length;
		char[][] board = new char[n][n];
		for (int i = 0; i < n; i++) board[i] = sBoard[i].toCharArray();
		
		int[][] hL = new int[n+1][n+1];
		int[][] hC = new int[n+1][n+1];
		int[][][] hCell = new int[3][3][10];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (board[i][j] == '.') continue;

				int x = board[i][j] - '0';
				int xCell = i/3;
				int yCell = j/3;
				
				if (hL[i][x] == 1 || hC[j][x] == 1 || hCell[xCell][yCell][x] == 1)
					return 0;
				
				hL[i][x]++;hC[j][x]++;hCell[xCell][yCell][x]++;
			}
		}
		
		return 1;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String[] board = new String[] 
				{"53..7....",
				 "6..195...",
				 ".98....6.",
				 "8...6...3",
				 "4..8.3..1",
				 "7...2...6",
				 ".6....28.",
				 "...419..5",
				 "....8..79"};
		ValidSudoku t = new ValidSudoku();
		int r = t.isValidSudoku(board);
		System.out.println(r);
		
	}
}
