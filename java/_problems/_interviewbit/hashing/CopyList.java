package _problems._interviewbit.hashing;

/*
 * A linked list is given such that each node contains an additional random pointer
 *  which could point to any node in the list or NULL.
 *  
 * Return a deep copy of the list.
 */

import java.util.HashMap;

public class CopyList {

	private class RandomListNode {
		int label;
		RandomListNode next, random;
		public RandomListNode(int v) { next = null; random = null; label = v; }
	};
	
	public RandomListNode copyRandomList(RandomListNode L) {
		if (L == null) return null;
		
		// keep track of the random pointer for those pointers that are not null
		HashMap<RandomListNode, RandomListNode> hm = new HashMap<>();
		
		// copy clone nodes
		RandomListNode copyL = null, copyPrev = null;
		RandomListNode n = L, cn;
		while (n != null) {
			cn = new RandomListNode(n.label);
			hm.put(n, cn);

			if (copyPrev != null) copyPrev.next = cn;				
			copyPrev = cn;
			
			if (copyL == null) copyL = cn; 
			n = n.next;
		}

		// update random pointer
		n = L; cn = copyL;
		while (n != null) {
			RandomListNode r = n.random;
			RandomListNode cr = hm.get(r);
			cn.random = cr;

			n = n.next;
			cn = cn.next;
		}
		
		return copyL;
	}
	
	private RandomListNode buildList() {
		RandomListNode n1 = new RandomListNode(1);
		RandomListNode n2 = new RandomListNode(2);
		RandomListNode n3 = new RandomListNode(3);
		RandomListNode n4 = new RandomListNode(4);
		n1.next = n2;
		n2.next = n3;
		n3.next = n4;
		n1.random = n3;
		n2.random = n3;

		return n1;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		CopyList t = new CopyList();
		RandomListNode L = t.buildList();
		RandomListNode cL = t.copyRandomList(L);
	}
}
