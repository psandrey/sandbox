package _problems._interviewbit.hashing;

/*
 * Given two integers representing the numerator and denominator of a fraction, 
 * return the fraction in string format.
 * 
 * If the fractional part is repeating, enclose the repeating part in parentheses.
 * 
 * Example :
 *   Given numerator = 1, denominator = 2, return "0.5
 *   Given numerator = 2, denominator = 1, return "2"
 *   Given numerator = 2, denominator = 3, return "0.(6)"
 */

import java.util.HashMap;

public class Fraction {
	
	public String fractionToDecimal(int numerator, int denominator) {
			StringBuilder sb = new StringBuilder();
			HashMap<Long, Integer> remHm = new HashMap<>();
			
			// do this first because of the (1<<31) test... bastards :)
			long N = (long)numerator, D = (long)denominator;
			if ((N > 0L &&  D < 0L) || (N < 0 &&  D > 0)) sb.append('-');
			if (N < 0L) N *= -1;
			if (D < 0L) D *= -1;
	
			long Q = N/D, R = N%D; 
			sb.append(Long.toString(Q));
			if (R == 0) return sb.toString();
			
			sb.append('.');
			remHm.put(R, sb.length());
			do {
				N = R*10;
				Q = N/D;
				R = N%D;
	
				sb.append(Long.toString(Q));
				if (!remHm.containsKey(R)) remHm.put(R, sb.length());
				else {
					int i = remHm.get(R);
					sb.insert(i, '(');sb.append(')');
					break;
				}
			} while (R != 0L);
			
			return sb.toString();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int N = 945;
		int D = 103;
		Fraction t = new Fraction();
		System.out.println(t.fractionToDecimal(N, D));
	}
}
