package _problems._interviewbit.hashing;

/*
 * Find the largest continuous sequence in a array which sums to zero.
 * 
 * Example:
 *   Input:  {1 ,2 ,-2 ,4 ,-4}
 *   Output: {2 ,-2 ,4 ,-4}
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class LargestContinuousSequenceZeroSum {

	public ArrayList<Integer> lszero(ArrayList<Integer> a) {
		if (a == null || a.size() == 0) return new ArrayList<Integer>();
		
		HashMap<Long, Integer> hm = new HashMap<>(); 
		
		int n = a.size(), maxSize = 0 , i = 0, j = 0;
		long s = 0;
		for (int k = 0; k < n; k++) {
			s += (long)a.get(k);
			
			// 1. if current sum is 0, then the longest sub-sequence is the current one
			//
			// 2. we ask our selves how much can we cut from the beginning
			//  of the sequence to have a sum 0, so for each sum (assuming we encounter
			//  same sum multiple times on the way) we record the earliest, thus we
			//  cut as little as possible and obtain the sub array with maximum length
			if (s == 0L) {
				i = 0; j = k; maxSize = k+1;
			} else if (s != 0L && hm.containsKey(s)) {
				int earliest = hm.get(s);
				if (maxSize < k - earliest) { i = earliest+1; j = k; maxSize = j-i+1;} 
			} else hm.put(s, k);
		}
  
		if (maxSize == 0) return new ArrayList<Integer>();
		return new ArrayList<Integer>(a.subList(i, j+1)); 
	} 
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer a[] = {1, 2, -2, 4, -4};
		ArrayList<Integer> A = new ArrayList<Integer>(Arrays.asList(a));
		LargestContinuousSequenceZeroSum t = new LargestContinuousSequenceZeroSum();
		System.out.println(t.lszero(A));
	}
}
