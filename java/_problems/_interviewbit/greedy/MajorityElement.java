package _problems._interviewbit.greedy;

/*
 * Given an array of size n, find the majority element. The majority element is the element
 *  that appears more than floor(n/2) times. You may assume that the array is non-empty and
 *  the majority element always exist in the array.
 */

public class MajorityElement {

	/* Greedy version: based on Boyer_Moore majority vote algorithm. */
	public int majorityElement(final int[] a) {
		int m = 0;
		int c = 0;
		for (int i = 0; i < a.length; i++) {
			if (m == a[i]) c++;
			else {
				if (c == 0) { c = 1; m = a[i]; }
				else c--;
			}
		}
		
		return m;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		int[] a = { 1, 2, 2, 1, 2};
		
		MajorityElement t = new MajorityElement();
		int r = t.majorityElement(a);
		System.out.println(r);
	}
}
