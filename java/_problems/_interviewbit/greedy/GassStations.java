package _problems._interviewbit.greedy;

/*
 * There are N gas stations along a circular route,
 * where the amount of gas at station i is gas[i]. 
 * You have a car with an unlimited gas tank and
 * it costs cost[i] of gas to travel from station i to
 * its next station (i+1).
 * 
 * You begin the journey with an empty tank at one of the gas stations.
 */

public class GassStations {

	public int canCompleteCircuit(final int[] gas, final int[] cost) {
		if (gas == null || gas.length == 0 || cost == null | cost.length == 0) return -1;
		
		/* Condition 1: To have enough gas to do the circuit, we must have sum(gas) >= sum(cost)
		 *              sum(gas) - sum(cost) >= 0 => sum(gas[i] - cost[i]) >= 0
		 * Condition 2: Based on Kadane's algorithm:
		 *              a) if tank + d >= 0, at next step if d >= 0 then tank + d >= d => tank = tank + d
		 *              b) if tank + d < 0 , at next step if d >= 0 then tank + d <  d => tank = d
		 */
		int tank = 0;		// gas in the tank
		int totalGas = 0;	// total gas
		int start = 0;		// station start
		for (int i = 0; i < gas.length; i++) {
			int d = gas[i] - cost[i];
	 
			if (tank >= 0) tank += d;
			else { tank = d; start = i; }

			totalGas += d;
		}

		// do we have enough gas to do the circuit?
		if (totalGas >= 0) return start;
		
		return -1;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		//int[] gas  = {0, 4, 3};
		//int[] cost = {2, 4, 1};
		//int [] gas  = {1,3,3,4,5};
		//int [] cost = {1,3,3,4,5};
		//int[] gas  = {5,1,2,3,4};
		//int[] cost = {4,4,1,5,1};
		//int[] gas = {3, 1, 1};
		//int[] cost = {1, 2, 2};
		//int[] gas  = {6,1,4,3,5};
		//int[] cost = {3,8,2,4,2};
		int[] gas = {204, 918, 18, 17, 35, 739, 913, 14, 76, 555, 333, 535, 653, 667, 52, 987, 422, 553, 599, 765, 494, 298, 16, 285, 272, 485, 989, 627, 422, 399, 258, 959, 475, 983, 535, 699, 663, 152, 606, 406, 173, 671, 559, 594, 531, 824, 898, 884, 491, 193, 315, 652, 799, 979, 890, 916, 331, 77, 650, 996, 367, 86, 767, 542, 858, 796, 264, 64, 513, 955, 669, 694, 382, 711, 710, 962, 854, 784, 299, 606, 655, 517, 376, 764, 998, 244, 896, 725, 218, 663, 965, 660, 803, 881, 482, 505, 336, 279};
		int[] cost = {273, 790, 131, 367, 914, 140, 727, 41, 628, 594, 725, 289, 205, 496, 290, 743, 363, 412, 644, 232, 173, 8, 787, 673, 798, 938, 510, 832, 495, 866, 628, 184, 654, 296, 734, 587, 142, 350, 870, 583, 825, 511, 184, 770, 173, 486, 41, 681, 82, 532, 570, 71, 934, 56, 524, 432, 307, 796, 622, 640, 705, 498, 109, 519, 616, 875, 895, 244, 688, 283, 49, 946, 313, 717, 819, 427, 845, 514, 809, 422, 233, 753, 176, 35, 76, 968, 836, 876, 551, 398, 12, 151, 910, 606, 932, 580, 795, 187};
		
		GassStations gs = new GassStations();
		int rc = gs.canCompleteCircuit(gas, cost);
		System.out.println("Site:" + rc);
	}
}
