package _problems._interviewbit.binary_search;

/*
 * Implement pow(x, n) % d.
 * 
 * In other words, given x, n and d, find (x^n % d).
 */

public class PowerFunction {

	public int pow(int x, int n, int m) {
		long ans = 1;
		if (x == 0) return 0;
		if (n == 1) {
			ans = x%m;
			return (int)(ans < 0 ? ans + m: ans); // handle negative remainder
		}

		long xx = (long) x;
		while (n > 0) {
			if  (n%2 == 1) ans = (ans * xx	) %m;

			xx = (xx * xx)%m;
			n = n/2;
		}
		
		// Note: if the remainder is negative, it means it is between (-m, 0)
		//  thus, by adding m it will go into (0, m)
		ans = (ans < 0 ? ans + m: ans);
		return (int)ans;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int x = 17,
			n = 7,
			m = 5;
		
		PowerFunction t = new PowerFunction();
		System.out.println(t.pow(x, n, m));
	}
}
