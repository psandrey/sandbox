package _problems._interviewbit.binary_search;

/*
 * N number of books are given. 
 * The ith book has Pi number of pages. 
 * You have to allocate books to M number of students so that maximum number of pages
 *  alloted to a student is minimum. A book will be allocated to exactly one student.
 * Each student has to be allocated at least one book. Allotment should be in contiguous order,
 *  for example: A student cannot be allocated book 1 and book 3, skipping book 2.
 * 
 * NOTE: Return -1 if a valid assignment is not possible
 */

public class AllocateBooks {

	public int books(int[] p, int m) {
		if (p == null || m == 0 || m > p.length) return -1;

		long lo = getMin(p);
		long hi = getSum(p);
		long minPages = getMinPages(p, lo, hi, m);
		
		return (int)minPages;
	}
	
	private long getMin(int[] p) {
		int min = Integer.MAX_VALUE;
		for (int i = 0; i < p.length; i++) min = Math.min(min, p[i]);
		
		return (long)min;
	}
	
	private long getSum(int[] p) {
		long s = 0;
		for (int i = 0; i < p.length; i++) s += (long)p[i];

		return s;
	}
	
	private long getMinPages(int[] p, long lo, long hi, int m) {
		long ans = -1;

		while (lo <= hi) {
			long middle = (lo+hi)/2L;

			if (validConfig(p, middle, m)) {
				ans = middle;
				hi = middle-1;
			}
			else
				lo = middle+1;
		}
		
		return ans;
	}
	
	private boolean validConfig(int[] p, long pages, int k) {
		int s = 1;
		int n = p.length;
		long pagesPerStudent = 0;		
		for (int i = 0; i < n; i++) {
			if (pages < p[i]) return false; // the book has too many pages
			else {
				pagesPerStudent += (long)p[i];
				if (pagesPerStudent > pages) {
					s++;
					pagesPerStudent = p[i];
				}
			}
			if (s > k) return false;
		}
		
		return true;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] p = new int[] {97, 26, 12, 67, 10, 33, 79, 49, 79, 21, 67, 72, 93, 36, 85, 45, 28, 91, 94, 57, 1, 53, 8, 44, 68, 90, 24};
		int m = 26;
		AllocateBooks t = new AllocateBooks();
		System.out.println("min pages: " + t.books(p, m));
		
	}
}
