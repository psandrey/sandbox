package _problems._interviewbit.binary_search;

/*
 * Given a sorted array of integers, find the starting and ending position of a given target value.
 * Your algorithmís runtime complexity must be in the order of O(log n).
 * If the target is not found in the array, return [-1, -1].
 */

import java.util.ArrayList;
import java.util.List;

public class SearchRange {

	// Iterative version: T(n) = O(logn)
	public ArrayList<Integer> searchRange(final List<Integer> a, int t) {
		ArrayList<Integer> ans = new ArrayList<Integer>();
		ans.add(-1); ans.add(-1);
		if (a == null || a.size() == 0) return ans;

		int n = a.size();
		int left = -1, right = -1;
		int i = 0, j = n-1;
		
		// get left limit
		while (i <= j) {
			int m = (i+j)/2;
			if (t == a.get(m)) {
				left = m;
				if (m == 0 || a.get(m-1) != t) break;
				j = m-1;
				continue;
			} else if (t < a.get(m)) j = m-1;
			else i = m + 1;
		}
				
		// if there is no left, then there is no right
		if (left == -1) return ans;
		
		// get right limit
		right = left;
		i = left+1; j = n-1;
		while (i <= j) {
			int m = (i+j)/2;
			if (t == a.get(m)) {
				right = m;
				if (m == n-1 || a.get(m+1) != t) break;
				i = m+1;
				continue;
			} else if (t < a.get(m)) j = m-1;
			else i = m + 1;
		}
		
		ans.set(0, left);
		ans.set(1, right);
		return ans;
	}

	public int countOccurences(final int[] a, int t) {;
		if (a == null || a.length == 0) return 0;

		int n = a.length;
		int left = -1, right = -1;
		int i = 0, j = n-1;
		
		// get left limit
		while (i <= j) {
			int m = (i+j)/2;
			if (t == a[m]) {
				left = m;
				if (m == 0 || a[m-1] != t) break;
				j = m-1;
				continue;
			} else if (t < a[m]) j = m-1;
			else i = m + 1;
		}
				
		// if there is no left, then there is no right
		if (left == -1) return 0;
		
		// get right limit
		right = left;
		i = left+1; j = n-1;
		while (i <= j) {
			int m = (i+j)/2;
			if (t == a[m]) {
				right = m;
				if (m == n-1 || a[m+1] != t) break;
				i = m+1;
				continue;
			} else if (t < a[m]) j = m-1;
			else i = m + 1;
		}
		
		return (right-left);
	}
	
	// Recursive version with array support (TLE - because of the extra space used)
	// T(n) = O(logn)
	// S(n) = O(logn)
	public int[] search(int[] a, int t) {
		if (a == null || a.length == 0) return new int[] {};
		int[] range = search(a, 0, a.length-1, t);
		
		if (range[0] == Integer.MAX_VALUE && range[1] == Integer.MIN_VALUE)
			return new int[] {};

		return range;
	}
	
	private int[] search(int[] a, int i, int j, int t) {
		if (i > j) return new int[] {Integer.MAX_VALUE, Integer.MIN_VALUE};
		
		int m = (j+i)/2;
		if (t == a[m]) {
			int[] range = new int[] {m, m};
			int[] rangeL = search(a, i, m-1, t);
			int[] rangeR = search(a, m+1, j, t);
			range[0] = Math.min(range[0], rangeL[0]);
			range[1] = Math.max(range[1], rangeR[1]);
			return range;
		} else if (t < a[m])
			return search(a, i, m-1, t);
	
		return search(a, m+1, j, t);
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//int[] a = {5, 7, 7, 8, 8, 10};
		//int t = 8;
		
		List<Integer> a = new ArrayList<Integer>();
		a.add(5);
		a.add(7);
		a.add(7);
		a.add(8);
		a.add(8);
		a.add(10);
		int t = 1;
		
		SearchRange  test = new SearchRange();
		ArrayList<Integer> r = test.searchRange(a, t);
		System.out.println("[" + r.get(0) + "," + r.get(1) + "]");
	}
}
