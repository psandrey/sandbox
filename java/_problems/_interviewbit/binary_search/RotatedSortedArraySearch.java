package _problems._interviewbit.binary_search;

import java.util.List;

/*
 * Suppose a sorted array is rotated at some pivot unknown to you beforehand.
 * (i.e., 0 1 2 4 5 6 7  might become 4 5 6 7 0 1 2 ).
 * You are given a target value to search. If found in the array, return its index, otherwise return -1.
 * 
 * Note: You may assume no duplicate exists in the array.
 */

public class RotatedSortedArraySearch {

	// Iterative version: T(n) = O(logn)
	public int search(final List<Integer> a, int t) {
		if (a == null || a.size() == 0) return -1;
		int n = a.size();
		if (n == 1) {
			if (a.get(0) == t) return 0;
			else return -1;
		}

		int i = 0, j = n-1, m;

		while (i <= j) {
			m = (i+j)/2;
			int ai = a.get(i);
			int aj = a.get(j);
			int am = a.get(m);
			
			if (t == am) return m;
			if (t == ai) return i;
			if (t == aj) return j;
			
			
			// case 1: the pivot point is outside of range i:j
			if (ai < aj) {
				if (t < am) j = m-1;
				else i = m+1;
			}
			// case 2: the pivot point is in rage i:j
			else {
				// case 2.1: m is in the left bigger half
				if (am > ai) {
					if (t < am) {
						if (t < aj) i = m+1;
						else j = m-1;
					} else i = m+1;;
				}
				// case 2.2: m is in the right smaller half
				else {
					if (t > am) {
						if (t < aj) i = m+1;
						else j = m-1;
					} else j = m-1;
				}
			}
		}
		
		return -1;
	}
	
	// Iterative version with array support: T(n) = O(logn)
	public int find(int[] a, int t) {
		
		int n = a.length;
		int i = 0, j = n-1;
		int m = -1;
		
		while (i <= j) {
			m = (i+j)/2;
			if (t == a[m]) return m;
			if (t == a[i]) return i;
			if (t == a[j]) return j;
			
			// case 1: the pivot point is outside of range i:j
			if (a[i] < a[j]) {
				if (t < a[m]) j = m-1;
				else i = m+1;
			}
			// case 2: the pivot point is in rage i:j
			else {
				// case 2.1: m is in the left bigger half
				if (a[m] > a[i]) {
					if (t < a[m]) {
						if (t < a[j]) i = m+1;
						else j = m-1;
					} else i = m+1;;
				}
				// case 2.2: m is in the right smaller half
				else {
					if (t > a[m]) {
						if (t < a[j]) i = m+1;
						else j = m-1;
					} else j = m-1;
				}
			}
		}

		return -1;
	}

	// The recursive version: T(n) = O(logn)
	public int findRecursive(int[] a, int z) {
		if (a == null || a.length == 0)
			return -1;
		return findRecursive(a, 0, a.length - 1, z);
	}
	
	public int findRecursive(int[] a, int s, int e, int z) {
		int k = (s+e)/2;
		if (z == a[k])
			return k;

		if (e == s)
			return -1;

		// there is a border at one side
		if ( k == s)
			return findRecursive(a, k+1, e, z);
		else if ( k == e)
			return findRecursive(a, s, k - 1, z);

		// case when the sub-array is rotated
		if (a[s] > a[e]) {
			if (a[k] > a[s]) {
				if (z > a[k] || z < a[s])
					return findRecursive(a, k+1, e, z);
				return findRecursive(a, s, k-1, z);
			}
		}

		// case when the sub-array is not rotated
		if ( z < a[k])
			return findRecursive(a, s, k - 1, z);
		return findRecursive(a, k+1, e, z);
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//int[] a = new int[] {4, 5, 6, 7, 0, 1, 2};
		//int target = 7;
		
		int[] a = new int[] {19, 20, 21, 22, 28, 29, 32, 36, 39, 40, 41, 42, 43, 45, 48, 49, 51, 54, 55, 56, 58, 60, 61, 62, 65, 67, 69, 71, 72, 74, 75, 78, 81, 84, 85, 87, 89, 92, 94, 95, 96, 97, 98, 99, 100, 105, 107, 108, 109, 110, 112, 113, 115, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 128, 130, 131, 133, 134, 135, 136, 137, 138, 139, 141, 142, 144, 146, 147, 148, 149, 150, 153, 155, 157, 159, 161, 163, 164, 169, 170, 175, 176, 179, 180, 185, 187, 188, 189, 192, 196, 199, 201, 203, 205, 3, 7, 9, 10, 12, 13, 17};
		int target = 6;
		
		RotatedSortedArraySearch t = new RotatedSortedArraySearch();
		System.out.println(t.find(a, target));
	}
}
