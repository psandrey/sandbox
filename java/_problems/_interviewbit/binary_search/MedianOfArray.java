package _problems._interviewbit.binary_search;

/*
 * There are two sorted arrays A and B of size m and n respectively.
 * Find the median of the two sorted arrays.
 * The overall run time complexity should be O(log (m+n)).
 * 
 * T(n) = O(log (m+n))
 */

import java.util.List;

public class MedianOfArray {
	
	// Iterative version with array support: T(n) = O(log(n + m))
	public double median(int[] a, int[] b) {
		int n = a.length;
		int m = b.length;
		int len = n+m;

		if (len%2 != 0)
			return findKth(a, b, (n+m)/2);
		else {
			int m1 = findKth(a, b, (n+m-1)/2);
			int m2 = findKth(a, b, (n+m)/2);
			return (m1+m2)/2.0;
		}
	}

	private int findKth(int[] a, int[] b, int k) {
		int n = a.length;
		int m = b.length;
		int ia = 0, ib = 0;
		while (true) {
			int lena = n-ia; // length of the remainder array a
			int lenb = m-ib; // length of the remainder array b

			// base cases
			if (lenb == 0) return a[ia+k];
			if (lena == 0) return b[ib+k];
			if (k == 0)
				return (a[ia] < b[ib] ? a[ia] : b[ib]);
			
			int med = k/2;
			int ka, kb;
			// Mind the equation: ka+kb=k-1, because ka and kb both starts from 0
			// let a: a0 a1 a2 a3 a4
			// let b: b0 b1 b2 b3
			// a+b:   c0 c1 c2 c3 c4 c5 c6 c7 c8
			// let k=4 => ka=2, kb=1, this goes like this:
			//
			//        a0 a1 a2 b0 b1, so ka=2 and kb=1 are correct
			// a+b:   c0 c1 c2 c3 c4 c5 c6 c7 c8
			if (lenb < lena) {
				kb = Math.min(med, lenb-1);
				ka = k - kb - 1;
			} else {
				ka = Math.min(med, lena-1);
				kb = k - ka - 1;
			}

			// drop the smaller half
			if (a[ia+ka] < b[ib+kb]) { ia += ka + 1; k = kb; } 
			else { ib += kb + 1; k = ka; }
		}
	}

	// Iterative version with List: T(n) = O(log(n + m))
	public double findMedianSortedArrays(final List<Integer> a, final List<Integer> b) {
		int n = a.size();
		int m = b.size();
		int len = n+m;

		if (len%2 != 0)
			return findKth(a, b, (n+m)/2);
		else {
			int m1 = findKth(a, b, (n+m-1)/2);
			int m2 = findKth(a, b, (n+m)/2);
			return (m1+m2)/2;
		}
	}

	private int findKth(final List<Integer> a, final List<Integer> b, int k) {
		int n = a.size();
		int m = b.size();
		int ia = 0, ib = 0;
		while (true) {
			int lena = n-ia; // length of the remainder array a
			int lenb = m-ib; // length of the remainder array b

			// base cases
			if (lenb == 0) return a.get(ia+k);
			if (lena == 0) return b.get(ib+k);
			if (k == 0)
				return (a.get(ia) < b.get(ib) ? a.get(ia) : b.get(ib));
			
			int med = k/2;
			int ka, kb;
			if (lenb < lena) {
				kb = Math.min(med, lenb-1);
				ka = k - kb - 1;
			} else {
				ka = Math.min(med, lena-1);
				kb = k - ka - 1;
			}

			// drop the smaller half
			if (a.get(ia+ka) < b.get(ib+kb)) { ia += ka + 1; k = kb; } 
			else { ib += kb + 1; k = ka; }
		}
	}

	// Recursive version: T(n) = O(log(n + m))
	private double findMedianSortedArrays(int[] a, int[] b) {
		if (a == null || b == null)
			return 0;

		int len = a.length + b.length;
		if (len == 0)
			return 0;
		else if (len % 2 != 0)
			return findKth(a, 0, b, 0, len/2);
		else
			return (findKth(a, 0, b, 0, (len-1)/2) + findKth(a, 0, b, 0, len/2))/2;
	}
	
	private double findKth(int[] a, int sa, int[] b, int sb, int k) {
		int sizeA = a.length - sa;
		int sizeB = b.length - sb;

		if (sizeA == 0)
			return b[sb + k];
		else if (sizeB == 0)
			return a[sa + k];
			
		if (k == 0)
			return Math.min(a[sa], b[sb]);

		int ka, kb;
		if (sizeA < sizeB) {
			ka = Math.min(k/2, sizeA-1);
			kb = k - ka - 1;
		} else {
			kb = Math.min(k/2, sizeB-1);
			ka = k - kb - 1;	
		}

		if (a[sa+ka] >= b[sb+kb])
			return findKth(a, sa, b, sb+kb+1, ka);

		return findKth(a, sa+ka+1, b, sb, kb);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		//int a[] = {1, 3, 5, 6, 8, 9};
		//int b[] = {0, 2, 4, 10, 12};

		//int a[] = {1, 3, 5};
		//int b[] = {2, 4};

		//int a[] = {1, 3};
		//int b[] = {3, 4};

		//int a[] = {1};
		//int b[] = {2, 3};

		//int a[] = {2, 3};
		//int b[] = {1};

		//int a[] = {1, 2};
		//int b[] = {3, 4};
		
		//int[] a = {2,3,4,5,6};
		//int[] b = {1};
		//int[] a = {0, 23};
		//int[] b = {};
		
		int[] a = {1};
		int[] b = {3, 4, 5, 6, 7, 8};
		
		MedianOfArray t = new MedianOfArray();
		System.out.println(t.median(a, b));
	}
}
