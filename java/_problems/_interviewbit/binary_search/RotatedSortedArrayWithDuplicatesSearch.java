package _problems._interviewbit.binary_search;

/*
 * Suppose a sorted array is rotated at some pivot unknown to you beforehand.
 * (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2). 
 * 
 * You are given a target value to search. If found in the array return its index,
 *  otherwise return -1.
 *
 * Note: Duplicate may exists in the array.
 */

public class RotatedSortedArrayWithDuplicatesSearch {

	private boolean search(int[] a, int z) {
		if (a == null || a.length == 0)
			return false;
		return search(a, 0, a.length - 1, z);
	}
	
	private boolean search(int[] a, int s, int e, int z) {
		// remove duplicates if any
		while ((e > s) && (a[s] == a[e] || a[e] == a[e-1]))
			e--;
		while ((e > s) && (a[s] == a[e] || a[s] == a[s+1]))
			s++;

		int k = (s+e)/2;
		if (z == a[k])
			return true;

		if (e == s)
			return false;
		
		// there is a border at one side
		if ( k == s)
			return search(a, k+1, e, z);
		else if ( k == e)
			return search(a, s, k - 1, z);

		// case when the sub-array is rotated
		if (a[s] > a[e]) {
			if (a[k] > a[s]) {
				if (z > a[k])
					return search(a, k+1, e, z);
				return search(a, s, k-1, z);
			}
			
			if (z < a[k])
				return search(a, s, k-1, z);
			return search(a, k+1, e, z);
		}

		// case when the sub-array is not rotated
		if ( z < a[k])
			return search(a, s, k-1, z);
		return search(a, k+1, e, z);
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		int[] a = {1};
		int z = 3;

		RotatedSortedArrayWithDuplicatesSearch t = new RotatedSortedArrayWithDuplicatesSearch();
		boolean rc = t.search(a, z);
		System.out.println(rc);
	}
}
