package _problems._interviewbit.binary_search;

/*
 * You have to paint N boards of length {A0, A1, A2, A3 � AN-1}.
 * There are K painters available and you are also given how much time a painter
 *  takes to paint 1 unit of board. You have to get this job done as soon as possible
 *  under the constraints that any painter will only paint contiguous sections of board.
 * 
 * Notes:
 * 1. Two painters cannot share a board to paint. That is to say, a board cannot be painted
 *  partially by one painter, and partially by another. 
 * 2. A painter will only paint contiguous boards.
 *  Which means a configuration where painter 1 paints board 1 and 3 but not 2 is invalid.
 * 
 * Return the ans % 10000003;
 */

public class PaintersPartitionProblem {

	public int paint(int k, int t, int[] a) {
		long lo = getMax(a);
		long hi = getSum(a);
		long timeUnits = getTimeUnits(a, lo, hi, k);
		
		//long m = 10000003L;
		//return (int)(((timeUnits%m)*(t%m))%m);
		return (int)timeUnits;
	}
	
	private long getMax(int[] a) {
		int max = Integer.MIN_VALUE;
		for(Integer b: a) max = Math.max(max, b);
		
		return (long)max;
	}
	
	private long getSum(int[] a) {
		long s = 0;
		for(Integer b: a) s += (long)b;

		return s;
	}
	
	private long getTimeUnits(int[] a, long lo, long hi, int k) {
		long ans = 0;

		while (lo <= hi) {
			long m = (lo+hi)/2L;
			if (paintersRequired(a, m) <= k) {
				ans = m;
				hi = m-1;
			} else lo = m+1;
		}
		
		return ans;
	}
	
	private int paintersRequired(int[] a, long t) {
		int p = 1;
		long unitsPerPainter = 0;

		int n = a.length;
		for (int i = 0; i < n; i++) {
			unitsPerPainter += (long)a[i];
			if (unitsPerPainter > t) {
				p++;
				unitsPerPainter = a[i];
			}
		}

		return p;
	}

	// Brute force version:
	// T(n) = O(2^n)
	// S(n) = O(2^n) -- due to recursive calls
	public int paintBF(int[] a, int k) {
		if (a == null || a.length == 0 || k == 0) return 0;

		return helperBF(a, 0, k);
	}
	
	private int helperBF(int[] a, int i, int w) {
		int n = a.length;
		int len = n-i;
		if (w == 1) return getTime(a, i, n);
		if (len == 1) return a[n-1];
		
		int S = Integer.MAX_VALUE, s = Integer.MAX_VALUE, min = Integer.MAX_VALUE;
		for (int j = i; j < n; j++) {
			s = getTime(a, i, j+1);
			S = helperBF(a, j+1, w-1);
			int max = Math.max(s, S);
			min = Math.min(min, max);
		}
		
		return min;
	}
	
	private int getTime(int[] a, int i, int j) {
		int s = 0;
		for(int k = i; k < j; k++)
			s += a[k];
		
		return s;
	}
	
	// Dynamic programming:
	// T(n) = O(k.n^2)
	// S(n) = O(n^2)
	public int paintDP(int[] a, int k) {
		if (a == null || a.length == 0 || k == 0) return 0;
		int n = a.length;
		
		// partial sums
		int[][] s = new int[n][n];
		for(int i = 0; i < n; i++) {
			s[i][i] = a[i];
			for(int j = i+1; j < n; j++)
				s[i][j] = s[i][j-1] + a[j];
		}
		
		int[][] dp = new int[n+1][k+1];

		// base cases
		for(int len = 1; len <= n; len++)
			dp[len][1] = s[0][len-1]; // 1 worker
		for(int w = 1; w <= k; w++)
			dp[1][w] = a[0]; // 1 length
		
		// fill the table
		for(int w = 2; w <= k; w++) {
			for(int len = 2; len <= n; len++) {
				int min = Integer.MAX_VALUE;
				for (int p = 1; p < len; p++) {
					int max = Math.max(s[p][len-1], dp[p][w-1]);
					min = Math.min(min, max);
				}

				dp[len][w] = min;
			}
		}

		return dp[n][k];
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		//int[] a = {10, 20, 30, 40};
		//int k = 2;
		int[] a = { 10, 20, 60, 50, 30, 40 };
		int k = 3;
		
		PaintersPartitionProblem t = new PaintersPartitionProblem();
		System.out.println("BF:" + t.paintBF(a, k));
		System.out.println("DP:" + t.paintDP(a, k));

		System.out.println("BS:" + t.paint(k, 1, a));
	}
}
