package _problems._interviewbit.two_pointers;

/*
 * Find the intersection of two sorted arrays OR in other words,
 * Given 2 sorted arrays, find all the elements which occur in both the arrays.
 */

import java.util.LinkedList;
import java.util.List;

public class IntersectionSortedArrays {
	
	// T(n) = O(n+m)
	public int[] intersect(final int[] a, final int[] b) {
		if (a == null || b == null || a.length == 0 || b.length == 0) return new int[] {};
		
		List<Integer> l = new LinkedList<Integer>();
		int n = a.length;
		int m = b.length;
		int i = 0,j = 0;
		while (i < n && j < m) {
			if (a[i] == b[j]) {
				l.add(a[i]);
				i++; j++;
			} else if (a[i] < b[j]) i++;
			else j++;
		}
		
		return listToArray(l);
	}
	
	private int[] listToArray(List<Integer> l) {
		int[] ans = new int[l.size()];
		int i = 0;
		for (Integer x: l) ans[i++] = x;
		return ans;
	}
	
	private void print(int[] a) {
		for (Integer x: a) {
			System.out.print(x + ",");
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = new int[]{1, 2, 3, 3, 4, 5, 6};
		int[] b	= new int[]{3, 3, 5};
		
		IntersectionSortedArrays t = new IntersectionSortedArrays();
		int[] ans = t.intersect(a, b);
		t.print(ans);
	
	}

}
