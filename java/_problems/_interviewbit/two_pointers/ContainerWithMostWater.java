package _problems._interviewbit.two_pointers;

/*
 * Given n non-negative integers a1, a2, ..., an, where each represents a point at coordinate (i, ai).
 * 'n' vertical lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0).
 * Find two lines, which together with x-axis forms a container, such that the container contains
 * the most water.
 * 
 * Your program should return an integer which corresponds to the maximum area of water that
 *  can be contained ( Yes, we know maximum area instead of maximum volume sounds weird.
 *  But this is 2D plane we are working with for simplicity ).
 */

public class ContainerWithMostWater {

	public int maxArea(int[] a) {
		if (a == null || a.length == 0) return 0;
		int ans = 0;
		
		int n = a.length;
		int l = 0, r = n-1;
		while (l < r) {
			int h = Math.min(a[l], a[r]);
			int w = h * (r-l);
			ans = Math.max(ans, w);
			if (h == a[l]) l++;
			else r--;
		}
		
		return ans;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		int[] a = new int[] {1, 5, 4, 3};
		
		ContainerWithMostWater t = new ContainerWithMostWater();
		System.out.println(t.maxArea(a));
	}
}
