package _problems._interviewbit.two_pointers;

/*
 * Remove duplicates from Sorted Array
 * Given a sorted array, remove the duplicates in place such that each element appears only once
 * and return the new length.
 * 
 * Note: that even though we want you to return the new length, make sure to change
 *  the original array as well in place. Do not allocate extra space for another array,
 *  you must do this in place with constant memory.
 */

import java.util.ArrayList;
import java.util.Arrays;

public class RemoveDuplicatesSortedArray {
	
	// Arraylist: T(n) = O(n)
	public int removeDuplicates(ArrayList<Integer> a) {
 		if (a == null || a.size() == 0) return 0;

		int n = a.size();
		int i = 0;
		// to the first duplicate
		while (i < n-1 && a.get(i).intValue() != a.get(i+1).intValue()) i++;

		// remove duplicates (in-place)
		int j = i+1;
		while (j<n) {
			if (a.get(j).intValue() == a.get(j-1).intValue()) j++;
			else a.set(++i, a.get(j++));
		}
		
		// delete duplicates
		i++;
		while (i < n) { a.remove(a.size()-1); i++; }
		
		return a.size();
	}

	// array support T(n) = O(n)
	public int removeDuplicates(int[] a) {
		if (a == null || a.length == 0)
			return 0;

		int i = 0;
		for (int j = 0; j < a.length; j++)
			if (a[j] != a[i]) a[++i] = a[j];
		
		return (i + 1);
	}
	
	private void printArray(int[] a, int n) {
		if (n > a.length)
			return;

		for(int i = 0; i < n; i++)
			System.out.print(a[i] + ",");
		System.out.println();
	}
	
	private void print(ArrayList<Integer> a) {
		for (Integer x: a) {
			System.out.print(x + ",");
		}
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a      = new int[]    { 1, 1, 2, 3, 3, 3, 4, 5 };
		Integer[] aa = new Integer[]{ 1, 1, 2, 3, 3, 3, 4, 5 };
		ArrayList<Integer> A =new ArrayList<Integer>(Arrays.asList(aa));
		RemoveDuplicatesSortedArray t = new RemoveDuplicatesSortedArray();

		int len = t.removeDuplicates(a);
		System.out.println("len=" + len);
		t.printArray(a, a.length);
		
		len = t.removeDuplicates(A);
		System.out.println("len=" + len);
		t.print(A);
	}
}
