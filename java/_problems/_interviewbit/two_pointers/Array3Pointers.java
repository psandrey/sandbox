package _problems._interviewbit.two_pointers;

/*
 * You are given 3 arrays A, B and C. All 3 of the arrays are sorted.
 * Find i, j, k such that: 
 * max(abs(A[i] - B[j]), abs(B[j] - C[k]), abs(C[k] - A[i])) is minimized.
 * Return the minimum max(abs(A[i] - B[j]), abs(B[j] - C[k]), abs(C[k] - A[i]))
 * 
 * Input : 
 *         A : [1, 4, 10]
 *         B : [2, 15, 20]
 *         C : [10, 12]
 * 
 * Output : 5 
 *          With 10 from A, 15 from B and 10 from C.
 *          
 * Note: The problem can be solved in O(n.logn)
 */

public class Array3Pointers {

	// T(n) = O(n+m+l);
	public int minimize(final int[] a, final int[] b, final int[] c) {
		if (a == null || b == null || c == null) return 0;
		int n = a.length;
		int m = b.length;
		int l = c.length;
		if (n == 0 || m == 0 || l == 0) return 0;
		
		int i = 0, j = 0, k = 0;
		int ans = Integer.MAX_VALUE;
		
		while (i < n && j < m && k < l) {
			//System.out.println(a[i] + " , " + b[j] + " , " + c[k]);
			int d = maxTriple(Math.abs(a[i]-b[j]), Math.abs(b[j]-c[k]), Math.abs(a[i]-c[k]));
			if (ans > d) ans = d;
			
			int min = Math.min(a[i],Math.min(b[j],c[k]));
			
			if (min == a[i]) i++;
			else if (min == b[j]) j++;
			else k++;
		}
		
		return ans;
	}
	
	private int maxTriple(int a, int b, int c) {
		int max = Math.max(a, b);
		max = Math.max(max, c);
		return max;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		int[] a = new int[] {1, 4, 10};
		int[] b = new int[] {2, 15, 20};
		int[] c = new int[] {10, 12};
		/*
		int[] a = new int[] { 1, 4, 5, 8, 10 };
		int[] b = new int[] { 6, 9, 10 };
		int[] c = new int[] { 2, 3, 6, 10 };
		*/
		Array3Pointers t = new Array3Pointers();
		System.out.println(t.minimize(a, b, c));
	}
}
