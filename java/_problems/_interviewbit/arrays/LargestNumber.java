package _problems._interviewbit.arrays;

/*
 * Given a list of non negative integers, arrange them such that
 * they form the largest number. For example, given [3, 30, 34, 5, 9],
 * the largest formed number is 9534330.
 * (Note: The result may be very large, so you need to return a string
 * instead of an integer.)
 */

import java.util.Arrays;
import java.util.Comparator;

public class LargestNumber {

	private String getLargestNumber(int[] a) {
		String[] sA = new String[a.length];
		for (int i = 0; i < a.length; i++)
			sA[i] = String.valueOf(a[i]);
		
		Comparator<String> comp = new Comparator<String>() {
			public int compare(String a, String b) {
				return (b+a).compareTo(a+b);
			}
		};
		
		Arrays.sort(sA, comp);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < sA.length; i++)
			sb.append(sA[i]);

		while (sb.charAt(0) == '0' && sb.length() > 1)
			sb.deleteCharAt(0);

		return sb.toString();
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		int[] a = {3, 30, 34, 5, 9};
		
		LargestNumber l = new LargestNumber();
		String s = l.getLargestNumber(a);
		System.out.println(s);
	}
}
