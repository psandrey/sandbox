package _problems._interviewbit.arrays;

/*
 * Implement the next permutation, which rearranges numbers into the numerically next greater permutation of numbers.
 * If such arrangement is not possible, it must be rearranged as the lowest possible order ie, sorted in an ascending order.
 *
 * Note: The replacement must be in-place, do not allocate extra memory.
 */

public class NextPermutation {

	public int[] nextPermutation(int[] a) {
		if (a == null || a.length == 1 ) return a;

		int n = a.length;
		int k = n - 2;
		while (k >= 0 && a[k] > a[k + 1])k--;
		if (k < 0) {
			int i = 0, j = n - 1;
			while ( i++ < j--) { int t = a[i]; a[i] = a[j]; a[j] = t; }
		} else {
			int l = n - 1;
			while (a[k] > a[l]) l--;
			int t = a[k]; a[k] = a[l]; a[l] = t;
			
			int i = k+1, j = n - 1;
			while ( i < j) { t = a[i]; a[i] = a[j]; a[j] = t; i++; j--;}
		}
		
		return a;
	}
    
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = {20, 50, 113};
		
		NextPermutation t = new NextPermutation();
		int[] np = t.nextPermutation(a);
		for (int i = 0; i < np.length; i++) System.out.print(np[i] + " ");
		
	}
}
