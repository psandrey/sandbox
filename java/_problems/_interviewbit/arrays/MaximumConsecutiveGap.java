package _problems._interviewbit.arrays;

/*
 * Given an unsorted array, find the maximum difference between the successive elements in its sorted form.
 *
 * Try to solve it in linear time/space.
 *
 * Example :
 * Input : [1, 10, 5]
 * Output : 5
 * Return 0 if the array contains less than 2 elements.
 *
 * You may assume that all the elements in the array are non-negative integers and fit in the 32-bit signed integer range.
 * You may also assume that the difference will not overflow.
 */

public class MaximumConsecutiveGap {

	public int maximumGap(final int[] a) {
		if (a.length < 2) return 0;
		int n = a.length;
		
		// bucket sort + pigeonhole principle
		int m = Integer.MAX_VALUE, M = Integer.MIN_VALUE;
		for (int j = 0; j < n; j++) { m = Math.min(m, a[j]); M = Math.max(M, a[j]); }
		if (M - m == 0) return 0;
		
		int[] b_min = new int[n - 1];
		int[] b_max = new int[n - 1];
		for (int j = 0; j < n - 1; j++) { b_min[j] = Integer.MAX_VALUE; b_max[j] = Integer.MIN_VALUE; }
		
		// bucket-sort: distribute n - 2 values into n - 1 buckets, thus
		//  at least 1 bucket remains empty (by pigeonhole principle) revealing
		//  the biggest gap between consecutive values in sorted the array
		double d = (double)(M - m)/((double)(n-1));
		for (int j = 0; j < n; j++) {
			if (a[j] == m || a[j] == M) continue;
			
			int i = (int)((double)(a[j] - m) / d);
			b_min[i] = Math.min(b_min[i], a[j]);
			b_max[i] = Math.max(b_max[i], a[j]);
		}
		
		int prev = m, max_gap = Integer.MIN_VALUE;
		for (int j = 0; j < n - 1; j++) {
			if (b_min[j] == Integer.MAX_VALUE) continue;

			max_gap = Math.max(max_gap, b_min[j] - prev);
			prev = b_max[j];
		}
		max_gap = Math.max(max_gap, M - prev);
		
		return max_gap;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
/*
		int[] a = { 46158044, 9306314, 51157916, 93803496, 20512678, 55668109, 488932,
				24018019, 91386538, 68676911, 92581441, 66802896, 10401330, 57053542,
				42836847, 24523157, 50084224, 16223673, 18392448, 61771874, 75040277,
				30393366, 1248593, 71015899, 20545868, 75781058, 2819173, 37183571,
				94307760, 88949450, 9352766, 26990547, 4035684, 57106547, 62393125,
				74101466, 87693129, 84620455, 98589753, 8374427, 59030017, 69501866,
				47507712, 84139250, 97401195, 32307123, 41600232, 52669409, 61249959,
				88263327, 3194185, 10842291, 37741683, 14638221, 61808847, 86673222,
				12380549, 39609235, 98726824, 81436765, 48701855, 42166094, 88595721,
				11566537, 63715832, 21604701, 83321269, 34496410, 48653819, 77422556,
				51748960, 83040347, 12893783, 57429375, 13500426, 49447417, 50826659,
				22709813, 33096541, 55283208, 31924546, 54079534, 38900717, 94495657,
				6472104, 47947703, 50659890, 33719501, 57117161, 20478224, 77975153,
				52822862, 13155282, 6481416, 67356400, 36491447, 4084060, 5884644,
				91621319, 43488994, 71554661, 41611278, 28547265, 26692589, 82826028,
				72214268, 98604736, 60193708, 95417547, 73177938, 50713342, 6283439,
				79043764, 52027740, 17648022, 33730552, 42851318, 13232185, 95479426,
				70580777, 24710823, 48306195, 31248704, 24224431, 99173104, 31216940,
				66551773, 94516629, 67345352, 62715266, 8776225, 18603704, 7611906 };
*/
		int[] a = {83089327, 27703799, 6760166, 77644755, 77997282, 40163231, 26824430,
				51194068, 30111403, 33448636, 28357643, 62008750, 39568094};

		MaximumConsecutiveGap t = new MaximumConsecutiveGap();
		int max_gap = t.maximumGap(a);
		System.out.println(max_gap);
	}
}
