package _problems._interviewbit.arrays;

/*
 * You are in an infinite 2D grid where you can move in any of the 8 directions :
 * You are given a sequence of points and the order in which you need to cover the points. 
 * Give the minimum number of steps in which you can achieve it. You start from the first point.
 *
 * Example: 
 *  Input : [(0, 0), (1, 1), (1, 2)]
 *  Output : 2
 */

public class MinStepsInInfiniteGrid {
	public int coverPoints(int[] X, int[] Y) {
		if (X.length <= 1) return 0;
		
		int x = X[0], y = Y[0], steps = 0;
		for (int k = 1; k < X.length; k++) {
			int a = X[k], b = Y[k];
			if (a == x && b == y) continue;
			
			int dx = Math.abs(a - x), dy = Math.abs(b - y);
			steps += Math.min(dx, dy) + Math.abs(dy - dx);
			//steps += Math.max(dx, dy);

			x = a; y = b;
		}
		
		return steps;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		MinStepsInInfiniteGrid t = new MinStepsInInfiniteGrid();
		int[] X = { 4, 8, -7, -5, -13, 9, -7, 8 };
		int[] Y = { 4, -15, -10, -3, -13, 12, 8, -8 };

		System.out.println(t.coverPoints(X, Y));
	}
}
