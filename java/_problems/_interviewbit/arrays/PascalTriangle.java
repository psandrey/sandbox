package _problems._interviewbit.arrays;

/*
 * Given n, generate first n rows of Pascal's triangle.
 */

import java.util.ArrayList;

public class PascalTriangle {
	
	public ArrayList<ArrayList<Integer>> solve(int n) {
		ArrayList<ArrayList<Integer>> ans = new ArrayList<ArrayList<Integer>>();
		if (n == 0) return ans;
		
		ArrayList<Integer> prevRow = new ArrayList<Integer>();
		prevRow.add(1);
		ans.add(prevRow);
		for (int i = 1; i < n; i++) {
			ArrayList<Integer> row = new ArrayList<Integer>();
			row = new ArrayList<Integer>();
			row.add(1);
			for (int j = 1; j < i; j++) row.add(prevRow.get(j-1)+prevRow.get(j));
			row.add(1);
			ans.add(row);
			prevRow = row;
		}
		
		return ans;
	}
	
	private void print(ArrayList<ArrayList<Integer>> ll) {
		for (ArrayList<Integer> l: ll) {
			System.out.print("[");
			for (Integer x: l ) System.out.print(x + ", ");
			System.out.print("]");
			System.out.println();
		}
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int n = 5;
		PascalTriangle t = new PascalTriangle();
		ArrayList<ArrayList<Integer>> pascal = t.solve(n);
		t.print(pascal);
	}
}
