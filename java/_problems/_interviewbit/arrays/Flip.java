package _problems._interviewbit.arrays;

/*
 * You are given a binary string(i.e. with characters 0 and 1) S consisting of characters S1, S2, ..., SN.
 * In a single operation, you can choose two indices L and R such that 1 <= L <= R <= N and
 * flip the characters SL, SL+1, ..., SR. By flipping, we mean change character 0 to 1 and vice-versa.
 *
 * Your aim is to perform ATMOST one operation such that in final string number of 1s is maximised.
 * If you don�t want to perform the operation, return an empty array. Else, return an array
 * consisting of two elements denoting L and R. If there are multiple solutions,
 * return the lexicographically smallest pair of L and R.
 *
 * Example:
 * S = 010
 *
 * Pair of [L, R] | Final string
 * _______________|_____________
 * [1 1]          | 110
 * [1 2]          | 100
 * [1 3]          | 101
 * [2 2]          | 000
 * [2 3]          | 001
 * [3 3]          | 011
 *
 * We see that [1, 1], [1, 3] and [3, 3] give same number of 1s in final string.
 * So, we return [1, 1].
 *
 */

public class Flip {
	public int[] flip(String s) {
		if (s.isEmpty()) return new int[] {};
		
		int k = 0, c = -1, b = 0, e = 0, maxc = 0;
		for (int j = 0; j < s.length(); j++) {
			if (c == -1) {k = j; c = 0; }
			
			if (s.charAt(j) == '0') c++;
			else c--;
			
			if (maxc < c) { b = k; e = j; maxc = c; }
		}
		
		
		if (maxc > 0) {return new int[] {b + 1, e + 1}; }
		return new int[] {};
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String s = "1101010001";
		
		Flip t = new Flip();
		int[] ans = t.flip(s);
		System.out.println(ans[0] + " , " + ans[1]);
	}
}
