package _problems._interviewbit.arrays;

/*
 * You are given an array A containing N integers. The special product of each ith integer in this array is defined
 *  as the product of the following:<ul>
 *
 * LeftSpecialValue: For an index i, it is defined as the index j such that A[j]>A[i] (i>j).
 *    If multiple A[j]�s are present in multiple positions, the LeftSpecialValue is the maximum value of j. 
 * RightSpecialValue: For an index i, it is defined as the index j such that A[j]>A[i] (j>i).
 *    If multiple A[j]s are present in multiple positions, the RightSpecialValue is the minimum value of j.
 * 
 * Write a program to find the maximum special product of any integer in the array.
 *  Input: You will receive array of integers as argument to function.
 *  Return: Maximum special product of any integer in the array modulo 1000000007.
 * 
 * Note: If j does not exist, the LeftSpecialValue and RightSpecialValue are considered to be 0.
 * Constraints 1 <= N <= 10^5 1 <= A[i] <= 10^9
 *
 * Note: solved based on the algorithm: Next Greater Element (using stack)
 */

import java.util.Stack;

public class Maxspprod {

	public int maxSpecialProduct(int[] a) {
		int n = a.length;
		Stack<Integer> st;
		
		int[] Ls = new int[n];
		st = new Stack<Integer>();
		for (int i = 0; i < n; i++) {
			while (!st.isEmpty() && a[st.peek()] <= a[i]) st.pop();
			
			int m = 0;
			if (!st.isEmpty()) m = st.peek();
			Ls[i] = m;
			
			st.push(i);
		}
		
		st = new Stack<Integer>();
		int[] Rs = new int[n];
		for (int i = n - 1; i >= 0; i--) {
			while (!st.isEmpty() && a[st.peek()] <= a[i]) st.pop();
			
			int m = 0;
			if (!st.isEmpty()) m = st.peek();
			Rs[i] = m;
			
			st.push(i);
		}
		
		
		long mx = Long.MIN_VALUE;
		for(int i = 0; i < n; i++)
			mx = Long.max(mx, 1L * Ls[i] * Rs[i]);

		return (int)(mx % 1000000007);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = { 6, 7, 9, 5, 5, 8 };
		
		Maxspprod t = new Maxspprod();
		System.out.println(t.maxSpecialProduct(a));
	}
}
