package _problems._interviewbit.arrays;

public class SpiralOrderMatrixII {

	public int fill_edges(int[][] a, int k, int n, int m) {
		if (n == 1) a[k][k] = m++;
		else {
			// upper line
			for (int j = 0; j < n - 1; j++) a[k][k+j] = m++;
			// right col
			for (int j = 0; j < n - 1; j++) a[k + j][k + n - 1] = m++;
			//bottom line
			for (int j = n - 1; j > 0; j--) a[k + n - 1][k + j] = m++;
			//left col
			for (int j = n - 1; j > 0; j--) a[k + j][k] = m++;
		}
		
		return m;
	}
	
	public int[][] generateMatrix(int n) {
		int[][] a = new int[n][n];
		
		int m = 1;
		for (int k = 0; k < (n + 1)/2; k++)
			m = fill_edges(a, k, n - 2*k, m);
		
		return a;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int n = 3;
		SpiralOrderMatrixII t = new SpiralOrderMatrixII();
		
		int[][] a = t.generateMatrix(n);
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) System.out.print(a[i][j] + " ");
			System.out.println();
		}
	}
}
