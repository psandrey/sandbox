package _problems._interviewbit.arrays;

/*
 * Given a non-negative number represented as an array of digits,
 * add 1 to the number ( increment the number represented by the digits ).
 * The digits are stored such that the most significant digit is at the head of the list.
 * 
 * Example:
 * If the vector has [1, 2, 3]
 * the returned vector should be [1, 2, 4]
 */

import java.util.ArrayList;

public class AddOneToNumber {

	public ArrayList<Integer> plusOne(ArrayList<Integer> num) {
		if (num == null || num.size() == 0) {
			num = new ArrayList<Integer>(); num.add(1); return num; }

		// increment number
		int n = num.size();
		int i = n-1, carry = 0;
		while (i >= 0) {
			int d = num.get(i); d++;
			carry = 0;
			if (d == 10) { d = 0; carry = 1; } 
			
			num.set(i, d); 
			if (carry == 0) break;
			i--;
		}
				
		// we have carry bit == 1, so take care of it
		if (i == -1 && carry == 1) { num.add(0, 1); return num; }
		
		// cut leading zeroes (if any)
		i = 0;
		while (i < n && num.get(i) == 0) i++;
		if (i == 0) return num; // no leading zeroes, so just return num
		
		// make a copy, but exclude the leading zeroes
		return new ArrayList<Integer>(num.subList(i, n));
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		ArrayList<Integer> num = new ArrayList<Integer>();
		num.add(0);num.add(9);
		
		AddOneToNumber t = new AddOneToNumber();
		ArrayList<Integer> ans = t.plusOne(num);
		System.out.println(ans);
		
	}
}
