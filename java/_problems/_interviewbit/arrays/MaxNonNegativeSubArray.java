package _problems._interviewbit.arrays;

/*
 * Find out the maximum sub-array of non negative numbers from an array.
 * The sub-array should be continuous.
 *  
 * Maximum sub-array is defined in terms of the sum of the elements in the sub-array.
 * Sub-array A is greater than sub-array B if sum(A) > sum(B).
 * 
 * NOTE: If there is a tie, then compare with segment's length and return segment
 *  which has maximum length.
 * NOTE 2: If there is still a tie, then return the segment with minimum starting index.
 */

import java.util.Arrays;

public class MaxNonNegativeSubArray {

	public int[] maxset(int[] a) {
		class Seg {
			int start, end, len;
			public Seg(int s, int e) { start = 0; end = 0; len = 0; }
		};
		if (a == null || a.length == 0) return new int[] {};
		
		int n = a.length, i = 0;
		// 1. advance to the first non-negative element
		while (i < n && a[i] < 0) i++;
		
		// 2. find the largest non-negative sub-array
		long sum = 0L, maxSum = -1L;
		int j = i;
		Seg seg = new Seg(0, 0);
		while (i < n) {
			if (a[i] < 0) {
				if (sum > maxSum || (sum == maxSum && i - j > seg.len)) {
					maxSum = sum;
					seg.len = i - j;
					seg.start = j;
					seg.end = i;
				}

				j = i + 1;
				sum = 0;
				i++; continue;
			}
			
			sum += ((long)(a[i]));
			i++;
		}
		
		// case: the array is made of zeroes
		// case: the last sub-array is the maximum array
		if (a[i-1] == 0 || sum > 0) {
			if (sum > maxSum || (sum == maxSum && i - j > seg.len)) {
				maxSum = sum;
				seg.len = i - j;
				seg.start = j;
				seg.end = i;
			}
		}
	
		if (maxSum < 0) return new int[] {};
		
		return Arrays.copyOfRange(a, seg.start, seg.end);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = new int[] {1967513926, 1540383426, -1303455736, -521595368};
		
		MaxNonNegativeSubArray t = new MaxNonNegativeSubArray();
		int[] ans = t.maxset(a);
		
		for(int i = 0; i < ans.length; i++) System.out.print(ans[i] + ", ");
	
	}
}
