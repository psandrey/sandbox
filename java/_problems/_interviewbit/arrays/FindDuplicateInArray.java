package _problems._interviewbit.arrays;

/*
 * Given a read only array of n + 1 integers between 1 and n, find one number
 *  that repeats in linear time using less than O(n) space and traversing the stream
 *  sequentially O(1) times.
 *  
 *  Note: Tortoise and hare algorithm.
 *  See here the explanation: http://keithschwarz.com/interesting/code/?dir=find-duplicate
 */

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class FindDuplicateInArray {

	public int repeatedNumber(final List<Integer> a) {
		if (a == null || a.size() == 0 || a.size() == 1) return -1;
		
		int tortoise = 0;
		int hare = 0;
		
		/*
		 * Since the number are from 1..n, then index 0 has no incoming edges, also
		 * for sure we won't see a new cell forever, so Floyd's algorithm will detect
		 * a cycle.
		 */
		int steps = 0;
		do {
			tortoise = a.get(tortoise);
			hare = a.get(a.get(hare));
			steps++;
		} while (tortoise != hare);
		
		System.out.println("Meet after:" + steps + " steps");
		
		/*
		 * Now, we know that tortoise and hare meet after k steps and hare too 2k steps, so
		 * if we start a new tortoise they will met after exactly k steps and point to a cell
		 * that has at least 2 incoming edges.
		 * 
		 * Note: Don' Knuth needed 24h to prove this :)... very nice proof...
		 */
		int find = 0;
		steps = 0;
		while (find != tortoise) {
			find = a.get(find);
			tortoise = a.get(tortoise);
			steps++;
		}
		
		System.out.println("Meet after:" + steps + " steps");
		
		return find;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//Integer[] a = new Integer[] {3, 2, 4, 1, 2};
		Integer[] a = new Integer[] {3, 4, 1, 4, 1};
		List<Integer> A = new LinkedList<Integer>(Arrays.asList(a));
		
		FindDuplicateInArray t = new FindDuplicateInArray();
		System.out.println(t.repeatedNumber(A));
		
	}
}
