package _problems._interviewbit.arrays;

/*
 * You�re given a read only array of n integers.
 * Find out if any integer occurs more than n/3 times in the array in linear time and
 *  constant additional space.
 *  
 *  If so, return the integer. If not, return -1.
 *  If there are multiple solutions, return any one.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class N3RepeatNumber {

	public int repeatedNumber(final List<Integer> a) {
		if (a == null) return -1;
		int n = a.size();
		if (n == 1) return a.get(0);
		
		/*
		 * There can be at most 2 majority elements > n/3.
		 * So, we apply the Moore's algorithm... and find two candidates.
		 */
		Integer m1 = null, m2 = null;
		int c1 = 0, c2 = 0;
		for (int i = 0; i < n; i++) {
			int x = a.get(i);
			
			if (m1 != null && m1.intValue() == x) c1++;
			else if (m2 != null && m2.intValue() == x) c2++;
			else if (m1 == null || c1 == 0) { m1 = x; c1 = 1; }
			else if (m2 == null || c2 == 0) { m2 = x; c2 = 1; }
			else { c1--; c2--; }
		}
		
		/* We know that Moore's algorithm doesn't find the most frequent element, unless it is
		 * majority element. So, we have two candidates, but we do not know if any of them is majority. */
		c1 = 0; c2 = 0;
		for (int i = 0; i < n; i++) {
			int x = a.get(i);
			
			if (m1 != null && m1.intValue() == x) c1++;
			else if (m2 != null && m2.intValue() == x) c2++;
		}
		
		if (c1 > n/3) return m1.intValue();
		else if (c2 > n/3) return m2.intValue();

		return -1;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[] a = new Integer[] {1,2,1,2,1};
		List<Integer> l = new ArrayList<Integer>(Arrays.asList(a));
		
		N3RepeatNumber t = new N3RepeatNumber();
		int ans = t.repeatedNumber(l);
		System.out.println(ans);
	}
}
