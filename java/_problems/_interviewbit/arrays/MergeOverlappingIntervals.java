package _problems._interviewbit.arrays;

/*
 * Given a collection of intervals, merge all overlapping intervals.
 * 
 * For example:
 *     Given [1,3],[2,6],[8,10],[15,18],
 *     return [1,6],[8,10],[15,18].
 *     
 * Note: Make sure the returned intervals are sorted.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MergeOverlappingIntervals {

	private class Interval {
		int start;
		int end;
		public Interval(int s, int e) { start = s; end = e; }
	}
	
	public ArrayList<Interval> merge(ArrayList<Interval> is) {
		ArrayList<Interval> ans = new ArrayList<Interval>();
		if (is == null || is.size() == 0) return ans;
		int n = is.size();
		if (n == 1) return is;

		Comparator<Interval> comp = new Comparator<Interval>() {
			@Override
			public int compare(Interval i1, Interval i2) {
				return i1.start - i2.start;
			}
		};
		Collections.sort(is, comp);
		
		int  j = 0;
		while (j < n) {
			Interval i = is.get(j);
			Interval merged = new Interval(i.start, i.end);

			while (j+1 < n &&  (i = is.get(j+1)).start <= merged.end) {
				merged.end = Math.max(merged.end, i.end);
				j++;
			}
			
			ans.add(merged);
			j++;
		}
		
		return ans;
	}
	
	private ArrayList<Interval> buildIntervals() {
		ArrayList<Interval> is = new ArrayList<Interval>(4);
		is.add(new Interval(2, 6));
		is.add(new Interval(15, 18));
		is.add(new Interval(8, 10));
		is.add(new Interval(1, 3));

		return is;
	}
	
	private void printIntervals(ArrayList<Interval> is) {
		for(Interval i: is) {
			System.out.print("[" + i.start + " , " + i.end + "]");
		}
		System.out.println();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		MergeOverlappingIntervals t = new MergeOverlappingIntervals();
		ArrayList<Interval> is = t.buildIntervals();
		ArrayList<Interval> ans = t.merge(is);
		t.printIntervals(ans);
	}
}
