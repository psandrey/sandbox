package _problems._interviewbit.arrays;

/*
 * A hotel manager has to process N advance bookings of rooms for the next season.
 * His hotel has K rooms. Bookings contain an arrival date and a departure date.
 * He wants to find out whether there are enough rooms in the hotel to satisfy the demand.
 * Write a program that solves this problem in time O(N log N).
 *
 * Input:
 *   First list for arrival time of booking.
 *   Second list for departure time of booking.
 *   Third is K which denotes count of rooms.
 *
 * Output:
 *   A boolean which tells whether its possible to make a booking.
 *   Return 0/1 for C programs.
 *   O -> No there are not enough rooms for N booking.
 *   1 -> Yes there are enough rooms for N booking.
 * 
 * Example :
 * Input :
 * 		Arrivals :   [1 3 5]
 * 		Departures : [2 6 8]
 * 		K : 1
 *
 * Return : False / 0
 * At day = 5, there are 2 guests in the hotel. But I have only one room.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class HotelBookingsPossible {
	public boolean hotel(ArrayList<Integer> a, ArrayList<Integer> d, int k) {
		if (a.isEmpty()) return true;
		
		// sort arrivals and departure
		Collections.sort(a);
		Collections.sort(d);
		
		// each time someone depart (free a room) k++
		// each time someone arrives (occupies a room) k--
		// if the arrival time = departure time, then the rooms remains unchanged
		int i = 0, j = 0;
		while (i < a.size() && j < d.size()) {
			if (a.get(i) < d.get(j)) { k--; i++; }
			else if (a.get(i) < d.get(j)) { k++; j++; }
			else { i++; j++; }
			
			if (k < 0) return false;
		}
		
		return true;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[] a = {2, 2};
		Integer[] d = {2, 5};
		ArrayList<Integer> A = new ArrayList<>(Arrays.asList(a));
		ArrayList<Integer> D = new ArrayList<>(Arrays.asList(d));
		
		HotelBookingsPossible t = new HotelBookingsPossible();
		System.out.println(t.hotel(A, D, 0));
	}
}
