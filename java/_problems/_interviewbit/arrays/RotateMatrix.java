package _problems._interviewbit.arrays;

/*
 * You are given an n x n 2D matrix representing an image.
 * Rotate the image by 90 degrees (clockwise).
 * 
 * Note: You need to do this in place.
 */

import java.util.ArrayList;
import java.util.Arrays;

public class RotateMatrix {
	
	// Iterative version with arrays
	public void rotate(int[][] a) {
		if (a == null || a.length == 1) return;

		int n = a.length;
		int nEdges = n/2;
		for (int edge = 0; edge <= nEdges; edge++)
			rotateEdge(a, edge, n-1);
	}
	
	private void rotateEdge(int[][] a, int edge, int n) {
		int size = n-2*edge; // size of the sub-matrix
		int m = n-edge; // outer-edge
		
		for (int off = 0; off < size; off++) {			
			int t = a[edge][edge+off];
			a[edge][edge+off] = a[m-off][edge];
			a[m-off][edge]    = a[m][m-off];
			a[m][m-off]       = a[edge+off][m];
			a[edge+off][m]    = t;
		}
	}

	// Iterative version with ArrayList
	public void rotate(ArrayList<ArrayList<Integer>> a) {
		if (a == null || a.size() == 1) return;

		int n = a.size();
		int nEdges = n/2;
		for (int edge = 0; edge <= nEdges; edge++)
			rotateEdge(a, edge, n-1);
	}
	
	private void rotateEdge(ArrayList<ArrayList<Integer>> a, int edge, int n) {
		int size = n-2*edge; // size of the sub-matrix
		int m = n-edge; // outer-edge
		
		ArrayList<Integer> lineEdge = a.get(edge); // first line
		ArrayList<Integer> lineM = a.get(m); // last line
		for (int off = 0; off < size; off++) {			
			int t = lineEdge.get(edge+off);
			lineEdge.set(edge+off, a.get(m-off).get(edge));
			a.get(m-off).set(edge, lineM.get(m-off));
			lineM.set(m-off, a.get(edge+off).get(m));
			a.get(edge+off).set(m,t);
		}
	}

	// The recursive version
	private void rotateRecursice(int[][] a) {
		if (a == null || a.length <= 1)
			return;
		
		rotate(a, 0, a.length-1);
	}
	
	private void rotate(int[][] a, int start, int limit) {
		if (limit <= 0)
			return;

		for (int j = start; j < start+limit; j++) {
			int t = a[start][j];
			int jOff = j-start;
			a[start][j] = a[start+limit-jOff][start];
			a[start+limit-jOff][start] = a[start+limit][start+limit-jOff];
			a[start+limit][start+limit-jOff] = a[j][start+limit];
			a[j][start+limit] = t;
		}
		
		rotate (a, start+1, limit-2);
	}

	private void printMatrix(int[][] a) {
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				System.out.print(a[i][j] + "  ");
			}
			System.out.println();
		}
	}

	private void printMatrix(ArrayList<ArrayList<Integer>> a) {
		for (ArrayList<Integer> line : a) {
			for (Integer e: line) {
				System.out.print(e + "  ");
			}
			System.out.println();
		}
	}

	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
/*
		int[][] a = new int[][] {{1}};
	
		int[][] a = new int[][]
			{{1, 2, 3},
			 {4, 5, 6},
			 {7, 8, 9}};

		int[][] a = new int[][]
				{{ 1,  2,  3,  4},
				 { 5,  6,  7,  8},
				 { 9, 10, 11, 12},
				 {13, 14, 15, 16}};

		int[][] a = new int[][]
				{{ 1,  2,  3,  4,  5},
				 { 6,  7,  8,  9, 10},
				 {11, 12, 13, 14, 15},
				 {16, 17, 18, 19, 20},
				 {21, 22, 23, 24, 25}};

*/
		ArrayList<Integer> line1 = new ArrayList<>(Arrays.asList(new Integer[]{ 1,  2,  3,  4}));
		ArrayList<Integer> line2 = new ArrayList<>(Arrays.asList(new Integer[]{ 5,  6,  7,  8}));
		ArrayList<Integer> line3 = new ArrayList<>(Arrays.asList(new Integer[]{ 9, 10, 11, 12}));
		ArrayList<Integer> line4 = new ArrayList<>(Arrays.asList(new Integer[]{13, 14, 15, 16}));
		ArrayList<ArrayList<Integer>> a = new ArrayList<ArrayList<Integer>>();
		a.add(line1);
		a.add(line2);
		a.add(line3);
		a.add(line4);
		
		RotateMatrix t = new RotateMatrix();
		t.rotate(a);
		t.printMatrix(a);
	}
}
