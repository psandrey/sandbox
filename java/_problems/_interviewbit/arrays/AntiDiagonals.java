package _problems._interviewbit.arrays;

/*
 * Give a N*N square matrix, return an array of its anti-diagonals.
 * Look at the example for more details.
 *
 * Input: 	
 *   1 2 3
 *   4 5 6
 *   7 8 9
 *
 * Return the following :
 *   [1],
 *   [2, 4],
 *   [3, 5, 7],
 *   [6, 8],
 *   [9]
 */

import java.util.ArrayList;
import java.util.Arrays;

public class AntiDiagonals {

	public ArrayList<ArrayList<Integer>> diagonal(ArrayList<ArrayList<Integer>> a) {
		if (a.isEmpty()) return new ArrayList<ArrayList<Integer>>();
		
		int n = a.size();
		ArrayList<ArrayList<Integer>> d = new ArrayList<ArrayList<Integer>>(2*n - 1);
		for (int i = 0; i < 2*n - 1; i++)d.add(new ArrayList<Integer>());

		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++) {
				ArrayList<Integer> row = d.get(i + j);
				row.add(a.get(i).get(j));
			}
		
		return d;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[][] a = { { 1, 2, 3 },
				  { 4, 5, 6 },
				  { 7, 8, 9 } };
		
		ArrayList<ArrayList<Integer>> A = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i < a.length; i++)
			A.add(new ArrayList<Integer>(Arrays.asList(a[i])));

		AntiDiagonals t = new AntiDiagonals();
		ArrayList<ArrayList<Integer>> d = t.diagonal(A);
		
		for (int i = 0; i < d.size(); i++) {
			for (int j = 0; j < d.get(i).size(); j++)
				System.out.print(d.get(i).get(j) + " ");
			System.out.println();
		}
	}
}
