package _problems._interviewbit.arrays;

/*
 * Given an array of integers, sort the array into a wave like array and return it.
 * In other words, arrange the elements into a sequence such that a1 >= a2 <= a3 >= a4 <= a5.....
 */

import java.util.ArrayList;
import java.util.Collections;

public class WaveArray {

	public ArrayList<Integer> wave(ArrayList<Integer> a) {
		if (a == null || a.size() == 0) return new ArrayList<Integer>();
		
		Collections.sort(a);
		int n = a.size(), i = 0;
		while (i < n) {
			// switch
			if (i+1 < n) {
				int temp = a.get(i);
				a.set(i, a.get(i+1));
				a.set(i+1, temp);
			}
			i += 2;
		}
		
		return a;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		ArrayList<Integer> a = new ArrayList<Integer>();
		a.add(2);
		a.add(1);
		a.add(3);
		
		WaveArray t = new WaveArray();
		ArrayList<Integer> ans = t.wave(a);
		System.out.println(ans);
	}
}
