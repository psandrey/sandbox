package _problems._interviewbit.arrays;

/*
 * Given a set of non-overlapping intervals, insert a new interval into the intervals
 * (merge if necessary).
 * 
 * Note: You may assume that the intervals were initially sorted according to their start times.
 * 
 * Example 1:
 * Given intervals [1,3],[6,9] insert and merge [2,5] would result in [1,5],[6,9].
 * 
 * Example 2:
 * Given [1,2],[3,5],[6,7],[8,10],[12,16], insert and merge [4,9] would result in [1,2],[3,10],[12,16].
 * This is because the new interval [4,9] overlaps with [3,5],[6,7],[8,10].
 * 
 * Note: Make sure the returned intervals are also sorted.
 */

import java.util.ArrayList;
import java.util.List;

public class MergeIntervals {

	private class Interval {
		int start;
		int end;
		public Interval(int s, int e) { start = s; end = e; }
	}

	// Version: creates new ArrayList of intervals
	public ArrayList<Interval> insert(ArrayList<Interval> is, Interval newI) {
		ArrayList<Interval> ans = new ArrayList<Interval>();

		if (is == null && newI == null) return ans;
		if ((is == null || is.size() == 0) && newI != null) { ans.add(newI); return ans; }

		int n = is.size();
		int i = 0;
		
		// 1. add all previous intervals
		while (i < n && is.get(i).end < newI.start) { ans.add(is.get(i)); i++; }
		
		// if all intervals are previous, then add new interval and return
		if (i == n) { ans.add(newI); return ans; }
		
		// 2. merge intervals
		if (newI.end >= is.get(i).start) {
			newI.start = Math.min(newI.start, is.get(i).start);

			while (i < n && is.get(i).start <= newI.end) {
				newI.end = Math.max(newI.end, is.get(i).end);
				i++;
			}
		}
		
		// 3. add the new interval
		ans.add(newI);
		
		// 4. add successors if any
		while (i < n) { ans.add(is.get(i)); i++; }		
		
		// done
		return ans;
	}

	// faster version
	public ArrayList<Interval> inPlaceInsert(ArrayList<Interval> is, Interval newI) {
		if (is == null && newI == null) return is;
		if ((is == null || is.size() == 0) && newI != null) {
			if (is == null) is = new ArrayList<Interval>();
			is.add(newI); return is;
		}
		ArrayList<Interval> result = new ArrayList<Interval>();
		
		// add first non-overlapping intervals
		int offset = getOffsetOfLastNonOverlappingInterval(is, newI);
		result.addAll(is.subList(0, offset));
		
		// merge all overlapping intervals
		for (int i = offset; i < is.size(); i++) {
			Interval cur = is.get(i);
			if (cur.end < newI.start)
				result.add(cur);
			else if (cur.start > newI.end) {
				result.add(newI);
				newI = cur;
			} else if (cur.end >= newI.start || cur.start <= newI.end) {
				newI.start = Math.min(cur.start, newI.start);
				newI.end = Math.max(cur.end, newI.end);
			}
		}
		result.add(newI);

		return result;
		
	}
	
	private int getOffsetOfLastNonOverlappingInterval(List<Interval> intervals, Interval newInterval) {
		int i = 0;
		int j = intervals.size() - 1;
		
		while ( i < j ) {
			int mid = i + (j - i)/2;
			if (newInterval.start <= intervals.get(mid).start) j = mid;
			else i = mid + 1;
		}

		return j == 0 ? 0 : j - 1;
	}
	
	private ArrayList<Interval> buildIntervals() {
		
		ArrayList<Interval> is = new ArrayList<Interval>();
		/*
		// test 1: insert: 4,9
		is.add(new Interval(1, 2));
		is.add(new Interval(3, 5));
		is.add(new Interval(6, 7));
		is.add(new Interval(8, 10));
		is.add(new Interval(12, 16));
		*/
		
		// test 2: insert: 2,5
		is.add(new Interval(1, 3));
		is.add(new Interval(6, 9));
		
		/*
		// test 3 : insert: 1,5 and insert: 1,2
		is.add(new Interval(2, 3));
		*/
		
		/*
		// test 4 : insert: 3,4 and insert: 4,5
		is.add(new Interval(2, 3));
		*/
		return is;
	}
	private Interval getNewInterval(int s, int e) {
		return new Interval(s, e);
	}
	
	private void printIntervals(ArrayList<Interval> is) {
		for(Interval i: is) {
			System.out.print("[" + i.start + ", " + i.end + "] ");
		}
		System.out.println();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		MergeIntervals t = new MergeIntervals();
		
		ArrayList<Interval> is = t.buildIntervals();
		Interval newI = t.getNewInterval(5, 5);
		
		ArrayList<Interval> ans= t.inPlaceInsert(is, newI);
		t.printIntervals(ans);
	}
}
