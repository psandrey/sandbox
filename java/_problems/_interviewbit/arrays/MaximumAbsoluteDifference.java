package _problems._interviewbit.arrays;

/*
 * You are given an array of N integers, A1, A2 ,..., AN. Return maximum value of f(i, j) for all 1 <= i, j <= N.
 * f(i, j) is defined as |A[i] - A[j]| + |i - j|, where |x| denotes absolute value of x.
 */

public class MaximumAbsoluteDifference {

	public int maxArr(int[] a) {
		// T(n) = O(n)
		int max_pos = Integer.MIN_VALUE, min_pos = Integer.MAX_VALUE,
			max_neg = Integer.MIN_VALUE, min_neg = Integer.MAX_VALUE;

		for (int i = 0; i < a.length; i++) {
			max_pos = Math.max(max_pos, a[i] + i);
			min_pos = Math.min(min_pos, a[i] + i);
			
			max_neg = Math.max(max_neg, a[i] - i);
			min_neg = Math.min(min_neg, a[i] - i);
		}

		return Math.max(max_pos - min_pos, max_neg - min_neg);
/*
		// T(n) = O(n^2)
		int ans = Integer.MIN_VALUE;
		if (a.length <= 1) return ans;
		
		for (int i = 0; i < a.length - 1; i++)
			for (int j = i + 1; j < a.length; j++) {
				int da = Math.abs(a[i] - a[j]);
				int di = Math.abs(i - j);
				ans = Math.max(ans, da + di);
			}
			
		return ans;
*/
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = { -39, -24, 82, 95, 91, -65, 16, -76, -56, 70 };
		
		MaximumAbsoluteDifference t = new MaximumAbsoluteDifference();
		System.out.println(t.maxArr(a));
	}
}
