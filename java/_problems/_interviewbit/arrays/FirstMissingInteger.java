package _problems._interviewbit.arrays;

/*
 * Given an unsorted integer array, find the first missing positive integer.
 *
 * Example:
 *
 * Given [1,2,0] return 3,
 *   [3,4,-1,1] return 2,
 *   [-8, -7, -6] returns 1
 *
 * Note: Your algorithm should run in O(n) time and use constant space.
 */

public class FirstMissingInteger {
	
	public int firstMissingPositive(int[] a) {
		if (a.length == 0) return 1;
		
		int j = 0;
		while (j < a.length) {
			// 0 and in-place values are ignored
			if (a[j] <= 0 || a[j] == j + 1) j++;
			// all values that are beyond a.length and in-placed values will be ignored
			else if (a[j] > a.length || a[a[j]-1] == a[j]) a[j] = -a[j];
			// all values until a.length are put in-place
			else {
				int k = a[j];
				a[j] = a[k - 1];
				a[k - 1] = k; 
			}
		}
		
		// find the first missing integer between 1..a.length
		for (j = 0; j < a.length; j++)
			if (a[j] != j + 1) break;
		
		return (j+1);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = {3, 1, 4, 0, 5, 1, 2, 17, 4};
		FirstMissingInteger t = new FirstMissingInteger();
		System.out.println(t.firstMissingPositive(a));
	}
}
