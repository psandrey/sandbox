package _problems._interviewbit.arrays;

/*
 * Given an array A of integers, find the maximum of j - i subjected to
 *  the constraint of A[i] <= A[j].
 *  
 * Note: If there is no solution possible, return -1.
 */

/* 
 * There are 5 versions:
 * - Thumb-up: T(n) = O(n) - find the maximum window based on leftMin-rightMax arrays
 * - T(n) = O(n.logn) - sorting the array
 * - T(n) = O(2^n) - recursive brute-force
 * - T(n) = O(n^2) - brute-force
 * - T(n) = O(n^2/2) = O(n^2) - dynamic programming version
 */

import java.util.Arrays;
import java.util.Comparator;

public class MaxDistance {

	// Thumb-up version: T(n) = O(n)
	public int maximumGap(final int[] a) {
		int d = -1;
		if (a == null || a.length == 0) return d;
		
		int n = a.length;
		int[] leftMin = new int[n];
		int[] rightMax = new int[n];
		
		// For each element i if there is a greater element on its right side, then we do not need
		//  to consider element i. So, basically we need to consider the rightmost element bigger
		//  than element i.
		int maxSoFar = a[n-1];
		rightMax[n-1] = maxSoFar;
		for (int i = n-2; i >= 0; i--) {
			maxSoFar = Math.max(maxSoFar, a[i]);
			rightMax[i] = maxSoFar;
		}

		// Similarly, for each element i if there is a smaller element in its left side, then we
		//  do not need to consider element i. So, basically we need to consider the leftmost
		//  element smaller than i.
		int minSoFar = a[0];
		leftMin[0] = minSoFar;
		for (int i = 1; i < n; i++) {
			minSoFar = Math.min(minSoFar, a[i]);
			leftMin[i] = minSoFar;	
		}
		
		// Now, after we have constructed these 2 arrays we need to find the biggest windows,
		//  such that leftMin[i] <= rightMax[j]. So, we do that like this:
		//    - open the window while leftMin[i] <= rightMin[j] and record the maximum (j-i)
		//    - otherwise close the window until the above statement is true.
		//    - repeat until one of the window's edges reach the end.
		//
		// Note: if the statement didn't happened at all, then there is no solution.
		int i = 0, j = 0;
		while (i < n && j < n) {
			if (leftMin[i] > rightMax[j]) { i++; continue; }
			
			d = Math.max(d, j-i);
			j++;
		}
		
		return d;
	}
	
	// Good version: T(n) = O(n.logn)
	public int maximumGapV2(final int[] a) {
		int d = -1;
		if (a == null || a.length == 0) return d;
		
		// 1. sort array, but also keep the original index
		class Item {
			int v;
			int i;
			public Item(int val, int idx) { v = val; i = idx; }
		}
		int n = a.length;
		Item[] items = new Item[n];
		for(int i = 0; i < n; i++) items[i] = new Item(a[i], i);
		Arrays.sort(items, new Comparator<Item>() {
			public int compare(Item i1, Item i2) {
				return i1.v - i2.v;
			}
		});
		
		// 2. since we have the array sorted, we are sure that all elements on the left of
		//   element i are smaller, so we are interested of the leftmost smaller element than
		//   element i, basically a left element with the smallest index. 
		int minIdxSoFar = Integer.MAX_VALUE;
		for(int i = 0; i < n; i++) {
			minIdxSoFar = Math.min(minIdxSoFar, items[i].i);
			d = Math.max(d, items[i].i - minIdxSoFar);
		}

		return (d==0 ? -1: d);
	}
	
	// Terrible brute force T(n) = O(2^n)
	public int maximumGapV3(final int[] a) {
		if (a == null || a.length == 0) return -1;
		
		int n = a.length;
		return helper(a, n, 0, n-1);
	}
	
	public int helper(final int[] a, int n, int i, int j) {
		if (i == j || i == n || j == 0) return -1;
		if (a[i] <= a[j]) return (j - i);

		return Math.max(helper(a, n, i+1, j), helper(a, n, i, j-1));
	}
	
	// brute force T(n) = O(n^2)
	public int maximumGapV4(final int[] a) {
		int d = -1;
		if (a == null || a.length == 0) return d;
	
		int n = a.length;
		int[][] dp = new int[n][n];
		for (int i = 0; i < n-1; i++) dp[i][i+1] = (a[i] <= a[i+1] ? 1 : -1);
		
		for (int i = 0; i<n; i++)
			for (int j=i+1; j<n; j++)
				if (a[i]<=a[j]) d = Math.max(d, j-i);
			
		return d;
	}

	// dynamic programming: T(n) = O(n^2/2) = O(n^2)
	public int maximumGapV5(final int[] a) {
		int d = -1;
		if (a == null || a.length == 0) return d;
	
		int n = a.length;
		int[][] dp = new int[n][n];
		for (int i = 0; i < n-1; i++) dp[i][i+1] = (a[i] <= a[i+1] ? 1 : -1);
		
		for (int len = 1; len<n; len++)
			for (int i=0; i<n-len; i++)
				dp[i][i+len] = (a[i] <= a[i+len] ? len : Math.max(dp[i][i+len-1], dp[i+1][i+len]));
			
		return dp[0][n-1];
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = new int[] {9, 2, 3, 4, 5, 6, 7, 8, 18, 0};
		//int[] a = new int[] {3, 5, 4, 1, 2, 0};
		MaxDistance t = new MaxDistance();
		int ans = t.maximumGap(a);
		System.out.println(ans);
		
		ans = t.maximumGapV2(a);
		System.out.println(ans);
	
		ans = t.maximumGapV3(a);
		System.out.println(ans);

		ans = t.maximumGapV4(a);
		System.out.println(ans);

		ans = t.maximumGapV5(a);
		System.out.println(ans);
	}
}
