package _problems._interviewbit.arrays;

/*
 * Given an index k, return the kth row of the Pascal�s triangle.
 */

import java.util.ArrayList;

public class PascalTriangleKthRow {

	public ArrayList<Integer> getRow(int row) {
		ArrayList<Integer> ans = new ArrayList<Integer>();
		if (row < 0) return ans;
		
		ans.add(1);
		long prev = 1;
		long n = (long)row;
		for (long k = 1; k < row+1; k++) {
			long kth = prev * (n + 1L - k) / k;
			prev = kth;
			ans.add((int)kth);
		}

		return ans;
	}
	
	private void print(ArrayList<Integer> l) {
		System.out.print("[");
		for (Integer x: l ) System.out.print(x + ", ");
		System.out.print("]");
		System.out.println();
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int n = 3;
		PascalTriangleKthRow t = new PascalTriangleKthRow();
		ArrayList<Integer> pascalRow = t.getRow(n);
		t.print(pascalRow);
	}
	
}
