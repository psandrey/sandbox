package _problems._interviewbit.arrays;

/*
 * Given a positive integer n and a string s consisting only of letters D or I,
 * you have to find any permutation of first n positive integer that satisfy the given input string.
 *
 * D means the next number is smaller, while I means the next number is greater.
 *
 * Notes:
 *  Length of given string s will always equal to n - 1
 *  Your solution should run in linear time and space.
 *  
 * Example :
 * Input 1: n = 3 , s = ID
 * Return: [1, 3, 2]
 *
 */

import java.util.ArrayList;

public class FindPermutation {

	public ArrayList<Integer> findPerm(final String s, int n) {
		if (n <= 2) return new ArrayList<Integer>();
		ArrayList<Integer> p = new ArrayList<Integer>(n);
		
		int d = 0, i = 0, k = 1;
		for (int j = 0; j < n - 1; j++) {
			if (s.charAt(j) == 'D') k++;
		}
		p.add(k);

		d = k; i = k;
		for (int j = 0; j < s.length(); j++)
			if (s.charAt(j) == 'D') p.add(--d);
			else p.add(++i);
		
		return p;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String s = "ID";
		int n = 3;
		
		FindPermutation t = new FindPermutation();
		ArrayList<Integer> p = t.findPerm(s, n);
		for (Integer x: p) System.out.print(x + " ");
		
	}
}
