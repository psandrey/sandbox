package _problems._interviewbit.arrays;

/*
 * Given an m x n matrix of 0s and 1s, if an element is 0, set its entire row and column to 0.
 * Note: Do it in place.
 *
 * Example:
 *  1 0 1
 *  1 1 1
 *  1 1 1
 *
 * On returning, the array A should be :
 *  0 0 0
 *  1 0 1
 *  1 0 1
 */

import java.util.ArrayList;
import java.util.Arrays;

public class SetMatrixZeros {
	
	public void setZeroes(ArrayList<ArrayList<Integer>> a) {
		if (a.isEmpty()) return;
		int n = a.size(), m = a.get(0).size();
		
		// zeroes on the first line?
		boolean l0_status = false;
		for (int j = 0; j < m; j++)
			if (a.get(0).get(j) == 0) { l0_status = true; break; }
		
		// zeroes on the first col?
		boolean c0_status = false;
		for (int i = 0; i < n; i++)
			if (a.get(i).get(0) == 0) { c0_status = true; break; }
	
		// set the corresponded line and col for each zeroed found
		for (int i = 1; i < n; i++)
			for (int j = 1; j < m; j++) {
				if (a.get(i).get(j) == 0) {
					a.get(0).set(j, 0);
					a.get(i).set(0, 0);
				}
			}
		
		// zeroed lines
		for (int i = 1; i < n; i++)
			if (a.get(i).get(0) == 0)
				for (int j = 1; j < m; j++) a.get(i).set(j, 0);
		
		// zeroed cols
		for (int j = 1; j < m; j++)
			if (a.get(0).get(j) == 0)
				for (int i = 1; i < n; i++) a.get(i).set(j, 0);

		// set the first line/col to zero?
		if (l0_status)
			for (int j = 0; j < m; j++) a.get(0).set(j, 0);
		if (c0_status)
			for (int i = 0; i < n; i++) a.get(i).set(0, 0);
	}
	
	
	public void print_a(ArrayList<ArrayList<Integer>> A){
		for (int i = 0; i < A.size(); i++) {
			for (int j = 0; j < A.get(i).size(); j++)
				System.out.print(A.get(i).get(j) + " ");
			System.out.println();
		}
		System.out.println();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
/*		
		Integer[][] a = {
				{ 0, 1 },
				{ 1, 1 } };
*/
		Integer[][] a = {
				{ 1, 0, 1 },
				{ 1, 1, 1 },
				{ 1, 0, 1 }};
		
		ArrayList<ArrayList<Integer>> A = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i < a.length; i++)
			A.add(new ArrayList<Integer>( Arrays.asList(a[i]) ));
		
		SetMatrixZeros t = new SetMatrixZeros();
		t.setZeroes(A);
		
		t.print_a(A);
	}
}
