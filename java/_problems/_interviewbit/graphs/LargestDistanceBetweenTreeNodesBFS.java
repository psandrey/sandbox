package _problems._interviewbit.graphs;

/*
 * Given an arbitrary unweighted rooted tree which consists of N (2 <= N <= 40000) nodes.
 * The goal of the problem is to find largest distance between two nodes in a tree.
 * Distance between two nodes is a number of edges on a path between the nodes
 * (there will be a unique path between any pair of nodes since it is a tree).
 * The nodes will be numbered 0 through N - 1.
 * 
 * The tree is given as an array P, there is an edge between nodes P[i] and i (0 <= i < N). 
 * Exactly one of the i�s will have P[i] equal to -1, it will be root node.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class LargestDistanceBetweenTreeNodesBFS {

	private void addEdge(HashMap<Integer, ArrayList<Integer>> hAdj, int u, int v) {
		ArrayList<Integer> adj;
		if (hAdj.containsKey(u))
			adj = hAdj.get(u);
		else {
			adj = new ArrayList<Integer>();
			hAdj.put(u, adj);
		}
		adj.add(v);
	}

	private HashMap<Integer, ArrayList<Integer>> arrayToAdjList(int[] parent) {
		HashMap<Integer, ArrayList<Integer>> hAdj = new HashMap<Integer, ArrayList<Integer>>();
		int n = parent.length;
		for (int i = 0; i < n ;i++) {
			int p = parent[i];
			if (p == -1)
				continue;
			addEdge(hAdj, i, p);
			addEdge(hAdj, p, i);
		}
		
		return hAdj;
	}
	
	public int solve(int[] parent) {
		if (parent == null || parent.length == 0)
			return 0;
		HashMap<Integer, ArrayList<Integer>> hAdj = arrayToAdjList(parent);	
		
		int[] dist = getFarthest(0, hAdj, parent.length);

		int nIdx = 0, maxD = 0;
		for (int i = 0; i < dist.length; i++)
			if (maxD < dist[i]) {maxD = dist[i]; nIdx = i; }
		
		dist = getFarthest(nIdx, hAdj, parent.length);
		maxD = 0;
		for (int i = 0; i < dist.length; i++)
			maxD = Math.max(maxD, dist[i]);

		return maxD;
	}
	
	private int[] getFarthest(int u, HashMap<Integer, ArrayList<Integer>> hAdj, int m) {
		int[] dist = new int[m];
		Arrays.fill(dist, -1);

		Queue<Integer> q = new LinkedList<Integer>();
		q.add(u);
		dist[u] = 0;

		while (!q.isEmpty()) {
			int node = q.poll();

			ArrayList<Integer> adj = hAdj.get(node);
			for (int c: adj) {
				if (dist[c] != -1)
					continue;

				dist[c] = dist[node] + 1;
				q.add(c);
			}			
		}

		return dist;	
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//int[] parent = new int[] {-1, 0, 0, 0, 3};
		//int[] parent = new int[]{-1, 0, 1, 1, 2, 0, 5, 0, 3, 0, 0, 2, 3, 1, 12, 14, 0, 5, 9, 6, 16, 0, 13, 4, 17, 2, 1, 22, 14, 20, 10, 17, 0, 32, 15, 34, 10, 19, 3, 22, 29, 2, 36, 16, 15, 37, 38, 27, 31, 12, 24, 29, 17, 29, 32, 45, 40, 15, 35, 13, 25, 57, 20, 4, 44, 41, 52, 9, 53, 57, 18, 5, 44, 29, 30, 9, 29, 30, 8, 57, 8, 59, 59, 64, 37, 6, 54, 32, 40, 26, 15, 87, 49, 90, 6, 81, 73, 10, 8, 16 };
		int[] parent = new int[]{-1};
		LargestDistanceBetweenTreeNodes t = new LargestDistanceBetweenTreeNodes();
		int len = t.solve(parent);
		System.out.println(len);
	}
}
