package _problems._interviewbit.graphs;

/*
 * Given two words (start and end), and a dictionary, find the length of shortest transformation
 * sequence from start to end, such that:
 *  - You must change exactly one character in every transformation
 *  - Each intermediate word must exist in the dictionary
 * Example:
 * start = "hit",  end = "cog"
 * dict = ["hot","dot","dog","lot","log"]
 * 
 * As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog", return its length 5
 */

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class WordLadder {

	private boolean adjacent(String s, String ss) {
		int n = 0;
		int i = -1;
		while (n < 2 && ++i < s.length()) {
			if (s.charAt(i) != ss.charAt(i))
				n++;
		}

		return (n <= 1);
	}

	private int wordLadder(String[] dict, String start, String end) {
		
		HashMap<String, Integer> dist = new HashMap<String, Integer>();
		for (int i = 0; i < dict.length; i++)
			dist.put(dict[i], Integer.MAX_VALUE);
		dist.put(start, 0);

		HashSet<String> marked = new HashSet<String>();
		Queue<String> q = new LinkedList<String>();
		q.add(start);
		marked.add(start);

		while (!q.isEmpty()) {
			String s = q.poll();
			
			if (s.compareTo(end) == 0)
				break;

			for (int i = 0; i < dict.length; i++) {
				String ss = dict[i];
				if (!marked.contains(ss) && adjacent(s, ss)) {
					if (dist.get(ss) > dist.get(s) + 1)
						dist.put(ss, dist.get(s) + 1);
					
					q.add(ss);
					marked.add(ss);
				}
			}
		}

		return dist.get(end) != Integer.MAX_VALUE ? dist.get(end) + 1 : 0;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		String[] dict = {"POON", "PLEE", "SAME", "POIE", "PLEA", "PLIE", "POIN"};
		String start = "TOON";
		String end = "PLEA";
		
		WordLadder t = new WordLadder();
		System.out.println(t.wordLadder(dict, start, end));
	}
	
}
