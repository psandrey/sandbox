package _problems._interviewbit.graphs;

/*
 * Given a 2D board containing 'X' and 'O', capture all regions surrounded by 'X'.
 * A region is captured by flipping all 'O's into 'X's in that surrounded region.
 */

import java.util.ArrayList;

public class CaptureRegionsOnBoard {
	
	private int[][] d = new int[][]{{-1,0} , {0,1}, {1,0}, {0,-1}};
	private char unconquerable = '#';
	
	public void solve(ArrayList<ArrayList<Character>> mat) {
		if (mat == null)
			return;

		int n = mat.size();
		if (n < 3)
			return;
		int m = mat.get(0).size();
		if (m < 3)
			return;
		
		// top border
		for (int i = 0; i < n; i++)
			if (mat.get(i).get(0) == 'O')
				helper(mat, i, 0);

		// bottom border
		for (int i = 0; i < n; i++)
			if (mat.get(i).get(m-1) == 'O')
				helper(mat, i, m-1);

		// left border
		for (int i = 0; i < m; i++)
			if (mat.get(0).get(i) == 'O')
				helper(mat, 0, i);

		// bottom border
		for (int i = 0; i < m; i++)
			if (mat.get(n-1).get(i) == 'O')
				helper(mat, n-1, i);

		for (int i = 0; i < n; i++) {
			ArrayList<Character> col = mat.get(i);
			for (int j = 0; j < m; j++)
				if (col.get(j) == unconquerable)
					col.set(j, 'O');
				else
					col.set(j, 'X');
		}
	}
	
	private void helper(ArrayList<ArrayList<Character>> mat, int i, int j) {
		int n = mat.size();
		int m = mat.get(0).size();
		
		if (i < 0 || j < 0 || i == n || j == m || mat.get(i).get(j) != 'O')
			return;
		
		ArrayList<Character> col = mat.get(i);
		col.set(j, unconquerable);
		
		for (int k = 0; k < d.length; k++)
			helper(mat, i + d[k][0], j + d[k][1]);
	}
	
	private ArrayList<ArrayList<Character>> toMat ( String[] str) {
		ArrayList<ArrayList<Character>> mat = new ArrayList<ArrayList<Character>>();
		
		for (int i = 0; i < str.length; i++) {
			String s = str[i];
			ArrayList<Character> col = new ArrayList<Character>();
			for (int j = 0; j < s.length() ; j++) {
				col.add(s.charAt(j));
			}
			mat.add(col);
		}
		
		return mat;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String[] str = new String[] { 
				"XOXXXXOOXX",
				"XOOOOXOOXX", 
				"OXXOOXXXOO", 
				"OXOXOOOXXO", 
				"OXOOXXOOXX", 
				"OXXXOXXOXO", 
				"OOXXXXOXOO" };

		CaptureRegionsOnBoard t = new CaptureRegionsOnBoard();
		ArrayList<ArrayList<Character>> mat = t.toMat(str);
		t.solve(mat);
		System.out.println(mat);
	}
}
