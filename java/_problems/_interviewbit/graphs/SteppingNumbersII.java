package _problems._interviewbit.graphs;

/*
 * Given N and M find all stepping numbers in range N to M.
 * 
 * Note: Version 2 -> BFS version.
 */

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Queue;

public class SteppingNumbersII {

	public int[] stepnum(int min, int max) {
		if (min > max)
			return null;
		
		LinkedList<Integer> steps = new LinkedList<Integer>();
		Queue<Integer> q = new LinkedList<Integer>();
		for (int i = 1; i <= 9; i++)
			q.add(i);
		
		if (min == 0)
			steps.add(0);
		
		while (!q.isEmpty()) {
			int num = q.poll();

			if (num < max) {
				int d = num % 10;
				if (d > 0) q.offer(num*10 + (d - 1));
				if (d < 9) q.offer(num*10 + (d + 1));
			}
			
			if (num >= min && num <= max)
				steps.add(num);
		}
		
		return LinkedListToArray(steps);
	}
	
	private int[] LinkedListToArray(LinkedList<Integer> steps) {
		int n = steps.size();
		if (n == 0)
			return null;

		int[] ints = new int[steps.size()];
		ListIterator<Integer> listIter = steps.listIterator();
		int i = 0;
		while (listIter.hasNext())
			ints[i++] = listIter.next();

		return ints;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int min = 0;
		int max = 1000;

		SteppingNumbersII t = new SteppingNumbersII();
		int[] result = t.stepnum(min, max);
		for (int i = 0; i < result.length; i++)
			System.out.print(result[i] + ", ");
	}
}
