package _problems._interviewbit.graphs;

/*
 * Get from corner (0,0) to corner (x,y) without touching any circle. 
 */

import java.util.LinkedList;
import java.util.Queue;

public class ValidPath {

	private boolean isInside(int[] x0, int[] y0, int r, int x, int y) {
		int n = x0.length;
		for (int i = 0; i < n; i++) {
			long dx = (x - x0[i]);
			long dy = (y - y0[i]);
			long sqr = dx*dx + dy*dy;
			if (sqr <= r*r)
				return true;
		}
		
		return false;
	}
	
	private int[][] d = new int[][] {
						{ 1, 0}, { 1, 1},
						{-1, 0}, {-1,-1},
						{ 0, 1}, { 1,-1},
						{ 0,-1}, {-1, 1}};

	private class Pos {
		int x;
		int y;
		public Pos(int x, int y) {
			this.x = x;
			this.y = y;
		}
	};

    public static final String YES = "YES";
    public static final String NO = "NO";
    
	public String solve(int x, int y, int n, int r, int[] x0, int[] y0) {
		boolean[][] marked = new boolean[x+1][y+1];

		Queue<Pos> q = new LinkedList<Pos>();
		q.add(new Pos(0, 0));
		marked[0][0] = true;
		
		while (!q.isEmpty()) {
			Pos p = q.poll();
			
			if (p.x == x && p.y == y)
				return "YES";
			
			for (int i = 0; i < d.length; i++) {
				int newX = p.x + d[i][0];
				int newY = p.y + d[i][1];
				if ((newX < 0 || newX > x) || (newY < 0 || newY > y))
					continue;
				if (marked[newX][newY] == true)
					continue;
				if (isInside(x0, y0, r, newX, newY))
					continue;
				
				q.add(new Pos(newX, newY));
				marked[newX][newY] = true;
			}
		}	
		
		return "NO";
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int x = 5;
		int y = 5;
		int N = 2;
		int R = 1;
		int[] A = new int[]{1, 3};
		int[] B = new int[]{3, 3};
		
		ValidPath t = new ValidPath();
		String r = t.solve(x, y, N, R, A, B);
		System.out.println(r);
	}
}
