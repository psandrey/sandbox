package _problems._interviewbit.graphs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;


import graphs.UndirectedGraph;
import graphs.Vertex;

public class FindAllPaths {

	private LinkedList<ArrayList<Vertex>> findAllPaths(Integer[][] g, Integer start, Integer end) {
		UndirectedGraph G = new UndirectedGraph(g);
		
		return DFS(G, G.getVertex(start), G.getVertex(end));
	}

	private LinkedList<ArrayList<Vertex>> DFS(UndirectedGraph G, Vertex start, Vertex end) {
		LinkedHashSet<Vertex> marked = new LinkedHashSet<Vertex>();
		LinkedList<ArrayList<Vertex>> result = new LinkedList<ArrayList<Vertex>>();
		LinkedList<Vertex> path = new LinkedList<Vertex>();
		path.add(start);

		DFSHelper(G, start, end, start, marked, path, result);
		return result;
	}

	private void DFSHelper(UndirectedGraph G, Vertex start, Vertex end, Vertex n, 
			HashSet<Vertex> marked, LinkedList<Vertex> path, LinkedList<ArrayList<Vertex>> result) {

		if (n == end) {
			result.add(new ArrayList<Vertex>(path));
			marked.remove(n);
			return;
		}

		marked.add(n);
		
		List<Vertex> list;
		if (!G.adjList.containsKey(n) || (list = G.adjList.get(n)) == null)
			return;
		
		for (Vertex adj : list) {
			if (marked.contains(adj))
				continue;

			path.add(adj);
			DFSHelper(G, start, end, adj, marked, path, result);
			path.remove(adj);
		}
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[][] g = new Integer[][]{
			{1, 2},
			{1, 5},
			{2, 3},
			{3, 4},
			{5, 4}};
		Integer start = 1;
		Integer end = 4;
		
		FindAllPaths t = new FindAllPaths();
		LinkedList<ArrayList<Vertex>> result = t.findAllPaths(g, start, end);
		for (ArrayList<Vertex> list : result) {
			System.out.println();
			System.out.print("[");
			for (int i = 0; i < list.size(); i++)
				System.out.print(" ," + list.get(i).id);
			System.out.print("]");
		}
	}
}
