package _problems._interviewbit.graphs;

/*
 * Clone an undirected graph. Each node in the graph contains a label and a list of its neighbors.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ClonedGraph {

	private class UndirectedGraphNode {
		int label;
		List<UndirectedGraphNode> neighbors;
		public UndirectedGraphNode (int label) {
			this.label = label;
			neighbors = new ArrayList<UndirectedGraphNode>();
		}
	};
	
	public UndirectedGraphNode cloneGraph(UndirectedGraphNode n) {
		UndirectedGraphNode root = new UndirectedGraphNode(n.label);
		HashMap<Integer, UndirectedGraphNode> hmClones = new HashMap<Integer, UndirectedGraphNode>();
		hmClones.put(n.label, root);
		
		Queue<UndirectedGraphNode> Q = new LinkedList<UndirectedGraphNode>();
		Q.add(n);
		UndirectedGraphNode newNode;
		while (!Q.isEmpty()) {
			n = Q.poll();
			newNode = hmClones.get(n.label);
			
			if (n.neighbors == null || n.neighbors.size() == 0)
				continue;
			
			for (UndirectedGraphNode neighbor : n.neighbors) {

				UndirectedGraphNode newNeighbor;
				if (hmClones.containsKey(neighbor.label))
					newNeighbor = hmClones.get(neighbor.label);
				else {
					newNeighbor = new UndirectedGraphNode(neighbor.label);
					hmClones.put(neighbor.label,newNeighbor);
					Q.add(neighbor);
				}
				
				newNode.neighbors.add(newNeighbor);
			}
		}
		
		return root;
	}

	public UndirectedGraphNode buildGraph() {
		UndirectedGraphNode n1 = new UndirectedGraphNode(703);
		UndirectedGraphNode n2 = new UndirectedGraphNode(43);
		UndirectedGraphNode n3 = new UndirectedGraphNode(279);
		
		n1.neighbors.add(n1);
		n1.neighbors.add(n2);
		n1.neighbors.add(n3);
		
		
		n2.neighbors.add(n3);
		n2.neighbors.add(n1);
		
		n3.neighbors.add(n2);
		n3.neighbors.add(n3);
		n3.neighbors.add(n3);
		return n1;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		ClonedGraph t = new ClonedGraph();
		UndirectedGraphNode root = t.buildGraph();
		UndirectedGraphNode cloneRoot = t.cloneGraph(root);
		System.out.println(cloneRoot.label);
	}
}
