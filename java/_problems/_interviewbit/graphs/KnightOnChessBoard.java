package _problems._interviewbit.graphs;

/*
 * Given any source point and destination point on a chess board,
 * we need to find whether Knight can move to the destination or not.
 */

import java.util.LinkedList;
import java.util.Queue;

public class KnightOnChessBoard {

	private class Pos {
		int x;
		int y;
		public Pos(int x, int y) {
			this.x = x;
			this.y = y;
		}
	};
	
	private int[][] d = new int[][] {
		{ 2, 1}, { 1, 2},
		{ 2,-1}, { 1,-2},
		{-2, 1}, {-1, 2},
		{-2,-1}, {-1,-2}};

	private int movesKnightToTarget(int[] kPos, int[] tPos, int n) {
		if (kPos == null || tPos == null || n < 3)
			return -1;

		boolean[][] marked = new boolean[n][n];
		int[][] dist = new int[n][n];
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++) {
				dist[i][j] = Integer.MAX_VALUE;
				marked[i][j] = false;
			}

		Queue<Pos> q = new LinkedList<Pos>();
		q.add(new Pos(kPos[0], kPos[1]));
		marked[kPos[0]][kPos[1]] = true;
		dist[kPos[0]][kPos[1]] = 0;
		
		while (!q.isEmpty()) {
			Pos k = q.poll();
			
			if (k.x == tPos[0] && k.y == tPos[1])
				return dist[tPos[0]][tPos[1]];
			
			for (int i = 0; i < d.length; i++) {
				if ((k.x + d[i][0] < 0 || k.x + d[i][0] >= n) ||
					(k.y + d[i][1] < 0 || k.y + d[i][1] >= n))
					continue;
				if (marked[k.x + d[i][0]][k.y + d[i][1]] == true)
					continue;
				
				Pos newK = new Pos(k.x + d[i][0], k.y + d[i][1]);
				if (dist[newK.x][newK.y] > dist[k.x][k.y] + 1)
					dist[newK.x][newK.y] = dist[k.x][k.y] + 1;
				q.add(newK);
				marked[newK.x][newK.y] = true;
			}
		}

		printTableDist(dist, n);
		System.out.println();
		printTableMarked(marked, n);
		return -1;
	}
		
	private void printTableDist(int[][] t, int n) {
		for (int i = 0; i < n; i++) {
			System.out.println();
			for (int j = 0; j < n; j++) {
				System.out.print(t[i][j] + "  ");
			}
		}
	}
	
	private void printTableMarked(boolean[][] t, int n) {
		for (int i = 0; i < n; i++) {
			System.out.println();
			for (int j = 0; j < n; j++) {
				int x = (t[i][j] == false) ? 0 : 1;
				System.out.print(x + "  ");
			}
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] kPos = new int[] {1, 3};
		int[] tPos = new int[] {5, 0};
		int n = 6;
		KnightOnChessBoard t = new KnightOnChessBoard();
		int min = t.movesKnightToTarget(kPos, tPos, n);
		System.out.println(min);
	}
}
