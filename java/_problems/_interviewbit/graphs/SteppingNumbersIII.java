package _problems._interviewbit.graphs;

/*
 * Given n, find count of n digit Stepping numbers.
 */

public class SteppingNumbersIII {

	private int countSteppingNumbers(int len) {
		int[][] dp = new int[len][10];
		
		for (int i = 0; i <= 9; i++)
			dp [0][i] = 1;
		
		for (int i = 1; i < len ; i++)
			for (int j = 0; j <= 9; j++)
				if (j == 0) dp[i][j] = dp[i-1][j+1];
				else if (j == 9) dp[i][j] = dp[i-1][j-1];
				else dp[i][j] = dp[i-1][j-1] + dp[i-1][j+1];
		
		int sum = 0;
		for (int i = 1; i <= 9; i++)
			sum += dp[len-1][i];
		
		return sum;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		SteppingNumbersIII t = new SteppingNumbersIII();
		int result = t.countSteppingNumbers(4);
		System.out.println(result);
	}
}
