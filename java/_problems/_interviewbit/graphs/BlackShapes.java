package _problems._interviewbit.graphs;

/*
 * Given N * M field of O's and X's, where O=white, X=black
 * Return the number of black shapes. A black shape consists of
 * one or more adjacent X's (diagonals not included).
 */

public class BlackShapes {

	private int[][] d = new int[][]{{-1,0} , {0,1}, {1,0}, {0,-1}};
		
	public int black(String[] str) {
		if (str == null || str.length == 0)
			return 0;

		int n = str.length;
		int m = str[0].length();
		
		boolean[][] marked = new boolean[n][m];
		int count = 0;
		for (int i = 0; i < n; i ++)
			for (int j = 0; j < m; j++) {
				if (marked[i][j] == false && str[i].charAt(j) == 'X') {
					count++;
					helper(str, marked, i, j);
				}
				
			}

		return count;
	}
	
	private void helper(String[] str, boolean[][] marked, int i, int j) {
		int n = str.length;
		int m = str[0].length();
		
		marked[i][j] = true;

		for (int k = 0; k < d.length; k++) {
			int ii = i+d[k][0];
			int jj = j+d[k][1];
			if (ii < 0 || jj < 0 || ii == n || jj == m ||
					marked[ii][jj] == true || str[ii].charAt(jj) != 'X')
				continue;
			
			helper(str, marked, ii, jj);
		}
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String[] str = new String[] { 
				"OOOXOOO",
				"OOXXOXO",
				"OXOOOXO" };
		BlackShapes t = new BlackShapes();
		int count = t.black(str);
		System.out.println(count);
	}
}
