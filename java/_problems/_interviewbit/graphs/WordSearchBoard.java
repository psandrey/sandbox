package _problems._interviewbit.graphs;

/*
 * Given a 2D grid of characters and a word, find all occurrences of given word in grid.
 * A word can be matched in all 8 directions at any point. Word is said be found in a direction
 *  if all characters match in this direction (not in zig-zag form).
 * The 8 directions are, Horizontally Left, Horizontally Right, Vertically Up, Vertically Down and
 *  4 Diagonal directions.
 */

public class WordSearchBoard {
	
	public void patternSearch(char[][] grid, String s) {
		int n = grid.length;
		int m = grid[0].length;	
		for (int row = 0; row < n; row++)
			for (int col = 0; col < m; col++)
				if (search2D(row, col, grid, s))
					System.out.println("Found@ " + row + " , " + col);
	}
	
	private int[][] d = new int[][] {
		{ 1, 0}, { 1, 1},
		{-1, 0}, {-1,-1},
		{ 0, 1}, { 1,-1},
		{ 0,-1}, {-1, 1}};

	private boolean search2D(int row, int col, char[][] grid, String word) {
		if (word.charAt(0) != grid[row][col])
			return false;

		int n = grid.length;
		int m = grid[0].length;
		int len = word.length();
		for (int k = 0; k < d.length; k++) {
			int lenX = row + len*d[k][0];
			int lenY = col + len*d[k][1];
			if (lenX < 0 || lenX > n || lenY < 0 || lenY > m)
				continue;

			boolean found = true;
			for (int j = 1; j < len ; j++)
				if (grid[row + j*d[k][0]][col + j*d[k][1]] != word.charAt(j)) {
					found = false;
					break;
				}
			if (found == false)
				continue;

			return true;			 
		}

		return false;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		char[][] grid = {
				{'a','a','b','c'},
				{'c','b','a','a'},
				{'b','a','c','a'},
				{'a','c','b','a'}};
		String s = "abc";
		WordSearchBoard t = new WordSearchBoard();
		t.patternSearch(grid, s);
		
	}
}
