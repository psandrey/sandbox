package _problems._interviewbit.graphs;

/*
 * Given a 2D board and a word, find if the word exists in the grid. 
 * The word can be constructed from letters of sequentially adjacent cell,
 *  where "adjacent" cells are those horizontally or vertically neighboring. The cell itself does not count as an adjacent cell. 
 * 
 * Note: The same letter cell may be used more than once.
 */

public class WordSearchBoardII {
	
	private int[][] d = new int[][]{{-1,0} , {0,1}, {1,0}, {0,-1}};
	
	public int exist(String[] grid, String word) {
		if (grid == null || grid.length == 0 || word == null)
			return 0;

		int n = grid.length;
		int m = grid[0].length();
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++) {
				if (searchPattern(grid, i, j, word, 0) == 1)
					return 1;
			}

		return 0;
	}
	
	private int searchPattern(String[] grid, int row, int col, String word, int idx) {
		if (grid[row].charAt(col) != word.charAt(idx))
			return 0;
		
		if (idx == word.length() - 1)
			return 1;
		
		int n = grid.length;
		int m = grid[0].length();
		for (int i = 0; i < d.length; i++) {
			int newRow = row + d[i][0];
			int newCol = col + d[i][1];
			if (newRow < 0 || newRow >= n || newCol < 0 || newCol >= m)
				continue;
			
			if (searchPattern(grid, newRow, newCol, word, idx + 1) == 1)
				return 1;
		}

		return 0;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String[] grid = {
				"ABCE",
				"SFCS",
				"ADEE"};
		String word = "ABFSAX";
		
		WordSearchBoardII t = new WordSearchBoardII();

		int found = t.exist(grid, word);
		System.out.println(found);
	}
}
