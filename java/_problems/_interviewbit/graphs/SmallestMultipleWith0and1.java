package _problems._interviewbit.graphs;

/*
 * You are given an integer N. You have to find smallest multiple of N which consists of
 *  digits 0 and 1 only. Since this multiple could be large, return it in form of a string.
 * 
 * Note: Returned string should not contain leading zeroes.
 */

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class SmallestMultipleWith0and1 {

/*
	private int modString(String s, int x, HashMap<String, Integer> hmPRems) {
		int n = s.length();
		// s = (((0.10+An).10 + An-1).10 ... +A0)%x
		//   = (((0.10+An)%x .10 + An-1)%x .10 ... +A0)%x
		// because: (a + b)%x = (a%x + b)%x = (a + b%x)%x = (a%x + b%x)%x
		int r = 0;
		for (int i = 0; i < n; i++) {
			int d = s.charAt(i) - '0';
			r = (r*10 + d) % x;
		}

		return r;
	}
*/
	private final int Limit = 0x7ffff;
	
	private int nextState(int rem, int b, int mod) {
		return (rem * 10 + b) % mod;
	}

	private String buildAns(int[] binary, int[] prev, int startPos) {
		StringBuilder s = new StringBuilder();
		int cPos = startPos;
		int pPos = prev[cPos];
		
		while (cPos != pPos) {
			s.append(binary[cPos]);
			cPos = pPos;
			pPos = prev[cPos];
		}
		s.append(binary[cPos]);

		return s.reverse().toString();
	}
	
	public String multiple(int n) {
		if (n == 0)
			return "0";
		if (n == 1)
			return "1";
		
		int[] binary = new int[Limit];
		int[] prev = new int[Limit];
		int[] rems = new int[Limit];
		Arrays.fill(rems, -1);
		//HashMap<Integer, Integer> rems = new HashMap<Integer, Integer>();
		
		binary[0] = 1;
		prev[0] = 0;
		rems[1] = 0;
		
		Queue<Integer> q = new LinkedList<Integer>();
		q.add(1);
		int cPos = 0;
		while (!q.isEmpty() && cPos < Limit - 2) {
			int rem = q.poll();
			int pos = rems[rem];
			
			int nextRem = 0;
			nextRem = nextState(rem, 0, n);
			if (rems[nextRem] == -1) {
				cPos++;
				binary[cPos] = 0;
				prev[cPos] = pos;
				rems[nextRem] = cPos;
				//rems.put(nextRem, cPos);
				q.add(nextRem);	
			}
			if (nextRem == 0)
				break;
			
			nextRem = nextState(rem, 1, n);
			if (rems[nextRem] == -1) {
				cPos++;
				binary[cPos] = 1;
				prev[cPos] = pos;
				//rems.put(nextRem, cPos);
				rems[nextRem] = cPos;
				q.add(nextRem);
			}
			if (nextRem == 0)
				break;

		}
		
		return buildAns(binary, prev, cPos);
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		SmallestMultipleWith0and1 t = new SmallestMultipleWith0and1();
		String result = t.multiple(34535);
		System.out.println(result);
	}
}
