package _problems._interviewbit.graphs;

/*
 * Given 3 different prime numbers, return the first (smallest) k numbers that
 * factors only to these prime numbers.
 * 
 * [2, 3, 5,] k=5 => [2, 3, 4, 5, 6]
 */

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Comparator;

public class SmallestSequencePrimes {
	private class Prime {
		int val;
		int idx;
		public Prime(int v, int i) {
			this.val = v;
			this.idx = i;
		}
	};
	
	public ArrayList<Integer> solve(int a, int b, int c, int d) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		int[] p = new int[3];
		p[0] = a; p[1] = b; p[2] = c;		
		Comparator<Prime> comp = new Comparator<Prime>() {
			@Override
			public int compare(Prime p1, Prime p2) {
				return p1.val - p2.val;
			}
		};
		PriorityQueue<Prime> pq = new PriorityQueue<Prime>(comp);
		pq.add(new Prime(p[0], 0));
		pq.add(new Prime(p[1], 1));
		pq.add(new Prime(p[2], 2));

		while (d > 0) {
			Prime n = pq.poll();

			for (int i = n.idx; i < 3; i++)
				pq.add(new Prime((n.val * p[i]), i));

			if (result.isEmpty() || result.get(result.size()-1) != n.val) {
				result.add(n.val);
				d--;
			}
		}

		return result;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		SmallestSequencePrimes t = new SmallestSequencePrimes();
		//ArrayList<Integer> result = t.solve(19, 31, 31, 9);
		ArrayList<Integer> result = t.solve(2, 3, 5, 10);
		System.out.println(result);
	}
}
