package _problems._interviewbit.graphs;

/*
 * There are n islands and there are many bridges connecting them.
 * Each bridge has some cost attached to it.
 * We need to find bridges with minimal cost such that all islands are connected.
 */

import java.util.Comparator;
import java.util.PriorityQueue;

public class CommutableIslands {
	
	private int find(int[] parent, int v) {
		if (parent[v] != v)
			parent[v] = find(parent, parent[v]);

		return parent[v];
	}

	private void union(int[] parent, int[] rank, int u, int v) {
		int uId = find(parent, u);
		int vId = find(parent, v);
		
		if (rank[uId] > rank[vId])
			parent[vId] = uId;
		else {
			parent[uId] = vId;
			if (rank[uId] == rank[vId])
				rank[vId] = rank[uId] + 1;
		}
	}
	
	public int solve(int n, int[][] bridges) {
		if (n == 0 || bridges.length == 0)
			return 0;

		int[] parent = new int[n];
		int[] rank = new int[n];
		for (int i = 0; i < n; i++) { parent[i] = i; rank[i] = 0; }
		
		Comparator<Integer> comp = new Comparator<Integer>() {
			@Override
			public int compare(Integer idxBridge1, Integer idxBridge2) {
				return bridges[idxBridge1][2] - bridges[idxBridge2][2];
			}
		};
		PriorityQueue<Integer> pq = new PriorityQueue<Integer>(comp);
		// feed priority queue with all bridges
		for (int i = 0; i < bridges.length; i++)
			pq.add(i);
		
		int cost = 0;
		int connections = 0;
		while(connections < n - 1 || !pq.isEmpty()) {
			int idxBridge = pq.poll();
			int e0 = bridges[idxBridge][0] - 1;
			int e1 = bridges[idxBridge][1] - 1;

			if (find(parent, e0) != find(parent, e1)) {
				union(parent, rank, e0, e1);
				cost += bridges[idxBridge][2];
				connections++;
			}
		}

		return cost;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int n = 4;
		int[][] bridges = new int[][] { 
		{1, 2, 1},
		{2, 3, 4},
		{1, 4, 3},
		{4, 3, 2},
		{1, 3, 10}};

		CommutableIslands t = new CommutableIslands();
		int cost = t.solve(n, bridges);
		System.out.println(cost);
	}
}
