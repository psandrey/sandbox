package _problems._interviewbit.graphs;

/*
 * Given an arbitrary unweighted rooted tree which consists of N (2 <= N <= 40000) nodes.
 * The goal of the problem is to find largest distance between two nodes in a tree.
 * Distance between two nodes is a number of edges on a path between the nodes
 * (there will be a unique path between any pair of nodes since it is a tree).
 * The nodes will be numbered 0 through N - 1.
 * 
 * The tree is given as an array P, there is an edge between nodes P[i] and i (0 <= i < N). 
 * Exactly one of the i�s will have P[i] equal to -1, it will be root node.
 */

import java.util.ArrayList;
import java.util.HashMap;

public class LargestDistanceBetweenTreeNodes {
	
	private int root;
	private HashMap<Integer, ArrayList<Integer>> arrayToAdj(int[] parent) {
		HashMap<Integer, ArrayList<Integer>> hAdj = new HashMap<Integer, ArrayList<Integer>>();
		int n = parent.length;
		for (int i = 0; i < n ;i++) {
			int p = parent[i];
			if (p == -1) {
				root = i;
				continue;
			}
			ArrayList<Integer> adj;
			if (hAdj.containsKey(p))
				adj = hAdj.get(p);
			else {
				adj = new ArrayList<Integer>();
				hAdj.put(p, adj);
			}
			adj.add(i);
		}
		
		return hAdj;
	}
	
	private int maxD = 0;
	public int solve(int[] parent) {
		if (parent == null || parent.length <= 1)
			return 0;

		HashMap<Integer, ArrayList<Integer>> hAdj = arrayToAdj(parent);
		diameter(root, hAdj);
		return maxD;
	}
	
	private int diameter(Integer n, HashMap<Integer, ArrayList<Integer>> hAdj) {

		//leaf
		if (!hAdj.containsKey(n))
			return 0;

		// children
		ArrayList<Integer> adj = hAdj.get(n);
		int h0 = -1, h1 = -1;
		for(Integer c: adj) {
			int h = diameter(c, hAdj);
			if      (h > h0) { h1 = h0; h0 = h; }
			else if (h > h1) { h1 = h; }
		}

		int path = h0 + ((h1 == -1) ? 1 : h1 + 2);
		maxD = Math.max(maxD, path);
		return h0 + 1;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//int[] parent = new int[] {-1, 0, 0, 0, 3};
		int[] parent = new int[]{-1, 0, 1, 1, 2, 0, 5, 0, 3, 0, 0, 2, 3, 1, 12, 14, 0, 5, 9, 6, 16, 0, 13, 4, 17, 2, 1, 22, 14, 20, 10, 17, 0, 32, 15, 34, 10, 19, 3, 22, 29, 2, 36, 16, 15, 37, 38, 27, 31, 12, 24, 29, 17, 29, 32, 45, 40, 15, 35, 13, 25, 57, 20, 4, 44, 41, 52, 9, 53, 57, 18, 5, 44, 29, 30, 9, 29, 30, 8, 57, 8, 59, 59, 64, 37, 6, 54, 32, 40, 26, 15, 87, 49, 90, 6, 81, 73, 10, 8, 16 };
		LargestDistanceBetweenTreeNodes t = new LargestDistanceBetweenTreeNodes();
		int len = t.solve(parent);
		System.out.println(len);
	}
}
