package _problems._interviewbit.graphs;

/*
 * Given an arbitrary unweighted rooted tree which consists of N (2 <= N <= 40000) nodes.
 * The goal of the problem is to find largest distance between two nodes in a tree.
 * Distance between two nodes is a number of edges on a path between the nodes
 * (there will be a unique path between any pair of nodes since it is a tree).
 * The nodes will be numbered 0 through N - 1.
 * 
 * The tree is given as an array P, there is an edge between nodes P[i] and i (0 <= i < N). 
 * Exactly one of the i�s will have P[i] equal to -1, it will be root node.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LargestDistanceBetweenTreeNodesFastest {

	private class NodeProp {
		public int height;
		public int longest_path;
		public NodeProp(int h, int l) { height = h; longest_path = l; }
	}
	
	public int solve(ArrayList<Integer> A) {
		ArrayList<NodeProp> nodeProp = new ArrayList<>();
		for (int i = 0; i < A.size(); ++i) nodeProp.add(new NodeProp(0, 0));
		
		Collections.sort(A);
		
		int ans = 0;
		for (int i = A.size() - 1; i > 0; --i) {
			int u = i;			// current node
			int p = A.get(u);	// its parent
			
 			/* - update the longest path for the subtree rooted in p:
 			 *      1. the longest might the previous path or
 			 *      2. the longest might be the previous height + the height of the child + 1 
 			 */
			nodeProp.get(p).longest_path = Math.max(
									nodeProp.get(p).longest_path,
									nodeProp.get(p).height + nodeProp.get(u).height + 1);
			/* - update parent node height */
			nodeProp.get(p).height = Math.max(
									nodeProp.get(p).height,
									nodeProp.get(u).height + 1);
			
			/* - update ans */
			ans = Math.max(ans, nodeProp.get(p).longest_path);
		}
		
		return ans;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//int[] parent = new int[] {-1, 0, 0, 0, 3};
		//Integer[] parent = new Integer[]{-1, 0, 1, 1, 2, 0, 5, 0, 3, 0, 0, 2, 3, 1, 12, 14, 0, 5, 9, 6, 16, 0, 13, 4, 17, 2, 1, 22, 14, 20, 10, 17, 0, 32, 15, 34, 10, 19, 3, 22, 29, 2, 36, 16, 15, 37, 38, 27, 31, 12, 24, 29, 17, 29, 32, 45, 40, 15, 35, 13, 25, 57, 20, 4, 44, 41, 52, 9, 53, 57, 18, 5, 44, 29, 30, 9, 29, 30, 8, 57, 8, 59, 59, 64, 37, 6, 54, 32, 40, 26, 15, 87, 49, 90, 6, 81, 73, 10, 8, 16 };
		LargestDistanceBetweenTreeNodesFastest t = new LargestDistanceBetweenTreeNodesFastest();
		
		Integer[] parent = new Integer[]{1, 6, 6, -1, 3, 3, 3};
		
		ArrayList<Integer> A = new ArrayList<Integer>(Arrays.asList(parent));
		int longest = t.solve(A);
		System.out.println(longest);
	}
}
