package _problems._interviewbit.graphs;

/*
 * How many minimum numbers from fibonacci series are required such that
 *  sum of numbers should be equal to a given Number N.
 * 
 * Note : repetition of number is allowed.
 */

import java.util.ArrayList;

public class SumOfFibonacciNumbers {

	private ArrayList<Integer> fibonnaci(int n) {
		ArrayList<Integer> f = new ArrayList<Integer>();
		f.add(1);
		f.add(1);
		int newF = 1;
		while (newF < n) {
			newF = f.get(f.size() - 1) + f.get(f.size()-2);
			if (newF > n)
				break;
			f.add(newF);
		}

		return f;
	}

	// greedy version
	int fibsum(int k) {
		if (k < 1)
			return 0;
		System.out.println("Compute for k:" + k);
		ArrayList<Integer> f = fibonnaci(k);
		int count = 0;
		int j = f.size() - 1;
		while (k > 0) {
			count += (k / f.get(j)); 
			k %= f.get(j); 
			System.out.println("count=" + count + ", f=" + f.get(j) + ", rem=" + k);
			j--;
		}
		
		return count;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		SumOfFibonacciNumbers t = new SumOfFibonacciNumbers();
		int min = t.fibsum(4);
		System.out.println(min);
	}
}
