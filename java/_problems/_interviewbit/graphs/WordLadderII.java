package _problems._interviewbit.graphs;

/*
 * Given two words (start and end), and a dictionary, find the shortest transformation sequence
 * from start to end, such that:
 *   - Only one letter can be changed at a time
 *   - Each intermediate word must exist in the dictionary
 *   
 * If there are multiple such sequence of shortest length, return all of them.
 * Refer to the example for more details.
 * 
 * Given:
 *   start = "hit"
 *   end = "cog"
 *   dict = ["hot","dot","dog","lot","log"]
 *   
 * Return
 *
 *  [
 *    ["hit","hot","dot","dog","cog"],
 *    ["hit","hot","lot","log","cog"]
 *  ]
 * Note:
 *  All words have the same length.
 *  All words contain only lowercase alphabetic characters.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Queue;

public class WordLadderII {
	
	private boolean adjacent(String s, String ss) {
		int n = 0;
		int i = -1;
		while (n < 2 && ++i < s.length()) {
			if (s.charAt(i) != ss.charAt(i))
				n++;
		}

		return (n <= 1);
	}
	
	private void predToArrayList(ArrayList<ArrayList<String>> result, LinkedList<String> list,
			String start, String end, String cNode, HashMap<String, LinkedList<String>> pred) {

		if (cNode.compareTo(start) == 0) {
			ArrayList<String> l = new ArrayList<String>(list);
			l.add(end);
			result.add(l);
			return;
		}
		
		LinkedList<String> l = pred.get(cNode);
		if (l == null)
			return;

		for (String n: l) {
			list.add(0, n);
			predToArrayList(result, list, start, end, n, pred);
			list.remove(0);
		}
	}

	public ArrayList<ArrayList<String>> findLadders(
			String start, String end, ArrayList<String> dict) {

		// remove duplicates and
		// make sure the dictionary does not contain start and contains end
		LinkedHashSet<String> hashSet = new LinkedHashSet<>(dict);
		if (!hashSet.contains(end))
			hashSet.add(end);
		hashSet.remove(start);
		dict = new ArrayList<>(hashSet);
	
		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();

		// initialize predecessors and distances
		HashMap<String, LinkedList<String>> pred = new HashMap<String, LinkedList<String>>();
		HashMap<String, Integer> dist = new HashMap<String, Integer>();
		for (int i = 0; i < dict.size(); i++)
			dist.put(dict.get(i), Integer.MAX_VALUE);
		dist.put(start, 0);
		
		// start...
		HashSet<String> marked = new HashSet<String>();
		Queue<String> q = new LinkedList<String>();
		q.add(start);
		marked.add(start);

		while (!q.isEmpty()) {
			String s = q.poll();
			
			if (s.compareTo(end) == 0)
				continue;

			for (int i = 0; i < dict.size(); i++) {
				String ss = dict.get(i);
				if (s.compareTo(ss) == 0)
					continue;
				
				if (adjacent(s, ss)) {
					if (!marked.contains(ss)) {

						// relax for all unvisited adjacent nodes
						if (dist.get(ss) > dist.get(s) + 1) {
							dist.put(ss, dist.get(s) + 1);
							
							LinkedList<String> l = new LinkedList<String>();
							l.add(s);
							pred.put(ss, l);
						}
						
						q.add(ss);
						marked.add(ss);
					} else if (!pred.get(ss).contains(s) && dist.get(ss) == dist.get(s) + 1)
						// for the same distance adds its predecessor
						pred.get(ss).add(s);
				}
			}
		}

		predToArrayList(result, new LinkedList<String>(), start, end, end, pred);

		return result;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		//String[] dict = {"hot","dot","dog","lot","log"};
		//String start = "hit";
		//String end   = "cog";
		
		//String[] dict = {
		//		"baba", "abba", "aaba", "bbbb", "abaa", "abab",
		//		"aaab", "abba", "abba", "abba", "bbba", "aaab",
		//		"abaa", "baba", "baaa", "bbaa", "babb"};
		//String start = "bbaa";
		//String end   = "babb";

		String[] dict = {
				"bb", "aa", "bb", "ba", "ab", "aa", "ab", "bb", "aa", "aa",
				"ab", "bb", "ba", "aa", "ba", "ab", "ba", "ab", "ab", "aa",
				"bb", "ab", "aa", "aa", "ba", "bb", "ab", "ba", "ab", "aa",
				"aa", "aa", "ba", "ab", "ab", "aa", "aa", "ab", "ba", "ba", "bb"};
		String start = "ba";
		String end   = "bb";
		
		//String[] dict = {
		//	"aaababaaabbaabbbaaaaabbaaabaababbbbabbaaaaabbb", "babbbaabbbbbabaaaabbaabbbaabbabbbaababbaabbbba",
		//	"aaaaaaaabbbaababbbbbbbbababbbbbaaabaaababbaaba", "abaabbaaabbbbbaabaaababababbabbabbbbbbabaababb",
		//	"babbbabaaaababbbabbbabaabbbaababaabaabaabbabbb", "baabbaaabaabbaabbbbbbbaabbabbaaabaaabbabbbbbaa",
		//	"baaabbabbbbaaabaabbbbabbbaabbaaabaabbabbaabaab", "bbabbaaaaaabaabbabbbabaaaabbbbaabababaabbaabba",
		//	"bbabbabbbbbbbabbaabaaabaaabbaaaaaaaaabbabbaaab", "baaabbaababbabaaaaaabaaabaaaabaabbbaaabbbbabba",
		//	"babbbaaaabbaabaaaaabaaabbabaabaabaaaabababaaaa", "aabbabaabaaababaabbbbbbaaabaaaaabbaaaaabaaaaba",
		//	"abbbabbabaabbabbbabaababaaaabaaabaaaaaabbbaaaa", "aaaababaaaababaaabbabaabaabaabaababbbabbbaaaab",
		//	"abbbabaaabaababbbbbaaaaabbbbbbaabbababbbabbbaa", "aabbbabbbabaaaaabaaaabaaaabaabbbbabbbaaaaaaaaa",
		//	"abbabbaababbbaaaaabaaabbbbaaaabbbbbaabbaaababa", "baaaaaabaaaaabaabaaabababaaabaaaaaaababbabbbbb",
		//	"bbaabaabaababababbbbbbabbabbbbbbbbabbabbbbbbbb", "abbaaaaaabbabaabbababaaaabbbbaaaaabaabbaaaabaa",
		//	"aaaababbbbabaaaababaaaabbabbbbbabababaaabbaabb", "abbbbbbbabaababaababbababaaabaaabaababbaaaabaa",
		//	"babbbaaabbbbbabaaaaaaabaaaabaaabbabaabbababbbb", "abbabbaaabaabaaaaaabbabbaabaaaaabababbaabbaaaa",
		//	"abbaaabbbbbabbbbabaaaaaabaaabaabaaabbabbbababb", "bababbbbaaabaabbbbbbabbaabbbbbaaabaaabbbbbbbba",
		//	"aabaaaabbabbaabaaababbbaabbaabababaaaababbbbbb", "baababbbbbbbbbbaabaabaabbbbbbababbabaaababbaba",
		//	"babbabbabaaaababaabababbabaaaaaabbbaaaabaaabba", "aabbbaabaabaaabbbbbbabbbbaaabababbbbbbaababaaa",
		//	"abaabaaababbbabbbbaaaabaaababaabbababbbaabbbba"};
		//String start = "aaababaaabbaabbbaaaaabbaaabaababbbbabbaaaaabbb";
		//String end = "abaabaaababbbabbbbaaaabaaababaabbababbbaabbbba";
		ArrayList<String> dictA = new ArrayList<String>();
		dictA.addAll(Arrays.asList(dict));
		
		WordLadderII t = new WordLadderII();

		ArrayList<ArrayList<String>> r = t.findLadders(start, end, dictA);
		for (ArrayList<String> l : r) {
			System.out.println();
			System.out.print("[");
			for (String s: l)
				System.out.print(" ," + s);
			System.out.print("]");
		}
	}
}
