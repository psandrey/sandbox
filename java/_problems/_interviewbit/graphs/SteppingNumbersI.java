package _problems._interviewbit.graphs;

/*
 * Given N and M find all stepping numbers in range N to M.
 * 
 * Note: Version 1 -> incremental version.
 */

import java.util.LinkedList;

public class SteppingNumbersI {
	
	public LinkedList<Integer> stepping(int min, int max) {
		LinkedList<Integer> steps = new LinkedList<Integer>();
		
		int step = initStep(min);
		if (step > max)
			return steps;
		
		steps.add(step);
		while ((step = nextStep(step)) < max)
			steps.add(step);

		return steps;
	}
	
	private int initStep(int min) {
		int len = getLen(min);
		int msd = getMSD(min, len);
		int f = getFirst(msd, len);
		while (f < min)
			f = nextStep(f);
		
		return f;
	}

	private int getLen(int n) {
		return (int) Math.floor(Math.log10(n)+1);
	}
	
	private int getMSD(int n, int len) {
		if (len < 1)
			return -1;
		return (int) (n/Math.pow(10, --len));
	}
	
	private int getFirst(int msd, int len) {
		int n = msd;
		int d = n;
		while (--len > 0) {
			d += (d > 0) ? -1 : 1;
			n *=10;
			n += d;
		}
		
		return n;
	}
	
	private int nextStep(int n) {
		int len = getLen(n);
		if (len == 1) {
			if (n == 9) return 10;
			else return n+1;
		}
		
		int d  = n%10; n /= 10;
		int nd = n%10; n /= 10;
		int pos = 0;
		while ((nd < d || nd == 9) && n > 0) {
			d = nd;
			nd = n % 10;
			n /= 10;
			pos++;
		}
		
		// case: 210 => 212
		if (nd > d && nd < 9) {
			int trail = getFirst(nd + 1, pos + 1);
			n *= 10; n += nd;
			n *= Math.pow(10, pos + 1);
			n += trail;
			return n;
		}

		// case: 123 => 210
		if (nd < d && nd < 8)
			return getFirst(nd + 1, len);
		
		// case: 8989 => 9876
		if (nd == 8 && d == 9) {
			return getFirst(9, pos + 2);
		}

		// we need next number with len+1
		if (pos + 1 == len - 1) {
			if (nd > d)
				return getFirst(1, len + 1);
		}
		
		return -1;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int min = 1000;
		int max = 9999;

		SteppingNumbersI t = new SteppingNumbersI();
		LinkedList<Integer> result = t.stepping(min, max);
		int i = 0;
		for (int step: result) {
			i++;
			System.out.print(step + " ,");
		}
		System.out.println();
		System.out.println("count:" + i);
	}
}
