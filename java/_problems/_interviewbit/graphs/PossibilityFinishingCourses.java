package _problems._interviewbit.graphs;

/*
 * Given the total number of courses and a list of prerequisite pairs, is it possible
 * for you to finish all courses. return 1/0 if it is possible/not possible.
 * The list of prerequisite pair are given in two integer arrays B and C where B[i] is
 * a prerequisite for C[i].
 */

import java.util.LinkedList;
import java.util.Queue;

public class PossibilityFinishingCourses {

	public int solve(int n, int[] b, int[] c) {
		int m = n + 1;
		int[] in = new int[m];
		
		// count for each node the incoming edges
		for (int i = 0; i < b.length; i++) 
			in[c[i]]++;
		
		Queue<Integer> q = new LinkedList<Integer>();
		for (int i = 1; i < m; i++)
			if (in[i] == 0)
				q.add(i);

		if (q.size() == 0)
			return 0;
		
		int edges = b.length;
		while (!q.isEmpty()) {
			int from = q.poll();
			
			for (int i = 0; i <  b.length; i++)
				if (from == b[i]) {
					in[c[i]]--; edges--;
					if (in[c[i]] == 0)
						q.add(c[i]);
				}
		}
		
		// if there are still edges, then it is not a DAG
		return (edges == 0) ? 1 : 0;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//int n = 5;
		//int[] b = new int [] { 1, 3, 4, 5 };
		//int[] c = new int [] { 2, 1, 5, 3 };
		
		int n = 70;
		int[] b = new int[] { 67, 8, 48, 42, 35, 25, 37, 69, 31, 36, 7, 33, 2, 47, 42, 52, 31, 70, 29, 38, 36, 60, 15, 37, 33, 27, 4, 32, 43, 55, 49, 35, 21, 28, 62, 17, 2, 61, 54, 22, 9, 56, 12, 3, 60, 52, 21, 15, 54, 63, 33, 64, 38, 16, 59, 69, 49, 52, 10, 10, 6, 56, 43, 32, 41, 66, 6 };
		int[] c = new int[] { 51, 43, 55, 27, 34, 8, 14, 5, 70, 64, 65, 57, 45, 19, 53, 50, 44, 51, 19, 41, 14, 68, 12, 58, 50, 66, 7, 47, 40, 62, 29, 5, 22, 39, 23, 34, 25, 4, 40, 26, 26, 45, 18, 28, 61, 59, 17, 46, 39, 46, 68, 24, 63, 59, 67, 53, 9, 11, 3, 44, 24, 37, 13, 1, 65, 18, 48};

		//int n = 3;
		//int[] b = new int [] { 1, 2, 1 };
		//int[] c = new int [] { 2, 3, 3 };

		PossibilityFinishingCourses t = new PossibilityFinishingCourses();
		int r = t.solve(n, b, c);
		System.out.println(r);
	}
}
