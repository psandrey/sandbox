package _problems._interviewbit.graphs;

/*
 * Given a singly linked list where elements are sorted in ascending order,
 *  convert it to a height balanced BST.
 */

public class ConvertSortedListToBinarySearchTree {

	private class ListNode {
		public ListNode next;
		public int val;

		public ListNode(int data) {
			this.val = data;
			this.next = null;
		}	
	};

	private class TreeNode {
		public TreeNode left;
		public TreeNode right;
		public int val;

		public TreeNode(int data) {
			this.val = data;
			this.left = null;
			this.right = null;
		}
	};
	
	private TreeNode sortedListToBST(ListNode L) {
		if (L == null)
			return null;
		if (L.next == null)
			return new TreeNode(L.val);
		
		return helper(L, null);
	}
	
	private ListNode getParentMiddle(ListNode s, ListNode e) {
		if (s == e || s.next == e)
			return s;
		
		ListNode slow = s;
		ListNode fast = s.next.next;
		while (fast != e && fast.next != e) {
			slow = slow.next;
			fast = fast.next.next;
		}

		return slow;
	}
	
	private TreeNode helper(ListNode s, ListNode e) {
		if (s == null)
			return null;
		if (s == e || s.next == null)
			return new TreeNode(s.val);
		
		ListNode pM = getParentMiddle(s, e);
		TreeNode n = new TreeNode(pM.next.val);
		n.left = helper(s, pM);
		n.right = (pM.next == e) ? null : helper(pM.next.next, e);
		return n;
	}

	private void inOrderPrint(TreeNode n, int level) {
		if (n == null)
			return;

		inOrderPrint(n.left, level+1);
		for(int i=0; i<=level;i++)
			System.out.print("-");
		System.out.print(n.val);
		System.out.println();
		inOrderPrint(n.right, level+1);
	}

	private ListNode buildLinkedList(int n) {
		ListNode L = new ListNode(0);
		ListNode prev = L;
		for (int i = 1; i < n; i ++) {
			ListNode node = new ListNode(i);
			prev.next = node;
			prev = node;
		}
		
		return L;
	}
	
	private void printLinkedList(ListNode L) {
		ListNode n = L;

		System.out.println();
		System.out.print("[");
		while (n != null) {
			System.out.print(", " +n.val);
			n = n.next;
		}
		System.out.print("]");
		System.out.println();


	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {

		ConvertSortedListToBinarySearchTree t = new ConvertSortedListToBinarySearchTree();
		int n = 5;
		ListNode L = t.buildLinkedList(n);
		t.printLinkedList(L);
		
		TreeNode root = t.sortedListToBST(L);
		t.inOrderPrint(root, 0);
	}
}
