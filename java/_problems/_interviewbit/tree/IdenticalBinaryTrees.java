package _problems._interviewbit.tree;

/*
 * Given two binary trees, write a function to check if they are equal or not.
 * Two binary trees are considered equal if they are structurally identical and
 * the nodes have the same value.
 * 
 * Return 0 / 1 ( 0 for false, 1 for true ) for this problem
 */

import java.util.LinkedList;
import java.util.Queue;

public class IdenticalBinaryTrees {

	private class TreeNode {
		int val;
		TreeNode left, right;
		public TreeNode(int val) {
			this.val = val;
			this.left = null;
			this.right = null;
		}
	};
	
	public int isSameTree(TreeNode u, TreeNode v) {
		if (u == null && v == null) return 1;
		else if (u == null || v == null) return 0;
		else if (u.val != v.val) return 0;
		
		return ((isSameTree(u.left, v.left) == 1 && isSameTree(u.right, v.right) == 1) ? 1 : 0);
	}
	
	private TreeNode buildTreeSerializationTopDown() {
		int[] a = new int[]
				{47, 42, 52, 41, 44, 50, 64, 40, -1, 43, 45, 49, 51, 63, 77, -1, -1, -1, -1, -1,
				 46, 48, -1, -1, -1, 55, -1, 75, 88, -1, -1, -1, -1, 53, 58, 69, 76, 81, 94, -1,
				 54, 56, 60, 68, 73, -1, -1, 79, 87, 92, 100, -1, -1, -1, 57, 59, 61, 66, -1, 72,
				 74, 78, 80, 85, -1, 89, 93, 96, 102, -1, -1, -1, -1, -1, 62, 65, 67, 71, -1, -1,
				 -1, -1, -1, -1, -1, 84, 86, -1, 90, -1, -1, 95, 99, 101, -1, -1, -1, -1, -1, -1,
				 -1, 70, -1, 83, -1, -1, -1, -1, 91, -1, -1, 98, -1, -1, -1, -1, -1, 82, -1, -1, 
				 -1, 97, -1, -1, -1, -1, -1}; 

		TreeNode root = new TreeNode(a[0]);
		int n = a.length;
		
		Queue<TreeNode> q = new LinkedList<TreeNode>();
		q.add(root);
		
		int i = 1;
		int size;
		while (i < n) {

			size = q.size();
			for (int j = 0; j < size; j++) {
				TreeNode node = q.poll();
				if (a[i] != -1) { node.left = new TreeNode(a[i]); q.add(node.left);   }
				i++;
				if (a[i] != -1) { node.right = new TreeNode(a[i]); q.add(node.right); }
				i++;
			}
		}

		return root;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		IdenticalBinaryTrees t = new IdenticalBinaryTrees();
		TreeNode root1 = t.buildTreeSerializationTopDown();
		TreeNode root2 = t.buildTreeSerializationTopDown();
		System.out.println(t.isSameTree(root1, root2));
	}
}
