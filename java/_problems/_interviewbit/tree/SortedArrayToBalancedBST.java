package _problems._interviewbit.tree;

/*
 * Given an array where elements are sorted in ascending order,
 * convert it to a height balanced BST.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SortedArrayToBalancedBST {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	};

	// with array support
	public TreeNode sortedArrayToBST(int[] a) {
		return helper(a, 0, a.length-1);
	}
	
	private TreeNode helper(int[] a, int i, int j) {
		if (i > j) return null;
		else if (i == j) return new TreeNode(a[i]);
		
		int m = (i+j)/2;
		TreeNode n = new TreeNode(a[m]);
		n.left = helper(a, i, m-1);
		n.right = helper(a, m+1, j);
		return n;
	}
	
	// with list
	public TreeNode sortedArrayToBST(final List<Integer> a) {
		return helper(a, 0, a.size()-1);
	}
	
	private TreeNode helper(final List<Integer> a, int i, int j) {
		if (i > j) return null;
		else if (i == j) return new TreeNode(a.get(i));
		
		int m = (i+j)/2;
		TreeNode n = new TreeNode(a.get(m));
		n.left = helper(a, i, m-1);
		n.right = helper(a, m+1, j);
		return n;
	}
	
	// for testing...
	private void inOrder(TreeNode node) { 
		if (node == null) 
			return;
		
		inOrder(node.left); 
		System.out.print(node.val + " "); 
		inOrder(node.right); 
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) { 
		int[] a = new int[] {1, 2, 3, 4};
		ArrayList<Integer> A = new ArrayList<>(Arrays.asList(new Integer[] {1, 2, 3, 4}));

		SortedArrayToBalancedBST t = new SortedArrayToBalancedBST();
		TreeNode root1 = t.sortedArrayToBST(a);
		TreeNode root2 = t.sortedArrayToBST(A);
		
		System.out.println("From array:");
		t.inOrder(root1);System.out.println();
		System.out.println("From array-list:");
		t.inOrder(root2);
	}	
}
