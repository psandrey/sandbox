package _problems._interviewbit.tree;

/*
 * Given preorder and inorder traversal of a tree, construct the binary tree.
 */

import java.util.HashMap;

public class BinaryTreeFromInorderPreorder {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	};

	private int k = 0;
	public TreeNode buildTree(int[] preorder, int[] inorder ) {
		buildHmap(inorder);
		return helper(inorder, 0, inorder.length-1,preorder);
	}
	
	private TreeNode helper(int[] inorder, int i, int j, int[] preorder) {
		if (i > j) return null;
		int p = find(preorder[k]);

		// since pre-order has always the parent node the leftmost element,
		//  we increase k every time we create a node
		TreeNode n = new TreeNode(preorder[k++]);
		
		// so, after parent, we have the left subtree
		n.left = helper(inorder, i, p-1, preorder);

		// then the right subtree
		n.right = helper(inorder, p+1, j, preorder);
		return n;
	}

	HashMap<Integer, Integer> hMap = new HashMap<Integer, Integer>();
	private int find(int value) { 
		return hMap.get(value).intValue(); 
	}
	
	private void buildHmap(int in[]) {
		for(int i = 0; i < in.length; i++)
			hMap.put(in[i], i);
	}

	private void postOrder(TreeNode node) { 
		if (node == null) 
			return;

		postOrder(node.left); 
		postOrder(node.right);
		System.out.print(node.val + " ");
	} 
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) { 
		BinaryTreeFromInorderPreorder t = new BinaryTreeFromInorderPreorder(); 
		int in[]   = new int[] { 4, 8, 2, 5, 1, 6, 3, 7 };
		int pre[] = new int[] { 1, 2, 4, 8, 5, 3, 6, 7 };

		TreeNode root = t.buildTree(pre, in); 
		System.out.println("Postorder of the constructed tree : "); 
		t.postOrder(root);
		
		/* Should be:
		 * Postorder of the constructed tree : 
		 * 8 4 5 2 6 7 3 1 
		 */
	}
}
