package _problems._interviewbit.tree;

/*
 * Given a binary tree, find its maximum depth.
 * The maximum depth of a binary tree is the number of nodes along the longest path
 * from the root node down to the farthest leaf node.
 */

import java.util.Stack;

public class MaxDepthBinaryTree {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	}
	
	// The recursive version
	public int maxDepth(TreeNode n) {
		if (n == null) return 0;
		
		return Math.max(maxDepth(n.left), maxDepth(n.right)) + 1;
	}
	
	// The iterative version
	public int maxDepthIterative(TreeNode root) {
		if (root == null) return 0;
	
		// pre-order traversal
		Stack<TreeNode> st = new Stack<>();
		Stack<Integer> stdepth = new Stack<>();

		int max = 1;
		st.push(root); stdepth.push(1);
		while (!st.isEmpty()) {
			TreeNode n = st.pop();
			int d = stdepth.pop();
			max = Math.max(max, d);
			
			if (n.right != null) {
				st.push(n.right);
				stdepth.push(d+1);
			}

			if (n.left != null) {
				st.push(n.left);
				stdepth.push(d+1);
			}
		}
	
		return max;
	}
	
	private TreeNode createTree() {
		TreeNode root = new TreeNode(5);
		root.left = new TreeNode(4);
		root.right = new TreeNode(8);
		root.left.left = new TreeNode(11);
		root.left.left.left = new TreeNode(7);
		root.left.left.right = new TreeNode(2);
		
		root.right.left = new TreeNode(13);
		root.right.right = new TreeNode(4);
		root.right.right.left = new TreeNode(5);
		root.right.right.right = new TreeNode(1);
		return root;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		
		MaxDepthBinaryTree t = new MaxDepthBinaryTree();
		TreeNode root = t.createTree();

		System.out.println("Iterative: " + t.maxDepthIterative(root));
		System.out.println("Recursive: " + t.maxDepth(root));
	}
}
