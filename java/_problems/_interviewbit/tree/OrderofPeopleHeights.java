package _problems._interviewbit.tree;

/*
 * You are given the following :
 *  A positive number N
 *  Heights : A list of heights of N persons standing in a queue
 *  Infronts : A list of numbers corresponding to each person (P) that gives the number of persons who are taller
 *  than P and standing in front of P
 *  
 *  You need to return list of actual order of personsís height.
 *  Note: Consider that heights will be unique
 */

import java.util.Arrays;
import java.util.Comparator;

public class OrderofPeopleHeights {
	// T(n) = O(n.logn)
	private class Pair {
		public int h, f;
		public Pair(int hh, int ff) { h = hh; f = ff; }
	}
	
	Pair[] buildPairs(int[] h, int[] f) {
		int n = h.length;
		Pair[] pairs = new Pair[n];
		for (int i = 0; i < n; i++)	pairs[i] = new Pair(h[i], f[i]);

		return pairs;
	}
	
	private void sort(Pair[] p) {
		Comparator<Pair> comp = new Comparator<Pair>() {
			public int compare(Pair a, Pair b) {
				return (a.h - b.h);
			}
		};
		Arrays.sort(p, comp);
	}
	
	private class SegTree {
		int n;
		int m;
		int[] M;

		public SegTree(int initialArrayLength) {
			n = initialArrayLength;
			//m = 4*(n-1) + 1;
			// 2^len - 1, where len = ceil(log(n, 2)) 
			m = 2 * (int)Math.pow(2, Math.ceil(Math.log(n) / Math.log(2))) - 1;
			M = new int[m];
			build(0, 0, n-1);
		}
		
		public void build(int idx, int lo, int hi) {
			if (lo == hi) { M[idx] = 1; return; } // one cell holds one empty slot

			int mid = (lo+hi)/2,
				leftIdx = left(idx),
				rightIdx = right(idx);
			
			build(leftIdx , lo   , mid);
			build(rightIdx, mid+1, hi);

			M[idx] = M[leftIdx] + M[rightIdx]; // left's and right's empty slots
		}

		public int querryUpdate(int idx, int lo, int hi, int spaces) {
			if (lo == hi) { M[idx] = 0; return lo; }

			int mid = (lo+hi)/2, remSpaces = 0;
			if (spaces <= M[left(idx)])
				remSpaces = querryUpdate(left(idx), lo, mid, spaces);
			else
				remSpaces = querryUpdate(right(idx), mid+1, hi, spaces - M[left(idx)]);
			
			M[idx] = M[left(idx)] + M[right(idx)];
			return remSpaces;
		}
		
		public int left (int i) { return (2*i + 1); }
		public int right(int i) { return (2*i + 2); }
	};

	public int[] order(int[] h, int[] f) {
		Pair[] pairs = buildPairs(h, f);
		sort(pairs);
		
		int n = pairs.length;
		int[] ans = new int[n];
		SegTree st = new SegTree(n);
		
		for (int i = 0; i < n; i++) {
			int idx = st.querryUpdate(0, 0, n-1, pairs[i].f + 1);
			ans[idx] = pairs[i].h;
		}

		return ans;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] h = new int[]{5, 3, 2, 6, 1, 4};
		int[] f = new int[]{0, 1, 2, 0, 3, 2};
		

		//int[] h = new int[]{5, 7, 5, 6, 4, 7};
		//int[] f = new int[]{0, 0, 2, 1, 4, 0}; 
		
		OrderofPeopleHeights t = new OrderofPeopleHeights();
		int[] ans = t.order(h, f);
		
		for (int i = 0; i < ans.length; i++)
			System.out.print(ans[i] + ", ");
		System.out.println();
	}
}
