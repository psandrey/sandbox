package _problems._interviewbit.tree;

/*
 * Given a binary tree and a sum, determine if the tree has a root-to-leaf path
 * such that adding up all the values along the path equals the given sum.
 */

import java.util.Stack;

public class RootToLeafPathsWithSum {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	}

	// The recursive version
	public int hasPathSumRecursive(TreeNode root, int target) {
		if (root == null) return 0;
		
		return helper(root, 0, target);
	}

	private int helper(TreeNode n, int sum, int target) {	
		sum += n.val;
		
		// if we are at leaf level and we hit target, then we have a solution
		if (n.left == null && n.right == null && sum == target) {
			sum -= n.val;
			return 1;
		}
		
		// if we can, we go deeper
		int hasSum = 0;
		if (n.left != null) hasSum = helper(n.left, sum, target);
		if (hasSum == 0 && n.right != null) hasSum = helper(n.right, sum, target);
		
		sum -= n.val;
		return hasSum;
	}
	
	// The iterative version:
	// T(n) = O(n)
	// S(n) = O(n)
	public int hasPathSum(TreeNode root, int target) {
		if (root == null) return 0;

		// pre-order traversal
		Stack<TreeNode> st = new Stack<>();

		TreeNode n = root, prev = n;
		int sum = n.val;
		st.push(n);
		while (!st.isEmpty()) {
			n = st.peek();

			// if we are not backtrack neither from left nor from right, then
			//  go deeper on the left side if possible
			if (n.left != null && n.left != prev && n.right != prev) {
				prev = n; n = n.left;
				st.push(n);
				sum += n.val;
				continue;
			// else if we backtrack from left and we go right, then go deeper on
			//  the right side
			} else if (n.right != null && n.right != prev) {
				prev = n; n = n.right;
				st.push(n);
				sum += n.val;
				continue;
			}

			// we are at leaf level and we hit target, so record result
			if (n.left == null && n.right == null && sum == target)
				return 1;

			// try another path
			prev = n;
			sum -= st.pop().val;
		}

		return 0;
	}
	
	private TreeNode createTree() {
		TreeNode root = new TreeNode(5);
		root.left = new TreeNode(4);
		root.right = new TreeNode(8);
		root.left.left = new TreeNode(11);
		root.left.left.left = new TreeNode(7);
		root.left.left.right = new TreeNode(2);
		
		root.right.left = new TreeNode(13);
		root.right.right = new TreeNode(4);
		root.right.right.left = new TreeNode(5);
		root.right.right.right = new TreeNode(1);
		return root;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		
		RootToLeafPathsWithSum t = new RootToLeafPathsWithSum();
		TreeNode root = t.createTree();
		int target = 22;

		System.out.println("Iterative: " + t.hasPathSum(root, target));
		System.out.println("Recursive: " + t.hasPathSumRecursive(root, target));
	}
}
