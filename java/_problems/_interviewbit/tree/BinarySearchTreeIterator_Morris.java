package _problems._interviewbit.tree;

/*
 * Implement an iterator over a binary search tree (BST).
 * Your iterator will be initialized with the root node of a BST.
 * Calling next() will return the next smallest number in the BST.
 * 
 * Note: Morris's version of in-order traversal
 * 
 * Note: next() 
 *         T(n)=O(1) amortized time complexity
 *         S(n)=O(1) space complexity
 */

public class BinarySearchTreeIterator_Morris {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	};

	TreeNode p;
	public void Solution(TreeNode root) { p = root; }
	
	public boolean hasNext() { return (p != null); }

	public int next() {
		int ans = -1;
		while (hasNext()) {
			
			// if there is nothing on the left side of p, then p is next
			if (p.left == null) {
				ans = p.val;
				p = p.right;
				break;
			} else {
				TreeNode cursor = p.left;
				// we seek p's predecessor and make a link to it, or
				//  we have to return next and cut the link to p
				while (cursor.right != null && cursor.right != p) cursor = cursor.right;
				
				if (cursor.right == null) {
					// we found the predecessor, so make the link, and go deeper on left side
					//  and seek next
					cursor.right = p;
					p = p.left;
				} else {
					// cut the link and seek the successor on the right side of next
					ans = p.val;
					cursor.right = null;
					p = p.right;
					break;
				}
			}
		}
		
		return ans;
	}
	
	private TreeNode createTree() {
		TreeNode root = new TreeNode(10);
		root.left = new TreeNode(5);
		root.left.left = new TreeNode(3);
		root.left.right = new TreeNode(8);
		root.right = new TreeNode(20);
		
		return root;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		BinarySearchTreeIterator_Morris t = new BinarySearchTreeIterator_Morris();
		TreeNode root = t.createTree();
		t.Solution(root);
		
		while (t.hasNext()) System.out.println(t.next());
	}
}
