package _problems._interviewbit.tree;

/*
 * Given a binary tree, check whether it is a mirror of itself (i.e., symmetric around its center).
 */

import java.util.LinkedList;
import java.util.Queue;

public class SymmetricBinaryTree {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int v) { val = v; }
	};
	
	public int isSymmetric(TreeNode n) {
		if (n == null) return 1;
		
		return helper(n.left, n.right);
	}
	
	public int helper(TreeNode u, TreeNode v) {
		if (u == null && v == null) return 1;
		else if (u == null || v == null) return 0;
		else if (u.val != v.val) return 0;
		
		return ((helper(u.left, v.right) == 1 && helper(u.right, v.left) == 1) ? 1 : 0);
	}
	
	private TreeNode buildTreeSerializationTopDown() {
		int[] a = new int[]
				{0, 4, 4, 10, 6, 6, 10, 16, 3, 14, 9, 9, 14, 3, 16, 13, -1, 7, 11, -1, -1, -1, 15,
				15, -1, -1, -1, 11, 7, -1, 13, -1, 12, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1, 1,
				-1, 12, -1, 8, -1, 5, 2, 2, 5, -1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				-1}; 

		TreeNode root = new TreeNode(a[0]);
		int n = a.length;
		
		Queue<TreeNode> q = new LinkedList<TreeNode>();
		q.add(root);
		
		int i = 1;
		int size;
		while (i < n) {

			size = q.size();
			for (int j = 0; j < size; j++) {
				TreeNode node = q.poll();
				if (a[i] != -1) { node.left = new TreeNode(a[i]); q.add(node.left);   }
				i++;
				if (a[i] != -1) { node.right = new TreeNode(a[i]); q.add(node.right); }
				i++;
			}
		}

		return root;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		SymmetricBinaryTree t = new SymmetricBinaryTree();
		TreeNode r = t.buildTreeSerializationTopDown();
		System.out.println(t.isSymmetric(r));
	}
}
