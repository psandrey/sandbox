package _problems._interviewbit.tree;

/*
 * Given a binary tree, invert the binary tree and return it. 
 */

import java.util.LinkedList;
import java.util.Queue;

public class InvertBinaryTree {

	private class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		public TreeNode(int val) {
			this.val = val;
			this.left = null;
			this.right = null;
		}
	};
	
	public TreeNode invertTree(TreeNode root) {
		if (root == null || (root.left == null && root.right == null))
			return root;
		
		helper(root);
		return root;
	}
	
	private void helper(TreeNode root) {
		TreeNode t = root.left;
		root.left = root.right;
		root.right = t;
		
		if (root.left != null) helper(root.left);
		if (root.right != null) helper(root.right);			
	}
	
	private TreeNode buildBT() {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		
		root.left.left = new TreeNode(4);
		//root.left.right = new TreeNode(5);
		
		root.right.left = new TreeNode(6);
		root.right.right = new TreeNode(7);

		return root;
	}
	
	private void levelOrder(TreeNode root) {
		if (root == null)
			return;
		
		Queue<TreeNode> q = new LinkedList<TreeNode>();
		q.add(root);
		while(!q.isEmpty()) {
			int size = q.size();
			for (int i = 0; i < size; i++) {
				TreeNode n = q.poll();
				System.out.print(n.val + " ");
				if (n.left != null) q.add(n.left);
				if (n.right != null) q.add(n.right);				
			}
			System.out.println();
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		InvertBinaryTree t = new InvertBinaryTree();
		TreeNode root = t.buildBT();
		t.levelOrder(root);
		System.out.println();
		
		TreeNode inverted = t.invertTree(root);
		t.levelOrder(inverted);
	}
}
