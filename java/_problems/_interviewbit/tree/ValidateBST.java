package _problems._interviewbit.tree;

/*
 * Given a binary tree, determine if it is a valid binary search tree (BST).
 */

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class ValidateBST {
	
	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	};
	
	// Based on in-order traversal
	// T(n) = O(n)
	// S(n) = O(n) - in worst case h = n
	public int isValidBST (TreeNode root) {
		if (root == null || (root.left == null && root.right == null)) return 1;
		
		Stack<TreeNode> st = new Stack<>();
		TreeNode n = root, prev = null;
		while (!st.isEmpty() || n != null) {
			while (n != null) { st.push(n); n = n.left; }

			n = st.pop();
			if (prev != null && prev.val >= n.val) return 0;
			prev = n;
			n = n.right;
		}
		
		return 1;
	}


	// Based in BST definition
	// T(n) = O(n)
	// S(n) = O(n) - in worst case h = n
	public int isValidBSTV2(TreeNode root) {
		if (root == null) return 1;
		
		return isValidBSTV2(root, Integer.MIN_VALUE, Integer.MAX_VALUE); 
	}
	
	private int isValidBSTV2(TreeNode n, int min, int max) {
		if (n == null) return 1;
	 
		if (n.val <= min || n.val >= max) return 0;

		int left = isValidBSTV2(n.left, min, n.val);
		int right = isValidBSTV2(n.right, n.val, max);
		return (left == 1 && right == 1 ? 1 : 0);
	}

	// just for testing...
	private TreeNode buildTreeSerializationTopDown() {
		int[] a = new int[] {3, 2, 4, 1, 3, -1, -1, -1, -1, -1, -1}; 

		TreeNode root = new TreeNode(a[0]);
		int n = a.length;
		
		Queue<TreeNode> q = new LinkedList<TreeNode>();
		q.add(root);
		
		int i = 1;
		int size;
		while (i < n) {

			size = q.size();
			for (int j = 0; j < size; j++) {
				TreeNode node = q.poll();
				if (a[i] != -1) { node.left = new TreeNode(a[i]); q.add(node.left);   }
				i++;
				if (a[i] != -1) { node.right = new TreeNode(a[i]); q.add(node.right); }
				i++;
			}
		}

		return root;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		
		ValidateBST t = new ValidateBST();
		TreeNode root = t.buildTreeSerializationTopDown();

		System.out.println(t.isValidBST(root));
		System.out.println(t.isValidBSTV2(root));
	}
}
