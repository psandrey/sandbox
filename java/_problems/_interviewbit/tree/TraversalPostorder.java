package _problems._interviewbit.tree;

/*
 * Given a binary tree, return the postorder traversal of its nodes� values.
 * 
 * Note: Solution contains recursive version and iterative version.
 */

import java.util.ArrayList;
import java.util.Stack;

public class TraversalPostorder {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	};
	
	// recursive version
	public ArrayList<Integer> postOrderRecursive(TreeNode root) {
		ArrayList<Integer> ans = new ArrayList<Integer>();
		if (root == null) return ans;
		helper(root, ans);
		return ans;
	}
	
	private void helper(TreeNode n, ArrayList<Integer> ans) {
		if (n == null) return;
		
		helper(n.left, ans);
		helper(n.right, ans);
		ans.add(n.val);
	}
	
	// iterative version
	public ArrayList<Integer> postOrderIterativeV1(TreeNode root) {
		ArrayList<Integer> ans = new ArrayList<Integer>();
		if (root == null) return ans;

		// Note:
		//   case 1. when we go down the tree, prev is the parent of the current node. So,
		//		   we go down until we hit a leaf.
		//   case 2. when a node is processed (popped from stack), prev will be the processed
		//		   node. So, it means we go up the tree. Following the post-order traversal,
		//		   when we go up the tree we need to check if we came from left or from right
		//		   branch (st.peek().left == prev or st.peek().right == prev).
		//		   If left, then push the right node, if right then process the node
		//		   and continue go up (mind Left, Right, Parent order).
		TreeNode prev = null;
		Stack<TreeNode> st = new Stack<>();
		st.push(root);
		while (!st.isEmpty()) {
			TreeNode n = st.peek();
			
			// goes down the tree until hits a leaf and then process it
			if (prev == null || prev.left == n || prev.right == n) {
				if (n.left != null) st.push(n.left);
				else if (n.right != null) st.push(n.right);
				// we just hit a leaf, so process it
				else {
					n = st.pop();
					ans.add(n.val);
				}
			// if we came from the left branch, then check if we are a leaf, in this case process
			// the node, or if there is something on the right, push it in the stack
			} else if (n.left == prev) {
				if (n.right != null) st.push(n.right);
				else {
					n = st.pop();
					ans.add(n.val);
				}
			// if we came from the right branch, then just process the node and continue to go up
			} else {
				n = st.pop();
				ans.add(n.val);
			}
			
			prev = n;
		}

		return ans;
	}
	
	// iterative version
	public ArrayList<Integer> postOrderIterativeV2(TreeNode n) {
		ArrayList<Integer> ans = new ArrayList<Integer>();
		if (n == null) return ans;
		
		Stack<TreeNode> st = new Stack <TreeNode>();
		TreeNode prev = null;
		while(!st.isEmpty() || n != null){
			
			// we go down until bottom
			if (n != null) {
				st.push(n);
				n = n.left;

			// we backtrack
			} else {
				TreeNode top = st.peek();
				
				// if the right node hasn't been processed yet, then push it in the stack
				if (top.right != null && prev != top.right) n = top.right;
				
				// if top node is leaf or we go up, then process the node
				else {
					ans.add(top.val);
					prev = st.pop();
				}
			}
		}
		
		return ans;
	}
	
	private void printList(ArrayList<Integer> l) {
		System.out.print("[");
		for (Integer x: l) System.out.print(x + ", ");
		System.out.print("]");
		System.out.println();
	}
	
	private TreeNode createTree() {
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.left.left = new TreeNode(1);
		root.left.right = new TreeNode(5);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);
		
		return root;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) { 
		TraversalPostorder t = new TraversalPostorder();
		TreeNode root = t.createTree();
		ArrayList<Integer> ans = t.postOrderRecursive(root);
		t.printList(ans);
		ans = t.postOrderIterativeV1(root);
		t.printList(ans);
	}
}
