package _problems._interviewbit.tree;

/*
 * Given inorder and postorder traversal of a tree, construct the binary tree.
 */

import java.util.HashMap;

public class BinaryTreeFromInorderPostorder {
	
	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	};

	private int k = 0;
	public TreeNode buildTree(int[] inorder, int[] postorder) {
		buildHmap(inorder);
		k = postorder.length-1;
		return helper(inorder, 0, inorder.length-1, postorder);
	}
	
	private TreeNode helper(int[] inorder, int i, int j, int[] postorder) {
		if (i > j) return null;
		int p = find(postorder[k]);

		// since post-order has always the parent node the rightmost element,
		//  we decrease k every time we create a node
		TreeNode n = new TreeNode(postorder[k--]);
		// so, after parent, we have the right subtree
		n.right = helper(inorder, p+1, j, postorder);
		// then the left subtree
		n.left = helper(inorder, i, p-1, postorder);
		return n;
	}

	HashMap<Integer, Integer> hMap = new HashMap<Integer, Integer>();
	private int find(int value) { 
		return hMap.get(value).intValue(); 
	}
	
	private void buildHmap(int in[]) {
		for(int i = 0; i < in.length; i++)
			hMap.put(in[i], i);
	}

/*	
	private TreeNode helper(int in[],   int iStart, int iEnd,
							int post[], int pStart, int pEnd) {

		if (iStart > iEnd || pStart > pEnd) 
			return null;

		int i = find(in, iStart, iEnd + 1, post[pEnd]);
		//int i = search(post[pEnd]);

		TreeNode node = new TreeNode(post[pEnd]);
		node.left  = helper(in, iStart, i - 1, post, pStart                , pStart + (i - iStart - 1));
		node.right = helper(in, i + 1 , iEnd , post, pStart + (i - iStart) , pEnd - 1);
		return node; 
	}

	private int find(int[] a, int i, int j, int x) {
		for (int k = i; k <= j; k++)
			if (a[k] == x) return k;
		
		return -1;
	}
*/

	private void preOrder(TreeNode node) { 
		if (node == null) 
			return; 
		System.out.print(node.val + " "); 
		preOrder(node.left); 
		preOrder(node.right); 
	} 

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		BinaryTreeFromInorderPostorder t = new BinaryTreeFromInorderPostorder(); 
		int in[]   = new int[] { 4, 8, 2, 5, 1, 6, 3, 7 };
		int post[] = new int[] { 8, 4, 5, 2, 6, 7, 3, 1 };

		TreeNode root = t.buildTree(in, post); 
		System.out.println("Preorder of the constructed tree : "); 
		t.preOrder(root);
		
		/* Should be:
		 * Preorder of the constructed tree : 
		 * 1 2 4 8 5 3 6 7 
		 */
	}
}
