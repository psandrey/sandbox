package _problems._interviewbit.tree;

/*
 * Given a binary tree and a sum, find all root-to-leaf paths
 * where each path�s sum equals the given sum.
 */

import java.util.ArrayList;
import java.util.Stack;

public class RootToLeafPathsWithSumII {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	}

	// The recursive version
	public ArrayList<ArrayList<Integer>> pathSumRecursive(TreeNode root, int target) {
		ArrayList<ArrayList<Integer>> ans = new ArrayList<ArrayList<Integer>>();
		if (root == null) return ans;
		Stack<Integer> st = new Stack<>();
		helper(root, st, ans, 0, target);
		
		return ans;
	}

	private void helper(TreeNode n, Stack<Integer> st,
			ArrayList<ArrayList<Integer>> ans, int sum, int target) {
		// this condition make sense iff values are only positive
		//if (sum + n.val > target) return;
		
		st.push(n.val);
		sum += n.val;
		
		// if we are at leaf level and we hit target, then we have a solution
		if (n.left == null && n.right == null && sum == target) {
			ans.add(new ArrayList<Integer>(st));
			sum -= n.val;
			st.pop();
			return;
		}
		
		// if we can, we go deeper
		if (n.left != null) helper(n.left, st, ans, sum, target);
		if (n.right != null) helper(n.right, st, ans, sum, target);
		
		sum -= n.val;
		st.pop();
	}
	
	// The iterative version:
	// T(n) = O(n)
	// S(n) = O(n)
	public ArrayList<ArrayList<Integer>> pathSum(TreeNode root, int target) {
		ArrayList<ArrayList<Integer>> ans = new ArrayList<ArrayList<Integer>>();
		if (root == null) return ans;

		// pre-order traversal
		Stack<TreeNode> st = new Stack<>();
		Stack<Integer> stans = new Stack<>();

		TreeNode n = root, prev = n;
		int sum = n.val;
		st.push(n); stans.push(n.val);
		while (!st.isEmpty()) {
			n = st.peek();
			
			// condition needed iff values are positive only:
			// if sum is less than target, then lets try to go deeper
			//if (sum <= target) {

			// if we are not backtrack neither from left nor from right, then
			//  go deeper on the left side if possible
			if (n.left != null && n.left != prev && n.right != prev) {
				prev = n; n = n.left;
				st.push(n); stans.push(n.val);
				sum += n.val;
				continue;
			// else if we backtrack from left and we go right, then go deeper on
			//  the right side
			} else if (n.right != null && n.right != prev) {
				prev = n; n = n.right;
				st.push(n); stans.push(n.val);
				sum += n.val;
				continue;
			}

			// we are at leaf level and we hit target, so record result
			if (n.left == null && n.right == null && sum == target)
				ans.add(new ArrayList<Integer>(stans));

			// try another path
			prev = n;
			st.pop();
			sum -= stans.pop();
		}

		return ans;
	}
	
	private void printListList(ArrayList<ArrayList<Integer>> ll) {
		for (ArrayList<Integer> l : ll) {
			System.out.print("[");
			for (Integer x: l) System.out.print(x + ", ");
			System.out.print("]");
			System.out.println();
		}
	}
	
	private TreeNode createTree() {
		TreeNode root = new TreeNode(5);
		root.left = new TreeNode(4);
		root.right = new TreeNode(8);
		root.left.left = new TreeNode(11);
		root.left.left.left = new TreeNode(7);
		root.left.left.right = new TreeNode(2);
		
		root.right.left = new TreeNode(13);
		root.right.right = new TreeNode(4);
		root.right.right.left = new TreeNode(5);
		root.right.right.right = new TreeNode(1);
		return root;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		
		RootToLeafPathsWithSumII t = new RootToLeafPathsWithSumII();
		TreeNode root = t.createTree();
		int target = 22;
		ArrayList<ArrayList<Integer>> ans;

		ans = t.pathSum(root, target);
		t.printListList(ans);
	
		System.out.println();
		ans = t.pathSumRecursive(root, target);
		t.printListList(ans);
	}
}
