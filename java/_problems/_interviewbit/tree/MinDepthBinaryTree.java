package _problems._interviewbit.tree;

/*
 * Given a binary tree, find its minimum depth.
 * The minimum depth is the number of nodes along the shortest path from
 *  the root node down to the nearest leaf node.
 */

import java.util.Stack;

public class MinDepthBinaryTree {
	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	}
	
	// The recursive version
	public int minDepth(TreeNode n) {
		if (n == null) return 0;
		
		if (n.left != null && n.right != null)
			return Math.min(minDepth(n.left), minDepth(n.right)) + 1;
		
		if (n.left != null) return minDepth(n.left) + 1;
		else if (n.right != null) return minDepth(n.right) + 1;
		
		return 1;
	}
	
	// The iterative version
	public int minDepthIterative(TreeNode root) {
		if (root == null) return 0;
	
		// pre-order traversal
		Stack<TreeNode> st = new Stack<>();
		Stack<Integer> stdepth = new Stack<>();

		int min = Integer.MAX_VALUE;
		st.push(root); stdepth.push(1);
		while (!st.isEmpty()) {
			TreeNode n = st.pop();
			int d = stdepth.pop();
			
			if (n.left == null && n.right == null)
				min = Math.min(min, d);
			
			if (n.right != null) {
				st.push(n.right);
				stdepth.push(d+1);
			}

			if (n.left != null) {
				st.push(n.left);
				stdepth.push(d+1);
			}
		}
	
		return min;
	}
	
	private TreeNode createTree() {
		TreeNode root = new TreeNode(5);
		root.left = new TreeNode(4);
		root.right = new TreeNode(8);
		root.left.left = new TreeNode(11);
		root.left.left.left = new TreeNode(7);
		root.left.left.right = new TreeNode(2);
		
		root.right.left = new TreeNode(13);
		root.right.right = new TreeNode(4);
		root.right.right.left = new TreeNode(5);
		root.right.right.right = new TreeNode(1);
		return root;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		
		MinDepthBinaryTree t = new MinDepthBinaryTree();
		TreeNode root = t.createTree();

		System.out.println("Iterative: " + t.minDepthIterative(root));
		System.out.println("Recursive: " + t.minDepth(root));
	}
}
