package _problems._interviewbit.tree;

/*
 * Given an inorder traversal of a cartesian tree, construct the tree.
 * Note: Cartesian tree is a heap ordered binary tree, where the root is greater
 *       than all the elements in the subtree.
 *       
 * Note 1: The recursive version: T(n) = O(n^2)
 * Note 2: The linear version (using all nearest smaller values algorithm): T(n) = O(n)
 */

import java.util.Stack;

public class InorderTraversalCartesianTree {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	};
	
	// T(n) = O(n)
	public TreeNode buildTree(int[] inorder) {
		
		// stack is used to keep track of the nearest greater node up to the root
		Stack<TreeNode> st = new Stack<>();
		TreeNode root = null;
		int m = inorder.length;
		
		for (int i = 0; i < m; i++) {
			// if the stack is empty, then inorder[i] must be the root
			if (st.isEmpty()) {
				root = new TreeNode(inorder[i]);
				st.push(root);
			} else {
				TreeNode n = new TreeNode(inorder[i]);
				
				// popping elements from stack until a node with a bigger value is found
				while (!st.isEmpty() && st.peek().val < n.val) st.pop();
				
				// if the stack is empty, then inorder[i] is the greatest element (i.e. the root)
				if (st.isEmpty()) {
					n.left = root;
					root = n;

				// else, make a left rotation with the right child of the the greatest element
				} else {
					n.left = st.peek().right;
					st.peek().right = n;
				}
				st.push(n);
			}
		}
		return root;
	}
	
	// T(n) = O(n^2)
	public TreeNode buildTreeV1(int[] inorder) {
		return helper(inorder, 0, inorder.length-1);
	}
	
	private TreeNode helper(int[] inorder, int i, int j) {
		if (i > j) return null;
		else if (i == j) return new TreeNode(inorder[i]);
		
		int max = getMax(inorder, i, j);
		TreeNode n = new TreeNode(inorder[max]);
		n.left = helper(inorder, i, max-1);
		n.right = helper(inorder, max+1, j);

		return n;
	}
	
	private int getMax(int[] a, int i, int j) {
		int max =Integer.MIN_VALUE;
		int maxK = -1;
		for (int k = i; k <= j; k++)
			if (max < a[k]) { max = a[k]; maxK = k; };

		return maxK;
	}
	
	// for testing...
	private void inOrder(TreeNode node) { 
		if (node == null) 
			return;
		
		inOrder(node.left); 
		System.out.print(node.val + " "); 
		inOrder(node.right); 
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) { 
		int[] a = new int[] {1, 5, 10, 40, 30, 15, 28, 20};
		InorderTraversalCartesianTree t = new InorderTraversalCartesianTree();
		TreeNode root1 = t.buildTreeV1(a);
		TreeNode root2 = t.buildTree(a);
		
		System.out.println("V1:");
		t.inOrder(root1);System.out.println();
		System.out.println("V2");
		t.inOrder(root2);
	}
}
