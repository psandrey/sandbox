package _problems._interviewbit.tree;

/*
 * Given a binary tree, return the zigzag level order traversal of its nodes� values.
 * (ie, from left to right, then right to left for the next level and alternate between).
 */

import java.util.ArrayList;
import java.util.Stack;

public class ZigZagLevelOrderTraversalBT {
	
	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	};
	
	// T(n) = O(n)
	// S(n) = O(logn)
	public ArrayList<ArrayList<Integer>> zigzagLevelOrder(TreeNode root) {
		ArrayList<ArrayList<Integer>> ans = new ArrayList<ArrayList<Integer>>();
		if (root == null) return ans;
		
		Stack<TreeNode> stLR = new Stack<>();
		Stack<TreeNode> stRL = new Stack<>();
		Stack<TreeNode> st = stLR;
		Stack<TreeNode> nextSt = stRL;
		int dir = 1; // left to right
		stLR.push(root);
		
		ArrayList<Integer> level = null;
		while (!st.isEmpty()) {
			
			level = new ArrayList<Integer>();
			while (!st.isEmpty()) {
				TreeNode n = st.pop();
				level.add(n.val);
				if (dir == 1) { // left to right
					if (n.left  != null) nextSt.push(n.left);
					if (n.right != null) nextSt.push(n.right);
				} else { // right to left
					if (n.right != null) nextSt.push(n.right);					
					if (n.left  != null) nextSt.push(n.left);
				}
			}
			ans.add(level);
		
			// change direction
			Stack<TreeNode> t = st;
			st = nextSt;
			nextSt = t;
			dir = dir^1;
		}
		
		return ans;
	}
	
	private TreeNode createTree() {
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);
		
		return root;
	}
	
	private void printListOfLists(ArrayList<ArrayList<Integer>> ll) {
		for (ArrayList<Integer> l : ll) {
			System.out.print("[");
			for (Integer x : l)
				System.out.print(x + ", ");
			System.out.print("]");
			System.out.println("");
		}
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		ZigZagLevelOrderTraversalBT t = new ZigZagLevelOrderTraversalBT();
		TreeNode root = t.createTree();
		ArrayList<ArrayList<Integer>> ll = t.zigzagLevelOrder(root);
		t.printListOfLists(ll);
	}
}
