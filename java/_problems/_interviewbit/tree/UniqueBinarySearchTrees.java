package _problems._interviewbit.tree;

/*
 * Given n, how many structurally unique BST's (binary search trees) that store values 1...n? 
 * For example, Given n = 3, there are a total of 5 unique BST's.
 * 
 * Note: the solution contains also: the number of BST the brute-force and dynamic programing versions
 */

import java.util.ArrayList;
import java.util.List;

public class UniqueBinarySearchTrees {

	private class TreeNode {
		public TreeNode left = null;
		public TreeNode right = null;
		public int val ;
		public TreeNode(int x) { val = x; }
	};

	// generate all binary search trees
	public ArrayList<TreeNode> generateTrees(int n) {
		if (n == 0) return new ArrayList<TreeNode>();
	 
		return helper(1, n);
	}
	
	// The recursive version
	private ArrayList<TreeNode> helper(int start, int end){
		ArrayList<TreeNode> result = new ArrayList<TreeNode>();
		if (start > end) {
			result.add(null);
			return result;
		}
	 
		for (int i = start; i <= end; i++){
			List<TreeNode> ls = helper(start, i-1);
			List<TreeNode> rs = helper(i+1, end);

			for (TreeNode l: ls) {
				for (TreeNode r: rs) {
					TreeNode curr = new TreeNode(i);
					curr.left=l;
					curr.right=r;
					result.add(curr);
				}
			}
		}
	 
		return result;
	}
	
	// number of binary trees (labeled): dynamic programming version
	private int numBT(int n) {
		// there are Cn number of binary structures
		// and we can place n label in n! ways (permutations)
		if ( n == 0 || n == 1)
			return 1;

		int[] dp = new int[n+1];
		dp[0] = 1;
		dp[1] = 1;
		for(int i = 2; i <= n; i++)
			for (int j = 0; j < i; j++)
				dp[i] += dp[j]*dp[i-j-1];
		
		return dp[n] * factorial(n);
	}
	
	static int factorial(int n) 
	{ 
		int res = 1; 
	  
		for (int i = 1; i <= n; ++i)
			res *= i; 
	  
		return res; 
	}
	
	// number of binary search trees: dynamic programming version
	private int numBSTTrees(int n) {
		if ( n == 0 || n == 1)
			return 1;

		int[] dp = new int[n+1];
		dp[0] = 1;
		dp[1] = 1;
		for(int i = 2; i <= n; i++)
			for (int j = 0; j < i; j++)
				dp[i] += dp[j]*dp[i-j-1];
		
		return dp[n];
	}
	
	// number of binary search trees:  recursive brute force
	private int f(int n) {
		if (n == 0 || n == 1)
			return 1;
		
		int s = 0;
		for (int i = 1; i <= n; i++)
			s += f(i-1)*f(n-i);

		return s;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		UniqueBinarySearchTrees t = new UniqueBinarySearchTrees();
		int n = 5;
		System.out.println("Total number of BST:" + t.f(n));
		System.out.println("Total number of BST:" + t.numBSTTrees(n));
		System.out.println("Total number of labeled BT: " + t.numBT(n));
		ArrayList<TreeNode> bsts = t.generateTrees(3);
		System.out.println(bsts);
		
	}

}
