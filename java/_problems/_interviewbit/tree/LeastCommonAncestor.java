package _problems._interviewbit.tree;

/*
 * Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree.
 */

//import java.util.LinkedList;

public class LeastCommonAncestor {
	
	private class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		public TreeNode(int val) {
			this.val = val;
			this.left = null;
			this.right = null;
		}
	};
	
	public TreeNode LCA(TreeNode root, int a, int b) {
		boolean[] found = new boolean[2];
		
		TreeNode n = helper(root, a, b, found);
		if (found[0] && found[1])
			return n;
		
		return null;
	}
	
	// if we know that both a and b are present in the tree
	/*private TreeNode helper(TreeNode n, int a, int b) {
		if (n == null)
			return null;

		if (n.val == a || n.val == b)
			return n;
		
		TreeNode l = helper(n.left , a, b);
		TreeNode r = helper(n.right, a, b);
	
		if (l != null && r != null)
			return n;
		
		return (l == null ? r : l);
	}*/
	
	// if we don't know if both a and b are present
	private TreeNode helper(TreeNode n, int a, int b, boolean[] found) {
		if (n == null)
			return null;
		
		boolean me = false;
		if (n.val == a) { found[0] = true; me = true; }
		if (n.val == b) { found[1] = true; me = true; }
		
		TreeNode l = helper(n.left , a, b, found);
		TreeNode r = helper(n.right, a, b, found);

		if (me || (l != null && r != null))
			return n;
		
		return (l == null ? r : l);
	}

	/*LinkedList<TreeNode> path = new LinkedList<TreeNode>();
	private boolean buildPath(TreeNode n, int a) {
		path.add(n);
		if (n.val == a)
			return true;
		
		boolean found = false;
		if (n.left != null) found = buildPath(n.left, a);
		if (!found && n.right != null) found = buildPath(n.right, a);
		if (!found)
			path.removeLast();
		return found;
	}
	
	private void printLinkedList(LinkedList<TreeNode> l) {
		for (TreeNode n: l) {
			System.out.print(n.val + " ");
		}
	}*/

	private TreeNode buildBT() {
		TreeNode root = new TreeNode(3);
		
		root.left = new TreeNode(5);
		root.right = new TreeNode(1);
		
		root.left.left = new TreeNode(6);
		root.left.right = new TreeNode(2);
		
		root.left.right.left = new TreeNode(7);
		root.left.right.right = new TreeNode(4);
				
		root.right.left = new TreeNode(0);
		root.right.right = new TreeNode(8);

		return root;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		LeastCommonAncestor t = new LeastCommonAncestor();
		
		TreeNode root = t.buildBT();
		//t.buildPath(root, 2);
		//t.printLinkedList(t.path);
		
		
		TreeNode n = t.LCA(root, 6, 7);
		System.out.println(n.val);
	}

}
