package _problems._interviewbit.tree;

/*
 * Two elements of a binary search tree (BST) are swapped by mistake.
 * Tell us the 2 values swapping which the tree will be restored.
 */

public class RecoverBinarySearchTree {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	};
	
	
	// T(n) = O(n)
	// S(n) = O(n) - due to recursive calls, but we can consider (abusive) that S(n) = O(1)
	//  since we do not use an array or linked list of size n
	TreeNode first, second, pred;
	public int[] recoverTree(TreeNode root) {
		inorder(root);

		if (first != null && second != null)
			return new int[] {second.val, first.val};
	
		return new int[] {};
	}
	
	private void inorder(TreeNode n) {
		if (n == null) return;
		
		inorder(n.left);
		
		// if predecessor is null, it means we just reached to the leftmost element
		//  i.e. smallest element in the BST (or, first element in the ascending order)
		if (pred == null) pred = n;
		else {
			// if the predecessor is bigger than the current node, it means that we
			//  found a swapping place
			if (pred.val > n.val) {
				// if first hasn't been set, then we ar the first swapping place:
				//  case 1. one swapping place, meaning that predecessor is swapped with current node
				//  case 2. if we found another swapping place, then this predecessor with
				//          the current node of the second swapping place are swapped
				if (first == null) first = pred;
				second = n;
			}

			// keep track of the predecessor for the next node
			pred = n;
		}

		inorder(n.right);
	}
	
/*	
	public int[] recoverTree(TreeNode root) {
		if (root == null || (root.left == null && root.right == null)) return new int[] {};
		
		helper(root);
		if (pair == null) return new int[] {}; // perfect BST
		
		return new int[] {pair.b, pair.a};
	}
	
	private class Pair {
		int a, b;
		public Pair(int x, int y) { a = x; b = y; }
	}
	Pair pair = null;
	private int[] helper(TreeNode n) {
		if (n == null) return new int[] {};
		
		int[] minMax = new int[] {n.val, n.val};
		if (n.left != null) {
			int[] mM = helper(n.left);
			// if predecessor from left branch (maximum element) > current, then
			//  the predecessor is swapped with the current, or with another successor
			if (mM[1] > n.val) {
				if (pair == null) pair = new Pair(mM[1], n.val);
				else pair.b = n.val;
			}
			else minMax[0] = mM[0];
		}
		
		if (n.right != null) {
			int[] mM = helper(n.right);
			// if the successor from the right branch (the minimum element) < current, then
			//  the successor is swapped with a predecessor
			if (n.val > mM[0]) {
				if (pair == null) pair = new Pair(n.val, mM[0]);
				else pair.b = mM[0];
			}
			else minMax[1] = mM[1];
		}
		
		// return minimum and maximum element on the tree rooted in n (since we do not know if
		//  we are on the left branch or on the right branch
		return minMax;
	}
*/
	
	private TreeNode createTree() {
		TreeNode root = new TreeNode(5);
		root.left = new TreeNode(1);
		root.right = new TreeNode(7);
		root.right.left = new TreeNode(9);
		root.right.left.left = new TreeNode(6);
		root.right.left.right = new TreeNode(8);

		return root;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {	
		RecoverBinarySearchTree t = new RecoverBinarySearchTree();
		TreeNode r = t.createTree();
		
		int[] minMax = t.recoverTree(r);
		if (minMax.length == 2) System.out.println("[" + minMax[0] + "," + minMax[1] + "]");
	}
}
