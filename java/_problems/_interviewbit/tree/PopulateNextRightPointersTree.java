package _problems._interviewbit.tree;

/*
 * Populate each next pointer to point to its next right node.
 * If there is no next right node, the next pointer should be set to NULL.
 * 
 * Initially, all next pointers are set to NULL.
 */

public class PopulateNextRightPointersTree {

	private class TreeLinkNode {
		TreeLinkNode left = null, right = null, next = null;
		int val = 0;
	};
	
	// T(n) = O(n)
	// S(n) = O(1)
	public void connect(TreeLinkNode root) {
		TreeLinkNode head = null, prevHead = root, cur = null, prevCur = null;
		
		while (prevHead != null) {
			prevCur = prevHead;
			
			// while we have a next to link
			while (prevCur != null) {
				
				// jump to the next non-leaf prevCur
				if (prevCur != null && prevCur.left == null && prevCur.right == null) {
					prevCur = prevCur.next;
					continue;
				}
				
				// handle left child
				if (prevCur.left != null) {
					// this is the first node at current level, so initialize it
					if (head == null) {
						head = prevCur.left;
						cur = head;
					// else, link next right
					} else {
						cur.next = prevCur.left;
						cur = cur.next;
					}
				}
					
				// handle right child
				if (prevCur.right != null) {
					// this is the first node at current level, so initialize it
					if (head == null) {
						head = prevCur.right;
						cur = head;
					// else, link next right
					} else {
						cur.next = prevCur.right;
						cur = cur.next;
					}
				}
				
				prevCur = prevCur.next;
			}
			
			prevHead = head;
			head = null;
		}
	}
	
	private TreeLinkNode buildTestTree() {
		TreeLinkNode root = new TreeLinkNode();root.val = 1;
		root.left  = new TreeLinkNode();root.left.val = 2;
		root.right = new TreeLinkNode();root.right.val = 3;

		root.left.left  = new TreeLinkNode();root.left.left.val = 4;
		root.right.left = new TreeLinkNode();root.right.left.val = 6;
		return root;
	}
	
	private void traverseNext(TreeLinkNode root) {
		TreeLinkNode head, prevHead;
		head = root;
		while (head != null) {
			prevHead = head;
			
			while (head != null) {
				System.out.print(head.val + ", ");
				head = head.next;
			}
			System.out.println();
			
			TreeLinkNode prevCur = prevHead;
			while (prevCur != null && prevCur.left == null && prevCur.right == null)
				prevCur = prevCur.next;
			
			if (prevCur != null)
				head = (prevCur.left != null ? prevCur.left : prevCur.right);
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) { 
		PopulateNextRightPointersTree t = new PopulateNextRightPointersTree();
		TreeLinkNode root = t.buildTestTree();
		t.connect(root);
		t.traverseNext(root);
	}
}
