package _problems._interviewbit.tree;

/*
 * Given a binary tree, determine if it is height-balanced.
 */

public class BalancedBinaryTree {
	
	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	};
	
	// Recursive version:
	// T(n) = O(n);
	// S(n) = O(log.n) - average case, O(n) - worst case
	public boolean isBalanced(TreeNode root) {
		if (root == null || (root.left == null && root.right == null)) return true;

		int height = getHeight(root);
		return (height == -1? false : true);
	}
	
	private int getHeight(TreeNode n) {
		if (n == null) return 0;
		
		int hl = getHeight(n.left);
		int hr = getHeight(n.right);
		if (hl == -1 || hr == -1) return -1;
		if (Math.abs(hr - hl) >= 2) return -1;
		
		return (Math.max(hr, hl) + 1);				
	}

	private TreeNode createTree() {
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.left.left = new TreeNode(1);
		root.left.right = new TreeNode(5);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);
		
		return root;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) { 
		BalancedBinaryTree t = new BalancedBinaryTree();
		TreeNode root = t.createTree();
		System.out.println(t.isBalanced(root));
	}
}
