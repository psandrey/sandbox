package _problems._interviewbit.tree;

/*
 * Find shortest unique prefix to represent each word in the list.
 * Note: Solution using Trie.
 */

public class ShortestUniquePrefix {

	final int sizeE = 26;
	private class TrieNode {
		TrieNode[] child = new TrieNode[sizeE];
		int freq = 0;
		public TrieNode() { }
	};
		
	private void insert(TrieNode t, String s) {
		TrieNode n = t;
		for (int i = 0; i < s.length(); i++) {
			int idx = s.charAt(i) - 'a';
			if (n.child[idx] == null)
				n.child[idx] = new TrieNode();
			
			n.freq++;
			n = n.child[idx];
		}
	}
	
	public String[] prefix(String[] words) {
        if (words == null || words.length == 0)
            return new String[0];

		TrieNode T = new TrieNode();
		for (int i = 0; i < words.length; i++)
			insert(T, words[i]);

		return buildPrefixes(T, words);
	}
	
	private String[] buildPrefixes(TrieNode t, String[] words) {
		String[] pref = new String[words.length];
		
		for (int i = 0; i < words.length; i ++) {
			TrieNode n = t;
			
			for (int j = 0; j < words[i].length(); j++) {
				char c = words[i].charAt(j);
				int idx = c - 'a';
				
				pref[i] = ((pref[i] == null) ? Character.toString(c) : pref[i] + c);
				if (n.child[idx].freq == 1)
					break;
				n = n.child[idx];
			}
		}

		return pref;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		ShortestUniquePrefix t = new ShortestUniquePrefix();
		String[] s = {"zebra", "dog", "duck", "dove"};
		String[] pref = t.prefix(s);
		
		for (int i = 0; i < pref.length; i++) {
			System.out.println(pref[i]);
		}
	}
}
