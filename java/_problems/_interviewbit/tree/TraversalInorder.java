package _problems._interviewbit.tree;

/*
 * Given a binary tree, return the inorder traversal of its nodes� values.
 */

import java.util.ArrayList;
import java.util.Stack;

public class TraversalInorder {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	}
	
	// Iterative version:
	// T(n) = O(n)
	// S(n) = O(n) - worst case
	public ArrayList<Integer> inorderTraversal(TreeNode root) {
		ArrayList<Integer> ans = new ArrayList<>();
		if (root == null) return ans;
		
		Stack<TreeNode> st = new Stack<>();
		TreeNode n = root;
		while (!st.isEmpty() || n != null) {
			while (n != null) { st.push(n); n = n.left; }

			n = st.pop();
			ans.add(n.val);
			n = n.right;
		}
		
		return ans;
	}
	
	// The recursive version
	public ArrayList<Integer> inorderTraversalRecursive(TreeNode root) {
		ArrayList<Integer> ans = new ArrayList<>();
		if (root == null) return ans;
		inorderTraversalRecursive(root, ans);

		return ans;
	}
	
	public void inorderTraversalRecursive(TreeNode n, ArrayList<Integer> ans) {
		if (n == null) return;
		
		inorderTraversalRecursive(n.left, ans);
		ans.add(n.val);
		inorderTraversalRecursive(n.right, ans);
	}
	
	private void printList(ArrayList<Integer> l) {
		System.out.print("[");
		for (Integer x: l) System.out.print(x + ", ");
		System.out.print("]");
		System.out.println();
	}
	
	private TreeNode createTree() {
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.left.left = new TreeNode(1);
		root.left.right = new TreeNode(5);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);
		
		return root;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		TraversalInorder t = new TraversalInorder();
		TreeNode root = t.createTree();
		ArrayList<Integer> ans = t.inorderTraversal(root);
		t.printList(ans);
		ans = t.inorderTraversalRecursive(root);
		t.printList(ans);
	}
}
