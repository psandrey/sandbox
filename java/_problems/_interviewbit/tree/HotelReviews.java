package _problems._interviewbit.tree;

/*
 * Given a set of reviews provided by the customers for different hotels and
 *  a string containing �Good Words�, you need to sort the reviews in descending order
 *  according to their �Goodness Value� (Higher goodness value first).
 *  We define the �Goodness Value� of a string as the number of �Good Words� in that string.
 *  
 *  Note: Sorting should be stable. If review i and review j have the same �Goodness Value�
 *   then their original order would be preserved.
 *   
 * Example:
 * Input:
 *  S = "cool_ice_wifi"
 *  R = ["water_is_cool", "cold_ice_drink", "cool_wifi_speed"]
 * 
 * Output:
 *  ans = [2, 0, 1]
 *  Here, sorted reviews are ["cool_wifi_speed", "water_is_cool", "cold_ice_drink"]
 */

//import java.util.Stack;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

public class HotelReviews {	
	
	// classic trie node
	private class TrieNode {
		TrieNode[] children;
		boolean isWord;
		public TrieNode(int alphabet) {
			children = new TrieNode[alphabet];
			isWord = false;
		}
	};
	
	// classic trie (insert, containsWord)
	private class Trie {
		int ALPHABET = 26; // only lower characters (a:z)
		TrieNode root;
		public Trie() { root = new TrieNode(ALPHABET); }
		
		public void insert(String word) {
			if (word == null) return;
			
			TrieNode n = root;
			for (int i = 0; i < word.length(); i++) {
				int k = word.charAt(i) - 'a';
				if (n.children[k] == null) n.children[k] = new TrieNode(ALPHABET);
				n = n.children[k];
			}
			n.isWord = true;
		}
		
		public boolean containsWord(String word) {
			if (word == null) return false;
			
			TrieNode n = root;
			for (int i = 0; i < word.length(); i++) {
				int k = word.charAt(i) - 'a';
				if (n.children[k] == null) return false;
				n = n.children[k];
			}
			
			return n.isWord;
		}
	};

	private Trie buildTrie(String S) {
		Trie t = new Trie();
		if (S == null || S.length() == 0) return t;
		String[] goodWords = S.split("_");
		
		for (String s: goodWords) t.insert(s);
		
		return t;
	}

	public ArrayList<Integer> solve(String S,  ArrayList<String> R) {
		ArrayList<Integer> ans = new ArrayList<Integer>();
		if (S == null || S.length() == 0 || R == null || R.size() == 0) return ans;
		
		Trie trie = buildTrie(S);
		
		
		HashMap<Integer, ArrayList<Integer>> hm = new HashMap<>();
		
		int n = R.size();
		for (int i = 0; i < n; i++) {
			String[] words = R.get(i).split("_");
			int count = 0;
			for (String w: words) count = (trie.containsWord(w) ? count+1: count);
			
			if (hm.containsKey(count))
				hm.get(count).add(i);
			else {
				ArrayList<Integer> l = new ArrayList<Integer>();
				l.add(i);
				hm.put(count, l);
			}
		}
		
		ArrayList<Integer> keys = new ArrayList<Integer>(hm.keySet());
		Collections.sort(keys, Collections.reverseOrder());
		for (Integer key: keys) ans.addAll(hm.get(key));

		return ans;
	}
/*
	private void printTrie(Trie t) {
		Stack<Character> st = new Stack<Character>();
		printTrie(t.root, st);
	}
	
	private void printTrie(TrieNode n, Stack<Character> st) {
		if (n == null) return;
		if (n.isWord) System.out.println(st);

		for (int i = 0; i < n.children.length; i++) {
			if (n.children[i] == null) continue;

			st.push((char)('a' + i));
			printTrie(n.children[i], st);
			st.pop();
		}
	}
	
	private void unitTestTrie() {
		String S = "cool_ice_wifi";

		Trie trie = buildTrie(S);
		printTrie(trie);
		System.out.println(trie.containsWord("cool"));
	}
*/
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		String S = "play_boy";
		ArrayList<String> R = new ArrayList<String>
			(Arrays.asList("smart_guy", "girl_and_boy_play", "play_ground"));
	
		HotelReviews t = new HotelReviews();
		//t.unitTestTrie();
		ArrayList<Integer> ans = t.solve(S, R);
		System.out.print(ans);
	}
}
