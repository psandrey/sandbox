package _problems._interviewbit.tree;

/*
 * Given a binary tree, return the preorder traversal of its nodes� values.
 * 
 * Note: 2 iterative versions and the recursive version.
 */

import java.util.ArrayList;
import java.util.Stack;

public class TraversalPreorder {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	}

	// The iterative version 1
	public ArrayList<Integer> preorderTraversalV1(TreeNode root) {
		ArrayList<Integer> ans = new ArrayList<>();
		if (root == null) return ans;
	
		Stack<TreeNode> st = new Stack<>();
		TreeNode n = root;
		st.push(n);
		while (!st.isEmpty()) {
			n = st.pop();
			ans.add(n.val);
			
			// push right and then left, since the stack reverses the order, so
			//  we need left then right :)
			if (n.right != null) st.push(n.right);
			if (n.left != null) st.push(n.left);
		}
		
		return ans;	
	}

	// The iterative version 2: this implementation is similar with in-order traversal
	public ArrayList<Integer> preorderTraversalV2(TreeNode root) {
		ArrayList<Integer> ans = new ArrayList<>();
		if (root == null) return ans;
	
		Stack<TreeNode> st = new Stack<>();
		TreeNode n = root;
		while (!st.isEmpty() || n != null) {
			while (n != null) {
				ans.add(n.val);
				st.push(n);
				n = n.left;
			}

			n = st.pop();
			n = n.right;
		}
		
		return ans;	
	}

	// The recursive version
	public ArrayList<Integer> preorderTraversalRecursive(TreeNode root) {
		ArrayList<Integer> ans = new ArrayList<>();
		if (root == null) return ans;
		preorderTraversalRecursive(root, ans);

		return ans;
	}

	public void preorderTraversalRecursive(TreeNode n, ArrayList<Integer> ans) {
		if (n == null) return;
		
		ans.add(n.val);
		preorderTraversalRecursive(n.left, ans);
		preorderTraversalRecursive(n.right, ans);
	}
	
	private void printList(ArrayList<Integer> l) {
		System.out.print("[");
		for (Integer x: l) System.out.print(x + ", ");
		System.out.print("]");
		System.out.println();
	}
	
	private TreeNode createTree() {
		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.left.left = new TreeNode(1);
		root.left.right = new TreeNode(5);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);
		
		return root;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		TraversalPreorder t = new TraversalPreorder();
		TreeNode root = t.createTree();
		ArrayList<Integer> ans = t.preorderTraversalV1(root);
		t.printList(ans);
		ans = t.preorderTraversalV2(root);
		t.printList(ans);
		ans = t.preorderTraversalRecursive(root);
		t.printList(ans);
	}
}
