package _problems._interviewbit.tree;

/*
 * Given a binary search tree, write a function to find the kth smallest element in the tree.
 */

import java.util.Stack;

public class KthSmallestElementInTree {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	}
	
	public int kthsmallest(TreeNode root, int k) {
		if (root == null) return -1;
		
		Stack<TreeNode> st = new Stack<>();
		TreeNode n = root;
		while (!st.isEmpty() || n != null) {
			while (n != null) { st.push(n); n = n.left; }

			n = st.pop(); k--;
			if (k == 0) return n.val;
			n = n.right;
		}
		
		return -1;
	}
	
	private TreeNode createTree() {
		TreeNode root = new TreeNode(10);
		root.left = new TreeNode(5);
		root.right = new TreeNode(12);
		root.left.left = new TreeNode(3);
		root.left.right = new TreeNode(7);
		root.left.right.left = new TreeNode(6);
		
		return root;
	}
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		KthSmallestElementInTree t = new KthSmallestElementInTree();
		TreeNode root = t.createTree();
		System.out.println(t.kthsmallest(root, 3));
	}
}
