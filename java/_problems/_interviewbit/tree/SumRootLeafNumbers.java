package _problems._interviewbit.tree;

/*
 * Given a binary tree containing digits from 0-9 only, each root-to-leaf path could represent a number.
 * An example is the root-to-leaf path 1->2->3 which represents the number 123.
 * Find the total sum of all root-to-leaf numbers % 1003.
 */

public class SumRootLeafNumbers {

	private class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		public TreeNode(int val) {
			this.val = val;
			this.left = null;
			this.right = null;
		}
	};
	
	public int sumNumbers(TreeNode n) {
		if (n == null) return -1;
		
		return sum(n, 0);
	}
	
	private int sum(TreeNode n, int s) {
		if (n == null) return 0;
		s = (s*10 + n.val)%1003;
		
		if (n.left == null && n.right == null) return s;
		
		return (sum(n.left, s) + sum(n.right, s))%1003;
	}
	
	private TreeNode buildBT() {
		TreeNode root = new TreeNode(1);
		
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(5);

		root.left.left.left = new TreeNode(7);
		root.left.left.right = new TreeNode(8);
		
		root.right.right = new TreeNode(6);
		
		return root;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		SumRootLeafNumbers t = new SumRootLeafNumbers();
		TreeNode n = t.buildBT();
		int S = t.sumNumbers(n);
		System.out.println(S);
		//t.printLinkedList(S);
	}
}
