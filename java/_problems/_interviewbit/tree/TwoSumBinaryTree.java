package _problems._interviewbit.tree;

/*
 * Given a binary search tree T, where each node contains a positive integer,
 * and an integer K, you have to find whether or not there exist two different
 * nodes A and B such that A.value + B.value = K.
 * 
 * Return 1 to denote that two such nodes exist. Return 0, otherwise.
 */

import java.util.Stack;

public class TwoSumBinaryTree {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	};

	// Iterative version using stack
	// T(n) = O(n)
	// S(n) = O(h) - worst case O(n)
	public int t2Sum(TreeNode root, int k) {
		if (root == null || (root.left == null && root.right == null)) return 0;
		Stack<TreeNode> in = new Stack<>();  // in-order traversal
		Stack<TreeNode> rev = new Stack<>(); // reverse-in-order traversal
		
		TreeNode l = root;
		while (l != null) { in.push(l); l = l.left; }
		
		TreeNode r = root;
		while (r != null) { rev.push(r); r = r.right; }
		
		// 2-sum pointers on BST
		do {
			l = in.peek();
			r = rev.peek();
			if (l == r) break;
			
			if (l.val + r.val == k)
				return 1; // found the sum
			else if (l.val + r.val > k) { // move r backward
				rev.pop();
				r = r.left;
				while (r != null) { rev.push(r); r = r.right; }
			} else { // move l forward
				in.pop();
				l = l.right;
				while (l != null) { in.push(l); l = l.left; }
			}
		} while (!in.isEmpty() && !rev.isEmpty());
		
		return 0;
	}
	
	private TreeNode createTree() {
		TreeNode root = new TreeNode(5);
		root.left = new TreeNode(1);
		root.right = new TreeNode(9);
		root.right.left = new TreeNode(7);
		root.right.left.left = new TreeNode(6);
		root.right.left.right = new TreeNode(8);

		return root;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		TwoSumBinaryTree t = new TwoSumBinaryTree();
		
		TreeNode root = t.createTree();
		int k = 16;
		System.out.println(t.t2Sum(root, k));
	}
}
