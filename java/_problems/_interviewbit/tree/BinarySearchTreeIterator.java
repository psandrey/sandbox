package _problems._interviewbit.tree;

/*
 * Implement an iterator over a binary search tree (BST).
 * Your iterator will be initialized with the root node of a BST.
 * Calling next() will return the next smallest number in the BST.
 * 
 * Note: next() 
 *         T(n)=O(1) amortized time complexity
 *         S(n)=O(h) space complexity (h=n in worst case, so T(n)=O(n))
 */

import java.util.Stack;

public class BinarySearchTreeIterator {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	};

	Stack<TreeNode> st;

	public void Solution(TreeNode root) {
		st = new Stack<TreeNode>();
		if (root == null) return;

		while (root != null) {
			st.push(root);
			root = root.left;
		}
	}
	
	public boolean hasNext() {
		return (!st.empty());
	}
	
	public int next() {
		if (!hasNext()) return -1; 

		TreeNode n = st.pop();
		int next = n.val;
		
		if (n.right != null) {
			n = n.right;
			st.push(n);
			n = n.left;
			while (n != null) { st.push(n); n = n.left; }
		}
		
		return next;
	}
	
	private TreeNode createTree() {
		TreeNode root = new TreeNode(10);
		root.left = new TreeNode(5);
		root.left.left = new TreeNode(3);
		root.left.right = new TreeNode(8);
		root.right = new TreeNode(20);
		
		return root;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		BinarySearchTreeIterator t = new BinarySearchTreeIterator();
		TreeNode root = t.createTree();
		t.Solution(root);
		
		while (t.hasNext()) 
			System.out.println(t.next());
	}
}
