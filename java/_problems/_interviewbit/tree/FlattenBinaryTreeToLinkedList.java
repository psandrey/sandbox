package _problems._interviewbit.tree;

/*
 * Given a binary tree, flatten it to a linked list in-place (on the right link).
 */

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class FlattenBinaryTreeToLinkedList {

	private class TreeNode {
		TreeNode left, right;
		int val;
		public TreeNode(int v) { val = v; }
	};
	
	public TreeNode flatten(TreeNode n) {
		TreeNode ans = n;
		helperR(n);
		return ans;
	}
	
	// Recursive (in-place) version
	// T(n) = O(n)
	// S(n) = O(n) - because of the recursion
	private TreeNode helperR(TreeNode n) {
		if (n == null || (n.left == null && n.right == null))
			return n;
		
		/*
		 * Note: pre-order traversal: Root first, then left and right
		 */
		
		// if the left is not null, then switch and flatten
		if (n.left != null) {
			// decouple the right branch and move the left branch to the right
			TreeNode t = n.right;
			n.right = n.left;
			n.left = null;

			// continue to flatten
			TreeNode depthMost = helperR(n.right);
			
			// link the right subtree to the depth most element from the left subtree
			depthMost.right = t;
			
			// if the right branch had a subtree... continue flatten that subtree
			//  and return the depth most element to the upper layers
			if (t != null)
				return helperR(t);
			
			// the right branch was empty, so the depth most element is the depth most
			//  element from the left branch
			return depthMost;
		}

		// so, the left branch was empty, then flatten the right branch
		return helperR(n.right);
	}

	// Iterative (in-place) version using a stack
	// T(n) = O(n)
	// S(n) = O(n/2) = O(n) - because of the stack
	private void helperS(TreeNode root) {
		Stack<TreeNode> st = new Stack<TreeNode>();
		
		TreeNode n = root;
		while (n != null || !st.empty()) {
			
			// the children from the right branch are processed
			//  in reverse order (basically, they will be linked in reverse order
			//  on the right most node flatten from the left branch)
			if (n.right != null)
				st.push(n.right);
			
			if (n.left != null) {
				// the right branch was saved in the stack, so it safe
				n.right = n.left;
				n.left = null;
				
			} else if (!st.empty())
				// if there is nothing on the left branch, then link the top 
				//  node (last right branch decoupled) from the stack
				n.right = st.pop();
			
			n = n.right;
		}
	}
	
	// Iterative (in-place) version - no stack
	// T(n) = O(n^2)
	// S(n) = O(1)
	private void helperI(TreeNode n) {
		if (n == null || (n.left == null && n.right == null)) return;

		TreeNode cur = n; // root
		while (cur != null) {
			
			if (cur.left != null) {  
				if (cur.right != null) {
					TreeNode next = cur.left;  
					while (next.right != null) next = next.right;  
					next.right = cur.right;
				}
				
				cur.right = cur.left;
				cur.left = null;
			}
			
			cur = cur.right;
		}
	}

	private void print(TreeNode n) {
		while (n != null) {
			System.out.print(n.val + " ");
			n = n.right;
		}
	}
	
	private TreeNode buildTree() {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(5);
		root.left.left = new TreeNode(3);
		root.left.right = new TreeNode(4);
		root.right.right = new TreeNode(6);
		return root;
	}
	
	private TreeNode buildTreeSerializationTopDown() {
		int[] a = new int[]
				{47, 42, 52, 41, 44, 50, 64, 40, -1, 43, 45, 49, 51, 63, 77, -1, -1, -1, -1, -1,
				 46, 48, -1, -1, -1, 55, -1, 75, 88, -1, -1, -1, -1, 53, 58, 69, 76, 81, 94, -1,
				 54, 56, 60, 68, 73, -1, -1, 79, 87, 92, 100, -1, -1, -1, 57, 59, 61, 66, -1, 72,
				 74, 78, 80, 85, -1, 89, 93, 96, 102, -1, -1, -1, -1, -1, 62, 65, 67, 71, -1, -1,
				 -1, -1, -1, -1, -1, 84, 86, -1, 90, -1, -1, 95, 99, 101, -1, -1, -1, -1, -1, -1,
				 -1, 70, -1, 83, -1, -1, -1, -1, 91, -1, -1, 98, -1, -1, -1, -1, -1, 82, -1, -1, 
				 -1, 97, -1, -1, -1, -1, -1}; 

		TreeNode root = new TreeNode(a[0]);
		int n = a.length;
		
		Queue<TreeNode> q = new LinkedList<TreeNode>();
		q.add(root);
		
		int i = 1;
		int size;
		while (i < n) {

			size = q.size();
			for (int j = 0; j < size; j++) {
				TreeNode node = q.poll();
				if (a[i] != -1) { node.left = new TreeNode(a[i]); q.add(node.left);   }
				i++;
				if (a[i] != -1) { node.right = new TreeNode(a[i]); q.add(node.right); }
				i++;
			}
		}

		return root;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		
		FlattenBinaryTreeToLinkedList t = new FlattenBinaryTreeToLinkedList();
		TreeNode r = t.buildTreeSerializationTopDown();
		TreeNode ans = t.flatten(r);
		t.print(ans);
	}
}
