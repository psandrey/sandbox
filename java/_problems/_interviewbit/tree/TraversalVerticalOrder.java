package _problems._interviewbit.tree;

/*
 * Given a binary tree, return a 2-D array with vertical order traversal of it.
 */

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class TraversalVerticalOrder {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	}
	
	// T(n) = O(n)
	// S(n) = O(n) - because of the ans (however, the Queue is O(logn))
	public ArrayList<ArrayList<Integer>> verticalOrderTraversal(TreeNode root) {
		ArrayList<ArrayList<Integer>> ans = new ArrayList<ArrayList<Integer>>();
		if (root == null) return ans;
		
		// min-max - to compute width
		int[] mm = new int[2];
		getMinMax(root, mm, 0);
		int width = mm[1]-mm[0]+1;
		for(int i = 0; i < width; i++) ans.add(new ArrayList<Integer>());
		int d = Math.abs(mm[0]);
		
		Queue<TreeNode> qn = new LinkedList<TreeNode>();
		Queue<Integer> ql = new LinkedList<Integer>();

		qn.add(root);
		ql.add(0);
		while (!qn.isEmpty()) {
			
			int size = qn.size();
			for (int i = 0; i < size; i++) {
				TreeNode n = qn.poll();
				int level = ql.poll();
				ans.get(level+d).add(n.val);

				if (n.left != null) { qn.add(n.left); ql.add(level-1); }
				if (n.right != null) { qn.add(n.right); ql.add(level+1); }
			}
		}
		
		return ans;		
	}
	
	private void getMinMax(TreeNode n, int[] mm, int level) {
		if (n == null) return;
		
		mm[0] = Math.min(mm[0], level);
		mm[1] = Math.max(mm[1], level);
		getMinMax(n.left, mm, level-1);
		getMinMax(n.right, mm, level+1);
	}
	
	private TreeNode createTree() {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.left.left = new TreeNode(7);
		root.left.right = new TreeNode(4);
		root.right.left = new TreeNode(5);
		root.right.right = new TreeNode(6);
		
		return root;
	}
	
	private void printListList(ArrayList<ArrayList<Integer>> ll) {
		for (ArrayList<Integer> l : ll) {
			System.out.print("[");
			for (Integer x: l) System.out.print(x + ", ");
			System.out.print("]");
			System.out.println();
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		TraversalVerticalOrder t = new TraversalVerticalOrder();
		TreeNode root = t.createTree();
		ArrayList<ArrayList<Integer>> ans = t.verticalOrderTraversal(root);
		t.printListList(ans);
	}
}
