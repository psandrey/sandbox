package _problems._interviewbit.linked_lists;

/*
 * Sort a linked list in O(n.log n) time using constant space complexity.
 */

import java.util.Stack;

public class SortList {

	public class ListNode {
		public int val;
		public ListNode next;
		ListNode(int x) { val = x; next = null; }
	}
	
	public ListNode sortList(ListNode L) {
		// add sentinel
		ListNode santinel = new ListNode(-1);
		santinel.next = L;
		L = santinel;
		
		ListNode Last = L;
		while (Last.next != null) Last = Last.next;
		
		//quickSort(L, Last);
		quickSortI(L, Last);
		
		// remove sentinel
		L = L.next;
		return L;
	}
	
	// quickSort (P(D)arent, Last) - recursive
	private void quickSort(ListNode D, ListNode L) {
		if (D == L || D.next == L) return;
		
		// the last node can change (if it is smaller than P)
		ListNode[] nodes = partition(D, L);
		ListNode P = nodes[0];
		L = nodes[1];

		// no matter what happens here the relation holds: D < P < L
		if (P != D) {
			quickSort(D, P);
			quickSort(P, L);
		} else
			quickSort(D.next, L);
	}
	
	// quickSort (P(D)arent, Last) - iterative
	private void quickSortI(ListNode D, ListNode L) {
		Stack<ListNode> stD = new Stack<>();
		Stack<ListNode> stL = new Stack<>();
		
		stD.push(D); stL.push(L);
		while (!stD.isEmpty() && !stL.isEmpty()) {
			ListNode d = stD.pop();
			ListNode l = stL.pop();
			
			// the last node can change (if it is smaller than P)
			ListNode[] nodes = partition(d, l);
			ListNode p = nodes[0];
			l = nodes[1];
			
			// if we have two halves
			if (p != d) {

				// if we have something to partition on the left of the pivot
				if (d.next != p) { stD.push(d); stL.push(p); }

				// if we have something to partition on the right of the pivot
				if (p != l && p.next != l) { stD.push(p); stL.push(l); }

			} else if (d.next != l && d.next.next != l) {
					// all the elements on the right of the pivot are greater than the pivot, but
					//  we do not know anything about the next element, so advance one and if there 
					//  are more than 1 element left, then partition...
					stD.push(d.next); stL.push(l);
			}
		}
	}
	
	private ListNode[] partition(ListNode D, ListNode L) {
		ListNode p = D;
		ListNode c = p.next;
		while (c != L) {
			if (c.next.val < p.next.val) {
				ListNode t = c.next;
				c.next = c.next.next;
				t.next = p.next;
				p.next = t;
				p = t;
				if (t == L) { L = c; break; }
				else continue;
			}
			
			c = c.next;
		}
		
		return new ListNode[]{p, L};
	}
	
	
	ListNode buildList() {
		ListNode L = new ListNode(3);
		L.next = new ListNode(2);
		//L.next.next = new ListNode(4);
		//L.next.next.next = new ListNode(1);
		//L.next.next.next.next = new ListNode(0);
		//L.next.next.next.next.next = new ListNode(7);
		return L;
	}
	
	void printList(ListNode L) {
		ListNode n = L;
		while (n != null) {
			System.out.print(n.val + ", ");
			n = n.next;
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		SortList t = new SortList();
		ListNode L = t.buildList();
		ListNode newL = t.sortList(L);
		t.printList(newL);	
	}	
}
