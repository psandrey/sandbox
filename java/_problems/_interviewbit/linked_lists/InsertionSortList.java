package _problems._interviewbit.linked_lists;

/*
 * Sort a linked list using insertion sort.
 */

public class InsertionSortList {

	public class ListNode {
		public int val;
		public ListNode next;
		ListNode(int x) { val = x; next = null; }
	}
	
	public ListNode insertionSortList(ListNode L) {
		// add sentinel
		ListNode santinel = new ListNode(-1);
		santinel.next = L;
		L = santinel;
		
		ListNode c = L;
		ListNode l = L.next;
		ListNode s = l.next;
		while (s != null) {
			while (c.next != s && c.next.val < s.val) c = c.next;
			
			if (c.next != s) {
				l.next = l.next.next;
				s.next = c.next;
				c.next = s;
			} else
				l = s;

			s = l.next;
			c = L;
		}
		
		// remove sentinel
		L = L.next;
		return L;
	}
	
	ListNode buildList() {
		ListNode L = new ListNode(1);
		L.next = new ListNode(2);
		L.next.next = new ListNode(3);
		L.next.next.next = new ListNode(0);
		return L;
	}
	
	void printList(ListNode L) {
		ListNode n = L;
		while (n != null) {
			System.out.print(n.val + ", ");
			n = n.next;
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		InsertionSortList t = new InsertionSortList();
		ListNode L = t.buildList();
		ListNode newL = t.insertionSortList(L);
		t.printList(newL);	
	}
}
