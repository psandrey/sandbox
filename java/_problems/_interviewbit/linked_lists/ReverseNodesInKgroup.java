package _problems._interviewbit.linked_lists;

/*
 * Given a linked list, reverse the nodes of a linked list k at a time
 *  and return its modified list. If the number of nodes is not a multiple of k
 *  then left-out nodes in the end should remain as it is.
 *  
 *  You may not alter the values in the nodes, only nodes itself may be changed.
 */

public class ReverseNodesInKgroup {
	
	private class Node {
		Node next;
		int data;
		public Node(int val) {
			data = val;
			next = null;
		}
	};

	private Node reverseNodesInKGroup(Node head, int k, boolean recVer) {
		Node L = new Node(0);
		L.next = head;
		Node preH = L;
		Node t = preH.next;
		while (t != null) {
			t = getNextT(t, k);
			if (t == null)
				return L.next;
			
			Node h = preH.next;
			Node rest = null;
			if (recVer)
				rest = reverseRecHTGroup(h, t);
			else
				rest = reverseItHTGroup(h, t);
			preH.next = rest;
			preH = h;
			t = preH.next;			
			printList(L.next);
		}

		return L.next;
	}

	private Node getNextT(Node t, int k) {
		k--;
		while (t != null && k > 0) {
			k--;
			t = t.next;
		}
		return (k == 0 ? t : null);
	}
	
	private Node reverseRecHTGroup(Node h, Node t) {
		if (h == t)
			return h;
		
		Node s = h.next;
		h.next = t.next;
		Node rest = reverseRecHTGroup(s, t);
		s.next = h;
		return rest;
	}
	
	private Node reverseItHTGroup(Node head, Node tail) {
		if (head == tail)
			return head;
		Node last = tail.next;

		Node h = head;
		Node p = h.next;
		h.next = last;
		while (p != last) {
			Node t = p.next;
			p.next = h;
			h = p;
			p = t;
		}

		return h;
	}
	
	private Node buildList(int n) {
		Node head = new Node(0);
		Node pre = head;
		for (int i = 1; i < n; i++) {
			Node node = new Node(i);
			pre.next = node;
			pre = node;
		}

		return head;
	}
	
	private void printList(Node l) {
		System.out.print(" [ ");
		Node n = l;
		while (n != null) {
			System.out.print(n.data + ",");
			n = n.next;
		}
		System.out.print(" ] ");
		System.out.println();	
	}
	
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		ReverseNodesInKgroup t = new ReverseNodesInKgroup();
		int n = 5;
		Node l = t.buildList(n);
		t.printList(l);
		Node h = t.reverseNodesInKGroup(l, 2, false);
		t.printList(h);
	}
}
