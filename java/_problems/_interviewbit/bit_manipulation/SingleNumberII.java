package _problems._interviewbit.bit_manipulation;

/*
 * Given an array of integers, every element appears thrice except for one which occurs once.
 * Find that element which does not appear thrice.
 */

public class SingleNumberII {

	public int singleNumber(final int[] a) {
		if (a == null || a.length == 0) return 0;
		
		int ans = 0;
		int n = a.length;
		for (int b = 0; b < 32; b++) {
			int mask = (1<<b);
			long count = 0;
			for (int i = 0; i < n; i++)
				if ( (mask & a[i]) == mask) count++;
			
			ans |= (count%3L == 0L ? 0 : mask);
		}
		
		return ans;
	}
	
	public int singleNumberV2(final int[] a) {
		int n = a.length;
		int first = 0;
		int second = 0;
		for(int i = 0; i < n; i++){
			// Set the bits to first, if the bits were already set,
            // they are going to get toggled, then we check if they aren't
            // in the second variable by doign a negation of the variable and
            // an and
			first = (first ^ a[i]) & ~second;
			// We do another xor in case the first one toggled the bits, and
            // After that we need to check that we don't have any bits set in 
            // the first bitset.
			second = (second ^ a[i]) & ~first;
		}
		return first;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = {1, 2, 4, 3, 3, 2, 2, 3, 1, 1};
		SingleNumberII t = new SingleNumberII();
		System.out.println(t.singleNumber(a));
		System.out.println(t.singleNumberV2(a));
	}
}
