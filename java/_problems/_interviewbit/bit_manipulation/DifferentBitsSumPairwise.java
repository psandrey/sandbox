package _problems._interviewbit.bit_manipulation;

/*
 * We define f(X, Y) as number of different corresponding bits in binary representation of X and Y.
 * For example, f(2, 7) = 2, since binary representation of 2 and 7 are 010 and 111, respectively.
 * The first and the third bit differ, so f(2, 7) = 2.
 * 
 * You are given an array of N positive integers, A1, A2 ,�, AN.
 * Find sum of f(Ai, Aj) for all pairs (i, j) such that 1 <= i, j <= N.
 * Return the answer modulo 109+7.
 */

public class DifferentBitsSumPairwise {

	public int cntBits(int[] a) {
		if (a == null || a.length == 0) return 0;
		
		long ans = 0;
		long m = 1000000007;
		long n = (long)a.length;
		for (int b = 0; b < 32; b++) {
			int mask = (1<<b);
			long count = 0;
			for (int i = 0; i < n; i++)
				if ( (mask & a[i]) == mask) count++;
			
			if (count > 0) ans = ans%m + (2L*count*(n-count))%m;
		}
		
		return (int)(ans%m);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = new int[] {81, 13, 2, 7, 96};
		
		DifferentBitsSumPairwise t = new DifferentBitsSumPairwise();
		int ans = t.cntBits(a);
		System.out.println(ans);
	}
}
