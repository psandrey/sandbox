package _problems._leetcode.dynamic_programming;

/*
 * Given a string s, partition s such that every substring of the partition is a palindrome.
 * Return all possible palindrome partitioning of s.
 * 
 * For example, given s = "aab":
 *     ["aa","b"],
 *     ["a","a","b"]
 */

import java.util.ArrayList;
import java.util.Stack;

public class PalindromePartitioning {

	private ArrayList<String> palindromePartitioning(String s) {
		ArrayList<String> palindromesList = new ArrayList<String>();
		if (s == null || s.length() == 0)
			return palindromesList;

		int n = s.length();
		int[][] dp = new int[n][n];

		for (int len = 0; len < n; len++)
			for (int i = 0; i < n-len; i++) {
				int j = i + len;
				if (s.charAt(i) == s.charAt(j))
					if (j - i <= 2 || dp[i+1][j-1] == 1) {
						dp[i][j] = 1;
						palindromesList.add(s.substring(i, j+1));
					}
			}

		return palindromesList;
	}
	
	private ArrayList<ArrayList<String>> partition (String s) {
		ArrayList<ArrayList<String>> listsOfPalindromesPartitions = new ArrayList<ArrayList<String>>();
		if (s == null || s.length() == 0)
			return listsOfPalindromesPartitions;

		Stack<String> stack = new Stack<String>();
		dfs(s, stack, listsOfPalindromesPartitions);
		return listsOfPalindromesPartitions;
	}

	private void dfs(String a, Stack<String> stack, ArrayList<ArrayList<String>> listsOfPalindromesPartitions) {
		if (a == null || a.length() == 0) {
			listsOfPalindromesPartitions.add(new ArrayList<String>(stack));
			return;
		}

		int n = a.length();
		for (int i = 1; i <= n; i++) {
			String left = a.substring(0, i);
			if (isPalindrome(left)) {
				stack.push(left);
				String right = a.substring(i, n);
				dfs(right, stack, listsOfPalindromesPartitions);
				stack.pop();
			}
		}
	}

	private boolean isPalindrome(String s) {
		int i = 0;
		int j = s.length() - 1;
		while (i < j) {
			if (s.charAt(i) != s.charAt(j))
				return false;
			i++; j--;
		}

		return true;
	}

	private void printListOfLists(ArrayList<ArrayList<String>> lists) {
		for (ArrayList<String> list:lists)
			printList(list);
	}
	
	private void printList(ArrayList<String> list) {
		System.out.print("[");
		
		for (int i = 0; i < list.size(); i++)
			System.out.print(list.get(i) + ",");

		System.out.print("]");
		System.out.println();
	}

/*
	private void printString(String a, int spaces) {
		String spaceDepth = "";
		if (spaces != 0) {
			String format = "%" + String.valueOf(spaces*4) +"s";
			spaceDepth = String.format(format, " ");
		}
		System.out.print(spaceDepth + "+" + a);
		System.out.println();
	}
*/

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		String s = "aab";
		
		PalindromePartitioning t = new PalindromePartitioning();
		ArrayList<ArrayList<String>> lists = t.partition(s);
		t.printListOfLists(lists);
		
		ArrayList<String> list = t.palindromePartitioning(s);
		System.out.println();
		t.printList(list);
		
	}
}
