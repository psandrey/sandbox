package _problems._leetcode.dynamic_programming;

/*
 * Write an efficient program to find the sum of contiguous subarray within
 *  a one-dimensional array of numbers which has the largest sum.
 */
public class LargestSumContiguousSubarray {

	private int maxSubarraySum(int[] a) {
		if (a == null || a.length == 0)
			return -1;
		int n = a.length;
		int[] dp = new int[n];
		
		dp[0] = a[0];
		int max = dp[0];
		for (int i = 1; i < n; i++) {
			dp[i] = Math.max(dp[i-1] + a[i], a[i]);
			max = Math.max(max, dp[i]);
		}
		
		return max;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		//int[] a = {-2, -3, 4, -1, -2, 1, 5, -3};
		//int[] a = {10, 20, 30, 40};
		//int[] a = {-10, -20, -30, -40};
		int[] a = {-10, 2, 3, -1, 2, -3};
		LargestSumContiguousSubarray t = new LargestSumContiguousSubarray();
		int max = t.maxSubarraySum(a);
		System.out.println(max);
	}
}
