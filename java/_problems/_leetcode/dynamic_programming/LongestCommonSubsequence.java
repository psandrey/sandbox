package _problems._leetcode.dynamic_programming;
/*
 * The longest common subsequence (LCS) problem is the problem of finding the
 *  longest subsequence common to two sequences.
 */

import java.util.Arrays;

public class LongestCommonSubsequence {
	
	private int LCS_(int[] v, int[] w) {
		if (v == null || w == null || v.length == 0 || w.length == 0)
			return -1;

		int n = v.length;
		int m = w.length;

		int[][] dp = new int[n][m];
		for (int i = 1; i < n; i++)
			for (int j = 1; j < m; j++) {
				dp[i][j] = (v[i] == w[j]) ? dp[i-1][j-1] + 1: Math.max(dp[i-1][j], dp[i][j-1]);
			}
		return dp[n-1][m-1] + 1;
	}
	
	// T(n+m) =  T(n*m)
	// Note: Works only if there are no duplicates!!!
	private int LCS(int[] v, int[] w) {
		if (v == null || w == null || v.length == 0 || w.length == 0)
			return -1;
		int n = v.length;
		int m = w.length;
		if (m < n)
			return LCS(w, v);

		int[] pred = new int [n];
		Arrays.fill(pred, -1);
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				if (v[i] == w[j])
					pred[i] = j;
		
		// find the longest ascending sub-array => LCS(v, w);
		int[] dp = new int[n];
		int lenMax = 0;
		for (int i = 0; i < n; i++) {
			dp[i] = 1;
			for (int j = 0; j < i; j++) {
				if (pred[i] > pred[j])
					dp[i] = Math.max(dp[i], dp[j] + 1);
			}
			lenMax = Math.max(lenMax, dp[i]);
		}

		return lenMax;
	}
	
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		//int[] v = {6, -1, 9};
		//int[] w = {0, 6, 2, 9, 8};
		
		int[] v = {2, 1, 5, 3, 4, 5, 2, 7};
		int[] w = {1, 5, 5, 7, 4};
		
		int max = 0;
		LongestCommonSubsequence t = new LongestCommonSubsequence();
		max = t.LCS(v, w);
		System.out.println(max);
		max = t.LCS_(v, w);
		System.out.println(max);
	}
}
