package _problems._leetcode.dynamic_programming;

/*
 * Find the longest palindromic substring.
 */

public class LongestPalindromicSubstring {

	private String longestPalindrome(String s) {
		String palindrome = "";
		int n = s.length();
		int[][] dp = new int[n][n];

		int maxLen = 0;
		for (int len = 0; len < n; len++)
			for (int i = 0; i < n-len; i++) {
				int j = i + len;
				if (s.charAt(i) == s.charAt(j))
					if (j - i <= 2 || dp[i+1][j-1] == 1) {
						dp[i][j] = 1;

						if (maxLen < j-i) {
							maxLen = j - i;
							palindrome = s.substring(i, j+1);
						}
					}
			}

		return palindrome;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		//String s = "aab";
		String s = "dabcba";
		LongestPalindromicSubstring t = new LongestPalindromicSubstring();
		String l = t.longestPalindrome(s);
		System.out.println(l);
	}
}
