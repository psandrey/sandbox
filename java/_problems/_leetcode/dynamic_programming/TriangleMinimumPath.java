package _problems._leetcode.dynamic_programming;

/*
 * Given a triangle, find the minimum path sum from top to bottom.
 * Each step you may move to adjacent numbers on the row below.
 * 
 * For example, given the following triangle
 *      [2],
 *     [3,4],
 *    [6,5,7],
 *   [4,1,8,3]
 * The minimum path sum from top to bottom is 11 (i.e., 2 + 3 + 5 + 1 = 11).
 */

import java.util.ArrayList;
import java.util.Arrays;

public class TriangleMinimumPath {

	private int minimumTotal(ArrayList<ArrayList<Integer>> triangle) {
		if (triangle == null || triangle.size() == 0)
			return 0;

		int[] dp = new int[triangle.size()];
		
		int baseLine = triangle.size()-1;
		int baseLineSize = triangle.get(baseLine).size();
		for (int i = 0; i < baseLineSize; i++) // baseLineSize is the same as triangle.size()-1
			dp[i] = triangle.get(baseLine).get(i);
		
		for (int i = triangle.size() - 2; i >= 0; i--)
			for (int j = 0; j < triangle.get(i+1).size() - 1; j++) {
				dp[j] = triangle.get(i).get(j) + Math.min(dp[j], dp[j+1]);
			}

		return dp[0];
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		ArrayList<ArrayList<Integer>> triangle = new ArrayList<>();
		triangle.add(new ArrayList<Integer>( Arrays.asList(2) ));
		triangle.add(new ArrayList<Integer>( Arrays.asList(3,4) ));
		triangle.add(new ArrayList<Integer>( Arrays.asList(6,5,7) ));
		triangle.add(new ArrayList<Integer>( Arrays.asList(4,1,8,3) ));

		TriangleMinimumPath t = new TriangleMinimumPath();
		int pathLen = t.minimumTotal(triangle);
		System.out.println(pathLen);
	}
}
