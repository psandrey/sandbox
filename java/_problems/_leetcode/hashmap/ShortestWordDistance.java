package _problems._leetcode.hashmap;

/*
 * Design a class which receives a list of words in the constructor,
 * and implements a method that takes two words word1 and word2 and return
 * the shortest distance between these two words in the list.
 */

import java.util.ArrayList;
import java.util.HashMap;

public class ShortestWordDistance {

	private HashMap<String, ArrayList<Integer>> hMP;

	private ShortestWordDistance(String[] ws) {
		hMP = new HashMap<String, ArrayList<Integer>>();
		for (int i = 0; i < ws.length; i++) {
			if (hMP.containsKey(ws[i])) {
				ArrayList<Integer> l = hMP.get(ws[i]);
				l.add(i);
			} else {
				ArrayList<Integer> l = new ArrayList<Integer>();
				l.add(i);
				hMP.put(ws[i], l);
			}
		}
	}
	
	private int do_shortestWordDistance(String w1, String w2) {
		ArrayList<Integer> l1 = hMP.get(w1);
		ArrayList<Integer> l2 = hMP.get(w2);
 
		int i = 0, j = 0, result = Integer.MAX_VALUE;
		while(i<l1.size() && j<l2.size()){
			result = Math.min(result, Math.abs(l1.get(i)-l2.get(j)));

			if(l1.get(i)<l2.get(j)) i++;
			else j++;
		}

		return result;
	}

	private void print(String s) {
		System.out.println(s);
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		String[] words = { "practice", "coding", "makes", "coding", "makes" };
		ShortestWordDistance t = new ShortestWordDistance(words);
		int dist = t.do_shortestWordDistance("practice", "makes");
		t.print(String.valueOf(dist));
	}
}
