package _problems._leetcode.hashmap;

/* https://www.hackerearth.com/practice/data-structures/hash-tables/basics-of-hash-tables/practice-problems/algorithm/e-16/description/
 * 
 * Let an array of N integers. 
 * Find all the possible subsequences of 1 to K which can be formed from the given array.
 * The subsequence should contain all the elements from  1,2,3...K. Here K can be any integer.
 * K can be different for different subsequences. Each element of the array should belong to
 * exactly one subsequence. If it possible to do so then print the number of subsequence
 * and in next line print all the subsequence in increasing order of their length.
 * If not then print only the minimum numbers to add to the array such that it is possible to
 * assign each element to a subsequence.
 *
 * For e.g. :
 * 5
 * 1 3 3 4 5
 *
 * Now here it is not possible to assign each number to a subsequence.
 * If we add elements 1,2,2  to the array then it is possible.
 *
 * 1 2 3 
 * 1 2 3 4 5
 *
 * So the minimum count of numbers required is 3.
 */

import java.util.HashMap;

public class FindSubsequence {

	private int nLists = 0;

	private class subSeq {
		public int limit;
		public int need;
		public HashMap<Integer, Integer> mp;

		public subSeq(int val) {
			mp = new HashMap<Integer, Integer>();
			mp.put(val, 1);
			limit = val;
			
			// need all numbers from (val-1) to 1, so:
			need = val - 1;
		}
	}

	private HashMap<Integer, subSeq> seq = new HashMap<Integer, subSeq>();

	private void updateList(int listIndex, int val) {
		subSeq subS = null;
		if (seq.containsKey(listIndex)) {
			subS = seq.get(listIndex);
			if (val > subS.limit) {
				subS.need += val - subS.limit - 1;
				subS.limit = val;
			} else
				subS.need -= 1;

			subS.mp.put(val, 1);
		} else
			subS = new subSeq(val);

		seq.put(listIndex, subS);
	}

	private void doFindSubsequence(int[] a) {
		HashMap<Integer, Integer> mp = new HashMap<Integer, Integer>();
		
		for (int i = 0; i < a.length; i++) {

			int listIndex = 0;
			if (mp.containsKey(a[i])) {
				listIndex = mp.get(a[i]);
				mp.put(a[i], ++listIndex);
			} else
				mp.put(a[i], listIndex);

			nLists = Math.max(nLists, listIndex + 1);
			updateList(listIndex, a[i]);
		}
	
		printSeq();
	}

	private void printSeq() {
		for (int i = 0; i < nLists; i++) {
			subSeq subS = seq.get(i);
			
			System.out.println(" + List index:" + i);
			System.out.println("    - Needs:" + subS.need);
			System.out.println("    - limit:" + subS.limit);
			System.out.print("    - contains:");
			for (int k = 1; k <= subS.limit; k++) {
				if (subS.mp.containsKey(k))
					System.out.print(k + ",");
				else
					System.out.print("x,");
			}
			System.out.println();
		}
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		int[] arr = {3, 1, 3, 4, 5};
		FindSubsequence t = new FindSubsequence();
		t.doFindSubsequence(arr);
	}
}
