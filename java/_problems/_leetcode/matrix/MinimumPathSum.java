package _problems._leetcode.matrix;

/*
 * Given a m x n grid filled with non-negative numbers,
 *  find a path from top left to bottom right which minimizes the sum
 *  of all numbers along its path.
 */

import java.util.Stack;

public class MinimumPathSum {

	// dynamic programming
	private int minPathSumDP(int[][] a) {
		int n = a.length;
		int m = a[0].length;
		
		int[][] dp = new int[n][m];
		dp[0][0] = a[0][0];
		for (int j = 1; j < m; j++)
			dp[0][j] = a[0][j] + dp[0][j-1];

		for (int i = 1; i < n; i++)
			for (int j = 0; j < m; j++) {
				if (j > 0)
					dp[i][j] = a[i][j] + Math.min(dp[i-1][j], dp[i][j-1]);
				else
					dp[i][j] = a[i][j] + dp[i-1][j];
			}

		return dp[n-1][m-1];
	}
	
	// depth first search
	private int minPathSumDFS(int[][] a) {
		return dfs(a, 0, 0);
	}
	
	private int dfs(int[][] a, int i, int j) {
		if (i == a.length - 1 && j == a[0].length - 1)
			return a[i][j];
		
		if  (i < a.length - 1 && j < a[0].length - 1) {
			int r = a[i][j] + dfs(a, i, j + 1);
			int d = a[i][j] + dfs(a, i + 1, j);
			return Math.min(r,  d);
		}
		
		if (j < a[0].length - 1)
			return a[i][j] + dfs(a, i, j + 1);
		
		if (i < a.length - 1)
			return a[i][j] + dfs(a, i + 1, j);
		
		return 0;
	}
	
	// standard iterative backtracking
	private final int R = 0;
	private final int D = 1 ;
	private class Item {
		int l;
		int c;
		int dir;
		int sum;
		public Item(int l, int c, int d, int s) {
			this.l = l;
			this.c = c;
			this.dir = d;
			this.sum = s;
		}
	};
	
	private int minPathSumBck(int[][] a){
		boolean isSucc = false;
		Stack<Item> st = new Stack<Item>();		
		int min = Integer.MAX_VALUE;

		initStack(a, st);
		while (!st.empty()) {
			do {
				isSucc = successor(a, st);
			} while (isSucc == true && !isValid(a, st));
			
			if (isSucc == true) {
				nextValidMove(a, st);

				if (isSolution(a, st))
					min = Math.min(min, st.peek().sum);
			} else
				st.pop();
		}
		
		return min;
	}

	private void initStack(int[][] a, Stack<Item> st) {
		st.add(new Item(0, 0, -1, a[0][0]));
	}
	
	private boolean successor(int[][] a, Stack<Item> st) {
		Item top = st.peek();
		if (top.dir == D)
			return false;

		if (top.dir == -1)
			top.dir = R;
		else
			top.dir = D;
		st.pop();
		st.add(top);

		return true;
	}

	private boolean isValid(int[][] a, Stack<Item> st) {
		int n = a.length;
		int m = a[0].length;
		Item top = st.peek();
		if (top.dir == R && top.c == m - 1)
			return false;
		if (top.dir == D && top.l == n - 1)
			return false;
		return true;
	}
	
	private boolean isSolution(int[][] a, Stack<Item> st) {
		int n = a.length;
		int m = a[0].length;
		Item top = st.peek();
		if (top.l == n - 1 && top.c == m - 1)
			return true;
		return false;
	}
	
	private void nextValidMove(int[][] a, Stack<Item> st) {
		Item top = st.peek();

		Item next = null;
		if (top.dir == R)
			next = new Item(top.l, top.c + 1, -1, top.sum + a[top.l][top.c + 1]);
		else
			next = new Item(top.l + 1, top.c, -1, top.sum + a[top.l+1][top.c]);

		st.add(next);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int grid[][] = {
				{1, 1, 10, 2},
				{2, 10, 10, 2},
				{1, 1, 1, 2}};

		MinimumPathSum t = new MinimumPathSum();
		int min = 0;
		min = t.minPathSumBck(grid);
		System.out.println("Bck min: " + min);
		
		min = t.minPathSumDFS(grid);
		System.out.println("DFS min: " + min);
		
		min = t.minPathSumDP(grid);
		System.out.println("DP min: " + min);
	}	
}
