package _problems._leetcode.matrix;

/*
 * A robot is located at the top-left corner of a m x n grid.
 * It can only move either down or right at any point in time.
 * The robot is trying to reach the bottom-right corner of the grid.
 * How many possible unique paths are there?
 */

public class UniquePaths {

	// dynamic programming
	private int uniquePathsDP(int n, int m) {
		int[][] dp = new int[n][m];

		for (int i = 0; i < m; i++)
			dp[0][i] = 1;
		for (int i = 0; i < n; i++)
			dp[i][0] = 1;
		
		for (int i = 1; i < n; i++)
			for (int j = 1; j < m; j++) {
				dp[i][j] = dp[i-1][j] + dp[i][j-1];
			}

		return dp[n-1][m-1];
	}
	
	// DFS
	private int uniquePathsDFS(int n, int m) {
		return dfs(0,0,m,n);
	}

	public int dfs(int i, int j, int m, int n){
		if (i == m-1 && j == n-1)
			return 1;

		if (i < m-1 && j < n-1)
			return dfs(i+1, j, m, n) + dfs(i, j+1, m, n);
	 
		if (i < m-1)
			return dfs(i+1, j, m, n);
	 
		if (j < n-1)
			return dfs(i, j+1, m, n);
	 
		return 0;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		int m = 3;
		int n = 3;
		UniquePaths t = new UniquePaths();
		int paths = 0;
		
		paths = t.uniquePathsDFS(m, n);
		System.out.println("DFS unique paths:" + paths);		
		
		paths = t.uniquePathsDP(m, n);
		System.out.println("DP unique paths:" + paths);
	}
}
