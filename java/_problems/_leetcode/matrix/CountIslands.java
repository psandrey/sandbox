package _problems._leetcode.matrix;

/*
 * Given a 2-d grid map of '1's (land) and '0's (water), count the number of islands.
 * An island is surrounded by water and is formed by connecting adjacent lands
 * horizontally or vertically. You may assume all four edges of the grid are all
 * surrounded by water.
 */

import java.util.Hashtable;

public class CountIslands {
	
	// Disjoint sets version (no compression)
	private int countDS_(int[][] a) {
		int cc = 0;
		if (a == null || a.length == 0 || a[0].length == 0)
			return cc;
		
		int n = a.length;
		int m = a[0].length;
		int[] dsRoot = new int[n*m];
		for (int i = 0; i < n; i++)
			for( int j = 0; j < m; j++) {
				if (a[i][j] == 0)
					continue;
				
				int u = i*m + j;
				dsRoot[u] = u;
				cc++;
			}

		//		  W   N   S  E
		int[] dx = {-1, 1,  0, 0};
		int[] dy = { 0, 0, -1, 1};
		for (int i = 0; i < n; i++) {
			for( int j = 0; j < m; j++) {
				if (a[i][j] == 0)
					continue;

				int u = i*m + j;
				
				// neighbors: west, north, south and est 
				for (int d = 0; d < 4; d++) {
					int ii = i + dx[d];
					int jj = j + dy[d];
										
					if(ii >= 0 && ii < n && jj >= 0 && jj < m && a[ii][jj] == 1){
						int v = ii*m + jj;
					
						int cRoot = getRoot(dsRoot, u);
						int nRoot = getRoot(dsRoot, v);

						if (nRoot != cRoot) {
							dsRoot[cRoot] = nRoot;
							cc--;
						}
					}
				} // neighbors
			}
		}
		
		return cc;
	}
	
	public int getRoot(int[] a , int i){
		while (a[i] != i)
			i = a[a[i]];
	 
		return i;
	}

	// Disjoint sets version (with path compression)
	private class DisjointSet<E extends Comparable<E>> {
		public Hashtable<E, E> parent;
		public Hashtable<E, Integer> rank;

		public DisjointSet() {
			parent = new Hashtable<E, E>();
			rank = new Hashtable<E, Integer>();
		}
		
		public void newSet(E u) {
			parent.put(u, u);
			rank.put(u, 0);
		}
		
		public E find(E v) {
			if (parent.containsKey(v) == false)
				return null;

			E pV = parent.get(v);
			if (v.compareTo(pV) != 0) {
				E p = find(pV);
				parent.put(v, p);
			}

			return parent.get(v);
		}

		public void union(E u, E v) {
			E uId = find(u);
			E vId = find(v);
			Integer rU = rank.get(uId);
			Integer rV = rank.get(vId);
			
			if (rU > rV)
				parent.put(vId, uId);
			else {
				parent.put(uId, vId);
				if (rU == rV)
					rank.put(vId, rU + 1);
			}
		}
	};
	
	private int countDS(int[][] a) {
		int cc = 0;
		if (a == null || a.length == 0 || a[0].length == 0)
			return cc;
		
		DisjointSet<Integer> ds = new DisjointSet<Integer>();

		//		  W   N   S  E
		int[] dx = {-1, 1,  0, 0};
		int[] dy = { 0, 0, -1, 1};
		int n = a.length;
		int m = a[0].length;
		for (int i = 0; i < n; i++) {
			for( int j = 0; j < m; j++) {
				if (a[i][j] == 0)
					continue;
			
				int u = i*m + j;
				if (ds.find(u) == null) {
					ds.newSet(u);
					cc++;
				}
				
				// neighbors: west, north, south and est 
				for (int d = 0; d < 4; d++) {
					int ii = i + dx[d];
					int jj = j + dy[d];
					
					if(ii >= 0 && ii < n && jj >= 0 && jj < m && a[ii][jj] == 1){
						int v = ii*m + jj;
						if (ds.find(v) == null) {
							ds.newSet(v);
							cc++;
						}
						
						if (ds.find(u) != ds.find(v)) {
							ds.union(u, v);
							cc--;
						}
					}
				} // neighbors
			}
		}
	
		return cc;
	}
	
	// Using connected components: DFS
	private int countDFS(int[][] a) {
		if (a == null || a.length == 0 || a[0].length == 0)
			return 0;
		
		return connectedComponents(a);
	}
	
	private int connectedComponents(int[][] a) {
		int cc = 0;
		int n = a.length;
		int m = a[0].length;
		int[][] marked = new int[n][m];
		for (int i =0; i < n; i++)
			for (int j = 0; j < m; j++)
				if (a[i][j] == 1 && marked[i][j] == 0) {
					cc++;
					DFS(a, marked, i, j);
				}

		return cc;
	}
	
	private void DFS(int[][] a, int[][] marked, int i, int j) {
		int n = a.length;
		int m = a[0].length;

		if (i < 0 || i >= n || j < 0 || j >= m)
			return;
		if (a[i][j] == 0 || marked[i][j] == 1)
			return;
		
		marked[i][j] = 1;
		DFS(a, marked, i - 1, j);
		DFS(a, marked, i + 1, j);
		DFS(a, marked, i,     j - 1);
		DFS(a, marked, i,     j + 1);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int grid[][] = {
				{0, 0, 0, 0},
				{0, 1, 0, 1},
				{0, 1, 1, 1},
				{0, 0, 0, 0},
				{0, 1, 1, 0}};
		
		CountIslands t = new CountIslands();
		int cc = 0;
		
		cc = t.countDFS(grid);
		System.out.println("DFS: number of islands:" + cc);

		cc = t.countDS(grid);
		System.out.println("DS (with compression): number of islands:" + cc);

		cc = t.countDS_(grid);
		System.out.println("DS (no compression): number of islands:" + cc);
	}
}
