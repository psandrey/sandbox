package _problems._leetcode.matrix;

/*
 * Given a matrix of m x n elements (m rows, n columns),
 * return all elements of the matrix in spiral order.
 */

import java.util.LinkedList;

public class SpiralMatrix {

	private LinkedList<Integer> spiralOrder(int[][] a) {
		LinkedList<Integer> result = new LinkedList<Integer>();
		if(a == null || a.length == 0 || (a.length == 0 && a[0].length == 0))
			return result;
		
		spiralOrder(result, a, 0, a[0].length - 1, 0, a.length - 1);
		return result;
	}
	
	private void spiralOrder(LinkedList<Integer> result,
			int[][] a, int sL, int eL, int sC, int eC) {

		if (sL > eL || sC > eC)
			return;
		
		// one line or one col left
		if (sL == eL) {
			for(int i = sC; i <= eC; i++)
				result.add(a[sL][i]);			
			return;
		}
		
		if (sC == eC) {
			for(int i = sL+1; i <= eL-1; i++)
				result.add(a[i][eC]);
			return;
		}
		
		// upper line
		for(int i = sC; i <= eC; i++)
			result.add(a[sL][i]);

		// right col
		for(int i = sL+1; i <= eL-1; i++)
			result.add(a[i][eC]);
		
		//bottom line
		for(int i = eC; i >= sC; i--)
			result.add(a[eL][i]);
		
		//left col
		for(int i = eL-1; i >= sL+1; i--)
			result.add(a[i][sL]);
		
		spiralOrder(result, a, sL+1, eL-1, sC+1, eC-1);
	}
	
	private void printLL(LinkedList<Integer> a) {
		int n = a.size();
		System.out.print("[ ");
		for (int i = 0; i < n; i++)
			System.out.print(a.get(i) + ",");
		System.out.print(" ]");
		System.out.println();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[][] a = new int[][]
			{{1, 2, 3},
			 {4, 5, 6},
			 {7, 8, 9}};
			
		SpiralMatrix t = new SpiralMatrix();
		LinkedList<Integer> list = t.spiralOrder(a);
		t.printLL(list);
	}
}
