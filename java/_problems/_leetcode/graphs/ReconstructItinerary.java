package _problems._leetcode.graphs;

/*
 * Given a list of airline tickets represented by pairs of departure and arrival airports [from, to],
 * reconstruct the itinerary in order. All of the tickets belong to a man who departs from JFK.
 * Thus, the itinerary must begin with JFK.
 * 
 * Note: The itinerary must be in lexicographic form, thus we need a PriorityQueue instead
 *       of an ArrayList or classic queue. (also, we cannot use a TreeSet because we can have
 *       duplicate locations). It can be loops (i.e. the guy can go back to the a previous location).
 *       
 * Note: The following version uses Hierholzer's algorithm (to find Eulerian path).
 * Note: The graph must have a source and a sink node and all the other nodes must have the
 *       in-degree = out-degree, otherwise the graph doesn't have an Eulerian path.
 */

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

public class ReconstructItinerary {

	private List<String> findItinerary(String[][] tickets) {
		
		int n = tickets.length;
		HashMap<String, PriorityQueue<String>> adjL = new HashMap<String, PriorityQueue<String>>();
		for (int u = 0; u < n; u++) {
			PriorityQueue<String> list = null;

			if (!adjL.containsKey(tickets[u][0])) {
				list = new PriorityQueue<String>();
				adjL.put(tickets[u][0], list);
			} else
				list = adjL.get(tickets[u][0]);

			list.add(tickets[u][1]);
		}

		List<String> itinerary = new LinkedList<String>();
		eulerianPath(adjL, "A", itinerary);
		
		return itinerary;
	}
	
	private void eulerianPath(
			HashMap<String, PriorityQueue<String>> adjL,
			String location, List<String> itinerary) {

		// Note 1: queue.poll removes the item (i.e. removes the edge),
		// thus we are not in danger to loop infinitely (basically,
		// we do not need to mark a visited location/node).
		//
		// Note 2: priority queue is used (instead of queue) because wee need the in
		// lexicographic order.
		PriorityQueue<String> q = adjL.get(location);
		while (q != null && !q.isEmpty()) {
			String l = q.poll();
			eulerianPath(adjL, l, itinerary);
		}
		
		// add to the left of the list
		itinerary.add(0, location);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String[][] tickets = new String[][] {
			{"A", "B"},
			{"B", "D"},
			{"B", "C"},
			{"C", "B"},
			{"D", "E"},
			{"E", "F"},
		};
		
		ReconstructItinerary t = new ReconstructItinerary();
		List<String> eulerPath = t.findItinerary(tickets);
		System.out.println(eulerPath);
	}
}