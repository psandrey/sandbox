package _problems._leetcode.graphs;

/*
 * Given n nodes labeled from 0 to n - 1 and a list of undirected edges
 * (each edge is a pair of nodes), check if these edges form a valid tree.
 * 
 * Note: The following version detect cycles, though if the graph doen't have cycles
 *  it doesn't necessarily mean it is a valid Tree (i.e. if there are unvisited nodes,
 *  then it is not a valid tree).
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class DetectingCycles {
	
	private boolean detectCycles(int n, int[][] edges, boolean dfs) {

		// build adjacency list for the undirected graph
		HashMap<Integer, ArrayList<Integer>> adjLists = new HashMap<Integer, ArrayList<Integer>>();
		for (int[] e: edges) {
			if (!adjLists.containsKey(e[0])) {
				ArrayList<Integer> l = new ArrayList<Integer>();
				adjLists.put(e[0], l);
			}
			
			if (!adjLists.containsKey(e[1])) {
				ArrayList<Integer> l = new ArrayList<Integer>();
				adjLists.put(e[1], l);
			}
			
			adjLists.get(e[0]).add(e[1]);
			adjLists.get(e[1]).add(e[0]);
		}
		
		boolean[] marked = new boolean[n];
		return dfs == false ? detectCyclesBFS(adjLists, n, 0) : detectCyclesDFS(adjLists, marked, 0, -1);
	}
	
	private boolean detectCyclesBFS(HashMap<Integer, ArrayList<Integer>> adjLists, int n, int start) {
		boolean[] marked = new boolean[n];
		LinkedList<Integer> Q = new LinkedList<Integer>();

		Q.add(start);
		while (!Q.isEmpty()) {
			int u = Q.poll();
			if (marked[u])
				return true;
			
			marked[u] = true;
			ArrayList<Integer> l = adjLists.get(u);
			for (int v:l)
				// if v is unvisited and it is already in the q, then we have a cycle
				//  and it will be detected when v is dequeued second time (first time it will be
				//  marked visited and second time the cycle is detected)
				if (marked[v] == false)
					Q.offer(v);
		}
		
		return false;
	}

	private boolean detectCyclesDFS(HashMap<Integer, ArrayList<Integer>> adjLists,
			boolean[] marked, int u, int p) {

		boolean hasCycles = true;
		if (marked[u] == true)
			return hasCycles;
		
		marked[u] = true;
		if (adjLists.containsKey(u)) {
			ArrayList<Integer> l = adjLists.get(u);
			for (int v:l)
				if (v != p)
					hasCycles = detectCyclesDFS(adjLists, marked, v, u);
		}
		
		return hasCycles;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[][] edges = new int[][] {
			{0, 1},
			{0, 3},
			{1, 3},
			{1, 2},
		};
		int n = 4;
		DetectingCycles t = new DetectingCycles();
		boolean hasCycles = true;
		
		hasCycles = t.detectCycles(n, edges, false);
		System.out.println("BFS version: has cycles:" + hasCycles);
		
		hasCycles = t.detectCycles(n, edges, true);
		System.out.println("DFS version: has cycles:" + hasCycles);
	}
}
