package _problems._leetcode.graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

/*
 * There are a total of n courses you have to take, labeled from 0 to n - 1.
 * Some courses may have prerequisites, for example to take course 0
 * you have to first take course 1, which is expressed as a pair: [0,1].
 * Given the total number of courses and a list of prerequisite pairs,
 * is it possible for you to finish all courses?
 * 
 * For example, given 2 and [[1,0]], there are a total of 2 courses to take.
 * To take course 1 you should have finished course 0. So it is possible.
 * 
 * For another example, given 2 and [[1,0],[0,1]], there are a total of 2 courses to take.
 * To take course 1 you should have finished course 0, and to take course 0
 * you should also have finished course 1. So it is impossible.
 * 
 * Note: A digraph G has a topological sort ONLY if G has no cycle. 
 *       ( G must be a DAG: Direct Acyclic Graph).
 */

public class CourseSchedule {

	// Version based on Khan's algorithm
	private boolean canFinishKhan(int n, int[][] prereq) {
		if (n == 0 || prereq == null || prereq.length == 0)
			return false;

		// build adjacency lists and inDegree array
		int[] inDegree = new int[n];
		HashMap<Integer, ArrayList<Integer>> adjLists = new HashMap<Integer, ArrayList<Integer>>();
		for (int[] e: prereq) {
			inDegree[e[0]]++;

			if (adjLists.containsKey(e[1])) {
				ArrayList<Integer> l = adjLists.get(e[1]);
				l.add(e[0]);
			} else {
				ArrayList<Integer> l = new ArrayList<Integer>();
				l.add(e[0]);
				adjLists.put(e[1], l);
			}
		}
		
		// add source nodes into the queue (a DAG must have at least one source and one sink)
		LinkedList<Integer> queue = new LinkedList<Integer>();
		for (int u = 0; u < n; u++) {
			if (inDegree[u] == 0)
				queue.add(u);
		}
		
		int counterMarked = 0;
		while (!queue.isEmpty()) {
			int u = queue.poll();
			counterMarked++;
			
			// this is a sink node
			if (!adjLists.containsKey(u))
				continue;
			
			ArrayList<Integer> adjList = adjLists.get(u);
			for (Integer v: adjList) {
				if(--inDegree[v] == 0)
					queue.add(v);
			}
		}
		
		return counterMarked == n ? true : false;
	}	
	
	// Version based on DFS-Topological-sort
	private boolean canFinish(int n, int[][] prereq) {
		if (n == 0 || prereq == null || prereq.length == 0)
			return false;
		
		// build adjacency lists
		HashMap<Integer, ArrayList<Integer>> adjLists = new HashMap<Integer, ArrayList<Integer>>();
		for (int[] e: prereq) {
			if (adjLists.containsKey(e[1])) {
				ArrayList<Integer> l = adjLists.get(e[1]);
				l.add(e[0]);
			} else {
				ArrayList<Integer> l = new ArrayList<Integer>();
				l.add(e[0]);
				adjLists.put(e[1], l);
			}
		}
		
		// check for cycles
		int[] marked = new int[n];
		for (int c = 0; c < n; c++) {
			boolean cantAttend = detectCycle(adjLists, marked, c);
			if (cantAttend == true)
				return false;
		}

		return true;
	}
	
	private boolean detectCycle(
			HashMap<Integer, ArrayList<Integer>> adjLists, int[] marked, int u) {

		// if there is no edge leaving from u, then u is a sink node.
		if (!adjLists.containsKey(u)) {
			marked[u] = -1;
			return false;
		}
		
		if (marked[u] == 1)
			return true;
		if (marked[u] == -1)
			return false;

		marked[u] = 1;
		ArrayList<Integer> adjL = adjLists.get(u);
		for (Integer v: adjL) {
			if (detectCycle(adjLists, marked, v))
				return true;
		}
		
		// when backtracks (equivalent with stacked)
		marked[u] = -1;
		return false;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[][] prereq = new int[][] {
			{0, 1},
			{1, 2},
			{2, 3},
			{3, 1},
		};
		int n = 4;
		
		CourseSchedule t = new CourseSchedule();
		boolean canAttend;
		
		canAttend = t.canFinish(n, prereq);
		System.out.println("Based on DFS:" + canAttend);
		
		canAttend = t.canFinishKhan(n, prereq);
		System.out.println("Based on Khan:" + canAttend);
	}
	
}
