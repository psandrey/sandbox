package _problems._leetcode.graphs;

/*
 * For a undirected graph with tree characteristics, we can choose any node as the root.
 * The result graph is then a rooted tree. Among all possible rooted trees,
 * those with minimum height are called minimum height trees (MHTs). Given such a graph,
 * write a function to find all the MHTs and return a list of their root labels.
 * 
 * The graph contains n nodes which are labeled from 0 to n - 1. You will be given the number n
 * and a list of undirected edges (each edge is a pair of labels).
 * You can assume that no duplicate edges will appear in edges.
 * Since all edges are undirected, [0, 1] is the same as [1, 0] and thus will not appear together in edges.
 * 
 * Note:The MHT must lie on the middle of the longest end to end path in the graph.
 *      So there can at most be two roots for such MHT in a graph.
 *
 * Example:
 * n = 6, edges = [[0, 3], [1, 3], [2, 3], [4, 3], [5, 4]]
 * return: [3, 4]
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class MinimumHeightTrees {

	public List<Integer> findMinHeightTrees(int n, int[][] edges) {
		if (n == 0 || edges == null || edges.length == 0)
			return new ArrayList<Integer>();
		if (n == 1) {
			List<Integer> result = new ArrayList<Integer>();
			result.add(0);
			return result;
		}
		
		// build adjacency list
		ArrayList<HashSet<Integer>> adjL = new ArrayList<HashSet<Integer>>(n);
		for(int i = 0; i < n; i++)
			adjL.add(new HashSet<Integer>());
		
		for (int[] e: edges) {
			adjL.get(e[0]).add(e[1]);
			adjL.get(e[1]).add(e[0]);
		}
		
		LinkedList<Integer> Q = new LinkedList<Integer>();
		for(int u = 0; u < n; u++) {
			if (adjL.get(u).size() == 1)
				Q.add(u);
		}
		
		while (n > 2) {
			int size = Q.size();
			for (int i = 0; i < size; i++) {
				int u = Q.poll();
				int v = adjL.get(u).iterator().next();
				
				HashSet<Integer> nAdjL = adjL.get(v);
				nAdjL.remove(u);
				if (nAdjL.size() == 1)
					Q.add(v);
				
				// this is not necessarily
				//adjL.set(u, null);
			}
			n -= size;
		}
		
		return Q;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[][] edges = new int[][] {
			{0, 3},
			{1, 3},
			{2, 3},
			{4, 3},
			{5, 4},
		};
		int n = 6;
		
		MinimumHeightTrees t = new MinimumHeightTrees();
		List<Integer> roots = t.findMinHeightTrees(n, edges);
		System.out.println(roots);
	}
}
