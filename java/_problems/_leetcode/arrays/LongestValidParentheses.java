package _problems._leetcode.arrays;

import java.util.Stack;

/*
 *  Given a string containing just the characters '(' and ')',
 *  find the length of the longest valid (well-formed) parentheses substring. 
 *  
 *  For "(()", the longest valid parentheses substring is "()", which has length = 2. 
 *  Another example is ")()())", where the longest valid parentheses substring is "()()",
 *  which has length = 4.
 */

public class LongestValidParentheses {
	
	private int longestValidParentheses(String s) {
		int result = 0;
		
		Stack<int []> stack = new Stack<int []>();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			// code '(' = 0 and ')' = 1
			if (c == '(') // each time a opened parentheses, push it 
				stack.push(new int[] {i, 0});
			else {
				// this is a closed parentheses
				if (stack.isEmpty() || stack.peek()[1] == 1)
					// it the peek is also closed, then is not a valid sequence, so just push it 
					stack.push(new int[]{i, 1});
				else {
					// this is a valid sequence, so lets compute the length
					stack.pop();
					int len = 0;
					if (stack.empty())
						len = i + 1;
					else
						len = i - stack.peek()[0];

					// is this length bigger?
					result = Math.max(result, len);
				}
			}
		}

		return result;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String s = ")()())";
		
		LongestValidParentheses t = new LongestValidParentheses();
		int rc = t.longestValidParentheses(s);
		System.out.println(rc);
	}
}
