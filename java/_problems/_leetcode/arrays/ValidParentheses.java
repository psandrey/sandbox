package _problems._leetcode.arrays;

/* 
 * Given a string containing just the characters '(', ')', '{', '}', '[' and ']',
 * determine if the input string is valid. 
 * 
 * The brackets must close in the correct order, "()" and "()[]{}" are all valid but
 * "(]" and "([)]" are not.
 */

import java.util.HashMap;
import java.util.Stack;

public class ValidParentheses {

	private boolean validity(String s) {
		HashMap<Character, Character> mp = new HashMap<Character, Character>();
		mp.put('(',')');
		mp.put('[',']');
		mp.put('(',')');
		
		Stack<Character> stack = new Stack<Character>();
		for (int i = 0; i<s.length(); i++) {
			char c = s.charAt(i);

			if (mp.containsKey(c))
				stack.push(c);

			else if (mp.containsValue(c)) {
				if (!stack.empty() && mp.get(stack.peek()) == c)
					stack.pop();
				else
					return false;
			}
		}

		return true;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String s = "{{[()()]}}";

		ValidParentheses t = new ValidParentheses();
		boolean rc = t.validity(s);
		System.out.println(rc);
	}
}
