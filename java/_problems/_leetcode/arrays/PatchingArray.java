package _problems._leetcode.arrays;

/*
 * Given a sorted positive integer array nums and an integer n,
 * add/patch elements to the array such that any number in range [1, n] inclusive
 * can be formed by the sum of some elements in the array.
 * Return the minimum number of patches required.
 * 
 * Example 1:
 * nums = [1, 3], n = 6
 * Return 1. 
 */

public class PatchingArray {

	private int doMinPatches(int[] nums, int n) {
	    long miss = 1;
	    int count = 0;
	    int i = 0;
	 
	    while(miss <= n){
	        if (i<nums.length && nums[i] <= miss) {
	            miss = miss + nums[i];
	            i++;
	        } else {
	            miss += miss;
	            count++;
	        }
	    }
	 
	    return count;
	}
	
	private void print(String s) {
		System.out.println(s);
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		int[] nums = {1, 3};
		int n = 6;
		PatchingArray t = new PatchingArray();
		int c = t.doMinPatches(nums, n);
		t.print(String.valueOf(c));
	}
}
