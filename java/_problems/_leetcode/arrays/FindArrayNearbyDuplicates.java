package _problems._leetcode.arrays;

/*
 * Given an array of integers, find out whether there are two distinct indices i and j
 * in the array such that the difference between nums[i] and nums[j] is at most t and
 * the difference between i and j is at most k.
 */

import java.util.SortedSet;
import java.util.TreeSet;

public class FindArrayNearbyDuplicates {

	private boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
		if(nums==null||nums.length<2||k<0||t<0)
			return false;
	 
		TreeSet<Long> set = new TreeSet<Long>();
		for(int i=0; i<nums.length; i++){ // O(n)
			long curr = (long) nums[i];
	 
			long leftBoundary = (long) curr-t;
			long rightBoundary = (long) curr+t+1; //right boundary is exclusive => +1
			SortedSet<Long> sub = set.subSet(leftBoundary, rightBoundary); // O(1)
			
			if(sub.size()>0) // O(k)
				return true;

			set.add(curr); // O(log k)

			// remove all elements further than k
			if (i>=k)
				set.remove((long)nums[i-k]);  // O(log k)
		}

		// T(n) = O(n*k);
		return false;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = {1, 3, 5, 7};
		int t = 3;
		int k = 3;
		FindArrayNearbyDuplicates test = new FindArrayNearbyDuplicates();
		boolean rc = test.containsNearbyAlmostDuplicate(a, k, t);
		System.out.println(rc);
	}
}
