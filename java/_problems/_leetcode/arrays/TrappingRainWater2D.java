package _problems._leetcode.arrays;

import java.util.PriorityQueue;

public class TrappingRainWater2D{

	private class Cell implements Comparable<Cell> {
		public int i;
		public int j;
		public int height;

		public Cell(int i, int j, int height) {
			this.i = i;
			this.j = j;
			this.height = height;
		}

		@Override
		public int compareTo(Cell o) {
			return this.height - o.height;
		}
	}

	// main code block
	public int trapRainWater(int[][] heightMap) {
		if (heightMap == null || heightMap.length == 0 || heightMap[0].length == 0)
			return 0;

		int n = heightMap.length;		// number of rows
		int m = heightMap[0].length;	// number of cols
		int[][] marked = new int[n][m];
		for (int i = 0; i < n; i++)		
			for (int j = 0; j < m; j++)
				marked[i][j] = 0;

		PriorityQueue<Cell> pq = new PriorityQueue<Cell>();
		
		// add map borders to the priority queue
		for (int i = 0; i < m ; i++) { // first row
			pq.add(new Cell(0, i, heightMap[0][i]));
			marked[0][i] = 1;
		}
		for (int i = 0; i < m ; i++) { // last row
			pq.add(new Cell(n-1, i, heightMap[n-1][i]));
			marked[n-1][i] = 1;
		}
		for (int i = 0; i < n ; i++) { // first col
			pq.add(new Cell(i, 0, heightMap[i][0]));
			marked[i][0] = 1;
		}
		for (int i = 0; i < n ; i++) { // last col
			pq.add(new Cell(i, m-1, heightMap[i][m-1]));
			marked[i][m-1] = 1;
		}

		int totalWater = 0;
		int maxHeight = 0;
		while (!pq.isEmpty()) {
			Cell c = pq.poll();
			int i = c.i;
			int j = c.j;
			maxHeight = Math.max(maxHeight, c.height);

			// visit up
			if (i-1>=0 && marked[i-1][j] == 0) {
				// can we elevate?
				if (maxHeight > heightMap[i-1][j])
					totalWater += maxHeight - heightMap[i-1][j];

				pq.add(new Cell(i-1, j, heightMap[i-1][j]));
				marked[i-1][j] = 1;
			}
			
			// visit down
			if (i+1<n && marked[i+1][j] == 0) {
				// can we elevate?
				if (maxHeight > heightMap[i+1][j])
					totalWater += maxHeight - heightMap[i+1][j];

				pq.add(new Cell(i+1, j, heightMap[i+1][j]));
				marked[i+1][j] = 1;
			}
			
			// visit left
			if (j-1>=0 && marked[i][j-1] == 0) {
				// can we elevate?
				if (maxHeight > heightMap[i][j-1])
					totalWater += maxHeight - heightMap[i][j-1];

				pq.add(new Cell(i, j-1, heightMap[i][j-1]));
				marked[i][j-1] = 1;
			}
			
			// visit right
			if (j+1<m && marked[i][j+1] == 0) {
                //System.out.println(i + " " + j);
				// can we elevate?
				if (maxHeight > heightMap[i][j+1])
					totalWater += maxHeight - heightMap[i][j+1];

				pq.add(new Cell(i, j+1, heightMap[i][j+1]));		
				marked[i][j+1] = 1;
			}
		}

		return totalWater;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		/*
		int map[][] = {{1,4,3,1,3,2},
		               {3,2,1,3,2,4},
		               {2,3,3,2,3,1}};
		
		int map[][] = {
				{12,13,1,12},
				{13,4,13,12},
				{13,8,10,12},
				{12,13,12,12},
				{13,13,13,13}};*/
		int map[][] =
				{{9,9,9,9,9,9,8,9,9,9,9},
				 {9,0,0,0,0,0,1,0,0,0,9},
				 {9,0,0,0,0,0,0,0,0,0,9},
				 {9,0,0,0,0,0,0,0,0,0,9},
				 {9,9,9,9,9,9,9,9,9,9,9}};
		/*
		int map[][] =
				{{12,13,1,12},
				 {13,4,13,12},
				 {13,8,10,12},
				 {12,13,12,12},
				 {13,13,13,13}};*/
		TrappingRainWater2D t = new TrappingRainWater2D();
		int total = t.trapRainWater(map);
		System.out.println(total);
		
	}
}
