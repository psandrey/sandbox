package _problems._leetcode.arrays;

/*
 * Suppose a sorted array is rotated at some pivot unknown to you beforehand.
 * (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2). 
 * 
 * Find the minimum element.You may assume no duplicate exists in the array.
 */

public class FindMinimumInRotatedArray {

	private int findMin(int[] a) {
		if (a == null || a.length == 0)
			return -1;

		return findMin(a, 0, a.length-1);
	}
	
	private int findMin(int[] a, int s, int e) {
		// the base cases
		if (s == e)
			return a[s];
		int k = (s+e)/2;
		if ((e - s == 2) && (a[k-1]>a[k] && a[k]<a[k+1]))
			return a[k];
		else if (e-s == 1)
			return Math.min(a[s], a[e]);
		
		// the general case
		if (a[s] > a[e]) {

			if (a[k] >= a[s])
				return findMin(a, k+1, e);
			return findMin(a, s, k-1);
		}

		// this is the case when the sub-set a[e...s] is sorted, so the smallest is a[s]
		return a[s];
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		//int[] a = {4, 5, -2, -1, 0, 1, 2};
		int[] a = {1, 3};

		FindMinimumInRotatedArray t = new FindMinimumInRotatedArray();
		int min = t.findMin(a);
		System.out.println(min);
	}
}
