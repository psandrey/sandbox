package _problems._leetcode.arrays;

/*
 * Given a string S, you are allowed to convert it to a palindrome
 * by adding characters in front of it.
 * Find and return the shortest palindrome you can find by performing this transformation. 
 * 
 * For example, given "aacecaaa", return "aaacecaaa"; given "abcd", return "dcbabcd".
 */

public class ShortestPalindrome {
	
	/* the KMP version */
	public String shortestPalindrome(String s) {
		if (s == null || s.length() == 0)
			return "";
		int n = s.length();
		if (n == 1)
			return s;
		
		int offset = longestPreffixPalindrome(s);
		return new StringBuilder(s.substring(offset, n)).reverse().toString() + s;
    }
	
	private int longestPreffixPalindrome(String pattern) {
		int n = pattern.length();
		int[] lpist = new int[n]; // longest-prefix-reversed-suffix table
		int i = 0;	// candidate
		int j = n-1;// current reversed-suffix

		int k = 0;
		boolean fullPalindrome = true;
		
		lpist[0] = 0;
		while (j > 0) {
			// find the previous proper prefix-reversed-suffix
			while (i > 0 && pattern.charAt(j-1) != pattern.charAt(i))
				i = lpist[i-1];
	
			// can we extend the proper prefix-reversed-suffix ?
			if (pattern.charAt(j-1) == pattern.charAt(i))
				i++;

			// the proper prefix-reversed-suffix for the pattern[j-1..n-1]
			j--;
			lpist[n-1-j] = i;
			
			// check here if the entire string is a full palindrome
			// :( I couldn't come up with a better idea
			if (fullPalindrome == true) {
				if(k < j+1 && pattern.charAt(k) != pattern.charAt(j+1))
					fullPalindrome = false;
				else if (k >= j+1)
					break;
				k++;
			}
		}

		return fullPalindrome == true ? n : lpist[n - 1];
	}

	/* The naive version */
	private String shortestPalindrome_(String s) {
		if (s == null || s.length() == 0)
			return "";
		int n = s.length();
		if (n == 1)
			return s;
		
		int offset = longestPreffixPalindrome_(s);
		String reversedSuffix = new StringBuilder(s.substring(offset + 1)).reverse().toString();
		return reversedSuffix + s;
	}

	private int longestPreffixPalindrome_(String s) {
		int n = s.length();
		int e = n - 1;
		while (e > 0) {
			int i = 0;
			int j = e;
			while ( i < j && s.charAt(i) == s.charAt(j)) { i++; j--;}
			if (i >= j)
				break;
			e--;
		}
		return e;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		//String s = "aacecaaa";
		//String s = "abc";
		//String s = "aaba";
		//String s = "aba";
		//String s = "aa";
		//String s = "adcba";
		String s ="babbbabbaba";		
		//String s = "aaaabbaa";
		//String s = "ababbbabbaba";
		//String s = "aaabaa";
		//String s = "aacecaaa";
		//String s = "abb";
		//String s = "abbaxbayba";
		ShortestPalindrome t = new ShortestPalindrome();
		
		String smallestPalindrome;
		smallestPalindrome= t.shortestPalindrome_(s);
		System.out.println(smallestPalindrome);

		smallestPalindrome= t.shortestPalindrome(s);
		System.out.println(smallestPalindrome);

	}
}
