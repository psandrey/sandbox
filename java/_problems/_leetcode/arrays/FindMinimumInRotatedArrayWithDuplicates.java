package _problems._leetcode.arrays;

/*
 * Suppose a sorted array is rotated at some pivot unknown to you beforehand.
 * (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2). 
 * 
 * Find the minimum element. The array might contain duplicates.
 */

public class FindMinimumInRotatedArrayWithDuplicates {
	private int findMin(int[] a) {
		if (a == null || a.length == 0)
			return -1;

		return findMin(a, 0, a.length-1);
	}
	
	private int findMin(int[] a, int s, int e) {
		// remove duplicates if any
		while ((e > s) && (a[s] == a[e] || a[e] == a[e-1]))
			e--;
		while ((e > s) && (a[s] == a[e] || a[s] == a[s+1]))
			s++;
		
		// base cases
		if (s == e) // one element... just return it
			return a[s];
		int k = (s+e)/2;
		if (e - s == 1) // if there are 2 elements... no need to go deep
			return Math.min(a[s], a[e]);
		else if (a[k-1] > a[k] && a[k] < a[k+1]) // we need at least 3 elements (minimum global)
			return a[k];

		// general case
		if (a[s] > a[e]) {
			if (a[k] >= a[s])
				return findMin(a, k+1, e);
			return findMin(a, s, k-1);
		}
		
		// this is the case when the sub-set a[e...s] is sorted, so the smallest is a[s]
		return a[s];
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		int[] a = {3, 3, 1, 3, 3};

		FindMinimumInRotatedArrayWithDuplicates t = new FindMinimumInRotatedArrayWithDuplicates();
		int min = t.findMin(a);
		System.out.println(min);
	}
}
