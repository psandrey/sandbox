package _problems._leetcode.arrays;

/*
 * Implement a basic calculator to evaluate a simple expression string. 
 * The expression string may contain open ( and closing parentheses ),
 * the plus + or minus sign -, non-negative integers and empty spaces.
 * You may assume that the given expression is always valid. 
 * 
 * Some examples: "1 + 1" = 2, "(1)" = 1, "(1-(4-5))" = 2
 */

import java.util.Stack;

public class BasicCalculator {

	private final char OPEN  = '(';
	private final char CLOSE = ')';
	private final char ADD   = '+';
	private final char SUB   = '-';
	private final char SPACE = ' ';

	private int calculate(String s) {
		if (s == null || s.length() == 0)
			return 0;

		Stack<String> st = new Stack<String>();
		int eval = 0;

		int pos = advanceToNext(s, -1);
		while (pos < s.length()) {
			char c = s.charAt(pos); 
			switch (c) {
			case OPEN:
			case ADD:
			case SUB:
				st.add(Character.toString(c));
				pos = advanceToNext(s, pos);
				break;
			case CLOSE:
				eval = 0;
				while (!st.empty() && st.peek().charAt(0) != OPEN) {
					String top = st.pop();
					int no = Integer.valueOf(top);
					if (st.empty())
						return no;
					if (st.peek().charAt(0) == SUB) {
						no *= -1;
						st.pop();
					} else if (st.peek().charAt(0) == ADD)
						st.pop();
					eval += no;
				}
				st.pop();
				st.add(Integer.toString(eval));
				pos = advanceToNext(s, pos);
				break;
			case SPACE:
				pos = advanceToNext(s, pos);
				break;

			// this is a number
			default:
				int[] tuple = getNumber(s, pos);
				pos = tuple[1];
				st.add(Integer.toString(tuple[0]));
				continue;
			}
		}
		
		eval = 0;
		while (!st.empty() && st.peek().charAt(0) != OPEN) {
			String top = st.pop();
			int no = Integer.valueOf(top);
			if (st.empty()) {
				eval += no;
				break;
			}
			if (st.peek().charAt(0) == SUB) {
				no *= -1;
				st.pop();
			} else if (st.peek().charAt(0) == ADD)
				st.pop();
			eval += no;
		}

		return eval;
	}
	
	private int advanceToNext(String s, int pos) {
		do {pos++;} while (pos < s.length() && s.charAt(pos) == SPACE);
		return pos;
	}
	
	private int[] getNumber(String s, int pos) {
		int i = pos;
		char c;
		do {
			pos++;
			if (pos == s.length())
				break;
			c = s.charAt(pos);
		} while (pos < s.length() && c != OPEN && c != CLOSE && c != ADD && c != SUB && c != SPACE);
		String sNumber = s.substring(i, pos);
		return new int[] {Integer.valueOf(sNumber), pos};
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		//String expression = "(1-(4 -5   ))";
		//String expression = "(1)";
		//String expression = "1 + 1";
		String expression = "1+1+(2+3)-(1+2)";
		BasicCalculator calc = new BasicCalculator();
		int val = calc.calculate(expression);
		System.out.println(val);
	}
}
