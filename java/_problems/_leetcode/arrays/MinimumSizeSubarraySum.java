package _problems._leetcode.arrays;

/*
 * Given an array of n positive integers and a positive integer s,
 * find the minimal length of a subarray of which the sum >= s.
 * If there isn't one, return 0 instead.
 * 
 * For example, given the array [2,3,1,2,4,3] and s = 7,
 * the subarray [4,3] has the minimal length of 2 under the problem constraint.
 */

public class MinimumSizeSubarraySum {

	public int minSubArrayLen(int[] a, int s) {
		if (a == null || a.length == 0)
			return 0;
		
		int i = 0;
		int j = 0;
		int S = 0;
		
		int minLen = Integer.MAX_VALUE;
		while (j < a.length) {
			if (S < s) 
				S += a[j++];
			else {
				minLen = Math.min(minLen, j-i);
				if (i == j - 1)
					return 1;
				S -= a[i++];
			}
		}
		
		// reduce the last window as much as possible
		while (S >= s) {
			minLen = Math.min(minLen, j - i);
			S -= a[i++];
		}
		
		return minLen == Integer.MAX_VALUE ? 0 : minLen;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = {2,3,1,2,4,3};
		int s = 7;
		
		MinimumSizeSubarraySum t = new MinimumSizeSubarraySum();
		int minLen = t.minSubArrayLen(a, s);
		System.out.println(minLen);
	}
}
