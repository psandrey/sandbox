package _problems._leetcode.arrays;

/*
 * Find the k-th largest element in an unsorted array.
 * Note that it is the k-th largest element in the sorted order, not the k-th distinct element.
 */

import java.util.Comparator;
import java.util.PriorityQueue;

public class FindKLargestElement {

	private int findKLargest_QuickSort(int[] a, int k) {
		if (k<1 || a == null)
			return -1;
		
		return getKth(a, 0, a.length - 1, k);
	}
	
	private int getKth(int[] a, int st, int end, int k) {
		
		int pivot = a[end];
		int i = st;
		int j = end;

		printArray(a);
		while (true) {
			while (a[i] > pivot && i < j)
				i++;
			while (a[j] <= pivot && i < j)
				j--;
			
			if( i == j)
				break;
			
			swap(a, i, j);
		}
		swap(a, i, end);
		printArray(a);

		// the pivot is on position i
		// if there are exactly k elements on the left side then the pivot is the
		// k-th largest elements
		if ( k == i + 1)
			return pivot;
		// if there are more than k elements on the left side of the pivot, then the
		// k-th element is on the left side
		else if ( k < i + 1)
			return getKth(a, st, i - 1, k);
		// otherwise the k-th element is on the right side
		return getKth(a, i + 1, end, k);
	}
	
	private void swap(int[] a, int i, int j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
	
	private void printArray(int[] a) {
		System.out.print("a:");
		for (int v:a)
			System.out.print(v + " ,");
		System.out.println();
	}

	private int findKLargest_Heap(int[] a, int k) {
		Comparator<Integer> comp = new Comparator<Integer>() {
			@Override
			public int compare(Integer v1, Integer v2) {
				return v2 - v1;
			}
		};
	
		PriorityQueue<Integer> q = new PriorityQueue<Integer>(comp);
		for (int i = 0; i < a.length; i++)
			q.add(a[i]);
		
		while (k > 1) {
			q.poll();
			k--;
		}

		return q.peek();
	}
	
	private int findKLargest_HeapOptim(int[] a, int k) {
		PriorityQueue<Integer> q = new PriorityQueue<Integer>(k);
		for (int v: a) {
			q.add(v);			
			if (q.size() > k)
				q.poll();
			printPriorityQueue(q);
		}
		
		return q.peek();
	}
	
	private void printPriorityQueue(PriorityQueue<Integer> q) {
		Integer[] arr1 = new Integer[q.size()];
		Integer[] h = q.toArray(arr1);
		for (int g : h)
			System.out.print(g + ",");
		System.out.println();
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		int[] a = {3,2,1,5,6,4};
		int k = 2;
		FindKLargestElement t = new FindKLargestElement();
		System.out.println("Largest(heap) " + k + "-th: " + t.findKLargest_Heap(a, k));
		System.out.println("Largest(heap optim) " + k + "-th: " +  t.findKLargest_HeapOptim(a, k));
		System.out.println("Largest(q-sort) " + k + "-th: " +  t.findKLargest_QuickSort(a, k));
	}
}
