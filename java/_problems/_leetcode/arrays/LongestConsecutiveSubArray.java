package _problems._leetcode.arrays;

import java.util.HashSet;

public class LongestConsecutiveSubArray {

	public int longestConsecutive(int[] a) {
		if ( a == null || a.length == 0)
			return 0;

		HashSet <Integer> hs = new HashSet<Integer>();

		for (int e:a)
			hs.add(e);
	
		int maxLen = 1;
		for (int e: a) {
			int left = e-1;
			int right = e+1;
			int count = 1;
			while (hs.contains(left)) {
				hs.remove(left--);
				count++;
			}
			while (hs.contains(right)) {
				hs.remove(right++);
				count++;
			}
			maxLen = Math.max(maxLen, count);
		}

		return maxLen;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = {100, 4, 200, 1, 3, 2};

		LongestConsecutiveSubArray t = new LongestConsecutiveSubArray();
		int len = t.longestConsecutive(a);
		System.out.println(len);
	}
}
