package _problems._leetcode.arrays;

import java.util.ArrayList;
import java.util.Arrays;

public class TargetNumberUsingArithmeticOperations {

	public boolean isReachable(ArrayList<Integer> list, int target) {
		if (list == null || list.size() == 0)
			return false;
	 
		int i = 0;
		int j = list.size() - 1;
	 
		ArrayList<Integer> results = getResults(list, i, j, target, 0);
	 
		for (int num : results) {
			if (num == target) {
				return true;
			}
		}
	 
		return false;
	}
	 
	public ArrayList<Integer> getResults(ArrayList<Integer> list,
			int left, int right, int target, int deep) {
		ArrayList<Integer> result = new ArrayList<Integer>();
	 
		if (left > right) {
			return result;
		} else if (left == right) {
			result.add(list.get(left));
			return result;
		}
	 
		for (int i = left; i < right; i++) {
			System.out.println(deep + ":(left,right):(" + left + "," + i + "):" + i);
			System.out.println(deep + ":(left,right):(" + (i+1) + "," + right + "):" + i);
			ArrayList<Integer> result1 = getResults(list, left, i, target, deep+1);
			ArrayList<Integer> result2 = getResults(list, i + 1, right, target, deep+1);
			System.out.println("  # op...");
			System.out.println();
			
			for (int x : result1) {
				for (int y : result2) {
					
					result.add(x + y);
					result.add(x - y);
					result.add(x * y);
					if (y != 0)
						result.add(x / y);
				}
			}
		}
		System.out.println();
	 
		return result;
	}
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(1,2,3));
		int target = 21;
		
		TargetNumberUsingArithmeticOperations t = new TargetNumberUsingArithmeticOperations();
		t.isReachable(list, target);
	}
}
