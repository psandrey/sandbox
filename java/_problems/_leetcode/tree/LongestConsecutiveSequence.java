package _problems._leetcode.tree;

/*
 * Given a binary tree, find the length of the longest consecutive sequence path.
 * The path refers to any sequence of nodes from some starting node to any node
 * in the tree along the parent-child connections. The longest consecutive path
 * need to be from parent to child (cannot be the reverse).
 */

public class LongestConsecutiveSequence {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	};
	
	private int longestConsecutiveSeq(TreeNode root) {
		if (root == null) return 0;
		
		return helper(root, null, -1);
	}

	private int helper(TreeNode n, TreeNode prev, int maxSoFar) {
		if (n == null) return 0;
		
		int cMax = 1;
		if(prev != null && n.val == prev.val + 1) cMax = maxSoFar + 1;
		
		int Lmax = helper(n.left, n, cMax);
		int RMax = helper(n.right, n, cMax);
			
		return Math.max(cMax, Math.max(Lmax, RMax));
	}

	private TreeNode createTree() {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(4);
		root.left.left = new TreeNode(3);
		root.right.right = new TreeNode(5);
		root.right.right.left = new TreeNode(5);
		root.right.right.right = new TreeNode(6);
		root.right.right.right.left  = new TreeNode(7);
		
		return root;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		LongestConsecutiveSequence t = new LongestConsecutiveSequence();
		TreeNode root = t.createTree();

		int max = t.longestConsecutiveSeq(root);
		System.out.println(max);
	}
}
