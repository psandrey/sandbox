package _problems._leetcode.tree;

/*
 * Given a binary tree, find the largest subtree which is a Binary Search Tree (BST),
 *  where largest means subtree with largest number of nodes in it.
 */

public class LargestBSTinBT {
	
	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	};

	private class TreeInfo {
		boolean isBST = false;
		int size = 0;
		int[] mm; // min/max
		public TreeInfo(boolean is, int s, int[] m) {
			isBST = is; size = s; mm = m; }
	};
	
	// The version based on BST definition
	// T(n) = O(n)
	// S(n) = O(n) - due to recursive call
	public int getSizeLargestBST(TreeNode root) {
		if (root == null) return 0;

		return helper(root).size;
	}
	
	private TreeInfo helper(TreeNode n) {
		if (n == null)
			return new TreeInfo(true, 0, new int[]{ Integer.MAX_VALUE, Integer.MIN_VALUE });
		if (n.left == null && n.right == null)
			return new TreeInfo(true, 1, new int[]{ n.val, n.val });

		TreeInfo L = helper(n.left);
		TreeInfo R = helper(n.right);

		TreeInfo I = new TreeInfo(true, 1, new int[] { n.val, n.val });

		// if we can extend BST
		if (L.isBST && R.isBST && L.mm[1] < n.val && R.mm[0] > n.val) {
			
			I.isBST = true;
			I.size = L.size+R.size+1;
			I.mm[0] = Math.min(n.val, L.mm[1]);
			I.mm[1] = Math.max(n.val, R.mm[1]);

		// if we cannot extend, return the biggest
		} else {
			I.isBST = false;
			I.size = Math.max(L.size, R.size);
		}
		
		return I;
	}

	private TreeNode createTree() {
		TreeNode root = new TreeNode(4);
		root.left = new TreeNode(3);
		root.left.left = new TreeNode(2);
		root.left.left.left = new TreeNode(1);
		root.left.left.right = new TreeNode(0);

		return root;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		LargestBSTinBT t = new LargestBSTinBT();
		TreeNode root = t.createTree();
		
		int largestBST = t.getSizeLargestBST(root);
		System.out.println(largestBST);	
	}
}
