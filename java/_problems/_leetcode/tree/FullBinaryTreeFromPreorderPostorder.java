package _problems._leetcode.tree;

/*
 * Build a full binary tree from pre-order and post-order traversals.
 */

public class FullBinaryTreeFromPreorderPostorder {

	private class TreeNode {
		TreeNode left = null, right = null;
		int val;
		public TreeNode(int val) { this.val = val; }
	};
	
	public TreeNode build(int[] pre, int[] post) {
		if (pre == null) return null;
		else if (pre.length == 1) return new TreeNode(pre[0]);
		
		return build(pre, 0, pre.length-1, post, 0, post.length-1);
	}
	
	private TreeNode build(int[] pre, int i, int j, int[] post, int k, int l) {
		if (i == j) return new TreeNode(pre[i]);
		
		TreeNode n = new TreeNode(pre[i]);
		int p = find(post, k, l, pre[i+1]);
		
		n.left = build(pre, i+1, i+1 + (p-k), post, k, p-1);
		n.right = build(pre, i+1 + (p-k) + 1, j, post, p+1, l);
		
		return n;
	}
	
	// find can be improved, however this problem is just an exercise :)
	private int find(int[] a, int i, int j, int key) {
		for (int k = i; k <= j; k++)
			if (a[k] == key) return k;
		
		return -1;
	}
	
	private void printPre(TreeNode n) {
		if (n == null) return;
		System.out.print(n.val + ", ");
		printPre(n.left);
		printPre(n.right);
	}

	private void printPost(TreeNode n) {
		if (n == null) return;
		printPre(n.left);
		printPre(n.right);
		System.out.print(n.val + ", ");
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		int[] pre  = new int[] {1, 2, 4, 5, 8, 9, 14, 15, 3, 6, 10, 11, 7, 12, 16, 17, 13};
		int[] post = new int[] {4, 8, 14, 15, 9, 5, 2, 10, 11, 6, 16, 17, 12, 13, 7, 3, 1};
		
		FullBinaryTreeFromPreorderPostorder t = new FullBinaryTreeFromPreorderPostorder();
		TreeNode root = t.build(pre, post);

		System.out.print("pre: ");
		t.printPre(root);
		System.out.println();
		
		System.out.print("post:");
		t.printPost(root);
		System.out.println();
	}
}
