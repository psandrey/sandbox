package _problems._leetcode.tree;

/*
 * Design an algorithm to serialize and deserialize a binary tree.
 * There is no restriction on how your serialization/deserialization
 * algorithm should work. You just need to ensure that a binary tree
 * can be serialized to a string and this string can be deserialized
 * to the original tree structure.
 * 
 * Note: this version uses level traversal. But, it can be used
 * pre-order traversal and post-order.
 */

import java.util.LinkedList;

public class SerializeDeserializeBT {

	private class TreeNode {
		public TreeNode left, right;
		public int value;
		public TreeNode(int value) {
			this.left = null;
			this.right = null;
			this.value = value;
		}
	};


	public String levelSerialize(TreeNode root) {
		if (root == null) return "";
	 
		StringBuilder sb = new StringBuilder();
		LinkedList<TreeNode> q = new LinkedList<>();

		q.add(root);
		while (!q.isEmpty()) {
			int size = q.size();
			
			for (int i = 0; i < size; i++) {
				TreeNode n = q.poll();
				if (n == null) sb.append("#,");
				else {
					sb.append(String.valueOf(n.value) + ",");
					q.add(n.left);
					q.add(n.right);
				}
			}
		}
	 
		sb.deleteCharAt(sb.length()-1);
		System.out.println(sb.toString());
		return sb.toString();
	}

	private TreeNode levelDeserialize(String s) {
		if (s == null || s.length() == 0) return null;

		String[] u = s.split(",");
		TreeNode root = new TreeNode(Integer.valueOf(u[0]));
		
		LinkedList<TreeNode> q = new LinkedList<>();
		q.add(root);
		int j = 1;

		while (!q.isEmpty()) {
			int size = q.size();
			
			for (int i = 0; i < size; i++) {
				TreeNode n = q.poll();
				String l = u[j++];
				String r = u[j++];

				if (l.compareTo("#") != 0) {
					n.left = new TreeNode(Integer.valueOf(l));
					q.add(n.left);
				}
				
				if (r.compareTo("#") != 0) {
					n.right = new TreeNode(Integer.valueOf(r));
					q.add(n.right);					
				}
			}
		}

		return root;
	}
	
	private boolean sameBT(TreeNode n, TreeNode q) {
		if (n == null && q == null)
			return true;
		else if (n == null || q == null)
			return false;
		
		
		if (n.value == q.value)
			return sameBT(n.left, q.left) && sameBT(n.right, q.right);

		return false;
	}
	
	private TreeNode createTree() {
		TreeNode root = new TreeNode(1);
		TreeNode n1 = new TreeNode(2);
		TreeNode n2 = new TreeNode(3);
		root.left = n1;
		root.right = n2;

		TreeNode n3 = new TreeNode(7);
		TreeNode n4 = new TreeNode(8);
		TreeNode n5 = new TreeNode(9);
		n2.left = n3;
		n3.left = n4;
		n4.left = n5;

		return root;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		SerializeDeserializeBT t = new SerializeDeserializeBT();
		TreeNode root = t.createTree();
		
		String serial = t.levelSerialize(root);
		System.out.println(serial);
		
		TreeNode newRoot = t.levelDeserialize(serial);
		System.out.println(t.sameBT(root, newRoot));
	}
}
