package _problems._leetcode.tree;

/*
 * One way to serialize a binary tree is to use pre-order traversal.
 * When we encounter a non-null node, we record the node's value.
 * If it is a null node, we record using a sentinel value such as #.
 * 
 * or example, the above binary tree can be serialized to the string
 * "9,3,4,#,#,1,#,#,2,#,6,#,#", where # represents a null node.
 */

import java.util.LinkedList;

public class VerifyPreorderSerialisationBT {

	public boolean isValidSerializationStack(String preorder) {
		LinkedList<String> stack = new LinkedList<String>();
		String[] arr = preorder.split(",");
	 
		for (int i = 0; i < arr.length; i++){
			stack.add(arr[i]);
	 
			while (stack.size() >= 3
				&& stack.get(stack.size()-1).equals("#")
				&& stack.get(stack.size()-2).equals("#")
				&& !stack.get(stack.size()-3).equals("#")) {
	 
				stack.remove(stack.size()-1);
				stack.remove(stack.size()-1);
				stack.remove(stack.size()-1);
	 
				stack.add("#");
			}
		}

		if (stack.size() == 1 && stack.get(0).equals("#")) return true;

		return false;
	}

	// Solution based on full binary tree properties
	// N = 2I+1, I = (N-1)/2. L = (N+1)/2, N = 2L-1, I = L-1
	// Basically:
	//   1. For a full binary tree, # of node = # of edges + 1,
	//      thus if we manually add an edge to the root, then the # of edges = the # of nodes;
	//    2. For a node, it consumes one edge and produces 2 new edges(if not null)
	public boolean isValidSerialization(String preorder) {
		if(preorder == null || preorder.length() <= 0) return true;

		String [] nodes = preorder.split(",");
		int edges  = 1;
		for (String node : nodes) {
			edges --; // consume one edge
			
			if(edges  < 0) return false; // well, if we consumed more edges that we can...
			if(!node.equals("#")) edges += 2; // generate 2 edges
		}

		// well, if we didn't consume all edges...
		return edges == 0;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String s = "9,3,#,#,#";
		VerifyPreorderSerialisationBT t = new VerifyPreorderSerialisationBT();
		
		System.out.println(t.isValidSerializationStack(s));
		System.out.println(t.isValidSerialization(s));
	}
}
