package _problems._leetcode.heaps;

/*
 * Given an array of meeting time intervals consisting of start and end times
 * [[s1,e1],[s2,e2],...] find the minimum number of conference rooms required.
 */

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

public class MeetingRooms {

	private class Interval {
		int s;
		int e;
		public Interval(int s, int e) {
			this.s = s;
			this.e = e;
		}
	};
	
	private int getNumberOfMeetingRooms(Interval[] intervals) {
		Comparator<Interval> compSort = new Comparator<Interval>() {
			@Override
			public int compare(Interval i1, Interval i2) {
				return i1.s - i2.s;
			}
		};
		Arrays.sort(intervals, compSort);

		Comparator<Interval> compQ = new Comparator<Interval>() {
			@Override
			public int compare(Interval i1, Interval i2) {
				return i1.e - i2.e;
			}
		};
		PriorityQueue<Interval> q = new PriorityQueue<Interval>(compQ);
		q.offer(intervals[0]);
		int rooms = 1;
		for (int i = 1; i < intervals.length; i++) {
			Interval head = q.peek();

			if (!q.isEmpty() && intervals[i].s >= head.e)
				q.poll();
			else
				rooms++;
			q.offer(intervals[i]);
		}
		
		return rooms;
	}
	
	private Interval[] buildIntervals() {
		Interval[] intervals = new Interval[3];
		intervals[0] = new Interval(0, 30);
		intervals[1] = new Interval(15, 20);
		intervals[2] = new Interval(5, 10);
		
		return intervals;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		MeetingRooms t = new MeetingRooms();
		Interval[] intervals = t.buildIntervals();
		System.out.println(t.getNumberOfMeetingRooms(intervals));
	}
}
