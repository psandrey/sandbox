package _problems._leetcode.heaps;

import java.util.ArrayList;

public class MinPrioQueue {
	private int capacity;
	private int heapSize;
	private ArrayList<Integer> heapItems;
	private ArrayList<Integer> heapPrios;
	public MinPrioQueue(int capacity) {
		this.capacity = capacity;
		this.heapItems = new ArrayList<Integer>(capacity);
		this.heapPrios = new ArrayList<Integer>(capacity);
		this.heapSize = 0;
	}
	
	public boolean containsItem(int item) {
		return heapItems.contains(item);
	}
	
	public int getPriority(int item) {
		if (!heapItems.contains(item))
			return -1;
		
		return heapPrios.get(heapItems.indexOf(item));
	}
	
	public boolean insert(int item, int p) {
		if (heapSize >= capacity)
			return false;
		
		heapItems.add(item);
		heapPrios.add(p);
		heapSize++;
		minHeapifyRise(heapSize - 1);
		return true;
	}
	
	public int extractMin() {
		if (heapSize == 0)
			return -1;
		int minItem  = heapItems.get(0);
		
		int lastLeafPrio = heapPrios.remove(heapSize - 1);
		int lastLeafItem = heapItems.remove(heapSize - 1);
		heapSize--;
		
		if (heapSize > 0) {
			heapPrios.set(0, lastLeafPrio);
			heapItems.set(0, lastLeafItem);
			minHeapifySink(0);
		}
		
		return minItem;
	}
	
	public void decreasePrio(int item, int p) {
		int idxItem = heapItems.indexOf(item);
		heapPrios.set(idxItem, p);
		
		minHeapifyRise(idxItem);
	}
	
	public void increasePrio(int item, int p) {
		int idxItem = heapItems.indexOf(item);
		heapPrios.set(idxItem, p);
		
		minHeapifySink(idxItem);
	}
	
	public boolean isFull() {
		if (heapSize == capacity)
			return true;
		return false;
	}
	
	public boolean isEmpty() {
		if (heapSize == 0)
			return true;
		return false;
	}
	
	private int left(int idx) {
		return 2*idx + 1;
	}

	private int right(int idx) {
		return 2*idx + 2;
	}

	private int parent(int idx) {
		return (int)(((double)idx)/((double)2) + 0.5) - 1;
	}

	private void swap(int i, int j) {		
		int auxPrio = heapPrios.get(i);
		int auxItem = heapItems.get(i);

		heapPrios.set(i, heapPrios.get(j));
		heapItems.set(i,  heapItems.get(j));
		
		heapPrios.set(j, auxPrio);
		heapItems.set(j, auxItem);
	}
	
	private void minHeapifyRise(int idx) {
		int p = parent(idx);

		int smallest = idx;   
		if (p >= 0 && heapPrios.get(p) > heapPrios.get(smallest)) {
			smallest = p;
		
			if (smallest != idx) {
				swap(idx, smallest);
				minHeapifyRise(smallest);
			}
		}
	}
	
	private void minHeapifySink(int idx) {
		int l = left(idx);
		int r = right(idx);
		
		int smallest = idx;
		if (l < heapSize && heapPrios.get(l) < heapPrios.get(smallest))
			smallest = l;
	
		if (r < heapSize && heapPrios.get(r) < heapPrios.get(smallest))
			smallest = r;

		if (smallest != idx) {
			swap(idx, smallest);
			minHeapifySink(smallest);
		}
	}
}
