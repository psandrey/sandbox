package _problems._leetcode.linked_lists;

/*
 * Implement a stack by using an array.
 */

public class StackWithArray {

	private class Stack<T> {
		private T[] s = null;
		private int maxCapacity;
		private int top;

		// we need to suppress warnings because of the following cast
		@SuppressWarnings("unchecked")
		public Stack(int capacity) {
			this.maxCapacity = capacity;
			this.s = (T []) new Object[capacity];
		}
		
		public T pop() {
			if (top == 0)
				return null;

			T head = s[top];
			s[top] = null;
			top--;
			return head;	
		}
		
		public boolean push(T e) {
			if (isFull())
				return false;

			s[++top] = e;
			return true;
		}
		
		public boolean isFull() {
			return (top == (maxCapacity - 1));
		}
	}
	
	private Stack<Integer> newStack(int cap) {
		return new Stack<Integer>(cap);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		StackWithArray t = new StackWithArray();
		int capacity = 10;
		Stack<Integer> stack = t.newStack(capacity);
		stack.push(10);
		stack.pop();
	}
}
