package _problems._leetcode.linked_lists;

/*
 * Given a singly linked list, group all odd nodes together followed by the even nodes.
 * Please note here we are talking about the node number and not the value in the nodes.
 */

public class OddEvenList {

	private class Node {
		Node next;
		int data;
		public Node(int val) {
			data = val;
			next = null;
		}
	};
	
	private Node oddEvenList(Node head) {
		if (head == null || head.next == null)
			return head;
		
		Node p1 = head;
		Node p2 = head.next;
		while (p2 != null) {
			Node t = p2.next;
			if (t == null)
				break;
			
			p2.next = t.next;
			t.next = p1.next;
			p1.next = t;
			
			p1 = p1.next;
			p2 = p2.next;
		}
		
		return head;
	}
	
	private Node buildList(int n) {
		Node head = new Node(1);
		Node pre = head;
		for (int i = 2; i < n; i++) {
			Node node = new Node(i);
			pre.next = node;
			pre = node;
		}

		return head;
	}
	
	private void printList(Node l) {
		System.out.print(" [ ");
		Node n = l;
		while (n != null) {
			System.out.print(n.data + ",");
			n = n.next;
		}
		System.out.print(" ] ");
		System.out.println();	
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		OddEvenList t = new OddEvenList();
		
		int n = 12;
		Node l = t.buildList(n);
		t.printList(l);
		Node rl = t.oddEvenList(l);
		t.printList(rl);
	}
}
