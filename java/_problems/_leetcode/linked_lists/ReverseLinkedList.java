package _problems._leetcode.linked_lists;

/*
 * Methods of reversing a singly linked list. 
 */

public class ReverseLinkedList {
	
	private class Node {
		Node next;
		int data;
		public Node(int val) {
			data = val;
			next = null;
		}
	};
	
	private Node reverseIterative(Node head) {
		if (head == null || head.next == null)
			return head;

		Node h = head;
		Node p = h.next;
		h.next = null;
		while (p != null) {
			Node t = p.next;
			p.next = h;
			h = p;
			p = t;
		}

		return h;
	}

	private Node reverseRecursiveV1(Node h) {
		if (h == null || h.next == null)
			return h;

		Node head = reverseRecursiveV1(h.next);
		Node tail = h.next;
		tail.next = h;
		h.next = null;
		return head;
	}

	private Node reverseRecursiveV2(Node h) {
		if (h == null || h.next == null)
			return h;
		
		Node second = h.next;
		h.next = null;
		Node rest = reverseRecursiveV1(second); 
		second.next = h;
		return rest;
	}
	
	private Node buildList(int n) {
		Node head = new Node(0);
		Node pre = head;
		for (int i = 1; i < n; i++) {
			Node node = new Node(i);
			pre.next = node;
			pre = node;
		}

		return head;
	}
	
	private void printList(Node l) {
		System.out.print(" [ ");
		Node n = l;
		while (n != null) {
			System.out.print(n.data + ",");
			n = n.next;
		}
		System.out.print(" ] ");
		System.out.println();	
	}
	
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		ReverseLinkedList t = new ReverseLinkedList();
		Node head = t.buildList(4);
		t.printList(head);
		head = t.reverseRecursiveV1(head);
		t.printList(head);
		head = t.reverseRecursiveV2(head);
		t.printList(head);
		head = t.reverseIterative(head);
		t.printList(head);
		
	}
}
