package _problems._basket._sums;

/*
 * You have been given an integer array A and a number K.
 * Now, you need to find out whether any two different elements of the array A sum to the number K.
 * Two elements are considered to be different if they lie at different positions in the array.
 * If there exists such a pair of numbers, print "YES", else print "NO".
 */

import java.util.HashMap;

public class pairSums {

	private boolean isSumK(int[] a, int k) {
		HashMap<Integer, Integer> mp = new HashMap<Integer, Integer>();
		
		for (int i=0; i < a.length; i++) {
			int d = k - a[i];
			if (mp.containsKey(d))
				return true;
			else
				mp.put(a[i], d);
		}
	
		return false;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String[] args) {
		int[] a = {1, 3, 5, 2, 4};
		int k = 9;
		pairSums t = new pairSums();
		boolean flag = t.isSumK(a, k);
		System.out.println(flag);
		
	}
}
