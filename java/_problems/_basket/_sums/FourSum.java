package _problems._basket._sums;

/*
 * Given an array S of n integers, are there elements a, b, c, and d in S such that a + b + c + d = target?
 * Find all unique quadruplets in the array which gives the sum of target.
 * 
 * Note:
 * Elements in a quadruplet (a,b,c,d) must be in non-descending order. (i.e. a <= b <= c <= d)
 * The solution set must not contain duplicate quadruplets.
 * 
 * 
 * For example, given array S = {1 0 -1 0 -2 2}, and target = 0.
 * 
 *  A solution set is:
 *  (-1,  0, 0, 1)
 *  (-2, -1, 1, 2)
 *  (-2,  0, 0, 2)
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FourSum {
	private List<List<Integer>> fourSum(int[] nums, int target) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
	 
		if(nums == null|| nums.length < 4)
			return result;
	 
		Arrays.sort(nums);
	 
		for(int i=0; i<nums.length-3; i++) {
			// if there was a solution with nums[i] = nums[i+1] it was find when nums[i] = nums[j]
			// so, if we would allow this, then we would have a duplicate solution.
			if(i!=0 && nums[i]==nums[i-1])
				continue;
			for(int j=i+1; j<nums.length-2; j++) {
				// if there was a solution with nums[j] = nums[j+1] it was find when nums[l] = nums[j]
				// so, if we would allow this, then we would have a duplicate solution.
				if(j!=i+1 && nums[j]==nums[j-1])
					continue;

				int k=j+1;
				int l=nums.length-1;
				while (k < l) {
					if (nums[i]+nums[j]+nums[k]+nums[l]<target)
						k++;
					else if (nums[i]+nums[j]+nums[k]+nums[l]>target)
						l--;
					else {
						List<Integer> t = new ArrayList<Integer>();
						t.add(nums[i]);
						t.add(nums[j]);
						t.add(nums[k]);
						t.add(nums[l]);
						result.add(t);
						
						k++;
						l--;

						// handle duplicate here
						// basically [i,j] [l,k] was, now to avoid duplicates 
						// we need to advance until skip this set
						while(k<l &&nums[l]==nums[l+1])
							l--;
						while(k<l &&nums[k]==nums[k-1])
							k++;
					}
				}
			}
		}
 
		return result;
	}

	private void printListOfLists(List<List<Integer>> ll) {
		for (List<Integer> l: ll) {
			System.out.print("[");
			for(Integer v: l) {
				System.out.print(v + ",");	
			}
			System.out.print("]");
			System.out.println();
		}
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		int[] S = {1, 0, -1, 0, -2, 0, 2, 5, 6};
		int target = 0;
		FourSum t = new FourSum();
		List<List<Integer>> result = t.fourSum(S, target);
		t.printListOfLists(result);
	}
}
