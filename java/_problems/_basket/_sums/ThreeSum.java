package _problems._basket._sums;

/*
 * Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0?
 * Find all unique triplets in the array which gives the sum of zero.
 * 
 * Note:
 * Elements in a triplet (a,b,c) must be in non-descending order. (ie, a <= b <= c)
 * The solution set must not contain duplicate triplets.
 * 
 * For example, given array S = {-1 0 1 2 -1 -4},
 * A solution set is:
 *   (-1, 0, 1)
 *   (-1, -1, 2)
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class ThreeSum {

	// The version with array support: T(n) = O(n), S(n) = O(1)
	public int[][] threeSumZeroV1(int[] a) {
		if (a == null || a.length < 3) return new int[][] {};
	 
		Arrays.sort(a);
	 
		List<Integer[]> ans = new ArrayList<Integer[]>();
		int n = a.length;
		for (int i = 0; i < n-2; i++) {

			// skips duplicates
			if (i > 0 && a[i] == a[i-1]) continue;
			
			int j = i+1 ,k = n-1;
			while (j < k) {
				long s = (long)a[i] + (long)a[j] + (long)a[k];
				//System.out.println(a[i]+","+a[j]+","+a[k]+"="+s);
				
				if (s < 0) j++;
				else if (s > 0) k--;
				else {
					ans.add(new Integer[] {a[i], a[j], a[k]});

					j++; k--;
				
					// handle duplicate here
					// basically [i,j,k] is already in ans
					while (j < k && a[j] == a[j-1]) j++;
					while (j < k && a[k] == a[k+1]) k--;
				}
			} /* while (j < k) */
		}
	 
		return toMatrix(ans);
	}
	
	private int[][] toMatrix(List<Integer[]> l) {
		int n = l.size();
		int m = 3;
		int[][] ans = new int[n][3];
		for (int i = 0; i < n; i++) {
			Integer[] row = l .get(i);
			for (int j = 0; j < m; j++)
				ans[i][j] = row[j].intValue(); 
		}
		
		return ans;
	}
	
	// Randomized version with a little twist
	// T(n) = O(n^2), S(n) = O(n)
	public List<List<Integer>> threeSumZerV2(int[] a) {
		int n = a.length;
		
		// using a HashSet and not List, just to avoid duplicates
		HashSet<ArrayList<Integer>> ans = new HashSet<ArrayList<Integer>>();
		for (int i = 0; i < n-2; i++) {
			HashSet<Integer> hs = new HashSet<Integer>();
			
			for (int j = i+1; j < n; j++) {
				int c = (-1)*(a[i]+a[j]);
				if (!hs.contains(c)) hs.add(a[j]);
				else {
					ArrayList<Integer> row = new ArrayList<Integer>();
					row.add(a[i]);
					row.add(c);
					row.add(a[j]);
					Collections.sort(row); // this is to avoid duplicates
					hs.remove(c); // this is not actually need it
					ans.add(row);
				}
			}
		}
		
		List<List<Integer>> ansL = new ArrayList(ans);		
		return ansL;
	}
	
	private void printListOfLists(List<List<Integer>> ll) {
		for (List<Integer> l: ll) {
			System.out.print("[");
			for(Integer v: l) {
				System.out.print(v + ",");	
			}
			System.out.print("]");
			System.out.println();
		}
	}
	
	private void printMatrix(int[][] a) {
		int n = a.length;
		int m = a[0].length;
		for (int i = 0; i < n; i++) {
			System.out.print("[");
			for(int j = 0; j < m; j++) {
				System.out.print(a[i][j] + ",");	
			}
			System.out.print("]");
			System.out.println();
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		int[] S = {-1, 0, 1, 2, 0, -1, 4, 0, 0, 0};

		ThreeSum t = new ThreeSum();

		int[][] ans = t.threeSumZeroV1(S);
		System.out.println("V1:");
		t.printMatrix(ans);

		
		List<List<Integer>> r = t.threeSumZerV2(S);
		System.out.println("V2:");
		t.printListOfLists(r);
		
	}
}
