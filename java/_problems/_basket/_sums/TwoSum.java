package _problems._basket._sums;

/*
 * Given an array of integers, find two numbers such that they add up to a specific target number.
 */

import java.util.HashMap;

public class TwoSum {
	private int[] twoSum(int[] a, int target) {
		HashMap<Integer, Integer> mp = new HashMap<Integer, Integer>();
		
		for (int i = 0; i < a.length; i++) {
			int d = target - a[i];
			if (mp.containsKey(a[i])) 
				return new int[] {mp.get(a[i]), i};
			else
				mp.put(d, i);
		}
		
		return new int[] {0,0};
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		int[] a = {1, 3, 5, 2, 4};
		int k = 9;
		TwoSum t = new TwoSum();
		int[] pair = t.twoSum(a, k);
		System.out.println("(" + pair[0] + "," + pair[1] + ")=(" + a[pair[0]] + "," + a[pair[1]]+ ")");
	}
}
