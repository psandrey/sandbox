package _problems._basket;

/*
 * Given two binary strings, return their sum (also a binary string).
 */

public class BinarySum {

	private String addBinary(String a, String b) {
		StringBuilder sb = new StringBuilder();
		
		int i = a.length() - 1;
		int j = b.length() - 1;
		
		int carry = 0;
		while (i >= 0 | j >=0 ) {
			int bA = 0;
			int bB = 0;
			if (i >= 0)
				bA = a.charAt(i--) == '0' ? 0 : 1;
			if (j >= 0)
				bB = b.charAt(j--) == '0' ? 0 : 1;

			int sum = bA + bB + carry;
			if (sum >=2) {
				sb.append(String.valueOf(sum - 2));
				carry = 1;
			} else {
				sb.append(String.valueOf(sum));
				carry = 0;
			}
		}
		
		if( carry == 1)
			sb.append("1");
		
		return sb.reverse().toString();
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		String a = "111";
		String b = "111";
		
		BinarySum t = new BinarySum();
		String r = t.addBinary(a, b);
		System.out.println(r);
	}
}
