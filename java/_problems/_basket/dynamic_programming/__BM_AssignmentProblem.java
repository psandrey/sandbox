package _problems._basket.dynamic_programming;
import static java.lang.System.out;

// DP with Bit Masking

/*
 * There are  persons and  tasks, each task is to be alloted to a single person.
 * We are also given a matrix  of size , where  denotes, how much person 
 * is going to charge for task . Now we need to assign each task to a person
 * in such a way that the total cost is minimum.
 * 
 * Note that each task is to be alloted to a single person, and each person
 * will be alloted only one task.
 */

public class __BM_AssignmentProblem {

	// T(n) = O(n.2^n), S(n) = O(2^n)
	public int assignTasks(int n, int[][] cost) {
		int all = (1<<n);
		int[] dp = new int[all];
		
		for (int mask = 0; mask < all; mask++) dp[mask] = Integer.MAX_VALUE;
		dp[0] = 0;
		
		for (int mask = 0; mask < all; mask++) {
			int k = Integer.bitCount(mask);
			
			for (int j = 0; j < n; j++) {
				if ((mask & (1<<j)) == 0) continue;
				
				int mask_no_j = mask & (~(1<<j)); // task j is assigned to k
				dp[mask] = Math.min(dp[mask], dp[mask_no_j] + cost[k - 1][j]);
			}
		}
		
		return dp[all-1];
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int n = 3;
		int[][] cost = new int[][] {
				{2, 1, 2},
				{1, 1, 2},
				{1, 2, 1}};
				
		__BM_AssignmentProblem t = new __BM_AssignmentProblem();
		System.out.println(t.assignTasks(n, cost));
	}
}
