package _problems._basket.dynamic_programming;

/*
 * Given a value V, if we want to make change for V cents,
 * and we have infinite supply of each of C = { C1, C2, .. , Cm} valued coins.
 * What is the minimum number of coins to make the change?
 */

import java.util.Arrays;

public class Coins {

	// dynamic programming
	// T(n) = O (S*n)
	int coins(int c[], int S) {
		if (c == null || c.length == 0)
			return -1;
		
		int n = c.length;
		int[] dp = new int[S+1];
		Arrays.fill(dp, S+1);
		
		dp[0] = 0;
		for (int s = 1; s <= S; s++)
			for (int j = 0; j < n; j++) {
				if (c[j] <= s) {
					dp[s] = Math.min(dp[s], dp[s - c[j]]) + 1;
				}
			}
	
		return dp[S] > S ? -1 : dp[S];
	}

	// brute-force approach
	// T(n) = O (S^n)
	private int coins(int i, int[] a, int rem) {
		if (rem == 0)
			return 0;
		int n = a.length;
		if (i >= n || rem < 0)
			return -1;

		int maxCoins = rem/a[i];
		int min = Integer.MAX_VALUE;
		for (int j = 0; j <= maxCoins; j++) {
			int rest = rem - j*a[i];
			if (rest < 0)
				continue;
			
			int nCoins = coins(i + 1, a, rest);
			if (nCoins != -1)
				min = Math.min(min, j+nCoins);
		}
		
		return  (min == Integer.MAX_VALUE) ? -1 : min;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		int[] a = {1, 2, 3, 6};
		int S = 12;
		
		int minCoins = -1;
		Coins t = new Coins();
		minCoins = t.coins(0, a, S); 
		System.out.println("Min number of coins (brute force 1):" + minCoins);

		minCoins = t.coins(a, S); 
		System.out.println("Min number of coins (DP iterrative):" + minCoins);
	}
}
