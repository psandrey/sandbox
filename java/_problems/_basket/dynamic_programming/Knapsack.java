package _problems._basket.dynamic_programming;

/*
 * Given weights and values of n items, put these items in a knapsack of capacity W
 *  to get the maximum total value in the knapsack. In other words, given two integer
 *  arrays val[0..n-1] and wt[0..n-1] which represent values and weights associated
 *  with n items respectively. Also given an integer W which represents knapsack capacity,
 *  find out the maximum value subset of val[] such that sum of the weights of this subset
 *  is smaller than or equal to W. You cannot break an item, either pick the complete item,
 *  or don�t pick it (0-1 property).
 */

public class Knapsack {
	// dp - bottom-up version (space complexity improved)
	// T(n) = O(n*W) , S(n) = O(n*W);
	private int knapsack_(int[] w, int[] p, int W) {
		if (w == null || p == null || w.length == 0 || w.length != p.length)
			return -1;
		
		int n = w.length;
		int[][] dp = new int[2][W+1];

		int k = 0;
		for (int i = 0; i < n; i++) {
			k ^= 1;
			for (int cap = 0; cap <= W; cap++) {

				// by default we don't take the object
				dp[k][cap] = dp[k^1][cap];
				
				// if you have room we see if it is better to take the object
				if (cap - w[i] >= 0)
					dp[k][cap] = Math.max(dp[k][cap], dp[k^1][cap - w[i]] + p[i]);
			}
		}
		
		return dp[k][W];

	}

	// dp - bottom-up version
	// T(n) = O(n*W) , S(n) = O(n*W);
	private int knapsack(int[] w, int[] p, int W) {
		if (w == null || p == null || w.length == 0 || w.length != p.length)
			return -1;
		
		int n = w.length;
		int[][] dp = new int[n][W+1];
		for (int cap = 0; cap <= W; cap++)
			dp[n-1][cap] = cap >= w[n-1] ? p[n-1] : 0;
		
		for (int cap = 0; cap <= W; cap++) 
			for (int i = n - 2; i >= 0; i--) {
			
				if (cap < w[i]) {
					dp[i][cap] = dp[i+1][cap];
					continue;
				}
				
				dp[i][cap] = Math.max(dp[i+1][cap], dp[i+1][cap - w[i]] + p[i]);
			}
		
		return dp[0][W];
	}

	// brute-force version
	// T(n) = O(2^n)
	private int knapsack(int i, int remCap, int[] w, int[] p) {
		int n = w.length;
		if (i == n || remCap <= 0)
			return 0;
		
		// no room for current object
		if (remCap < w[i])
			return knapsack(i+1, remCap, w, p);

		// maximize profit
		int take = knapsack(i+1, remCap - w[i], w, p) + p[i];
		int notake = knapsack(i+1, remCap, w, p);
		return Math.max(take, notake);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {

		int[] w = {3, 3, 1, 1, 2};
		int[] p = {6, 3, 2, 8, 5};
		int W = 5;
/*
		int[] w = {1, 1, 1};
		int[] p = {10, 20, 30};
		int W = 2;
*/
		Knapsack t = new Knapsack(); 
		int maxProfit = t.knapsack(0, W, w, p);
		System.out.println("Max profit(by brute-force):" + maxProfit);
		maxProfit = t.knapsack(w, p, W);
		System.out.println("Max profit (by DP):" + maxProfit);
		maxProfit = t.knapsack_(w, p, W);
		System.out.println("Max profit (by improved DP):" + maxProfit);
	}
}
