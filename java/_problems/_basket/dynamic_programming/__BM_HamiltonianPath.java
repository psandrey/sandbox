package _problems._basket.dynamic_programming;

//DP with Bit Masking

/*
 * Hamiltonian Path is a path in a directed or undirected graph that visits
 * each vertex exactly once. The problem to check whether a graph (directed or undirected)
 * contains a Hamiltonian Path is NP-complete, so is the problem of finding all
 * the Hamiltonian Paths in a graph.
 * 
 * Note: This problem can be implemented in at least 3 ways:
 *       1. Brute-force: generating all permutation of n vertices and check if they
 *          form a Hamiltonian Path: T(n) = O(n.n!)
 *       2. Using backtracking: T(n) = O(n!)
 *       3. Using dynamic programming with bit mask: T(n) = O(n^2.2^n)
 */

public class __BM_HamiltonianPath {

	// Time complexity: T(n) = O(n^2.2^n)
	public boolean graphHasHamiltonianPath(int[][] a, int n) {
		int set_size = (1<<n);

		boolean[][] dp = new boolean[set_size][n];
		for (int mask = 0; mask < set_size; mask++)
			for (int i = 0; i < n; i++)
				dp[mask][i] = false;
		
		// the subset made of only one vertex is a HP
		for (int i = 0; i < n; i++) dp[1<<i][i] = true;
		
		// for all subsets
		//  for all j vertices in the current subset
		//    for all k vertices in the current subset different from current vertex j
		//      if there is an edge from i to j then there is a Hamiltonian Path 
		//      in this subset, so we can seek for another HP
		//
		// Note: The algorithm finds all HP for each subset
		for (int mask = 0; mask < set_size; mask++) {
			for (int j = 0; j < n; j++) {
				// if the vertex is not contained in the subset, then continue;
				if ( (mask & (1<<j)) == 0) continue;
				
				for (int k = 0; k < n; k++) {
					 // if k is the same as j, or k is not in the current subset,
					// then exclude it
					if (j == k || (mask&(1<<k)) == 0) continue;
					
					// if the current subset w/o j is a HP ending with k, and if there is
					//  an edge from k to j, then there is a HP in the current subset
					//  and ending with j
					int maskj = mask ^ (1<<j);
					if (isEdge(a, k, j) == true && dp[maskj][k] == true) {
						dp[mask][j] = true;
						break;
					}					
				}
			}
		}
		
		// check if there is a HP in the set ending with any of the n vertices
		for (int i = 0; i < n; i++)
			if (dp[set_size-1][i]) return true;
		
		return false;
	}
	
	// undirected graph
	private boolean isEdge(int[][] edges, int u, int v) {
		int n = edges.length;
		
		for (int i = 0; i < n; i++) {
			if ((edges[i][0] == u && edges[i][1] == v)
					|| (edges[i][0] == v && edges[i][1] == u))
				return true;
		}
		
		return false;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int n = 4;
		int[][] edges = new int[][] {
			{0,2},
			{1,2},
			{2,3}};

		__BM_HamiltonianPath t = new __BM_HamiltonianPath();
		System.out.println("Does the graph contain a Hamiltonian Path? ");
		System.out.println(t.graphHasHamiltonianPath(edges, n));
	}
}
