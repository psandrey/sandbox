package _problems._basket.combinatorics;

/*
 * Given a set of candidate numbers (C) and a target number (T),
 * find all unique combinations in C where the candidate numbers sums to T.
 * The same repeated number may be chosen from C unlimited number of times. 
 * 
 * Note: All numbers (including target) will be positive integers.
 * Elements in a combination (a1, a2, ... , ak) must be in non-descending order.
 * (ie, a1 <= a2 <= ... <= ak).
 * The solution set must not contain duplicate combinations.
 * 
 * For example, given candidate set 2,3,6,7 and target 7, A solution set is:
 * [7], [2,2,3]
 */

import java.util.LinkedList;

public class CombinationSum {

	private LinkedList<LinkedList<Integer>> generateSum(int[] a, int t) {
		LinkedList<LinkedList<Integer>> result = new LinkedList<LinkedList<Integer>>();
		if (a == null || a.length == 0)
			return result;
		
		LinkedList<Integer> L = new LinkedList<Integer>();
		generateSum(result, L, t, a, 0);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private void generateSum(LinkedList<LinkedList<Integer>> result, LinkedList<Integer> L, int rem, int[] a, int k) {
		
		if (rem == 0) {
			result.add((LinkedList<Integer>)L.clone());
			return;
		} else if (rem < 0 || k == a.length)
			return;
		
		int n = rem/a[k];
		for (int i = 0; i <= n; i++) {
			for (int j = 1; j <=i; j++)
				L.add(a[k]);
			generateSum(result, L, rem - i*a[k], a, k+1);
			for (int j = 1; j <=i; j++)
				L.remove();
		}
	}
	
	private void printLLLL(LinkedList<LinkedList<Integer>> a) {
		for (int i = 0; i < a.size(); i++)
			printLL(a.get(i));
	}

	private void printLL(LinkedList<Integer> a) {
		int n = a.size();
		System.out.print("[ ");
		for (int i = 0; i < n; i++)
			System.out.print(a.get(i) + ",");
		System.out.print(" ]");
		System.out.println();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		CombinationSum cs = new CombinationSum();
		int[] a = {2, 3, 6, 7};
		int t = 7;
		LinkedList<LinkedList<Integer>> result = cs.generateSum(a, t);
		cs.printLLLL(result);
	}
}
