package _problems._basket.combinatorics;

/* 
 * Given a digit string, return all possible letter combinations that
 * the number could represent. (Check out your cellphone to see the mappings).
 * 
 * Input:Digit string "23",
 * Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
 */

import java.util.Arrays;
import java.util.LinkedList;

public class PhoneNumberLetterCombinations {

	private LinkedList<String> letterCombinations(String digits) {
		LinkedList<String> result = new LinkedList<String>();
		if (digits == null || digits.length() == 0)
			return result;
		String[] dict = new String[]{"","","abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};

		int[] c = new int[digits.length()];
		result.add(processSolution(digits, dict, c)); // O(n)
		
		int k = digits.length() - 1;
		while (k >= 0) { // O(k^n)
			int d = digits.charAt(k) - '0';
			if (c[k] < dict[d].length() - 1) { // O(1)
				c[k]++;
				Arrays.fill(c, k+1, digits.length(), 0); // O(n)
				k = digits.length() - 1;
				
				result.add(processSolution(digits, dict, c)); // O(n)
			} else
				k--;
		}
		// O(n.k^n)
		
		return result;
	}
	
	private String processSolution(String digits, String[] dict, int[] c) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < c.length; i++) { // O(n)
			int d = digits.charAt(i) - '0'; // charAt is constant time
			sb.append(dict[d].charAt(c[i])); // O(1)
		}

		return sb.toString();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		PhoneNumberLetterCombinations t = new PhoneNumberLetterCombinations();
		String digits = "234";
		LinkedList<String> L = t.letterCombinations(digits);
		for(int i = 0; i < L.size(); i++)
			System.out.println(L.get(i));
	}
}
