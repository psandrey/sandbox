package _problems._basket.combinatorics;

/*
 * Permutations of an array of integers:
 *     1. Lexicographic (method used in C++:std)
 *     2. Iteration method
 *     3. Decrease and conquer method
 *     4. Based on classic backtracking
 * 
 * K-permutation of an array of integers: TODO
 */

import java.util.LinkedList;
import java.util.Stack;

public class __Permutations {

	// Generate permutations in lexicographic form [method implemented in c++:std]
	private LinkedList<Integer []>  permutationsLex(Integer[] a) {
		LinkedList<Integer []> result = new LinkedList<Integer []>();
		
		if (a == null || a.length < 2)
			result.add(a);
		else
			generatePermutationsLex(result, a);
		
		return result;
	}
	
	private void generatePermutationsLex(LinkedList<Integer []> result, Integer[] a) {
		int n = a.length;
		Integer[] p = new Integer[n];
		for(int i = 0; i < n; i++)
			p[i] = i;
		
		result.add(setNewPermutation(a, p));
		while (true) {
			// find the largest index such that p[k] < p[k+1]
			int k = n - 2;
			while (k >= 0 && p[k] > p [k+1]) k--;
			
			// if there is no such index, that means we've generated all permutations
			if (k < 0)
				return;
			
			// find the largest index such that p[l] > p[k]
			int l = n - 1;
			while (p[l] < p[k]) l--;
			
			// swap and reverse
			swap(p, k, l);
			reverse(p, k+1);
			
			// save the new permutation
			result.add(setNewPermutation(a, p));
		}	
	}
	
	private void reverse(Integer[] a, int start) {
		int i = start;
		int j = a.length - 1;
		while (i<j) {
			swap(a, i, j);
			i++;j--;
		}
	}
	
	private Integer[] setNewPermutation(Integer[] a, Integer[] p) {
		Integer[] newA = new Integer[a.length];
		for (int i = 0; i < a.length; i++)
			newA[i] = a[p[i]];
		
		return newA;
	}
	
	// Using insertion method
	private LinkedList<LinkedList<Integer>> permutationsInsert(Integer[] a) {
		LinkedList<LinkedList<Integer>> result = new LinkedList<LinkedList<Integer>>();
		result.add(new LinkedList<Integer>());

		for (int i = 0 ; i < a.length; i++ ) {
			int key = a[i];
			
			int size = result.size();
			LinkedList<LinkedList<Integer>> newResult = new LinkedList<LinkedList<Integer>>();
			for (int j = 0; j < size; j++) {
				LinkedList<Integer> cList = result.get(j);
				
				for (int k = 0; k < cList.size()+1; k++) {
					LinkedList<Integer> newList = new LinkedList<Integer>(cList);
					newList.add(k, key);
					newResult.add(newList);
				}
			}
			result = newResult;
		}

		return result;
	}
	
	// using "decrease and conquer" ( mind the definition P(n) = n * (n-1)! ):
	//   1. divide the problem into a problem of size 1 and a problem of size (n-1)
	//   2. solve the sub-problem of size (n-1) recursively
	//   3. finally, add the remaining individual back to the sub-problem' solution
	private LinkedList<Integer []> permutationsDC(Integer[] a) {
		LinkedList<Integer []> result = new LinkedList<Integer []>();
		
		if (a == null || a.length < 2)
			result.add(a);
		else
			permutationsDC(result, a, a.length);
		
		return result;
	}
	
	private void permutationsDC(LinkedList<Integer []> result, Integer[] a, int n) {
		if (n == 1) {
			result. add(a.clone());
			return;
		}
		
		for(int i = 0; i < n; i++) {
			swap(a, i, n-1);
			permutationsDC(result, a, n-1);
			swap(a, i, n-1);
		}
	}
	
	private void swap(Integer a[], int i, int j) {
		int t = a[i];
		a[i] = a[j];
		a[j] = t;
	}
	
	// using standard backtracking method
	private LinkedList<Integer []> permutationsBck(Integer[] a) {
		LinkedList<Integer []> result = new LinkedList<Integer []>();
		
		if (a == null || a.length < 2)
			result.add(a);
		else
			permutationsBck(result, a);
			
		return result;
	}

	private void permutationsBck(LinkedList<Integer []> result, Integer[] a) {
		int n = a.length;
		Stack<Integer> st = new Stack<Integer>();
		st.push(-1);
		while (!st.isEmpty()) {
			boolean isSucc = false;
			
			// loop until we have a valid successor
			do {
				isSucc = isSuccessor(st, n-1);
			} while (isSucc && !isValid(st));

			// if the successor is valid, we check if we have a solution to process
			if (isSucc == true) {
				if (isSolution(st, n)) {
					Integer[] sol = processSolution(st, a);
					result.add(sol); 
				} else
					st.push(-1);
				
			// so, we do not have any successor on this level, thus backtracks
			} else
				st.pop();
		}
	}
	
	private boolean isSuccessor(Stack<Integer> st, int limit) {
		int top = st.peek();
		if (top >= limit)
			return false;

		st.set(st.size() - 1, ++top);
		return true;
	}
	
	private boolean isValid(Stack<Integer> st) {
		int top = st.peek();
		int n = st.size();
		for (int i = 0; i < n-1; i++)
			if (top == st.get(i))
				return false;

		return true;
	}
	
	private boolean isSolution(Stack<Integer> st, int n) {
		if (st.size() == n)
			return true;
		return false;
	}
	
	private Integer[] processSolution(Stack<Integer> st, Integer[] a) {
		int n = a.length;
		Integer[] sol = new Integer[n];
		
		for (int i = 0; i < n; i++)
			sol[i] = a[st.get(i)];

		return sol;
	}
	
	private void printLLLL(LinkedList<LinkedList<Integer>> a) {
		for (int i = 0; i < a.size(); i++)
			printLL(a.get(i));
	}

	private void printLL(LinkedList<Integer> a) {
		int n = a.size();
		System.out.print("[ ");
		for (int i = 0; i < n; i++)
			System.out.print(a.get(i) + ",");
		System.out.print(" ]");
		System.out.println();
	}
	
	private void printLLArr(LinkedList<Integer []> a) {
		for (int i = 0; i < a.size(); i++)
			printArray(a.get(i));
	}
	
	private void printArray(Integer[] a) {
		int n = a.length;
		System.out.print("[ ");
		for (int i = 0; i < n; i++)
			System.out.print(a[i]+",");
		System.out.print(" ]");
		System.out.println();
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[] a = {1, 2, 3, 4};
		__Permutations t = new __Permutations();

		LinkedList<Integer []> permsArr = t.permutationsBck(a);
		System.out.println("> Backtracking:" + permsArr.size());
		t. printLLArr(permsArr);
		
		
		LinkedList<LinkedList<Integer>> persLists = t.permutationsInsert(a);
		System.out.println("> Insertion:" + persLists.size());
		t.printLLLL(persLists);

		permsArr = t.permutationsDC(a);
		System.out.println("> Decrease and Conquer:" + permsArr.size());
		t. printLLArr(permsArr);

		permsArr = t.permutationsLex(a);
		System.out.println("> Lexicographic:" + permsArr.size());
		t. printLLArr(permsArr);
	}
}
