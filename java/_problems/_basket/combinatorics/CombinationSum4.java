package _problems._basket.combinatorics;

/*
 * Given an integer array with all positive numbers and no duplicates,
 * find the number of possible combinations that add up to a positive integer target.
 * 
 * Example:
 * [1, 2, 3] , target = 4;
 * 
 * (1, 1, 1, 1)
 * (1, 1, 2)
 * (1, 2, 1)
 * (1, 3)
 * (2, 1, 1)
 * (2, 2)
 * (3, 1)
 * 
 * return: 7
 */

import java.util.Arrays;

public class CombinationSum4 {

	// T(n) = O(target * n)
	private int combinationSumDP_TopDown(int[] a, int target) {
		if (a == null || a.length == 0)
			return 0;
		int[] dp = new int[target + 1];
		Arrays.fill(dp, -1);
		dp[0] = 1;
		return combinationSumDP_TopDown(a, target, dp);
	}	
	
	private int combinationSumDP_TopDown(int[] a, int rem, int[] dp) {
		if (dp[rem] != -1)
			return dp[rem];
		
		int sols = 0;
		for(int j = 0; j < a.length; j++) {
			if (a[j] > rem)
				continue;
			sols += combinationSumDP_TopDown(a, rem-a[j], dp);
		}
		
		return sols;
	}
	
	// T(n) = O(target * n)
	private int combinationSumDP_BottomUp(int[] a, int target) {
		int[] dp = new int[target+1];
		dp[0] = 1;
		
		for(int i = 0; i <= target; i++) {
			for(int j = 0; j < a.length; j++) {
				if (a[j] > i)
					continue;
				
				dp[i] += dp[i-a[j]];
			}
		}
		
		return dp[target];
	}
	
	// T(n) = O(S^n)
	private int combinationSum(int[] a, int rem) {
		if(rem == 0) 
			return 1;
		
		int result = 0;
		for (int i = 0; i < a.length; i++) {			
			if (rem < a[i])
				continue;
			result += combinationSum(a, rem - a[i]);
		}
		
		return result;
	}
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		CombinationSum4 cs = new CombinationSum4();
		int[] a = {1, 2, 3};
		int t = 4;

		System.out.println("Brute force:" + cs.combinationSum(a, t));
		System.out.println("DP bottom-up:" + cs.combinationSumDP_BottomUp(a, t));
		System.out.println("DP Top-down:" + cs.combinationSumDP_TopDown(a, t));
	}	
}
