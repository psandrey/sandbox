package _problems._basket.combinatorics;

/*
 * Combination of k items of an array of integers:
 *     1. Lexicographic method T(n) = O(k.n!)
 *     2. Recursive method
 *     3. Based on binary representation method
 *     4. Based on classic backtracking method
 */

import java.util.LinkedList;
import java.util.Stack;

public class __Combinations {
	
	// Lexicographic iterative version
	private LinkedList<Integer[]> combinationsIt(Integer[] a, int k) {
		LinkedList<Integer[]> result = new LinkedList<Integer[]>();
		if (a == null || a.length < 2 || k > a.length)
			return result;
		
		combinationsIt(result, a, k);
		return result;		
	}
	
	private void combinationsIt(LinkedList<Integer[]> result, Integer[] a, int k) {
		Integer[] c = new Integer[k];
		for (int i = 0; i < k; i++) c[i] = i;
		result.add(processSolution(c, a, k));

		int n = a.length;		
		int i = k - 1; // index starts from 0
		while ( i >= 0) {
			if (c[i] < n - k + i) {
				c[i]++;
				for(int j = i+1; j < k; j++)
					c[j] = c[j-1] + 1;
				
				result.add(processSolution(c, a, k));
				i = k - 1;
			} else
				i--;
		}
	}
	
	private Integer[] processSolution(Integer[] c, Integer[] a, int k) {
		Integer[] sol = new Integer[k];
		for (int i = 0; i < k; i++)
			sol[i] = a[c[i]];
		
		return sol;
	}
	
	// recursive method
	private LinkedList<Integer[]> combinationsRec(Integer[] a, int k) {
		LinkedList<Integer[]> result = new LinkedList<Integer[]>();
		if (a == null || a.length < 2 || k > a.length)
			return result;
		
		Integer[] c = new Integer[k];
		combinationsRec(result, c, 0, a, 0, k);
		return result;		
	}
	
	private void combinationsRec(LinkedList<Integer[]> result,
			Integer[] c, int idx, Integer[] a, int j, int k) {
		if (idx == k) {
			result.add(c.clone());
			return;
		}
		
		if (j == a.length)
			return;
		
		// we take element j
		c[idx] = a[j];
		combinationsRec(result, c, idx + 1, a, j + 1, k );
		
		// we don't take element j
		combinationsRec(result, c, idx    , a, j + 1, k );
	}

	// using binary representation
	private LinkedList<Integer[]> combinationsBin(Integer[] a, int k) {
		LinkedList<Integer[]> result = new LinkedList<Integer[]>();
		if (a == null || a.length < 2 || k > a.length || k > 32 )
			return result;
		
		combinationsBin(result, a, k);
		return result;		
	}
	
	private void combinationsBin(LinkedList<Integer[]> result, Integer[] a, int k) {
		int n = a.length;
		int limit = 1<<n;
		for (int i = 0; i <= limit - 1; i++)
			if (bitCount(i) == k) {
				Integer[] sol = processSolution(i, a, k);
				result.add(sol);
			}
	}

	private int bitCount(int x) {
		int n = 0;
		while (x > 0) {
			if ((x&1) == 1)
				n++;
			x >>= 1;
		}
		return n;
	}
	
	private Integer[] processSolution(int x, Integer[] a, int k) {
		Integer[] sol = new Integer[k];
		
		int bit = 0;
		int idx = 0;
		while (x > 0) {
			if ((x&1) == 1) sol[idx++] = a[bit];
			x >>= 1;
			bit++;
		}
		
		return sol;
	}
	
	// using standard backtracking method
	private LinkedList<Integer[]> combinationsBck(Integer[] a, int k) {
		LinkedList<Integer[]> result = new LinkedList<Integer[]>();
		if (a == null || a.length < 2 || k > a.length)
			return result;
		
		combinationsBck(result, a, k);
		return result;
	}

	private void combinationsBck(LinkedList<Integer[]> result, Integer[] a, int k) {
		int n = a.length;
		Stack<Integer> st = new Stack<Integer>();
		
		st.push(-1);
		while (!st.isEmpty()) {
			boolean isSucc = false;
			do {
				isSucc = isSuccessor(st, n - 1);
			} while (isSucc && !valid(st));
			if (isSucc) {
				if (solution(st, k)) {
					Integer[] sol = processSolution(st, a, k);
					result.add(sol);
				} else
					st.push(-1);
			} else
				st.pop();
		}
	}

	private boolean isSuccessor(Stack<Integer> st, int limit) {
		Integer top = st.peek();
		if (top < limit) {
			st.set(st.size() - 1, ++top);
			return true;
		}

		return false;
	}
	
	private boolean valid(Stack<Integer> st) {
		if (st.size() < 2)
			return true;
		
		if (st.peek() <= st.get(st.size() -2))
			return false;
		
		return true;
	}
	
	private boolean solution (Stack<Integer> st, int limit) {
		if (st.size() == limit)
			return true;
		return false;
	}
	
	private Integer[] processSolution(Stack<Integer> st, Integer[] a, int k) {
		Integer[] sol = new Integer[k];
		for (int i = 0; i < k; i++)
			sol[i] = a[st.get(i)];
		
		return sol;
	}
	
	private void printLLArr(LinkedList<Integer []> a) {
		for (int i = 0; i < a.size(); i++)
			printArray(a.get(i));
	}
	
	private void printArray(Integer[] a) {
		int n = a.length;
		System.out.print("[ ");
		for (int i = 0; i < n; i++)
			System.out.print(a[i]+",");
		System.out.print(" ]");
		System.out.println();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Integer[] a = {1, 2, 3, 4};
		int k = 3;
		__Combinations t = new __Combinations();
		LinkedList<Integer []> combArr = t.combinationsBck(a, k);
		System.out.println("> Combinations (classic backtracking version):"+ combArr.size());
		t.printLLArr(combArr);
		
		combArr = t.combinationsBin(a, k);
		System.out.println("> Combinations (binary version):" + combArr.size());
		t.printLLArr(combArr);
		
		combArr = t.combinationsIt(a, k);
		System.out.println("> Combinations (Iterative version):" + combArr.size());
		t.printLLArr(combArr);

		combArr = t.combinationsRec(a, k);
		System.out.println("> Combinations (Recursive version):" + combArr.size());
		t.printLLArr(combArr);

	}
}

