package _problems._basket.combinatorics;

/*
 * Given n pairs of parentheses, write a function to generate
 *  all combinations of well-formed parentheses.
 */

import java.util.LinkedList;
//import java.util.Stack;

public class GenerateParentheses {

	private LinkedList<String> generate(int n) {
		LinkedList<String> result = new LinkedList<String>();
		if (n == 0)
			return result;
		if (n == 1) {
			result.add("()");
			return result;
		}
		
//		Stack<Character> st = new Stack<Character>();
		String s = new String();
		generate(result, s, n, 0);
		return result;
	}
/*
	private void generate(LinkedList<String> result,Stack<Character> st, int rema, int remb) {
		if (rema == 0 && remb == 0) {
			result.add(st.toString());
			return;
		}
	
		if (rema > 0) {
			st.push('(');
			generate(result, st, rema - 1, remb + 1);
			st.pop();
		}
		
		if (remb > 0) {
			st.push(')');
			generate(result, st, rema, remb - 1);
			st.pop();
		}
	}
*/
	
	private void generate(LinkedList<String> result, String s, int rema, int remb) {
		if (rema == 0 && remb == 0) {
			result.add(s);
			return;
		}
	
		if (rema > 0)
			generate(result, s + "(", rema - 1, remb + 1);
		
		if (remb > 0)
			generate(result, s + ")", rema, remb - 1);
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int n = 3;
		GenerateParentheses t = new GenerateParentheses();
		LinkedList<String> l = t.generate(n);
		for (String s:l)
			System.out.println(s);
	}
}
