package _problems._basket;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

public class RoadsInHackerLand {
	static class Pair{
		int u;
		int k;
		Pair(int u, int k) { this.u = u; this.k = k; }
	};
	
	static int find(int u, int[] p) {
		if (p[u] != u) p[u] = find(p[u], p);		
		return p[u];
	}
	
	static void union(int u, int v, int[] p) {
		int iu = find(u, p);
		int iv = find(v, p);
		p[iv] = iu;
	}
	
	static int DFS(int u, int p, int n, HashMap<Integer, ArrayList<Pair>> adjL, int[] times) {
		int t = 1;
		for (Pair x : adjL.get(u)) {
			int v = x.u, k = x.k;
			if (v == p) continue;
			
			int c = DFS(v, u, n, adjL, times);
			t += c;
			times[k] = c * (n - c);
		}
		
		return t;
	}
	
	static String roadsInHackerland(int n, int[][] e) {
		Comparator<int[]> comp = new Comparator<int []>() {
			public int compare(int[] a, int[] b) {
				return (a[2] - b[2]);
			}
		};
		Arrays.sort(e, comp);

		
		HashMap<Integer, ArrayList<Pair>> adjL = new HashMap<Integer, ArrayList<Pair>>();
		
		int m = e.length;
		int[] p = new int[n];
		for (int i = 0; i < n; i++) p[i] = i;
		for (int i = 0; i < m; i++) {
			int u = e[i][0] - 1, v = e[i][1] - 1;
			if (find(u, p) != find(v, p)) {
				union(u, v, p);
				if (!adjL.containsKey(u)) adjL.put(u, new ArrayList<Pair>());
				if (!adjL.containsKey(v)) adjL.put(v, new ArrayList<Pair>());
				
				adjL.get(u).add(new Pair(v, i));
				adjL.get(v).add(new Pair(u, i));
			}
		}
		
		int[] times = new int[m];
		DFS(0, -1, n, adjL, times);
		
		BigInteger ans = new BigInteger("0");
		for (int i = 0; i < m ; i++) {
			if (times[i] == 0) continue;
			BigInteger T = new BigInteger(String.valueOf(times[i]));
			BigInteger W = new BigInteger("1").shiftLeft(e[i][2]);
			ans = ans.add(W.multiply(T));
		}
		
		return ans.toString(2);
    }
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[][] e = {
				{1, 3, 5},
				{4, 5, 0},
				{2, 1, 3},
				{3, 2, 1},
				{4, 3, 4},
				{4, 2, 2}};
		int n = 5;
		int m = 6;
		
		System.out.println(roadsInHackerland(n, e));
		
	}
}
