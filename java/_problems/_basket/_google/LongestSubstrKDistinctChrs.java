package _problems._basket._google;

/*
 * Given a string, find the longest substring that contains k unique characters. For example,
 *  given "abcbbbbcccbdddadacb", the longest substring that contains K=2 unique character is "bcbbbbcccb".
 */

import java.util.HashMap;

public class LongestSubstrKDistinctChrs {

	private int start;
	private int end;

	private int do_longestSubstrKDistinctChrs(String s, int k) {
		int max=0;
		HashMap<Character,Integer> map = new HashMap<Character, Integer>();

		start = 0;
		end = 0;
		int i = 0, j = 0;
		for(i = 0; i < s.length(); i++){
			char c = s.charAt(i);

			if(map.containsKey(c))
				map.put(c, map.get(c) + 1);
			else
				map.put(c,1);
	 
			if(map.size() > k){
				if (max < i-j) {
					max = i - j;
					start = j;
					end = i;					
				}
	 
				while(map.size() > k){ //O(K), since K is constant, so O(n) is preserved.
					char t = s.charAt(j);
					int count = map.get(t);

					if(count > 1)
						map.put(t, count-1);
					else
						map.remove(t);

					j++;
				}
			}
		}

		if (max < s.length() - j) {
			max = s.length() - j;
			start = j;
			end = s.length();					
		}

		max = Math.max(max, s.length()-j);
		return max;
	}

	private void info(String s) {
		System.out.print(s);
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) { 
		//String s = "abacaac"; // k=2 => (2,6) = 5
		//int K = 2;
		String s = "abcbbbbcccbdddadacb"; // k=2 => (1,10) = 10
		int K = 2;

		LongestSubstrKDistinctChrs lSubstr = new LongestSubstrKDistinctChrs();
		int l = lSubstr.do_longestSubstrKDistinctChrs(s, K);
		
		lSubstr.info("(" + lSubstr.start + "," + lSubstr.end + ")=" + String.valueOf(l) + " : " + 
				s.substring(lSubstr.start, lSubstr.end));
	}
}
