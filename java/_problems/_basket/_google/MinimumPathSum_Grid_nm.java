package _problems._basket._google;

/*
 * Given a m x n grid filled with non-negative numbers, find a path from top left to bottom right
 *  which minimizes the sum of all numbers along its path.
 */

public class MinimumPathSum_Grid_nm {

	private int[][] doMinPathSum(int grid[][]) {
		int n = grid.length;
		int m = grid[0].length;

		int[][] dp = new int[n][m];
		
		dp [0][0] = grid[0][0];
		for (int i = 1; i < n; i++)
			dp[i][0] = grid[i][0] + dp[i-1][0];

		for (int j = 1; j < m; j++)
			dp[0][j] = grid[0][j] + dp[0][j];

		for (int i = 1; i < n; i++)
			for (int j = 1; j < m; j++) {
				if (dp[i-1][j] > dp[i][j-1])
					dp[i][j] = grid[i][j] + dp[i][j-1];
				else
					dp[i][j] = grid[i][j] + dp[i-1][j];
			}

		return dp;
	}

	private void info(String s) {
		System.out.print(s);
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) { 
		int grid[][] = {
				{1, 1, 10},
				{2, 10, 10},
				{1, 1, 1}};
		MinimumPathSum_Grid_nm minPathSum = new MinimumPathSum_Grid_nm();
		int dp[][] = minPathSum.doMinPathSum(grid);

		int i = grid.length - 1;
		int j = grid[0].length - 1;
		while (i !=0 || j != 0) {
			minPathSum.info("(" + i + "," + j + ")=" + dp[i][j] + ", ");
			if ((i > 0 && j > 0 && dp[i-1][j] > dp[i][j-1]) || (i==0))
				j--;
			else
				i--;
		}
		minPathSum.info("(" + i + "," + j + ")=" + dp[i][j] + ", ");
	}
}
