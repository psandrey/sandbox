package _problems._basket._google;

/*
 * Given two integers representing the numerator and denominator of a fraction,
 * return the fraction in string format.
 */

import java.util.HashMap;

public class FractionToRecurringDecimal {

	private String do_fractionToRecurringDecimal(int n, int d) {
		if (n == 0)
			return "0";

		String res = "";
		if (n < 0 ^ d < 0)
			res += "-";

		n = Math.abs(n);
		d = Math.abs(d);
		
		int q = n / d;
		res += String.valueOf(q);

		int r = n % d;
		if (r == 0)
			return res;
		r = r * 10;

		HashMap<Integer, Integer> mp = new HashMap<Integer, Integer>();
		res += ".";
		while (r != 0) {
			if (mp.containsKey(r)) {
				int lenTillRecurring = mp.get(r);
				String p1 = res.substring(0, lenTillRecurring);
				String p2 = res.substring(lenTillRecurring, res.length());
				res = p1 + "(" + p2 + ")";
				return res;
			}

			mp.put(r, res.length());
			int digit = r / d;
			res += String.valueOf(digit);
			r = (r % d) * 10;
		}
		
		return res;
	}

	private void println(String s) {
		System.out.println(s);
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//int n = 7;
		//int d = 12;
		int n = -9;
		int d = 11;

		FractionToRecurringDecimal t = new FractionToRecurringDecimal();
		String res = t.do_fractionToRecurringDecimal(n, d);
		t.println(res);
	}
}
