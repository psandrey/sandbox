package _problems._basket._google;

/*
 * A robot is located at the top-left corner of a m x n grid.
 * It can only move either down or right at any point in time.
 * The robot is trying to reach the bottom-right corner of the grid.
 * The grid may contain obstacles.
 * 
 * How many possible unique paths are there?
 */

public class UniquePathsRobo_withObstacles {

	private int uniquePathsRobo(int grid[][]) {
		if (grid == null || grid.length == 0)
			return 0;

		int n = grid.length;
		int m = grid[0].length;
		
		if (grid[0][0] == 1 || grid[n-1][m-1] == 1)
			return 0;
		
		int dp[][] = new int[n][m];
		dp[0][0] = 1;
		for (int i = 1; i < n; i++)
			if (grid[i][0] == 1)
				dp[i][0] =0;
			else
				dp[i][0] = dp[i-1][0];
		for (int j = 1; j < m; j++)
			if (grid[0][j] == 1)
				dp[0][j] =0;
			else
				dp[0][j] = dp[0][j-1];
		for (int i = 1; i < n; i++)
			for (int j = 1; j < m; j++)
				if (grid[i][j] == 1)
					dp[i][j] = 0;
				else
					dp[i][j] = dp[i-1][j] + dp[i][j-1];

		return dp[n-1][m-1];
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		int grid[][] = {
				{0, 0, 0},
				{0, 1, 0},
				{0, 0, 0}};
		UniquePathsRobo_withObstacles t = new UniquePathsRobo_withObstacles();
		int paths = t.uniquePathsRobo(grid);
		System.out.println("Number of unique paths:" + paths);
	}
}
