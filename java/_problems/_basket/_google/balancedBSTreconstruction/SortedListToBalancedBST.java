package _problems._basket._google.balancedBSTreconstruction;

/*
 * Given a singly linked list where elements are sorted in ascending order,
 * convert it to a height balanced BST.
 */

public class SortedListToBalancedBST {

	private class ListNode {
		public ListNode next;
		public int data;

		public ListNode(int data) {
			this.data = data;
			this.next = null;
		}	
	};

	private class TreeNode {
		public TreeNode left;
		public TreeNode right;
		public int data;

		public TreeNode(int data) {
			this.data = data;
			this.left = null;
			this.right = this.left;
			this.left = this.right;
		}
	};
	
	private ListNode getKthElement(ListNode L, int k) {
		if (L == null) return null;
		ListNode n = L;

		int i = 0;
		while (n.next != null && i++ < k)
			n = n.next;

		return n;
	}

	private TreeNode buildBST(ListNode a, int n) {
		if (n < 0)
			return null;
		if (n == 0)
			return new TreeNode(a.data);

		int k = n / 2;
		ListNode kItem = getKthElement(a, k);

		TreeNode node = new TreeNode(kItem.data);
		node.left  = buildBST(a         , k - 1);
		node.right = buildBST(kItem.next, n - (k - 1));

		return node;
	}
	
	private ListNode buildLinkedList(int n) {
		ListNode L = new ListNode(0);
		ListNode prev = L;
		for (int i = 1; i < n; i ++) {
			ListNode node = new ListNode(i);
			prev.next = node;
			prev = node;
		}
		
		return L;
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		SortedListToBalancedBST t = new SortedListToBalancedBST();
		int n = 4;
		ListNode L = t.buildLinkedList(n);

		TreeNode root = t.buildBST(L, n - 1);
		System.out.println(root.data);
	}
}
