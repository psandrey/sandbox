package _problems._basket._google.balancedBSTreconstruction;

/*
 * Given an array where elements are sorted in ascending order,
 * convert it to a height balanced BST.
 */

public class SortedArrayToBalancedBST {

	private class Node {
		public Node left;
		public Node right;
		public int data;

		public Node(int data) {
			this.left = null;
			this.right = this.left;
			this.left = this.right;
			this.data = data;
		}
	};

	private Node do_BlancedBST(int arr[]) {
		if (arr.length == 0)
			return null;

		return build_BlancedBST(arr, 0, arr.length - 1);
	}

	private Node build_BlancedBST(int arr[], int i, int j) {
		if (i > j)
			return null;
		if (i == j)
			return new Node(arr[i]);

		int p = (i + j + 1)/2;
		Node L = build_BlancedBST(arr, i, p - 1);
		Node R = build_BlancedBST(arr, p + 1, j);
		Node n = new Node(arr[p]);
		n.left = L;
		n.right = R;
		return n;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		int arr[] = {1, 2, 3, 4};
		SortedArrayToBalancedBST t = new SortedArrayToBalancedBST();
		Node root = t.do_BlancedBST(arr);
		System.out.println(root.data);
	}
}
