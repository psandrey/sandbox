package _problems._basket._google;

/*
 * You are a professional robber planning to rob houses along a street.
 * Each house has a certain amount of money stashed, the only constraint
 * stopping you from robbing each of them is that adjacent houses have
 * security system connected and it will automatically contact the police
 * if two adjacent houses were broken into on the same night.
 * 
 * Given a list of non-negative integers representing the amount of money
 * of each house, determine the maximum amount of money you can rob tonight
 * without alerting the police.
 */

public class DP_HouseRobber {

	private int do_HouseRobber_v1(int[] m) {
		if (m == null || m.length == 0)
			return 0;

		int n = m.length;
		if (n == 1)
			return m[0];
		else if (n == 2)
			return Math.max(m[0], m[1]);
		else if (n == 3)
			return Math.max(m[0] + m[1], m[2]);
		
		
		int[] dp = new int[n];
		dp[0] = m[0];
		dp[1] = m[1];
		dp[2] = m[0] + m[2];
		for (int i = 3; i < n; i++)
			dp[i] = Math.max(dp[i-2], dp[i-3]) + m[i];

		return Math.max(dp[n-2], dp[n-1]);
	}

	private int do_HouseRobber_v2(int[] m) {
		if (m == null || m.length == 0)
			return 0;

		int n = m.length;
		if (n == 1)
			return m[0];		
		
		int[] dp = new int[n];
		dp[0] = m[0];
		dp[1] = Math.max(m[0], m[1]);
		for (int i = 2; i < n; i++)
			dp[i] = Math.max(dp[i-1], dp[i-2] + m[i]);
		
		return dp[n-1];
	}

	private void print(String s) {
		System.out.print(s);
	}

	private void println(String s) {
		System.out.println(s);
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//int[] m = { 50, 1, 1, 50};
		int[] m = new int[100];
		for (int i = 0; i < m.length; i++)
			m[i] = (int)(Math.random()*100);

		DP_HouseRobber t = new DP_HouseRobber();
		int money_v1 = 0;
		int money_v2 = 0;
		//while (money_v1 == money_v2) {
			money_v1 = t.do_HouseRobber_v1(m);
			money_v2 = t.do_HouseRobber_v2(m);
		//}

		for (int i = 0; i < m.length; i++)
			t.print(String.valueOf(m[i]) + ",");
		t.println("");
		t.println("Money V1:" + String.valueOf(money_v1));
		t.println("Money V2:" + String.valueOf(money_v2));
	}
}
