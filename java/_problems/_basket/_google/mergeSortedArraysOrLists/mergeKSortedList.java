package _problems._basket._google.mergeSortedArraysOrLists;

/*
 * Merge k sorted linked lists and return it as one sorted list.
 * 
 * T(n) = O(nlog(k))
 */

import java.util.Comparator;
import java.util.PriorityQueue;

public class mergeKSortedList {

	private class ListNode {
		ListNode next;
		int data;

		public ListNode(int data) {
			this.data = data;
			this.next = null;
		}	
	};

	private ListNode do_mergeKSortedList(ListNode[] lists) {
		// java < 8
		Comparator<ListNode> comp = new Comparator<ListNode>() {
			@Override
			public int compare(ListNode n1, ListNode n2) {
				return n1.data - n2.data;
			}
		};

		// java 8
		//Comparator<ListNode> comp = Comparator.comparing(ListNode->ListNode.data);
		
		PriorityQueue<ListNode> q = new PriorityQueue<ListNode>(comp);
		for(int i = 0; i < lists.length; i++)
			q.add(lists[i]);

		ListNode prev = null;
		ListNode L = null;
		while (!q.isEmpty()) {
			ListNode cL = q.poll();
			ListNode n = new ListNode(cL.data);

			if (prev == null)
				L = prev = n;
			else {
				prev.next = n;
				prev = n;
			}

			if (cL.next != null)
				q.add(cL.next);
		}

		return L;
	}

	private ListNode arrayToList(int[] arr) {
		if (arr == null || arr.length == 0)
			return null;

		ListNode L = new ListNode(arr[0]);
		ListNode prev = L;
		for (int i = 1; i < arr.length; i++) {
			ListNode n = new ListNode(arr[i]);
			prev.next = n;
			prev = n;
		}

		return L;
	}

	private void printList(ListNode L) {
		ListNode n = L;
		while (n != null) {
			System.out.print(n.data + ",");
			n = n.next;
		}
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[][] arrs = {
				{ 1, 3, 5, 7 },
				{ 2, 4, 6, 8 },
				{ 0, 9, 10, 11 }
		};
		
		mergeKSortedList t = new mergeKSortedList();
		ListNode[] lists = new ListNode[arrs.length];
		for (int i = 0; i < arrs.length; i++)
			lists[i] = t.arrayToList(arrs[i]);
		ListNode L = t.do_mergeKSortedList(lists);
	
		t.printList(L);
	}
}
