package _problems._basket._google.mergeSortedArraysOrLists;

import java.util.Comparator;

/*
 * Merge K Sorted Arrays.
 * 
 * T(n) = O(nlog(k))
 */

import java.util.PriorityQueue;

public class mergeKSortedArrays {

	private class ArrayContainer {
		public int[] a;
		public int j;
		public ArrayContainer(int[] arr) {
			this.a = arr;
			this.j = 0;
		}
	};

	private int[] do_mergeKSortedArrays(int[][] arrs) {
		Comparator<ArrayContainer> comp = new Comparator<ArrayContainer>() {
			@Override
			public int compare(ArrayContainer c1, ArrayContainer c2) {
				return c1.a[c1.j] - c2.a[c2.j];
			}
		};
		PriorityQueue<ArrayContainer> q = new PriorityQueue<ArrayContainer>(comp);

		int len = 0;
		for (int i = 0; i < arrs.length; i++) {
			q.add(new ArrayContainer(arrs[i]));
			len += arrs[i].length;
		}
		int[] result = new int[len];

		int k = 0;
		while (!q.isEmpty()) {
			ArrayContainer ac = q.poll();
			result[k++] = ac.a[ac.j];
	
			if (ac.j < ac.a.length - 1) {
				ac.j++;
				q.add(ac);
			}
		}

		return result;
	}

	private void printArray(int[] arr) {
		println("The sorted array:");
		for (int i = 0; i < arr.length; i++)
			print(String.valueOf(arr[i]) + ",");
	}
	
	private static void print(String s) {
		System.out.print(s);
	}

	private static void println(String s) {
		System.out.println(s);
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[][] arrs = {
				{ 1, 3, 5, 7 },
				{ 2, 4, 6, 8 },
				{ 0, 9, 10, 11 }
		};
		
		mergeKSortedArrays t = new mergeKSortedArrays();
		int[] arr = t.do_mergeKSortedArrays(arrs);
		t.printArray(arr);
	}
}
