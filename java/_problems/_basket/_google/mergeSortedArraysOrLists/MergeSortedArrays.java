package _problems._basket._google.mergeSortedArraysOrLists;

public class MergeSortedArrays {

	private void merge(int[] a, int n, int[] b, int m) {
		int k = n + m - 1;
		int i = n - 1;
		int j = m - 1;
		
		while (j >= 0) {
			if (i>=0 && a[i] > b[j])
				a[k--] = a[i--];
			else
				a[k--] = b[j--];
		}
	}

	private void printArray(int[] arr) {
		System.out.println("The sorted array:");
		for (int i = 0; i < arr.length; i++)
			System.out.print(String.valueOf(arr[i]) + ",");
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		int[] a = {10, 11, 12, 0, 0, 0, 0};
		int[] b = {14, 15, 16, 17};
		MergeSortedArrays t = new MergeSortedArrays();
		t.merge(a, 3, b, b.length);
		t.printArray(a);
		
	}
}
