package _problems._basket._google._interview;

import java.util.HashMap;

/* Consuming a stream of data.
 * The stream is received in buffers of different sizes, and each buffer is at a different offset
 * in the stream.
 * 
 * The stream is read by an application, but in contiguous form.
 */

public class scatterGatherBuffers {
	private int currentOff;
	private int clamp;

	private HashMap<Integer, byte[]> sgBuffers;

	private scatterGatherBuffers() {
		sgBuffers = new HashMap<Integer, byte[]>();
		currentOff = 0;
		clamp = 0;
	}

	private byte[] getAvailabelEntry() {
		byte[] buf = sgBuffers.get(currentOff);
		if (buf != null)
			return buf;
	
		return null;
	}

	private void recvBuffer(byte[] buf, int offset) {
		sgBuffers.put(offset, buf);
	}
	
	private int readBuffer(byte[] buf) {
		int bufClamp = 0;
		int bufSize = buf.length;

		byte[] aBuf = getAvailabelEntry();
		int space = bufSize;
		while (aBuf != null && space > 0) {
			int aBufSize = aBuf.length;

			if (space >= aBufSize - clamp) {
				System.arraycopy(aBuf, clamp, buf, bufClamp, aBufSize - clamp);
				sgBuffers.remove(currentOff);

				bufClamp += aBufSize - clamp;
				currentOff += aBufSize;
				clamp = 0;
			} else {
				System.arraycopy(aBuf, clamp, buf, bufClamp, space);
				bufClamp += space;
				clamp += space;
			}

			aBuf = getAvailabelEntry();
			space = bufSize - bufClamp;
		}
		
		return buf.length - space;
	}
	
	private static void printBuffer(byte[] buf, int len) {
		for (int i = 0; i < len; i++)
			System.out.print(buf[i] + ",");
		System.out.println();
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		byte[] buf1 = {1, 2, 3};
		int off1 = 0;
		byte[] buf2 = {4, 5};
		int off2 = 3;
		byte[] buf3 = {8, 9};
		int off3 = 8;

		scatterGatherBuffers t = new scatterGatherBuffers();
		t.recvBuffer(buf1, off1);
		t.recvBuffer(buf2, off2);
		t.recvBuffer(buf3, off3);
		
		byte[] buf = new byte[2];
		int readBytes = t.readBuffer(buf);
		System.out.print("Len:" + readBytes + " : ");
		printBuffer(buf, readBytes);
		
		buf = new byte[2];
		readBytes = t.readBuffer(buf);
		System.out.print("Len:" + readBytes + " : ");
		printBuffer(buf, readBytes);

		buf = new byte[2];
		readBytes = t.readBuffer(buf);
		System.out.print("Len:" + readBytes + " : ");
		printBuffer(buf, readBytes);
	}
}
