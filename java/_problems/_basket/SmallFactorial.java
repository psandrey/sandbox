package _problems._basket;

import java.math.BigInteger;

public class SmallFactorial {

    private static BigInteger fact(int n) {
        if (n == 0 || n == 1) return BigInteger.valueOf(1);
        
        BigInteger bi = new BigInteger(String.valueOf(n));
        bi = bi.multiply(fact(n-1));
        
        return bi;
    }
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		
		System.out.println(fact(100));
	}
}
