package _playground.java_basics._tests;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ClassLoaderTest {

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
		ClassLoaderCustom customClassLoader = new ClassLoaderCustom();
		Class<?> c = customClassLoader.findClass(ClassLoaderPrint.class.getName());
		Object ob = c.newInstance();
		
		System.out.println("-- SUN class loader:");
		ClassLoaderPrint ccp = new ClassLoaderPrint();
		ccp.printClassLoaders();
		System.out.println("");

		System.out.println("-- CUSTOM class loader:");
		Method md = c.getMethod("printClassLoaders");
		md.invoke(ob);

	}
}
