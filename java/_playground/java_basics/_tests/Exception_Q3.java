package _playground.java_basics._tests;

import static java.lang.System.out;

public class Exception_Q3 {
	
	static int f() {
		try { return 1; }
		catch (RuntimeException ex) { out.println("A"); }
		finally { out.println("B"); }
	
		out.println("C");
		return 2;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		
		int x = f();
		System.out.println(x);
		
		try { return; }
		catch (RuntimeException ex) { out.println("A"); }
		finally { out.println("B"); }
	
		out.println("C");
	}
}
