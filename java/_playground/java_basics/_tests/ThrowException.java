package _playground.java_basics._tests;

public class ThrowException {
	
	static void validate(int age) {
		if(age < 18)
			throw new ArithmeticException("# Not valid");
		else
			System.out.println("welcome to vote");  
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		validate(10);
		System.out.println("rest of the code...");
	}
}
