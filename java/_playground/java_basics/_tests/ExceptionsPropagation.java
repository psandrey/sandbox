package _playground.java_basics._tests;

import java.io.IOException;
import static java.lang.System.out;

public class ExceptionsPropagation {
	void m() throws IOException {
		out.println("before exception");
		
		throw new IOException("Sorry!"); 

		// this code is unreachable
	}
	
	void n() throws IOException { m(); }
	
	void p() throws IOException { n(); }
		
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) throws IOException {
		ExceptionsPropagation t = new ExceptionsPropagation();
		
		//t.p();
		try { t.p(); }
		catch (Exception e) {
			out.println("Throw it further...");
			throw e;
		}
		
		out.println("rest of the code...");
	}
}
