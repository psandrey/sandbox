package _playground.java_basics._tests;


public class Play {
	
	class M {
		public void h() { System.out.println("from base calls: "); f(); }
		private void f () {System.out.println("base");}
		
//		protected void k() { }
	}
	
	class N extends M {
		public void g() { f(); }
		private void f () {System.out.println("derived");}		
	}
    
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		Play t = new Play();
		M m = t.new N();
		m.h();
		
		Play2 p = new Play2();
		p.k();
		
		Play4 p4 = new Play4();
		Play4.Inner p4Inner = p4.new Inner();
		p4Inner.y = 10;
	}
}
