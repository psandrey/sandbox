package _playground.java_basics._tests;

/* if there isn't a specific catch block for the thrown exception, then 
 * the catch blocks of parent try block is inspected for the exception.
 **/

public class NestedException_2 {

	public static void main(String args[]) { 
		try { // try-block1  
			try { // try-block2

				try { // try-block3
					int arr[] = { 1, 2, 3, 4 }; 
					System.out.println(arr[10]); 
				} catch (ArithmeticException e) { // handles ArithmeticException if any
					System.out.println("Arithmetic exception");
					System.out.println(" try-block1");
				}

			// block 2
			} catch (ArithmeticException e) { // handles ArithmeticException if any
				System.out.println("Arithmetic exception"); 
				System.out.println(" try-block2"); 
			} 
		}
		
		// block 1
		catch (ArrayIndexOutOfBoundsException e4) { // handles ArrayIndexOutOfBoundsException if any
			System.out.print("ArrayIndexOutOfBoundsException");
			System.out.println(" block 1 try-block");
		}
		catch (Exception e5) { 
			System.out.print("Exception"); 
			System.out.println(" handled in main try-block"); 
		} 
	} 
}
