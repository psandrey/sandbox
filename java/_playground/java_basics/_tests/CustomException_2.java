package _playground.java_basics._tests;

import static java.lang.System.out;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

class IncorrectFileNameException extends Exception {
    private static final long serialVersionUID = 1L;
    
    public IncorrectFileNameException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}

class IncorrectFileExtensionException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public IncorrectFileExtensionException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}

public class CustomException_2 {
	static String scanFile(String fileName) throws IncorrectFileNameException {

		try (Scanner file = new Scanner(new File(fileName))) {
			
			if (file.hasNextLine()) return file.nextLine();
			else throw new IllegalArgumentException("Non readable file");

		} catch (FileNotFoundException err) {
			if (!isCorrectFileName(fileName))
				throw new IncorrectFileNameException("Incorrect filename : " + fileName, err);
				// Logging etc
		} catch (IllegalArgumentException err) {
			if (!containsExtension(fileName))
				throw new IncorrectFileExtensionException(""
						+ "Filename does not contain extension : " + fileName, err);
			
			// Other error cases and logging
		}
		
		return "";
	}

	static boolean isCorrectFileName(String fileName) { return false; }
	static boolean containsExtension(String fileName) { return false; }

	/* The MAIN ---------------------------------------------------------------------------------*/
	//public static void main(String args[]) throws IncorrectFileNameException{
	public static void main(String args[]) {
		//scanFile("foo");
		try { scanFile("foo"); }
		catch (IncorrectFileNameException e) {
			out.println("Custom exception: " + e + "\nCause: " + e.getCause()); }
	}
}
