package _playground.java_basics._tests;

public class AbstractClass_2 {
	static abstract class Abstr {
		protected int a;
		Abstr() { a = 10; }

		abstract public void set(int a);
		abstract public void get();
	}

	static class Test extends Abstr {
		public void set(int a) { this.a = a; }
		public void get() { System.out.println("a = " + a); }
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]){ 
		Test obj = new Test();
		//Abstr o = new Test();
		obj.set(20);
		obj.get();
	}
}
