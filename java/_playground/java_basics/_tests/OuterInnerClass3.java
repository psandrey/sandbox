package _playground.java_basics._tests;

import static java.lang.System.out;

public class OuterInnerClass3 {

	private static int x = 100;

	static class A {
		void m() { out.println(x); }
	}
	
	void msg() {
		class Local {
			void m() { out.println(x); }
		}
		Local l = new Local();
		l.m();
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) throws Exception {
		OuterInnerClass3 t = new OuterInnerClass3();
		t.msg();
	}
}
