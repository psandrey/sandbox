package _playground.java_basics._tests;

import java.lang.ArrayIndexOutOfBoundsException;
import static java.lang.System.out;

public class FinalFinallyFinalize {
	static final class A { }; // cannot be inherited
	static class B {
		final int x; // cannot be changed
		B() { x = 1; } // final variable can be initialized in constructor
		final void m() { } // cannot be overridden
	}
	
	static class C {
		void m() {
			Integer[] a = new Integer[]{1, 2, 3};
			try { a[10] = 4; }
			catch (ArrayIndexOutOfBoundsException  e) { out.println("Exception handled"); }
			finally { out.println("finally called"); }
		}
		 
		public void finalize(){ out.println("finalize called"); } 
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]){
		C t = new C();
		t.m();	
		t = null;
		System.gc();
	}
}
