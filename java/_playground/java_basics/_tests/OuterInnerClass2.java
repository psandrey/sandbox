package _playground.java_basics._tests;

public class OuterInnerClass2 {
	class XA {
		private int x = 0;
		private class AA {
			public void AA_f1() { x++; }
			private void AA_f2() { x++; }
		}
	}

	class XB extends XA {
		
		public class AA extends XA.AA {
			public void AA_g() { }
		}
		
		private void B_f() {
			AA x = new AA();
			x.AA_f1();
			//x.AA_f2(); // can't access it, its private
			x.AA_g();
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		OuterInnerClass2 t = new OuterInnerClass2();
		XB b = t.new XB();
		XB.AA a = b.new AA();
	}
}
