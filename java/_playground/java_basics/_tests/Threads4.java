package _playground.java_basics._tests;

import java.util.Random;
import java.util.concurrent.Semaphore;

public class Threads4 {
	
	class WorkerClass implements Runnable {
		String name;
		Semaphore s;
		WorkerClass(Semaphore s, String name) { this.s = s; this.name = name; }

		@Override
		public void run() {
			Random rand = new Random();

			try {
				for (int i = 0; i < 100; i++) {
					s.acquire();
					System.out.println(" Thread (" + name + ") working.");

					int time = rand.nextInt(100);
					Thread.sleep(time);
					
					System.out.println(" Thread (" + name + ") done.");
					s.release();
				}
			} catch (InterruptedException e) { e.printStackTrace(); }
		}	
	}

	
	public static void main(String[] args) throws InterruptedException {
		// binary semaphore:
		// OS: main use of binary semaphore is for signaling not as a locking mechanism,
		//     however I'm just playing with the API here and the semaphore acquire/release
		//     is done by the same thread, thus is used as a mutex.
		Semaphore s = new Semaphore(1);
		
		Threads4 t = new Threads4();
		WorkerClass w1 = t.new WorkerClass(s, "th1");
		WorkerClass w2 = t.new WorkerClass(s, "th2");
		
		Thread th1 = new Thread(w1);
		Thread th2 = new Thread(w2);

		th1.start();
		th2.start();
		
		th1.join();
		th2.join();
	}
}
