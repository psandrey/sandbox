package _playground.java_basics._tests;

import static java.lang.System.out;

class InvalidAgeException extends Exception {
	private static final long serialVersionUID = 1L;
	InvalidAgeException (String msg) { super(msg); }
}

public class CustomException {
	static void validate(int age) throws InvalidAgeException {
		if (age < 18) throw new InvalidAgeException("Age less than 18");
		else out.println("Welcome to vote!");
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]){
		try { validate(10); }
		catch (InvalidAgeException e) {out.println("Custom exception: " + e); }
		
		out.println("rest of the code");
	}
}
