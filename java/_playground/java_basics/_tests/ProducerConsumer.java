package _playground.java_basics._tests;

/*
 * Classic synchronization problem: Producer-Consumer problem.
 * Note: use mutex based on synchronized keyword.
 */

import static java.lang.System.out;
import java.util.LinkedList;

public class ProducerConsumer {
	class Items {
		int produced;
		int capacity;
		LinkedList<Integer> q = new LinkedList<Integer>();
		Items(int c, int p) { capacity = c; produced = p; }
		
		synchronized public void produce() throws InterruptedException {
			int x = 1;
			while (x <= produced ) {
				if (q.size() == capacity) wait();

				out.println("Producer produced-" + x);
				q.add(x++);

				notify();

				Thread.sleep(250);
			}
		}

		synchronized public void consume() throws InterruptedException {
			int x = 0;
			do {
				if (q.size() == 0) wait();

				x = q.removeFirst();
				out.println("Consumer consumed-" + x); 

				notify();

				Thread.sleep(250);
			} while (x < produced);
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		ProducerConsumer pc = new ProducerConsumer();
		ProducerConsumer.Items pcItms = pc.new Items(3, 25);
		
		Thread thProducer = new Thread(new Runnable() {
			@Override
            public void run() {
				try { pcItms.produce(); }
				catch (InterruptedException e) { e.printStackTrace(); }
			}
		});

		Thread thConsumer = new Thread(new Runnable() {
			@Override
            public void run() {
				try { pcItms.consume(); }
				catch (InterruptedException e) { e.printStackTrace(); }
			}
		});
		
		thProducer.start();
		thConsumer.start();
		
		thProducer.join();
		thConsumer.join();

		out.println("Done...");
	}
}
