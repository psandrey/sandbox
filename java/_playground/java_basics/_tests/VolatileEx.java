package _playground.java_basics._tests;

// synchronized on read_modify_write(increment) to assure atomicity
// volatile on read to assure that we get the most recent version of counter

public class VolatileEx {
	public class SafeCounter {
		private volatile int counter = 0;

		// volatile for reads and synchronized for writes
		public int get() { return counter; }
		public synchronized void inc(long thId) {
			++counter;
			System.out.println("[Thread " + thId
					+ "]: Old value = " +  counter);
			}
	}
	
	public class CounterThread extends Thread {
		private final SafeCounter data;
		public CounterThread(SafeCounter data) { this.data = data; }
		 
		@Override
		public void run() {
			data.inc(Thread.currentThread().getId());
			System.out.println("[Thread " + Thread.currentThread().getId()
					+ "]: New value = " +  data.get());
		}
	}
	
	private final static int TOTAL_THREADS = 20;
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) throws InterruptedException {
		VolatileEx t = new VolatileEx();
		SafeCounter volatileData = t.new SafeCounter();

		Thread[] threads = new Thread[TOTAL_THREADS];
		for(int i = 0; i < TOTAL_THREADS; ++i)
			threads[i] = t.new CounterThread(volatileData);
		 
		//Start all reader threads.
		for(int i = 0; i < TOTAL_THREADS; ++i)
			threads[i].start();
		 
		//Wait for all threads to terminate.
		for(int i = 0; i < TOTAL_THREADS; ++i) threads[i].join();
	}
}
