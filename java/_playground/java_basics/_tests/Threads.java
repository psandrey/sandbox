package _playground.java_basics._tests;

import java.util.concurrent.CountDownLatch;

public class Threads {
	static int TICKS = 1000000;
	
	class Counter {
		private int k;
		private int clicks;
		Counter() { k = 0; clicks = 0; }
		
		public synchronized void inc() throws InterruptedException {
			// By executing wait() from a synchronized block,
			//  a thread gives up its hold on the lock and goes to sleep.
			while (k >= 10) wait();
			k++; clicks++;
			notify();
		}
		
		public synchronized void dec() throws InterruptedException {
			while (k <= 0) wait();
			k--; clicks++;
			notify();
		}
		
		public synchronized int get() { return k; }
		public synchronized int getClicks() { return clicks; }
	}
	
	class ThreadInc implements Runnable {
		private Counter c;
		CountDownLatch latch;
		ThreadInc(Counter c,  CountDownLatch latch) { this.c = c; this.latch = latch; }
		
		public void run() {
			latch.countDown();
			
			while (true) {
				try {
					c.inc();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if (c.getClicks() >= TICKS) break;
			}
		}
	}
	
	class ThreadDec extends Thread {
		private Counter c;
		CountDownLatch latch;
		ThreadDec(Counter c,  CountDownLatch latch) { this.c = c; this.latch = latch; }
		
		public void run() {
			latch.countDown();
			
			while (true) {
				try {
					c.dec();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if (c.getClicks() >= TICKS) break;
			}
		}
		
	}

	class ThreadPrint extends Thread {
		private Counter c;
		CountDownLatch latch;
		ThreadPrint(Counter c, CountDownLatch latch) { this.c = c; this.latch = latch; }
		
		public void run() {
			latch.countDown();
			while(true) System.out.println(c.get());
		}
	}
	
	public static void main( String args [] ) throws InterruptedException {
		Threads t = new Threads();
		
		Counter cnt = t.new Counter();
		CountDownLatch latch = new CountDownLatch(3);
		
		ThreadInc ith = t.new ThreadInc(cnt, latch);
		Thread bth = new Thread(ith);
		bth.setPriority(Thread.MIN_PRIORITY);
		
		ThreadDec dth = t.new ThreadDec(cnt, latch);
		dth.setPriority(Thread.MIN_PRIORITY);
		
		ThreadPrint pth = t.new ThreadPrint(cnt, latch);
		pth.setDaemon(true);
		
		bth.start();
		dth.start();
		pth.start();
		
		bth.join();
		dth.join();
	}
}
