package _playground.java_basics._tests;

public class AbstractClass {
	
	interface A {
		void a();
		void b();
		void c();
		void d();
	}
		  
	static abstract class B implements A {
		abstract public void a();
		public void c(){ System.out.println("B: I am c"); } 
	}  
		  
	static class M extends B {
		protected int x = 2;
		public void a() { System.out.println("M: I am a"); }
		public void b() { System.out.println("M: I am b"); }
		public void d() { System.out.println("M: I am d"); }
	}  
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) throws CloneNotSupportedException {
		A a = new M();
		a.a();
		a.b();
		a.c();
		a.d();
		
		M m = new M();
		m.x = 0;
		
		B b = new M(); // we can have reference of an abstract, though we cannot have an instance
		b.c();
	}
}
