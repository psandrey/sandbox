package _playground.java_basics._tests;

import static java.lang.System.out;

public class Exception_Q2 {
	public static void badMethod() {
		throw new RuntimeException();
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		try {
			badMethod();
			out.println("A");
		} catch (RuntimeException ex) {
			out.println("B");
		} catch (Exception ex) {
			out.println("C");
		} finally {
			out.println("D");
		}
		
		out.println("E");
	}	
}
