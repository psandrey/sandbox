package _playground.java_basics._tests;

import static java.lang.System.out;

public class Exception_Q1 {
	
	public static void aMethod() throws Exception {
		try {
			throw new Exception();
		}
		finally {
			out.println("finally");
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		try { aMethod(); }
		catch (Exception e) { out.println("exception"); }
		
		out.println("finished");
	
	}
}