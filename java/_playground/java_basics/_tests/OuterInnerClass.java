package _playground.java_basics._tests;

import static java.lang.System.out;

public class OuterInnerClass {

	private int data = 100;
	
	class Inner {
		private void msgInner() { out.println("Inner: method, outter data:" + data); }
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) throws Exception {
		OuterInnerClass o = new OuterInnerClass();
		OuterInnerClass.Inner in = o.new Inner();
		
		in.msgInner();
	}
}
