package _playground.java_basics._tests;

import java.util.ArrayList;

public class ClassLoaderPrint {
	public void printClassLoaders() throws ClassNotFoundException {

		System.out.println("Classloader of this class: " + ClassLoaderPrint.class.getClassLoader());
		System.out.println("Classloader of ArrayList: " + ArrayList.class.getClassLoader());
	}
}
