package _playground.java_basics._tests;

class A {
	static int a = 1;
	int aa = 2;
	A () { System.out.println("Constructor A"); }
	{ System.out.println("Initialization block A"); }
	static { System.out.println("Static Initialization block A"); }
}

class B extends A {
	static int b = 3;
	int bb = 3;
	B () { System.out.println("Constructor B"); }
	{ System.out.println("Initialization block B"); }
	static { System.out.println("Static Initialization block B"); }
}

public class InitSeq {

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		B obj = new B();
	}
}
