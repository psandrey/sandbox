package _playground.java_basics._tests;

//import static java.lang.System.out;

public class Exception_Q6 {

	/* The MAIN ---------------------------------------------------------------------------------*/
	@SuppressWarnings("finally")
	public static void main(String args[]) throws Exception {
		try {
			throw new Exception("try");
		} catch (Exception e) {
			throw new Exception("catch");
		}
		finally {
			throw new Exception("finally");
		}
		
		// Unreachable code
		// out.println("rest of the code");
	}
}
