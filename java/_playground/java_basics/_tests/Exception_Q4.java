package _playground.java_basics._tests;

import static java.lang.System.out;

public class Exception_Q4 {
	public static void throwit() {
		out.println("thorwit");
		throw new RuntimeException();
		//throw new Error();
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		try {
			out.println("hello");
			throwit();
		} catch (Exception ex) {
			out.println("caught");
			throw new RuntimeException();
		} finally {
			out.println("finally");
		}
		
		out.println("after");
	}
}
