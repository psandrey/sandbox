package _playground.java_basics._tests;


public class StaticBlock {
	static {
		System.out.println("Static block executes..");
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		System.out.println("Main executes..");
	}
}
