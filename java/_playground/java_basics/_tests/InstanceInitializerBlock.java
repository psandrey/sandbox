package _playground.java_basics._tests;

public class InstanceInitializerBlock {

	static class A {
		static int a = 1;
		static int aa;
		static {
			aa = 2;
			System.out.println("Static A");
		}
		int aaa = 2;
		
		A() { System.out.println("Constructor A"); }
		
		{ System.out.println("Initializer A"); }
	}

	static class B extends A {
		static int b = 1;
		static int bb;
		static {
			bb = 2;
			System.out.println("Static B");
		}
		int bbb = 2;
		
		B() { System.out.println("Constructor B"); }
		
		{ System.out.println("Initializer B"); }
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		B obj = new B();
	}
}