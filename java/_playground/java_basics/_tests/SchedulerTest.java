package _playground.java_basics._tests;

/* THe scheduler:
 *  - Each thread is backed by OS-thread and scheduled according OS-scheduler profile.
 *  - If a thread has a higher priority and the OS-scheduler supports prioritization then
 *     the thread with higher priority will more likely receive more CPU time.
 *  Note: This simple test shows the time slice support and shows the Foo/Bar interleaved. 
 */

class SchedulerTestThread extends Thread {
	String msg;
  
	SchedulerTestThread ( String msg ) { this.msg = msg; }
  
	public void run( ) {
		while ( true ) {
			System.out.println( msg );
		}
	}
}

public class SchedulerTest {
	public static void main( String args [] ) {  
		new SchedulerTestThread("Foo").start( );  
		new SchedulerTestThread("Bar").start( );
		//new SchedulerTestThread("x").start( );  
		//new SchedulerTestThread("y").start( );  
	}  
}
