package _playground.java_basics._tests;

public class SuperCalls {
	class A {
		public int x;
		A() { System.out.println("Constructor A"); }
	}

	class B extends A {
		B() {
			//super(); // if omitted compiler adds it by default
			System.out.println("Constructor B"); }
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		SuperCalls t = new SuperCalls();
		B b = t.new B();
	}
}
