package _playground.java_basics._tests;

public class DeepClone {
	static class A implements Cloneable {
		protected String field_a = "A's original field";
		
		@Override
		public A clone() throws CloneNotSupportedException {
			return (A)super.clone();
		}
	}
	
	static class B implements Cloneable {
		A a;
		B() { a = new A(); }
		
		@Override
		public B clone() throws CloneNotSupportedException {
			B clone = (B)super.clone();
			clone.a = this.a.clone();
			clone.a.field_a = "A's clone field";
			return clone;
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]){
		B b_orig = new B();
		B b_clone = null;
		
		try { b_clone = b_orig.clone(); }
		catch (CloneNotSupportedException e) { e.printStackTrace(); }
		
		System.out.println(b_orig.a.field_a);
		System.out.println(b_clone.a.field_a);
	}
}
