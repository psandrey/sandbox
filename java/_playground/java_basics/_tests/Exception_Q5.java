package _playground.java_basics._tests;

import static java.lang.System.out;

public class Exception_Q5 {
	public static void m1() throws Exception {
		m2();
	}
	
	public static void m2() throws Exception {
		throw m3();
	}
	
	public static Exception m3() {
		return new Exception();
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		try {
			m1();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			out.println("finally");
		}
		
		out.println("the rest of the code");
	}
}
