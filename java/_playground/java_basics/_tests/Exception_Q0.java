package _playground.java_basics._tests;

import java.util.Random;

public class Exception_Q0 {

	public class Base extends Exception {
		private static final long serialVersionUID = 1L;} 
	public class Derived1 extends Base  {
		private static final long serialVersionUID = 1L;} 

	public class Derived2 extends Base  {
		private static final long serialVersionUID = 1L;} 
	public class Derived3 extends Derived2  {
		private static final long serialVersionUID = 1L;} 
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		Random rand = new Random();
		Exception_Q0 x = new Exception_Q0();
		try {
			// specific to general
			if (rand.nextInt(1000) % 2 == 0) throw x.new Derived1();
			else if (rand.nextInt(1000) % 3 == 0) throw x.new Derived3();
			else throw x.new Base();
		}
		catch(Derived1 d1) {
			System.out.println("Derived 1 Base class handled!");
		}
		catch(Derived3 d3) {
			System.out.println("Derived 3 Base class handled!");
		}
		catch(Base b) {
			System.out.println("Base class handled!");
		}
	}
}
