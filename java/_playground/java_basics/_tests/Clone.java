package _playground.java_basics._tests;

public class Clone {

	public class X {
		public int x;
		
		public X(int i) { x = i; }
		public int get() { return x; }
	}

	public class Y {
		public int y;
		
		public Y(int i) { y = i; }
		public int get() { return y; }
	}
	
	public class A implements Cloneable {
		public int a;
		public X x;
		
		public A(int i) { a = i; x = new X(i); }
		public A clone() throws CloneNotSupportedException {
			A c = (A)super.clone();
			
			// deep copy X
			X cx = new X(this.x.get());
			c.x = cx;
			
			return c;
		}
		
		public int getA() { return a; }
		public int getX() { return x.get(); }
	}
	
	public class B extends A implements Cloneable {
		public int b;
		public Y y;
		public B(int x) {
			super(x);
			b = x;
			y = new Y(x);
		}
		
		// copy constructor
		public B(B c) {
			super(c.b);
			 b = c.b;
			 y = new Y(c.y.get());
		}
		
		// implement clone
		public B clone() throws CloneNotSupportedException {
			B c = (B)super.clone();
			
			// deep copy Y
			Y cy = new Y(this.y.get());
			c.y = cy;
			
			//this.x.x = 200;
			//System.out.println(c.x.x);		
			return c; 
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) throws CloneNotSupportedException {
		Clone t = new Clone();
		
		B b1 = t.new B(10);
		B b2 = b1.clone(); // thru cloneable
		B b3 = t.new B(b1); // thru copy-constructor
	}
}
