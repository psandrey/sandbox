package _playground.java_basics._tests;

public class DynamicDispatch {
	
	public class A {
		public void f() {System.out.println("A"); }
	}

	public class B extends A {
		@Override
		public void f() {System.out.println("B"); }
		
		public void g() { super.f(); }
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		DynamicDispatch t = new DynamicDispatch();
		
		// upcast
		A x = t.new B();
		x.f();
		
		// downcast
		((B)x).g();
	}
}
