package _playground.java_basics._tests;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Threads2 {
	static int TICKS = 1000000;
	
	class Counter {
		private int k;
		private int clicks;
		private final Lock lock;
		private final Condition isFull;
		private final Condition isEmpty;
		Counter() {
			k = 0; clicks = 0;
			lock = new ReentrantLock();
			isFull = lock.newCondition();
			isEmpty = lock.newCondition();
		}
		
		public void inc() throws InterruptedException {
			if (lock.tryLock()) {
				try {
					while (k >= 10) isFull.await();
					k++; clicks++;
				} finally {
					isEmpty.signal();
					lock.unlock();
				}
			}
		}
		
		public void dec() throws InterruptedException {
			if (lock.tryLock()) {
				try {
					while (k <= 0) isEmpty.await();
					k--; clicks++;
				} finally {
					isFull.signal();
					lock.unlock();
				}
			}
		}
		
		public synchronized int get() { return k; }
		public synchronized int getClicks() { return clicks; }
	}
	
	class ThreadInc implements Runnable {
		private Counter c;
		CountDownLatch latch;
		ThreadInc(Counter c,  CountDownLatch latch) { this.c = c; this.latch = latch; }
		
		public void run() {
			latch.countDown();
			
			while (true) {
				try {
					c.inc();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if (c.getClicks() >= TICKS) break;
			}
		}
	}
	
	class ThreadDec extends Thread {
		private Counter c;
		CountDownLatch latch;
		ThreadDec(Counter c,  CountDownLatch latch) { this.c = c; this.latch = latch; }
		
		public void run() {
			latch.countDown();
			
			while (c.getClicks() < TICKS) {
				try {
					c.dec();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			for (int i = 0; i < 100; i++);
			while (c.get() > 0)
				try {
					c.dec();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
		
	}

	class ThreadPrint extends Thread {
		private volatile boolean exit = false; 
		private Counter c;
		CountDownLatch latch;
		ThreadPrint(Counter c, CountDownLatch latch) { this.c = c; this.latch = latch; exit = false; }
		
		public void run() {
			latch.countDown();
			//while(!exit) System.out.println(c.get());
			while(!Thread.interrupted()) System.out.println(c.get());
		}
		
		public void stopThread() {
			exit = true;
		}
	}

	public static void main( String args [] ) throws InterruptedException {
		Threads2 t = new Threads2();
		
		Counter cnt = t.new Counter();
		CountDownLatch latch = new CountDownLatch(3);
		
		ThreadInc ith = t.new ThreadInc(cnt, latch);
		Thread bth = new Thread(ith);
		bth.setPriority(Thread.MIN_PRIORITY);
		
		ThreadDec dth = t.new ThreadDec(cnt, latch);
		dth.setPriority(Thread.MIN_PRIORITY);
		
		ThreadPrint pth = t.new ThreadPrint(cnt, latch);
		pth.setDaemon(true);
		
		bth.start();
		dth.start();
		pth.start();
		
		bth.join();
		dth.join();
		
		//pth.stopThread();
		pth.interrupt();
		System.out.println("End with: " + cnt.get());
	}
}
