package _playground.java_basics._tests;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Clone2 implements Serializable {
	private static final long serialVersionUID = 1L;
	static int q = 1;
	
	public class X implements Cloneable, Serializable {
		private static final long serialVersionUID = 1L;
		public String str;
		
		// deep copy
		public X clone() throws CloneNotSupportedException {
			X cloneX = (X) super.clone();
			cloneX.str = new String(str);
			
			return cloneX;
		}
	}
	
	public class A implements Cloneable, Serializable{
		private static final long serialVersionUID = 1L;
		
		X x;
		A(String a) {
			this.x = new X();
			this.x.str = a;
		}
		
		// shallow clone
		//public A clone() throws CloneNotSupportedException {
		//	A cloneA = (A)super.clone();
		//	
		//	return cloneA;
		//}

		// deep clone
		public A clone() throws CloneNotSupportedException {
			A cloneA = (A)super.clone();
			cloneA.x = (X)x.clone();
			
			return cloneA;
		}
		
		protected void g() { System.out.println("A"); }
	}
	
	public class B extends A implements Serializable {
		private static final long serialVersionUID = 1L;

		B(String b) { super(b); }
		
		// copy ctor
		B(B master) {
			super(master.x.str);
		}
		
		// clone
		public B clone() throws CloneNotSupportedException {
			B cloneB = (B)super.clone();
			
			return cloneB;
		}
		
		// serializable
		public B deepClone() throws IOException, ClassNotFoundException {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        ObjectOutputStream out = new ObjectOutputStream(bos);
			
			out.writeObject(this);

			ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
			ObjectInputStream in = new ObjectInputStream(bis);
			return (B) in.readObject();
		}
		
		@Override
		public void g() { System.out.println("B"); }
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) throws CloneNotSupportedException, ClassNotFoundException, IOException {
		Clone2 t = new Clone2();
		
		B b = t.new B("cucu");
		
		System.out.println("Shallow/Deep clone with cloneable: ");
		B cloneB = b.clone();
		
		System.out.println("master: " + b.x.str);
		System.out.println("clone: " + cloneB.x.str);
		
		cloneB.x.str = "mumu";
		
		System.out.println("master: " + b.x.str);
		System.out.println("clone: " + cloneB.x.str);
		
		System.out.println("\nDeep clone with cloneable: ");
		
		B cloneB2 = b.deepClone();
		
		cloneB2.x.str = "dudu";
		
		System.out.println("master: " + b.x.str);
		System.out.println("clone: " + cloneB2.x.str);
		
		A a = t.new B("zdrag");
		A copyA = a.clone();

	}
}
