package _playground.java_basics._tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class Play5 {

	public class test {
		private int x, y;
		
		public test(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
		@Override
		public int hashCode() {
			int hash = 7;
			hash = 31 * hash + (int) x;
			hash = 31 * hash + (int) y;
			
			System.out.println("Hash: " + hash);
			return hash;
		}
		
		@Override
		public boolean equals(Object obj) {
			if(this == obj)
				return true;
			
			if(obj == null || obj.getClass() != this.getClass())
				return false;
			
			test t = (test)obj;
			//return (this.x == t.x && this.y == t.y);
			return (this.hashCode() == t.hashCode());
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		Play5 t = new Play5();
		
		test t1 = t.new test(1, 2);
		test t2 = t.new test(1, 2);
		
		System.out.println("Are equal: " + t1.equals(t2));
		//System.out.println("Hash codes: " + t1.hashCode() + " , " + t2.hashCode());
		
		System.out.println("\nHashSet: >>");
		ArrayList<test> a = new ArrayList<>( Arrays.asList(t.new test(1, 2), t.new test(2, 3)));
		HashSet<test> hm = new HashSet<test>();
		for (int i = 0; i < a.size(); i++) {
			hm.add(a.get(i));
		}
	}
}
