package _playground.java_basics._tests;
import static java.lang.System.*;

interface Ia {
	final static int a = 10; 
	int fa();
	default void ga() { 
		out.println("Hello Ia"); 
	}
	
	default void ha() {
		System.out.println("Hello ha default"); 
	}
}

interface Ib {
	final int b = 10; 
	int fb();
	default void gb() { 
		System.out.println("Hello Ib"); 
	}
}

interface Ic extends Ia, Ib {
	final int c = 10; 
	int fc();
	default void gc() { 
		System.out.println("Hello Ic"); 
	}
}

class X implements Ic {

	@Override
	public int fa() {
		System.out.println(" Hello fa");
		return 0;
	}
	
	@Override
	public int fb() {
		System.out.println(" Hello fb");
		return 0;
	}

	@Override
	public int fc() {
		System.out.println(" Hello fc");
		return 0;
	}
	
	@Override
	public void ha() {
		System.out.println("Hello ha overridden"); 
	}
	
	protected void x() { }
}

class Y extends X {
	void func () { x(); }
}

public class TestIterface {
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]){
		X x = new X();
		x.fa();
		x.fb();
		x.fc();
		x.ga();
		x.gb();
		x.gc();
		x.ha();
		
		x.x();
		
		Y y = new Y();
		y.func();
	}
}
