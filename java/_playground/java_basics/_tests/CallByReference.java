package _playground.java_basics._tests;

public class CallByReference {
	static class Ref {
		public int x;
	}
	
	public void foo(Ref r) {
		r.x = 2;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]){
		CallByReference t =  new CallByReference();
		Ref r = new Ref(); r.x = 3;
		t.foo(r);
		System.out.println(r.x);
	}
}
