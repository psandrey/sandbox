package _playground.java_basics._tests;

/* parent exception is handled */

public class NestedException_1 {
	
	public static void main(String args[]) {
		// Main try block 
		try {
				int a[] = { 1, 2, 3, 4, 5 }; 
				System.out.println(a[5]);
	  
				// try-block2 inside another try block 
				try { int x = a[2] / 0; } // performing division by zero
				catch (ArithmeticException e2) {
					System.out.println("division by zero is not possible"); }

		} catch (ArrayIndexOutOfBoundsException e1) {
				System.out.println("ArrayIndexOutOfBoundsException thrown");
				System.out.println("Element at such index does not exists");
		}
	}
}
