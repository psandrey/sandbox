package _playground.java_basics._tests;

import java.util.concurrent.locks.ReentrantLock;

public class Threads3 {

	class WorkerClass implements Runnable {
		String name;
		ReentrantLock rlck;
		WorkerClass(ReentrantLock rlck, String name) { this.rlck = rlck; this.name = name; }

		@Override
		public void run() {

			// just for testing
			if (name.equals("th2")) {
				try { Thread.sleep(1000); } catch (InterruptedException e) { e.printStackTrace(); }
				System.out.println(" Thread(" + name + ") is locked: " + rlck.isLocked());
			}
			
			for (int i = 0; i < 10; i++) {
				rlck.lock();
				System.out.println(" Thread(" + name + ") acquire:" + rlck.getHoldCount());
			}
			
			for (int i = 0; i < 10; i++) rlck.unlock();
			System.out.println(" Thread(" + name + ") count:" + rlck.getHoldCount());
		}
	}
	   
	public static void main(String[] args) throws InterruptedException {
		Threads3 t = new Threads3();
		ReentrantLock rlck = new ReentrantLock();

		WorkerClass w = t.new WorkerClass(rlck, "th1");
		WorkerClass w2 = t.new WorkerClass(rlck, "th2");
		Thread th = new Thread(w);
		Thread th2 = new Thread(w2);

		th.start();
		th2.start();

		th.join();
		th2.join();
	}  
}
