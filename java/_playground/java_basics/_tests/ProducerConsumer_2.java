package _playground.java_basics._tests;

/*
 * Classic synchronization problem: Producer-Consumer problem.
 * Note: use mutex based on Reentrant mutex.
 */

import static java.lang.System.out;

import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;


public class ProducerConsumer_2 {
	private ReentrantLock mutex = new ReentrantLock();

	class Items {
		int produced;
		int capacity;
		LinkedList<Integer> q = new LinkedList<Integer>();
		Items(int c, int p) { capacity = c; produced = p; }
		
		public void produce() throws InterruptedException {
			int x = 1;
			while (x <= produced ) {
				
				mutex.lock();
				if (q.size() <= capacity) {
					out.println("Producer produced-" + x);
					q.add(x++);
				}
				mutex.unlock();

				Thread.sleep(250);
			}
		}

		public void consume() throws InterruptedException {
			int x = 0;
			do {
			
				mutex.lock();
				if (q.size() > 0) {
					x = q.removeFirst();
					out.println("Consumer consumed-" + x); 
				}
				mutex.unlock();

				Thread.sleep(250);
			} while (x < produced);
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		ProducerConsumer pc = new ProducerConsumer();
		ProducerConsumer.Items pcItms = pc.new Items(3, 25);
		
		Thread thProducer = new Thread(new Runnable() {
			@Override
            public void run() {
				try { pcItms.produce(); }
				catch (InterruptedException e) { e.printStackTrace(); }
			}
		});

		Thread thConsumer = new Thread(new Runnable() {
			@Override
            public void run() {
				try { pcItms.consume(); }
				catch (InterruptedException e) { e.printStackTrace(); }
			}
		});
		
		thProducer.start();
		thConsumer.start();
		
		thProducer.join();
		thConsumer.join();

		out.println("Done...");
	}
}
