package _playground.java_basics._tests;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadPool {
	
	class WorkerClass implements Runnable {
		String name;
		WorkerClass(String name) { this.name = name; }

		@Override
		public void run() {
			Random rand = new Random();

			try {
				for (int i = 0; i < 10; i++) {
					System.out.println(" Thread (" + name + ") working.");

					int time = rand.nextInt(100);
					Thread.sleep(time);
					
					System.out.println(" Thread (" + name + ") done.");
				}
			} catch (Exception e) { e.printStackTrace(); }
			finally { System.out.println(" --- Thread (" + name + ") EXIT."); }
		}	
	}

	static final int WRKS = 10;
	
	public static void main(String[] args) throws InterruptedException {
		ThreadPool t = new ThreadPool();
		ExecutorService pool = Executors.newFixedThreadPool(2);
		
		WorkerClass w[] = new WorkerClass[WRKS];
		for (int i = 0; i < WRKS; i++) w[i] = t.new WorkerClass("th_" + Integer.toString(i));
		
		for (int i = 0; i < WRKS; i++)
			pool.execute(w[i]);
		
		// kind of join
		pool.shutdown();
		while (!pool.awaitTermination(24L, TimeUnit.HOURS))
		    System.out.println("Still working...");
		
		System.out.println("...Work Done...");
	}
}
