package _playground.java_basics._tests;
import static java.lang.System.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OutputParameter {
	class IntBox {
		public IntBox() {x = 0;}
		public IntBox(int x) { this.x = x;}
		
		public int x;
	}

	void f(IntBox x) {
		int y = 10;
		
		x.x = y;
	}
	
	void g(String s) {
		// - s is a reference, at this point both caller and callee points to the same string, but
		//   there are 2 different reference variables
		// - since String is immutable, a new String is created and only the callee changes
		// ! if String would have mutable function member, then we could change caller's content
		s = "cd";
	}
		
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]){ 
		OutputParameter t = new OutputParameter();
/*
		IntBox x = t.new IntBox();

		t.f(x);
		out.println(x.x);
*/

		String s = new String("ab");
		t.g(s);
		out.println(s);

/*
		Immutable x = t.new Immutable(10);
		t.func(x);
		out.println(x.get());
*/	
	}
}
