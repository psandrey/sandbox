package _playground.java_basics._design_patterns;

import javax.inject.*;

public class A_Dep_Inj_A {
	private A_Dep_Inj_B b;
	
	@Inject
	A_Dep_Inj_A(A_Dep_Inj_B b) { this.b = b; }
	
	public void doSomething() {
		System.out.println("Injected object: " + b.msg);
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		A_Dep_Inj_A a = new A_Dep_Inj_A(new A_Dep_Inj_B());
		
		a.doSomething();
	}
}
