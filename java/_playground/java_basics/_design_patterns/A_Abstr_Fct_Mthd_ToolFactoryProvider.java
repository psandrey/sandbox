package _playground.java_basics._design_patterns;

public class A_Abstr_Fct_Mthd_ToolFactoryProvider {	
	public static A_Abstr_Fct_Mthd_ToolFactory<A_Abstr_Fct_Mthd_Toolbar> getToolbarFactory() {
		return new A_Abstr_Fct_Mthd_ToolbarFactory();
	}

	public static A_Abstr_Fct_Mthd_ToolFactory<A_Abstr_Fct_Mthd_Dialog> getDialogFactory() {
		return new A_Abstr_Fct_Mthd_DialogFactory();
	}
	
	/*
	public static A_Abstr_Fct_Mthd_ToolFactory<?> getFactory(String tool) {	
		if ("TOOLBAR".equalsIgnoreCase(tool))
			return new A_Abstr_Fct_Mthd_ToolbarFactory();
		else if ("DIALOG".equalsIgnoreCase(tool))
			return new A_Abstr_Fct_Mthd_DialogFactory();
		
		return null;
	}
	 */
}
