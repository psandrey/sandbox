package _playground.java_basics._design_patterns;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class C_Iterator_CustomList<T> implements Iterable<T> {
	LinkedListNode<T> head, tail; 
	
	C_Iterator_CustomList( ) { }
	
	public void add(T data) {
		LinkedListNode<T> node = new LinkedListNode<>(data, null);
		
		if (head == null) tail = head = node;
		else {
			tail.setNext(node);
			tail = node;
		}
	}
	
	public LinkedListNode<T> getHead() {
		return head;
	}
	
	public LinkedListNode<T> getTail() {
		return tail;
	}
	
	@Override
	public Iterator<T> iterator() {
		return new ListIterator();
	}
	
	// default
	class ListIterator implements Iterator<T> {
		LinkedListNode<T> current;
		
		ListIterator() { current = getHead(); }
		
		@Override
		public boolean hasNext() {
			return (current != null);
		}
	
		@Override
		public T next() {
			if (!hasNext()) throw new NoSuchElementException();
            
			T data = current.getData();
			current = current.getNext();
			
			return data;
		}
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String[] args) {
		C_Iterator_CustomList<String> list = new C_Iterator_CustomList<>();
		list.add("abc");
		list.add("def");
		list.add("ghi");
		
		for (String s : list)
			System.out.println(s);
	}
}

