package _playground.java_basics._design_patterns;

public class A_Abstr_Fct_Mthd_DarkToolbar implements A_Abstr_Fct_Mthd_Toolbar {

	@Override
	public void draw() {
		System.out.println("Dark Toolbar: Draw()");
	}
}
