package _playground.java_basics._design_patterns;

public class A_Abstr_Fct_Mthd_DialogFactory implements A_Abstr_Fct_Mthd_ToolFactory<A_Abstr_Fct_Mthd_Dialog> {
	
	public A_Abstr_Fct_Mthd_Dialog create(A_Abstr_Fct_Mthd_ColorType color) {
		switch(color) {
		case DARK:
			return new A_Abstr_Fct_Mthd_DarkDialog();
		case LIGHT:
			return new A_Abstr_Fct_Mthd_LightDialog();
		default:
			return null;
		}
	}
}
