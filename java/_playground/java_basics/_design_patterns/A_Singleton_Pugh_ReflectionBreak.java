package _playground.java_basics._design_patterns;

import java.lang.reflect.Constructor; 

public class A_Singleton_Pugh_ReflectionBreak {
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		System.out.println("Start...");
		A_Singleton_Pugh sgt_1 = A_Singleton_Pugh.getInstance();
		A_Singleton_Pugh sgt_2 = null;
		try {
			Constructor<?>[] ctors = A_Singleton_Pugh.class.getDeclaredConstructors();
			System.out.println("Constructors: " + ctors.length);
			for (Constructor<?> ctor : ctors) {
/*				
				System.out.println("Constructor: " + ctor);
				Class<?>[] pType  = ctor.getParameterTypes();
				System.out.println("Constructor argc: " + pType.length);
				for (int i = 0; i < pType.length; i++)
					System.out.println("Write argument #" + (i + 1) + ". Of type: " + pType[i].getName());

				
				 // Java adds a second synthetic ctor, so we have to make sure we access the
				 // right ctor.
				if (pType.length == 0) {
					ctor.setAccessible(true);		
					sgt_2 = (A_Singleton_Pugh) ctor.newInstance(sgt_1); 
					break;
				}
*/
				ctor.setAccessible(true);		
				sgt_2 = (A_Singleton_Pugh) ctor.newInstance(sgt_1); 
				break;
			}
		} catch (Exception e) { e.printStackTrace(); }
		
		System.out.println("---------------------");
		System.out.println("instance1.hashCode(): " + sgt_1.hashCode());
		System.out.println("instance1.hashCode(): " + sgt_2.hashCode());
	}
}
