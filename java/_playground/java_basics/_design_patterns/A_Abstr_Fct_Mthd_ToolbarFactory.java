package _playground.java_basics._design_patterns;

public class A_Abstr_Fct_Mthd_ToolbarFactory implements A_Abstr_Fct_Mthd_ToolFactory<A_Abstr_Fct_Mthd_Toolbar> {

	@Override
	public A_Abstr_Fct_Mthd_Toolbar create(A_Abstr_Fct_Mthd_ColorType color) {
		switch(color) {
		case DARK:
			return new A_Abstr_Fct_Mthd_DarkToolbar();
		case LIGHT:
			return new A_Abstr_Fct_Mthd_LightToolbar();
		default:
			return null;
		}
	}
}
