package _playground.java_basics._design_patterns;

/*
 * When serializing an enum, field variables are not getting serialized.
 * For example, if we serialize and deserialize the A_Singleton_Enum class,
 * we will lose the value of the String s field.
 */
public enum A_Singleton_Enum {
	INSTANCE;
	
	String s = "Singleton";
	
	public void doSomething() {
		System.out.println(s + " : " + "does something...");
	}
}
