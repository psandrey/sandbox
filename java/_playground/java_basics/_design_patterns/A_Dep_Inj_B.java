package _playground.java_basics._design_patterns;

public class A_Dep_Inj_B {
	public String msg;
	
	A_Dep_Inj_B() { msg = "DYDI Object"; }
}
