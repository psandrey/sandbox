package _playground.java_basics._design_patterns;

import java.io.Serializable;

/* Comments:
 * 
 * Singleton pattern is mostly used in multi-threaded and database applications.
 * It is used in logging, caching, thread pools, configuration settings etc.
 */
public final class A_Singleton_Pugh implements Cloneable, Serializable {
	private static final long serialVersionUID = 1L;
	private final String s;

	private A_Singleton_Pugh() {
		// prevent to instantiate through reflection
		if (SingletonHolder.INSTANCE != null)
			throw new InstantiationError( "Creating of this object is not allowed." );    
		s = "Singleton";
	}
	
	/*
	 * In this method is based on the Java Language Specifications (JLS).
	 * 1. JVM guarantee that a class will not be initialized until is not used. This means
	 * that when class A_Singleton_Pugh is loaded, its private inner class will not be
	 * initialized. But, when getInstance will be called for the first time, the field
	 * SingletonHolder.INSTANCE is accessed thus the inner class SingletonHolder gets
	 * initialized.
	 * 2. JLS ensures that the class initialization phase is sequential. Basically, this
	 * means that the getInstance don't need any synchronization mechanism and all
	 * concurrent invocations will return the same correctly initialized single INSTANCE
	 * without the synchronization overhead.
	 * 
	 */
	private static class SingletonHolder {
		private static final A_Singleton_Pugh INSTANCE = new A_Singleton_Pugh();
	}

	public static A_Singleton_Pugh getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	public void doSomething() {
		System.out.println(s + " : " + "does something...");
	}
	
	// prevent cloning
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }
    
    // protect from serializable:
    // when an object is read, it is replaced with the singleton instance.
    protected Object readResolve() {
    	return getInstance();
    }
}


