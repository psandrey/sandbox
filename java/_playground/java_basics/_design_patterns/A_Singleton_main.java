package _playground.java_basics._design_patterns;

public class A_Singleton_main {
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main(String args[]) {
		// Bill Pugh version
		//A_Singleton sgt1 = A_Singleton.getInstance();
		//A_Singleton sgt2 = A_Singleton.getInstance();

		// Singleton with enum (best method of making singletons in Java)
		A_Singleton_Enum sgt1 = A_Singleton_Enum.INSTANCE;
		A_Singleton_Enum sgt2 = A_Singleton_Enum.INSTANCE;
		
		sgt1.doSomething();
		sgt2.doSomething();

		System.out.println(sgt1 + " " + sgt2);
	}
}
