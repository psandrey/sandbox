package _playground.java_basics._design_patterns;

public final class A_Singleton_Sync {
	private String s;
	private static volatile A_Singleton_Sync INSTANCE;
	
	private A_Singleton_Sync() { s = "Singleton"; }

	public static A_Singleton_Sync getInstance() {
		if (INSTANCE == null) {
			synchronized (A_Singleton_Sync.class) {
				if (INSTANCE == null) {
					INSTANCE = new A_Singleton_Sync();
				}
			}
		}
		
		return INSTANCE;
	}
	
	public void doSomething() {
		System.out.println(s + " : " + "does something...");
	}
}
