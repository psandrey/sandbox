package _playground.java_basics._design_patterns;

public class LinkedListNode<T> {
	private T data;
	private LinkedListNode<T> next;
	
	LinkedListNode(T data, LinkedListNode<T> next) {
		this.data = data;
		this.next = next;
	}
	
	public void setData(T data) {
		this.data = data;
	}
	
	public T getData() {
		return this.data;
	}
	
	public void setNext(LinkedListNode<T> next) {
		this.next = next;
	}
	
	public LinkedListNode<T> getNext() {
		return this.next;
	}
}
