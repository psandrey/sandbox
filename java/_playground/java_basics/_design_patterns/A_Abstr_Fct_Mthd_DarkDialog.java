package _playground.java_basics._design_patterns;

public class A_Abstr_Fct_Mthd_DarkDialog implements A_Abstr_Fct_Mthd_Dialog {

	@Override
	public void draw() {
		System.out.println("Dark Dialog: Draw()");
	}
}
