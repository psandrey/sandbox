package _playground.java_basics._design_patterns;

public class A_Abstr_Fct_Mthd_Client {
	public static void main(String args[]) {
		A_Abstr_Fct_Mthd_ToolFactory<A_Abstr_Fct_Mthd_Toolbar> toolbarFactory =
					A_Abstr_Fct_Mthd_ToolFactoryProvider.getToolbarFactory();
		A_Abstr_Fct_Mthd_Toolbar toolbar = toolbarFactory.create(A_Abstr_Fct_Mthd_ColorType.DARK);

		A_Abstr_Fct_Mthd_ToolFactory<A_Abstr_Fct_Mthd_Dialog> dialogFactory =
					A_Abstr_Fct_Mthd_ToolFactoryProvider.getDialogFactory();
		A_Abstr_Fct_Mthd_Dialog dialog = dialogFactory.create(A_Abstr_Fct_Mthd_ColorType.DARK);
		
		// Draw Dark theme
		System.out.println("--Draw Dark Theme--");
		toolbar.draw();
		dialog.draw();
	}
}
