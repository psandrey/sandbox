
/*
 *-  Greatest Common Divisor based on Euclidian algorithm
 * - Extended Euclidian algorithm to find the smallest linear combination of a and b
 * - Modular Multiplicative Inverse
 */

#include <vector>
#include <iostream>
using namespace std;

class Euclid {
public:
	static int gcd(int a, int b) {
		if (b == 0) return -1;
		if (a == 0) return 0;
		if (b > a) swap(a, b);

		while (b != 0) {
			int r = a % b;
			a = b; b = r;
		}

		return a;
	}

	static int egcd(int a, int b, int* x, int* y) {
		if (b == 0) return -1;
		if (a == 0) return 0;
		if (b > a) swap(a, b);

		int s2 = 1, s1 = 0;
		int t2 = 0, t1 = 1;

		while (b != 0) {
			int q = a / b;
			int r = a % b;

			int s = s2 - q * s1;
			int t = t2 - q * t1;

			s2 = s1; t2 = t1;
			s1 = s; t1 = t;

			a = b; b = r;
		}

		*x = s2; *y = t2;
		return a;
	}

	static int modular_multiplicative_inverse(int a, int m) {
		int x, y, gcd;
		gcd = Euclid::egcd(a, m, &x, &y);
		if (gcd != 1) return -1;

		// wrap around if y > m, and handle negaive values of y
		y = (y % m + m) % m;
		return y;
	}
};

/*
int main(void) {

	//int x, y, gcd;
	//gcd = Euclid::egcd(259, 70, &x, &y);
	//cout << "gcd: " << gcd << ", x: " << x << ", y: " << y << endl;


	int a = 2, m = 7;
	cout << " inverse:" << Euclid::modular_multiplicative_inverse(a, m) << endl;

	return 0;
}
*/