
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class SieveOf	 {
public:
	// version 1
	vector<bool> primes_v1(int n) {
		if (n < 2) return vector<bool>(0);

		vector<bool> p(n + 1, true); p[0] = false; p[1] = false;
		for (int i = 2; i <= n; i++) {
			if (p[i] == false) continue;
			for (int j = 2*i; j <= n; j += i) p[j] = false;
		}

		return p;
	}

	// version 2
	vector<bool> primes_v2(int n) {
		if (n < 2) return vector<bool>(0);

		vector<bool> p(n + 1, true); p[0] = false; p[1] = false;
		for (int i = 2; i <= sqrt(n); i++) {
			if (p[i] == false) continue;
			for (int j = i * i; j <= n; j += i) p[j] = false;
		}

		return p;
	}
};

#if 0
int main(void) {
	int n = 30;

	SieveOfEratosthenes t;
	vector<bool> p = t.primes_v1(n);
	for (int j = 0; j <= n; j++)
		if (p[j] == true) cout << j << " ";
	cout << endl;

	p = t.primes_v2(n);
	for (int j = 0; j <= n; j++)
		if (p[j] == true) cout << j << " ";
	cout << endl;

	return 0;
}
#endif