
#include <iostream>
using namespace std;

namespace smart_ptr {
	template <class T> class SmartPtr {
	private:
		T* ptr;
	public:
		explicit SmartPtr(T* p = NULL) : ptr(p) {} // you can't do implicit conversion	
		~SmartPtr() { delete(ptr); ptr = NULL; }

		T& operator * () { return *ptr; }
		T* operator -> () { return ptr; }
	};

	class A {
	public:
		int x;
		A(int x) : x(x) {}
	};
};

#if 0
int main(void) {
	{
		smart_ptr::SmartPtr<smart_ptr::A> p(new smart_ptr::A(2));
		p->x = 3;
		cout << p->x << endl;
	}

	return 0;
}
#endif