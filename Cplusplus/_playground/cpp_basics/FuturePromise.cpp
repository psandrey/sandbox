#include <thread>
#include <future>
#include <iostream> 
using namespace std;

namespace fp_ns {
	void producer(promise<int>* o) {
		this_thread::sleep_for(chrono::milliseconds(3000));

		cout << "Inside Thread set promise" << endl;
		o->set_value(35);
	}
};

#if 0
int main(void) {
#if 0
	A a(2), b;
	const A c(a);
	const A& d = c;
	 A e;
	e = b;
//#if 0
	promise<int> promise;
	future<int> future = promise.get_future();

	thread th(fp_ns::producer, &promise);

	// main thread is the consumer
	cout << " milestone 1" << endl;
	cout << future.get() << std::endl;
	cout << " milestone 2" << endl;

	th.join();
#endif
	return 0;
}
#endif