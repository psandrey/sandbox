
#include <map>
#include <iterator>
#include <algorithm>
#include <cassert>
#include <cstddef>
#include <functional>
#include <memory>
#include <vector>
#include <iostream>
using namespace std;

namespace play {

	class M {
		static int x;
	public:
		void h() { cout << "from base calls: " << endl; f(); }
		virtual ~M() = 0 {}

		void f() const { x++;  }

	private:
		void virtual f() { cout << "base" << endl; }
	};

	class N : public M {
	public:
		void g() { f(); }
	private:
		void f() { cout << "derived" << endl; }
	};

	class G {
	public:
		int y;
		G(int i) : y(i) { }
	};

	class H {
	public:
		const int x;
		const G* g;
		H(int i) : x(i), g(new G(i)) {}
	};

	class Outter {
	private: int x = 1;
	public: 
		class Inner {
		private: int y = 0;
		public: void f(Outter o) { y = o.x; }
		};
	};
}
	
#if 0
using namespace play;
int main(void) {

	class A : public enable_shared_from_this<A> {
	public:
		int x;
		A(int y) : x(y) {}
		~A() { cout << " destructor called" << endl; }
	};

	A *x = new A(1);

	{
		//std::shared_ptr<A> p1(x);
		//cout << p1.use_count() << endl;
		//cout << endl;
	}

	cout << endl;

#if 0
	std::shared_ptr<A> p2 = x->shared_from_this();

	cout << p1.use_count() << endl;
	cout << p2.use_count() << endl;
	cout << endl;

	p1.reset();
	cout << p1.use_count() << endl;
	cout << p2.use_count() << endl;

	p2.reset();
	cout << p1.use_count() << endl;
	cout << p2.use_count() << endl;
#endif


	return 0;
#if 0
	map<int, int> mp;
	mp[4] = 1;
	mp[1] = 3;

	map<int, int>::iterator itr;
	for (itr = mp.begin(); itr != mp.end(); ++itr) {
		cout << '\t' << itr->first << '\t' << itr->second << endl;
	}
#endif

#if 0
	Outter o;
	Outter::Inner i;
	i.f(o);
#endif

#if 0
	const G* g1 = new G(0);
	const H h(1);
	//h.g->y = 2;
	cout << h.g->y << endl;
	int b = 1;
	int c = 2;

	const int* a;
	a = &c;
	a = &b;

	M* m = new N();
	m->h();
#endif
	return 0;
}

#endif