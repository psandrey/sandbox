
#include <iostream>
using namespace std;

class A {
public:

	virtual void a() = 0;
};

class B : public A {
public :
	void a() { cout << "I am a of B" << endl; }
};

class X {
public:
	int* x;
	X() { cout << " Ctor X" << endl; x = new int; *x = 10; }
	~X() { cout << " Dtor X" << endl; delete x; x = NULL;  }
	X(const X& that) { cout << "X : copy constructor called:" << this << endl; }
	X& operator=(const X& that) { cout << "X : assignment operator called:" << this << endl; return *this; }
};

class Y {
public:
	X f() {
		X x;
		return x;
	}
};

#if 0
int main(void) {
	int x = 1;
	int& y = x;
	cout << x << "  " << y << endl;
	cout << &x << "  " << &y << endl;

	B b;
	A &a = b;
	a.a();

	return 0;
}
#endif