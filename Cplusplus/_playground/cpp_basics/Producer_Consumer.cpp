
#include <string>
#include <mutex>
#include <queue> 
#include <iostream> 
#include <thread> 
using namespace std;

namespace prod_com {

	static mutex print_m;
	void print_atomic(string s) {
		lock_guard<mutex> lock(print_m);
		cout << s << endl;
	}

	class Items {
	private :
		int capacity;
		queue<int> _q;
		mutex _m;
		int _next_item;
		condition_variable q_full;
		condition_variable q_empty;

	public:
		Items(int cap):_next_item(0), capacity(cap) { }

		void produce() {
			unique_lock<mutex> lock(_m);
			while (_q.size() == capacity) q_full.wait(lock);
			_q.push(++_next_item);
			q_empty.notify_one();
			//print_atomic("- Q size:" + to_string((int)_q.size()));
		}

		int consume() {
			unique_lock<mutex> lock(_m);
			while (_q.empty()) q_empty.wait(lock);
			int item = _q.front();
			_q.pop();
			q_full.notify_one();
			//print_atomic("- Q size:" + to_string((int)_q.size()));
			return item;
		}
	};

	class Producer {
	private:
		int n_items;
		Items& items;
	public:
		Producer(int n_items, Items& items) :n_items(n_items), items(items) { }

		void operator()() {
			for (int i = 0; i < n_items; i++) {
				items.produce();
				print_atomic(">>> produced item : " + to_string(i));
			}
		}
	};

	class Consumer {
	private:
		int n_items;
		Items& items;
	public:
		Consumer(int n_items, Items& items) :n_items(n_items), items(items) { }

		// functor
		void operator()() {
			for (int i = 0; i < n_items; i++) {
				int item = items.consume();
				print_atomic("<< consumed item : " + to_string(item));
				this_thread::sleep_for(chrono::milliseconds(rand()%17 + 1));
			}
		}
	};

};

#if 0
int main(void) {
	int ITMS = 100;
	int QLEN = 5;

	srand((unsigned)time(0));

	prod_com::Items itms(QLEN);
	prod_com::Producer producer(ITMS, itms);
	prod_com::Consumer consumer(ITMS, itms);

	thread th_p(producer);
	thread th_c(consumer);

	// Note: after join, a thread obj has no associated thread with it.
	if (th_p.joinable()) th_p.join();
	if (th_c.joinable()) th_c.join();
	cout << "... Job done..." << endl;

	return 0;
}
#endif