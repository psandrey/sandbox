
#include <time.h> 
#include <vector>
#include <iostream>
using namespace std;

#include "Template_ex1.h"

namespace tns {
	// copy constructor. assignment operator
	//-----------------------------------------------------------------------------
	class B {
	public:
		B() { cout << "B : default constructor called:" << this << endl; }
		B(const B& b) { cout << "B : copy constructor called:" << this << endl; }
		B& operator=(const B& b) { cout << "B : assignment operator called:" << this << endl; return *this; }
		~B() { cout << "B : destructor called:" << this << endl; }
	};

	class A {
	public:
		B b;
		A(B& b) : b(b) { cout << "A : constructor called" << endl; }
		//A(B& x) { b = x; }
	};

	// class, struct, union
	//-----------------------------------------------------------------------------
	namespace test_ns {
		class test_t {
		public:
			test_t(int y = 0) : x(y) { cout << "test_t constructor called" << endl; };
			~test_t() { cout << "test_t destructor called" << endl; };

			int x;
			void f() { cout << "test_t f called" << endl; }

		private:
			void g() { }
		};

		struct test_s {
		public:
			test_s(int y = 0) : x(y) { cout << "test_s constructor called" << endl; };
			~test_s() { cout << "test_s destructor called" << endl; };

			int x;

			void f() { cout << "test_s f called" << endl; }

		private:
			void g() { }
		};

		union test_u {
			test_s s;
			test_t t;

			//test_u() { cout << "test_u constructor called" << endl; };
			~test_u() { cout << "test_u destructor called" << endl; };

			int g(int x) {}
		};
	}

	// inheritance
	//-----------------------------------------------------------------------------
	class X {
	public:
		X(int y = 0) : x(y), xx(y), xxx(y) { cout << "X constructor called " << x << endl; };
		virtual ~X() { cout << "X destructor called" << endl; }

		virtual void func() { cout << "X's func called " << endl; }

		int x;
	protected:
		int xx;
	private:
		int xxx;
	};

	class Y : virtual public X {
	public:
		Y(int z = 0) : y(z) { cout << "Y constructor called " << y << endl; };
		virtual ~Y() { cout << "Y destructor called" << endl; }

		virtual void func() = 0 { cout << "Y's func called " << endl; }

		int y;
	};

	class Z : virtual public X, Y {
	public:
		Z(int w = 0) : Y(w), X(w), z(w) { cout << "Z constructor called: " << z << endl; }
		~Z() { cout << "Z destructor called" << endl; }

		void func() { cout << "Z's func called " << Y::xx << endl; }

		int z;
	};

	class BaseO {
	public:
		void f() { cout << "Base f" << endl;  g(); }

	private:
		virtual void g() const { cout << "Base g" << endl; }
	};

	class DerivedO : virtual public BaseO {
	public:
		void f() { cout << "Derived f" << endl; }

	private:
		virtual void g() const { cout << "Derived g" << endl; }
	};

	class DownUpCastBase {
	private:
		int x;

	public:
		DownUpCastBase() : x(0) { cout << " Base this addr: " << this << endl; }
		void printThisAddr() { cout << " Base print this addr: " << this << endl; }

		//virtual void f() { x = 0; }
	};

	class DownUpCastDerived : public DownUpCastBase {
	private:
		int y;

	public:
		DownUpCastDerived() : y(0) { cout << " Derived this addr: " << this << endl; }
		void printThisAddr() { cout << " Derived print this addr: " << this << endl; }
		void printBase() { cout << " From derived: "; DownUpCastDerived::DownUpCastBase::printThisAddr(); }

		virtual void f() { y = 0; }
	};

	//templates
	//-----------------------------------------------------------------------------
	template<class T = int, int n = 10> class Array {
	public:
		vector<T> arr;
		void insert(T x) { }; // primary template member definition
	};

	template<> class Array<int, 2> {
	public:
		vector<int> arr;
		void insert(int x) {
			if (arr.size() < 2)
				arr.push_back(x);
		}
	};

	// overload
	//-----------------------------------------------------------------------------
	int myadd(int x, int y) { return (x + y); }
	float myadd(float x, float y) { return (x + y); }

	void ol_d(int x, int y = 0) { cout << " 1 " << endl; }
	//void ol_d(int x, int y) { cout << " 1 " << endl; } // cannot be overloaded like this


	class OL_OP {
	public:
		int x, y;
		OL_OP(int x, int y) :x(x), y(y) {};

		OL_OP& operator+=(const OL_OP& rhs) {
			this->x = this->x + rhs.x;
			this->y = this->y + rhs.y;

			return (*this);
		};

		/* << overloaded as a member function (a << b) is interpreted as a.operator<<(b),
		 *  so it only takes one explicit parameter (with this as a hidden parameter).
		 *
		 * It would require that your overload be part of the ostream class, not part of your class.
		 * Since you're not allowed to modify ostream, you can't do that.
		 * That leaves only the global overload as an alternative.
		 */
		friend ostream& operator<<(ostream& out, const OL_OP& rhs);
	};

	ostream& operator<<(ostream& out, const OL_OP& rhs) {
		out << rhs.x << " " << rhs.y << " ";
		return out;
	};

	// cast
	//-----------------------------------------------------------------------------
	class Cnst {
	private:
		int x = 1;
	public:
		int getX() const {
			// x = 2; cannot do this in a const member func, so we need to const_cast
			(const_cast <Cnst*> (this))->x = 2;
			return x;
		}
		void setX(int* y) { x = (*y); }
	};

	class Base {
	public:
		virtual void DoIt() = 0;    // pure virtual
		virtual ~Base() {};
	};

	class Foo : public Base {
	public:
		virtual void DoIt() { cout << "Foo/"; };
		void FooIt() { cout << "fooing It..." << endl; }
	};

	class Bar : public Base {
	public:
		virtual void DoIt() { cout << "Bar/"; }
		void BarIt() { cout << "baring It..." << endl; }
	};

	Base* CreateRandom()
	{
		if ((rand() % 2) == 0) return new Foo;
		else return new Bar;
	}

	// exception handling
	//-----------------------------------------------------------------------------
	class demo1 { };
	class demo2 : public demo1 { };

	// const functions modify static members
	//-----------------------------------------------------------------------------
	class AAA {
	public:
		static int a;

		void set(int x) const { AAA::a = x; }
	};
	int AAA::a = 0;

	// multiple inheritance
	//-----------------------------------------------------------------------------
	class MI1 {
	public:
		void f() { cout << " MI1 " << endl; }
	};

	class MI2 {
	public:
		void f() { cout << " MI2 " << endl; }
	};

	class MI : public MI1, public MI2 {

	};

	// oreder of initialization
	//-----------------------------------------------------------------------------
	class O1 {
	public:
		static int a;
		int aa = 2;
		O1(int x) : aa(x) { cout << "Constructor O1" << endl; }
	};
	int O1::a = 1;

	class O2 : public O1 {
	public:
		static int b;
		int bb = 3;
		O2(int x) : O1(x), bb(x) { cout << "Constructor O2" << endl; }
	};
	int O2::b = 3;

#if 0
	struct StaticBlock {
		StaticBlock() {
			cout << "Fake static block" << endl;
		}
	};
	static StaticBlock staticBlock;
#endif

	// final keyword from Java to C++ (from C11)
	//-----------------------------------------------------------------------------
	class A1 final {
	public:
		virtual void f() final = 0; // I can do this... strange
	};

	class B1 {
	public:
		int x = 1;
		// cannot inherit A1 and cannot override f
	};
};

#if 0
using namespace tns;

int main(void) {

#if 0
	O2 o(0);
	cout << endl << endl << o.bb << endl;
#endif

#if 0
	MI m;
	m.MI1::f();
	m.MI2::f();
#endif

#if 0
	const AAA t;
	t.set(1);
#endif

#if 0
	for (int i = 1; i <= 2; i++) {
		try {
			if (i == 1) throw demo1();
			else if (i == 2) throw demo2();
		}
		catch (demo1 d1) {
			cout << "Caught exception of demo1 class \n";
		}
		catch (demo2 d2) {
			cout << "Caught exception of demo2 class \n";
		}
	}
#endif

#if 0
	Cnst t;
	t.getX();

	const int x = 3;
	const int* ptr = &x;
	t.setX(const_cast <int*>(ptr));

	srand((int)time(NULL));
	for (int n = 0; n < 10; ++n) {
		Base* base = CreateRandom();
		base->DoIt();

		try {
			Bar* bar = dynamic_cast<Bar*>(base);
			if (bar) bar->BarIt();
		}
		catch (const std::bad_cast & e) {
			std::cout << e.what() << '\n';
		}
	}
#endif

#if 0
	// placement new example
	int x = 10;
	cout << x << endl;
	new(&x)int(20);
	cout << x << endl;
#endif

#if 0
	OL_OP o1(1, 2);
	OL_OP o2(3, 4);
	o1 += o2;
	cout << o1 << endl;
#endif

#if 0
	int a = 1, b = 2;
	cout << a << " " << b << endl;
	xchg<int>(a, b);
	cout << a << " " << b << endl;

	string s = "foo", t = "bar";
	cout << s << " " << t << endl;
	xchg<string>(s, t);
	cout << s << " " << t << endl;

	char x = 'a', y = 'b';
	//xchg<string>(x, y); // error: no overloads matches the argument list

	//Array<int, 10> arr;
	Array<int, 2> arr;
	arr.insert(10);
	arr.insert(20);
	arr.insert(30);
	for (int x : arr.arr) cout << x << " ";

#endif

#if 0
#if 0
	BaseO* o = new DerivedO();
	o->f();
	//o->g();
#endif

#if 0
	DownUpCastDerived* x = new DownUpCastDerived();
	cout << endl;

	//DownUpCastBase* y = static_cast<DownUpCastBase*>(x);
	DownUpCastBase* y = dynamic_cast<DownUpCastBase*>(x);
	if (y) y->printThisAddr();
#endif

#if 1
	DownUpCastBase* x = new DownUpCastDerived();
	cout << endl;

	DownUpCastDerived* y = static_cast<DownUpCastDerived*>(x);
	//DownUpCastDerived* y = dynamic_cast<DownUpCastDerived*>(x);
	if (y) {
		y->printThisAddr();
		y->printBase();
	}
#endif

#if 0
	X* z = new Z(100);
	z->func();
	delete z;
#endif
#endif

#if 0
	test_ns::test_u u;

	new (&u.t) test_ns::test_t(10);
	//u.t.x = 1;
	cout << u.t.x << endl;
	u.t.f();
	u.t.~test_t();

	new (&u.t) test_ns::test_s(20);
	//u.s.x = 2;
	u.s.f();
	cout << u.t.x << endl;
	u.s.~test_s();

#endif

#if 0
	B b;

	cout << endl << endl << "start" << endl;
	A a(b);
	cout << "end" << endl << endl;

	cout << &a.b << endl;
#endif

#if 0
	cout << myadd(1, 1) << endl;
	cout << myadd(1.1f, 1.1f) << endl;
#endif
	return 0;
}
#endif