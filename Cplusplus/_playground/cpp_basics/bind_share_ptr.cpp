
#include <random>
#include <iostream>
#include <memory>
#include <functional>
using namespace std;

namespace ns_bind_share_ptr {
    struct Good : std::enable_shared_from_this<Good>
    {
        std::shared_ptr<Good> getptr() {
            return shared_from_this();
        }
    };

    struct Bad
    {
        std::shared_ptr<Bad> getptr() {
            return std::shared_ptr<Bad>(this);
        }
        ~Bad() { std::cout << "Bad::~Bad() called\n"; }
    };
}

#if 0
using namespace ns_bind_share_ptr;
int main(void) {

    // Good: the two shared_ptr's share the same object
    std::shared_ptr<Good> gp1 = std::make_shared<Good>();
    std::shared_ptr<Good> gp2 = gp1->getptr();
    std::cout << "gp2.use_count() = " << gp2.use_count() << '\n';

    // Bad, each shared_ptr thinks it's the only owner of the object
    std::shared_ptr<Bad> bp1 = std::make_shared<Bad>();
    std::shared_ptr<Bad> bp2(bp1); //= bp1->getptr();
    std::cout << "bp2.use_count() = " << bp2.use_count() << '\n';

	return 0;
}
#endif