
#include <unordered_set>
#include <iostream>
using namespace std;

namespace ns_autoptr {
	template<class T>
	class Auto_ptr3
	{
		T* m_ptr;
	public:
		Auto_ptr3(T* ptr = nullptr)
			:m_ptr(ptr)
		{
		}

		~Auto_ptr3()
		{
			delete m_ptr;
		}

		// Copy constructor
		// Do deep copy of a.m_ptr to m_ptr
		Auto_ptr3(const Auto_ptr3& a)
		{
			m_ptr = new T;
			*m_ptr = *a.m_ptr;
		}

		// Copy assignment
		// Do deep copy of a.m_ptr to m_ptr
		Auto_ptr3& operator=(const Auto_ptr3& a)
		{
			// Self-assignment detection
			if (&a == this)
				return *this;

			// Release any resource we're holding
			delete m_ptr;

			// Copy the resource
			m_ptr = new T;
			*m_ptr = *a.m_ptr;

			return *this;
		}

		T& operator*() const { return *m_ptr; }
		T* operator->() const { return m_ptr; }
		bool isNull() const { return m_ptr == nullptr; }
	};

	class Resource
	{
	public:
		Resource() { std::cout << "Resource acquired\n"; }
		~Resource() { std::cout << "Resource destroyed\n"; }
	};

	Auto_ptr3<Resource> generateResource()
	{
		Auto_ptr3<Resource> res(new Resource);
		return res; // this return value will invoke the copy constructor
	}

	unique_ptr<Resource> get_resource() {
		return unique_ptr<Resource>(make_unique<Resource>());
	}

	class Person {
	public:
		string name;
		//shared_ptr<Person> partner;
		weak_ptr<Person> partner;
		Person(string n) : name(n) { cout << "Create: " << name << endl; }
		~Person() { cout << "Destroy: " << name << endl; }

		const std::shared_ptr<Person> getPartner() const { return partner.lock(); }
	};


	// custom deleter
	class C { };
	struct DC {
		void operator() (C* c) { cout << "$$ This is custom deleter $$" << endl; }
	};

	void del(C* c) { cout << "$$ This is custom deleter $$" << endl; }

	class custom_del_tst {
	private :
		//unique_ptr<C, DC> p; // class
		unique_ptr<C, decltype(&del)> p; // free function

	public:
		custom_del_tst() :p (new C, del) { 
			throw 0;
		}
	};

	// ------------------------------------------------------------------------
	class UA {
	private:
		int id;
	public:
		UA(int x) : id(x) { }
		int get() { return id; }
	};

	struct hash_func {
		size_t operator()(UA& a) const {
			return hash<int>()(a.get());
		}
	};

	int returnByValue() {
		return 5;
	}

	int& returnByReference() {
		static int x{ 5 };

		return x;
	}


	class BLA {
	public:
		BLA() {
			cout << "default ctor" << endl;
		}

		BLA(BLA& that) {
			cout << "copy ctor" << endl;
		}

		BLA& operator=(BLA& that) {
			cout << "asgn op" << endl;
			return *this;
		}
	};

	BLA& return_by_ref() {
		static BLA x;

		return x;
	}

	BLA return_by_val() {
		BLA x;
		return x; // this is a x-value (it has an address)
	}
}

#if 0
using namespace ns_autoptr;
int main() {

#if 0
	//BLA x{ return_by_ref() }; // call copy ctor
	//BLA& y{ return_by_ref() }; // do not call copy ctor
	//BLA& z{ return_by_val() }; // calls copy ctor
	//BLA z{ return_by_val() };  // calls copy ctor


	//int x = returnByReference(); // 
	//int& y{ returnByValue() };   // compile error (5 si a pure rvalue)
	//const int& y{ returnByValue() }; // he lifetime of the return value is extended to the lifetime of y
	//cout << x << endl;
#endif

	return 0;
#if 0
	cout << isnan(sqrt(-1.0)) << endl;
#endif

#if 0
	// custom unordered_set using custom functor
	unordered_set<UA, hash_func> s;
#endif

#if 0
	try {
		custom_del_tst d;
	}
	catch (int x) {
		cout << "Caught : " << x << endl;

	}
#endif

#if 0
	{
		shared_ptr<Person> p1 = make_shared<Person>("p1");
		shared_ptr<Person> p2 = make_shared<Person>("p2");
		shared_ptr<Person> p3 = make_shared<Person>("p3");
		p1.get()->partner = p2;
		p2.get()->partner = p3;
		p3.get()->partner = p1;

		cout << "p1 cnt: " << p1.use_count() << endl;
		cout << "p2 cnt: " << p2.use_count() << endl;
		cout << "p3 cnt: " << p3.use_count() << endl;

		shared_ptr<Person> p1_partner_p2 = p1.get()->getPartner();
		cout << "- p2 cnt: " << p2.use_count() << endl;
		cout << "- p1_partner_p2 cnt: " << p1_partner_p2.use_count() << endl;
	}
#endif

#if 0
	shared_ptr<Resource> r1(make_shared<Resource>());
	shared_ptr<Resource> r2 = r1;

	cout << r1.use_count() << endl;
	cout << r2.use_count() << endl;
#endif

#if 0
	Auto_ptr3<Resource> mainres;
	mainres = generateResource(); // this assignment will invoke the copy assignment
#endif

#if 0
	unique_ptr<Resource> p = get_resource();
	if (p.operator bool() == false)
		cout << " NULL" << endl;
	else cout << " not Null" << endl;
#endif

	return 0;
}
#endif