
namespace t2ns {

	class A {
	private:
		int x;
	public:
		bool operator>=(A& that) {
			return (this->x >= that.get_x());
		}

		int get_x() { return x; }
		void set_x(int v) { x = v; }
	};

	// template functions can be overloaded
	template <typename T>
	T max(T x, T y) {
		return ((x >= y) ? x : y);
	}

	template <typename T>
	T max(T x, T y, T z) {
		if (x >= y && x >= z) return x;
		if (y >= x && y >= z) return y;

		return z;
	}

	template <typename T>
	T average(T *array, int length) {
		T sum(0);

		for (int count{ 0 }; count < length; ++count)
			sum += array[count];

		sum /= length;
		return sum;
	}

	template <typename T, int n>
	void print(T t) {
		cout << n << endl;
	}
};