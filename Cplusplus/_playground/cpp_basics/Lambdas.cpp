
#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

namespace ns_lambdas {
	struct FunctorClass {
		void operator()(int n) {
			cout << n;

			if (n % 2 == 0) cout << " is even " << endl;
			else cout << " is odd " << endl;
		}
	};

	struct test {
		int x;
	};
};

#if 0
using namespace ns_lambdas;
int main(void) {

#if 0
	int x = 0, y = 1;
	[&, y]() { x += y; } ();
	cout << x << " : " << y << endl;
#endif

#if 0
	// mutable allows to change variable inside lambda when they are captured by value 
	int m = 0;
	int n = 0;
	[&, n](int a) mutable { n++; }(0);
	cout << m << endl << n << endl;
#endif

#if 0
	test t;
	t.x = 1;
	auto y = [&, t](int a) { cout << t.x << " : " << a << endl; };

	y(2);
#endif

#if 0
	vector<int> v;
	for (int i = 1; i < 10; ++i) v.push_back(i);

	// for_each using functor
	for_each(v.begin(), v.end(), FunctorClass());
	
	cout << endl;
	
	// for each using lambda
	for_each(v.begin(), v.end(), [](int n) {
		cout << n;

		if (n % 2 == 0) cout << " is even " << endl;
		else cout << " is odd " << endl;

		});
#endif
	
	return 0;
}
#endif