
#include <iostream>
using namespace std;

#if 0
#if 0
#include <C_tests1.h>

#define MIN(A,B) ((A) <=  (B) ? (A) : (B))

int f(int x) { return x; }
int (*g)(int);

static int xx; // visible only in this module, and initialized by the compiler to 0
static int fstatic(int x) { return x; } // visible only in this module
#endif

#if 0
#define _min(a, b) (a) < (b) ? (a) : (b)

char cc = 'X';
struct {
	int a = 0x11121314;
	double b = 0x212223242522728;
	char* c = &cc;
	char d[7] = { 'A', 'B', 'C','D','E','F', 'G' };
	short e = 0x33;
	int f = 0x31323334;
} s;
#endif

int main(void) {
#if 0
	cout << sizeof(s.a) << endl;
	cout << sizeof(s.b) << endl;
	cout << sizeof(s.c) << endl;
	cout << sizeof(s.d) << endl;
	cout << sizeof(s.e) << endl;
	cout << sizeof(s.f) << endl;

	cout << sizeof(s) << endl;
 	return 0;
#endif

#if 0
	int a = 0x01020304;
	char* p = (char *)&a;
	for (int i = 0; i < 4; i++)
		printf("%d ", p[i]);
#endif

#if 0
	char* p = NULL;
	p = (char*)malloc(0);
	*p = 'a';
	cout << p << endl;
#endif
	// pointer to a function
#if 0
	g = &f;
	int ans = g(1);
	cout << ans << endl;
#endif

	// const and pointers
#if 0
#if 0
	int x = 10;
	int* p;
	int** pp;

	p = &x;
	pp = &p;
	cout << (&x) << " : " << x << endl;
	cout << (&p) << " : " << p << " : " << (*p) << endl;
	cout << (&pp) << " : " << pp << " : " << (*pp) << " : " << (**pp) << endl;
#endif

#if 0
	int b = -1;
	int a0 = 0;
	const int a1 = 1;
	int const a2 = 2;

	int * const a3 = &a0;
	(*a3) = 10;
	printf("%d \n", a0);
	
	int const * const a4 = (&a1);
	printf("%d \n",*a4);

	const int* a5 = &a1;
	a5 = &a2;

	int const* a6 = &a1;
	a6 = &a2;
#endif
#endif

	// precedence of operations
#if 0
	int a[] = { 1, 2, 3 };
	int* p = a;

	cout << endl << endl;
	cout << p << " : " << *p << endl;
	(*p++);
	cout << p << " : " << *p << endl;
	for (int i = 0; i < 3; i++) cout << a[i] << " ";
#endif

#if 0
	int a[] = { 1, 2, 3 };
	int* p = a;

	cout << endl << endl;
	cout << p << " : " << *p << endl;
	(++ * p);
	cout << p << " : " << *p << endl;
	for (int i = 0; i < 3; i++) cout << a[i] << " ";
#endif

#if 0
	int ans = MIN(*p++, b);
	cout << ans << endl;
#endif

	return 0;
}
#endif
