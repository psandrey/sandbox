#include <memory>
#include <iostream>
using namespace std;

namespace exns {
	
	class E1 { public: void why() { cout << "This is the E1 exception" << endl; } };
	class E2 : public E1 { public: void why() { cout << "This is the E2 exception" << endl; } };

	class A {
	public:
		A() { cout << " A: ctor" << endl; }
		~A() { cout << " A: dtor" << endl; }

		void g() throw () {} // this function does not throw any exceptoion

		void f() throw(...) { // throws any type of exceptions
			try {
				int x = 10;
				cout << " throw x" << endl;
				throw x;
			}
			catch (int x) {
				cout << " catch x:" << x <<  endl;
				throw string("exception");
			}
		}

	public:
		virtual void h() throw (E2 &) { cout << "--Base" << endl; throw E2(); }
	};

	/* g++ requires to respect base class contract, MSVC++ does not.
	 * Base class contract: if base function throw an exception then the derived class
	 *
	 * must throw the same or child of that exception or none (so go more specific, not more basic).
	 */
	class B : public A {
	public:
		virtual void h() throw (int) override { cout << "--Derived(overridden)" << endl; int x = 1; throw x; }
	};
};

#if 0
using namespace exns;

int main(void) {
#if 0
	A* o = new B();
	try {
		o->h();
	}
	catch (E2 & e) { cout << "catch E2" << endl; e.why(); }
	catch (E1 & e) { cout << "catch E1" << endl; e.why();  }
	catch (int x) { cout << "catch: " << x << endl; }
#endif

#if 0
	exns::A a;
	try {
		a.f();
	}
	catch (string x) {
		cout << x << endl;
	}
#endif
}
#endif