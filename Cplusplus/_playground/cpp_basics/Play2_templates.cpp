
#if 0
#include "Play2_templates.h"
#include <iostream>
using namespace std;
using namespace t2ns;

// inline namespace
namespace ns {
	inline namespace ns2 {
		void f() { cout << "ns2" << endl; }
	}

	namespace ns3 {
		void f() { cout << "ns3" << endl; }
	}

	namespace ns4 {
		void f() { cout << "ns4" << endl; }
	}
}
using namespace ns;

int x = 20;
namespace outer {
	int x = 10;
	namespace inner {
		int x = 30;
	}
}

int main(void) {
#if 0
	int  i = 0;
	for (i = 0; i < 10; i++)
		cout << i << " ";
	cout << endl << i << endl << endl;

	i = 0;
	for (i = 0; i < 10; ++i)
		cout << i << " ";
	cout << endl << i << endl << endl;
#endif

#if 0
	cout << x << ", " << outer::x << ", " << outer::inner::x << endl;
#endif

#if 0
	f(); // default
	ns3::f();
	ns4::f();
#endif

#if 0
	A x, y, z;
	x.set_x(3);
	y.set_x(2);
	z.set_x(4);

	A b = max(x, y);
	cout << b.get_x() << endl;

	A c = max(x, y, z);
	cout << c.get_x() << endl;

	int a[] = { 1, 2, 3, 4, 5 };
	cout << average(a, 5) << endl;

	print<A, 4>(x);
#endif

	return 0;
}

#endif