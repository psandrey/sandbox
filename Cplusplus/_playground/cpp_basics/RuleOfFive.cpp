
#include <iostream>
using namespace std;

namespace rule_of_five {
	class X {
	public:
		int x;
		X() : x(0) {}
		X(int i) : x(i) { }

		int get() { return x; }
		void set(int i) { x = i; }
	};

	class Y {
	public:
		int y;
		Y() : y(0) {}
		Y(int i) : y(i) { }

		int get() { return y; }
		void set(int i) { y = i; }
	};

	class A {
	public:
		int a = 0;
		X* x = NULL; // should be encapsulated into unique_ptr

		A() : a(0), x(NULL) { cout << " A: default ctor: " << this << endl; }
		A(int i) : a(i), x(new X(i)) { cout << " A: ctor: " << this << endl; }
		~A() { cout << " A: dtor: " << this << endl; delete x; }

		// copy constructor
		A(const A& that) : a(that.a), x(new X(that.x->x)) { cout << " A: copy-ctor: " << this << endl; }

		// asignment operator
		A& operator=(const A& that) {
			cout << " A: assign-op: " << this << endl;

			if (this == &that) return *this;

			a = that.a;
			delete x;
			x = new X(that.x->x);

			return *this;
		}

		// move constructor
		A(A&& that) noexcept : a(0), x(NULL) {
			cout << " A: move ctor: " << this << endl;

			if (this == &that) return;
			this->a = that.a;
			this->x = that.x;

			that.a = 0;
			that.x = NULL;
		}

		// move assignment
		A& operator=(A&& that) noexcept {
			cout << " A: move-op: " << this << endl;

			if (this == &that) return *this;

			delete this->x;

			a = that.a;
			x = that.x;
			that.a = 0;
			that.x = NULL;

			return *this;
		}

		// getters and setters
		int getA() { return a; }
		int getX() {
			if (x) return x->get();
			return -1;
		}
		void setA(int i) { a = i; }
		void setX(int i) { if (x) x->set(i); }
	};

	A getResourceA() {
		A a(10);
		return a;
	};

	class B : public A {
	public:
		int b;
		Y* y; // should be encapsulated into unique_ptr

		B() : A(), b(0), y(NULL) { cout << " B: default ctor: " << this << " ( y : " << y << " ) " << endl; }
		B(int i) : A(i), b(i), y(new Y(i)) { cout << " B: ctor: " << this << " ( y : " << y << " ) " << endl; }
		~B() { cout << " B: dtor: " << this << " delete " << " ( y : " << y << " ) " << endl; delete y; }

		// copy constructor
		B(const B& that) : A(that), b(that.b), y(new Y(that.y->y)) { cout << " B: copy-ctor: " << this << endl; }


		// asignment operator
		B& operator=(const B& that) {
			cout << " B: assign-op: " << this << " delete " << " ( y : " << y << " ) " << endl;

			if (this != &that) {
				this->A::operator=(that);

				b = that.b;
				delete y;
				y = new Y(that.y->y);
			}

			return *this;
		}

		// move constructor:
		//
		/* Note: Above that the argument x is treated as an lvalue internal to the move functions,
		 * even though it is declared as an rvalue reference parameter. That's why it is necessary to say move(x)
		 * instead of just x when passing down to the base class. This is a key safety feature of move semantics
		 * designed to prevent accidently moving twice from some named variable. All moves occur only from rvalues,
		 * or with an explicit cast to rvalue such as using std::move. If you have a name for the variable,
		 * it is an lvalue.
		 */
		B(B&& that) noexcept : A(move(that)), b(0), y(NULL) {
			cout << " B: move ctor: " << this << endl;

			if (this == &that) return;
			this->b = that.b;
			this->y = that.y;

			that.b = 0;
			that.y = NULL;
		}

		// move assignment	
		B& operator=(B&& that) noexcept {
			cout << " B: move-op: " << this << " delete " << " ( y : " << y << " ) " << endl;

			if (this != &that) {
				A::operator=(move(that));

				delete y;

				b = that.b;
				y = that.y;
				that.b = 0;
				that.y = NULL;
			}

			return *this;
		}

		int getB() { return b; }
		int getY() {
			if (y) return y->get();
			return -1;
		}
		void setB(int i) { b = i; }
		void setY(int i) { if (y) y->set(i); }
	};

	B getResourceB() {
		B b(10);
		return b;
	};

	static A func(A a) {
		cout << "do something" << endl;
		return a;
	}

};

#if 0
int main(void) {
	rule_of_five::A a(0);
	rule_of_five::func(a); // argument by value : copy constructor is invoked here

#if 0
	rule_of_five::B b1(10);
	rule_of_five::B b2 = b1;
	
	b1.x->x = 200;
	cout << b2.x->x <<  " " << b1.x->x << endl;
#endif

#if 0
	rule_of_five::A a(1);

	/* Two steps:
	 * 1. construction of the right side of the assignment (move constructor is called)
	 * 2. the assignment (since the right side is a rvalue, then move assignment is called)
	 */
	cout << a.x->get() << endl;
	a = rule_of_five::getResourceA();
	cout << a.x->get() << endl;

#endif

#if 0
	rule_of_five::B b1(10);
	rule_of_five::B b2(b1); // copy constructor

	b1.setX(20);
	cout << "b1 :" << b1.getX() << endl;
	cout << "b2 :" << b2.getX() << endl;
#endif

	rule_of_five::B b(0);

	//cout << b.getX() << endl << endl;
	b = rule_of_five::getResourceB();
	cout << b.getX() << endl;

	return 0;
}
#endif