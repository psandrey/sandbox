
namespace nested_ns {
	class A {
	public:
		class B { // friend of A
		public:
			void bf(A a) { a.af(*this); }
		private:
			void bg() { }
		};
	private:
		void af(B& b) {
			//b.bg(); // error
		}
	
	};
}

int main(void) {

	return 0;
}