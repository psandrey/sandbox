
#include <memory>
#include <iostream>
using namespace std;

namespace RAII_tests {
	class X {
	public:
		X(int x) : x(x) { cout << " X: ctor: " << x << endl; }
		~X() { cout << " X: dtor: " << x << endl; }

		int get() { return x; }
		void set(int y) { x = y; }

	private:
		int x;
	};

	class Xptr {
	public:
		explicit Xptr(X* p = NULL) noexcept : ptr(p) {
			cout << " Xptr: ctor - this: " << this << " - p: " << ptr << endl; }
		~Xptr() {
			cout << " Xptr: dtor - this: " << this << " this.ptr: " << ptr << endl;
			if (ptr != nullptr) { delete (ptr); ptr = nullptr; }
		}
		Xptr(const Xptr& that) : ptr(that.ptr) {
			cout << " Xptr: copy-ctor - this.ptr: " << ptr << " that.ptr: " << that.ptr << endl;
		}
		Xptr& operator=(Xptr& that) {
			cout << " Xptr: assign-op - this: " << this << " this.ptr: " << ptr << " that: " << &that << " that.ptr: " << that.ptr << endl;

			if (this != &that) {
				delete ptr;
				ptr = that.ptr;
				that.ptr = nullptr;
			}

			return *this;
		}

		X& operator *() { return *ptr; }
		X* operator ->() { return ptr; }

	private:
		X* ptr;
	};

	class A {
	private:
		Xptr x;

	public:
		A() {
			x = Xptr(new X(10));
			cout << " A: ctor" << endl;
		}
		~A() { cout << " A: dtor" << endl; }

		int get() { return x->get(); };
	};
};

#if 0
int main(void) {
	//RAII_tests::A a;
	//cout << " result: " << a.get() << endl;
	RAII_tests::Xptr x;
	x = RAII_tests::Xptr(new RAII_tests::X(10));

	return 0;
}
#endif