#ifndef TEMPLATE_EX1 
#define TEMPLATE_EX1 
 
#include <iostream> 
using namespace std;

namespace tns {

	template <typename T>
	void xchg(T& a, T& b) { };

	template<>
	void xchg(string& x, string& y) {
		x.swap(y);
	};

	template<>
	void xchg(int& x, int& y) {
		int t = x;
		x = y;
		y = t;
	};
};

#endif // TEMPLATE_EX1 ///:~ 