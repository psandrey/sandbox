#include <stdio.h>
#include <string>
using namespace std;

namespace pre_post_ns {
	class CScopeMessage {
	public:
		CScopeMessage(const char* label) {
			PrintIndent();
			printf("%s\r\n", label);
			s_indentLevel++;
		}

		CScopeMessage(const char* label, int objectId) {
			PrintIndent();
			printf("%s obj %i\r\n", label, objectId);
			s_indentLevel++;
		}

		CScopeMessage(const char* label, int objectId, int copyObjectId) {
			PrintIndent();
			printf("%s (obj %i) to obj %i\r\n", label, objectId, copyObjectId);
			s_indentLevel++;
		}

		~CScopeMessage() { s_indentLevel--; }

		static void StartNewTest() {
			s_indentLevel = 0;
			s_lineNumber = 0;
			printf("\r\n");
		}

	private:
		void PrintIndent() {
			s_lineNumber++;
			printf("%2i:  ", s_lineNumber);
			for (int index = 0; index < s_indentLevel; ++index)
				printf("  ");
		}

	private:
		static int s_indentLevel;
		static int s_lineNumber;
	};

	int CScopeMessage::s_indentLevel = 0;
	int CScopeMessage::s_lineNumber = 0;

	class CTestClass {
	public:
		CTestClass() {
			m_objectID = s_objectID;
			s_objectID++;

			CScopeMessage msg("Constructing", m_objectID);

			// this is just noise in the test, but feel free to
			// comment out if you want to see for yourself
			//CScopeMessage msg("Constructing", m_objectID);
			m_value = new char[4];
			strcpy(m_value, "one");
		}

		CTestClass(const CTestClass& other) {
			m_objectID = s_objectID;
			s_objectID++;

			CScopeMessage msg("Copy Constructing", other.m_objectID, m_objectID);
			m_value = new char[strlen(other.m_value) + 1];
			strcpy(m_value, other.m_value);
		}

		~CTestClass() {
			CScopeMessage msg("Destroying", m_objectID);
			delete[] m_value;
		}

		// preincrement
		CTestClass& operator++() {
			CScopeMessage msg("Pre Increment", m_objectID);
			DoIncrement();
			return *this;
		}

		// postincrement
		CTestClass operator++(int) {
			CScopeMessage msg("Post Increment", m_objectID);
			CTestClass result(*this);
			DoIncrement();
			return result;
		}

		void DoIncrement() {
			CScopeMessage msg("Doing Increment", m_objectID);
		}

	private:
		char* m_value;
		int m_objectID;

		static int s_objectID;
	};

	int CTestClass::s_objectID = 0;
}

#if 0
using namespace pre_post_ns;
int main(int argc, char** argv)
{
	int i = 0;
	for (CTestClass testA; ; ++testA) {
		CTestClass testB = testA;

		i++;
		if (i == 2) break;
	}

#if 0
	CTestClass test;
	{
		CScopeMessage msg("--Post Increment--");
		test++;
	}

	CTestClass testA;
	CScopeMessage::StartNewTest();
	{
		CScopeMessage msg("--Post Increment Assign--");
		CTestClass testB = testA++;
	}

	CScopeMessage::StartNewTest();
	{
		CScopeMessage msg("--Pre Increment--");
		++test;
	}

	CScopeMessage::StartNewTest();
	{
		CScopeMessage msg("--Pre Increment Assign--");
		CTestClass testB = ++test;
	}

#endif
	return 0;
}
#endif