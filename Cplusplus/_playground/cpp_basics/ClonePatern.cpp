
#include <iostream>
using namespace std;

namespace clone_obj {
	// Depending upon your needs, you might not require a base class
	// clonable concept. It would only be needed if you need to store
	// clonable objects polymorphically.
	struct clonable1 {
		virtual clonable1* clone() const = 0;
		virtual ~clonable1() {}
	};

	class Base1 : public clonable1 {
	public:
		// covariant return type
		virtual Base1* clone() const {
			return new Base1(*this);
		}
	};

	class Derived : public Base1 {
	public:
		// covariant return type
		virtual Derived* clone() const {
			return new Derived(*this);
		}
	};

	// -- using conable and unique_ptr
	static int b_id = 0;
	static int d_id = 0;

	struct clonable2 {
		virtual unique_ptr<clonable2> clone() const = 0;
		virtual void is() {}
		virtual ~clonable2() {}
	};

	class Base2 : public clonable2 {
	private:
		int id;
	public:
		Base2() {
			id = b_id; b_id++;
			cout << "Ctor Base2: " << id << endl; }
		~Base2() { 
			cout << "Dtor Base2: " << id << endl; }
		Base2(const Base2& that) {
			id = b_id; b_id++;
			cout << "CpyCtor Base2: " << id << endl; }

		virtual void is() { cout << "Is Base2" << endl; }

		virtual unique_ptr<clonable2> clone() const {
			return make_unique<Base2>(Base2(*this));
		}
	};

	class Derived2 : public Base2 {
	private :
		int id;
	public:
		Derived2() { 
			id = d_id; d_id++;
			cout << "Ctor Derived2: " << id << endl; }
		~Derived2() { 
			cout << "Dtor Derived2: " << id << endl; }
		Derived2(const Derived2& that) : Base2(that) {
			id = d_id; d_id++;
			cout << "CpyCtor Derived2: " << id << endl;
		}

		virtual void is() { cout << "Is Derived2" << endl; }

		virtual unique_ptr<clonable2> clone() const {
			return make_unique<Derived2>(*this);
		}
	};
};

#if 0
using namespace clone_obj;
int main() {

#if 0
	unique_ptr<clonable2> p1 = make_unique<Derived2>();
	unique_ptr<clonable2> p2 = p1.get()->clone();

	cout << endl;

	p1.get()->is();
	p2.get()->is();

	cout << endl;

	unique_ptr<clonable2> p3 = make_unique<Base2>();

	cout << endl;

	p3.get()->is();

	cout << endl;

#endif

#if 0
	clone_obj::Base1* x = new clone_obj::Derived1();
	clone_obj::Base1* y = x->clone();
#endif

	return 0;
}
#endif