// mutex on read_modify_write(increment) to assure atomicity
// volatile on read to assure that we get the most recent version of counter

#include <vector>
#include <thread>
#include <mutex>
#include <string> 
#include <iostream>
using namespace std;

namespace vns {
	static mutex print_m;
	void print_locked(string s) {
		print_m.lock();

		cout << s << endl;

		print_m.unlock();
	}

	class SafeCounter {
	private:
		volatile int cnt = 0;
		mutex m;

	public:
		SafeCounter() : cnt(0) { }

		// volatile on read to assure that we get the most recent version of the scounter 
		int get() { return cnt; }

		// atomicity on write (read_modify_write)
		void inc(int thId) {
			m.lock();

			++cnt;
			print_locked("[Thread " + to_string(thId) + "]: Inc value = " + to_string(cnt));

			m.unlock();
		}
	};

	class CntThread {
	private:
		SafeCounter& cnt;
		int thId;

	public:
		CntThread(int thId, SafeCounter& c) : thId(thId), cnt(c) { };

		void f() {
			cnt.inc(thId);
			this_thread::sleep_for(chrono::milliseconds(2)); // this is to introduce some anarchy

			int c = cnt.get(); // read the last value of counter (last incremented)
			print_locked("[Thread " + to_string(thId) + "]: Get value = " + to_string(c));
		}
	};
};

#if 0
using namespace vns;

int main(void) {
	int n = 20;
	SafeCounter cnt;

	vector<thread> vth;
	for (int j = 0; j < n; j++) {
		CntThread* t = new CntThread(j, cnt);
		vth.push_back(thread(&CntThread::f, t));
	}

	for (int j = 0; j < n; j++)
		if(vth[j].joinable()) vth[j].join();

}
#endif