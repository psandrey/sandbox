
#include <iostream>
using namespace std;

extern int myadd(int x, int y);

class Intvec
{
public:
	explicit Intvec(size_t num = 0)
		: m_size(num), m_data(new int[m_size]) {
		log("constructor");
	}

	~Intvec() {
		log("destructor");

		if (m_data) {
			delete[] m_data;
			m_data = 0;
		}
	}

	Intvec(const Intvec& other)
		: m_size(other.m_size), m_data(new int[m_size])
	{
		log("copy constructor");
		for (size_t i = 0; i < m_size; ++i)
			m_data[i] = other.m_data[i];
	}
#if 0
	Intvec& operator=(const Intvec& other)
	{
		log("copy assignment operator");
		Intvec tmp(other);
		std::swap(m_size, tmp.m_size);
		std::swap(m_data, tmp.m_data);
		return *this;
	}
#endif
	Intvec& operator=(Intvec&& other) noexcept
	{
		log("move assignment operator");
		std::swap(m_size, other.m_size);
		std::swap(m_data, other.m_data);
		return *this;
	}

private:
	void log(const char* msg)
	{
		cout << "[" << this << "] " << msg << "\n";
	}

	size_t m_size;
	int* m_data;
};

class Test {
public:
	const int f() { return 1; }
	const char* g() { return "bla"; }
};

#if 0
int main()
{
	Test t;

	cout << myadd(1, 2) << " vv " << endl;
	//cout << ttt << " vv " << endl;

	int x = t.f();
	const char* y = t.g();

	cout << x << endl;
	cout << y << endl;
#if 0
	Intvec v1(20);
	Intvec v2;

	cout << "assigning lvalue...\n";
	v2 = move(v1);
	cout << "ended assigning lvalue...\n";
#endif
}
#endif