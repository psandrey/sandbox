
#include <vector>
#include <iostream>
using namespace std;

namespace vector_of_shared_ptr {
	class Test {
	public:
		Test() { cout << "ctor" << endl; }
		~Test() { cout << "dtor" << endl; }
	};
};

#if 0
using namespace vector_of_shared_ptr;
int main(void) {
	
	{
		shared_ptr<Test> p(new Test());
	}

	return 0;
	vector<shared_ptr<Test> > v;
	for (int i = 0; i < 3; i++) {
		shared_ptr<Test> p(new Test());
		v.push_back(move(p));
	}

	return 0;
}
#endif