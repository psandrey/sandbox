/*
	BOOST:ASIO client (multi-threaded/synchronous-blocking) example.
*/

#include <iomanip>
#include <vector>
#include <string>
#include <iostream>
#include <boost/thread/thread.hpp>
#include <boost/asio.hpp>

using namespace boost::asio;
using ip::tcp;
using std::string;
using std::to_string;
using std::cout;
using std::endl;

struct frame {
	uint32_t size;
	string dta;
};
typedef struct frame frame_type;

class cclient {
private:
	static int base_port_;
	int id_;
	string addr_;
	unsigned short port_;
	
	unsigned short srv_port_;
	ip::tcp::endpoint srv_ep_;

	io_service& ios_;

	static string msg_;
public:
	cclient(int id, string addr, unsigned short srv_port, io_service& ios) : id_(id),
		addr_(addr), srv_port_(srv_port), port_(base_port_ + id_),
		ios_(ios), srv_ep_(ip::address::from_string(addr_), srv_port_) { }

	// functor
	void operator()() {
		ip::tcp::endpoint c_ep(ip::address::from_string(addr_), port_ + 10	);
		
		boost::system::error_code ec;
		tcp::socket sock(ios_, c_ep.protocol());
		sock.bind(c_ep, ec);

		if (ec.value() != boost::system::errc::success) {
			std::cout << "Failed to bind the socket."
				<< "Error code = " << ec.value() << ". Message: "
				<< ec.message();

			return;
		}

		try { sock.connect(srv_ep_); }
		catch (std::exception& e) {
			std::cerr << e.what() << endl;
			return;
		}

		for (int i = 0; i < 10; i++) {
			frame_type frm;
			build_frame(frm, id_, i);

			boost::system::error_code error;

			// send frame size (header)
			uint32_t net_len = htonl(frm.size);
			write(sock, boost::asio::buffer(reinterpret_cast<char*>(&net_len), 4), error);
			if (error) cout << "Client:: send failed: " << error.message() << endl;

			// send frame size (header)
			write(sock, boost::asio::buffer(frm.dta), error);
			if (!error) cout << "Client:: sent: data!" << endl;
			else cout << "Client:: send failed: " << error.message() << endl;

			//boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
		}

		sock.shutdown(ip::tcp::socket::shutdown_both, ec);
		if (ec) cout << ec.message() << endl;

		sock.close();
	}

private:
	void build_frame(frame_type& frm, int id, int frame_id) {
		string sid = to_string(id_);
		string sframe_id = to_string(frame_id);
		frm.dta = msg_ + sframe_id + "/" + sid;
		frm.size = (uint32_t)frm.dta.size();
	}
};
int cclient::base_port_ = 1235;
string cclient::msg_ = "Message from client ";

int main() {
	string addr = "127.0.0.1";
	unsigned short srv_port = 1234;
	int n_threads = 1;

	io_service ios;
	std::vector<boost::thread> th(n_threads);
	for (int i = 0; i < n_threads; i++) {
		th[i] = boost::thread(cclient(i, addr, srv_port, ios));
		th[i].join();
	}

	cout << " Clients ended..." << endl;
	return 0;
}
