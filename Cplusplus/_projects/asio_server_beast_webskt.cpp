
/*
 boost::beast: Single threaded - echo server.
*/

#include <string>
#include <iostream>
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/dispatch.hpp>

using std::string;
using namespace boost::asio;
namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace http = beast::http;           // from <boost/beast/http.hpp>
namespace websocket = beast::websocket; // from <boost/beast/websocket.hpp>

using ip::tcp;
using std::cout;
using std::endl;

void fail(boost::system::error_code err, char const* what) {
	std::cerr << "fail: " << what << " ( " << err.message() << " ) " << endl;
}

class connection : public boost::enable_shared_from_this<connection> {
private :
	websocket::stream<beast::tcp_stream> ws_;
	beast::flat_buffer buffer_;

	//io_context::strand strand_;
public :
	typedef boost::shared_ptr<connection> ptr;
#if 0
	static ptr create(tcp::socket&& sock, io_context& ioc) { return ptr(new connection(std::move(sock), ioc); }
#else
	static ptr create(tcp::socket&& sock) { return ptr(new connection(std::move(sock))); }
#endif
	connection(tcp::socket&& sock) : ws_(std::move(sock)) { }

	void run() {
		ws_.set_option(websocket::stream_base::timeout::suggested(beast::role_type::server));

		// Accept the websocket handshake (we are sure this is going to happen)
		ws_.async_accept(
			beast::bind_front_handler(
				&connection::on_accept,
				shared_from_this()));

		cout << " + Wait for handshake..." << endl;
	}

private:

	void on_accept(beast::error_code err) {
		if (err) return fail(err, "accept");

			cout << " + Handshake accepted..." << endl;
		do_read();
	}

	void do_read() {
		ws_.async_read( buffer_,
			beast::bind_front_handler(
				&connection::on_read,
				shared_from_this()));
	}

	void on_read(beast::error_code ec, std::size_t bytes_transferred) {
		boost::ignore_unused(bytes_transferred);

		/* - beast::http::error::end_of_stream : successful read
		 * - beast::http::error::partial_message  : incomplete mesage
		 *
		 * - websocket::error::buffer_overflow
		 * - websocket::error::closed : connection closed
		 */
		if (ec == websocket::error::closed) {
			cout << " - Client closed connection! " << endl;
			return;
		}
		if (ec == beast::http::error::end_of_stream) cout << "message received ok" << endl;
		if (ec) return fail(ec, "on_read");

		cout << " + Received msg from client: " << beast::make_printable(buffer_.data()) << endl;

		ws_.async_write( buffer_.data(),
			beast::bind_front_handler(
				&connection::on_write,
				shared_from_this()));
	}

	void on_write(beast::error_code ec, std::size_t bytes_transferred) {
		boost::ignore_unused(bytes_transferred);

		if (ec) return fail(ec, "on_write");

		cout << " + Sent msg to client: " << beast::make_printable(buffer_.data()) << endl;

		buffer_.consume(buffer_.size());
		do_read();
	}
};

class server {
private:
	string addr_;
	unsigned short port_;

	io_context& ioc_;
	ip::tcp::endpoint ep_;
	tcp::acceptor acceptor_;

public:
	server(string addr, unsigned short port, io_context& ioc) : addr_(addr), port_(port), ioc_(ioc),
		acceptor_(ioc), ep_(ip::address::from_string(addr_), port){
	
		beast::error_code err;

		acceptor_.open(ep_.protocol(), err);
		if (err) { fail(err, "open"); return; }

		acceptor_.set_option(socket_base::reuse_address(true), err);
		if (err) { fail(err, "set_option"); return; }

		acceptor_.bind(ep_, err);
		if (err) { fail(err, "bind"); return; }

		acceptor_.listen(socket_base::max_listen_connections, err);
		if (err) { fail(err, "listen"); return; }
	}

	void run() { do_accept(); }

private :
	void do_accept() {
		// acceptor has a single argument: a callback that accepts an error code and a socket
		acceptor_.async_accept(
			//make_strand(ioc_),
			beast::bind_front_handler(&server::on_accept, this) );

		cout << endl << " # Server is waiting for connections ..." << endl << endl;
	}

	void on_accept(beast::error_code err, tcp::socket sock) {
		if (!err) {
			cout << endl << " # Server accepted new connection" << endl;

			connection::ptr conn = connection::create(std::move(sock));
			conn->run();

			//std::make_shared<connection>(std::move(sock))->run();
		}
		else fail(err, "on_accept");

		do_accept();
	}
};

int main(void) {
#if 1
	string addr = "127.0.0.1";
	int port = 1234;

	try {
		io_context ioc;

		server srv(addr, port, ioc);
		srv.run();

		ioc.run();
	} catch (std::exception& e) {
		std::cerr << e.what() << endl;
	}
#else
	string addr = "127.0.0.1";
	int port = 1234;
	int nthreads = 3;

	std::vector<std::thread> workers;
	workers.reserve(nthreads); 
	
	io_context ioc{ nthreads }; // hint the context that there are nthreads that may run concurently

	server srv(addr, port, ioc);
	srv.run();

	for (int i = 0; i < nthreads; i++) workers.emplace_back(
		[&ioc] {
			try { ioc.run(); }
			catch (std::exception& e) { std::cerr << e.what() << endl; }
		});
	for (auto& t : workers) t.join();

#endif

	return 0;
}