/*
 boost::beast: Single threaded - echo client.
*/

#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <cstdlib>
#include <iostream>
#include <string>

namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace http = beast::http;           // from <boost/beast/http.hpp>
namespace websocket = beast::websocket; // from <boost/beast/websocket.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>

using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>
using std::string;
using std::to_string;
using std::cout;
using std::endl;

void fail(beast::error_code ec, char const* what) {
	std::cerr << what << ": " << ec.message() << "\n";
}

class client {
private:
	string srv_addr_;
	unsigned short srv_port_;
	net::ip::tcp::endpoint srv_ep_;

	websocket::stream<beast::tcp_stream> ws_;

	net::io_context& ioc_;

	beast::flat_buffer buffer_;
	std::string text_ = "Hello!";

public:
	client(net::io_context& ioc, string srv_addr, unsigned short srv_port) : ioc_(ioc), ws_(ioc),
		srv_addr_(srv_addr), srv_port_(srv_port),
		srv_ep_(net::ip::address::from_string(srv_addr), srv_port) { }

	void run() { do_connect(); }

private:
	void do_connect() {
		tcp::resolver resolver{ ioc_ };
		tcp::resolver::results_type results = resolver.resolve(srv_ep_);
	
		// net::basic_socket (or something derived from it) to do the connection
		beast::get_lowest_layer(ws_).async_connect(
			results,
			beast::bind_front_handler(
				&client::on_connect,
				this));
	}

	void on_connect(beast::error_code ec, tcp::resolver::results_type::endpoint_type) {
		// Turn off the timeout on the tcp_stream, because
		// the websocket stream has its own timeout system.
		beast::get_lowest_layer(ws_).expires_never();
		
		cout << " + Connected to server." << endl;

		// Perform the websocket handshake
		ws_.async_handshake("localhost", "/",
			beast::bind_front_handler(
				&client::on_handshake,
				this));
	}

	void on_handshake(beast::error_code ec) {
		if (ec) return fail(ec, "on_handshake");

		cout << " + Handshake accepted by server." << endl;

		// Send the message
		ws_.async_write(
			net::buffer(text_),
			beast::bind_front_handler(
				&client::on_write,
				this));
	}

	void on_write( beast::error_code ec, std::size_t bytes_transferred) {
		boost::ignore_unused(bytes_transferred);

		if (ec) return fail(ec, "write");

		cout << " + Sent msg to server: " << text_ << endl;

		// Read a message into our buffer
		ws_.async_read(
			buffer_,
			beast::bind_front_handler(
				&client::on_read,
				this));
	}

	void on_read( beast::error_code ec, std::size_t bytes_transferred) {
		boost::ignore_unused(bytes_transferred);

		if (ec) return fail(ec, "read");

		cout << " + Received msg from server: " << beast::make_printable(buffer_.data()) << endl;

		// Close the WebSocket connection
		ws_.async_close(websocket::close_code::normal,
			beast::bind_front_handler(
				&client::on_close,
				this));
	}

	void on_close(beast::error_code ec) {
		if (ec) return fail(ec, "close");

		cout << " + Connection closed! " << endl;

		// The make_printable() function helps print a ConstBufferSequence
		std::cout << beast::make_printable(buffer_.data()) << std::endl;
	}
};

int main(void) {
	string addr = "127.0.0.1";
	unsigned short srv_port = 1234;

	net::io_context ioc;
	client client_conn(ioc, addr, srv_port);
	client_conn.run();
	ioc.run();

	return 0;
}