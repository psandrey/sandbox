/*
	BOOST:ASIO server example.
*/

#include <string>
#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/enable_shared_from_this.hpp>

using std::string;
using namespace boost::asio;
using ip::tcp;
using std::cout;
using std::endl;

struct frame {
	uint32_t size_payload;
	char* payload;
};
typedef struct frame frame_type;

enum class con_state {
	CON_INIT = 0,
	WAIT_FRM_HDR = 1,
	WAIT_FRM_PAYLOAD = 2,
	FRM_OK = 3,
	FRM_ERR = 4,
};

void fail(boost::system::error_code err, char const* what) {
	std::cerr << "fail: " << what << " ( " << err.message() << " ) " << endl;
}

// use/not the share_from_this/share_ptr idiom (for education purposes)
#define USE_SHARE_FROM_THIS

#if defined(USE_SHARE_FROM_THIS)
class con_handler : public boost::enable_shared_from_this<con_handler> {
#else
class con_handler {
#endif
private:
	int con_id_;

	ip::tcp::socket sock_;
	enum con_state con_state_;

public:
	con_handler(io_service& ios, int id) : sock_(ios), con_id_(id), con_state_(con_state::CON_INIT) { }
	~con_handler() { cout << " -- ( conn: " << con_id_ << " ) : dtor called." << endl; }

	// creating pointer to new connection
#if defined(USE_SHARE_FROM_THIS)
	typedef boost::shared_ptr<con_handler> ptr;
	static ptr create(io_service& ios, int id) { return ptr(new con_handler(ios, id)); }
#else
	typedef con_handler* ptr;
	static ptr create(io_service& ios, int id) { return new con_handler(ios, id); }
#endif

	tcp::socket& get_socket() { return sock_; }

	void start() { do_read_frame(); }
private :

	void do_read_frame() {
		frame_type* frm = new frame_type();
		frm->payload = NULL;


		// wait for header
		/* Note: we use shared_from_this because here we know the pointer *this*, but we do not know the others
				 shared_ptr, so by calling shared_from_this we share the ownership of *this*.
				 When no async operations are available, then the connection object is cleaned (delete pointer).

				 We are not obliged to use share_ptr/share_from_this, but if not used then the server must keep
				 a pointer to this connection and do the cleanup manualy.
				 Using the share_ptr/shared_from_this idiom the cleaning is done automatically.
		*/
#if defined(USE_SHARE_FROM_THIS)
		async_read(sock_, boost::asio::buffer(reinterpret_cast<char*>(&frm->size_payload), 4),
			transfer_exactly(4),
			boost::bind(&con_handler::on_read_hdr, shared_from_this(),
				placeholders::error, placeholders::bytes_transferred, frm));
#else
		async_read(sock_, boost::asio::buffer(reinterpret_cast<char*>(&frm->size_payload), 4),
			transfer_exactly(4),
			boost::bind(&con_handler::on_read_hdr, this,
				placeholders::error, placeholders::bytes_transferred, frm));
#endif
		con_state_ = con_state::WAIT_FRM_HDR;
	}

	void on_read_hdr(const boost::system::error_code& err, size_t bytes_transferred, frame_type* frm) {
		if (!err) {
			frm->size_payload = ntohl(frm->size_payload);
			frm->payload = new char[frm->size_payload];
			do_read_payload(frm);
		}
		else handle_con_err(err, frm);
	}

	void do_read_payload(frame_type* frm) {
		// wait for payload
#if defined(USE_SHARE_FROM_THIS)
		async_read(sock_, boost::asio::buffer(frm->payload, frm->size_payload),
			transfer_exactly(frm->size_payload),
			boost::bind(&con_handler::on_read_payload, shared_from_this(),
				placeholders::error, placeholders::bytes_transferred, frm));
#else
		async_read(sock_, boost::asio::buffer(frm->payload, frm->size_payload),
			transfer_exactly(frm->size_payload),
			boost::bind(&con_handler::on_read_payload, this,
				placeholders::error, placeholders::bytes_transferred, frm));
#endif

		con_state_ = con_state::WAIT_FRM_PAYLOAD;
	}

	void on_read_payload(const boost::system::error_code& err, size_t bytes_transferred, frame_type* frm) {
		if (!err) {
			cout << "( conn: " << con_id_ << " ) : Frame OK :";
			for (int i = 0; i < (int)frm->size_payload; i++) cout << frm->payload[i];
			cout << endl;
			
			delete frm->payload;
			delete frm;
			do_read_frame();
		} else handle_con_err(err, frm);
	}

	void handle_con_err(const boost::system::error_code& err, frame_type* frm) {
		if (frm->payload != NULL) delete frm->payload;
		if (frm != NULL) delete frm;

		if (err) fail(err, "connection");

		switch (err.value()) {
		case error::eof:
			cout << " ## ( conn: " << con_id_ << " ) : closed" << endl;
			sock_.close();

#if !defined(USE_SHARE_FROM_THIS)
			this->~con_handler();
#endif
			break;
		default:
			// wait for another frame
			do_read_frame();
		}
	}
};

class Server {
private:
	string addr_;
	unsigned short port_;

	io_service& ios_;
	ip::tcp::endpoint ep_;
	ip::tcp::acceptor acceptor_;

	int con_cnt;

public:
	Server(string addr, unsigned short port, io_service& ios) :
		addr_(addr), port_(port), ios_(ios),
		ep_(ip::address::from_string(addr_), port), acceptor_(ios), con_cnt(0) {
		boost::system::error_code err;
		
		acceptor_.open(ep_.protocol(), err);
		if (err) { fail(err, "open"); return; }

		acceptor_.set_option(socket_base::reuse_address(true), err);
		if (err) { fail(err, "set_option"); return; }

		acceptor_.bind(ep_, err);
		if (err) { fail(err, "bind"); return; }

		acceptor_.listen(socket_base::max_listen_connections, err);
		if (err) { fail(err, "listen"); return; }
	}

	void run() { do_accept(); }

private :
	void do_accept() {
		// prepare new connection
		con_cnt++;
		con_handler::ptr new_con = con_handler::create(ios_, con_cnt); // new_con is a shared_ptr

		// asynchronous accept new connection
		acceptor_.async_accept(new_con->get_socket(),
			boost::bind(&Server::on_accept, this, new_con, placeholders::error));
	}

	void on_accept(con_handler::ptr client_con, const boost::system::error_code& err) {
		/* In the client connection we need to use shared_from_this when we pass *this* pointer
		   otherwise when on_accept's scope is ending the new_con is freed and we need to keep connection
		   alive as long as the other endpoint (the client) is connected.

		   Note: If share_ptr is not used, then when connection is closed we need to call the connection dtor
				manually, and delete the pointer (since it is dinamically created).
				However, by using share_ptr/shared_from_this idiom this problem is solved automatically.
		*/
		if (!err) client_con->start();
		else fail(err, "async_accept");

		// accept another connection
		do_accept();
	}
};

int main(int argc, char* argv[]) {
	string addr = "127.0.0.1";
	int port = 1234;

	try {
		io_service ios;
		Server server(addr, port, ios);
		server.run();
		ios.run();
	} catch (std::exception& e) {
		std::cerr << e.what() << endl;
	}

	return 0;
}
