
#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class JourneyToTheMoon {
public:
	int dfind(int u, vector<int>& p) {
		if (p[u] != u) p[u] = dfind(p[u], p);
		return p[u];
	}

	void dunion(int u, int v, vector<int>& p, vector<int>& r, vector<int>& c) {
		int idu = dfind(u, p), idv = dfind(v, p);
		if (idu != idv) {
			if (r[idu] > r[idv]) { p[idv] = idu; c[idu] += c[idv]; }
			else if (r[idv] > r[idu]) { p[idu] = idv; c[idv] += c[idu]; }
			else { r[idu]++; p[idv] = idu; c[idu] += c[idv]; }
		}
	}

	long long journeyToMoon(int n, vector<vector<int>>& e) {
		vector<int> r(n, 0), c(n, 1), p(n); for (int i = 0; i < n; i++) p[i] = i;
		int m = (int)e.size();

		for (int i = 0; i < m; i++) {
			int u = e[i][0], v = e[i][1];

			if (dfind(u, p) != dfind(v, p)) dunion(u, v, p, r, c);
		}

		long long ans = -1, s = 0;
		vector<bool> counted(n, false);
		for (int i = 0; i < n; i++) {
			int id = dfind(i, p);
			if (counted[id] == true) continue;

			if (ans == -1) { ans = 0; s = c[id]; }
			else {
				ans += s * (long long)c[id];
				s += (long long)c[id];
			}
			counted[id] = true;
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<vector<int>> e = { {0, 2} };
	int n = 4;
	
	JourneyToTheMoon t;
	cout << t.journeyToMoon(n, e) << endl;

	return 0;
}
#endif