
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class LilysHomework {
public:
	int get_swps_asc(vector<pair<int, int>>& a) {
		int n = (int)a.size();
		int sw = 0;
		for (int i = 0; i < n; i++) {
			if (a[i].first <= 0) continue;
			if (a[i].second == i) { a[i].first *= -1;  continue; };

			a[i].first *= -1;
			int j = a[i].second;
			while (j != i) {
				sw++;
				a[j].first *= -1;
				j = a[j].second;
			}
		}

		return sw;
	}

	int get_swps_dsc(vector<pair<int, int>>& a) {
		int n = (int)a.size();
		int sw = 0;
		for (int i = 0; i < n; i++) {
			if (a[i].first >= 0) continue;
			if (a[i].second == i) { a[i].first *= -1;  continue; }

			a[i].first *= -1;
			int j = a[i].second;
			while (j != i) {
				sw++;
				a[j].first *= -1;
				j = a[j].second;
			}
		}

		return sw;
	}

	int lilysHomework(vector<int>& arr) {
		vector<pair<int, int>> a(arr.size());
		for (int i = 0; i < (int)a.size(); i++)
			a[i] = make_pair(arr[i], i);

		sort(a.begin(), a.end());
		int sw1 = get_swps_asc(a);
		
		sort(a.begin(), a.end());
		int sw2 = get_swps_dsc(a);

		return min(sw1, sw2);
	}
};

#if 0
int main() {
	//vector<int> a = { 7, 15, 12, 3 };
	//vector<int> a = { 3, 4, 2, 5, 1 };
	//vector<int> a = { 3, 2, 1 };

	LilysHomework t;
	cout << t.lilysHomework(a) << endl;

	return 0;
}
#endif