
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class FlippingTheMatrix {
public:
	int flippingMatrix(vector<vector<int>>& a) {
		int m = (int)a.size();

		int ans = 0;
		for (int i = 0; i < m / 2; i++) {
			for (int j = 0; j < m / 2; j++) {
				int l1 = max(a[i][j], a[i][m - 1 - j]);
				int l2 = max(a[m - 1 - i][j], a[m - 1 - i][m - 1 - j]);

				ans += max(l1, l2);
			}
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<vector<int>> a = { {112, 42, 83, 119},
							{56, 125, 56, 49},
							{15, 78, 101, 43},
							{62, 98, 114, 108} };

	FlippingTheMatrix t;
	cout << t.flippingMatrix(a) << endl;

	return 0;
}
#endif