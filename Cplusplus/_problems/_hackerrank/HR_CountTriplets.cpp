
#include <algorithm>
#include <unordered_map>
#include <iostream>
#include <numeric>
using namespace std;

class CountTriplets {
public:
#if 0
	// this version does not pass 2/13 tests
	long countTriplets(vector<long>& a, long r) {
		int n = (int)a.size();

		unordered_map<double, long> hm;
		long ans = 0L;
		for (int j = 0; j < n; j++) {

			double k1 = a[j] / r;
			if (hm.find(k1) != hm.end()) {
				if (a[j] == k1) {
					if (hm[k1] >= 2) {
						long long m = ((long long)hm[k1] * (hm[k1] - 1)) / 2LL;
						ans += (long)m;
					}
				} else {
					double k2 = k1 / r;
					if (hm.find(k2) != hm.end())
						ans += hm[k1] * hm[k2];
				}
			}

			if (hm.find(a[j]) == hm.end()) hm[a[j]] = 1;
			else hm[a[j]]++;
		}

		return ans;
	}
#else
	long countTriplets(vector<long>& a, double r) {

		long ans = 0;
		int n = (int)a.size();
		unordered_map<double, long> b1, b2, b3;
		for (int j = 0; j < n; j++) {

			double k = (double)a[j] / r;
			if (b2.find(k) != b2.end()) { b3[a[j]] += b2[k]; ans += b2[k]; }
			if (b1.find(k) != b1.end()) b2[a[j]] += b1[k];
			b1[a[j]]++;
		}

		return ans;
	}
#endif
};

#if 0
int main(void) {
	vector<long> a = { 1, 1, 1, 1 };
	long r = 1;
	
	CountTriplets t;
	int ans = t.countTriplets(a, r);
	cout << ans << endl;

	return 0;
}
#endif