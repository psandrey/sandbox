
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class BigSorting {
public:
	vector<string> bigSorting(vector<string>& unsorted) {
		struct BigNComp {
			bool operator() (const string& a, const string& b) {
				if (a.size() == b.size())
					return a < b;

				return (a.size() < b.size());
			}
		};

		sort(unsorted.begin(), unsorted.end(), BigNComp());

		return unsorted;
	}
};

#if 0
int main() {
	vector<string> a = {
		"6",
		"31415926535897932384626433832795",
		"1",
		"3",
		"10",
		"3",
		"5" };

	BigSorting t;
	vector<string> ans = t.bigSorting(a);
	for (string x : ans)
		cout << x << " ";
	cout << endl;

	return 0;
}
#endif