
#include <algorithm>
#include <unordered_set>
#include <iostream>
#include <numeric>
using namespace std;

class TripleSum {
public:
	long long triplets(vector<int>& a, vector<int>& b, vector<int>& c) {
		long long ans = 0LL;

		// eliminate duplicates
		unordered_set<int> hs;
		for (int i = 0; i < (int)a.size(); i++) hs.insert(a[i]);
		a.assign(hs.begin(), hs.end());
		hs.clear();

		for (int i = 0; i < (int)b.size(); i++) hs.insert(b[i]);
		b.assign(hs.begin(), hs.end());
		hs.clear();

		for (int i = 0; i < (int)c.size(); i++) hs.insert(c[i]);
		c.assign(hs.begin(), hs.end());

		// sort
		sort(a.begin(), a.end());
		sort(b.begin(), b.end());
		sort(c.begin(), c.end());

		int i = (int)a.size() - 1, j = (int)c.size() - 1, k = (int)b.size() - 1;
		while (k >= 0) {
			while (i >= 0 && a[i] > b[k]) i--;
			while (j >= 0 && c[j] > b[k]) j--;
			ans += (long long)(i + 1) * (long long)(j + 1);
			k--;
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = {1, 3, 5};
	vector<int> b = {2, 3};
	vector<int> c = { 1, 2, 3 };
	
	TripleSum t;
	cout << t.triplets(a, b, c) << endl;
	
	return 0;
}
#endif