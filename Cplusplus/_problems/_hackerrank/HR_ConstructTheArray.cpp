
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class ConstructTheArray {
public:
	long countArray(int n, int k, int x) {
		long end_1 = 0, end_x = 1; // len = 1
		long m = 1000000007;
		for (int i = 2; i < n; i++) { // lengths from 2 to n
			long iend_1 = (end_x * (k - 1)) % m;
			long iend_x = ((end_1 + end_x * (k - 1)) - end_x) % m;

			end_1 = iend_1; end_x = iend_x;
		}

		return (x == 1 ? end_1 : end_x);
	}
};

#if 0
int main(void) {
	int n = 5, k = 4, x = 2;

	ConstructTheArray t;
	cout << t.countArray(n, k, x) << endl;

	return 1;
}
#endif