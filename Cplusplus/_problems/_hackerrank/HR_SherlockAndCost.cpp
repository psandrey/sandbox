#include <string>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class SherlockAndCost {
public:
	int cost(vector<int>& b) {
		int n = (int)b.size();
		vector<int> dp1(n), dp(n);

		dp1[0] = 0; dp[0] = 0;
		for (int k = 1; k < n; k++) {
			dp1[k] = max(dp1[k - 1], abs(1 - b[k - 1]) + dp[k - 1]);
			dp[k] = max(b[k] - 1 + dp1[k - 1], abs(b[k] - b[k - 1]) + dp[k - 1]);
		}
		
		return max(dp1[n - 1], dp[n - 1]);
	}
};

#if 0
int main(void) {

	//vector<int> b = { 4, 7, 9 }; // 12
	//vector<int> b = { 9, 2, 3, 9 }; // 16
	vector<int> b = { 2, 7, 9, 9 }; // 20
	
	SherlockAndCost t;
	cout << t.cost(b) << endl;
	return 0;
}
#endif