
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class MakingCandies {
public:
	typedef unsigned long long ll;
	ll buy(ll& goods, ll& candies, ll m, ll w, ll p) {
		ll steps = 0;
		if (p <= m * w + candies) {
			ll produced = (m * w + candies);
			goods = produced / p;
			candies = produced % p;
			
			steps = 1;
		} else {
			goods = 1;
			steps = (ll)ceil((double)(p - candies) / (double)(m * w));
			candies = steps * m * w - (p - candies);
		}

		return steps;
	}

	void distribute(ll& m, ll& w, ll goods) {
		if (goods == 0) return;
		if (m == w) {
			ll half = goods / 2;
			m += half;
			w += (goods - half);
			return;
		}
		
		if (m < w) {
			ll d = w - m;
			m += min(d, goods);
			goods -= min(d, goods);

			distribute(m, w, goods);
		} else {
			ll d = m - w;
			w += min(d, goods);
			goods -= min(d, goods);

			distribute(m, w, goods);
		}
	}

	ll minimumPasses(ll M, ll W, ll P, ll N) {
		// this is what I call madness
		if (M >= 4294967296ULL && W >= 4294967296ULL) return 1;
		ll m = M, w = W, p = P;
		
		ll ans = LLONG_MAX;
		ll target_candies = N; //  the candies needed to be produced
		ll steps = 0ULL, candies = 0ULL; // steps already made and candies we have
		do {
			// how many steps needed until all candies are produced with current m and w
			ll c_steps = steps + (ll)ceil((double)(target_candies - candies) / (m * w));
			if (c_steps > ans) break;

			ans = min(ans, c_steps);

			// buy machines/workers (goods)
			ll goods = 0;
			steps += buy(goods, candies, m, w, p); // add the steps needed to aquire the goods

			// distribute the goods (machine & workers)
			distribute(m, w, goods);
		} while (true);

		return (long)ans;
	}
};

// 5/50 test cases are failing.... freeks
#if 0
int main() {
	typedef unsigned long long ll;

	long m = 1, w = 1, p = 6, n = 45;
	//long m = 3, w = 1, p = 2, n = 12;
	//ll m = 123456789012, w = 215987654321, p = 50000000000, n = 1000000000000;
	//ll m = 3, w = 13, p = 13, n = 1000000000000LL;
	//ll m = 4294967296ULL, w = 4294967296ULL, p = 10ULL, n = 10000ULL;
	//ll m = 4294967297ULL, w = 4294967297ULL, p = 1000000000000ULL, n = 1000000000000ULL;
	//ll m = 1, w = 1, p = 1000000000000ULL, n = 1000000000000ULL;

#if 0
	{ 5, 2, 10302, 9133131738L },
	{ 5361, 3918, 8_447_708L, 989_936_375_520L },
	{ 1, 3, 92, 373 },
	{ 3, 8, 71, 487 },
	{ 5, 1, 172, 364 },
	{ 3, 1, 2, 12 },
	{ 1, 2, 1, 60 },
	{ 1, 1, 6, 45 },
#endif

	MakingCandies t;
	cout << t.minimumPasses(m, w, p, n) << endl;

	return 0;
}
#endif