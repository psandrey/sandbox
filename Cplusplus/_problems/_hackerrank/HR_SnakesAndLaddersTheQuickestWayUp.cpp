
#include <algorithm>
#include <string>
#include <queue>
#include <unordered_map>
#include <vector>
#include <iostream>
using namespace std;

class SnakesAndLaddersTheQuickestWayUp {
public:
	int quickestWayUp(vector<vector<int>> ladders, vector<vector<int>> snakes) {

		unordered_map<int, int> lmp;
		for (int i = 0; i < (int)ladders.size(); i++)
			lmp[ladders[i][0]] = ladders[i][1];

		unordered_map<int, int> smp;
		for (int i = 0; i < (int)snakes.size(); i++)
			smp[snakes[i][0]] = snakes[i][1];

		vector<bool> marked(101, false);
		vector<int> d(101, -1);
		queue<int> q;
		q.push(1); marked[1] = true; d[1] = 0;
		while (!q.empty()) {
			int u = q.front(); q.pop();
			if (u == 100) break;

			for (int i = 1; i <= 6; i++) {
				int v = u + i;
				if (v > 100 || marked[v] == true) continue;
				marked[v] = true;

				if (smp.find(v) != smp.end()) v = smp[v];
				else if (lmp.find(v) != lmp.end()) v = lmp[v];

				q.push(v);
				marked[v] = true;
				d[v] = d[u] + 1;
			}
		}

		return d[100];
	}

};

#if 0
int main(void) {
	vector<vector<int>> ladders = { {32, 62}, {42, 68}, {12, 98} };
	vector<vector<int>> snakes = { {95, 13}, {97, 25}, {93, 37}, {79, 27}, {75, 19}, {49, 47}, {67, 17} };

	SnakesAndLaddersTheQuickestWayUp t;
	cout << t.quickestWayUp(ladders, snakes) << endl;
	
	return 0;
}
#endif