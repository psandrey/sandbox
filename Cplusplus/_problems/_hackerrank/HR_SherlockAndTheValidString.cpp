
#include <algorithm>
#include <unordered_map>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
using namespace std;

class SherlockAndTheValidString {
public:
	string isValid(string s) {

		vector<int> fq(26, 0);
		for (int i = 0; i < (int)s.size(); i++) {
			fq[s[i] - 'a']++;
		}

		int n1 = -1, n2 = -1, k1 = 0, k2 = 0;
		for (int i = 0; i < 26; i++) {
			if (fq[i] == 0) continue;

			if (fq[i] == n1) k1++;
			else if (fq[i] == n2) k2++;
			else {
				if (n1 == -1) { n1 = fq[i]; k1 = 1; }
				else if (n2 == -1) {n2 = fq[i]; k2 = 1;}
				else return "NO";
			}
		}

		if (n1 != -1 && n2 != -1) {
			if (k1 == 1 && n1 - n2 == 1) return "YES";
			if (k2 == 1 && n2 - n1 == 1) return "YES";

			if (k1 == 1 && n1 == 1) return "YES";
			if (k2 == 1 && n2 == 1) return "YES";
		}

		return "NO";
	}

	void from_file() {
		ifstream myfile("d:\\temp\\in.txt");


		string s;
		getline(myfile, s);

		string result = isValid(s);

		cout << result << "\n";
	}
};

#if 0
int main(void) {
	string s = "aaaaabc";

	SherlockAndTheValidString t;
	t.from_file();
	//cout << t.isValid(s) << endl;

	return 0;
}
#endif