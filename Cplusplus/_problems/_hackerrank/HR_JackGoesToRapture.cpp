
#include <queue>
#include <unordered_map>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class JackGoesToRapture {
public:
	// v3 passes all tests

	void getCost(int n, vector<int> from, vector<int> to, vector<int> weight) {
		// Print your answer within the function and return nothing

		vector<vector<int>> p(n, vector<int>(n, INT_MAX));
		for (int i = 0; i < n; i++)p[i][i] = 0;
		for (int i = 0; i < (int)weight.size(); i++) {
			int u = from[i] - 1, v = to[i] - 1, w = weight[i];
			p[u][v] = w; p[v][u] = w;
		}

		print_matrix(p);

		for (int k = 0; k < n; k++) {
			for (int u = 0; u < n; u++) {
				for (int v = 0; v < n; v++) {
					int w = max(p[u][k], p[k][v]);
					p[u][v] = min(p[u][v], w);
				}
			}
		}

		cout << p[0][n - 1] << endl;
	}

	int get_min(vector<int>& p, vector<bool>& marked) {
		int u = -1, w = INT_MAX;
		for (int v = 0; v < (int)p.size(); v++) {
			if (marked[v] == true) continue;

			if (w > p[v]) { u = v; w = p[v]; }
		}

		return u;
	}

	void getCost_v2(int n, vector<int>& from, vector<int>& to, vector<int>& weight) {
		// Print your answer within the function and return nothing
		int m = (int)weight.size();
		unordered_map<int, vector<pair<int, int>>> adjL;
		for (int i = 0; i < m; i++) {
			int u = from[i] - 1, v = to[i] - 1, w = weight[i];
			if (adjL.find(u) == adjL.end()) adjL[u] = vector<pair<int, int>>();
			if (adjL.find(v) == adjL.end()) adjL[v] = vector<pair<int, int>>();
			adjL[u].push_back(make_pair(v, w));
			adjL[v].push_back(make_pair(u, w));
		}

		vector<int> p(n, INT_MAX);
		vector<bool> marked(n, false);
		p[0] = 0;
		for (int i = 0; i < n; i++) {
			int u = get_min(p, marked);
			if (u == -1) break;

			marked[u] = true;
			for (pair<int, int> pair : adjL[u]) {
				int v = pair.first, w = pair.second;
				if (marked[v] == true) continue;
				
				if (p[v] > max(p[u], w)) p[v] = max(p[u], w);
			}
		}

		if (p[n - 1] == INT_MAX) cout << "NO PATH EXISTS" << endl;
		else cout << p[n - 1] << endl;
	}

	void getCost_v3(int n, vector<int>& from, vector<int>& to, vector<int>& weight) {
		// Print your answer within the function and return nothing
		int m = (int)weight.size();
		unordered_map<int, vector<pair<int, int>>> adjL;
		for (int i = 0; i < m; i++) {
			int u = from[i] - 1, v = to[i] - 1, w = weight[i];
			if (adjL.find(u) == adjL.end()) adjL[u] = vector<pair<int, int>>();
			if (adjL.find(v) == adjL.end()) adjL[v] = vector<pair<int, int>>();
			adjL[u].push_back(make_pair(v, w));
			adjL[v].push_back(make_pair(u, w));
		}

		vector<int> p(n, INT_MAX); p[0] = 0;
		vector<bool> marked(n, false);
		struct compare {
			bool operator()(const pair<int, int>& a, const pair<int, int>& b) {
				return (a.second > b.second);
			}
		};
		priority_queue<pair<int, int>, vector<pair<int, int>>, compare> q;
		
		q.push(make_pair(0, p[0]));
		while (!q.empty()) {
			pair<int, int> qp = q.top(); q.pop();
			int u = qp.first;

			if (marked[u] == true) continue;
			marked[u] = true;

			for (pair<int, int> pair : adjL[u]) {
				int v = pair.first, w = pair.second;
				if (marked[v] == true) continue;

				if (p[v] > max(p[u], w)) {
					p[v] = max(p[u], w);
					q.push(make_pair(v, p[v]));
				}
			}
		}

		if (p[n - 1] == INT_MAX) cout << "NO PATH EXISTS" << endl;
		else cout << p[n - 1] << endl;
	}

	void getCost_v4(int n, vector<int>& from, vector<int>& to, vector<int>& weight) {
		// Print your answer within the function and return nothing
		int m = (int)weight.size();
		vector<int> d(n, INT_MAX); d[0] = 0;
		for (int k = 0; k < n; k++) {
			for (int i = 0; i < m; i++) {
				int u = from[i] - 1, v = to[i] - 1, w = weight[i];
				d[u] = min(d[u], max(d[v], w));
				d[v] = min(d[v], max(d[u], w));
			}
		}

		if (d[n - 1] == INT_MAX) cout << "NO PATH EXISTS" << endl;
		else cout << d[n - 1] << endl;
	}

	void print_matrix(vector<vector<int>>& a) {
		for (int u = 0; u < (int)a.size(); u++) {
			for (int v = 0; v < (int)a[0].size(); v++) {
				if (a[u][v] == INT_MAX) cout << "- ";
				else cout << a[u][v] << " ";
			}
			cout << endl;
		}
		cout << endl;
	}
};

#if 0
int main(void) {
	vector<int> from = { 1, 3, 1, 4, 2 };
	vector<int> to = { 2, 5, 4, 5, 3 };
	vector<int> weight = { 60, 70, 120, 150, 80};
	int n = 5;

	JackGoesToRapture t;
	t.getCost(n, from, to, weight);
	t.getCost_v2(n, from, to, weight);
	t.getCost_v3(n, from, to, weight);
	t.getCost_v4(n, from, to, weight);

	return 0;
}
#endif