
#include <algorithm>
#include <string>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <iostream>
using namespace std;

class JeanieRoute {
public:
	void DFS(int u, long long dist, vector<bool>& marked, unordered_map<int, vector<pair<int, int>>>& adjL,
		vector<int>& degree, unordered_set<int>& mask, int& node, long long& max_dist) {
		marked[u] = true;

		for (pair<int, int> p : adjL[u]) {
			int v = p.first, w = p.second;
			if (mask.find(v) != mask.end() || marked[v] == true) continue;

			DFS(v, dist + w, marked, adjL, degree, mask, node, max_dist);
		}

		if (max_dist < dist) { node = u; max_dist = dist; }
	}

	int jeanisRoute(int n, vector<int> k, vector<vector<int>> roads) {

		unordered_set<int> kset;
		for (int i = 0; i < (int)k.size(); i++) kset.insert(k[i]);

		vector<int> degree(n + 1, 0);
		unordered_map<int, vector<pair<int, int>>> adjL;
		for (int i = 0; i < (int)roads.size(); i++) {
			int u = roads[i][0], v = roads[i][1], w = roads[i][2];

			adjL[u].push_back(make_pair(v, w));
			adjL[v].push_back(make_pair(u, w));
			degree[u]++; degree[v]++;
		}

		queue<int> q;
		for (int u = 1; u <= n; u++)
			if (degree[u] == 1 && kset.find(u) == kset.end())
				q.push(u);

		unordered_set<int> mask;
		while (!q.empty()) {
			int u = q.front(); q.pop(); degree[u]--;
			mask.insert(u);

			for (pair<int, int> p : adjL[u]) {
				int v = p.first, w = p.second;
				degree[v]--;
				if (degree[v] == 1 && kset.find(v) == kset.end()) q.push(v);
			}
		}

		long long T = 0LL;
		for (int i = 0; i < (int)roads.size(); i++) {
			int u = roads[i][0], v = roads[i][1], w = roads[i][2];

			if (mask.find(u) == mask.end() && mask.find(v) == mask.end()) T += (long long)w;
		}

		int node = -1;
		long long max_dist = 0LL;
		vector<bool> marked(n + 1, false);
		DFS(k[0], 0, marked, adjL, degree, mask, node, max_dist);
		fill(marked.begin(), marked.end(), false);
		DFS(node, 0, marked, adjL, degree, mask, node, max_dist);

		return (int)(2LL * T - max_dist);
	}
};

#if 0
int main(void) {
	int n = 5;
	vector<int> k = { 1, 3, 4 };
	vector<vector<int>> roads = { {1, 2, 1}, {2, 3, 2}, {2, 4, 2}, {3, 5, 3} };
	
	JeanieRoute t;
	cout << t.jeanisRoute(n, k, roads) << endl;

	return 0;
}
#endif