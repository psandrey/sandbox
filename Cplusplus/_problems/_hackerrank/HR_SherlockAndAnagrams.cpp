
#include <algorithm>
#include <string>
#include <unordered_map>
#include <iostream>
using namespace std;

class SherlockAndAnagrams {
public:
	int sherlockAndAnagrams(string a) {
		unordered_map<string, int> hm;
		int sz = (int)a.size();
		int ans = 0;
		for (int len = 1; len <= sz - 1; len++) {
			for (int i = 0; i <= sz - len; i++) {
				string key = a.substr(i, len);
				sort(key.begin(), key.end());

				if (hm.find(key) != hm.end()) {
					ans += hm[key];
					hm[key]++;
				} else hm[key] = 1;
			}
		}

		return ans;
	}
};

#if 0
int main(void) {
	string a = "ifailuhkqq";
	SherlockAndAnagrams t;
	int ans = t.sherlockAndAnagrams(a);
	cout << ans << endl;
}
#endif