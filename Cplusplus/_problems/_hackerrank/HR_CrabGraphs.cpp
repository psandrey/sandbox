
#include <algorithm>
#include <string>
#include <queue>
#include <unordered_map>
#include <vector>
#include <iostream>
using namespace std;

class CrabGraphs {
public:

	bool find_path(int s, int t, vector<vector<int>>& adjM, unordered_map<int, int>& p) {
		int n = (int)adjM.size();
		vector<bool> marked(n, false);
		queue<int> q;
		q.push(s); marked[s] = true;
		while (!q.empty()) {
			int u = q.front(); q.pop();
			if (u == t) return true;

			for (int v = 0; v < (int)adjM[u].size(); v++) {
				if (adjM[u][v] == 0 || marked[v] == true) continue;

				q.push(v); marked[v] = true;
				p[v] = u;
			}
		}

		return false;
	}

	void augment_flow(int s, int t, vector<vector<int>>& adjM, unordered_map<int, int>& p) {
		int v = t;

		while (v != s) {
			int u = p[v];
			adjM[u][v]--; // direct edge
			adjM[v][u]++; // back edge

			v = u;
		}
	}

	int crabGraphs(int n, int t, vector<vector<int>>& e) {
		int sz = 2 * (n + 1) + 1;
		vector<vector<int>> adjM(sz, vector<int>(sz, 0));
		for (int i = 0; i < (int)e.size(); i++) {
			int u = e[i][0], v = e[i][1];

			adjM[2 * u][2 * v + 1] = 1;
			adjM[2 * v][2 * u + 1] = 1;

			if (adjM[1][2 * u] < t) adjM[1][2 * u]++; // source
			if (adjM[1][2 * v] < t) adjM[1][2 * v]++; // source
		}
		for (int i = 1; i <= n; i++) adjM[2 * i + 1][sz - 1] = 1; // sink 

		int crabs = 0;
		unordered_map<int, int> p;
		while (find_path(1, sz - 1, adjM, p)) {
			crabs++;

			augment_flow(1, sz - 1, adjM, p);
			p.clear();
		}

		return crabs;
	}
};

#if 0
int main(void) {
	//vector<vector<int>> e = { {1, 2}, {2, 3}, {3, 4}, {4, 5}, {5, 6}, {6, 1}, {1, 4}, {2, 5} };
	//int n = 6, t = 3;

	vector<vector<int>> e = { {20, 19}, { 36, 27 }, { 25, 4 }, { 5, 1 }, { 5, 9 }, { 1, 23 }, { 1, 35 }};
	int n = 50, t = 2;
	
	CrabGraphs T;
	int ans = T.crabGraphs(n, t, e);
	cout << ans << endl;

	return 0;
}
#endif