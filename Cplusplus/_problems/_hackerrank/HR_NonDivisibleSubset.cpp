
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class NonDivisibleSubset {
public:
	int nonDivisibleSubset(int k, vector<int>& a) {
		vector<int> b(k, 0);
		for (int i = 0; i < (int)a.size(); i++) b[a[i] % k]++;

		int ans = (b[0] > 0 ? 1 : 0), j = 0;
		for (int i = 1; i < (k + 1) / 2; i++) {
			int m = max(b[i], b[k - i]);
			ans = ans + m;
		}
		if (k % 2 == 0 && b[k / 2] > 0) ans++;

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = { 1, 2, 3, 4, 5 };
	int k = 1;

	NonDivisibleSubset t;
	cout << t.nonDivisibleSubset(k, a) << endl;

	return 0;
}
#endif