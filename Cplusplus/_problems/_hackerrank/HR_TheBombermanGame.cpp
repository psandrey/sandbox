
#include <algorithm>
#include <string>
#include <queue>
#include <unordered_map>
#include <vector>
#include <iostream>
using namespace std;

class TheBombermanGame {
public:
	vector<string> neg_grid(vector<string>& g) {
		int n = (int)g.size(), m = (int)g[0].size();
		vector<string> ng(n, string(m, 'O'));

		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++) {
				if (g[i][j] == 'O') {
					if (j - 1 >= 0) ng[i][j - 1] = '.';
					if (j + 1 < m)  ng[i][j + 1] = '.';
					if (i - 1 >= 0) ng[i - 1][j] = '.';
					if (i + 1 < n)  ng[i + 1][j] = '.';
					ng[i][j] = '.';
				}
			}

		return ng;
	}

	vector<string> bomberMan(int n, vector<string>& g) {
		if (n <= 1) return g;
		if (n % 2 == 0) {
			int n = (int)g.size(), m = (int)g[0].size();
			return vector<string>(n, string(m, 'O'));
		}
		else if (n % 4 == 3) {
			return neg_grid(g);
		}

		return neg_grid(neg_grid(g));
	}
};

#if 0
int main(void) {
	vector<string> g = { ".......",
						"...O...",
						"....O..",
						".......",
						"OO.....",
						"OO....." };
	int n = 5;

	TheBombermanGame t;
	vector<string> ans = t.bomberMan(n, g);

	for (string line : ans)
		cout << line << endl;

	return 0;
}
#endif