
#include <time.h> 
#include <algorithm>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
using namespace std;

class FraudulentActivityNotifications {
public:
	int get_kth(vector<int>& h, int k) {
		for (int i = 0; i <= 200; i++) {
			k -= h[i];
			if (k <= 0) return i;
		}

		// this should never happend
		return -1;
	}

	double get_median(vector<int>& h, int n) {
		double m = 0.0;
		if (n % 2 == 0) {
			double k1 = (double)get_kth(h, (n + 1) / 2);
			double k2 = (double)get_kth(h, (n + 1) / 2 + 1);
			m = (k1 + k2) / 2.0;
		} else m = (double)get_kth(h, (n + 1) / 2);

		return m;
	}

	int activityNotifications(vector<int> a, int d) {
		int n = (int)a.size();
		if (d == n) return 0;

		// init frequencies
		vector<int> h(201, 0);
		for (int i = 0; i < d; i++)h[a[i]]++;

		int ans = 0;
		for (int i = d; i < n; i++) {

			double m = get_median(h, d);
			if ((((double)a[i]) / 2.0) >= m) ans++;

			// update frequencies
			h[a[i-d]]--;
			h[a[i]]++;

		}

		return ans;
	}

#if 0
	// correct version, but too long
	int partition(vector<int>& a, int i, int j) {
		srand(time(NULL));
		int p = rand() % (j - i);
		swap(a[i], a[i + p]);

		int k = i;
		for (int q = i + 1; q <= j; q++)
			if (a[q] < a[i]) { k++; swap(a[k], a[q]); }

		swap(a[i], a[k]);
		return k;
	}

	int get_kth(vector<int>& a, int i, int j, int k) {
		if (i == j) return a[i];

		int r = partition(a, i, j);
		int rank = r - i + 1;
		if (k == rank) return a[r];
		if (k < rank) return get_kth(a, i, r - 1, k);
		else return get_kth(a, r + 1, j, k - rank);
	}

	double get_median(vector<int>& a) {
		int n = (int)a.size();
		
		double m = 0.0;
		if (n % 2 == 0) {
			int k1 = get_kth(a, 0, n - 1, n / 2);
			int k2 = get_kth(a, 0, n - 1, n / 2 + 1);
			m = ((double)k1 + (double)k2) / 2.0;
		} else {
			int k = get_kth(a, 0, n - 1, n / 2 + 1);
			m = (double)k;
		}

		return m;
	}

	int activityNotifications(vector<int> a, int d) {
		int n = (int)a.size(), ans = 0;
		for (int i = d; i < n; i++) {
			vector<int> b(a.begin() + i - d, a.begin() + i);
			double m = get_median(b);
			if ((((double)a[i]) / 2.0) >= m) ans++;
		}

		return ans;
	}
#endif

	vector<int> read_from_file() {
		ifstream myfile("d:\\temp\\in.txt");
		string line, word;

		getline(myfile, line);
		int n = stoi(line);

		getline(myfile, line);
		istringstream ss(line);

		vector<int> a(n);
		for (int i = 0; i < n; i++) {
			ss >> word;
			a[i] = stoi(word);
		}

		return a;
	}
};

#if 0
int main(void) {
	//vector<int> a = { 2, 3, 4, 2, 3, 6, 8, 4, 5 };
	//int d = 5;

	FraudulentActivityNotifications t;
	vector<int> a = t.read_from_file();
	int d = 10122;

	int ans = t.activityNotifications(a, d);
	cout << ans << endl;

	return 0;
}
#endif