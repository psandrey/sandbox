
#include <algorithm>
#include <iostream>
using namespace std;

class Kangaroo {
public:
	string kangaroo(int x1, int v1, int x2, int v2) {
		int dx = x1 - x2, dv = v1 - v2;
		if (dx == 0 && dv == 0) return "YES";
		if (dx == 0 && dv != 0) return "NO";
		if (dx != 0 && dv == 0) return "NO";
		if (dx > 0 && dv > 0) return "NO";
		if (dx < 0 && dv < 0) return "NO";

		dx = abs(dx); dv = abs(dv);
		if (dx % dv == 0) return "YES";
		return "NO";
	}
};

#if 0
int main() {
	int x1 = 0, v1 = 3;
	int x2 = 4, v2 = 2;
	Kangaroo t;
	cout << t.kangaroo(x1, v1, x2, v2) << endl;

	return 0;
}
#endif