#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class Equal {
public:
	int find_ops(int x, int baseline) {
		x -= baseline;

		int five = x / 5; x %= 5;
		int two = x / 2; x %= 2;
		int one = x;

		return (five + two + one);
	}

	int equal(vector<int>& a) {
		int n = (int)a.size();
		int base = INT_MAX;
		for (int i = 0; i < n; i++) base = min(base, a[i]);

		int ans = INT_MAX;
		for (int d = 0; d <= 4; d++) {
			int baseline = base - d;

			int ops = 0;
			for (int i = 0; i < n; i++)
				ops += find_ops(a[i], baseline);

			ans = min(ans, ops);
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = { 2, 2, 3, 7 };

	Equal t;
	cout << t.equal(a) << endl;

	return 0;
}
#endif