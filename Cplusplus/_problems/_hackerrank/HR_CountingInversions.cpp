
#include <time.h> 
#include <algorithm>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
using namespace std;

class CountingInversions {
public:
	long merge(vector<int>& a, int i, int m, int j) {
		vector<int> L(a.begin() + i, a.begin() + m + 1);
		vector<int> R(a.begin() + m + 1, a.begin() + j + 1);

		// merge L and R in a so that a from i to j is sorted
		int szL = (int)L.size(), szR = (int)R.size();
		int l = 0, r = 0, k = i;
		long swp = 0;
		while (l < szL && r < szR)
			if (L[l] <= R[r]) a[k++] = L[l++];
			else { swp += (long)(szL - l); a[k++] = R[r++]; }

		// add tail of L or R (if items remains)
		while (l < szL) a[k++] = L[l++];
		while (r < szR) a[k++] = R[r++];

		return swp;
	}

	long count_inv(vector<int>& a, int i, int j) {
		if (i >= j) return 0;

		int k = (i + j) / 2;
		long l = count_inv(a, i, k);
		long r = count_inv(a, k + 1, j);
		long m = merge(a, i, k, j);

		return (l + r + m);
	}

	// Complete the countInversions function below.
	long countInversions(vector<int>& a) {
		if (a.empty()) return 0;
		return count_inv(a, 0, a.size() - 1);
	}
};

#if 0
int main(void) {
	vector<int> a = { 1, 1, 1, 2, 2 };
	
	CountingInversions t;
	int ans = t.countInversions(a);
	cout << ans << endl;
	return 0;
}
#endif