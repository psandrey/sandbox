
#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class BearAndSteadyGene {
public:
	int idx(char x) {
		if (x == 'A') return 0;
		if (x == 'C') return 1;
		if (x == 'G') return 2;

		return 3;
	}

	bool need(vector<int>& mp, vector<int>& mpq) {
		for (int j = 0; j < 4; j++)
			if (mp[j] < mpq[j]) return true;

		return false;
	}

	bool can_cut(vector<int>& mp, vector<int>& mpq) {
		for (int j = 0; j < 4; j++)
			if (mp[j] < mpq[j]) return false;

		return true;
	}

	// Complete the steadyGene function below.
	int steadyGene(string& a) {
		vector<int> mp(4, 0);
		int n = (int)a.size();
		for (int j = 0; j < n; j++) mp[idx(a[j])]++;

		bool do_we_need = false;
		int k = n / 4;
		vector<int> mpq(4, 0);
		for (int j = 0; j < 4; j++)
			if (mp[j] > k) { mpq[j] = mp[j] - k; do_we_need = true; }

		if (do_we_need == false) return 0;

		fill(mp.begin(), mp.end(), 0);
		int l = 0, r = 0, len = n;
		while (r < n) {
			while (r < n && need(mp, mpq) == true) {
				mp[idx(a[r])]++; r++;
			}

			while (can_cut(mp, mpq) == true) {
				mp[idx(a[l])]--; l++;
			}

			len = min(len, r - (l - 1));
		}

		return len;
	}
};

#if 0
int main(void) {
	string a = "TGATGCCGTCCCCTCAACTTGAGTGCTCCTAATGCGTTGC";

	BearAndSteadyGene t;
	int len = t.steadyGene(a);
	cout << len << endl;

	return 0;
}
#endif