
#include <algorithm>
#include <vector>
#include <iostream>
#include <numeric>
using namespace std;

class MinimumTimeRequired {
public:
	long long how_many(vector<long>& m, long long days) {
		long long ans = 0;
		for (int i = 0; i < (int)m.size(); i++)
			ans += (long long)days / m[i];

		return ans;
	}

	long get_max(vector<long>& m) {
		long M = m[0];
		for (int i = 1; i < (int)m.size(); i++)
			M = max(M, m[i]);

		return M;
	}

	long long minTime(vector<long>& m, long goal) {
		long M = get_max(m);
		long long lo = 1,  hi = (long long)M * goal, ans = 0;
		
		while (lo <= hi) {
			long long days = (lo + hi) / 2LL;

			long long items = how_many(m, days);
			if (items >= goal) { ans = days; hi = days - 1; }
			else lo = days + 1;
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<long> m = { 2, 3 };
	int i = 5;

	MinimumTimeRequired t;
	cout << t.minTime(m, i) << endl;

	return 0;
}
#endif