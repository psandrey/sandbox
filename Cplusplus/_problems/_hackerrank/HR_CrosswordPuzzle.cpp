
#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class CrosswordPuzzle {
public:
	bool h_valid(vector<string>& cross, int i, int j, string& a) {
		int m = cross[i].size();
		if (m - j < a.size()) return false;

		for (int k = 0; k < a.size(); k++) {
			if (cross[i][j + k] == a[k] || cross[i][j + k] == '-') continue;

			return false;
		}

		return true;
	}

	bool v_valid(vector<string>& cross, int i, int j, string& a) {
		int n = cross.size();
		if (n - i < a.size()) return false;

		for (int k = 0; k < a.size(); k++) {
			if (cross[i + k][j] == a[k] || cross[i + k][j] == '-') continue;

			return false;
		}

		return true;
	}

	string geth(vector<string>& cross, int i, int j, int sz) {
		string ans;
		for (int k = 0; k < sz; k++)
			ans += cross[i][j + k];

		return ans;
	}

	string getv(vector<string>& cross, int i, int j, int sz) {
		string ans;
		for (int k = 0; k < sz; k++)
			ans += cross[i + k][j];

		return ans;
	}

	void seth(vector<string>& cross, int i, int j, string s) {
		for (int k = 0; k < s.size(); k++)
			cross[i][j + k] = s[k];
	}

	void setv(vector<string>& cross, int i, int j, string s) {
		for (int k = 0; k < s.size(); k++)
			cross[i + k][j] = s[k];
	}

	// Complete the crosswordPuzzle function below.
	bool puzzle(vector<string>& cross, int i, int j, int n, int m,
		vector<string>& w, vector<bool>& a) {
		if (i == n) {
			for (int k = 0; k < a.size(); k++)
				if (a[k] == false) return false;

			return true;
		}
		if (j == m) return puzzle(cross, i + 1, 0, n, m, w, a);

		if (cross[i][j] == '+') return puzzle(cross, i, j + 1, n, m, w, a);

		// horizontal check
		if ((j == 0 || cross[i][j - 1] == '+') && cross[i][j] != '+') {
			for (int k = 0; k < a.size(); k++) {
				if (a[k] == false && h_valid(cross, i, j, w[k])) {
					string s = geth(cross, i, j, w[k].size());
					seth(cross, i, j, w[k]); a[k] = true;

					bool rc = puzzle(cross, i, j + 1, n, m, w, a);
					if (rc) return true;

					seth(cross, i, j, s); a[k] = false;
				}
			}
		}

		// vertical check
		if ((i == 0 || cross[i - 1][j] == '+') && cross[i][j] != '+') {
			for (int k = 0; k < a.size(); k++) {
				if (a[k] == false && v_valid(cross, i, j, w[k])) {
					string s = getv(cross, i, j, w[k].size());
					setv(cross, i, j, w[k]); a[k] = true;

					bool rc = puzzle(cross, i, j + 1, n, m, w, a);
					if (rc) return true;

					setv(cross, i, j, s); a[k] = false;
				}
			}
		}


		return puzzle(cross, i, j + 1, n, m, w, a);
	}

	vector<string> crosswordPuzzle(vector<string>& cross, string& words) {
		if (words.empty()) return cross;

		bool is_x = false;
		for (int i = 0; i < cross.size(); i++)
			for (int j = 0; j < cross[0].size(); j++) {
				if (cross[i][j] == 'X') {
					cross[i][j] = '+'; is_x = true;
				}
			}

		vector<string> ws;
		while (words.find(";") != string::npos) {
			string w = words.substr(0, words.find(";"));
			ws.push_back(w);
			words = words.substr(w.size() + 1, words.size() - w.size() - 1);
		}
		ws.push_back(words);

		vector<bool> a(ws.size(), false);
		bool rc = puzzle(cross, 0, 0, cross.size(), cross[0].size(), ws, a);

		if (is_x) {
			for (int i = 0; i < cross.size(); i++)
				for (int j = 0; j < cross[0].size(); j++) {
					if (cross[i][j] == '+') cross[i][j] = 'X';
				}
		}

		return cross;
	}

};

#if 0
int main(void) {
	vector<string> cross = {
		"+-++++++++",
		"+-++++++++",
		"+-++++++++",
		"+-----++++",
		"+-+++-++++",
		"+-+++-++++",
		"+++++-++++",
		"++------++",
		"+++++-++++",
		"+++++-++++"};
	string w = "LONDON;DELHI;ICELAND;ANKARA";

	CrosswordPuzzle t;
	vector<string> ans = t.crosswordPuzzle(cross, w);
	for (string s : ans) cout << s << endl;

	return 0;
}
#endif