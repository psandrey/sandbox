
#include <unordered_set>
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class Matrix {
public:
	bool has_machine(int u, unordered_set<int>& m) {
		if (m.find(u) == m.end()) return false;
		return true;
	}

	int find_ds(int u, vector<int>& p) {
		if (u != p[u]) p[u] = find_ds(p[u], p);

		return p[u];
	}

	void union_ds(int u, int v, vector<int>& p, vector<int>& r, vector<bool>& m, unordered_set<int>& m_set) {
		int pu = find_ds(u, p), pv = find_ds(v, p);
		bool has_m = false;
		if (has_machine(u, m_set) || has_machine(v, m_set) || m[pu] || m[pv]) has_m = true;

		if (r[pu] > r[pv]) { p[pv] = pu; m[pu] = has_m; }
		else if (r[pu] < r[pv]) { p[pu] = pv; m[pv] = has_m; }
		else {
			r[pu]++;
			p[pv] = pu; m[pu] = has_m;
		}
	}

	int minTime(vector<vector<int>>& roads, vector<int>& machines) {
		unordered_set<int> m_set;
		for (int i = 0; i < (int)machines.size(); i++) m_set.insert(machines[i]);
		
		// union by rank
		int n = (int)roads.size() + 1; // since is a tree, vertices = edges = 1
		vector<int> r(n, 0), p(n);
		vector<bool> m(n, false);
		for (int u = 0; u < n; u++) {
			p[u] = u;
			if (has_machine(u, m_set)) m[u] = true;
		}
		
		// sort roads in descending order
		struct {
			bool operator()(const vector<int>& a, const vector<int>& b) {
				return (a[2] > b[2]);
			}
		} comp;
		sort(roads.begin(), roads.end(), comp);

		int cost = 0;
		for (vector<int> road : roads) {
			int u = road[0], v = road[1], w = road[2];
			if (find_ds(u, p) != find_ds(v, p)) {
				int pu = find_ds(u, p), pv = find_ds(v, p);

				if (m[pu] && m[pv]) cost += w;
				else union_ds(u, v, p, r, m, m_set);
			}
		}

		return cost;
	}
};

#if 0
int main() {
#if 0
	vector<vector<int>> roads =
		{ {2, 1, 8},
		{1, 0, 5},
		{2, 4, 5},
		{1, 3, 4} };
	vector<int> machines = { 2, 4, 0 };
#endif
	vector<vector<int>> roads =
	{ {0, 1, 2},
	{1, 2, 2},
	{2, 3, 1},
	{3, 4, 2},
	{2, 5, 2} };
	vector<int> machines = { 1, 4, 5 };

	Matrix t;
	cout << t.minTime(roads, machines) << endl;

	return 0;
}
#endif