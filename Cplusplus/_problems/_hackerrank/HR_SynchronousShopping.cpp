
#include <algorithm>
#include <string>
#include <queue>
#include <unordered_map>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>

#include <chrono>
using namespace std;
using namespace std::chrono;

class SynchronousShopping {
public:

	int shop_BFS(int n, int k, vector<string>& centers, vector<vector<int>>& roads) {
		// mask of fish tanks / center
		vector<int> fish(n);
		for (int i = 0; i < (int)centers.size(); i++) {
			istringstream ss(centers[i]);
			string word;  ss >> word;
			int types = stoi(word);

			fish[i] = 0;
			for (int j = 0; j < types; j++) {
				word = ""; ss >> word;
				int k = stoi(word) - 1;
				fish[i] |= (1 << k);
			}
		}
		
		// build adjacency list
		vector<vector<pair<int, int>>> adjL(n, vector<pair<int, int>>());
		for (int i = 0; i < (int)roads.size(); i++) {
			int u = roads[i][0] - 1, v = roads[i][1] - 1, w = roads[i][2];
			adjL[u].push_back(make_pair(v, w));
			adjL[v].push_back(make_pair(u, w));
		}

		int m = (1 << k);
		vector<vector<long>> mask(n, vector<long>(m, LONG_MAX));
		queue<pair<int, int>> q;
		q.push(make_pair(0, fish[0]));
		mask[0][fish[0]] = 0;
		while (!q.empty()) {
			pair<int, int> p = q.front(); q.pop();
			int u = p.first;
			int ku = p.second;
			for (pair<int, int> pp : adjL[u]) {
				int v = pp.first, w = pp.second;
				int k = ku | fish[v];
				if (mask[v][k] > mask[u][ku] + w) {
					mask[v][k] = mask[u][ku] + w;
					q.push(make_pair(v, k));
				}
			}
		}

		long ans = LONG_MAX;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < m; j++) {
				if ((i | j) != (m - 1)) continue;

				long d1 = mask[n - 1][i];
				long d2 = mask[n - 1][j];
				if (d1 != LONG_MAX && d2 != LONG_MAX) ans = min(ans, max(d1, d2));
			}
		}

		return ans;
	}

	// timeout
	/*
		DFS: 685529 ms
		BFS: 15282  ms
	*/
	int shop_DFS(int n, int k, vector<string>& centers, vector<vector<int>>& roads) {
		// mask of fish tanks / center
		vector<int> fish(n);
		for (int i = 0; i < (int)centers.size(); i++) {
			istringstream ss(centers[i]);
			string word;  ss >> word;
			int types = stoi(word);

			fish[i] = 0;
			for (int j = 0; j < types; j++) {
				word = ""; ss >> word;
				int k = stoi(word) - 1;
				fish[i] |= (1 << k);
			}
		}

		// build adjacency list
		vector<vector<pair<int, int>>> adjL(n, vector<pair<int, int>>());
		for (int i = 0; i < (int)roads.size(); i++) {
			int u = roads[i][0] - 1, v = roads[i][1] - 1, w = roads[i][2];
			adjL[u].push_back(make_pair(v, w));
			adjL[v].push_back(make_pair(u, w));
		}

		int m = (1 << k);
		vector<vector<long>> mask(n, vector<long>(m, LONG_MAX));
		DFS(0, fish[0], mask, 0, adjL, fish);

		long ans = LONG_MAX;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < m; j++) {
				if ((i | j) != (m - 1)) continue;

				long d1 = mask[n - 1][i];
				long d2 = mask[n - 1][j];
				if (d1 != LONG_MAX && d2 != LONG_MAX) ans = min(ans, max(d1, d2));
			}
		}

		return ans;
	}

	void DFS(int u, int k, vector<vector<long>>& mask, int dist, vector<vector<pair<int, int>>>& adjL, vector<int>& fish) {
		if (mask[u][k] <= dist) return;

		mask[u][k] = dist;
		for (pair<int, int> pp : adjL[u]) {
			int v = pp.first, w = pp.second;
			DFS(v, k | fish[v], mask, mask[u][k] + w, adjL, fish);
		}
	}

	void from_file() {
		ifstream myfile("d:\\temp\\in.txt");

		string line, word;
		getline(myfile, line);
		istringstream ss(line);

		word = ""; ss >> word; int n = stoi(word);
		word = ""; ss >> word; int m = stoi(word);
		word = ""; ss >> word; int k = stoi(word);

		vector<string> centers(n);
		for (int i = 0; i < n; i++) {
			string line;
			getline(myfile, line);
			centers[i] = line;
		}

		vector<vector<int>> roads(m);
		for (int i = 0; i < m; i++) {
			string line, word;
			getline(myfile, line);
			istringstream ss(line);

			roads[i].resize(3);
			word = ""; ss >> word; roads[i][0] = stoi(word);
			word = ""; ss >> word; roads[i][1] = stoi(word);
			word = ""; ss >> word; roads[i][2] = stoi(word);
		}

		auto start = high_resolution_clock::now();
		cout << shop_BFS(n, k, centers, roads) << endl;
		auto stop = high_resolution_clock::now();
		auto duration = duration_cast<microseconds>(stop - start);
		cout << "Time taken by function: " << duration.count() << " microseconds" << endl;
	}
};

#if 0
int main(void) {
#if 0
	int n = 5;
	int k = 5;
	vector<string> centers = {
		"1 1",
		"1 2",
		"1 3",
		"1 4",
		"1 5"
	};

	vector<vector<int>> roads = { {1, 2, 10},
		{1, 3, 10},
		{2, 4, 10},
		{3, 5, 10},
		{4, 5, 10}
	};
#endif
	int n = 6;
	int k = 4;
	vector<string> centers = {
	"1 2",
	"1 2",
	"1 1",
	"2 3 4",
	"2 3 4",
	"1 4"};
	vector<vector<int>> roads = { {5, 4, 646},
	{4, 1, 997},
	{2, 1, 881},
	{2, 6, 114},
	{3, 1, 46} };

	SynchronousShopping t;
	//cout << t.shop(n, k, centers, roads) << endl;
	t.from_file();

	return 0;
}
#endif