
#include <algorithm>
#include <string>
#include <iostream>
using namespace std;

class RecursiveDigitSum {
public:
	int sd(string x) {
		if (1 == (int)x.size()) return (x[0] - '0');

		int s = 0;
		for (int i = 0; i < (int)x.size(); i++) {
			int d = x[i] - '0';
			s += d;
		}
		return sd(to_string(s));
	}

	int superDigit(string n, int k) {
		int s = 0;
		for (int i = 0; i < (int)n.size(); i++) {
			int d = n[i] - '0';
			int x = d * k;
			int D = sd(to_string(x));
			s += D;
		}

		return sd(to_string(s));
	}

};

#if 0
int main(void) {

	string n = "9875";
	int k = 4;

	RecursiveDigitSum t;
	cout << t.superDigit(n, k) << endl;

	return 0;
}
#endif