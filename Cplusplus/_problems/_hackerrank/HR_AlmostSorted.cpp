
#include <algorithm>
#include <string>
#include <queue>
#include <unordered_map>
#include <vector>
#include <iostream>
using namespace std;

class AlmostSorted {
public:
	void almostSorted(vector<int>& a) {
		int n = (int)a.size();
		if (n <= 1) cout << "yes" << endl;

		// swap
		int count = 0, l = -1, r = -1;
		for (int i = 1; i < n; i++) {
			if (a[i - 1] > a[i]) {
				if (l == -1 && r == -1) { l = i - 1; r = i; count++; }
				else { r = i; count++; }
			}

			if (count == 3) break;
		}

		if (count == 0) { cout << "yes" << endl; return; }
		if (count <= 2) {
			bool lok = true, rok = true;
			if (l > 0 && a[r] < a[l - 1]) lok = false;
			if (l + 1 != r && a[r] > a[l + 1] ) lok = false;

			if (r < n - 1 && a[l] > a[r + 1]) rok = false;
			if (l + 1 != r && a[l] < a[r - 1]) rok = false;

			if (lok && rok) cout << " yes swap " << (l+1) << " " << (r+1) << endl;
			else cout << "no" << endl;

			return;
		}

		// reverse
		count = 0; l = -1; r = -1;
		for (int i = 1; i < n; i++) {
			if (count == 0) {
				if (a[i] > a[i - 1]) continue;
				l = i - 1; count++;
			}
			else if (count == 1) {
				if (a[i] < a[i - 1]) continue;
				r = i - 1;
				count++;
			}
			else if (count == 2) {
				if (a[i] > a[i - 1]) continue;
				count++;
			}

			if (count == 3) break;
		}
		if (count == 1) r = n - 1;
		if (count == 3) { cout << "no" << endl; return; }

		bool lok = true, rok = true;
		if (l > 0 && a[r] < a[l - 1]) rok = false;
		if (r < n - 1 && a[l] > a[r + 1]) lok = false;

		if (rok && lok) { cout << "reverse " << (l+1) << " " << (r+1) << endl; return; }

		cout << "no" << endl;
	}
};

#if 0
int main(void) {
	//vector<int> a = { 4, 2 };
	vector<int> a = {1, 5, 4, 3, 2, 6};
	
	AlmostSorted t;
	t.almostSorted(a);
	
	return 0;
}
#endif