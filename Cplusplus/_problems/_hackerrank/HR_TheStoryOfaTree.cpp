
#include <string>
#include <queue>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
using namespace std;

class TheStoryOfaTree {
public:
	int gcd(int a, int b) {
		if (a < b) return gcd(b, a);

		while (b != 0) {
			int r = a % b;
			a = b; b = r;
		}

		return a;
	}

	int DFS(int u, vector<vector<int>>& adjL, vector<bool>& marked, unordered_map<int, unordered_set<int>>& guess) {
		marked[u] = true;
		int hits = 0;
		for (int v : adjL[u]) {
			if (marked[v] == true) continue;

			if (guess.find(v) != guess.end() && guess[v].find(u) != guess[v].end()) hits++;
			hits += DFS(v, adjL, marked, guess);
		}

		return hits;
	}

	int DFS(int u, vector<vector<int>>& adjL, vector<int>& hits, int k, unordered_map<int, unordered_set<int>>& guess) {
		int wins = 0;
		if (hits[u] >= k) wins++;

		for (int v : adjL[u]) {
			if (hits[v] != -1) continue;

			// is_parent[v]=u is_parent[u]=v
			bool is_pvu = false, is_puv = false;
			if (guess.find(v) != guess.end() && guess[v].find(u) != guess[v].end()) is_pvu = true;
			if (guess.find(u) != guess.end() && guess[u].find(v) != guess[u].end()) is_puv = true;
			
			if     (is_pvu && !is_puv) hits[v] = hits[u] - 1; // is reverse but not foreward
			else if(!is_pvu && is_puv) hits[v] = hits[u] + 1; // is foreward but not reverse
			else hits[v] = hits[u]; // is both or none

			wins += DFS(v, adjL, hits, k, guess);
		}

		return wins;
	}

	string storyOfATree(int n, vector<vector<int>>& e, int k, vector<vector<int>>& g) {	
		vector<vector<int>> adjL(n, vector<int>());
		for (int i = 0; i < (int)e.size(); i++) {
			int u = e[i][0] - 1, v = e[i][1] - 1;
			adjL[u].push_back(v);
			adjL[v].push_back(u);
		}

		unordered_map<int, unordered_set<int>> guess;
		for (int i = 0; i < (int)g.size(); i++) {
			int p = g[i][0] - 1, u = g[i][1] - 1;
			if (guess.find(u) == guess.end()) guess[u] = unordered_set<int>();
			guess[u].insert(p);
		}

		vector<int> hits(n, -1);
		vector<bool> marked(n, false);
		hits[0] = DFS(0, adjL, marked, guess);
		int wins = DFS(0, adjL, hits, k, guess);

		if (wins == 0) return "0/1";

		int _gcd = gcd(n, wins);
		n = n / _gcd; wins = wins / _gcd;
		return (to_string(wins) + "/" + to_string(n));
	}

	void from_file() {
		ifstream myfile("d:\\temp\\in.txt");
		int q;
		myfile >> q;
		myfile.ignore(numeric_limits<streamsize>::max(), '\n');

		for (int q_itr = 0; q_itr < q; q_itr++) {
			int n;
			myfile >> n;
			myfile.ignore(numeric_limits<streamsize>::max(), '\n');
		
			vector<vector<int>> edges(n - 1);
			for (int edges_row_itr = 0; edges_row_itr < n - 1; edges_row_itr++) {
				edges[edges_row_itr].resize(2);

				for (int edges_column_itr = 0; edges_column_itr < 2; edges_column_itr++) {
					myfile >> edges[edges_row_itr][edges_column_itr];
				}

				myfile.ignore(numeric_limits<streamsize>::max(), '\n');
			}

			string gk_temp;
			getline(myfile, gk_temp);
			istringstream ss(gk_temp);
			int g, k;
			{string word;  ss >> word; g = stoi(word); }
			{string word;  ss >> word; k = stoi(word); }

			vector<vector<int>> guesses(g);
			for (int guesses_row_itr = 0; guesses_row_itr < g; guesses_row_itr++) {
				guesses[guesses_row_itr].resize(2);

				for (int guesses_column_itr = 0; guesses_column_itr < 2; guesses_column_itr++) {
					myfile >> guesses[guesses_row_itr][guesses_column_itr];
				}

				myfile.ignore(numeric_limits<streamsize>::max(), '\n');
			}

			string result = storyOfATree(n, edges, k, guesses);

			cout << result << "\n";
		
		}
	}
};

#if 0
int main(void) {
	int n = 4, k = 2;
	vector<vector<int>> e = { {1,2},{1,3},{3,4} };
	vector<vector<int>> g = { {1,2}, {3,4} };
	
	TheStoryOfaTree t;
	t.from_file();
	//string ans = t.storyOfATree(n, e, k, g);
	//cout << ans << endl;
	
	return 0;
}
#endif