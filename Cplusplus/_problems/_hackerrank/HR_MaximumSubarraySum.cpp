
#include <set>
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class MaximumSubarraySum {
public:
	typedef long long ll;
	long maximumSum(vector<long>& a, long m) {
		int n = (int)a.size();
				
		vector<long> r(n); r[0] = a[0] % m;
		long ans = LONG_MIN, sum = 0L;
		for (int i = 0; i < n; i++) {
			sum = (sum + a[i]) % m;
			r[i] = sum;

			ans = max(ans, sum);
		}

		set<long> r_set;
		for (long x: r) {
			auto p = r_set.insert(x);

			// previous closest biggest rest
			if (++p.first != r_set.end()) {
				long d = (long)((ll)x - *p.first + m) % m;
				ans = max(ans, (long)d);
			}
		}

		return ans;
	}
};

#if 0
int main() {
	//vector<long> a = { 3, 3, 9, 9, 5 };
	//long m = 7;
	vector<long> a = { 1, 5, 9 };
	long m = 5;

	MaximumSubarraySum t;
	cout << t.maximumSum(a, m) << endl;

	return 0;
}
#endif