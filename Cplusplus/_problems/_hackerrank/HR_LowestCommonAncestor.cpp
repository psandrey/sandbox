
#include <algorithm>
#include <iostream>
using namespace std;

namespace LCA_ns {
	class Node {
	public:
		int data;
		Node* left;
		Node* right;
		Node(int d) {
			data = d;
			left = NULL;
			right = NULL;
		}
	};

	class LowestCommonAncestor {
	public:

		Node* findLCA(Node* n, Node** LCA, int v1, int v2) {
			if (n == NULL) return NULL;

			Node* me = NULL;
			if (n->data == v1 || n->data == v2) me = n;

			Node* l = NULL, * r = NULL;
			l = findLCA(n->left, LCA, v1, v2);
			r = findLCA(n->right, LCA, v1, v2);

			if (l != NULL && r != NULL) (*LCA) = n;
			if ((l != NULL || r != NULL) && me != NULL) (*LCA) = n;

			if (l != NULL) return l;
			if (r != NULL) return r;
			if (me != NULL) return me;

			return NULL;
		}

		Node* lca(Node* root, int v1, int v2) {
			Node* LCA = NULL;
			findLCA(root, &LCA, v1, v2);

			return LCA;
		}

		Node* build_tree() {
			Node* root = new Node(4);
			root->left = new Node(2);
			root->right = new Node(7);
			root->left->left = new Node(1);
			root->left->right = new Node(3);
			root->right->left = new Node(6);

			return root;

		}
	};
}

#if 0
using namespace LCA_ns;
int main() {
	LowestCommonAncestor t;
	Node* root = t.build_tree();
	int v1 = 1, v2 = 7;

	Node* LCA = t.lca(root, v1, v2);
	cout << LCA->data << endl;

	return 0;
}
#endif