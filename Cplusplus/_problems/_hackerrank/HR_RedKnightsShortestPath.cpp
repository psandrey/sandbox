
#include <algorithm>
#include <string>
#include <queue>
#include <unordered_map>
#include <vector>
#include <iostream>
using namespace std;

class RedKnightsShortestPath {
public:
	void printShortestPath(int n, int is, int js, int ie, int je) {
		// Print the distance along with the sequence of moves.
		vector<vector<int>> m = { {-2, -1}, {-2, 1}, {0, 2}, {2, 1}, {2, -1}, {0, -2} };
		vector<string>      p = { "UL"   , "UR"   , "R"   , "LR"  , "LL"   , "L" };

		vector<vector<int>> d(n, vector<int>(n, -1)), moves(n, vector<int>(n, -1));
		queue<pair<int, int>> q;
		q.push(make_pair(is, js)); d[is][js] = 0;
		while (!q.empty()) {
			pair<int, int> p = q.front(); q.pop();
			int i = p.first, j = p.second;
			if (i == ie && j == je) break;

			for (int k = 0; k < m.size(); k++) {
				int ii = i + m[k][0], jj = j + m[k][1];
				if (ii < 0 || jj < 0 || ii >= n || jj >= n) continue;
				if (d[ii][jj] != -1) continue;

				d[ii][jj] = d[i][j] + 1; moves[ii][jj] = k;
				q.push(make_pair(ii, jj));
			}
		}

		if (d[ie][je] == -1) cout << "Impossible" << endl;
		else {
			cout << d[ie][je] << endl;
			vector<string> path;
			int i = ie, j = je;
			while (d[i][j] > 0) {
				int k = moves[i][j];
				path.insert(path.begin(), p[k]);
				i = i - m[k][0]; j = j - m[k][1];
			}
			for (int k = 0; k < path.size(); k++) cout << path[k] << " ";
			cout << endl;
		}
	}
};

#if 0
int main(void) {
	int n = 7;
	int is = 6,
		js = 6,
		ie = 0,
		je = 1;

	RedKnightsShortestPath t;
	t.printShortestPath(n, is, js, ie, je);

	return 0;
}
#endif