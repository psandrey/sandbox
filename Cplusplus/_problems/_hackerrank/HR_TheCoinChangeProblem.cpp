
#include <string>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class TheCoinChangeProblem {
public:
	long f(int S, int k, vector<long>& c, vector<vector<long>>& memo) {
		if (memo[S][k] != -1) return memo[S][k];

		if (S == 0) memo[S][k] = 1;
		else if (k == 0) memo[S][k] = 0;
		else {
			if (S < c[k - 1]) memo[S][k] = f(S, k - 1, c, memo);
			else memo[S][k] = f(S, k - 1, c, memo) + f(S - c[k - 1], k, c, memo);
		}

		return memo[S][k];
	}

	long getWays(int S, vector<long> c) {
		int n = (int)c.size();
		vector<vector<long>> memo(S + 1, vector<long>(n + 1, -1));

		long ans = f(S, n, c, memo);
		return ans;
	}

};

#if 0
int main(void) {
	vector<long> c = { 1, 2, 3 };
	int S = 4;

	TheCoinChangeProblem t;
	cout << t.getWays(S, c) << endl;

	return 0;
}
#endif