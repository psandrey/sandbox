

#include <algorithm>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
using namespace std;

class BalancedForest {
public:
	long long build_sums(vector<vector<int>>& tree, int k, vector<int>& c, vector<bool>& marked, 
						vector<long long>& dp) {
		marked[k] = true;

		dp[k] = c[k];
		if (!tree[k].empty()) {
			for (int i = 0; i < (int)tree[k].size(); i++) {
				if (marked[tree[k][i]] == true) continue;

				dp[k] += build_sums(tree, tree[k][i], c, marked, dp);
			}
		}

		return dp[k];
	}

	void second_cut(vector<vector<int>>& tree, int k, long long S1, vector<bool>& marked,
					vector<long long>& dp, long long & ans) {
		marked[k] = true;

		if (!tree[k].empty()) {
			for (int j = 0; j < (int)tree[k].size(); j++) {
				if (marked[tree[k][j]] == true) continue;

				long long S2 = dp[tree[k][j]];
				long long S3 = dp[0] - S2;

				if (S1 == S2 && S1 - S3 >= 0)
					ans = min(ans, S1 - S3);
				else if (S1 == S3 && S1 - S2 >= 0)
					ans = min(ans, S1 - S2);
				else if (S2 == S3 && S2 - S1 >= 0) 
					ans = min(ans, S2 - S1);

				second_cut(tree, tree[k][j], S1, marked, dp, ans);
			}
		}
	}

	void find_min(vector<vector<int>>& tree, int k, vector<bool>& marked,
				 vector<long long>& dp, long long& ans, vector<int>& c) {
		
		marked[k] = true;

		if (!tree[k].empty()) {

			for (int j = 0; j < (int)tree[k].size(); j++) {
				if (marked[tree[k][j]] == true) continue;

				long long S1 = dp[tree[k][j]];
				long long S2 = dp[0] - S1;

				if (S1 == S2)
					ans = min(ans, S1);
				else if (S1 > S2) {
					vector<bool> marked2(tree.size(), false);
					marked2[k] = true;

					vector<long long> dps(tree.size(), 0LL);
					build_sums(tree, tree[k][j], c, marked2, dps);

					fill(marked2.begin(), marked2.end(), false);
					marked2[k] = true;
					second_cut(tree, tree[k][j], S2, marked2, dps, ans);
				}
				else {
					vector<bool> marked2(tree.size(), false);
					marked2[tree[k][j]] = true;

					vector<long long> dps(tree.size(), 0LL);
					build_sums(tree, 0, c, marked2, dps);

					fill(marked2.begin(), marked2.end(), false);
					marked2[tree[k][j]] = true;
					second_cut(tree, 0, S1, marked2, dps, ans);
				}

				find_min(tree, tree[k][j], marked, dp, ans,c );
			}
		}
	}

	long long balancedForest(vector<int>& c, vector<vector<int>>& e) {
		int n = (int)c.size(), m = (int)e.size();

		vector<vector<int>> tree(n, vector<int>());
		for (int i = 0; i < m; i++) {
			int u = e[i][0] - 1;
			int v = e[i][1] - 1;
			tree[u].push_back(v);
			tree[v].push_back(u);
		}
		
		vector<long long> dp(n, 0);
		vector<bool> marked(n, false);
		build_sums(tree, 0, c, marked, dp);

		long long ans = LLONG_MAX;
		fill(marked.begin(), marked.end(), false);
		find_min(tree, 0, marked, dp, ans, c);

		return (ans == LLONG_MAX ? -1 : ans);
	}

	void read_from_file() {
		ifstream myfile("d:\\temp\\in.txt");
		string word;

		int q;
		myfile >> q;
		myfile.ignore(numeric_limits<streamsize>::max(), '\n');

		for (int q_itr = 0; q_itr < q; q_itr++) {
			int n;
			myfile >> n;
			myfile.ignore(numeric_limits<streamsize>::max(), '\n');

			string c_temp_temp;
			getline(myfile, c_temp_temp);
			istringstream ss(c_temp_temp);

			vector<int> c(n);
			for (int i = 0; i < n; i++) {
				ss >> word;
				c[i] = stoi(word);;
			}

			vector<vector<int>> edges(n - 1);
			for (int i = 0; i < n - 1; i++) {
				edges[i].resize(2);

				for (int j = 0; j < 2; j++) {
					myfile >> edges[i][j];
				}

				myfile.ignore(numeric_limits<streamsize>::max(), '\n');
			}

			long long result = balancedForest(c, edges);

			cout << result << "\n";
		}
	}
};

#if 0
int main(void) {
	vector<int> c = { 15, 12, 8, 14, 13 };
	vector<vector<int>> e = {{ 1, 2 }, { 1, 3 }, { 1, 4 }, { 4, 5 }};

	BalancedForest t;
	t.read_from_file();
	
	//cout << t.balancedForest(c, e) << endl;
	
	return 0;
}
#endif