
#include <algorithm>
#include <iostream>
using namespace std;

class ThePowerSum {
public:
	int ps(int x, int m, int n) {
		if (x == 0) return 1;
		if (m == 0) return 0;

		int ways = 0;
		for (int y = m; y >= 1; y--) {
			int p = (int)pow(y, n);
			if (p > x) continue;

			ways += ps(x - p, y - 1, n);
		}

		return ways;
	}

	int powerSum(int x, int n) {
		double _sqrt = 1.0 / (double)n;
		double __sqrt = pow((double)x, _sqrt);
		int m = (int)__sqrt;
		return ps(x, m, n);
	}
};

#if 0
int main(void) {

	int x = 10;
	int n = 2;

	ThePowerSum t;
	cout << t.powerSum(x, n) << endl;
	return 0;
}
#endif