
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class ArrayManipulation {
public:
	long arrayManipulation(int n, vector<vector<int>>& q) {
		int m = (int)q.size();

		vector<pair<int,long>> b, e;
		for (int i = 0; i < m; i++) {
			if (q[i][0] > n || q[i][0] == 0) continue;

			b.push_back(make_pair(q[i][0], q[i][2]));
			e.push_back(make_pair(q[i][1], q[i][2]));
		}
		sort(b.begin(), b.end());
		sort(e.begin(), e.end());

		m = (int)b.size();
		long ans = 0L, c = 0L;
		int i = 0;
		for (int j = 0; j < m; j++) {
			while (i < m && b[i].first <= e[j].first) { c += b[i].second; i++; }

			ans = max(ans, c);
			c -= e[j].second;
		}

		return ans;
	}
};

#if 0
int main() {
	//vector<vector<int>> a =
	//{{ 1, 5, 3 },
	// {4, 8, 7},
	// {6, 9, 1}};
	// int n = 10;

	vector<vector<int>> a =
	{{ 1, 10, 3 },
	 { 4, 4, 7},
	 { 8, 9, 1}};
	int n = 10;

	ArrayManipulation t;
	cout << t.arrayManipulation(n, a) << endl;

	return 0;
}
#endif