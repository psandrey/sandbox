
#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class RoadsAndLibraries {
public:
	int dfind(int u, vector<int>& p) {
		if (p[u] != u) p[u] = dfind(p[u], p);
		return p[u];
	}

	void dunion(int u, int v, vector<int>& p, vector<int>& r) {
		int idu = dfind(u, p), idv = dfind(v, p);
		if (idu != idv) {
			if (r[idu] > r[idv]) p[idv] = idu;
			else if (r[idv] > r[idu]) p[idu] = idv;
			else { r[idu]++; p[idv] = idu; }
		}
	}

	long long roadsAndLibraries(int n, int clib, int croad, vector<vector<int>>& e) {
		vector<int> r(n, 0), p(n); for (int i = 0; i < n; i++) p[i] = i;
		int m = (int)e.size();

		for (int i = 0; i < m; i++) {
			int u = e[i][0] - 1, v = e[i][1] - 1;

			if (dfind(u, p) != dfind(v, p)) dunion(u, v, p, r);
		}

		long long cost = 0LL;
		if (clib < croad) cost = (long long)n * clib;
		else {
			vector<bool> l(n, false);
			for (int u = 0; u < n; u++) {
				int idu = dfind(u, p);
				if (l[idu] == true) cost += croad;
				else {
					cost += (long long)clib;
					l[idu] = true;
				}
			}
		}

		return cost;
	}
};

#if 0
int main(void) {
#if 0
	int n = 9;
	int clib = 91;
	int croad = 84;
	vector<vector<int>> e = {{8, 2}, { 2, 9 }};
#endif	

	int n = 5;
	int clib = 92;
	int croad = 23;
	vector<vector<int>> e = { {2, 1}, {5, 3}, {5, 1}, {3, 4}, {3, 1}, {5, 4}, {4, 1}, {5, 2}, {4, 2} };

	RoadsAndLibraries t;
	long long ans = t.roadsAndLibraries(n, clib, croad, e);
	cout << ans << endl;

	
	return 0;
}
#endif