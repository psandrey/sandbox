
#include <algorithm>
#include <unordered_map>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
using namespace std;

class FrequencyQueries {
public:
	void decrease_freq(int f, unordered_map<int, int>& h) {
		if (h.find(f) != h.end()) {
			h[f]--;
			if (h[f] == 0) h.erase(h.find(f));
		}
	}

	void increase_freq(int f, unordered_map<int, int>& h) {
		if (h.find(f) != h.end()) h[f]++;
		else h[f] = 1;
	}

	vector<int> freqQuery(vector<vector<int>> q) {
		vector<int> ans;
		unordered_map<int, int> hf, hm;

		for (int i = 0; i < (int)q.size(); i++) {
			int op = q[i][0];
			int x = q[i][1];
			switch (op) {
			case 1: // insert
				if (hm.find(x) != hm.end()) {
					decrease_freq(hm[x], hf);
					hm[x]++;
				} else hm[x] = 1;

				increase_freq(hm[x], hf);
				break;
			case 2: // delete
				if (hm.find(x) != hm.end()) {
					decrease_freq(hm[x], hf);
					hm[x]--;
					if (hm[x] == 0) hm.erase(hm.find(x));
					else increase_freq(hm[x], hf);
				}
				break;
			case 3: // querry
				if (hf.find(x) != hf.end()) ans.push_back(1);
				else ans.push_back(0);
				break;
			default:
				break;
			}
		}

		return ans;
	}

	vector<vector<int>> read_from_file() {
		ifstream myfile("d:\\temp\\in.txt");

		string q_temp;
		getline(myfile, q_temp);
		int q = stoi(q_temp);
		vector<vector<int>> queries(q);

		for (int i = 0; i < q; i++) {
			queries[i].resize(2);

			string queries_row_temp_temp, word;
			getline(myfile, queries_row_temp_temp);
			istringstream ss(queries_row_temp_temp);

			ss >> word;
			queries[i][0] = stoi(word);
			ss >> word;
			queries[i][1] = stoi(word);
		}

		return queries;
	}
};

#if 0
int main(void) {
	FrequencyQueries t;
	vector<vector<int>> q = { {1, 3}, {2, 3}, {3, 2}, {1, 4}, {1, 5}, {1, 5}, {1, 4}, {3, 2}, {2, 4}, {3, 2} };
	//vector<vector<int>> q = t.read_from_file();

	vector<int> ans = t.freqQuery(q);
	for (auto x : ans) cout << x << endl;

	return 0;
}
#endif