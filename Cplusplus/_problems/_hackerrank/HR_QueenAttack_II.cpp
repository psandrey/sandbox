
#include <algorithm>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
using namespace std;

class QueenAttack_II {
public:
	long long queensAttack(int n, int nk, int r, int c, vector<vector<int>>& a) {
		int hr = max(0, n - c), hl = max(0, c - 1), vu = max(0, r - 1), vd = max(0, n - r);
		int c1 = max(0, min(r - 1, n - c)),
			c2 = max(0, min(r - 1, c - 1)),
			c3 = max(0, min(n - r, c - 1)),
			c4 = max(0, min(n - r, n - c));

		for (int k = 0; k < nk; k++) {
			if (a[k][0] == r && a[k][1] == c) return 0LL;

			if (a[k][0] == r) {
				if (a[k][1] < c)      hl = min(hl, c - a[k][1] - 1);
				else if (a[k][1] > c) hr = min(hr, a[k][1] - c - 1);
			}
			else if (a[k][1] == c) {
				if (a[k][0] < r)      vu = min(vu, r - a[k][0] - 1);
				else if (a[k][0] > r) vd = min(vd, a[k][0] - r - 1);
			}
			else if (abs(a[k][0] - r) == abs(a[k][1] - c)) {
				if      (a[k][0] < r && a[k][1] > c) c1 = min(c1, a[k][1] - c - 1);
				else if (a[k][0] < r && a[k][1] < c) c2 = min(c2, c - a[k][1] - 1);
				else if (a[k][0] > r && a[k][1] < c) c3 = min(c3, c - a[k][1] - 1);
				else if (a[k][0] > r && a[k][1] > c) c4 = min(c4, a[k][1] - c - 1);
			}
		}

		long long ans = (long long)hr + (long long)hl +
			(long long)vu + (long long)vd +
			(long long)c1 + (long long)c2 +
			(long long)c3 + (long long)c4;

		return ans;
	}

	void read_from_file() {
		ifstream myfile("d:\\temp\\in.txt");
		string line, word;

		int n, nk, r, c;
		{
			getline(myfile, line);
			istringstream ss(line);
			ss >> word;
			n = stoi(word);
			ss >> word;
			nk = stoi(word);
		}

		{
			getline(myfile, line);
			istringstream ss(line);
			ss >> word;
			r = stoi(word);
			ss >> word;
			c = stoi(word);
		}

		vector<vector<int>> a(n + 1, vector<int>(2));
		for (int i = 1; i <= n; i++) {
			getline(myfile, line);
			istringstream ss(line);

			ss >> word;
			a[i][0] = stoi(word);
			ss >> word;
			a[i][1] = stoi(word);
		}

		long long ans = queensAttack(n, nk, r, c, a);
		cout << ans << endl;
	}
};

#if 0
int main(void) {
	int n = 5;
	int nk = 3;
	vector<vector<int>> a = { {5, 5}, {4, 2}, {2, 3} };
	int r = 4, c = 3;

	QueenAttack_II t;
	//long long ans = t.queensAttack(n, nk, r, c, a);
	//cout << ans << endl;
	t.read_from_file();

	return 0;
}
#endif