
#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class TheFullCountingSort {
public:
	void countSort(vector<vector<string>>& a) {
		vector<int> c(100, 0);

		int n = a.size();
		int k = n / 2;
		for (int i = 0, j = 0; i < n; i++, j++) {
			if (j < k) a[i][1] = "-";

			int key = stoi(a[i][0]);
			c[key]++;
		}

		for (int i = 1; i < 100; i++)
			c[i] += c[i - 1];

		vector<string> b(n);
		for (int i = n - 1; i >= 0; i--) {
			int key = stoi(a[i][0]);
			b[c[key] - 1] = a[i][1];
			c[key]--;
		}

		for (int i = 0; i < n; i++)
			cout << b[i] << " ";
		cout << endl;
	}
};

#if 0
int main() {
	vector<vector<string>> a =
		{ {"0", "ab"},
		{"6", "cd"},
		{"0", "ef"},
		{"6", "gh"},
		{"4", "ij"},
		{"0", "ab"},
		{"6", "cd"},
		{"0", "ef"},
		{"6", "gh"},
		{"0", "ij"},
		{"4", "that"},
		{"3", "be"},
		{"0", "to"},
		{"1", "be"},
		{"5", "question"},
		{"1", "or"},
		{"2", "not"},
		{"4", "is"},
		{"2", "to"},
		{"4", "the"} };

	TheFullCountingSort t;
	t.countSort(a);

	return 0;
}
#endif