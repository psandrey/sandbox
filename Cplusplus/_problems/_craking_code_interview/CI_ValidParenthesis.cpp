
#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class ValidParenthesis {
public:
	vector<string> solve(int n) {

		vector<string> ans;
		string st;
		solve(st, n, n, ans);

		return ans;
	}

	void solve(string& st, int n, int m, vector<string>& ans) {
		if (n == 0 && m == 0) ans.push_back(st);
		else {
			if (m > n) {
				if (n > 0) {
					st.push_back('(');
					solve(st, n - 1, m, ans);
					st.pop_back();
				}

				st.push_back(')');
				solve(st, n, m - 1, ans);
				st.pop_back();
			} else {
				st.push_back('(');
				solve(st, n - 1, m, ans);
				st.pop_back();
			}
		}
	}
};

#if 0
int main(void) {
	ValidParenthesis t;

	vector<string> ans = t.solve(3);
	for (string s : ans)
		cout << s << endl;

}
#endif