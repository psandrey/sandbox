
/*
 * A circus is designing a tower routine consisting of people standing atop one anotherís
 * shoulders  For practical and aesthetic reasons, each person must be both shorter
 * and lighter than the person below him or her  Given the heights and weights of each
 * person in the circus, write a method to compute the largest possible number of people
 * in such a tower.
 *
 * EXAMPLE:
 * Input (ht, wt): (65, 100) (70, 150) (56, 90) (75, 190) (60, 95) (68, 110)
 * Output: The  longest  tower  is  length  6  and  includes  from  top  to  bottom:  (56,  90)
 * (60,95) (65,100) (68,110) (70,150) (75,190)
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class CircusTower {
public:
	vector<pair<int, int>> solve(vector<int>& ht, vector<int>& wt) {
		int n = (int)ht.size();
		
		vector<pair<int, int>> p(n);
		for (int i = 0; i < n; i++) p[i] = make_pair(ht[i], wt[i]);

		struct {
			bool operator()(const pair<int, int>& a, const pair<int, int>& b) {
				if (a.first == b.first) return (a.second < b.second);

				return (a.first < b.first);
			}
		} comp;

		sort(p.begin(), p.end(), comp);

		vector<int> dp(n), prev(n); for (int i = 0; i < n; i++) { dp[i] = 1; prev[i] == i; }
		int tallest = 1, k = 0;
		for (int i = 1; i < n; i++) {
			for (int j = i - 1; j >= 0; j--) {
				if (p[j].second <= p[i].second && dp[i] < dp[j] + 1) {
					dp[i] = dp[j] + 1;
					prev[i] = j;
					if (tallest < dp[i]) { tallest = dp[i]; k = i; }
				}
			}
		}

		vector<pair<int, int>> out;
		out.push_back(p[k]);
		while (k != prev[k]) { k = prev[k]; out.push_back(p[k]); }

		return out;
	}
};

#if 0
int main(void) {
	vector<int> ht = {60, 70, 56, 75, 60, 68};
	vector<int> wt = {120, 150, 90, 190, 95, 110};

	CircusTower t;
	vector<pair<int, int>> ans = t.solve(ht, wt);
	for (pair<int, int> p : ans) {
		cout << "(" << p.first << "," << p.second << ")" << endl;
	}

	return 0;
}
#endif