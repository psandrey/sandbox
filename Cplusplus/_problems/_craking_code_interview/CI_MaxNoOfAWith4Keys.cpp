
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class MaxNoOfAWith4Keys {
public :
	
	int maxA(int n) {
#if 0
		return maxA_v1(0, 0, n, 0);
#else
		return maxA_v2(n);
#endif
	}

	// v1: brute-force
	int maxA_v1(int i, int b, int n, int a) {
		if (n - i < 3) return (a + max(n - i, (n - i) * b));
		
		int p = 0, k = 0;
		if (b > 0) p = maxA_v1(i + 1, b, n, a + b);
		else k = maxA_v1(i + 1, b, n, a + 1);

		int scp = 0;
		if (a > 0) scp = maxA_v1(i + 3, a, n, a + a);

		return max(k, max(p, scp));
	 }

	// v2: dynammic programming
	int maxA_v2(int n) {
		if (n < 7) return n;

		vector<int> screen(n + 1);
		for (int k = 0; k < 7; k++) screen[k] = k;

		for (int k = 7; k <= n; k++) {
			int m = INT_MIN;
			for (int j = k; j >= 1; j--)
				m = max(m, screen[j - 3] * (k - j + 2));
			screen[k] = m;
		}

		return screen[n];
	}
};

#if 0
int main(void) {
	int n = 20;
	
	MaxNoOfAWith4Keys t;
	int ans;
	
	ans = t.maxA(n);
	cout << ans << endl;
	
	return 0;
}
#endif