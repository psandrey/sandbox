
/*
 * Given a string S, you are allowed to convert it to a palindrome by adding characters in front of it.
 * Find and return the shortest palindrome you can find by performing this transformation.
 *
 * For example, given "aacecaaa", return "aaacecaaa"; given "abcd", return "dcbabcd".
 */

#include <algorithm>
#include <string>
#include <iostream>
using namespace std;

class ShortestPalindrome {
public:

	// version 1: T(n) = O(n^2)
	bool check(string& s, int e) {
		if (e == 0) return true;
		int i = 0, j = e;
		while (i <= j && s[i] == s[j]) { i++; j--; }

		if (i > j) return true;
		return false;
	}

	string shortest_palindrome_v1(string s) {
		if (s.empty()) return s;

		int len = (int)s.size();
		while (len > 1 && check(s, len - 1) == false) len--;

		string suffix = s.substr(len, s.size());
		reverse(suffix.begin(), suffix.end());
		return (suffix + s);
	}

	// version 2: recursive T(n) = O(n) ???, S(n) = O(n)
	string shortest_palindrome_v2(string s) {
		if (s.empty()) return s;
		int n = (int)s.size();

		int i = 0, j = n - 1;
		while (j >= 0) {
			if (s[i] == s[j]) { i++; j--; }
			else j--;
		}
		if (i == n) return s;

		string suffix = s.substr(i, n - i);
		string prefix = suffix; reverse(prefix.begin(), prefix.end());
		string mid = shortest_palindrome_v2(s.substr(0, i));

		return (prefix + mid + suffix);
	}
};

#if 0
int main(void) {
	//string s = "abbaxbayba";
	//string s = "abb";
	//string s = "babbbabbaba";
	string s = "abba";

	ShortestPalindrome t;
	cout << t.shortest_palindrome_v1(s) << endl;

	return 0;
}
#endif