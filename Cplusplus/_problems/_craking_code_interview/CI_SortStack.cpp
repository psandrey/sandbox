
#include <algorithm>
#include <stack>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class SortStack {
public:
	void sort(stack<int>& st) {
		if (st.empty()) return;

		stack<int> a;
		bool is_sorted = false;
		while (!is_sorted) {
			while (!st.empty() && (a.empty() || st.top() < a.top())) {
				a.push(st.top()); st.pop();
			}

			if (st.empty()) {
				while (!a.empty()) { st.push(a.top()); a.pop(); }
				is_sorted = true;
			} else {
				int x = st.top(); st.pop();
				while (!a.empty() && a.top() < x) { st.push(a.top()); a.pop(); }

				st.push(x);
				while (!a.empty()) { st.push(a.top()); a.pop(); }
			}
		}
	}
};

#if 0
int main(void) {
	SortStack t;
	stack<int> st;
	st.push(5);
	st.push(3);
	st.push(6);
	st.push(4);
	st.push(2);
	st.push(8);
	st.push(7);
	st.push(1);

	t.sort(st);
	while (!st.empty()) {
		cout << st.top() << endl;
		st.pop();
	}

	return 0;
}
#endif