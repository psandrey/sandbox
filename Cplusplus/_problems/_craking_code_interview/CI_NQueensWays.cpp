
/*
 * Write an algorithm to print all ways of arranging eight queens on a chess board so 
 * that none of them share the same row, column or diagonal. 
 */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class NQueensWays {
public:
	int ways(int n) {

		vector<int>st;
		return ways(st, n);
	}

	int ways(vector<int>& st, int n) {
		if (st.size() == n) return 1;

		int w = 0;
		for (int j = 0; j < n; j++) {
			if (valid(st, j) == true) {
				st.push_back(j);
				w += ways(st, n);
				st.pop_back();
			}
		}

		return w;
	}

	bool valid(vector<int>& st, int col) {
		int row = st.size();

		for (int i = 0; i < (int)st.size(); i++) {
			int j = st[i];
			if (i == row || j == col || abs(col - j) == abs(row - i))
				return false;
		}

		return true;
	}
};

#if 0
int main(void) {
	NQueensWays t;

	int w = t.ways(8);
	cout << w << endl;

	return 0;
}
#endif