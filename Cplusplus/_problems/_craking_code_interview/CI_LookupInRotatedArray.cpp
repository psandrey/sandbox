
/*
 * Given a sorted array  of  n  integers  that has been rotated an unknown number  of
 * times, give an O(log n) algorithm that finds an element in the array    You may assume
 * that the array was originally sorted in increasing order
 * EXAMPLE:
 * Input: find 5 in array (15 16 19 20 25 1 3 4 5 7 10 14)
 * Output: 8 (the index of 5 in the array)
 *
 * Note: if the array contains duplicates, there is no way we can apply binary search...
 *       we have to do linear search in this case.
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class LookupInRotatedArray {
public:
	int lookup(vector<int>& a, int x) {
		int n = (int)a.size();
		int t = a[0], h = a[n - 1];

		if (t > h) {// the array is rotated
			if (x >= t) { // look in lohalf
				int lo = 0, hi = n - 1;
				while (lo <= hi) {
					int m = (lo + hi) / 2;
					if (a[m] == x) return m;
					else if (a[m] > t) {
						if (a[m] < x) lo = m + 1;
						else hi = m - 1;
					} else hi = m - 1;
				}
			} else { // look in hihalf
				int lo = 0, hi = n - 1;
				while (lo <= hi) {
					int m = (lo + hi) / 2;
					if (a[m] == x) return m;
					else if (a[m] < h) {
						if (a[m] < x) lo = m + 1;
						else hi = m - 1;
					}
					else lo = m + 1;
				}
			}
		} else { // the array is not rotated
			int lo = 0, hi = n - 1;
			while (lo <= hi) {
				int m = (lo + hi) / 2;
				
				if (a[m] == x) return m;
				else if (a[m] < x) lo = m + 1;
				else hi = m - 1;
			}
		}

		return -1;
	}
};

#if 0
int main(void) {

	vector<int> a = {15, 16, 19, 20, 25, 1, 3, 4, 5, 7, 10, 14};
	int x = 10;

	LookupInRotatedArray t;
	int ans = t.lookup(a, x);
	cout << ans << endl;

	return 0;
}
#endif