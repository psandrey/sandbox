
/*
 * You are given a binary tree in which each node contains a value   Design an algorithm 
 * to print all paths which sum up to that value   Note that it can be any path in the tree 
 * - it does not have to start at the root 
 */

#include <algorithm>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class FindPathWithGivenSumInBT {
public:
	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	// v1
	void find_paths_v1(TreeNode* n, int t) {
		if (n == NULL) return;

		vector<int> st;
		int S = 0;
		find_paths_v1(n, S, t, st);

		find_paths_v1(n->left, t);
		find_paths_v1(n->right, t);
	}

	void find_paths_v1(TreeNode* n, int S, int t, vector<int>& st) {
		if (n == NULL) return;

		st.push_back(n->val);
		S += n->val;
		if (S == t)print_vector(st, 0);

		find_paths_v1(n->left, S, t, st);
		find_paths_v1(n->right, S, t, st);
		st.pop_back();
	}

	// v2
	void find_paths_v2(TreeNode* root, int t) {
		vector<int> st;
		find_paths_v2(root, t, st);
	}

	void find_paths_v2(TreeNode* n, int t, vector<int>& st) {
		if (n == NULL) return;
		st.push_back(n->val);

		int s = 0;
		for (int i = (int)st.size() - 1; i >= 0; i--) {
			s += st[i];
			if (s == t) print_vector(st, i);
		}
		find_paths_v2(n->left, t, st);
		find_paths_v2(n->right, t, st);
		st.pop_back();
	}

	void print_vector(vector<int> a, int j) {
		for (int i = j; i < (int)a.size(); i++)
			cout << a[i] << " ";
		cout << endl;
	}

	TreeNode* build_tree() {
		TreeNode* root = new TreeNode(10);
		root->left = new TreeNode(2);
		root->left->left = new TreeNode(-2);
		root->left->left->right = new TreeNode(1);
		root->left->left->right->left = new TreeNode(-1);

		root->right = new TreeNode(-10);
		root->right->left = new TreeNode(4);
		root->right->left->left = new TreeNode(-4);

		return root;
	}
};

#if 0
int main(void) {
	FindPathWithGivenSumInBT t;
	FindPathWithGivenSumInBT::TreeNode* root = t.build_tree();
	
	cout << " v1:" << endl;
	t.find_paths_v1(root, 0);

	cout << " v2:" << endl;
	t.find_paths_v2(root, 0);
}
#endif