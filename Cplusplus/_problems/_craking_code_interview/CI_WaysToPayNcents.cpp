
#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class WaysToPayNcents {
public:
	int ways(vector<int>& q, int n) {
		return ways(q, 0, n);
	}

	int ways(vector<int>& q, int i, int n) {
		if (n < 0) return 0;
		else if (n == 0) return 1;
		else if (i == q.size()) return 0;

		int take = ways(q, i, n - q[i]);
		int notake = ways(q, i + 1, n);

		return (take + notake);
	}
};

#if 0
int main(void) {
	vector<int> q = { 1, 5, 10, 25 };
	int n = 100;

	WaysToPayNcents t;
	int w = t.ways(q, n);
	cout << w << endl;

	return 0;
}
#endif