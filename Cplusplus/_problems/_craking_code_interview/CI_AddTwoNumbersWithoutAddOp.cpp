
/*
 * Write a function that adds two numbers  You should not use + or any arithmetic operators.
 */

#include <algorithm>
#include <iostream>
using namespace std;

class AddTwoNumbersWithoutAddOp {
public:
	int solve_v1(int x, int y) {
		if (x == 0) return y;
		if (y == 0) return x;

		int ans = 0, c = 0;
		for (int i = 0; i < 32; i++) {
			int a = x & 1, b = y & 1;
			
			int s = a ^ b ^ c;
			c = (c & a) | (c & b) | (a & b);
			ans |= (s << i);

			x >>= 1; y >>= 1;
		}

		return ans;
	}

	int solve_v2(int x, int y) {
		while (y != 0) {
			int c = x & y;	// carry out
			x = x ^ y;		// add w/o carry bit
			y =  (c << 1);	// carry in
		}

		return x;
	}

	int solve_v3(int x, int y) {
		while (y != 0) {
			y--;
			x++;
		}

		return x;
	}
};

#if 0
int main(void) {
	
	AddTwoNumbersWithoutAddOp t;
	int a = 53;
	int b = 27;

	cout << a << " + " << b << " = " << t.solve_v1(a, b) << endl;
	cout << a << " + " << b << " = " << t.solve_v2(a, b) << endl;
	cout << a << " + " << b << " = " << t.solve_v3(a, b) << endl;

	return 0;
}
#endif