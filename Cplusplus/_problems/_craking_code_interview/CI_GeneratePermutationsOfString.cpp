
/* Write a method to compute all permutations of a string. */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class PermutationsOfString {
public:
	vector<string> perm(string a) {

		vector<string> p;
		string first; first.push_back(a[0]);
		p.push_back(first);

		return perm(a, 1, a.size(), p);
	}

	vector<string> perm(string& a, int i, int n, vector<string>& p) {
		if (i == n) return p;

		vector<string> newp;
		for (int j = 0; j < (int)p.size(); j++)
			for (int k = 0; k <= (int)p[j].size(); k++) {
				string b = p[j];
				b.insert(b.begin() + k, a[i]);
				newp.push_back(b);
			}

		return perm(a, i+1, n, newp);
	}
};

#if 0
int main(void) {
	string a = "abcd";

	PermutationsOfString t;
	vector<string> p = t.perm(a);
	for (string s : p)
		cout << s << endl;

	return 0;
}
#endif