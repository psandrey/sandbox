
#include <algorithm>
#include <stack>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class HanoiTowers {
public:

	void hanoi(int n, stack<int>& s, stack<int>& a, stack<int>& d) {
		if (n == 1) {
			d.push(s.top()); s.pop();
		}
		else {
			hanoi(n - 1, s, d, a);
			d.push(s.top()); s.pop();
			hanoi(n - 1, a, s, d);
		}
	}
};

#if 0
int main(void) {
	stack<int> st1, st2, st3;
	st1.push(4);
	st1.push(3);
	st1.push(2);
	st1.push(1);

	HanoiTowers t;
	t.hanoi(4, st1, st2, st3);

	return 0;
}
#endif