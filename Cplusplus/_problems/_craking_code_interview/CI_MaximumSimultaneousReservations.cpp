
/*
 * We are given a list of hotel reservations for one hotel, each with a check-in and a check-out
 * date represented by integers. Find the maximum number of hotel rooms that will be occupied
 * on any day.
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class MaximumSimultaneousReservations {
public:
	int solve(vector<int>& in, vector<int>& out) {
		if (in.empty() || out.empty()) return 0;
		int n = (int) in.size();
		
		sort(in.begin(), in.end());
		sort(out.begin(), out.end());
		
		int i = 0, j = 0, k = 0, m = 0;
		while (i < n && j < n) {
			if (in[i] == out[j]) { i++; j++; }
			else if (in[i] == min(in[i], out[j])) { k++; i++; }
			else { k--; j++; }

			m = max(m, k);
		}

		return m;
	}
};

#if 0
int main(void) {
	vector<int> in = { 1, 2, 3, 2, 6, 5, 1, 5 };
	vector<int> out = { 3, 3, 5, 4, 7, 7, 4, 10 };

	MaximumSimultaneousReservations t;
	cout << t.solve(in, out) << endl;

	return 0;
}
#endif