
/*
 * Write a method to sort an array of strings so that all the anagrams are next to each
 * other
 */

#include <unordered_map>
#include <map>
#include <vector>
#include <algorithm>
#include <string>
#include <iostream>
using namespace std;

class SortAnagrams {
public:

	// v1: anagrams are near eachtoher, but there is no relation between non-anagrams
	vector<string> solve_v1(vector<string>& a) {
		unordered_map<string, vector<int>> hm;
		for (int i = 0; i < (int)a.size(); i++) {
			string key = a[i];
			sort(key.begin(), key.end());
			hm[key].push_back(i);
		}

		vector<string> ans;
		for (unordered_map<string, vector<int>>::iterator it = hm.begin(); it != hm.end(); it++) {
			for (int i = 0; i < it->second.size(); i++) {
				ans.push_back(a[it->second[i]]);
			}
		}

		return ans;
	}

	// v2: sort for non-anagrams as well
	vector<string> solve_v2(vector<string>& a) {

		struct {
			bool operator()(const string& a, const string& b) {
				string k1(a), k2(b);
				sort(k1.begin(), k1.end());
				sort(k2.begin(), k2.end());
				return k1 < k2;
			}
		}comp;

		sort(a.begin(), a.end(), comp);

		return a;
	}
};

#if 0
int main(void) {
	vector<string> a = { "zxy", "abcd", "xyz", "dbca", "klj", "acdb" };

	SortAnagrams t;
	vector<string> ans1 = t.solve_v1(a);
	for (string s : ans1)
		cout << s << endl;
	
	cout << endl;

	vector<string> ans2 = t.solve_v2(a);
	for (string s : ans2)
		cout << s << endl;

	return 0;
}
#endif