
/*
 * Given an array a of n integers and q querries find the number of distinct numbers for each querry.
 * Each querry is given as l, r indexes and the distinct numbers are counted including boundaries.
 */

#include <unordered_map>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class QuerriesForDistinctNumberInSubarray {
public:
	vector<int> count_distinct(vector<int>& a, vector<int>& l, vector<int>& r) {
		int q = (int)l.size();
		int n = (int)a.size();

		vector<int> distinct(q, 0);
		unordered_map<int, int> mp;
		for (int j = 0; j < n; j++) {
			int last = -1;
			if (mp.find(a[j]) != mp.end()) last = mp[a[j]];
			for (int i = 0; i < q; i++) {
				if (last >= l[i] && last <= r[i]) continue; // already counted
				if (j >= l[i] && j <= r[i]) distinct[i]++;
			}
			mp[a[j]] = j;
		}

		return distinct;
	}
};

#if 0
int main(void) {
#if 0
	vector<int> a = { 1, 1, 2, 1, 3 };
	vector<int> l = { 0, 1, 2 };
	vector<int> r = { 4, 3, 4 };
#else
	vector<int> a = { 1, 2, 3, 1,   2, 3, 2, 1, 1,    5, 5 };
	vector<int> l = { 0, 4 };
	vector<int> r = { 10, 8 };
#endif

	QuerriesForDistinctNumberInSubarray t;
	vector<int> ans = t.count_distinct(a, l, r);
	for (int x : ans) cout << x << " ";
	cout << endl;

	return 0;
}
#endif