
// Google codejam 2019, Qualify
// https://codingcompetitions.withgoogle.com/codejam/round/0000000000051705/00000000000881da

#include <queue>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

namespace CodeJam2019_Q_YouCanGoYourOwnWay {
	enum Color { E = 1, S = 2 };

	class YouCanGoYourOwnWay {
	public:
		// backtracking version: T(n) = O(2nCn)
		string solve_v1(int n, string lynda) {
			vector<vector<int>> lynda_moves(n, vector<int>(n, 0));
			int i = 0, j = 0;
			for (int k = 0; k < (int)lynda.size(); k++) {
				if (lynda[k] == 'E') lynda_moves[i][j++] = E;
				else lynda_moves[i++][j] = S;
			}

			string ans = "";
			string path;
			find_path(0, 0, n, path, ans, lynda_moves);

			return ans;
		}

		void find_path(int i, int j, int n, string& path, string& ans, vector<vector<int>>& lynda_moves) {
			if (i == n - 1 && j == n - 1) { ans = path; return; }

			if (ans.empty() && j + 1 < n && lynda_moves[i][j] != E) {
				path.push_back('E');
				find_path(i, j + 1, n, path, ans, lynda_moves);
				path.pop_back();
			}

			if (ans.empty() && i + 1 < n && lynda_moves[i][j] != S) {
				path.push_back('S');
				find_path(i + 1, j, n, path, ans, lynda_moves);
				path.pop_back();
			}
		}

		// BFS version: T(n) = O(n^2)
		string solve_v2(int n, string lynda) {
			vector<vector<int>> lynda_moves(n, vector<int>(n, 0));
			int i = 0, j = 0;
			for (int k = 0; k < (int)lynda.size(); k++) {
				if (lynda[k] == 'E') lynda_moves[i][j++] = E;
				else lynda_moves[i++][j] = S;
			}

			vector<vector<pair<int, int>>> path(n, vector<pair<int, int>>(n));
			queue<pair<int, int>> q;
			q.push(make_pair(0, 0));
			while (!q.empty()) {
				pair<int, int> p = q.front();  q.pop();
				i = p.first; j = p.second;

				if (i == n - 1 && j == n - 1) break;

				if (j + 1 < n && lynda_moves[i][j] != E) {
					path[i][j + 1] = make_pair(i, j);
					q.push(make_pair(i, j + 1));
				}

				if (i + 1 < n && lynda_moves[i][j] != S) {
					path[i + 1][j] = make_pair(i, j);
					q.push(make_pair(i + 1, j));
				}
			}

			// build path
			string ans = "";
			i = n - 1; j = n - 1;
			while(i > 0 || j > 0){
				pair<int, int> p = path[i][j];
				if (p.first + 1 == i) ans.insert(ans.begin(), 'S');
				else ans.insert(ans.begin(), 'E');

				i = p.first; j = p.second;
			}

			return ans;
		}

		// T(n) = O(n)
		string solve_v3(int n, string lynda) {

			string ans = "";
			for (int k = 0; k < (int)lynda.size(); k++)
				if (lynda[k] == 'E') ans.push_back('S');
				else ans.push_back('E');

			return ans;
		}
	};
}

#if 0
int main(void) {

	string lynda = "SEEESSES";
	int n = 5;

	CodeJam2019_Q_YouCanGoYourOwnWay::YouCanGoYourOwnWay t;
	string ans = t.solve_v2(n, lynda);
	cout << ans << endl;

	ans = t.solve_v3(n, lynda);
	cout << ans << endl;

	return 0;
}
#endif