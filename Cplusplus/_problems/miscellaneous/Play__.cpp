
#include <algorithm>
#include <stack>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class play {
public:
	int dfind(int u, vector<int>& p) {
		if (p[u] != u) p[u] = dfind(p[u], p);

		return p[u];
	}

	void dunion(int u, int v, vector<int>& r, vector<int>& p) {
		int pu = dfind(u, p);
		int pv = dfind(v, p);
		if (r[pu] > r[pv]) { p[pv] = pu; }
		else if (r[pu] < r[pv]) { p[pu] = pv; }
		else { p[pv] = pu; r[pu]++; }
	}

	int solve(vector<int>& c, vector<vector<int>>& edge) {
		int n = (int)c.size();
		int m = (int)edge.size();

		long long ans = LLONG_MAX;
		for (int i = 0; i < m - 1; i++)
			for (int j = i; j < m; j++) {
				vector<int> r(n), p(n);
				for (int k = 0; k < n; k++) { r[k] = 0; p[k] = k; }

				for (int k = 0; k < m; k++) {
					if (k == i || k == j) continue;
					int u = edge[k][0] - 1;
					int v = edge[k][1] - 1;
					if (dfind(u, p) != dfind(v, p)) dunion(u, v, r, p);
				}

				// roots of every component
				int u = edge[i][0] - 1, v = edge[i][1] - 1, q = edge[j][0] - 1;
				if (dfind(u, p) == dfind(q, p) || dfind(v, p) == dfind(q, p)) q = edge[j][1] - 1;
				u = dfind(u, p); v = dfind(v, p); q = dfind(q, p);

				long long s1 = 0, s2 = 0, s3 = 0;
				for (int i = 0; i < n; i++) {
					int id = dfind(i, p);

					if (id == u) s1 += c[i];
					else if (id == v) s2 += c[i];
					else if (id == q) s3 += c[i];
				}

				if (s1 == s2 && s1 - s3 > 0LL) ans = min(ans, s1 - s3);
				else if (s1 == s3 && s1 - s2 > 0LL) ans = min(ans, s1 - s2);
				else if (s2 == s3 && s2 - s1 > 0LL) ans = min(ans, s2 - s1);
			}

		return (ans == LLONG_MAX ? -1 : (int)ans);
	}
};

#if 0
int main(void) {
	vector<int> c = { 100, 100, 99, 99, 98, 98 };
	vector<vector<int>> edges = { {1, 3}, {3, 5}, {1, 2}, {2, 4}, {4, 6} };

	play t;
	int ans = t.solve(c, edges);
	cout << ans << endl;
}
#endif