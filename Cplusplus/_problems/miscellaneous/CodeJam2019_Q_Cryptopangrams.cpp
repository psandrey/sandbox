
// Google codejam 2019, Qualify
// https://codingcompetitions.withgoogle.com/codejam/round/0000000000051705/000000000008830b
// Note: You have to use GMP to use big numbers.

#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

namespace CodeJam2019_Q_Cryptopangrams {
	class Cryptopangrams {
	public:
		int gcd(int a, int b) {
			if (a < b) { int t = a; a = b; b = t; }
			int r = a % b;
			while (r != 0) {
				a = b; b = r;
				r = a % b;
			}

			return b;
		}

		string solve(int m, vector<int> coded) {
			int n = (int)coded.size();
			int a0, a1;
			a1 = gcd(coded[0], coded[1]);
			a0 = coded[0] / a1;

			unordered_set<int> hc;
			hc.insert(a0);
			int p = a0;
			for (int j = 0; j < n; j++) {
				p = coded[j] / p;
				hc.insert(p);
			}
			int const ALPHA = hc.size();
			vector<int> codes(ALPHA); int k = 0;
			for (auto it = hc.begin(); it != hc.end(); it++) codes[k++] = *it;
			sort(codes.begin(), codes.end());
			unordered_map<int, int> mc;
			for (int j = 0; j < ALPHA; j++) mc[codes[j]] = j;

			string ans(n + 1, 0);
			char c = 'A' + mc[a0];
			ans[0] = c;
			
			p = a0;
			for (int j = 0; j < n; j++) {
				p = coded[j] / p;
				c = 'A' + mc[p];
				ans[j + 1] = c;
			}

			return ans;
		}
	};

};

#if 0
int main(void) {
	vector<int> coded = { 217, 1891, 4819, 2291, 2987, 3811, 1739, 2491, 4717, 445, 65, 1079, 8383, 5353, 901, 187, 649, 1003, 697, 3239, 7663, 291, 123, 779, 1007, 3551, 1943, 2117, 1679, 989, 3053 };
	
	CodeJam2019_Q_Cryptopangrams::Cryptopangrams t;
	string ans = t.solve(103, coded);
	cout << ans << endl;

	return 0;
}
#endif