
#if 0
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int min_square(int n, int m) {
	int ans = INT_MAX;
	if (n == 0 || m == 0) ans = 0;
	else if (n == m) ans = 1;
	else if (n == 1) return 1 + min_square(n, m - 1);
	else if (n > m) ans = min_square(m, n);
	else {
		for (int j = n; j >= 0; j--) {
			int nw = min_square(j, j);
			int ne = min_square(n - j, j);
			int se = min_square(n - j, m - j);
			int sw = min_square(j, m - j);
			ans = min(ans, nw + ne + se + sw);
		}
	}

	return ans;
}

int main(void) {
	int m = 11, n = 13;
	cout << min_square(m, n);
	return 0;
}
#endif