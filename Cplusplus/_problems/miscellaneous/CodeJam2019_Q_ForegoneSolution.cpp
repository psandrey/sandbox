
// Google codejam 2019, Qualify
// https://codingcompetitions.withgoogle.com/codejam/round/0000000000051705/0000000000088231

#include <queue>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

namespace CodeJam2019_Q_ForegoneSolution {
	class ForegoneSolution {
	public:
		pair<int, int> solution(int n) {
			int a = 0, b = 0, p = 1;
			while (n > 0) {
				int d = n % 10;
				int d1 = d, d2 = 0;
				if (d == 4) { d1 = 2; d2 = 2; }

				a += d1 * p; b += d2 * p;
				n = n / 10; p *= 10;
			}

			return make_pair(a, b);
		}
	};
};

#if 0
int main(void) {
	int n = 111;

	CodeJam2019_Q_ForegoneSolution::ForegoneSolution t;
	pair<int, int> ans = t.solution(n);
	cout << ans.first << " + " << ans.second << endl;

	return 0;
}
#endif