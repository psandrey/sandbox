
#include <unordered_set>
#include <vector>
#include <iostream>
#include <algorithm>
#include <functional>
#include <climits>
using namespace std;

#if 0
class Source1 {
public:
	bool is_shine(vector<bool>& on, vector<bool>& shine) {
		int n = (int)on.size();
		
		bool prev = false;
		bool at_least_one = false;
		for (int i = 0; i < n; i++) {
			if (on[i] == false) break;

			if (i == 0) {
				if (on[0] && !shine[0]) {
					shine[0] = true; at_least_one = true;
				}
			} else {
				if (on[i - 1] && on[i] && !shine[i]) {
					shine[i] = true; at_least_one = true;
				}
			}
		}

		// I assume can dublicate bulbs may turn on
		// and assume these duplicates must not be counted twice
		if (at_least_one == false) return false;

		bool count = true;
		for (int i = 0; i < n; i++)
			if (on[i] && !shine[i]) count = false;

		return count;
	}

	int solution(vector<int>& a) {
		int n = (int)a.size();
		if (n == 0 || n == 1) return 0;

		vector<bool> on(a.size(), false);
		vector<bool> shine(a.size(), false);

		int c = 0;
		for (int i = 0; i < n; i++) {
			int b = a[i] - 1;
			on[b] = true;

			if (is_shine(on, shine)) c++;
		}

		return c;
	}
};
#endif

#if 0
class Source2 {
public:
	int solution(vector<int>& A, int X) {
		int N = A.size();
		if (N == 0) {
			return -1;
		}
		int l = 0;
		int r = N - 1;
		while (l < r) {
			int m = (l + r) / 2;
			if (A[m] >= X) {
				r = m;
			}
			else {
				l = m + 1;
			}
		}
		if (A[l] == X) {
			return l;
		}
		return -1;
	}
};
#endif

#if 0
class Source3 {
public:
	int max_apple(vector<int>& a, int s, int e, int k) {
		int ans = 0, c = 0, i = s;
		for (int j = s; j <= e; j++) {
			c += a[j];
			if (j - i + 1 > k) { c -= a[i]; i++; }
			if (j - i + 1 == k) ans = max(ans, c);
		}

		return ans;
	}

	int solution(vector<int>& a, int k, int l) {
		int n = (int)a.size();
		if (k + l > n) return -1;

		int alice = 0, ans = 0, i = 0;
		for (int j = 0; j < n; j++) {
			alice += a[j];

 			if (j - i + 1 > k) { alice -= a[i]; i++; }
			if (j - i + 1 == k) {
				int bob = -1;
				if (i >= l)
					bob = max_apple(a, 0, i - 1, l);
				if (n - j + 1 >= l)
					bob = max(bob, max_apple(a, j + 1, n - 1, l));

				if (bob != -1)
					ans = max(ans, alice + bob);
			}
		}

		return ans;
	}
};
#endif

#if 0
int main(void) {
#if 0
	//vector<int> a = { 2, 1, 3, 5, 4 }; // 3
	vector<int> a = { 2, 3, 4, 1, 5 }; // 2
	//vector<int> a = { 1, 3, 4, 2, 5 }; // 3

	Source1 t;
	cout << t.solution(a) << endl;
#endif

#if 0
	vector<int> a = { 1, 2 };
	int x = 1;

	Source2 t;
	cout << t.solution(a, x) << endl;
#endif

#if 0
	Source3 t;
	vector<int> a = { 6, 1, 4, 6, 3, 2, 7, 4 };
	int k = 3;
	int l = 2;

	cout << t.solution(a, k, l) << endl;
#endif
	return 0;
}
#endif