
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class combinatorics {
public:
	vector<vector<int>> gen_per(int n) {
		vector<vector<int>> pp;
		if (n == 0) return pp;

		vector<int> p;
		for (int i = 1; i <= n; i++) p.push_back(i);

		pp.push_back(p);
		if (n == 1) return pp;;

		while (true) {
			// find the largest index such that p[k] < p[k + 1]
			int k = n - 2;
			while (k >= 0 && p[k] > p[k + 1]) k--;
			if (k < 0) break;  // we hit the last permutation

			// find the largest index such that p[k] < p[l]
			int l = n - 1;
			while (p[k] > p[l]) l--;

			swap(p[k], p[l]);

			// reverse p[k + 1 ... n]
			int i = k + 1, j = n - 1;
			while (i < j) { swap(p[i], p[j]); i++; j--; }

			pp.push_back(p);
		}

		return pp;
	}

	// debug
	void print_v(vector<int>& a) {
		for (int j = 0; j < (int)a.size(); j++)
			cout << a[j] << ", ";
		cout << endl;
	}

	void print_vv(vector<vector<int>>& a) {
		if (a.empty()) return;

		cout << a.size() << endl;
		for (int i = 0; i < (int)a.size(); i++) {
			for (int j = 0; j < (int)a[i].size(); j++)
				cout << a[i][j] << ", ";
			cout << endl;
		}
	}
};

#if 0
int main(void) {
	combinatorics t;

	vector<vector<int>> pp = t.gen_per(4);
	t.print_vv(pp);

	return 0;
}
#endif