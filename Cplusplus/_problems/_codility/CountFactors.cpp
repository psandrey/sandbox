
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class CountFactors {
public:
	int solution(int n) {
		if (n == 1) return 1;
		int i = 0, m = (int)sqrt(n), ans = 0;
		for (i = 1; i <= m; i++)
			if (n % i == 0) ans += 2;

		// if n is perfect sqrt, then the last divider is counted twice
		if ((i - 1)* (i - 1) == n) ans -= 1; 
		return ans;
	}
};

#if 0
int main(void) {
	int x = 6;

	CountFactors t;
	cout << t.solution(x) << endl;

	return 0;
}
#endif