
#include <vector>
#include <iostream>
#include <functional>
#include <climits>
#include <algorithm>
using namespace std;

class MinAvgTwoSlice  {
public:
	typedef long long ll;
	int solution(vector<int>& a) {
		int n = (int)a.size();

		int ans = 0;
		ll s2 = 0, s3 = 0;
		double avg = INT_MAX, avg2 = 0.0, avg3 = 0.0;
		for (int j = 0; j < n; j++) {
			s2 += a[j]; s3 += a[j];

			if (j >= 2) s2 -= a[j - 2];
			if (j >= 3) s3 -= a[j - 3];

			if (j >= 1) avg2 = s2 / 2.0;
			if (j >= 2) avg3 = s3 / 3.0;

			if (j >= 1 && avg > avg2) { avg = avg2; ans = j - 1; }
			if (j >= 2 && avg > avg3) { avg = avg3; ans = j - 2; }
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = { 4, 2, 2, 5, 1, 5, 8 };

	MinAvgTwoSlice t;
	cout << t.solution(a) << endl;

	return 0;
}
#endif