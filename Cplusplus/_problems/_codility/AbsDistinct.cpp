
#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class AbsDistinct {
public:
	bool find_target(vector<int>& a, int k, int t) {
		int lo = 0, hi = k;
		while (lo <= hi) {
			int m = (lo + hi) / 2;

			if (a[m] < t) lo = m + 1;
			else if (a[m] > t) hi = m - 1;
			else return true;
		}

		return false;
	}

	int solution(vector<int>& a) {
		int n = (int)a.size();

		int c = 0;
		for (int i = 0; i < n; i++) {
			if (i == 0) c++;
			else if (a[i] != a[i - 1] 
					 && !find_target(a, i - 1, a[i])
					 && !find_target(a, i - 1, -a[i])) c++;
		}

		return c;
	}
};

#if 0
int main(void) {
	vector<int> a = { -4 , -4 , -2, 0 ,  1, 3, 4, 4 };

	AbsDistinct t;
	cout << t.solution(a) << endl;

	return 0;
}
#endif