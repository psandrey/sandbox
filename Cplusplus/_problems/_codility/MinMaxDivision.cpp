
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class MinMaxDivision {
public:
	int get_partitions(vector<int>& a, int S) {
		int p = 1, s = 0, n = (int)a.size();

		for (int i = 0; i < n; i++) {
			if (a[i] > S) return 0;

			if (s + a[i] <= S) s += a[i];
			else { p++; s = a[i]; }
		}

		return p;
	}

	int solution(int k, int m, vector<int>& a) {
		if (k == 0 || a.empty()) return 0;

		int n = (int)a.size();
		int lo = 0, hi = 0;
		for (int i = 0; i < n; i++) hi += a[i];

		int ans = hi;
		while (lo <= hi) {
			int m = (lo + hi) / 2;
			int p = get_partitions(a, m);
			if (p > 0 && p <= k) { ans = m; hi = m - 1; }
			else lo = m + 1;
		}

		return ans;
	}
};

#if 0
int main(void) {

	vector<int> a = { 2, 1, 5, 1, 2, 2, 2 };
	int k = 3;
	int m = 5;

	MinMaxDivision t;
	cout << t.solution(k , m, a) << endl;

	return 0;
}
#endif