
#include <algorithm>
#include <iostream>
#include <functional>
using namespace std;

class CountDiv {
public:
	typedef long long ll;
	int solution(int a, int b, int k) {
		if (k == 1) return (b - a + 1);

		int first = (int)ceil((double)a / (double)k);
		int second = (int)floor((double)b / (double)k);
		first *= k; second *= k;
		int d = second - first;

		if (d < 0) return 0;
		return (d / k) + 1;
	}
};

#if 0
int main(void) {
	int a = 6;
	int b = 11;
	int k = 2;

	CountDiv t;
	cout << t.solution(a, b, k) << endl;

	return 0;
}
#endif