
#include <unordered_set>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class NumberSolitaire {
public:
	int sol(int k, vector<int>& a, vector<int>& dp) {
		if (dp[k] != INT_MIN) return dp[k];

		if (k == 0) dp[k] = a[0];
		else {
			int limit = min(k, 6);
			int ans = INT_MIN;
			for (int i = limit; i > 0; i--) {
				int p = sol(k - i, a, dp) + a[k];
				ans = max(ans, p);
			}

			dp[k] = ans;
		}

		return dp[k];
	}

	int solution(vector<int>& a) {
		int n = (int)a.size();
		vector<int> dp(a.size(), INT_MIN);
		return sol(n - 1, a, dp);
	}
};

#if 0
int main(void) {
	vector<int> a = { 1, -2, 0, 9, -1, -2 };

	NumberSolitaire t;
	cout << t.solution(a) << endl;

	return 0;
}
#endif