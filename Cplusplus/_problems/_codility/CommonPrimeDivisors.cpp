#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class CommonPrimeDivisors {
public:
	int gcd(int a, int b) {
		if (b == 0) return a;
		if (b > a) return gcd(b, a);
		return gcd(b, a % b);
	}

	int remove_common(int x, int y) {
		int g = 1;
		while ((g = gcd(x, y)) != 1) { x /= g; }

		return x;
	}

	// passed all the tests
	// T(n) = O(n * log^2(max { a[i], b[i] }) )
	int solution_v1(vector<int>& a, vector<int>& b) {
		int n = (int)a.size(), c = 0;
		for (int i = 0; i < n; i++) {
			int g = gcd(a[i], b[i]);
			int x = remove_common(a[i], g);
			int y = remove_common(b[i], g);
			if (x == 1 && y == 1) c++;
		}

		return c;
	}

	// time out one test
	// T(n) = O(n * max { a[i], b[i] } )
	int solution_v2(vector<int>& a, vector<int>& b) {
		int n = (int)a.size();
		int M = 0;
		for (int i = 0; i < n; i++) M = max(M, max(a[i], b[i]));

		int m = (int)sqrt(M);
		vector<bool> sieve(m + 1, true);
		vector<int> primes;
		for (int i = 2; i <= m; i++) {
			if (sieve[i] == true) {
				primes.push_back(i);
				for (int j = 2 * i; j <= m; j += i) sieve[j] = false;
			}
		}

		int c = 0;
		for (int i = 0; i < n; i++) {
			int x = min(a[i], b[i]);
			int y = max(a[i], b[i]);
			int m = (int)sqrt(x);

			bool same = true;
			for (int j = 0; j < (int)primes.size(); j++){
				int p = primes[j];
				if (p > m) break;

				if      (x % p == 0 && y % p != 0) { same = false;  break; }
				else if (y % p == 0 && x % p != 0) { same = false;  break; }
				else {
					while (x % p == 0) x /= p;
					while (y % p == 0) y /= p;
				}
			}

			if (same == true) {
				// handle the case where x is prime number
				if (x > 1 && y > 1) {
					while (y % x == 0) y /= x;
					x /= x;
				}

				if (x == 1 && y == 1) c++;
			}
		}

		return c;
	}
};

#if 0
int main(void) {
	vector<int> a = {15, 10, 9};
	vector<int> b = { 75, 30, 5 };

	CommonPrimeDivisors t;
	cout << t.solution_v1(a, b) << endl;	
	cout << t.solution_v2(a, b) << endl;

	return 0;
}
#endif