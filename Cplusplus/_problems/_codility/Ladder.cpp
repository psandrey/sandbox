
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class Ladder {
public:

	typedef long long int ll;
	vector<int> solution(vector<int>& a, vector<int>& b) {
		int n = a.size();
		int M = 0;
		for (int x : a) M = max(M, x + 1);

		vector<int> f; f.push_back(0); f.push_back(1);
		ll mask = (1LL << 31) - 1LL;
		for (int i = 2; i <= M; i++) {
			ll x = f[f.size() - 1], y = f[f.size() - 2];
			ll z = (x + y) & mask;
			f.push_back((int)z);
		}

		vector<int> ans(n);
		for (int i = 0; i < n; i++) {
			ll mod = (1LL << b[i]) - 1LL;
			ans[i] = f[a[i] + 1] & (int)mod;
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = { 4, 4, 5, 5, 1 };
	vector<int> b = { 3, 2, 4, 3, 1 };

	Ladder t;
	vector<int> ans = t.solution(a, b);
	for (int x : ans)
		cout << x << " ";
	cout << endl;

	return 0;
}
#endif