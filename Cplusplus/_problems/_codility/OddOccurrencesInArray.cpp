
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class OddOccurrencesInArray {
public:
	int solution(vector<int>& a) {
		if (a.size() == 1) return a[0];

		int ans = 0;
		for (int pos = 0; pos <= 30; pos++) {
			int mask = (1 << pos), bit = 0;
			for (int i = 0; i < (int)a.size(); i++) {
				int x = (a[i] & mask);
				bit ^= x;
			}

			ans |= bit;
		}

		return ans;
	}

};

#if 0
int main(void) {
	vector<int> a = { 9, 3, 9, 3, 9, 7, 9 };

	OddOccurrencesInArray t;
	cout << t.solution(a) << endl;

	return 0;
}
#endif