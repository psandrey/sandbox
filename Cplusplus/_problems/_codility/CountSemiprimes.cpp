
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class CountSemiprimes {
public:
	vector<int> solution(int n, vector<int>& p, vector<int>& q) {
		vector<bool> sieve(n + 1, true); sieve[0] = false; sieve[1] = false;
		for (int i = 2; i <= n; i++) {
			if (sieve[i] == false) continue;

			for (int j = 2 * i; j <= n; j += i) sieve[j] = false;
		}

		vector<int> semi(n + 1, 0);
		for (int i = 4; i <= n; i++) {
			int m = (int)sqrt(i);
			for (int j = 2; j <= m; j++) {
				if (i % j == 0 && sieve[j] && sieve[i / j]) {
					semi[i] = 1; break;
				}
			}
		}

		for (int i = 1; i <= n; i++) semi[i] += semi[i - 1];

		int m = (int)p.size();
		vector<int>ans(m);
		for (int i = 0; i < m; i++) {
			ans[i] = semi[q[i]] - semi[p[i] - 1];
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> p = { 1, 4, 16 };
	vector<int> q = { 26, 10, 20 };
	int n = 26;

	CountSemiprimes t;
	vector<int> ans = t.solution(n, p, q);
	for (int x : ans) cout << x << " ";
	cout << endl;

	return 0;
}
#endif