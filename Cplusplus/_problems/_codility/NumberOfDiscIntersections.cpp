
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class NumberOfDiscIntersections {
public:
	typedef long long int ll;
	int solution(vector<int>& a) {
		int n = (int)a.size();
		vector<ll> start(n), end(n);
		for (int i = 0; i < n; i++) {
			start[i] = (ll)i - a[i];
			end[i] = (ll)i + a[i];
		}

		sort(start.begin(), start.end());
		sort(end.begin(), end.end());

		ll c = 1, ans = 0;
		int i = 0;
		for (int j = 0; j < n; j++) {
			while (i < n - 1 && start[i + 1] <= end[j]) { c++; i++; }

			ans += (c - 1);
			c--;

			if (ans > 10000000LL) return -1;
		}

		return ans;
	}
};

#if 0
int main() {
	vector<int> a = { 1, 5, 2, 1, 4, 0 };

	NumberOfDiscIntersections t;
	cout << t.solution(a) << endl;

	return 0;
}
#endif