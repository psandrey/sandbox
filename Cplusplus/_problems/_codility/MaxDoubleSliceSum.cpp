
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class MaxDoubleSliceSum {
public:
	int solution(vector<int>& a) {
		int n = (int)a.size();
		if (n <= 3) return 0;

		vector<int> left(n, 0);
		for (int i = 1; i < n - 1; i++)
			left[i] = max(0, left[i - 1] + a[i]);
		
		vector<int> right(n, 0);
		for (int i = n - 2; i > 0; i--)
			right[i] = max(0, right[i + 1] + a[i]);

		int ans = 0;
		for (int k = 1; k < n - 1; k++)
			ans = max(ans, left[k - 1] + right[k + 1]);

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = {3, 2, 6, -1, 4, 5, -1, 2};

	MaxDoubleSliceSum t;
	cout << t.solution(a) << endl;

	return 0;
}
#endif