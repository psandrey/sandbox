
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class Peaks {
public:
	bool split(vector<int>& peeks, int k) {
		int n = (int)peeks.size();
		for (int i = k; i <= n; i += k) {
			int l = i - k, r = i - 1;
			if (peeks[r] >= l) continue;

			return false;
		}

		return true;
	}

	int solution(vector<int>& a) {
		int n = (int)a.size();
		if (n < 3) return 0;

		int npeeks = 0;
		vector<int> peeks(n); peeks[0] = -1;
		for (int i = 1; i < n - 1; i++) {
			peeks[i] = peeks[i - 1];
			if (a[i - 1] < a[i] && a[i] > a[i + 1]) { peeks[i] = i; npeeks++; }
		}
		peeks[n - 1] = peeks[n - 2];
		if (npeeks == 0) return 0;

		int i = 1, m = (int)sqrt(n);
		int max_blocks = 0;
		for (i = 1; i <= m; i++) {
			if (n % i == 0) {
				if (split(peeks, i) == true) {
					max_blocks = max(max_blocks, n / i);
					break;
				}
				if (split(peeks, n / i) == true) max_blocks = max(max_blocks, i);
			}
		}

		return max_blocks;
	}
};

#if 0
int main(void) {
	//vector<int> a = { 1, 2, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2 };
	//vector<int> a = { 1, 3, 2,1 };
	//vector<int> a = { 1, 2, 1, 2, 1, 2, 1, 2, 1 };
	vector<int> a = {0, 1, 0, 0, 1, 0, 0, 1, 0 };


	Peaks t;
	cout << t.solution(a) << endl;

	return 0;
}
#endif