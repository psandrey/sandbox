
#include <vector>
#include <iostream>
#include <functional>
#include <climits>
#include <algorithm>
using namespace std;

class PermCheck {
public:
	int solution(vector<int>& a) {
		int n = (int)a.size();
		vector<int> c(n, 0);
		for (int i = 0; i < n; i++) {
			if (a[i] <= 0 || a[i] > n) return 0;
			if (c[a[i] - 1] > 0) return 0;
			c[a[i] - 1] = 1;
		}

		return 1;
	}
};

#if 0
int main(void) {
	vector<int> a = { 4, 1, 3, 2 };

	PermCheck t;
	cout << t.solution(a) << endl;

	return 0;
}
#endif