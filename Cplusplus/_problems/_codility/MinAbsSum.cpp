
#include <unordered_set>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class MinAbsSum {
public:
	int sol(int k, int s, vector<int>& a) {
		if (k == 0) return s;

		int l = abs(sol(k - 1, s + a[k - 1], a));
		int r = abs(sol(k - 1, s - a[k - 1], a));
		return min(l, r);
	}

	int solution(vector<int>& a) {
		return sol(a.size(), 0, a);
	}

};

#if 0
int main(void) {
	vector<int> a = { 1, 5, 2, -2 };

	MinAbsSum t;
	cout << t.solution(a) << endl;

	return 0;
}
#endif