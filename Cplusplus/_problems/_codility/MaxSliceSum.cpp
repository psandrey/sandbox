
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;
	
class MaxSliceSum {
public:
	typedef long long int ll;
	int solution(vector<int>& a) {
		int n = (int)a.size();

		ll m = a[0], s = a[0], ans = a[0];
		for (int i = 1; i < n; i++) {
			s += a[i];
			ans = max(ans, max(s, s - m));
			m = min(m, s);
		}

		return (int)ans;
	}
};

#if 0
int main(void) {

	vector<int> a = { 3, 2, -6, 4, 0 };
 
	MaxSliceSum t;
	cout << t.solution(a) << endl;
	return 0;
}
#endif