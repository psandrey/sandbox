
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class NailingPlanks {
public:
	bool nails(vector<pair<int, int>>& p, vector<pair<int, int>>& s, int k) {
		int n = (int)p.size();
		int m = (int)s.size();
		int i = 0;
		for (int j = 0; j < m; j++) {
			if (s[j].second > k) continue;

			while (i < n && p[i].first <= s[j].first && p[i].second >= s[j].first) i++;
			if (i == n) return true;
		}

		return false;
	}

	int solution(vector<int>& a, vector<int>& b, vector<int>& c) {
		int n = (int)a.size();
		vector<pair<int, int>> p(n);
		for (int i = 0; i < n; i++) p[i] = make_pair(a[i], b[i]);

		int m = (int)c.size();
		vector<pair<int, int>> s(m);
		for (int i = 0; i < m; i++) s[i] = make_pair(c[i], i);
		
		sort(p.begin(), p.end());
		sort(s.begin(), s.end());

		int lo = 0, hi = m - 1, ans = -1;
		while (lo <= hi) {
			int med = (lo + hi) / 2;
			bool rem = nails(p, s, med);
			if (rem == false) lo = med + 1; // need more nails
			else { ans = med, hi = med - 1; } // there are enough nails for all planks
		}
		
		return (ans == - 1 ? ans : (ans + 1));
	}
};

#if 0
int main(void) {
	vector<int> a = { 1, 4, 5, 8 };
	vector<int> b = { 4, 5, 9, 10 };
	vector<int> c = { 5, 7, 10, 2 };

	NailingPlanks t;
	cout << t.solution(a, b, c) << endl;

	return 0;
}
#endif