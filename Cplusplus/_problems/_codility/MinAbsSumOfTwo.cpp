
#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class MinAbsSumOfTwo {
public:
	typedef long long int ll;
	int get_closest(vector<int>& a, int t) {
		int n = (int)a.size();
		int lo = 0, hi = n - 1;

		ll md = LLONG_MAX;
		int ans = -1;
		while (lo <= hi) {
			int m = (lo + hi) / 2;
			
			ll d = abs(((ll)a[m] - (ll)t));
			if (md > d) { md = d; ans = a[m]; }

			if (a[m] == t) return a[m];
			else if (a[m] < t) lo = m + 1;
			else if (a[m] > t) hi = m - 1;
		}

		return ans;
	}

	int solution(vector<int>& a) {
		int n = (int)a.size();
		if (n == 1) return (abs(2 * a[0]));

		ll ans = LLONG_MAX;
		sort(a.begin(), a.end());
		for (int i = 0; i < n; i++) {
			int t = -a[i];
			int closest = get_closest(a, t);
			ans = min(ans, abs(((ll)a[i] + (ll)closest)));
		}

		return (int)ans;
	}
};

#if 0
int main(void) {
	vector<int> a = {-8, 4, 5, -10, 3};

	MinAbsSumOfTwo t;
	cout << t.solution(a) << endl;

	return 0;
}
#endif