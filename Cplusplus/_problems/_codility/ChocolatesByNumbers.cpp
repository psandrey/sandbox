
#include <iostream>
using namespace std;

class ChocolatesByNumbers {
public:
    typedef long long int ll;
    int gcd(int a, int b) {
        if (b == 0) return a;
        if (b > a) return gcd(b, a);
        return gcd(b, a % b);
    }

    int solution(int n, int m) {
        ll g = (ll)gcd(n, m);
        ll l = ((ll)n * m) / g;
        ll ans = l / m;
        return (int)ans;
    }
};

#if 0
int main(void) {
    int n = 10;
    int m = 4;

    ChocolatesByNumbers t;
    cout << t.solution(n, m) << endl;

	return 0;
}
#endif