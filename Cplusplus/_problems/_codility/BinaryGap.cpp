
#include <algorithm>
#include <iostream>
using namespace std;

class BinaryGap {
public:
	int solution(int n) {
		int gap = 0, zeroes = 0;
		bool start_gap = false;
		while (n!= 0) {
			if ((n & 1) != 0) {
				if (start_gap == false) start_gap = true;
				else gap = max(gap, zeroes);

				zeroes = 0;
			}
			else if (start_gap == true) zeroes++;

			n >>= 1;
		}

		return gap;
	}
};

#if 0
int main(void) {
	int n = 32;

	BinaryGap t;
	cout << t.solution(n) << endl;
	
	return 0;
}
#endif