
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class EquiLeader {
public:
	void find_dominator(vector<int>& a, int& d, int& c) {
		int n = (int)a.size();

		c = -1, d = -1;
		for (int i = 0; i < n; i++) {
			if (d != -1) {
				if (a[i] == d) c++;
				else c--;
			}

			if (c == -1) { d = a[i]; c = 1; }
		}

		c = 0;
		for (int i = 0; i < n; i++)
			if (a[i] == d) c++;

		if (c < ((n - 1)/ 2 + 2)) { d = -1; c = -1; }
	}

	int solution(vector<int>& a) {
		if (a.empty() || a.size() == 1) return 0;

		int d, c;
		find_dominator(a, d, c);
		if (d == -1) return 0;

		int dc = 0, ans = 0;
		int n = (int)a.size();
		for (int i = 0; i < n; i++) {
			if (a[i] == d) dc++;

			if (dc < (i + 1) / 2 + 1 || (c - dc) < ((n - i - 1) / 2 + 1)) continue;
			
			ans++;
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = { 4, 3, 4, 4, 4, 2 };

	EquiLeader t;
	cout << t.solution(a) << endl;

	return 0;
}
#endif