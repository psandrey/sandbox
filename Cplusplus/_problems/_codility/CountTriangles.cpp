
#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;
	
class CountTriangles {
public:
	int solution(vector<int>& a) {
		int n = (int)a.size();
		if (n < 3) return 0;

		sort(a.begin(), a.end());
		int c = 0;
		for (int i = 0; i < n - 2; i++) {
			for (int j = i + 1; j < n - 1; j++) {
				for (int k = n - 1; k > j; k--) {
					if (a[i] + a[j] > a[k]) { c += (k - j); break; }
				}
			}
		}

		return c;
	}
};

#if 0
int main(void) {
	vector<int> a = { 10,2, 5, 1, 8, 12 };

	CountTriangles t;
	cout << t.solution(a) << endl;

	return 0;
}
#endif