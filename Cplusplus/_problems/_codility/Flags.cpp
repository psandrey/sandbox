#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class Flags {
public:
	bool put_flags(vector<int>& p, int k) {
		if (k == 1) return false;

		int f = 1, d = 0;
		for (int i = 0; i < (int)p.size(); i++) {
			d += p[i];
			if (d >= k) { f++; d = 0; }
			if (f == k) return false;
		}
		
		return true;
	}

	int solution(vector<int>& a) {
		int n = (int)a.size();
		if (n < 3) return 0;

		int i = -1, len = 0, peeks = 0;
		vector<int> dpeeks;
		for (int j = 1; j < n - 1; j++) {
			if (a[j - 1] < a[j] && a[j] > a[j + 1]) {
				if (i != -1) { dpeeks.push_back(j - i); len += (j - i); }
				i = j; peeks++;
			}
		}
		if (peeks == 0) return 0;
		if (peeks == 1) return 1;

		int lo = 0, hi = len, ans = 0;
		while (lo <= hi) {
			int m = (lo + hi) / 2;
			bool rem = put_flags(dpeeks, m);
			if (rem == false) { ans = max(ans, m); lo = m + 1; }
			else hi = m - 1;
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = { 1, 5, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2 };
	//vector<int> a = { 0, 0, 0, 0, 0, 1, 0, 1, 0, 1 };

	Flags t;
	cout << t.solution(a) << endl;

	return 0;
}
#endif