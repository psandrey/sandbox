
#include <vector>
#include <iostream>
#include <functional>
#include <climits>
#include <algorithm>
using namespace std;

class MissingInteger {
public:
    int solution(vector<int>& a) {
        int n = (int)a.size();

         int up = n, i = 0;
        while (i < up) {
            if (a[i] <= 0) { up--; swap(a[i], a[up]); }
            else i++;
        }
        if (up == 0) return 1;

        for (i = 0; i < up; i++) {
            int x = abs(a[i]);

            if (x > up) continue;
            if (a[x - 1] > 0) a[x - 1] *= -1;
        }

        for (i = 0; i < up; i++)
            if (a[i] > 0) return i + 1;

        return up + 1;
    }
};

#if 0
int main(void) {
    vector<int> a = { 1, 2, 3 };

    MissingInteger t;
    cout << t.solution(a) << endl;

	return 0;
}
#endif