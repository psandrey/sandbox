
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class CountNonDivisible {
public:
	vector<int> solution(vector<int>& a) {
		int n = (int)a.size();

		vector<int> sieve(2 * n + 1, 0);
		for (int i = 0; i < n; i++) sieve[a[i]]++;

		vector<int> nond(n);
		for (int i = 0; i < n; i++) {
			int x = a[i];
			int m = (int)sqrt(x);
			bool exact = (m * m == x ? true : false);
			int d = 0;
			for (int j = 1; j <= m; j++) {
				if (x % j == 0) {
					d += sieve[j];
					if (!(j == m && exact)) d += sieve[x / j];
				}
			}
			nond[i] = n - d;
		}

		return nond;
	}
};

#if 0
int main(void) {
	vector<int> a = { 3, 1, 2, 3, 6 };

	CountNonDivisible t;
	vector<int> ans = t.solution(a);
	for (int x : ans) cout << x << " ";
	cout << endl;

	return 0;
}
#endif