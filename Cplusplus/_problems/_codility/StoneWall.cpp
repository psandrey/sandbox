
#include <vector>
#include <stack>
#include <algorithm>
#include <iostream>
using namespace std;

class StoneWall {
public:
	int solution(vector<int>& h) {
		stack<int> st;
		int c = 0;
		for (int i = 0; i < (int)h.size(); i++) {
			while (!st.empty() && st.top() > h[i]) st.pop();

			if (st.empty() || st.top() < h[i]) {
				st.push(h[i]);
				c++;
			}
		}

		return c;
	}

};

#if 0
int main() {
	vector<int> h = { 8, 8, 5, 7, 9, 8, 7, 4, 8 };

	StoneWall t;
	cout << t.solution(h) << endl;

	return 0;
}
#endif