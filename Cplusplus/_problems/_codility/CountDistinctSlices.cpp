
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class CountDistinctSlices {
public:
	int solution(int m, vector<int>& a) {
		int n = (int)a.size();
		vector<bool> f(m + 1, 0);
		int i = 0, j = 0, ans = 0;
		while(j < n) {
			if (f[a[j]] == false) {
				f[a[j]] = true;
				ans += (j - i + 1);
				if (ans > 1000000000) return 1000000000;

				j++;
			} else {
				while (f[a[j]] == true) {
					f[a[i]] = false; i++;
				}
			}
		}

		return ans;
	}

#if 0
	// 90% (1 performance test is falling)
	typedef long long int ll;
	int solution(int m, vector<int>& a) {
		int n = (int)a.size();
		
		unordered_set<int> f;

		int i = 0, j = -1, prev = -1, ans = 0;
		while (j < n - 1) {
			if (!f.empty()) {
				while (i <= j) {
					f.erase(f.find(a[i]));
					if (a[i] == a[j + 1]) { i++; break; }
					else i++;
				}
			}

			while (j + 1 < n) {
				if (f.find(a[j + 1]) == f.end()) { f.insert(a[j + 1]); j++; }
				else break;
			}
			
			ll slices = ((j - i + 2LL) * (j - i + 1LL)) / 2LL;
			if (prev != -1 && prev >= i) { // substract intersections
				ll q = ((prev - i + 2LL) * (prev - i + 1LL)) / 2LL;
				slices -= q;
			}

			ans += slices;
			if (ans > 1000000000LL)	return 1000000000;
			prev = j;
		}

		return ans;
	}
#endif
};

#if 0
int main(void) {
	vector<int> a = {1, 2, 3, 2, 4};
	int m = 6;
	
	CountDistinctSlices t;
	cout << t.solution(m, a) << endl;
	
	return 0;
}
#endif