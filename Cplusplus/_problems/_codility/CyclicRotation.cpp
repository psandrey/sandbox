
#include <vector>
#include <iostream>
using namespace std;

class CyclicRotation {
public:
	vector<int> solution(vector<int>& a, int k) {
		int n = (int)a.size();
		if (a.empty() || k % n == 0) return a;
		k %= n;

		int swaps = 0;
		for (int i_state = 0; swaps < n; i_state++) {
			int i = i_state + k, x = a[i_state];
			while (i != i_state) {
				swap(a[i], x); swaps++;
				i = (i + k) % n;
			}
			swap(a[i], x); swaps++;
		}

		return a;
	}
};

#if 0
int main(void) {
	vector<int> a = { 0, 1, 2, 3 };
	int k = 1;

	CyclicRotation t;
	vector<int> ans = t.solution(a, k);
	for (int x : ans) cout << x << " ";
	cout << endl;
	return 0;
}
#endif