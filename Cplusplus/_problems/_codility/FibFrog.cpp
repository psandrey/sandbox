
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class FibFrog {
public:
	int min_steps(int k, vector<int>& a, vector<int>& F, vector<int>& dp) {
		if (k == -1) return 0;
		if (dp[k] != -1) return dp[k];

		if (a[k] == 0) dp[k] = INT_MAX;
		else {
			int m = INT_MAX;
			for (int i = 2; i < (int)F.size(); i++) {
				int p = k - F[i];
				if (p >= -1) {
					int c = min_steps(k - F[i], a, F, dp);
					if (c != INT_MAX) m = min(m, c + 1);
				}
				else if (p < -1) break;
			}
			dp[k] = m;
		}

		return dp[k];
	}

	int solution(vector<int>& a) {
		if (a.empty()) return 1;

		a.push_back(1);
		int n = a.size();

		vector<int> F;
		F.push_back(0); F.push_back(1);
		while (F.back() < n)
			F.push_back(F[F.size() - 2] + F[F.size() - 1]);
		
		vector<int> dp(n, -1);
 		int m = min_steps(a.size() - 1, a, F, dp);

		return  (m == INT_MAX ? -1 : m);
	}
};

#if 0
int main(void) {
	//vector<int> a = { 0, 0, 0, 1, 1, 0, 1, 0 ,0 ,0 ,0 };
	vector<int> a = { 0, 0, 0 };

	FibFrog t;
	cout << t.solution(a) << endl;

	return 0;
}
#endif