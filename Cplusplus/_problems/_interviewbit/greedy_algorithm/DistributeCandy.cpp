
/*
 * There are N children standing in a line. Each child is assigned a rating value.
 *
 *  You are giving candies to these children subjected to the following requirements:
 * Each child must have at least one candy.
 * Children with a higher rating get more candies than their neighbors.
 * What is the minimum candies you must give?
 *
 * Sample Input : Ratings : [1 2]
 * Sample Output : 3
 * The candidate with 1 rating gets 1 candy and candidate with rating cannot get 1 candy as 1 is its neighbor.
 * So rating 2 candidate gets 2 candies. In total, 2+1 = 3 candies need to be given out.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class DistributeCandy {
public:
	int candies(vector<int>& ratings) {
		int n = (int)ratings.size();
		if (n == 0) return 0;
		vector<int> left_to_right;

		int candies = 1;
		left_to_right.push_back(1);
		for (int i = 1; i < n; i++) {
			if (ratings[i] > ratings[i - 1]) candies++;
			else candies = 1;
			left_to_right.push_back(candies);
		}

		candies = 1;
		int ans = max(candies, left_to_right[n - 1]);
		for (int i = n-2; i >= 0; i--) {
			if (ratings[i] > ratings[i + 1]) candies++;
			else candies = 1;
			ans += max(left_to_right[i], candies);
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> ratings = { -1, -1, - 2, 4 };

	DistributeCandy t;
	cout << t.candies(ratings);

	return 0;
}
#endif