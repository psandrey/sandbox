
/*
 * There is a row of seats. Assume that it contains N seats adjacent to each other.
 * There is a group of people who are already seated in that row randomly. i.e. some are sitting together &
 * some are scattered.
 * An occupied seat is marked with a character 'x' and an unoccupied seat is marked with a dot ('.')
 * Now your target is to make the whole group sit together i.e. next to each other, without having any vacant seat between them in such a way that the total number of hops or jumps to move them should be minimum.
 *
 * Return minimum value % MOD where MOD = 10000003
 *
 * Example
 * Here is the row having 15 seats represented by the String (0, 1, 2, 3, ......... , 14) -
 *				. . . . x . . x x . . . x . .
 *
 * Now to make them sit together one of approaches is -
 *				. . . . . . x x x x . . . . .
 *
 * Following are the steps to achieve this -
 * 1 - Move the person sitting at 4th index to 6th index - Number of jumps by him =   (6 - 4) = 2
 * 2 - Bring the person sitting at 12th index to 9th index - Number of jumps by him = (12 - 9) = 3
 *
 * So now the total number of jumps made =
 * 	( 2 + 3 ) % MOD = 5 which is the minimum possible jumps to make them seat together.
 *
 * There are also other ways to make them sit together but the number of jumps will exceed 5 and that will not be minimum.
 * For example bring them all towards the starting of the row i.e. start placing them from index 0.
 * In that case the total number of jumps will be
 * 	( 4 + 6 + 6 + 9 )%MOD = 25 which is very costly and not an optimized way to do this movement
 */

#include <string>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class Seats {
public:
	int seats(string a) {
		int n = (int)a.size();
		if (n == 0) return 0;

		vector<int> middle;
		for (int i = 0; i < n; i++) if (a[i] == 'x') middle.push_back(i);

		// find middle of all all people seats and cluster them related to it
		// basically, minimize the number of seats to jumb by every man 
		int j = 0;
		int m = (int)middle.size();
		if (m == 0) return 0;
		if (m % 2 != 0) j = middle[m / 2];
		else j = (middle[m / 2] + middle[(m - 1) / 2]) / 2;
	
		// find first free seat to the right
		int i = j;
		while (i < n && a[i] == 'x') i++;
		
		// cluster people
		int ans = 0, mod = 10000003;
		int free = i;
		while (i < n) {
			if (a[i] == 'x') { ans = (ans + (i - free) % mod) % mod; free++; }
			i++;
		}
		
		// find first free seat to the left
		i = j-1;
		while (i >= 0 && a[i] == 'x') i--;
		
		// cluster people
		free = i;
		while (i >= 0) {
			if (a[i] == 'x') { ans = (ans + (free - i) % mod) % mod; free--; }
			i--;
		}

		return ans;
	}
};

#if 0
int main(void) {
	//string a = "....x..xx...x..";
	string a = "....";
	Seats t;
	cout << t.seats(a);

	return 0;
}
#endif