
/*
 * Given an array of size n, find the majority element. The majority element is the element
 *  that appears more than floor(n/2) times. You may assume that the array is non-empty and
 *  the majority element always exist in the array.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MajorityElement {
public:
	int majorityElement(const vector<int>& a) {
		if (a.empty()) return INT_MIN;

		int m = -1, c = 0;
		for (int j = 0; j < (int)a.size(); j++) {
			if (c == 0) m = a[j];

			// cutting 1 majority element and 1 minority element
			// preserve the majority element for the remaining set
			if (m == a[j]) c++;
			else c--;
		}

		return m;
	}
};

#if 0
int main(void) {
	vector<int> a = { 1, 2, 2, 1, 2 };
	
	MajorityElement t;
	cout << t.majorityElement(a) << endl;

	return 0;
}
#endif