
/*
 * There are N gas stations along a circular route,
 * where the amount of gas at station i is gas[i].
 * You have a car with an unlimited gas tank and
 * it costs cost[i] of gas to travel from station i to
 * its next station (i+1).
 *
 * You begin the journey with an empty tank at one of the gas stations.
 */

#include <iostream>
#include <vector>
#include <iostream>
using namespace std;

class GasStation {
public:
	int canCompleteCircuit(const vector<int>& gas, const vector<int>& cost) {
		if (gas.empty() || cost.empty()) return -1;

		int start = 0; // start station
		int tank = gas[0] - cost[0]; // in tank after we go from station 0 -> 1
		int lowest_tank = 0; // sum of all gas needs (basically the sum of gaps)
		for (int j = 1; j < (int)gas.size(); j++) {
			// gaps is keep adding, so we need to leave the gaps behaind
			if (lowest_tank > tank) {
				lowest_tank = tank;
				start = j;
			}

			tank += (gas[j] - cost[j]);
		}

		// is the total gas enough to do the circut?
		if (tank >= 0) return start;

		return -1;
	}
};

#if 0
int main(void) {
	//vector<int> gas = { 204, 918, 18, 17, 35, 739, 913, 14, 76, 555, 333, 535, 653, 667, 52, 987, 422, 553, 599, 765, 494, 298, 16, 285, 272, 485, 989, 627, 422, 399, 258, 959, 475, 983, 535, 699, 663, 152, 606, 406, 173, 671, 559, 594, 531, 824, 898, 884, 491, 193, 315, 652, 799, 979, 890, 916, 331, 77, 650, 996, 367, 86, 767, 542, 858, 796, 264, 64, 513, 955, 669, 694, 382, 711, 710, 962, 854, 784, 299, 606, 655, 517, 376, 764, 998, 244, 896, 725, 218, 663, 965, 660, 803, 881, 482, 505, 336, 279 };
	//vector<int> cost = { 273, 790, 131, 367, 914, 140, 727, 41, 628, 594, 725, 289, 205, 496, 290, 743, 363, 412, 644, 232, 173, 8, 787, 673, 798, 938, 510, 832, 495, 866, 628, 184, 654, 296, 734, 587, 142, 350, 870, 583, 825, 511, 184, 770, 173, 486, 41, 681, 82, 532, 570, 71, 934, 56, 524, 432, 307, 796, 622, 640, 705, 498, 109, 519, 616, 875, 895, 244, 688, 283, 49, 946, 313, 717, 819, 427, 845, 514, 809, 422, 233, 753, 176, 35, 76, 968, 836, 876, 551, 398, 12, 151, 910, 606, 932, 580, 795, 187 };
	
	vector<int> gas = { 497, 337, 387, 935, 368, 729, 885, 349, 614, 183, 640, 604, 580, 457, 798, 354, 299, 203, 734, 639 };
	vector<int> cost = { 770, 238, 73, 60, 437, 851, 62, 291, 320, 140, 548, 612, 677, 852, 305, 524, 208, 631, 889, 72 };
	
	GasStation t;
	cout << t.canCompleteCircuit(gas, cost) << endl;

	return 0;
}
#endif