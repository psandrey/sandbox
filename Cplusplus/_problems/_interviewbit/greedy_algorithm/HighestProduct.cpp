
/*
 * Given an array of integers, return the highest product possible by multiplying 3 numbers from the array
 *
 * Input: array of integers e.g {1, 2, 3}
 * NOTE: Solution will fit in a 32-bit signed integer
 *
 * Example: [0, -1, 3, 100, 70, 50] => 70*50*100 = 350000
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class HighestProduct {
public:
	int highest_prod(vector<int>& a) {
		int n = (int)a.size();
		if (n < 3) return -1;

		int max1 = INT_MIN, max2 = INT_MIN, max3 = INT_MIN;
		int min1 = INT_MAX, min2 = INT_MAX;
		for (int i = 0; i < n; i++) {
			if (a[i] > max3) { max1 = max2; max2 = max3; max3 = a[i]; }
			else if (a[i] > max2) { max1 = max2; max2 = a[i]; }
			else if (a[i] > max1) { max1 = a[i]; }

			if (a[i] < min1) { min2 = min1; min1 = a[i]; }
			else if (a[i] < min2) min2 = a[i];
		}

		int ans1 = max1 * max2 * max3;
		int ans2 = min1 * min2 * max3; // if min1 < 0 & min2 < 0 their product will pe > 0
		return max(ans1, ans2);
	}
};

# if 0
int main(void) {
	vector<int> a = { -2, -3, -4 };
	
	HighestProduct t;
	cout << t.highest_prod(a);

	return 0;
}
#endif