
/*
 * Given three sorted arrays A, B and Cof not necessarily same sizes.
 *
 * Calculate the minimum absolute difference between the maximum and minimum number from the triplet a, b, c such that a, b, c belongs arrays A, B, C respectively.
 * i.e. minimize | max(a,b,c) - min(a,b,c) |.
 *
 * Example :
 * Input:
 *   A : [ 1, 4, 5, 8, 10 ]
 *   B : [ 6, 9, 15 ]
 *   C : [ 2, 3, 6, 6 ]
 * Output:
 *   1
*/

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MinimizeAbsoluteDifference {
public:
	int min_abs_diff(vector<int>& a, vector<int>& b, vector<int>& c) {
		int na = (int)a.size(), nb = (int)b.size(), nc = (int)c.size();
		if (na == 0 || nb == 0 || nc == 0) return -1;

		int ia = 0, ib = 0, ic = 0;
		int ans = INT_MAX;
		while (ia < na && ib < nb && ic < nc) {
			int m = min(min(a[ia], b[ib]), c[ic]);
			int M = max(max(a[ia], b[ib]), c[ic]);
			int delta = M - m;
			ans = min(ans, delta);

			// in order to minimze the differents, we need to get values closer,
			//  so, move the smallest value towards the others:
			if (a[ia] == m) ia++;		// a[ia] is the smallest
			else if (b[ib] == m) ib++;	// b[ib] is the smallest
			else if (c[ic] == m) ic++;	// c[ic] is the smallest
		}

		return ans;
	}
};

/*
int main(void) {
	vector<int> a = { 1, 4, 4, 8, 10 };
	vector<int> b = { 6, 8 };
	vector<int> c = { 2, 3, 6, 9 };
	MinimizeAbsoluteDifference t;

	cout << t.min_abs_diff(a, b, c);

	return 0;
}
*/