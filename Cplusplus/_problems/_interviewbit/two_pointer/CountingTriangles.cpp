
/*
 * You are given an array of N non-negative integers, A0, A1 ,�, AN-1.
 * Considering each array element Ai as the edge length of some line segment, count the number of triangles
 * which you can form using these array values.
 *
 * Notes:
 * You can use any value only once while forming each triangle. Order of choosing the edge lengths doesn�t matter.
 * Any triangle formed should have a positive area.
 *
 * Return answer modulo 109 + 7.
 *
 * For example :A = [1, 1, 1, 2, 2]
 * Return: 4
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class CountingTriangles {
public:

	int no_triangles(vector<int>& e) {
		int n = (int)e.size();

		// after sort, e[a] + e[b] <= e[c]
		sort(e.begin(), e.end());

		int ans = 0;
		int m = 1000000007UL;
		for (int c = n - 1; c >= 2; c--) {
			int a = 0, b = c - 1;

			while (a < b) {
				// e[a] + e[b] > e[c] then, all a's with fixed b can form triangles
				if (e[a] + e[b] > e[c]) {
					ans = (ans + (b - a) % m) % m;
					b--;
				}
				else a++;
			}
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = {3, 4, 5, 6, 7};

	CountingTriangles t;
	cout << t.no_triangles(a);

	return 0;
}
#endif