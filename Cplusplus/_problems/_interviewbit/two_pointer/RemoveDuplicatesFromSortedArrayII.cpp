
/*
 * Given a sorted array, remove the duplicates in place such that each element can appear atmost twice and return the new length.
 *
 * Note: Do not allocate extra space for another array, you must do this in place with constant memory.
 * Note: That even though we want you to return the new length, make sure to change the original array as well in place
 *
 * For example,
 * Given input array A = [1,1,1,2],
 *
 * Your function should return length = 3, and A is now [1,1,2].
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class RemoveDuplicatesFromSortedArrayII {
public:
	int remove_duplicates(vector<int>& a) {
		if (a.empty() || a.size() <= 2) return a.size();

		bool flag = false;
		int j = 0, prev = a[0];
		for (int i = 1; i < (int)a.size(); i++) {
			if (a[i] != a[i - 1] || flag == false) a[++j] = a[i];

			if (a[i] == prev) flag = true;
			else flag = false;

			prev = a[i];
		}

		a.resize(j + 1);
		return (int)a.size();
	}
};

#if 0
int main(void) {
	vector<int> a = { 0, 0, 0, 1, 1, 2, 2, 3 };

	RemoveDuplicatesFromSortedArrayII t;
	t.remove_duplicates(a);
	if (!a.empty()) for (int x : a) cout << x << " ";
	cout << endl;
	
	return 0;
}
#endif