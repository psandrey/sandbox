
/*
 * Remove duplicates from Sorted Array
 * Given a sorted array, remove the duplicates in place such that each element appears only once
 * and return the new length.
 *
 * Note: that even though we want you to return the new length, make sure to change
 *  the original array as well in place. Do not allocate extra space for another array,
 *  you must do this in place with constant memory.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class RemoveDuplicatesSortedArray {
public:
	int removeDuplicates(vector<int> &a) {
		if (a.empty()) return 0;
		if (a.size() == 1) return 1;
		int i = 0;
		for (int j = 1; j < (int)a.size(); j++) {
			if (a[j] != a[j - 1]) a[++i] = a[j];
		}

		a.erase(a.begin() + i + 1, a.end());
		return (i + 1);
	}
};

#if 0
int main(void) {
	vector<int> a = {1, 1, 2};

	RemoveDuplicatesSortedArray t;
	cout << t.removeDuplicates(a) << endl;

	return 0;
}
#endif