
/*
 * Given an array with n objects colored red, white or blue,
 * sort them so that objects of the same color are adjacent, with the colors in the order red, white and blue.
 *
 * Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively.
 *
 * Note: Using library sort function is not allowed.
 *
 * Example :
 *   Input : [0 1 2 0 1 2]
 *   Modify array so that it becomes : [0 0 1 1 2 2]
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class SortByColor {
public:
	void sort(vector<int>& a) {
		int n = (int)a.size();
		if (n == 0) return;

		// Pigeonhole sort
		vector<int> counter(3);
		for (int j = 0; j < n; j++) counter[a[j]]++;

		int i = 0;
		for (int j = 0; j < 3; j++) {
			while (counter[j] > 0) { a[i] = j; i++; counter[j]--; }
		}
	}
};

/*
int main(void) {
	vector<int> a = { 0, 1, 2, 0, 1, 2 };

	SortByColor t;
	t.sort(a);
	if (!a.empty()) {
		for (int i = 0; i < (int)a.size(); i++) cout << a[i] << " ";
		cout << endl;
	}

	return 0;
}
*/