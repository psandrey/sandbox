
/*
 * Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0?
 * Find all unique triplets in the array which gives the sum of zero.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class ThreeSumZero {
public:
	vector<vector<int> > threeSum(vector<int> &a) {
		vector<vector<int> > ans;
		if (a.empty() || a.size() < 3) return ans;

		sort(a.begin(), a.end());

		int n = (int)a.size();
		int k = 0;
		while (k < n - 2) {
			while (k > 0 && k < n - 2 && a[k] == a[k - 1]) k++;

			int i = k + 1, j = n - 1;
			while (i < j) {
				int s = a[k] + a[i] + a[j];

				if (s > 0) j--;
				else if (s < 0) i++;
				else {
					ans.push_back(vector<int>({ a[k], a[i], a[j] }));
					i++; j--;

					while (i < j && a[i] == a[i - 1]) i++;
					while (i < j && a[j] == a[j + 1]) j--;

				}
			}
			k++;
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = {-1, 0, 1, 2, -1, -4};
	
	ThreeSumZero t;
	vector<vector<int>> ans = t.threeSum(a);
	if (ans.empty() == false)
		for (int i = 0; i < (int)ans.size(); i++)
			cout << ans[i][0] << ", " << ans[i][1] << ", " << ans[i][2] << endl;

	return 0;
}
#endif