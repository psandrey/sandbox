
/*
 * You are given with an array of 1s and 0s. And you are given with an integer M, which signifies number of flips allowed.
 * Find the position of zeros which when flipped will produce maximum continuous series of 1s.
 *
 * For this problem, return the indices of maximum continuous series of 1s in order.
 *
 * Example:
 *   Input :
 *   Array = {1 1 0 1 1 0 0 1 1 1 }
 *   M = 1
 *
 *   Output : [0, 1, 2, 3, 4]
 *
 *  Note: If there are multiple possible solutions, return the sequence which has the minimum start index.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MaxContinuousSeriesOf1 {
public:
	vector<int> max_cont1(vector<int>& a, int m) {
		int n = (int)a.size();
		if (n == 0) return{};

		int i = 0, j = 0, c = m, lo = 0, hi = 0;
		for (j = 0; j < n; j++) {
			if (a[j] == 0) c--;

			if (c == -1) {
				if (hi - lo < j - i) { lo = i; hi = j; }

				while (i < n && a[i] == 1) i++;
				i++; c = 0;
			}
		}

		if (hi - lo < j - i) { lo = i; hi = j; }

		vector<int> ans(hi - lo);
		for (int i = 0; i < (hi - lo); i++) ans[i] = lo + i;
		return ans;
	}
};

#if 0
int main(void) {
	//vector<int> a = { 1, 1, 0, 1, 1, 0, 0, 1, 1, 1 };
	//int m = 0;

	vector<int> a = { 1, 1, 0 };
	int m = 2;

	MaxContinuousSeriesOf1 t;
	vector<int> ans = t.max_cont1(a, m);
	if (!ans.empty()) {
		for (int x : ans) cout << x << " ";
		cout << endl;
	}

	return 0;
}
#endif