
/*
 * Given an array S of n integers, find three integers in S such that the sum is closest to a given number, target.
 * Return the sum of the three integers.
 *
 * Assume that there will only be one solution
 *
 * Example: given array S = {-1 2 1 -4}, and target = 1.
 *
 * The sum that is closest to the target is 2. (-1 + 2 + 1 = 2)
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class ThreeSum {
public:
	 int three_sum(vector<int>& a, int t) {
		int n = (int)a.size();
		if (n < 3) return -1;
		long long int tt = t;
		long long int ans =  INT_MAX;
		long long int min_distance = LLONG_MAX;

		sort(a.begin(), a.end());
		for (int k = 0; k < n - 2; k++) {

 			int i = k + 1, j = n - 1;
			while (i < j) {
				long long int sum = a[k] + a[i] + a[j];
				long long int distance = abs(tt - sum);
				if (distance < min_distance) {
					ans = sum;
					min_distance = distance;
				}

				if (sum == t) return t;
				else if (sum > t) j--;
				else i++;
			}
		}

		return ((int)ans);
	}
};

#if 0
int main(void) {
	vector<int> a = { 2, 1, -9, -7, -8, 2, -8, 2, 3, -8 };
	int target = -1;

	ThreeSum t;
	cout << t.three_sum(a, target);

	return 0;
}
#endif