
/*
 * Given two sorted integer arrays A and B, merge B into A as one sorted array.
 *
 * Note: You have to modify the array A to contain the merge of A and B. Do not output anything in your code.
 * TIP: C users, please malloc the result into a new array and return the result.
 * If the number of elements initialized in A and B are m and n respectively, the resulting size of array A after your code is executed should be m + n
 *
 * Example :
 *
 * Input :
 * 		 A : [1 5 8]
 * 		 B : [6 9]
 *
 * Modified A : [1 5 6 8 9 ]
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MergeTwoSortedListsII {
public:
	void merge(vector<int>& a, vector<int>& b) {
		int m = (int)b.size();
		if (m == 0) return;

		int i = 0, j = 0;
		while (i < (int)a.size() && j < m) {
			if (b[j] < a[i]) {
				a.insert(a.begin() + i, b[j]);
				j++;
			} else i++;
		}
		
		// insert remaining elements from b
		while (j < m) {
			a.insert(a.begin() + i, b[j]);
			j++; i++;
		}
	}
};

/*
int main(void) {
	vector<int> a = { 2, 3, 5, 5, 9 };
	vector<int> b = { 1, 4, 7, 11, 12};

	MergeTwoSortedListsII t;
	t.merge(a, b);
	if (!a.empty()) {
		for (int x : a) cout << x << " ";
		cout << endl;
	}

	return 0;
}
*/