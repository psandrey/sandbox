
/*
 * Given an array �A� of sorted integers and another non negative integer k,
 * find if there exists 2 indices i and j such that A[i] - A[j] = k, i != j.
 *
 *  Example: Input :
 * 	A : [1 3 5]
 * 	k : 4
 *  Output : YES as 5 - 1 = 4
 * Return 0 / 1 ( 0 for false, 1 for true ) for this problem
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class Diffk {
public:
	int is_diffk(vector<int>& a, int k) {
		int n = (int)a.size();
		if (n < 2 || k < 0) return 0;

		// just like an elastic:
		// - if the diff is to small, then extend
		// - if it is to big, then contract
		int i = 0, j = 1;
		while (j < n) {
			if (i == j) { j++; continue; }

			int diff = a[j] - a[i];
			if (diff < k) j++;
			else if (diff > k) i++;
			else return 1;
		}

		return 0;
	}
};

/*
int main(void) {
	vector<int> a = { 1, 2, 2, 3, 4 };
	int k = 0;

	Diffk t;
	cout << t.is_diffk(a, k);

	return 0;
}
*/