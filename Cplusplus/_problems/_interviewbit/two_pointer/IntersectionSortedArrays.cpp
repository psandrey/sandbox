
/*
 * Find the intersection of two sorted arrays OR in other words,
 * Given 2 sorted arrays, find all the elements which occur in both the arrays.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class IntersectionSortedArrays {
public:
	vector<int> intersect(const vector<int> &a, const vector<int> &b) {
		vector<int> c;
		if (a.empty() || b.empty()) return c;

		int i = 0, j = 0;
		while (i < (int)a.size() && j < (int)b.size()) {
			if (a[i] == b[j]) { c.push_back(a[i]); i++; j++; }
			else if (a[i] < b[j]) i++;
			else j++;
		}

		return c;
	}
};

#if 0
int main(void) {
	vector<int> a = {1, 2, 3, 3, 4, 5, 6};
	vector<int> b = {3, 3, 5};

	IntersectionSortedArrays t;
	vector<int> c = t.intersect(a, b);
	for (int x : c) cout << x << ", ";

	return 0;
}
#endif