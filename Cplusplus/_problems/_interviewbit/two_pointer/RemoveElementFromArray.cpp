
/*
 * Given an array and a value, remove all the instances of that value in the array.
 * Also return the number of elements left in the array after the operation.
 * It does not matter what is left beyond the expected length.
 *
 * Example:
 * If array A is [4, 1, 1, 2, 1, 3] and value elem is 1,
 * then new length is 3, and A is now [4, 2, 3]
 *
 * Note: Do it in less than linear additional space complexity.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class RemoveElementFromArray {
public:
	int remove_element(vector<int>& a, int remove) {
		int n = (int)a.size();
		
		int i = 0;
		for (int j = 0; j < n; j++)
			if (a[j] != remove) a[i++] = a[j];

		a.resize(i);
		return i;
	}
};

#if 0
int main(void) {
	vector<int> a = { 1, 1, 1, 1, 1, 1 };
	RemoveElementFromArray t;

	int n = t.remove_element(a, 1);
	cout << n << endl;
	if (!a.empty()) {
		for (int i = 0; i < n; i++) cout << a[i] << " ";
		cout << endl;
	}
	return 0;
}
#endif