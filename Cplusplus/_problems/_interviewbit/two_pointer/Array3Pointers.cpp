
/*
 * You are given 3 arrays A, B and C. All 3 of the arrays are sorted.
 * Find i, j, k such that:
 * max(abs(A[i] - B[j]), abs(B[j] - C[k]), abs(C[k] - A[i])) is minimized.
 * Return the minimum max(abs(A[i] - B[j]), abs(B[j] - C[k]), abs(C[k] - A[i]))
 *
 * Input :
 *         A : [1, 4, 10]
 *         B : [2, 15, 20]
 *         C : [10, 12]
 *
 * Output : 5
 *          With 10 from A, 15 from B and 10 from C.
 *
 * Note: The problem can be solved in O(n.logn)
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class Array3Pointers{
public:
	int minimize(const vector<int> &a, const vector<int> &b, const vector<int> &c) {
		if (a.empty() || b.empty() || c.empty()) return 0;

		int i = 0, j = 0, k = 0, ans = INT_MAX;
		while (i < (int)a.size() && j < (int)b.size() && k < (int)c.size()) {
			ans = min(ans, max(abs(a[i] - b[j]), max(abs(a[i] - c[k]), abs(b[j] - c[k]))));
			
			int m = min(a[i], min(b[j], c[k]));
			if (m == a[i]) i++;
			else if (m == b[j]) j++;
			else k++;
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = { 1, 4, 10 };
	vector<int> b = { 2, 15, 20};
	vector<int> c = { 10, 12 };
	
	Array3Pointers t;
	cout << t.minimize(a, b, c) << endl;
	
	return 0;
}
#endif