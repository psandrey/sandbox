
/*
 * Given two words (start and end), and a dictionary, find the length of shortest transformation
 * sequence from start to end, such that:
 *  - You must change exactly one character in every transformation
 *  - Each intermediate word must exist in the dictionary
 * Example:
 * start = "hit",  end = "cog"
 * dict = ["hot","dot","dog","lot","log"]
 *
 * As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog", return its length 5
 */

#include <unordered_set>
#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class WordLadder_I {
public:
#if 1
	// fastest version
	int solve(string a, string b, vector<string>& d) {
		if (hamming(a, b) == 0) return 1;
		else if (hamming(a, b) == 1) return 2;

		// remove duplicates from dictionary
		sort(d.begin(), d.end());
		int k = 0;
		for (int i = 1; i < (int)d.size(); i++)
			if (d[i] != d[i - 1]) d[++k] = d[i];
		d.erase(d.begin() + k + 1, d.end());

		// check if a and b strings are in the dictionary (if not, add them)
		int s = -1, t = -1;
		for (int i = 0; i < (int)d.size(); i++) {
			if (s == -1 || t == -1) {
				if (s == -1 && a.compare(d[i]) == 0) s = i;
				if (t == -1 && b.compare(d[i]) == 0) t = i;
			}
			else break;
		}
		if (s == -1) { s = (int)d.size(); d.push_back(a); }
		if (t == -1) { t = (int)d.size(); d.push_back(b); }

		// build adjacency list
		int n = (int)d.size();
		vector<vector<int>> adjL(n, vector<int>());
		for (int u = 0; u < n - 1; u++) {
			for (int v = u + 1; v < n; v++) {
				if (hamming(d[u], d[v]) == 1) {
					adjL[u].push_back(v);
					adjL[v].push_back(u);
				}
			}
		}

		// BFS to get the shortest distance
		vector<bool> marked(n, false);
		queue<int> q;
		q.push(s); marked[s] = true;
		int depth = 1;
		while (!q.empty()) {
			int size = q.size();
			for (int i = 0; i < size; i++) {
				int u = q.front(); q.pop();
				if (u == t) return depth;
				if (adjL[u].empty()) continue;

				for (int j = 0; j < (int)adjL[u].size(); j++) {
					int v = adjL[u][j];
					if (marked[v] == false) {
						q.push(v);
						marked[v] = true;
					}
				}
			}
			depth++;
		}

		return 0;
}
#else
	// passed, but slower than previous one
	int ladderLength(string a, string b, vector<string>& dictV) {
		if (hamming(a, b) == 0) return 1;
		else if (hamming(a, b) == 1) return 2;

		// remove duplicates from dictionary
		unordered_set<string> dict;
		for (int j = 0; j < (int)dictV.size(); j++) dict.insert(dictV[j]);
		dict.insert(a);
		dict.insert(b);
		vector<string> d(dict.size());
		int k = 0, s = -1, t = -1;
		for (unordered_set<string>::iterator it = dict.begin(); it != dict.end(); it++) {
			d[k] = *it;
			if (s == -1 && d[k].compare(a) == 0) s = k;
			if (t == -1 && d[k].compare(b) == 0) t = k;
			k++;
		}

		// build adjacency list
		int n = (int)d.size();
		vector<vector<int>> adjL(n, vector<int>());
		for (int u = 0; u < n - 1; u++) {
			for (int v = u + 1; v < n; v++) {
				if (hamming(d[u], d[v]) == 1) {
					adjL[u].push_back(v);
					adjL[v].push_back(u);
				}
			}
		}

		// BFS to get the shortest distance
		vector<bool> marked(n, false);
		queue<int> q;
		q.push(s); marked[s] = true;
		int depth = 1;
		while (!q.empty()) {
			int size = q.size();
			for(int i = 0; i < size; i++) {
				int u = q.front(); q.pop();
				if (u == t) return depth;
				if (adjL[u].empty()) continue;

				for (int j = 0; j < (int)adjL[u].size(); j++) {
					int v = adjL[u][j];
					if (marked[v] == false) {
						q.push(v);
						marked[v] = true;
					}
				}
			}
			depth++;
		}

		return 0;
	}
#endif

#if 0
	// slowest version
	int ladderLength(string a, string b, vector<string>& d) {
		if (hamming(a, b) == 0) return 1;
		else if (hamming(a, b) == 1) return 2;

		unordered_set<string> dict;
		for (int j = 0; j < (int)d.size(); j++) dict.insert(d[j]);
		dict.insert(a);
		dict.insert(b);

		// BFS to get the shortest distance
		unordered_set<string> marked;
		queue<string> q;
		q.push(a); marked.insert(a);
		int depth = 0;
		while (!q.empty()) {
			int size = q.size();
			for (int i = 0; i < size; i++) {
				string u = q.front(); q.pop();
				if (u.compare(b) == 0) return depth;

				for (unordered_set<string>::iterator it = dict.begin(); it != dict.end(); it++) {
					string v = *it;
					if (marked.find(v) == marked.end() && hamming(u, v) == 1) {
						q.push(v);
						marked.insert(v);
					}
				}
			}
			depth++;
		}

		return 0;
	}
#endif

	int hamming(string& a, string& b) {
		int n = 0;
		int i = -1;
		while (n < 2 && ++i < (int)a.length()) {
			if (a[i] != b[i]) n++;
		}

		return n;
	}
};

#if 0
int main(void) {
#if 1
	vector<string> dictV = { "hot", "dot", "dog", "lot", "log" };
	string start = "hit";
	string end = "cog";
#else
	vector<string> dictV = {"baba", "abba", "aaba", "bbbb", "abaa", "abab", "aaab",
							"abba", "abba", "abba", "bbba", "aaab", "abaa", "baba", "baaa"};
	string start = "bbaa";
	string end = "babb";
#endif


	WordLadder_I t;
	cout << t.ladderLength(start, end, dictV);

	return 0;
}
#endif