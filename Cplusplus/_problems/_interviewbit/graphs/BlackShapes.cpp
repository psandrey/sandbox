
#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class BlackShapes {
public:
	int black(vector<string>& a) {
		if (a.empty()) return 0;
		int n = (int)a.size();
		int m = (int)a[0].size();
	
		int c = 0;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				if (a[i][j] == 'X') {
					c++;
					BFS(a, i, j);
				}

		return c;
	}

	void BFS(vector<string>& a, int i, int j) {
		vector<vector<int>> d = { {-1,0},{1,0},{0,-1},{0,1} };
		queue<pair<int, int>> q;
		q.push(make_pair(i, j));

		int n = (int)a.size();
		int m = (int)a[0].size();
		while (!q.empty()) {
			pair<int, int> p = q.front(); q.pop();
			int x = p.first;
			int y = p.second;
			a[x][y] = 'O';

			for (int k = 0; k < (int) d.size(); k++) {
				int xx = x + d[k][0];
				int yy = y + d[k][1];
				if (xx < 0 || xx == n || yy < 0 || yy == m || a[xx][yy] == 'O')
					continue;
				q.push(make_pair(xx, yy));
			}
		}
	}
};

#if 0
int main(void) {
	vector<string> a = {
		"OOOXOOO",
		"OOXXOXO",
		"OXOOOXO" };

	BlackShapes t;
	cout << t.black(a) << endl;

	return 0;
}
#endif