
/*
 * You are given an integer N. You have to find smallest multiple of N which consists of
 *  digits 0 and 1 only. Since this multiple could be large, return it in form of a string.
 *
 * Note: Returned string should not contain leading zeroes.
 */

#include <queue>
#include <iostream>
#include <vector>
using namespace std;

class SmallestMultipleWith0and1 {
public:

	/* X(i) = X(i-1) * 10 + b(i) => Ri = [X(i-1) * 10 + b(i)] % mod 
	 * R(i-1) = X(i-1) % m 
	 * Ri = [R(i-1) * 10 + b(i)] % m
	 */
	int next_rem(int prev_rem, int b, int mod) {
		return (prev_rem * 10 + b) % mod;
	}

	string build_answer(vector<int>& rems, vector<bool>& digit) {
		string ans;

		// build the answer in reverse order:
		// (pointer to previous state, and each state has a digit: 0 or 1)
		int r = 0; // last remainder must be 0
		while (rems[r] != r) {
			char b = (digit[r] == true ? '1' : '0');
			ans.insert(ans.begin(), b);

			r = rems[r];
		}
		char b = (digit[r] == true ? '1' : '0');
		ans.insert(ans.begin(), b);

		return ans;
	}

	string multiple(int n) {
		if (n == 0) return "0";
		if (n == 1) return "1";

		// rems handle states (rems[r] = pointer to previous state (previous rem))
		// and digit record the digit (0 or 1) for each state
		// there are at most n states, so each vector is size n
		vector<bool> digit(n, false);
		vector<int>  rems(n, -1);
		rems[1] = 1;
		digit[1] = true;

		queue<int> q; q.push(1);
		while (!q.empty()) {
			int rem = q.front(); q.pop();

			// next remainder if we add 0
			int r0 = next_rem(rem, 0, n);
			if (rems[r0] == -1) {
				rems[r0] = rem; // previous state for r0
				digit[r0] = false; // false means '0'

				q.push(r0);
			}
			if (r0 == 0) break; // if the rem is 0 then we are done

			// next remainder if we add 1
			int r1 = next_rem(rem, 1, n);
			if (rems[r1] == -1) {
				rems[r1] = rem; // previous state for r1
				digit[r1] = true; // true means '1'

				q.push(r1);
			}
			if (r1 == 0) break; // if the rem is 0 then we are done
		}

		return build_answer(rems, digit);
	}
};

#if 0
int main(void) {
	SmallestMultipleWith0and1 t;
	cout << t.multiple(34535) << endl;
	//cout << t.multiple(55) << endl;

	return 0;
}
#endif