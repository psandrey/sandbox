
/*
 * How many minimum numbers from fibonacci series are required such that
 *  sum of numbers should be equal to a given Number N.
 *
 * Note : repetition of number is allowed.
 */

#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class SumOfFibonacciNumbers {
public:
	int fibsum(int n) {
		if (n == 0) return 0;

		int pf = 1, f = 1;
		while (pf + f <= n) { int next = pf + f; pf = f; f = next; }
		int k = 0;
		while (n > 0) {
			while (f > n) { int ppf = f - pf; f = pf; pf = ppf; }

			n -= f; k++;
		}

		return k;
	}

#if 0
	int fibsum(int n) {
		if (n == 0) return 0;

		vector<int> f = fib(n);
		int m = (int)f.size();


		int k = f.size() - 1, ans = 0;
		while (n > 0) {
			ans += n / f[k];
			n = n % f[k];
			k--;
		}

		return ans;
	}

	vector<int> fib(int n) {
		vector<int> f; f.push_back(1);
		int next = 1;
		while (next <= n) {
			f.push_back(next);
			next = f[f.size() - 2] + f[f.size() - 1];
		}
		return f;
	}
#endif
};

#if 0
int main(void) {
	int n = 13;

	SumOfFibonacciNumbers t;
	cout << t.fibsum(n) << endl;

	return 0;
}
#endif