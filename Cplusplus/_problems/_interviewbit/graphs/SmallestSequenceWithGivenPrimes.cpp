
/*
 * Given 3 different prime numbers, return the first (smallest) k numbers that
 * factors only to these prime numbers.
 *
 * [2, 3, 5,] k=5 => [2, 3, 4, 5, 6]
 */

#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class SmallestSequenceWithGivenPrimes {
public:
	vector<int> solve(int A, int B, int C, int D) {
		vector<int> p = { A, B, C };
		
		// eliminate duplicates
		sort(p.begin(), p.end());
		int j = 0;
		for (int i = 1; i < (int)p.size(); i++)
			if (p[i] != p[i - 1]) p[++j] = p[i];
		p.erase(p.begin() + j + 1, p.end());

		struct compare {
			bool operator()(const pair<int, int>& a, const pair<int, int>& b) {
				return (a.first > b.first);
			}
		};
		priority_queue<pair<int, int>, vector<pair<int, int>>, compare> pq;
		for (int i = 0; i < (int)p.size(); i++)pq.push(make_pair(p[i], i));

		vector<int> ans;
		int k = D;
		while (k > 0) {
			pair<int, int> e = pq.top(); pq.pop();
			ans.push_back(e.first); k--;

			for(int i = e.second; i < (int)p.size(); i++)
				pq.push(make_pair(e.first * p[i], i));

		}

		return ans;
	}
};

#if 0
int main(void) {
	SmallestSequenceWithGivenPrimes t;

	vector<int> ans = t.solve(2, 3, 5, 5);
	for (int x : ans)cout << x << " ";

	return 0;
}
#endif