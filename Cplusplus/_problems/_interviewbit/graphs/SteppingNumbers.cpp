
/*
 * Given N and M find all stepping numbers in range N to M.
 *
 * Note: Version 2 -> BFS version.
 */

#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class SteppingNumbers {
public:
	vector<int> stepnum(int n, int m) {
		queue<int> q;
		for (int k = 1; k <= 9; k++) q.push(k);
		
		vector<int> ans;
		if (n == 0) ans.push_back(0);
		while (!q.empty()) {
			int k = q.front(); q.pop();
			if (k >= n && k <= m) ans.push_back(k);

			int d = k % 10;
			if (d != 0) {
				int next = k * 10 + (d - 1);
				if (next <= m)q.push(next);
			}
			if (d != 9) {
				int next = k * 10 + (d + 1);
				if (next <= m)q.push(next);
			}
		}

		return ans;
	}

};

#if 0
int main(void) {
	SteppingNumbers t;
	vector<int> ans = t.stepnum(0, 200);

	if (ans.empty() == false) {
		for (int x : ans) cout << x << " ";
	}
	return 0;
}
#endif