
/*
 * Clone an undirected graph. Each node in the graph contains a label and a list of its neighbors.
 */

#include <queue>
#include <unordered_map>
#include <vector>
#include <iostream>
using namespace std;

class CloneGraph {
public:
	struct UndirectedGraphNode {
		int label;
		vector<UndirectedGraphNode*> neighbors;
		UndirectedGraphNode(int x) : label(x) {};
	};

	UndirectedGraphNode* cloneGraph(UndirectedGraphNode* node) {
		if (node == NULL) return node;
	
		UndirectedGraphNode* cnode = new UndirectedGraphNode(node->label);
		unordered_map< UndirectedGraphNode*, UndirectedGraphNode*> mp;
		mp[node] = cnode;

		queue< UndirectedGraphNode*> q;
		q.push(node);
		while (!q.empty()) {
			UndirectedGraphNode* n = q.front(); q.pop();

			for (int i = 0; i < (int)n->neighbors.size(); i++) {
				UndirectedGraphNode* v = n->neighbors[i];

				if (mp.find(v) == mp.end()) {
					q.push(v);
					mp[v] = new UndirectedGraphNode(v->label);
				}
	
				mp[n]->neighbors.push_back(mp[v]);
			}

		}

		return cnode;
	}

	UndirectedGraphNode* buildGraph() {
		UndirectedGraphNode* n1 = new UndirectedGraphNode(703);
		UndirectedGraphNode* n2 = new UndirectedGraphNode(43);
		UndirectedGraphNode* n3 = new UndirectedGraphNode(279);

		n1->neighbors.push_back(n1);
		n1->neighbors.push_back(n2);
		n1->neighbors.push_back(n3);


		n2->neighbors.push_back(n3);
		n2->neighbors.push_back(n1);

		n3->neighbors.push_back(n2);
		n3->neighbors.push_back(n3);
		n3->neighbors.push_back(n3);
		return n1;
	}
};

#if 0
int main(void) {
	CloneGraph t;
	
	CloneGraph::UndirectedGraphNode* node = t.buildGraph();

	CloneGraph::UndirectedGraphNode* cnode = t.cloneGraph(node);

	return 0;
}
#endif