
/*
 * Given the total number of courses and a list of prerequisite pairs, is it possible
 * for you to finish all courses. return 1/0 if it is possible/not possible.
 * The list of prerequisite pair are given in two integer arrays B and C where B[i] is
 * a prerequisite for C[i].
 */

#include <queue>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class PossibilityOfFinishingAllCoursesGivenPre_requisites {
public:
#if 1
	// Kahn
	int solve(int n, vector<int>& b, vector<int>& c) {
		if (n == 0) return 1;

		vector<vector<int>> adjL = vector<vector<int>>(n, vector<int>());
		vector<int> indegree(n, 0);
		for (int i = 0; i < (int)b.size(); i++) {
			adjL[b[i] - 1].push_back(c[i] - 1);
			indegree[c[i] - 1]++;
		}

		queue<int> q;
		for (int i = 0; i < n; i++)
			if (indegree[i] == 0) q.push(i);

		int k = 0;
		while (!q.empty()) {
			int u = q.front(); q.pop(); k++;
			for (int v : adjL[u]) {
				indegree[v]--;
				if (indegree[v] == 0) q.push(v);
			}
		}

		return (k == n);
	}

#else
	// DFS
	int solve(int n, vector<int>& b, vector<int>& c) {
		if (n == 0) return 1;

		vector<vector<int>> graph = vector<vector<int>>(n, vector<int>());
		int m = (int)b.size();
		for (int i = 0; i < (int)b.size(); i++)graph[b[i] - 1].push_back(c[i] - 1);

		vector<bool> discovered(n, false);
		vector<bool> explored(n, false);
		for (int i = 0; i < n; i++) {
			if (explored[i] == false)
				if (has_cycles(graph, i, discovered, explored) == true) return false;
		}

		return true;
	}

	bool has_cycles(vector<vector<int>>& graph, int u, vector<bool>& discovered, vector<bool>& explored) {
		if (explored[u] == true) return false;
		if (discovered[u] == true) return true;

		discovered[u] = true;
		if (!graph[u].empty()) {
			for (int i = 0; i < (int)graph[u].size(); i++) {
				int v = graph[u][i];
				
				if (has_cycles(graph, v, discovered, explored) == true)
					return true;
			}
		}
		explored[u] = true;
		
		return false;
	}
#endif
};

#if 0
int main(void) {
	PossibilityOfFinishingAllCoursesGivenPre_requisites t;
	
	//int n = 3;
	//vector<int> b = { 1, 2, 1 };
	//vector<int> c = { 2, 3, 3 };

	int n = 70;
	vector<int> b = { 67, 8, 48, 42, 35, 25, 37, 69, 31, 36, 7, 33, 2, 47, 42, 52, 31, 70, 29, 38, 36, 60, 15, 37, 33, 27, 4, 32, 43, 55, 49, 35, 21, 28, 62, 17, 2, 61, 54, 22, 9, 56, 12, 3, 60, 52, 21, 15, 54, 63, 33, 64, 38, 16, 59, 69, 49, 52, 10, 10, 6, 56, 43, 32, 41, 66, 6 };
	vector<int> c = { 51, 43, 55, 27, 34, 8, 14, 5, 70, 64, 65, 57, 45, 19, 53, 50, 44, 51, 19, 41, 14, 68, 12, 58, 50, 66, 7, 47, 40, 62, 29, 5, 22, 39, 23, 34, 25, 4, 40, 26, 26, 45, 18, 28, 61, 59, 17, 46, 39, 46, 68, 24, 63, 59, 67, 53, 9, 11, 3, 44, 24, 37, 13, 1, 65, 18, 48};


	cout << t.solve(n, b, c) << endl;

	return 0;
}
#endif