
/*
 * Given any source and destination point on a chess board,
 * we need to find whether Knight can move to the destination or not.
 */

#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class KnightOnChessBoard {
public:
	bool valid(int x, int y, int n, int m) {
		if (x < 0 || x >= n || y < 0 || y >= m) return false;
		return true;
	}

#if 0
	int knight(int n, int m, int x1, int y1, int x2, int y2) {
		if (x1 < 0 || x1 > n || x2 < 0 || x2 > n ||
			y1 < 0 || y1 > m || y2 < 0 || y2 > m) return -1;
		if (x1 == x2 && y1 == y2) return 0;
		
		vector<vector<int>> d = {{  2, 1 }, {  1, 2 },
								 {  2,-1 }, {  1,-2 },
								 { -2, 1 }, { -1, 2 },
								 { -2,-1 }, { -1,-2 }};
		vector<vector<bool>> b(n, vector<bool>(m, false));
		vector<vector<int>> dist(n, vector<int>(m));

		x1--; x2--; y1--; y2--;
		queue<pair<int, int>> q;
		q.push(make_pair(x1, y1));
		b[x1][y1] = true;
		dist[x1][y1] = 0;
		while (!q.empty()) {
			pair<int, int> p = q.front(); q.pop();
			int x = p.first;
			int y = p.second;

			if (x == x2 && y == y2) return dist[x][y];

			for (int k = 0; k < (int)d.size(); k++) {
				int xx = x + d[k][0];
				int yy = y + d[k][1];

				if (valid(xx, yy, n, m) == false || b[xx][yy] == true) continue;

				b[xx][yy] = true;
				dist[xx][yy] = dist[x][y] + 1;
				q.push(make_pair(xx, yy));
			}
		}
	
		return -1;
	}
#else
	int knight(int n, int m, int x1, int y1, int x2, int y2) {
		if (x1 < 0 || x1 > n || x2 < 0 || x2 > n ||
			y1 < 0 || y1 > m || y2 < 0 || y2 > m) return -1;
		if (x1 == x2 && y1 == y2) return 0;

		vector<vector<int>> d = { {  2, 1 }, {  1, 2 },
								 {  2,-1 }, {  1,-2 },
								 { -2, 1 }, { -1, 2 },
								 { -2,-1 }, { -1,-2 } };
		vector<vector<bool>> b(n, vector<bool>(m, false));

		x1--; x2--; y1--; y2--;
		queue<pair<int, int>> q;
		q.push(make_pair(x1, y1));
		b[x1][y1] = true;
		
		int depth = 0;
		while (!q.empty()) {
			int size = q.size() - 1;

			while (size >= 0) {
				pair<int, int> p = q.front(); q.pop();
				int x = p.first;
				int y = p.second;
				if (x == x2 && y == y2) return depth;

				for (int k = 0; k < (int)d.size(); k++) {
					int xx = x + d[k][0];
					int yy = y + d[k][1];

					if (valid(xx, yy, n, m) == false) continue;
					if (b[xx][yy] == true) continue;

					b[xx][yy] = true;
					q.push(make_pair(xx, yy));
				}

				size--;
			}
			depth++;
		}

		return -1;
	}
#endif
};

#if 0
int main(void) {

	KnightOnChessBoard t;
	cout << t.knight(4, 7, 2, 6, 2, 4) << endl;

	return 0;
}
#endif