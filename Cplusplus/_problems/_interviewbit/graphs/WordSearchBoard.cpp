
/*
 * Given a 2D grid of characters and a word, find all occurrences of given word in grid.
 * A word can be matched in all 8 directions at any point. Word is said be found in a direction
 *  if all characters match in this direction (not in zig-zag form).
 * The 8 directions are, Horizontally Left, Horizontally Right, Vertically Up, Vertically Down and
 *  4 Diagonal directions.
 */

#include <vector>
#include <iostream>
using namespace std;

class WordSearchBoard {
public:
	int exist(vector<string>& b, string w) {

		vector<vector<int>> dirs = { {0, -1} , {0, 1}, {-1, 0}, {1, 0} };
		for (int i = 0; i < (int)b.size(); i++)
			for (int j = 0; j < (int)b[0].size(); j++)
				if (find(b, i, j, w, 0, dirs) == true) return true;
		
		return false;
	}

	bool find(vector<string>& b, int i, int j, string w, int k, vector<vector<int>>& dirs) {
		if (k == w.size()) return true;
		if (i < 0 || i >= (int)b.size() || j < 0 || j >= (int)b[0].size()) return false;
		if (b[i][j] != w[k])return false;

		bool ret = false;
		for (int t = 0; t < (int)dirs.size(); t++) {
			ret = find(b, i + dirs[t][0], j + dirs[t][1], w, k + 1, dirs);
			if (ret == true) return true;
		}
		
		return ret;
	}
};

#if 0
int main(void) {
	vector<string> b = { { "ABCE" } , { "SFCS" }, { "ADEE" } };
	string w = "ABFSAB";
	
	WordSearchBoard t;
	cout << t.exist(b, w) << endl;

	return 0;
}
#endif