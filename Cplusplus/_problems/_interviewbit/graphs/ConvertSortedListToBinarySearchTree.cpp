
/*
 * Given a singly linked list where elements are sorted in ascending order,
 *  convert it to a height balanced BST.
 */

#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class ConvertSortedListToBinarySearchTree {
public:
	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	struct ListNode {
		int val;
		ListNode *next;
		ListNode(int x) : val(x), next(NULL) {}
	};

	TreeNode* sortedListToBST(ListNode* L) {
		TreeNode* n = NULL;
		if (L == NULL) return n;
		else if (L->next == NULL) n = new TreeNode(L->val);
		else if (L->next->next == NULL) {
			n = new TreeNode(L->val);
			n->right = new TreeNode(L->next->val);
		} else {
			ListNode* head = new ListNode(0); //sentinel
			head->next = L;
			ListNode* p = head, * t = L, * h = L;
			while (h != NULL && h->next != NULL) {
				p = p->next;
				t = t->next;
				h = h->next->next;
			}

			n = new TreeNode(t->val);
			h = t->next; delete t; p->next = NULL;
			t = head->next; delete head;

			n->left = sortedListToBST(t);
			n->right = sortedListToBST(h);
		}

		return n;
	}

	// for debug purposes
	void inOrderPrint(TreeNode* n, int level) {
		if (n == NULL) return;

		inOrderPrint(n->left, level + 1);
		for (int i = 0; i <= level; i++) cout << "-";
		cout << n->val << endl;
		inOrderPrint(n->right, level + 1);
	}

	// for debug purposes
	ListNode* buildLinkedList(int n) {
		ListNode* L = new ListNode(0);
		ListNode* prev = L;
		for (int i = 1; i < n; i++) {
			ListNode* node = new ListNode(i);
			prev->next = node;
			prev = node;
		}

		return L;
	}
};

#if 0
int main(void) {
	ConvertSortedListToBinarySearchTree t;

	ConvertSortedListToBinarySearchTree::ListNode* ln = t.buildLinkedList(5);
	ConvertSortedListToBinarySearchTree::TreeNode* tn = t.sortedListToBST(ln);
	t.inOrderPrint(tn, 0);

	return 0;
}
#endif