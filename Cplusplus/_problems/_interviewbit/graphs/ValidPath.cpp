
/*
 * There is a rectangle with left bottom as  (0, 0) and right up as (x, y).
 * There are N circles such that their centers are inside the rectangle.
 * Radius of each circle is R. Now we need to find out if it is possible that we can move from (0, 0) to (x, y)
 * without touching any circle.
 *
 * Note : We can move from any cell to any of its 8 adjecent neighbours and we cannot move outside the boundary
 *  of the rectangle at any point of time.
 *
 * Constraints
 *   0 <= x, y, R <= 100
 *   1 <= N <= 1000
 *   Center of each circle would lie within the grid
 *
 * Example:
 *  Input: x = 2  y = 3  N = 1  R = 1  A = [2]  B = [3]
 *  Output:	NO
 *
 * Explanation: There is NO valid path in this case
 */

#include <vector>
#include <iostream>
using namespace std;

class ValidPath {
private:
	bool isInside( vector<int> &x0, vector<int> &y0, int n, int r, int x, int y) {
		long rr = r * r;
		for (int i = 0; i < n; i++) {
			long dx = (x - x0[i]);
			long dy = (y - y0[i]);
			long sqr = dx*dx + dy*dy;
			if (sqr <= rr) return true;
		}

		return false;
	}

	vector<vector<int>> d = { { 1, 0}, { 0, 1}, { 1, 1}, { 1,-1},
							{-1, 0}, { 0,-1}, {-1,-1}, {-1, 1}};

public:
	string solve(int X, int Y, int n, int r, vector<int>& x0, vector<int>& y0) {
		if (n == 0 || x0.size() == 0 || y0.size() == 0) return "NO";

		vector<vector<bool>> marked(X + 1, vector<bool>(Y + 1, false));

		bool ret = DFS(X, Y, x0, y0, r, n, 0, 0, marked);
		return (ret == true ? "YES" : "NO");
	}

	bool DFS(int X, int Y, vector<int>& x0, vector<int>& y0, int r, int n, int x, int y, vector<vector<bool>>& marked) {
		if (x < 0 || x > X || y < 0 || y > Y
			|| marked[x][y] == true || isInside(x0, y0, n, r, x, y) == true) return false;
		if (x == X && y == Y) return true;

		marked[x][y] = true;
		for (int i = 0; i < (int)d.size(); i++) {
			int xx = x + d[i][0];
			int yy = y + d[i][1];
			bool ret = DFS(X, Y, x0, y0, r, n, xx, yy, marked);
			if (ret == true) return true;
		}

		return false;
	}
};

#if 0
int main() {
	int x = 5;
	int y = 5;
	int N = 2;
	int R = 1;
	vector<int> A = {1, 3};
	vector<int> B = {3, 3};

	ValidPath t;
	string r = t.solve(x, y, N, R, A, B);
	cout<<r;
	return 0;
}
#endif