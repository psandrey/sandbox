
/*
 * Given two words (start and end), and a dictionary, find the shortest transformation sequence
 * from start to end, such that:
 *   - Only one letter can be changed at a time
 *   - Each intermediate word must exist in the dictionary
 *
 * If there are multiple such sequence of shortest length, return all of them.
 * Refer to the example for more details.
 *
 * Given:
 *   start = "hit"
 *   end = "cog"
 *   dict = ["hot","dot","dog","lot","log"]
 *
 * Return
 *
 *  [
 *    ["hit","hot","dot","dog","cog"],
 *    ["hit","hot","lot","log","cog"]
 *  ]
 * Note:
 *  All words have the same length.
 *  All words contain only lowercase alphabetic characters.
 */

#include <queue>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class WordLadder_II {
public:

	vector<vector<string> > findLadders(string start, string end, vector<string>& d) {
		vector<vector<string>> paths;
		vector<string> ans;

		// 0. handle corner cases
		if (hamming(start, end) == 0)
			paths.push_back(vector<string>({ start }));
		else if (hamming(start, end) == 1)
			paths.push_back(vector<string>({ start, end }));
		else {

			sort(d.begin(), d.end());
			int k = 0;
			for (int i = 1; i < (int)d.size(); i++)
				if (d[i] != d[i - 1]) d[++k] = d[i];
			d.erase(d.begin() + k + 1, d.end());

			// check if a and b strings are in the dictionary (if not, add them)
			int s = -1, t = -1;
			for (int i = 0; i < (int)d.size(); i++) {
				if (s == -1 || t == -1) {
					if (s == -1 && start.compare(d[i]) == 0) s = i;
					if (t == -1 && end.compare(d[i]) == 0) t = i;
				}
				else break;
			}
			if (s == -1) { s = (int)d.size(); d.push_back(start); }
			if (t == -1) { t = (int)d.size(); d.push_back(end); }

			// build adjacency list
			int n = (int)d.size();
			vector<vector<int>> adjL(n, vector<int>());
			for (int u = 0; u < n - 1; u++) {
				for (int v = u + 1; v < n; v++) {
					if (hamming(d[u], d[v]) == 1) {
						adjL[u].push_back(v);
						adjL[v].push_back(u);
					}
				}
			}

			// 4. BFS to get the shortest distances
			vector<vector<int>> pred(n, vector<int>());
			vector<bool> marked(n, false);
			vector<int> dist(n, 0);
			queue<int> q;
			q.push(s); dist[s] = 1; marked[s] = true;
			while (!q.empty()) {
				int u = q.front(); q.pop();
				if (u == t) break;
				if (adjL[u].empty()) continue;

				for (int j = 0; j < (int)adjL[u].size(); j++) {
					int v = adjL[u][j];
					if (marked[v] == true && dist[v] == dist[u] + 1) pred[v].push_back(u);
					else if (marked[v] == false) {
							q.push(v);
							marked[v] = true;
							dist[v] = dist[u] + 1;
							pred[v].push_back(u);
					}
				}
			}

			// 5. build paths
			if (!pred[t].empty()) {
				vector<int> st;
				st.push_back(t);
				build_paths(d, pred, s, t, st, paths);
				st.pop_back();
			}
		}

		return paths;
	}

	int hamming(string& a, string& b) {
		int n = 0;
		int i = -1;
		while (n < 2 && ++i < (int)a.length()) {
			if (a[i] != b[i]) n++;
		}

		return n;
	}

	void build_paths(vector<string>& dict, 
					 vector<vector<int>>& pred, int end, int i, vector<int>& st,
					 vector<vector<string>>& paths) {
		if (i == end) {
			vector<string> ans;
			for (int k = st.size() - 1; k >= 0; k--) ans.push_back(dict[st[k]]);
			paths.push_back(ans);
		} else if (!pred[i].empty()) {
			for (int j = 0; j < (int)pred[i].size(); j++) {
				st.push_back(pred[i][j]);
				build_paths(dict, pred, end, pred[i][j], st, paths);
				st.pop_back();
			}
		}
	}
};

#if 0
int main(void) {
	vector<string> dictV = { "hot","dot","dog","lot","log" };
	string start = "hit";
	string end = "cog";
	//vector<string> dictV;
	//string start = "bb";
	//string end = "bb";


	WordLadder_II t;
	vector<vector<string>> ans = t.findLadders(start, end, dictV);

	for (vector<string> l : ans) {
		cout << endl << "[";
		for (string s : l) cout<< " ,"  << s;
		cout << "]";
	}

	return 0;
}
#endif