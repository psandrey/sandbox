
/*
 * Given a 2D board containing 'X' and 'O', capture all regions surrounded by 'X'.
 * A region is captured by flipping all 'O's into 'X's in that surrounded region.
 */

#include <vector>
#include <iostream>
using namespace std;

class CaptureRegionsOnBoard {
public:
	void solve(vector<vector<char> >& a) {
		if (a.empty() || a[0].empty()) return;
		int n = (int)a.size(), m = (int)a[0].size();
		
		// the O's that are not going to be flipped are the ones that touches the borders
		for (int i = 0; i < n; i++) {
			do_not_flip(a, i, 0);
			do_not_flip(a, i, m - 1);
		}
		for (int j = 0; j < m; j++) {
			do_not_flip(a, 0, j);
			do_not_flip(a, n - 1, j);
		}

		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				if (a[i][j] == 'Y') a[i][j] = 'O';
				else a[i][j] = 'X';
	}

	void do_not_flip(vector<vector<char> >& a, int i, int j) {
		int n = (int)a.size(), m = (int)a[0].size();
		if (i == -1 || j == -1 || i == n || j == m) return;
		if (a[i][j] != 'O') return;
	
		a[i][j] = 'Y';

		do_not_flip(a, i + 1, j);
		do_not_flip(a, i - 1, j);
		do_not_flip(a, i, j + 1);
		do_not_flip(a, i, j - 1);
	}

	void print_board(vector<vector<char>>& a) {
		for (int i = 0; i < (int)a.size(); i++) {
			for (int j = 0; j < (int)a[0].size(); j++)
				cout << a[i][j] << " ";
			cout << endl;
		}
	}
};

#if 0
int main(void) {
	vector<vector<char>> a = {
		{'X', 'O', 'X', 'X', 'X', 'X', 'O', 'O', 'X', 'X'},
		{'X', 'O', 'O', 'O', 'O', 'X', 'O', 'O', 'X', 'X'},
		{'O', 'X', 'X', 'O', 'O', 'X', 'X', 'X', 'O', 'O'},
		{'O', 'X', 'O', 'X', 'O', 'O', 'O', 'X', 'X', 'O'},
		{'O', 'X', 'O', 'O', 'X', 'X', 'O', 'O', 'X', 'X'},
		{'O', 'X', 'X', 'X', 'O', 'X', 'X', 'O', 'X', 'O'},
		{'O', 'O', 'X', 'X', 'X', 'X', 'O', 'X', 'O', 'O'} };
	CaptureRegionsOnBoard t;

	t.print_board(a);
	t.solve(a);
	t.print_board(a);

	return 0;
}
#endif