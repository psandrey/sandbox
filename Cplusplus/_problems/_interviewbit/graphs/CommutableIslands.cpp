
/*
 * There are n islands and there are many bridges connecting them.
 * Each bridge has some cost attached to it.
 * We need to find bridges with minimal cost such that all islands are connected.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class CommutableIslands {
public:
	int solve(int n, vector<vector<int> >& bridges) {
		struct {
			bool operator()(const vector<int>& a, const vector<int>& b) {
				return (a[2] < b[2]);
			}
		} compare;
		sort(bridges.begin(), bridges.end(), compare);

		// init disjoint sets
		vector<int> ds_r(n, 0);
		vector<int> ds_p(n); for (int i = 0; i < n; i++) ds_p[i] = i;

		int min_cost = 0;
		for (int i = 0; i < (int)bridges.size(); i++) {
			int u = bridges[i][0] - 1;
			int v = bridges[i][1] - 1;
			int w = bridges[i][2];

			if (ds_find(u, ds_p) != ds_find(v, ds_p)) {
				min_cost += w;
				ds_union(u, v, ds_r, ds_p);
			}
		}

		return min_cost;
	}

	int ds_find(int u, vector<int>& ds_p) {
		if (ds_p[u] != u) ds_p[u] = ds_find(ds_p[u], ds_p);

		return ds_p[u];
	}

	void ds_union(int u, int v, vector<int>& ds_r, vector<int>& ds_p) {
		int pu = ds_find(u, ds_p);
		int pv = ds_find(v, ds_p);
		if (ds_r[pu] > ds_r[pv]) ds_p[pv] = pu;
		else if (ds_r[pu] < ds_r[pv]) ds_p[pu] = pv;
		else { ds_p[pv] = pu; ds_r[u]++; }
	}
};

#if 0
int main(void) {
	vector<vector<int>> b = {{ 1, 2, 1 }, { 2, 3, 4 }, { 1, 4, 3 }, { 4, 3, 2 }, { 1, 3, 10 }};
	int n = 4;

	CommutableIslands t;
	cout << t.solve(n, b) << endl;

	return 0;
}
#endif