
/*
 * Given an arbitrary unweighted rooted tree which consists of N (2 <= N <= 40000) nodes.
 * The goal of the problem is to find largest distance between two nodes in a tree.
 * Distance between two nodes is a number of edges on a path between the nodes
 * (there will be a unique path between any pair of nodes since it is a tree).
 * The nodes will be numbered 0 through N - 1.
 *
 * The tree is given as an array P, there is an edge between nodes P[i] and i (0 <= i < N).
 * Exactly one of the i�s will have P[i] equal to -1, it will be root node.
 */

#include <unordered_map>
#include <stack>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class LargestDistanceBetweenNodesOfATree {
public:
#if 0
	/* This is the fastest solution, however if the nodes are not given in the reverse order of their depth,
	 * it will return wrong answer (or, the tree must be level serialized).
	 */
	int solve(vector<int>& a) {
		if (a.empty() || 2 > (int)a.size()) return 0;

		int n = (int)a.size();
		vector<int> height(n, 0);
		vector<int> max_path(n, 0);
		int ans = 0;

		for (int i = n - 1; i > 0; i--) {
			int u = i;
			int p = a[i];

			// update max_path thru p
			max_path[p] = max(max_path[p], height[p] + height[u] + 1);
		
			// update height of p
			height[p] = max(height[p], height[u] + 1);

			// update ans
			ans = max(ans, max_path[p]);
		}

		return ans;
	}

#else
	/* Classic solution:
	 * The longest path must have a root, so for each node compute the longest path for the subtree rooted in in.
	 * The longest path is the deepest branch and second deepest branch + the edges that connects them to the root.
	 */
	int solve(vector<int>& a) {
		if (a.empty() || 2 > (int)a.size()) return 0;
		
		// build tree from a
		int n = (int)a.size();
		vector<vector<int>> tree(n, vector<int>());
		int root = -1;
		for (int i = 0; i < n; i++) {
			if (a[i] != -1) tree[a[i]].push_back(i);
			else root = i;
		}

		int diameter = 0;
		DFS(tree, root, &diameter);

		return diameter;
	}

	int DFS(vector<vector<int>>& tree, int u, int* diameter) {
		if (tree[u].empty()) return 0;

		int h1 = 0, h2 = 0;
		for (int v : tree[u]) {
			int h = 1 + DFS(tree, v, diameter);

			if (h > h1) { h2 = h1; h1 = h; }
			else if (h > h2) h2 = h;
		}

		*diameter = max(*diameter, h1 + h2);
		return h1;
	}
#endif
};

#if 0
int main(void) {
#if 0
	//vector<int> parent = { -1, 0, 1, 1, 3, 0, 4, 0, 2, 8, 9, 0 };
	vector<int> parent = { -1, 0, 1, 4, 1, 0, 3, 0, 2, 8, 9, 0 };
#else
	vector<int> parent = {-1, 0, 1, 1, 2, 0, 5, 0, 3, 0, 0, 2, 3, 1, 12, 14, 0, 5, 9, 6, 16, 0, 13, 4, 17, 2, 1, 22, 14,
						    20, 10, 17, 0, 32, 15, 34, 10, 19, 3, 22, 29, 2, 36, 16, 15, 37, 38, 27, 31, 12, 24, 29, 17,
						    29, 32, 45, 40, 15, 35, 13, 25, 57, 20, 4, 44, 41, 52, 9, 53, 57, 18, 5, 44, 29, 30, 9, 29,
							30, 8, 57, 8, 59, 59, 64, 37, 6, 54, 32, 40, 26, 15, 87, 49, 90, 6, 81, 73, 10, 8, 16 };
#endif
	LargestDistanceBetweenNodesOfATree t;
	cout << t.solve(parent) << endl;
}
#endif