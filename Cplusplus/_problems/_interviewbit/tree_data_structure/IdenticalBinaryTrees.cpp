
/*
 * Given two binary trees, write a function to check if they are equal or not.
 * Two binary trees are considered equal if they are structurally identical and
 * the nodes have the same value.
 *
 * Return 0 / 1 ( 0 for false, 1 for true ) for this problem
 */

#include <algorithm>
#include <queue>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class IdenticalBinaryTrees {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	int check_it(TreeNode* a, TreeNode* b) {
		if (a == NULL && b == NULL) return 1;
		if (a == NULL || b == NULL) return 0;

		stack<TreeNode*> sta;
		stack<TreeNode*> stb;
		sta.push(a);
		stb.push(b);
		while (!sta.empty() && !stb.empty()) {
			a = sta.top(); sta.pop();
			b = stb.top(); stb.pop();

			if (a->val != b->val) return 0;

			if (a->right != NULL && b->right != NULL) {
				sta.push(a->right);
				stb.push(b->right);
			}
			else if (!(a->right == NULL && b->right == NULL)) return 0;

			if (a->left != NULL && b->left != NULL) {
				sta.push(a->left);
				stb.push(b->left);
			}
			else if (!(a->left == NULL && b->left == NULL)) return 0;
		}

		return 1;
	}

	int check_rec(TreeNode* a, TreeNode* b) {
		if (a == NULL && b == NULL) return 1;
		if (a == NULL || b == NULL) return 0;
		if (a->val != b->val) return 0;

		int rcl = check_rec(a->left, b->left);
		int rcr = check_rec(a->right, b->right);

		return (rcl && rcr);
	}

	TreeNode* buildTreeSerializationTopDown() {
		vector<int> a =
		{ 47, 42, 52, 41, 44, 50, 64, 40, -1, 43, 45, 49, 51, 63, 77, -1, -1, -1, -1, -1,
			46, 48, -1, -1, -1, 55, -1, 75, 88, -1, -1, -1, -1, 53, 58, 69, 76, 81, 94, -1,
			54, 56, 60, 68, 73, -1, -1, 79, 87, 92, 100, -1, -1, -1, 57, 59, 61, 66, -1, 72,
			74, 78, 80, 85, -1, 89, 93, 96, 102, -1, -1, -1, -1, -1, 62, 65, 67, 71, -1, -1,
			-1, -1, -1, -1, -1, 84, 86, -1, 90, -1, -1, 95, 99, 101, -1, -1, -1, -1, -1, -1,
			-1, 70, -1, 83, -1, -1, -1, -1, 91, -1, -1, 98, -1, -1, -1, -1, -1, 82, -1, -1,
			-1, 97, -1, -1, -1, -1, -1 };

		TreeNode* root = new TreeNode(a[0]);
		int n = (int)a.size();

		queue<TreeNode*> q;
		q.push(root);

		int i = 1;
		int size;
		while (i < n) {

			size = q.size();
			for (int j = 0; j < size; j++) {
				TreeNode* node = q.front();
				q.pop();
				if (a[i] != -1) { node->left = new TreeNode(a[i]); q.push(node->left); }
				i++;
				if (a[i] != -1) { node->right = new TreeNode(a[i]); q.push(node->right); }
				i++;
			}
		}

		return root;
	}
};

#if 0
int main(void) {

	IdenticalBinaryTrees t;

	IdenticalBinaryTrees::TreeNode* root = t.buildTreeSerializationTopDown();
	cout << t.check_it(root, root) << endl;
	cout << t.check_rec(root, root) <<endl;

	return 0;
}
#endif