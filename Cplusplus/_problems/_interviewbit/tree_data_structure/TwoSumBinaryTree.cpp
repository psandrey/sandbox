
/*
 * Given a binary search tree T, where each node contains a positive integer,
 * and an integer K, you have to find whether or not there exist two different
 * nodes A and B such that A.value + B.value = K.
 *
 * Return 1 to denote that two such nodes exist. Return 0, otherwise.
 */

#include <algorithm>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class TwoSumBinaryTree {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	int t2Sum(TreeNode* root, int K) {
		if (root == NULL) return 0;

		stack<TreeNode*>st_inorder;
		TreeNode* n = root;
		while (n != NULL) { st_inorder.push(n); n = n->left; }

		stack<TreeNode*>st_revorder;
		n = root;
		while (n != NULL) { st_revorder.push(n); n = n->right; }

		while (st_inorder.top() != st_revorder.top()) {
			int sum = st_inorder.top()->val + st_revorder.top()->val;

			if (sum == K) return 1;
			else if (sum < K) {
				n = st_inorder.top(); st_inorder.pop();
				n = n->right;
				while (n != NULL) { st_inorder.push(n); n = n->left; }
			}
			else {
				n = st_revorder.top(); st_revorder.pop();
				n = n->left;
				while (n != NULL) { st_revorder.push(n); n = n->right; }

			}
		}

		return 0;
	}

	TreeNode* createTree() {
		TreeNode* root = new TreeNode(5);
		root->left = new TreeNode(1);
		root->right = new TreeNode(9);
		root->right->left = new TreeNode(7);
		root->right->left->left = new TreeNode(6);
		root->right->left->right = new TreeNode(8);
		return root;
	}
};

#if 0
int main(void) {
	TwoSumBinaryTree t;

	TwoSumBinaryTree::TreeNode* root = t.createTree();
	cout << t.t2Sum(root, 16);
	return 0;
}
#endif