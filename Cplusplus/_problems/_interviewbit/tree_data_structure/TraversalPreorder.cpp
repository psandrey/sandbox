
/*
 * Given a binary tree, return the preorder traversal of its nodes� values.
 */

#include <algorithm>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class TraversalPreorder {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	vector<int> preorder(TreeNode* root) {
		vector<int> ans;
		if (root == NULL) return ans;

		stack<TreeNode*> st; st.push(root);
		while (!st.empty()) {
			TreeNode* n = st.top(); st.pop();
			ans.push_back(n->val);
			
			if (n->right != NULL) st.push(n->right);
			if (n->left != NULL) st.push(n->left);
		}

		return ans;
	}

	TreeNode* createTree() {
		TreeNode* root = new TreeNode(3);
		root->left = new TreeNode(9);
		root->right = new TreeNode(20);
		root->left->left = new TreeNode(1);
		root->left->right = new TreeNode(5);
		root->right->left = new TreeNode(15);
		root->right->right = new TreeNode(7);

		return root;
	}
};

#if 0
int main(void) {
	TraversalPreorder t;
	TraversalPreorder::TreeNode* root = t.createTree();
	vector<int> ans = t.preorder(root);
	for (int x : ans) cout << x << " ";
	return 0;
}
#endif