
/*
 * Given a binary tree, return the inorder traversal of its nodes� values.
 */

#include <algorithm>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class TraversalInorder {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	vector<int> inorder(TreeNode* root) {
		vector<int> ans;
		if (root == NULL) return ans;

		stack<TreeNode*> st;
		TreeNode* n = root;
		while (n != NULL || !st.empty()) {
			if (n != NULL) {
				st.push(n);
				n = n->left;
			} else {
				n = st.top(); st.pop();
				ans.push_back(n->val);

				n = n->right;
			}
		}

		return ans;
	}

	vector<int> inorderv2(TreeNode* root) {
		vector<int> ans;
		if (root == NULL) return ans;

		stack<TreeNode*> st;
		TreeNode* n = root;
		while (n != NULL || !st.empty()) {
			while (n != NULL) { st.push(n); n = n->left; }

			n = st.top(); st.pop();
			ans.push_back(n->val);

			n = n->right;
		}

		return ans;
	}

	TreeNode* createTree() {
		TreeNode* root = new TreeNode(3);
		root->left = new TreeNode(9);
		root->right = new TreeNode(20);
		root->left->left = new TreeNode(1);
		root->left->right = new TreeNode(5);
		root->right->left = new TreeNode(15);
		root->right->right = new TreeNode(7);

		return root;
	}
};

#if 0
int main(void) {
	TraversalInorder t;
	TraversalInorder::TreeNode* root = t.createTree();

	vector<int> ans = t.inorder(root);
	for (int x : ans) cout << x << " ";
	cout << endl;

	ans = t.inorderv2(root);
	for (int x : ans) cout << x << " ";

	return 0;
}
#endif