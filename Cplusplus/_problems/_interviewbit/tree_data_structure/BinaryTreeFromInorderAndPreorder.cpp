
/*
 * Given preorder and inorder traversal of a tree, construct the binary tree.
 */

#include <algorithm>
#include <vector>
#include <unordered_map>
#include <iostream>
using namespace std;

class BinaryTreeFromInorderAndPreorder {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	TreeNode* buildTree(vector<int>& in, vector<int>& pre) {
		unordered_map<int, int> ump;
		for (int i = 0; i < (int)in.size(); i++)
			ump[in[i]] = i;

		return helper(in, 0, in.size() - 1, pre, 0, pre.size() - 1, ump);
	}

	TreeNode* helper(vector<int>& in, int ii, int ij, vector<int>& pre, int pi, int pj,
		unordered_map<int, int>& ump) {
		if (ii > ij) return NULL;

		TreeNode* n = new TreeNode(pre[pi]);

		int k = ump[pre[pi]];
		n->left = helper(in, ii, k - 1, pre, pi + 1, pi + (k - ii), ump);
		n->right = helper(in, k + 1, ij, pre, pi + 1 + (k - ii), pj, ump);
		return n;
	}

	void postOrder(TreeNode* node) {
		if (node == NULL) return;
		postOrder(node->left);
		postOrder(node->right);
		cout << node->val << " ";
	}
};

#if 0
int main(void) {
	vector<int> in = { 4, 8, 2, 5, 1, 6, 3, 7 };
	vector<int> pre = { 1, 2, 4, 8, 5, 3, 6, 7 };

	BinaryTreeFromInorderAndPreorder t;
	BinaryTreeFromInorderAndPreorder::TreeNode* root = t.buildTree(in, pre);

	t.postOrder(root);
	/* Should be:
	 * Postorder of the constructed tree :
	 * 8 4 5 2 6 7 3 1
	 */

	return 0;
}
#endif