
/*
 * Populate each next pointer to point to its next right node.
 * If there is no next right node, the next pointer should be set to NULL.
 *
 * Initially, all next pointers are set to NULL.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class PopulateNextRightPointersTree {
public:

	struct TreeLinkNode {
		int val;
		TreeLinkNode* left;
		TreeLinkNode* right;
		TreeLinkNode* next;
		TreeLinkNode() : val(-1), left(NULL), right(NULL), next(NULL) {}
	};

	void connect(TreeLinkNode* root) {
		if (root == NULL) return;
		TreeLinkNode* h = root, * n = NULL, * p = NULL;
		
		while (h != NULL) {
			p = h;
			
			// find next head
			while (h != NULL && h->left == NULL && h->right == NULL) h = h->next;
			if (h == NULL) break;

			// set next head and current node at current level
			h = (h->left != NULL ? h->left : h->right);
			n = h;

			// set next pointer to nodes at current level
			while (n != NULL) {

				// find next pointer for current node
				while ((p != NULL) && 
						((p->left == NULL && p->right == NULL) ||
						 (p->left == n    && p->right == NULL) ||
						 (p->right == n))) p = p->next;
				if (p == NULL) break;

				if (p->left != NULL && p->left != n) n->next = p->left;
				else n->next = p->right;

				// next current node
				n = n->next;
			}
		}
	}

	TreeLinkNode* buildTestTree() {
		TreeLinkNode* root = new TreeLinkNode(); root->val = 1;
		root->left = new TreeLinkNode(); root->left->val = 2;
		root->right = new TreeLinkNode(); root->right->val = 3;

		root->left->left = new TreeLinkNode(); root->left->left->val = 4;
		root->right->left = new TreeLinkNode(); root->right->left->val = 6;
		return root;
	}

	void traverseNext(TreeLinkNode* root) {
		TreeLinkNode* head,* prevHead;
		head = root;
		while (head != NULL) {
			prevHead = head;

			while (head != NULL) {
				cout << head->val << ", ";
				head = head->next;
			}
			cout << endl;

			TreeLinkNode* prevCur = prevHead;
			while (prevCur != NULL && prevCur->left == NULL && prevCur->right == NULL)
				prevCur = prevCur->next;

			if (prevCur != NULL)
				head = (prevCur->left != NULL ? prevCur->left : prevCur->right);
		}
	}
};

#if 0
int main(void) {
	PopulateNextRightPointersTree t;
	PopulateNextRightPointersTree::TreeLinkNode* root = t.buildTestTree();
	t.connect(root);
	t.traverseNext(root);
	return 0;
}
#endif