
/*
 * Given a binary tree, check whether it is a mirror of itself (i.e., symmetric around its center).
 */

#include <algorithm>
#include <queue>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class SymmetricBinaryTree {
public :

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	// iterative
	int is_symetric_it(TreeNode* root) {
		if (root == NULL) return 1;
		TreeNode* a = root->left;
		TreeNode* b = root->right;

		if (a == NULL && b == NULL) return 1;
		if (a == NULL || b == NULL) return 0;

		stack<TreeNode*> sta; sta.push(a);
		stack<TreeNode*> stb; stb.push(b);
		while (!sta.empty() && !stb.empty()) {
			a = sta.top(); sta.pop();
			b = stb.top(); stb.pop();

			if (a->val != b->val) return 0;
		
			if (a->left != NULL && b->right != NULL) {
				sta.push(a->left);
				stb.push(b->right);
			}
			else if (!(a->left == NULL && b->right == NULL)) return 0;

			if (a->right != NULL && b->left != NULL) {
				sta.push(a->right);
				stb.push(b->left);
			}
			else if (!(a->right == NULL && b->left == NULL)) return 0;

		}

		return 1;
	}

	TreeNode* buildTreeSerializationTopDown() {
		vector<int> a =
		{ 0, 4, 4, 10, 6, 6, 10, 16, 3, 14, 9, 9, 14, 3, 16, 13, -1, 7, 11, -1, -1, -1, 15,
		15, -1, -1, -1, 11, 7, -1, 13, -1, 12, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1, 1,
		-1, 12, -1, 8, -1, 5, 2, 2, 5, -1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1 };

		TreeNode* root = new TreeNode(a[0]);
		int n = (int)a.size();

		queue<TreeNode*> q;
		q.push(root);

		int i = 1;
		int size;
		while (i < n) {

			size = q.size();
			for (int j = 0; j < size; j++) {
				TreeNode* node = q.front();
				q.pop();
				if (a[i] != -1) { node->left = new TreeNode(a[i]); q.push(node->left); }
				i++;
				if (a[i] != -1) { node->right = new TreeNode(a[i]); q.push(node->right); }
				i++;
			}
		}

		return root;
	}
};

#if 0
int main(void) {

	SymmetricBinaryTree t;
	SymmetricBinaryTree::TreeNode* root = t.buildTreeSerializationTopDown();
	cout << t.is_symetric_it(root) << endl;

	return 0;
}
#endif