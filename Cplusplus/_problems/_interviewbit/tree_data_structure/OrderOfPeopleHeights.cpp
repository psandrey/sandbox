
/*
 * You are given the following :
 *  A positive number N
 *  Heights : A list of heights of N persons standing in a queue
 *  Infronts : A list of numbers corresponding to each person (P) that gives the number of persons who are taller
 *  than P and standing in front of P
 *
 *  You need to return list of actual order of personsís height.
 *  Note: Consider that heights will be unique
 */

#include <unordered_map>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class OrderOfPeopleHeights {
public:
	// version 1: T(n) = O(n^2), S(n) = O(1)
	vector<int> order(vector<int>& h, vector<int>& f) {
		int n = (int)h.size();
		vector<int> ans;
		if (n == 0) return ans;

		vector<pair<int, int>> hf;
		for (int i = 0; i < n; i++) hf.push_back(make_pair(h[i], f[i]));
		
		// sort
		sort(hf.rbegin(), hf.rend());

		// re-organize
		for (int i = 0; i < n; i++) {
			if (hf[i].second == i) continue;

			pair<int, int> tmp = hf[i];
			for (int j = i; j > tmp.second; j--) hf[j] = hf[j - 1];
			hf[tmp.second] = tmp;
		}

		// build answer
		for (int i = 0; i < n; i++) ans.push_back(hf[i].first);

		return ans;
	}

	// version 2: segment-tree
	// T(n) = O(n.logn), S(n) = O(n)
	vector<int> order_v2(vector<int>& h, vector<int>& f) {
		int n = (int)h.size();
		if (n == 0) return vector<int>();

		// each leaf contains at most one human, thus is 2 * 2^p,
		//  where p = ceill(log2(n))
		int p = (int)ceill(log2((double) n));
		int m = (1 << (p + 1)); // 2 * 2^p
		vector<int> sgt(m, 0);
		build_sgt(sgt, 0, 0, n - 1);

		// insert them in ascending order not to affect the infronts of upcomming insertions
		vector<pair<int, int>> hf(n);
		for (int i = 0; i < n; i++) hf[i] = make_pair(h[i], f[i]);
		sort(hf.begin(), hf.end());

		vector<int> ans(n);
		for (int i = 0; i < n; i++) {
			int slot = upd_querry_sgt(sgt, 0, 0, n - 1, hf[i].second);
			ans[slot] = hf[i].first;
		}

		return ans;
	}

	int build_sgt(vector<int>& sgt, int k, int lo, int hi) {
		if (lo == hi) sgt[k] = 1;
		else {
			int m = (hi + lo) / 2;
			int l = 2 * k + 1, r = 2 * k + 2;
 
			int left_slots  = build_sgt(sgt, l, lo, m);
			int right_slots = build_sgt(sgt, r, m + 1, hi);
			sgt[k] = left_slots + right_slots;
		}

		return sgt[k];
	}

	int upd_querry_sgt(vector<int>& sgt, int k, int lo, int hi, int slots) {
		int slot = lo;
		if (lo == hi) sgt[k] = 0;
		else {
			int m = (hi + lo) / 2;
			int l = 2 * k + 1, r = 2 * k + 2;
			if (slots < sgt[l]) // go left
				slot = upd_querry_sgt(sgt, l, lo, m, slots);
			else // go right
				slot = upd_querry_sgt(sgt, r, m + 1, hi, slots - sgt[l]);

			// update slots as: left + right
			sgt[k] = sgt[2 * k + 1] + sgt[2 * k + 2];
		}

		return slot;
	}
};

#if 0
int main(void) {

	vector<int> h = { 5, 3, 2, 6, 1, 4 };
	vector<int> f = { 0, 1, 2, 0, 3, 2 };

	OrderOfPeopleHeights t;
	//vector<int> ans = t.order(h, f);
	vector<int> ans = t.order_v2(h, f);

	if (!ans.empty()) {
		for (int x : ans) cout << x << " ";
	}

	return 0;
}
#endif