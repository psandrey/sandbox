
/*
 * Given a binary tree, determine if it is height-balanced.
 */

#include <algorithm>
#include <iostream>
using namespace std;

class BalancedBinaryTree {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	int isBalanced(TreeNode* root) {
		if (root == NULL) return 1;
		
		int h = height(root);
		return (h == -1 ? 0 : 1);
	}

	int height(TreeNode* n) {
		if (n == NULL) return 0;

		int hL = height(n->left);
		if (hL == -1) return -1;
		
		int hR = height(n->right);
		if (hR == -1) return -1;

		if (abs(hL - hR) > 1)return -1;

		return (max(hL, hR) + 1);
	}

	TreeNode* createTree() {
		TreeNode* root = new TreeNode(3);
		root->left = new TreeNode(9);
		root->right = new TreeNode(20);
		root->left->left = new TreeNode(1);
		root->left->right = new TreeNode(5);
		root->right->left = new TreeNode(15);
		root->right->right = new TreeNode(7);

		return root;
	}
};

#if 0
int main(void) {
	BalancedBinaryTree t;

	BalancedBinaryTree::TreeNode* root = t.createTree();
	cout << t.isBalanced(root) << endl;
	return 0;
}
#endif