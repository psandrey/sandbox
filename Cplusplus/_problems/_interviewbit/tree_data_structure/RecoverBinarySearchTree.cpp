
/*
 * Two elements of a binary search tree (BST) are swapped by mistake.
 * Tell us the 2 values swapping which the tree will be restored.
 */

#include <algorithm>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class RecoverBinarySearchTree {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	vector<int> recoverTree(TreeNode* root) {
		vector<int> ans;
		if (root == NULL) return ans;

		TreeNode* pred = NULL, * n = root,
				* first = NULL, * second = NULL;

#if 0
		// recursive 
		helper(root, &pred, &first, &second);
#else
		// iterative
		stack<TreeNode*>st;
		while (n != NULL || !st.empty()) {
			while (n != NULL) { st.push(n); n = n->left; }

			n = st.top(); st.pop();

			if (pred != NULL && pred->val > n->val) {
				if (first == NULL) { first = pred; second = n; }
				else second = n;
			}

			pred = n;
			n = n->right;
		}
#endif
		if (first != NULL && second != NULL) {
			ans.push_back(min(first->val, second->val));
			ans.push_back(max(first->val, second->val));
		}
		return ans;
	}

	void helper(TreeNode* n, TreeNode** prev, TreeNode * *f, TreeNode * *s) {
		if (n == NULL) return;

		helper(n->left, prev, f, s);
		if ((*prev) != NULL && (*prev)->val > n->val) {
			if ((*f) == NULL) { (*f) = (*prev); (*s) = n; }
			else (*s) = n;
		}
		(*prev) = n;
		helper(n->right, prev, f, s);
	}

	TreeNode* createTree() {
		TreeNode* root = new TreeNode(5);
		root->left = new TreeNode(1);
		root->right = new TreeNode(7);
		root->right->left = new TreeNode(9);
		root->right->left->left = new TreeNode(6);
		root->right->left->right = new TreeNode(8);

		return root;
	}
};

#if 0
int main(void) {
	RecoverBinarySearchTree t;
	
	RecoverBinarySearchTree::TreeNode* root = t.createTree();
	vector<int> ans = t.recoverTree(root);
	if (!ans.empty()) cout << ans[0] << " " << ans[1] << endl;

	return 0;
}
#endif