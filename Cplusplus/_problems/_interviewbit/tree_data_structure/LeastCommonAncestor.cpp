
/*
 * Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree.
 */

#include <algorithm>
#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class LeastCommonAncestor {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	// version 1: building paths
	int LCA_1(TreeNode* root, int a, int b) {
		if (root == NULL) return -1;
		vector<TreeNode*> patha, pathb;
		bool founda = false, foundb = false;
		
		helper_1(root, a, b, patha, &founda, pathb, &foundb);
		
		if (founda == true && foundb == true) {
			int len = min(patha.size(), pathb.size());

			int i = 0;
			for (i = 0; i < len; i++)
				if (patha[i] != pathb[i]) return patha[i - 1]->val;

			if (i == patha.size()) return patha[i - 1]->val;
			if (i == pathb.size()) return pathb[i - 1]->val;
		}

		return -1;
	}

	void helper_1(TreeNode* n, int a, int b, vector<TreeNode*>& patha, bool* founda, vector<TreeNode*>& pathb, bool* foundb) {
		if (n == NULL) return;
		if (*founda == false) patha.push_back(n);
		if (*foundb == false) pathb.push_back(n);
		
		if (n->val == a) *founda = true;
		if (n->val == b) *foundb = true;

		if (*founda == false || *foundb == false) {
			helper_1(n->left, a, b, patha, founda, pathb, foundb);
			helper_1(n->right, a, b, patha, founda, pathb, foundb);
		}

		if (*founda == false) patha.pop_back();
		if (*foundb == false) pathb.pop_back();
	}

	// version 2
	int LCA_2(TreeNode* root, int a, int b) {
		if (root == NULL) return -1;

		int lca = -1;
		find_lca(root, a, b, lca);

		return lca;
	}

	bool find_lca(TreeNode* n, int a, int b, int& lca) {
		if (n == NULL) return false;

		bool found = false;
		if (n->val == a || n->val == b) found = true;
		bool l = find_lca(n->left, a, b, lca);
		bool r = find_lca(n->right, a, b, lca);

		if (found && a == b) lca = n->val;
		else if (found && (l || r)) lca = n->val;
		else if (l && r) lca = n->val;

		return (found || l || r);
	}

	// building trees for testing
	TreeNode *buildBT() {
		TreeNode* root = new TreeNode(3);

		root->left = new TreeNode(5);
		root->right = new TreeNode(1);

		root->left->left = new TreeNode(6);
		root->left->right = new TreeNode(2);

		root->left->right->left = new TreeNode(7);
		root->left->right->right = new TreeNode(4);

		root->right->left = new TreeNode(0);
		root->right->right = new TreeNode(8);

		return root;
	}

	TreeNode* buildTreeSerializationTopDown() {
#if 0
		vector<int> a =
		{ 16, 23, 9, -1, 1, 22, 2, 25, 19, 6, 13, -1, 24, 14, -1, 30,
		4, 26, 29, -1, -1, -1, -1, -1, 3, -1, 8, -1, -1, 12, 18, 28,
		-1, 10, -1, 5, -1, 17, 11, 21, 7, -1, -1, -1, 20, -1, -1, -1,
		-1, -1, 15, -1, -1, -1, -1, -1, -1, -1, 27, -1, -1 };
#else
		vector<int> a =
		{ 1, -1, -1 };
#endif

		TreeNode* root = new TreeNode(a[0]);
		int n = (int)a.size();

		queue<TreeNode *> q;
		q.push(root);

		int i = 1;
		int size;
		while (i < n) {

			size = q.size();
			for (int j = 0; j < size; j++) {
				TreeNode* node = q.front();
				q.pop();
				if (a[i] != -1) { node->left = new TreeNode(a[i]); q.push(node->left); }
				i++;
				if (a[i] != -1) { node->right = new TreeNode(a[i]); q.push(node->right); }
				i++;
			}
		}

		return root;
	}

};

#if 0
int main(void) {

	LeastCommonAncestor t;

	//LeastCommonAncestor::TreeNode* root = t.buildTreeSerializationTopDown();
	//cout << t.LCA_1(root, 32, 24);

	LeastCommonAncestor::TreeNode* root = t.buildTreeSerializationTopDown();
	cout << t.LCA_1(root, 1, 1) << endl;
	cout << t.LCA_2(root, 1, 1) << endl;

	return 0;
}
#endif