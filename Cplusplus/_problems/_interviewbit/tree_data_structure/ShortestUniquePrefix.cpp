
/*
 * Find shortest unique prefix to represent each word in the list.
 * Note: Solution using Trie.
 */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class ShortestUniquePrefix {
	class Trie {
		static const int ALPHABET_SIZE = 26;

		class TrieNode {
		public:
			vector<TrieNode*> children;
			int load;
			bool terminal;

			TrieNode() {
				children = vector<TrieNode*>(ALPHABET_SIZE, nullptr);
				terminal = false;
				load = 0;
			}
			~TrieNode() {
				for (int i = 0; i < ALPHABET_SIZE; i++)
					if (children[i] != nullptr) delete children[i];
			}
		};

		public:
		TrieNode* root = nullptr;
		Trie() { root = new TrieNode; }
		~Trie() { delete root; } // TODO: delete the entire tree

		void insert(string s) {
			int n = (int)s.size();
			TrieNode* node = root;

			for (int i = 0; i < n; i++) {
				int ch_id = s[i] - 'a';
				if (node->children[ch_id] == nullptr) node->children[ch_id] = new TrieNode;
				node = node->children[ch_id];
				node->load++;
			}
		}

		string get_prefix(string s) {
			int n = (int)s.size();

			string prefix;
			TrieNode* node = root;
			for (int i = 0; i < n; i++) {
				prefix.push_back(s[i]);

				int ch_id = s[i] - 'a';
				if (node->children[ch_id]->load == 1) break;
				node = node->children[ch_id];
			}

			return prefix;
		}
	};

public:
	Trie t;

	void build_tree(vector<string>& a) {
		for (string s : a) t.insert(s);
	}

	vector<string> get_prefixes(vector<string>& a) {
		vector<string> ans;
		for (string x : a) {
			ans.push_back(t.get_prefix(x));
		}

		return ans;
	}

	vector<string> solve(vector<string>& a) {
		build_tree(a);
		return get_prefixes(a);
	}
};

#if 0
int main(void) {
	vector<string> a = {"zebra", "dog", "duck", "dove"};
	ShortestUniquePrefix t;
	
	vector<string> prefixes = t.solve(a);
	for (string prefix : prefixes) cout << prefix << endl;

	return 0;
}
#endif