
/*
 * Given a binary tree containing digits from 0-9 only, each root-to-leaf path could represent a number.
 * An example is the root-to-leaf path 1->2->3 which represents the number 123.
 * Find the total sum of all root-to-leaf numbers % 1003.
 */

#include <algorithm>
#include <iostream>
using namespace std;

class SumRootToLeafNumbers {
public:
	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}		
	};

	int sumNumbers(TreeNode* root) {
		if (root == NULL) return 0;

		return helper(root, 0);
	}

	int helper(TreeNode* n, int no) {
		int s = (no * 10 + n->val) % 1003;
		if (n->left == NULL && n->right == NULL) return s;

		int sl = 0, sr = 0;
		if (n->left != NULL) sl = helper(n->left, s);
		if (n->right != NULL) sr = helper(n->right, s);

		return (sl + sr) % 1003;
	}

	TreeNode* buildTestTree() {
		TreeNode* root = new TreeNode(1);

		root->left = new TreeNode(2);
		root->right = new TreeNode(3);

		root->left->left = new TreeNode(4);
		root->left->right = new TreeNode(5);

		root->left->left->left = new TreeNode(7);
		root->left->left->right = new TreeNode(8);

		root->right->right = new TreeNode(6);

		return root;
	}

	void freeTreeTest(TreeNode* n) {
		if (n->left != NULL) freeTreeTest(n->left);
		if (n->right != NULL) freeTreeTest(n->right);

		delete n;
	}
};

#if 0
int main(void) {
	SumRootToLeafNumbers t;

	SumRootToLeafNumbers::TreeNode* root = t.buildTestTree();
	cout << t.sumNumbers(root) << endl;
	t.freeTreeTest(root);

	return 0;
}
#endif