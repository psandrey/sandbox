
/*
 * Given a binary tree, find its minimum depth.
 * The minimum depth is the number of nodes along the shortest path from
 *  the root node down to the nearest leaf node.
 */

#include <algorithm>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class MinDepthOfBinaryTree {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	int minDepth(TreeNode* root) {
		vector<vector<int>> ans;
		if (root == NULL) return 0;

		stack<TreeNode*> st;
		TreeNode* n = root, * p = NULL;
		int h = 0, H = INT_MAX;
		while (n != NULL || !st.empty()) {
			while (n != NULL) {
				st.push(n); h++;
				n = n->left;
			}

			n = st.top();

			if (n->right == NULL || n->right == p) {
				if (n->left == NULL && n->right == NULL) H = min(H, h);

				st.pop(); h--;
				p = n; n = NULL;
			}
			else { n = n->right; p = NULL; }
		}

		return H;
	}

	TreeNode* createTree() {
		TreeNode* root = new TreeNode(5);
		root->left = new TreeNode(4);
		root->right = new TreeNode(8);
		root->left->left = new TreeNode(11);
		root->left->left->left = new TreeNode(7);
		root->left->left->right = new TreeNode(2);

		root->right->left = new TreeNode(13);
		root->right->right = new TreeNode(4);
		root->right->right->left = new TreeNode(5);
		root->right->right->right = new TreeNode(1);
		return root;
	}

};

#if 0
int main(void) {
	MinDepthOfBinaryTree t;
	MinDepthOfBinaryTree::TreeNode* root = t.createTree();
	cout << t.minDepth(root);

	return 0;
}
#endif