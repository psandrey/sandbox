
#include <map>
#include <string>
#include <sstream>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class HoteReviews {
public:
	struct TrieNode {
		TrieNode* childs[26] = { NULL }; // only lower characters
		bool is_word;
	};


	void insert(string s, TrieNode *root) {
		TrieNode* n = root;
		for (int j = 0; j < (int)s.length(); j++) {
			int idx = s[j] - 'a';
			if (n->childs[idx] == NULL) n->childs[idx] = new TrieNode();
			n = n->childs[idx];
		}
		n->is_word = true;
	}

	bool lookup(string s, TrieNode* root) {
		TrieNode* n = root;
		for (int j = 0; j < (int)s.length(); j++) {
			int idx = s[j] - 'a';
			if (n->childs[idx] == NULL) return false;
			n = n->childs[idx];
		}

		if (n != NULL && n->is_word == true) return true;
		return false;
	}

	vector<int> solve(string S, vector<string>& R) {
		vector<int> ans;
		if (S.empty() || R.empty()) return ans;

		// build Trie
		TrieNode* root = new TrieNode();
		stringstream ss(S);
		string word;
		while (getline(ss, word, '_')) { insert(word, root); }
		
		// build score board (like a bucket sort)
		map<int, vector<int>> mp;
		for (int j = 0; j < R.size(); j++) {
			
			// build score for review j
			int score = 0;
			stringstream ss(R[j]);
			string word;
			while (getline(ss, word, '_')) {
				if (lookup(word, root)) score++;
			}

			// insert score in table score
			if (mp.find(score) == mp.end()) mp[score] = vector<int>();
			mp[score].push_back(j);
		}

		// build ans
		for (map<int, vector<int>>::reverse_iterator it = mp.rbegin(); it != mp.rend(); ++it) {
			vector<int> v = (vector<int>)it->second;
			for (int j : v) ans.push_back(j);
		}

		return ans;
	}
};

#if 0
int main(void) {
	string S = "cool_ice_wifi";
	vector<string> R = { "water_is_cool", "cold_ice_drink", "cool_wifi_speed" };
	
	HoteReviews t;
	vector<int> ans = t.solve(S, R);

	for (int j = 0; j < (int)ans.size(); j++)
		cout << ans[j] << " ";

	return 0;
}
#endif