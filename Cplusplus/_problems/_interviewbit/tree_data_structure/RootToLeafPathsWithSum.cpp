
/*
 * Given a binary tree and a sum, find all root-to-leaf paths
 * where each path�s sum equals the given sum.
 */

#include <algorithm>
#include <unordered_map>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class RootToLeafPathsWithSum {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	vector<vector<int>> paths_v1(TreeNode* root, int k) {
		vector<int> st;
		vector<vector<int>> ans;
		DFS(root, st, 0, k, ans);

		return ans;
	}

	void DFS(TreeNode* n, vector<int>& st, int prev_s, int s, vector<vector<int>>& ans) {
		if (n == NULL) return;

		st.push_back(n->val);
		int sum = prev_s + n->val;
		if (n->left == NULL && n->right == NULL && sum == s)
			ans.push_back(vector<int>(st.begin(), st.end()));

		DFS(n->left, st, sum, s, ans);
		DFS(n->right, st, sum, s, ans);
		st.pop_back();
	}

	vector<vector<int>> paths_v2(TreeNode* root, int k) {
		vector<vector<int>> ans;
		if (root == NULL) return ans;

		int s = 0;
		vector<TreeNode*> st;
		TreeNode* n = root, * p = NULL;
		while (n != NULL || !st.empty()) {
			while (n != NULL) {
				st.push_back(n); s += n->val;
				n = n->left;
			}

			n = st.back();

			if (n->right == NULL || n->right == p) {
				if (n->right == NULL && n->left == NULL && s == k)
					ans.push_back(build_vector(st));

				s -= n->val;
				st.pop_back();
				p = n; n = NULL;
			}
			else { n = n->right; p = NULL; }
		}

		return ans;
	}

	vector<int> build_vector(vector<TreeNode*>& st) {
		vector<int> ans;
		int size = (int)st.size();
		for (int i = 0; i < size; i++) ans.push_back(st[i]->val);
		return ans;
	}

	void printListOfLists(vector<vector<int>> ll) {
		for (vector<int> l : ll) {
			cout << "[";
			for (int x : l) cout << x << ", ";
			cout << "]";
			cout << endl;
		}
	}

	TreeNode* createTree() {
		TreeNode* root = new TreeNode(5);
		root->left = new TreeNode(4);
		root->right = new TreeNode(8);
		root->left->left = new TreeNode(11);
		root->left->left->left = new TreeNode(7);
		root->left->left->right = new TreeNode(2);

		root->right->left = new TreeNode(13);
		root->right->right = new TreeNode(4);
		root->right->right->left = new TreeNode(5);
		root->right->right->right = new TreeNode(1);
		return root;
	}
};

#if 0
int main(void) {
	RootToLeafPathsWithSum t;

	RootToLeafPathsWithSum::TreeNode* root = t.createTree();
	vector<vector<int>> ans = t.paths_v1(root, 22);
	t.printListOfLists(ans);
	return 0;
}
#endif