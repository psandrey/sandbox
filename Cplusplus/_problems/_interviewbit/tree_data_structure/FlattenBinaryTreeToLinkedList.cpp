
/*
 * Given a binary tree, flatten it to a linked list in-place (on the right link).
 */

#include <algorithm>
#include <queue>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class FlattenBinaryTreeToLinkedList {
public:
	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	// recursive version
	TreeNode* flatten_1(TreeNode* root) {
		if (root == NULL) return root;
		stack<TreeNode*> st;
		helper(root, st);

		return root;
	}

	void helper(TreeNode* n, stack<TreeNode*>& st) {
		if (n->left != NULL) {
			if (n->right != NULL) st.push(n->right);
			n->right = n->left; n->left = NULL;
			helper(n->right, st);
		}
		else if (n->right != NULL)
			helper(n->right, st);
		else if (!st.empty()) {
			n->right = st.top(); st.pop();
			helper(n->right, st);
		}
	}

	// iterrative (with stack) version
	TreeNode* flatten_2(TreeNode* root) {
		if (root == NULL) return root;
		stack<TreeNode *> st;

// version 1
#if 1
		TreeNode* n = root;
		while (n != NULL || !st.empty()) {
			if (n->left != NULL) {
				if (n->right != NULL) st.push(n->right);
				n->right = n->left; n->left = NULL;
				n = n->right;
			}
			else if (n->right != NULL)
				n = n->right;
			else if (!st.empty()) {
				n->right = st.top(); st.pop();
				n = n->right;
			}
			else n = NULL;
	}
#if 0
		TreeNode* n = root, * p = NULL;
		while (n != NULL || !st.empty()) {
			while (n != NULL && n->left == NULL) { p = n; n = n->right; }

			if (n != NULL) {
				if (n->right != NULL) st.push(n->right);

				n->right = n->left;
				n->left = NULL;
			} else if (!st.empty()) {
				n = st.top(); st.pop();
				p->right = n;
			}
		}
#endif
// version 2
#else
		TreeNode* n = root;
		while (!st.empty()) {

			// the children from the right branch are processed
			//  in reverse order (basically, they will be linked in reverse order
			//  on the right most node flatten from the left branch)
			if (n->right != NULL)
				st.push(n->right);

			if (n->left != NULL) {
				// the right branch was saved in the stack, so it safe
				n->right = n->left;
				n->left = NULL;

			}
			else if (!st.empty()) {
				// if there is nothing on the left branch, then link the top 
				//  node (last right branch decoupled) from the stack
				n->right = st.top();
				st.pop();
			}

			n = n->right;
		}
#endif
		return root;
	}

	TreeNode* buildTreeSerializationTopDown() {
		vector<int> a =
		{ 47, 42, 52, 41, 44, 50, 64, 40, -1, 43, 45, 49, 51, 63, 77, -1, -1, -1, -1, -1,
		 46, 48, -1, -1, -1, 55, -1, 75, 88, -1, -1, -1, -1, 53, 58, 69, 76, 81, 94, -1,
		 54, 56, 60, 68, 73, -1, -1, 79, 87, 92, 100, -1, -1, -1, 57, 59, 61, 66, -1, 72,
		 74, 78, 80, 85, -1, 89, 93, 96, 102, -1, -1, -1, -1, -1, 62, 65, 67, 71, -1, -1,
		 -1, -1, -1, -1, -1, 84, 86, -1, 90, -1, -1, 95, 99, 101, -1, -1, -1, -1, -1, -1,
		 -1, 70, -1, 83, -1, -1, -1, -1, 91, -1, -1, 98, -1, -1, -1, -1, -1, 82, -1, -1,
		 -1, 97, -1, -1, -1, -1, -1
		};

		TreeNode* root = new TreeNode(a[0]);
		int n = (int)a.size();

		queue<TreeNode*> q;
		q.push(root);

		int i = 1;
		int size;
		while (i < n) {

			size = q.size();
			for (int j = 0; j < size; j++) {
				TreeNode* node = q.front();
				q.pop();
				if (a[i] != -1) { node->left = new TreeNode(a[i]); q.push(node->left); }
				i++;
				if (a[i] != -1) { node->right = new TreeNode(a[i]); q.push(node->right); }
				i++;
			}
		}

		return root;
	}

	void print(TreeNode* n) {
		while (n != NULL) {
			cout<<n->val << " ";
			n = n->right;
		}
	}
};

#if 0
int main(void) {
	FlattenBinaryTreeToLinkedList t;
	FlattenBinaryTreeToLinkedList::TreeNode* root = t.buildTreeSerializationTopDown();
	FlattenBinaryTreeToLinkedList::TreeNode* ans = t.flatten_2(root);
	t.print(ans);
	return 0;
}
#endif