
/*
 * Given level-order and inorder traversal of a tree, construct the binary tree.
 */

#include <algorithm>
#include <vector>
#include <unordered_map>
#include <iostream>
using namespace std;

class BinaryTreeFromInorderAndLevel {
public:
	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	TreeNode* buildTree(vector<int>& in, vector<int>& lvl) {
		unordered_map<int, int> mp;
		for (int i = 0; i < (int)in.size(); i++) mp[in[i]] = i;

		return helper(in, 0, in.size() - 1, lvl, mp);
	}

	TreeNode* helper(vector<int>& in, int i, int j, vector<int>& lvl, unordered_map<int, int>& mp) {
		if (i > j) return NULL;
		else if (i == j) return new TreeNode(in[i]);

		TreeNode* n = new TreeNode(lvl[0]);
		int k = mp[lvl[0]];
		vector<int> lvl_l = extract_keys(in, i, k - 1, lvl, mp);
		vector<int> lvl_r = extract_keys(in, k + 1, j, lvl, mp);
		n->left = helper(in, i, k - 1, lvl_l, mp);
		n->right = helper(in, k + 1, j, lvl_r, mp);

		return n;
	}

	vector<int> extract_keys(vector<int>& in, int i, int j, vector<int>& lvl, unordered_map<int, int>& mp) {
		vector<int> ans;
		if (i == j) ans.push_back(in[i]);
		else if (i < j) {
			for (int k = 0; k < (int)lvl.size(); k++) {
				int p = mp[lvl[k]];
				if (p >= i && p <= j) ans.push_back(lvl[k]);
			}
		}

		return ans;
	}

	void postOrder(TreeNode* node) {
		if (node == NULL) return;
		postOrder(node->left);
		postOrder(node->right);
		cout << node->val << " ";
	}
};

#if 0
int main(void) {
	vector<int> in  = { 4, 8, 2, 5, 1, 6, 3, 7 };
	vector<int> lvl = { 1, 2, 3, 4, 5, 6, 7, 8 };
	
	BinaryTreeFromInorderAndLevel t;
	BinaryTreeFromInorderAndLevel::TreeNode* r = t.buildTree(in, lvl);
	t.postOrder(r);
}
#endif