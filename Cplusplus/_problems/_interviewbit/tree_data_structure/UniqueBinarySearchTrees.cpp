
/*
 * Given n, how many structurally unique BST's (binary search trees) that store values 1...n?
 * For example, Given n = 3, there are a total of 5 unique BST's.
 */

#include <algorithm>
#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class UniqueBinarySearchTrees {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	vector<TreeNode*> generateTrees(int n) {
		if (n == 0) return vector<TreeNode*>();

		return build_BST(1, n);
	}

	vector<TreeNode*> build_BST(int s, int e) {
		vector<TreeNode*> ans;
		if (s < e) {
			for (int j = s; j <= e; j++) {
				vector<TreeNode*> l = build_BST(s, j - 1);
				vector<TreeNode*> r = build_BST(j + 1, e);
				for (TreeNode* ln : l) {
					for (TreeNode* rn : r) {
						TreeNode* root = new TreeNode(j);
						root->left = ln;
						root->right = rn;
						ans.push_back(root);
					}
				}
			}
		}
		else if (s == e) ans.push_back(new TreeNode(s));
		else ans.push_back(NULL);

		return ans;
	}

	void printLevelOrder(TreeNode* root) {
		if (root == NULL) return;
		queue<TreeNode*> q;
		q.push(root);
		while (!q.empty()) {
			int size = q.size();
			for (int i = 0; i < size; i++) {
				TreeNode* n = q.front();
				q.pop();
				cout << n->val << " ";
				if (n->left != NULL) q.push(n->left);
				if (n->right != NULL) q.push(n->right);
			}
			cout << endl;
		}
	}
};

#if 0
int main(void) {
	UniqueBinarySearchTrees t;

	vector<UniqueBinarySearchTrees::TreeNode*> ans = t.generateTrees(3);
	for (UniqueBinarySearchTrees::TreeNode* root: ans) {
		t.printLevelOrder(root);
		cout << endl;
	}

	return 0;
}
#endif