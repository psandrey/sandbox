
/*
 * Given a binary tree, return the zigzag level order traversal of its nodes� values.
 * (ie, from left to right, then right to left for the next level and alternate between).
 */

#include <algorithm>
#include <vector>
#include <stack>
#include <iostream>
using namespace std;

class ZigZagLevelOrderTraversalBT {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
		vector<vector<int>> ans;
		if (root == NULL) return ans;
	
		stack<TreeNode*> lr, rl;
		lr.push(root);

		int dir = 0; // 0: left->right, 1: right->left
		while (!lr.empty() || !rl.empty()) {
			vector<int> level;
			if (dir == 0) {
				while (!lr.empty()) {
					TreeNode* n = lr.top(); lr.pop();
					level.push_back(n->val);
					if (n->left != NULL) rl.push(n->left);
					if (n->right != NULL) rl.push(n->right);
				}
				ans.push_back(level);
				dir ^= 1;
			}
			else {
				while (!rl.empty()) {
					TreeNode* n = rl.top(); rl.pop();
					level.push_back(n->val);
					if (n->right != NULL) lr.push(n->right);
					if (n->left != NULL) lr.push(n->left);
				}
				ans.push_back(level);
				dir ^= 1;
			}
		}

		return ans;
	}

	TreeNode* createTree() {
		TreeNode* root = new TreeNode(3);
		root->left = new TreeNode(9);
		root->right = new TreeNode(20);

		root->right->left = new TreeNode(15);
		root->right->right = new TreeNode(7);

		return root;
	}

	void printListOfLists(vector<vector<int>> ll) {
		for (vector<int> l : ll) {
			cout<< "[";
			for (int x : l) cout << x << ", ";
			cout << "]";
			cout << endl;
		}
	}
};

#if 0
int main(void) {
	ZigZagLevelOrderTraversalBT t;

	ZigZagLevelOrderTraversalBT::TreeNode* root = t.createTree();
	vector<vector<int>> ans = t.zigzagLevelOrder(root);
	t.printListOfLists(ans);
	return 0;
}
#endif