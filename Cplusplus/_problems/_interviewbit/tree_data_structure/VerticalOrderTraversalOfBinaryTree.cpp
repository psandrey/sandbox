
/*
 * Given a binary tree, return a 2-D array with vertical order traversal of it.
 */

#include <algorithm>
#include <unordered_map>
#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class VerticalOrderTraversalOfBinaryTree {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	vector<vector<int> > verticalOrderTraversal(TreeNode* root) {
		vector<vector<int> > ans;
		if (root == NULL) return ans;
		unordered_map<int, vector<int>> mp;
		int left = 0, right = 0;
		
		queue<TreeNode* >qn; qn.push(root);
		queue<int> qi; qi.push(0);
		while (!qn.empty()) {
			int size = qn.size();
			for (int i = 0; i < size; i++) {
				TreeNode* n = qn.front(); qn.pop();
				int k = qi.front(); qi.pop();

				if (k < 0 && left > k) left = k;
				else if (k > 0 && right < k) right = k;

				if (mp.find(k) == mp.end()) mp[k] = vector<int>();
				mp[k].push_back(n->val);

				if (n->left != NULL) { qn.push(n->left); qi.push(k - 1); }
				if (n->right != NULL) { qn.push(n->right); qi.push(k + 1); }
			}
		}

		for (int i = left; i <= right; i++) ans.push_back(mp[i]);
		return ans;
	}

	TreeNode* createTree() {
		TreeNode* root = new TreeNode(1);
		root->left = new TreeNode(2);
		root->right = new TreeNode(3);
		root->left->left = new TreeNode(7);
		root->left->right = new TreeNode(4);
		root->right->left = new TreeNode(5);
		root->right->right = new TreeNode(6);

		return root;
	}

	void printListOfLists(vector<vector<int>> ll) {
		for (vector<int> l : ll) {
			cout << "[";
			for (int x : l) cout << x << ", ";
			cout << "]";
			cout << endl;
		}
	}
};

#if 0
int main(void) {
	VerticalOrderTraversalOfBinaryTree t;
	VerticalOrderTraversalOfBinaryTree::TreeNode* root = t.createTree();
	vector<vector<int>> ans = t.verticalOrderTraversal(root);

	t.printListOfLists(ans);
	return 0;
}
#endif