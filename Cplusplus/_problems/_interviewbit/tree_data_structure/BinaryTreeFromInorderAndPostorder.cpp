
/*
 * Given inorder and postorder traversal of a tree, construct the binary tree.
 */

#include <algorithm>
#include <vector>
#include <unordered_map>
#include <iostream>
using namespace std;

class BinaryTreeFromInorderAndPostorder {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	TreeNode* buildTree(vector<int>& in, vector<int>& post) {
		unordered_map<int, int> ump;
		for (int i = 0; i < (int)in.size(); i++)
			ump[in[i]] = i;

		return helper(in, 0, in.size() - 1, post, 0, post.size() - 1, ump);
	}

	TreeNode* helper(vector<int>& in, int ii, int ij, vector<int>& post, int pi, int pj,
					 unordered_map<int, int>& ump) {
		if (ii > ij) return NULL;

		TreeNode* n = new TreeNode(post[pj]);

		int k = ump[post[pj]];
		n->left = helper(in, ii, k - 1, post, pi, pi + (k - 1 - ii), ump);
		n->right = helper(in, k + 1, ij, post, pi + (k - ii), pj - 1, ump);
		return n;
	}

	void preOrder(TreeNode* node) {
		if (node == NULL)
			return;
		cout << node->val  << " ";
		preOrder(node->left);
		preOrder(node->right);
	}
};

#if 0
int main(void) {
	vector<int> in = { 4, 8, 2, 5, 1, 6, 3, 7 };
	vector<int> post = { 8, 4, 5, 2, 6, 7, 3, 1 };
	
	BinaryTreeFromInorderAndPostorder t;
	BinaryTreeFromInorderAndPostorder::TreeNode* root = t.buildTree(in, post);
	
	t.preOrder(root);
	/* Should be:
	 * Preorder of the constructed tree :
	 * 1 2 4 8 5 3 6 7
	 */

	return 0;
}
#endif