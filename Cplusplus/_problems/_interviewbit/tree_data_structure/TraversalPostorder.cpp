
/*
 * Given a binary tree, return the postorder traversal of its nodes� values.
 *
 * Note: Solution contains only iterative version.
 */

#include <algorithm>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class TraversalPostorder {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	vector<int> postorder(TreeNode* root) {
		vector<int> ans;
		if (root == NULL) return ans;

		stack<TreeNode*> st;
		TreeNode* n = root, * p = NULL;
		while (n != NULL || !st.empty()) {
			if (n != NULL) {
				st.push(n);
				p = n;
				n = n->left;
			} else {
				n = st.top();
				if (n->right != NULL && p != n->right) {
					p = n;
					n = n->right;
				} else {
					ans.push_back(n->val);
					p = n;
					n = NULL;
					st.pop();
				}
			}
		}

		return ans;
	}

	vector<int> postorderv2(TreeNode* root) {
		vector<int> ans;
		if (root == NULL) return ans;

		stack<TreeNode*> st;
		TreeNode
			* n = root, // current node
			* p = NULL; // previous processed node
		while (n != NULL || !st.empty()) {
			while (n != NULL) { st.push(n); n = n->left; }

			n = st.top();

			// if node n does not have a right child, or the last processed node
			//  was n's right child, then process node n since both left and right subtrees
			//  have been processed
			if (n->right == NULL || n->right == p) {
				ans.push_back(n->val);
				st.pop();
				p = n;    // don't go on n's right
				n = NULL; // don't go on n's left
			} else n = n->right;
		}

		return ans;
	}


	TreeNode* createTree() {
		TreeNode* root = new TreeNode(3);
		root->left = new TreeNode(9);
		root->right = new TreeNode(20);
		root->left->left = new TreeNode(1);
		root->left->right = new TreeNode(5);
		root->right->left = new TreeNode(15);
		root->right->right = new TreeNode(7);

		return root;
	}
};

#if 0
int main(void) {
	TraversalPostorder t;
	TraversalPostorder::TreeNode* root = t.createTree();
	vector<int> ans = t.postorder(root);
	for (int x : ans) cout << x << " ";
	cout << endl;
	ans = t.postorderv2(root);
	for (int x : ans) cout << x << " ";

	return 0;
}
#endif