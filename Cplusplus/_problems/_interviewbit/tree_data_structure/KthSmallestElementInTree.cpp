
/*
 * Given a binary search tree, write a function to find the kth smallest element in the tree.
 */

#include <algorithm>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class KthSmallestElementInTree {
public:

	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

#if 1

	// iterative
	int kthsmallest(TreeNode* root, int K) {
		int ans = -1;
		if (root == NULL) return ans;

		stack<TreeNode*> st;
		TreeNode* n = root;
		while (n != NULL || !st.empty()) {
			while (n != NULL) { st.push(n); n = n->left; }

			n = st.top(); st.pop();
			K--;
			if (K == 0) { ans = n->val; break; }

			n = n->right;
		}

		return ans;
	}

#else

	int kthsmallest(TreeNode* root, int K) {
		return helper(root, &K);
	}

	// recursive
	int helper(TreeNode* n, int* K) {
		int ans = INT_MAX;
		if (n == NULL) return ans;
		
		if (n->left != NULL) ans = helper(n->left, K);
		if (ans != INT_MAX) return ans;

		(*K)--;
		if ((*K) == 0) ans = n->val;
		if (n->right != NULL && ans == INT_MAX) ans = helper(n->right, K);
		
		return ans;
	}

#endif

	TreeNode* createTree() {
		TreeNode* root = new TreeNode(10);
		root->left = new TreeNode(5);
		root->right = new TreeNode(12);
		root->left->left = new TreeNode(3);
		root->left->right = new TreeNode(7);
		root->left->right->left = new TreeNode(6);

		return root;
	}
};

#if 0
int main(void) {
	KthSmallestElementInTree t;

	KthSmallestElementInTree::TreeNode* root = t.createTree();
	cout << t.kthsmallest(root, 3) << endl;

	return 0;
}
#endif