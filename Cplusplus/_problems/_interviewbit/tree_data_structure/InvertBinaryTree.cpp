
/*
 * Given a binary tree, invert the binary tree and return it.
 */

#include <algorithm>
#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class InvertBinaryTree {
public:
	struct TreeNode {
		int val;
		TreeNode* left;
		TreeNode* right;
		TreeNode(int x) : val(x), left(NULL), right(NULL) {}
	};

	TreeNode* invertTree(TreeNode* n) {
		if (n == NULL) return n;
		invertTree(n->left);
		invertTree(n->right);

		TreeNode* tmp = n->left;
		n->left = n->right;
		n->right = tmp;

		return n;
	}

	TreeNode* buildBT() {
		TreeNode* root = new TreeNode(1);
		root->left = new TreeNode(2);
		root->right = new TreeNode(3);

		root->left->left = new TreeNode(4);
		root->left->right = new TreeNode(5);

		root->right->left = new TreeNode(6);
		root->right->right = new TreeNode(7);

		return root;
	}

	void levelOrder(TreeNode* root) {
		if (root == NULL) return;
		queue<TreeNode*> q;
		q.push(root);
		while (!q.empty()) {
			int size = q.size();
			for (int i = 0; i < size; i++) {
				TreeNode* n = q.front();
				q.pop();
				cout << n->val << " ";
				if (n->left != NULL) q.push(n->left);
				if (n->right != NULL) q.push(n->right);
			}
			cout << endl;
		}
	}

};

#if 0
int main(void) {
	InvertBinaryTree t;
	InvertBinaryTree::TreeNode* root = t.buildBT();
	t.levelOrder(root);
	cout << endl;

	InvertBinaryTree::TreeNode* ans = t.invertTree(root);
	
	t.levelOrder(root);

	return 0;
}
#endif