
/*
 * Given an array of integers, every element appears thrice except for one which occurs once.
 * Find that element which does not appear thrice.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class SingleNumberII {
public:
	int singleNumber(const vector<int>& a) {
		if (a.empty()) return 0;

		int ans = 0;
		int n = (int)a.size();
		for (int b = 0; b < 32; b++) {
			int mask = (1 << b);
			int count = 0;
			for (int i = 0; i < n; i++)
				if ((mask & a[i]) != 0) count++;

			ans |= (count % 3 == 0 ? 0 : mask);
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = { 1, 2, 4, 3, 3, 2, 2, 3, 1, 1 };

	SingleNumberII t;
	cout << t.singleNumber(a);
	return 0;
}
#endif