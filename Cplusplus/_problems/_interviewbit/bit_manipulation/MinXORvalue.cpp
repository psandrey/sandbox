
/*
 * Given an array of N integers, find the pair of integers in the array which have minimum XOR value.
 * Report the minimum XOR value.
 *
 * Examples :
 *  Input: 0 2 5 7
 *  Output: 2 (0 XOR 2)
*/

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MinXORvalue {
public:
	int min_xor(vector<int>& a) {
		int n = (int)a.size();
		if (n == 0) return -1;

		/*
			- let a <= c <= b
			- let i-th bit be the first from the left for which a and b differs, then
			case 1: a[i] = c[i] = 0 => (a^c)[i] = 0 => a^c < a^b
			case 2: c[i] = b[i] = 1 => (c^b)[i] = 0 => c^b < a^b
			!!! a[i] cannot be 1, because if it were then a > b which contradicts: a <= c <= b,
			so, the only cases possible are the one stated above.
			Conclusion: the xor is minimized for the adjacent values.
		*/
		sort(a.begin(), a.end());
		unsigned int ans = UINT_MAX;
		for (int i = 1; i < n; i++) {
			unsigned int x = a[i - 1];
			unsigned int y = a[i];
			unsigned int exclusive_or = x ^ y;
			if (ans > exclusive_or) ans = exclusive_or;
		}

		return (int)ans;
	}
};

#if 0
int main(void) {
	vector<int> a = { 0, 4, 7, 9 };

	MinXORvalue t;
	cout << t.min_xor(a);

	return 0;
}
#endif