
/*
 * Given an array of integers, every element appears twice except for one. Find that single one.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class SingleNumber {
public:
	int find_number(const vector<int>& a) {
		unsigned int missing = 0;
		for (int x : a) {
			unsigned int xx = (unsigned int)x;
			missing ^= xx;
		}

		return (int)missing;
	}
};

#if 0
int main(void) {
	vector<int> a = { 1, 2, 2, 0, 0 };
	SingleNumber t;
	cout << t.find_number(a);
	return 0;
}
#endif