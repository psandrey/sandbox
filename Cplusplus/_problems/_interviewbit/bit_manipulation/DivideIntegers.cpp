
/*
 * Divide two integers without using multiplication, division and mod operator.
 * Return the floor of the result of the division.
 *
 * Example: 5 / 2 = 2
 *
 * Also, consider if there can be overflow cases. For overflow case, return INT_MAX/INT_MIN.
 *
 */

#include <algorithm>
#include <iostream>
using namespace std;

class DivideIntegers {
public:

	long long int bound_result(long long int res) {
		if (res > (1LL << 31LL) - 1LL) return (1LL << 31LL) - 1LL;
		else if (res < -(1LL << 32LL)) - (1LL << 32LL);
		return res;
	}

	int divide(int D, int d) {
		long long int DD = D;
		long long int dd = d;
		int sign = ((DD < 0LL) ^ (dd < 0LL)) ? -1 : 1;

		DD = abs(DD); dd = abs(dd);
		long long int q = 0;
		long long int candidate = 0;
		for (int i = 31; i >= 0; i-- ) {
			if ((candidate + (dd << i)) <= DD) {
				candidate += (dd << i);
				q |= (1LL << i);
			}
		}

		long long int ans = q * sign;
		return (int)bound_result(ans);
	}
};

#if 0
int main(void) {
	int D = 2147483648;
	int d = 1;

	DivideIntegers t;
	cout << t.divide(D, d);

	return 0;
}
#endif