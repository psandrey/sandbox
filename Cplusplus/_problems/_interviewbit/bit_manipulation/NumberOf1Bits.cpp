
/*
 * Population count (a.k.a. Hamming weight):
 *
 *   1. Right shift method: naive method O(32)
 *   2. Division method: O(number of 1's)
 *   3. Brian Kernighan's method: sparsely populated word O(number of 1's)
 *   4. Table method: T(n) = O(1), S(n) = O(256)
 *   5. Divide and conquer method (HAKMEN)
 */

#include <algorithm>
#include <iostream>
using namespace std;

#define B2(n)	n,     n+1,     n+1,     n+2		// 2 bits
#define B4(n)	B2(n), B2(n+1), B2(n+1), B2(n+2)	// 4 bits
#define B6(n)	B4(n), B4(n+1), B4(n+1), B4(n+2)	// 6 bits
#define B8		B6(0), B6(1),   B6(1),   B6(2)		// 8 bits
static const unsigned char byte_count_table[256] = { B8 };

class NumberOf1Bits{
public:
	// 1. Right shift method
	unsigned int pop_count_shift(unsigned int x) {
		unsigned int count = 0;
		while (x) {
			count += (x & 1);
			x >>= 1;
		}

		return count;
	}

	// 2. division method
	unsigned int pop_count_div(unsigned int x) {
		unsigned int count = x;
		while (x > 0) {
			x = x >> 1;
			count = count - x;
		}

		return count;
	}

	// 3. Brian Karnighan's method
	unsigned int pop_count_brian(unsigned int x) {
		int count = 0;
		for (count = 0; x; count++) x = x & (x - 1);
		return count;
	}

	// 4. Table method
	unsigned int pop_count_tbl(unsigned int x) {
		unsigned char* p = (unsigned char*)& x;
		unsigned int count =
			byte_count_table[p[0]] +
			byte_count_table[p[1]] +
			byte_count_table[p[2]] +
			byte_count_table[p[3]];

		return count;
	}

	// 5. IBM's parallel method
	unsigned int pop_count_ibm(unsigned int x) {
		// count bits in each 4 bit group and save the ans in 2bit groups
		x = x - ((x >> 1) & 0x55555555); 

		// add 2bit groups in pairs and save the ans in 4 bit groups
		x = (x & 0x33333333) + ((x >> 2) & 0x33333333);

		// add 4bit groups in pairs ans save the ans in 8bit groups
		x = (x + (x >> 4) & 0xF0F0F0F);
		
		// multiply by 0x01010101 gives the sum in the first 8 bits:
		// 0x0e0f0g0h * 0x01010101 = e+f+g+h
		unsigned int count = (x * 0x01010101);

		// shifting 24 bits to the right gives the answer
		count >>= 24;
		return count;
	}
};

#if 0
int main(void) {
	int x = 11;
	
	NumberOf1Bits t;
	cout << t.pop_count_shift(x) << endl;
	cout << t.pop_count_div(x) << endl;
	cout << t.pop_count_brian(x) << endl;
	cout << t.pop_count_tbl(x) << endl;
	cout << t.pop_count_ibm(x) << endl;

	return 0;
}
#endif