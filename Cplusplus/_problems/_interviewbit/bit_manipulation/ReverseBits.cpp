
/*
 * Reverse bits of an 32 bit unsigned integer.
 * 
 * Example: x = 3
 *    00000000000000000000000000000011
 *    11000000000000000000000000000000
 *
 *   1. Right shift method: naive method O(32)
 *   2. Divide and conquer method: O(logm)
 *
 */

#include <algorithm>
#include <iostream>
using namespace std;

class ReverseBits {
public:
	// shift method
	unsigned int reverse_bits_shift(unsigned int x) {
		unsigned int ans = 0;
		for (int i = 0; i < 32; i++) {
			int bit = (x & 1);
			ans = (ans << 1) | bit;
			x = x >> 1;
		}

		return ans;
	}

	// Swapping in groups
	unsigned int reverse_bits_swapping(unsigned int x) {
		// swap consecutive bits
		x = ((x >> 1) & 0x55555555) | ((x & 0x55555555) << 1);

		// swap consecutive 2bit groups
		x = ((x >> 2) & 0x33333333) | ((x & 0x33333333) << 2);

		// swap consecutive 4bit groups 
		x = ((x >> 4) & 0x0F0F0F0F) | ((x & 0x0F0F0F0F) << 4);

		// swap consecutive 8bit groups
		x = ((x >> 8) & 0x00FF00FF) | ((x & 0x00FF00FF) << 8);

		// swap consecutive 16bit groups
		x = (x >> 16) | (x << 16);
	
		// answer 32bit
		return x;
	}
};

#if 0
int main(void) {
	int x = 3;

	ReverseBits t;
	cout << t.reverse_bits_shift(x) << endl;
	cout << t.reverse_bits_swapping(x) << endl;

	return 0;
}
#endif