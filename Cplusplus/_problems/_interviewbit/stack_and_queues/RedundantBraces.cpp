
/*
 * Write a program to validate if the input string has redundant braces?
 * Return 0/1
 *
 * 0 -> NO
 * 1 -> YES
 * Input will be always a valid expression and operators allowed are only + , * , - , /
 *
 * Example:
 *  ((a + b)) has redundant braces so answer will be 1
 *  (a + (a + b)) doesn't have have any redundant braces so answer will be 0
 */

#include <stack>
#include <unordered_set>
#include <algorithm>
#include <iostream>
using namespace std;

class RedundandBraces {
public:
	int redundant(string s) {
		int n = s.size();
		stack<int> st;

#if 1
		unordered_set<char> op;
		op.insert('+'); op.insert('-'); op.insert('*'); op.insert('/');
		
		for (int j = 0; j < n; j++) {
			if (s[j] == ')') {
				if (st.empty() || op.find(st.top()) == op.end()) return 1;
				st.pop(); st.pop();
			} else if (s[j] == '(')
				st.push(s[j]);
			else if (!st.empty() && op.find(s[j]) != op.end() && op.find(st.top()) == op.end())
				st.push(s[j]);
		}
#else
		for (int i = 0; i < n; i++) {
			st.push(s[i]);

			if (st.top() == ')') {
				int count = -1;  // pop )
				while (!st.empty() && st.top() != '(') { st.pop(); count++; }
				if (!st.empty()) st.pop(); // pop (
				if (count <= 1) return 1;
			}
		}
#endif
		return 0;
	}
};

#if 0
int main(void) {
	RedundandBraces t;
	//string s = "a+b";
	//string s = "((b + c + d + e))";
	string s = "(a + (b + c + d + e))";

	int r = t.redundant(s);
	cout << r << endl;
	return 0;
}
#endif