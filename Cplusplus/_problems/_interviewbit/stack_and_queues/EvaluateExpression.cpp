
/*
 * Evaluate the value of an arithmetic expression in Reverse Polish Notation.
 * Valid operators are +, -, *, /. Each operand may be an integer or another expression.
 *
 * Examples:
 *   ["2", "1", "+", "3", "*"] -> ((2 + 1) * 3) -> 9
 *   ["4", "13", "5", "/", "+"] -> (4 + (13 / 5)) -> 6
 */

#include <string> 
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class EvaluateExpression {
public:
	int evalRPN(vector<string> &s) {
		stack<int> st;
		for (int i = 0; i < (int)s.size(); i++) {
			if (is_operator(s[i]) == true) {
				int b = st.top(); st.pop();
				int a = st.top(); st.pop();
				int x = eval(a, b, s[i][0]);
				st.push(x);
			}
			else st.push(stoi(s[i]));
		}

		int ans = st.top(); st.pop();
		return ans;
	}

	bool is_operator(string a) {
		if (a.length() > 1) return false;
		if (a[0] == '-' || a[0] == '+' || a[0] == '*' || a[0] == '/') return true;

		return false;
	}

	int eval(int a, int b, char op) {
		if (op == '+') return (a + b);
		if (op == '-') return (a - b);
		if (op == '*') return (a * b);
		return (a / b);
	}
};

#if 0
int main(void) {
	vector<string> e = { "15", "7", "1", "1", "+", "-", "/", "3", "*", "2", "1", "1", "+", "+", "-" };
	
	EvaluateExpression t;
	cout << t.evalRPN(e);

	return 0;
}
#endif