
/*
 * Given an array, find the nearest smaller element G[i] for every element A[i] in the array
 * such that the element has an index smaller than i.
 * Elements for which no smaller element exist, consider next smaller element as -1.
 *
 * Example:
 *
 * Input : A : [4, 5, 2, 10, 8]
 * Return : [-1, 4, -1, 2, 2]
 */

#include <iostream>
#include <vector>
#include <stack>
using namespace std;

class NearestSmallerElement {
public:
	vector<int> nearestSmaller(vector<int> a) {
		if (a.size() == 0) return {};

		vector<int> ans;
		stack<int> st;
		for (int i = 0; i < (int)a.size(); i++) {
			while (!st.empty() && st.top() >= a[i]) st.pop();
			int x = (st.empty() ? -1 : st.top());
			ans.push_back(x);

			st.push(a[i]);
		}
		
		while (!st.empty()) st.pop();

		return ans;
	}
};

#if 0
int main(void) {
	NearestSmallerElement t;
	vector<int> a = { 39, 27, 11, 4, 24, 32, 32, 1 };
	vector<int> ans = t.nearestSmaller(a);
	
	for (int x: ans) cout << x << ", ";
	cout << endl;
}
#endif