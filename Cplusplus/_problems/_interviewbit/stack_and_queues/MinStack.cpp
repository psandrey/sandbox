
/*
 * Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.
 *
 * push(x) � Push element x onto stack.
 * pop() � Removes the element on top of the stack.
 * top() � Get the top element.
 * getMin() � Retrieve the minimum element in the stack.
 * Note: that all the operations have to be constant time operations.
 */

#include <stack>
#include <algorithm>
#include <iostream>
using namespace std;

class MinStack {
	stack<int> st;
	stack<int> minst;

public:
	~MinStack() {
		stack<int>().swap(st);
		stack<int>().swap(minst);
	}

	void push(int x);
	void pop(void);
	int top(void);
	int getMin(void);
};

void MinStack::push(int x) {
	st.push(x);
	if (minst.empty() || x < minst.top()) minst.push(x);
}

void MinStack::pop(void) {
	if (st.empty()) return;
	if (st.top() == minst.top()) minst.pop();
	st.pop();
}

int MinStack::top(void) {
	if (st.empty()) return -1;
	return st.top();
}

int MinStack::getMin(void) {
	if (minst.empty() == true) return -1;
	return minst.top();
}

#if 0
int main(void) {
	MinStack s;
	s.push(10);
	s.push(5);
	s.push(12);
	s.push(1);
	while (s.top() != -1) {
		cout << s.getMin() << endl;
		s.pop();
	}
}
#endif