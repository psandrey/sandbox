
/*
 * Given n non-negative integers representing the histogramís bar height where the width of each bar is 1,
 * find the area of largest rectangle in the histogram.
 */

#include <algorithm>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class LargestRectangleInHistogram {
public:
	int largestRectangleArea(vector<int> &H) {
		int ans = INT_MIN;
		stack<int> st;
		int i = 0, n = (int)H.size();
		for (i = 0; i < n; i++) {
			while (!st.empty() && H[st.top()] >= H[i]) {
				int h = H[st.top()]; st.pop();
				int w = (!st.empty() ? i - st.top() - 1 : i);
				int area = h * w;
				ans = max(ans, area);
			}
			st.push(i);
		}

		while (!st.empty()) {
			int h = H[st.top()]; st.pop();
			int w = (!st.empty() ? i - st.top() - 1 : i);
			int area = h * w;
			ans = max(ans, area);
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> H = {2, 1, 5, 6, 2, 3};

	LargestRectangleInHistogram t;
	cout << t.largestRectangleArea(H) << endl;

	return 0;
}
#endif