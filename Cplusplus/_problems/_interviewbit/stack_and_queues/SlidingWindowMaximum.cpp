
/*
 * A long array A[] is given to you. There is a sliding window of size w which is moving
 * from the very left of the array to the very right. You can only see the w numbers in the window.
 *  Each time the sliding window moves rightwards by one position. You have to find the maximum
 *  for each window.
 *
 * The following example will give you more clarity:
 * Input:  [1 3 -1 -3 5 3 6 7], and w is 3
 * Output: [3 3 5 5 6 7]
 */

#include <deque>
#include <vector>
#include <iostream>
using namespace std;

class SlidingWindowMaximum {
public:
	vector<int> slidingMaximum(const vector<int> &a, int w) {
		vector<int> ans;
		deque<int> q;
		for (int i = 0; i < (int)a.size(); i++) {
			while (!q.empty() && a[i] > a[q.back()]) q.pop_back();
			q.push_back(i);
			if (q.front() < i - w + 1) q.pop_front();

			if (i - w + 1 >= 0) ans.push_back(a[q.front()]);
		}
		if ((int)a.size() - w < 0) ans.push_back(a[q.front()]);

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = {1, 3, -1, -3, 5, 3, 6, 7};
	int w = 3;

	SlidingWindowMaximum t;
	vector<int> ans = t.slidingMaximum(a, w);
	for (int x : ans) cout << x << " ";

	return 0;
}
#endif