
/*
 * Given n non-negative integers representing an elevation map where the width of each bar is 1,
 * compute how much water it is able to trap after raining.
 *
 * Example :
 * Given [0,1,0,2,1,0,1,3,2,1,2,1], return 6.
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class RainWaterTrapped {
public:
	int water(vector<int> a) {
		if (a.size() == 0) return 0;

		int n = a.size();
		vector<int> left(n);
		vector<int> right(n);
	
		int so_far = 0;
		for (int i = 0; i < n; i++) {
			so_far = max(so_far, a[i]);
			left[i] = so_far;
		}

		so_far = 0;
		for (int i = n-1; i >= 0; i--) {
			so_far = max(so_far, a[i]);
			right[i] = so_far;
		}

		int ans = 0;
		for (int i = 0; i < n; i++)
			ans += min(left[i], right[i]) - a[i];
	
		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = { 0,1,0,2,1,0,1,3,2,1,2,1 };
	
	RainWaterTrapped t;
	cout << t.water(a) << endl;

	return 0;
}
#endif