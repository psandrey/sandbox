
/*
 * Given an absolute path for a file (Unix-style), simplify it.
 *
 * Examples:
 *   path = "/home/", => "/home"
 *   path = "/a/./b/../../c/", => "/c"
 * Note: That absolute path always begin with �/� ( root directory )
 *       Path will not have whitespace characters.
*/

#include <string>
#include <stack>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;

class SimplifyDirectoryPath {
public:
	string simplify(string s) {
		if (s.empty()) return "";
		else if (s.compare("/") == 0) return "/";

		// tokenize using string stream
		stringstream stream(s);
		std::string str;
		stack<string> st;
		while (getline(stream, str, '/')) {
			if (str.compare(".") == 0) continue;
			else if (str.compare("..") == 0) {
				if (!st.empty()) st.pop();
				continue;
			}

			if (str.size() != 0) st.push(str);
		}

		// build answer
		string ans;
		if (st.empty()) ans = "/";
		else while (!st.empty()) { ans = "/" + st.top() + ans; st.pop(); }

		return ans;
	}
};

#if 0
int main(void) {
	SimplifyDirectoryPath t;
	string s = "/home//foo/";
	string ans = t.simplify(s);
	cout << ans << endl;
}
#endif