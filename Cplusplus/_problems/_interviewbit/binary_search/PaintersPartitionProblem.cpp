
/*
 * You have to paint N boards of length {A0, A1, A2, A3 ... AN-1}.
 * There are K painters available and you are also given how much time a painter
 *  takes to paint 1 unit of board. You have to get this job done as soon as possible
 *  under the constraints that any painter will only paint contiguous sections of board.
 *
 * Notes:
 * 1. Two painters cannot share a board to paint. That is to say, a board cannot be painted
 *  partially by one painter, and partially by another.
 * 2. A painter will only paint contiguous boards.
 *  Which means a configuration where painter 1 paints board 1 and 3 but not 2 is invalid.
 *
 * Return the ans % 10000003;
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class PaintersPartitionProblem {
public:
	// Binary search: T(n) = O(n.log(sum(fences)))
	int ppp_bs(vector<int>& fences, int k, int t) {
		if (fences.empty()) return -1;
		long long lo = 0LL, hi = 0LL;

		int n = (int)fences.size();
		for (int i = 0; i < n; i++) {
			hi += fences[i];
			lo = max(lo, (long long)fences[i]);
		}

		long long ans = lo;
		while (lo <= hi) { // we need to touch the boundaries and do some computations for them
			long long m = (lo + hi) / 2;
			if (painters(fences, m) <= k) { hi = m - 1; ans = m; }
			else lo = m + 1;
		}

		long long m = 10000003LL;
		return (int)((ans*t) % m);
	}

	int painters(vector<int>& fences, long long t) {
		int k = 1;
		long long sum = 0;
		for (int i = 0; i < (int)fences.size(); i++) {
			if (fences[i] > t) return INT_MAX;
			if (sum + fences[i] <= t) sum += fences[i];
			else { sum = fences[i]; k++; }
		}

		return k;
	}

	// Dynamic programming with tabulation: T(n) = O(n^2.k)
	int ppp_dp_t(vector<int>& fences, int k) {
		if (fences.empty()) return -1;

		int n = (int)fences.size();
		vector<int> sum(n); sum[0] = fences[0];
		for (int i = 1; i < n; i++) sum[i] = sum[i - 1] + fences[i];
	
		vector<vector<int>> dp(n + 1, vector<int>(k + 1));
		// time to paint any number of fences >= 1 with 0 workers is infinity
		for (int i = 1; i <= n; i++) dp[i][0] = INT_MAX;
		// time to paint 0 fences with any number of workers is 0
		for (int w = 0; w <= k; w++) dp[0][w] = 0;

		// file the entire table
		for (int i = 1; i <= n; i++) {
			for (int w = 1; w <= k; w++) {
				int w1 = sum[i - 1];
				dp[i][w] = w1;
				for (int j = 1; j < i; j++) {
					w1 = w1 - fences[j - 1];
					int candidate = max(dp[j][w - 1], w1);
					dp[i][w] = min(dp[i][w], candidate);
				}
			}
		}

		return dp[n][k];
	}

	// Dynamic programming with memoization: T(n) = O(n^2.k)
	int ppp_dp_m(vector<int>& fences, int k) {
		if (fences.empty()) return -1;

		int n = (int)fences.size();
		vector<vector<int>> memo(n, vector<int>(k + 1, -1));
		vector<int> sum(n); sum[0] = fences[0];
		for (int i = 1; i < n; i++) sum[i] = sum[i - 1] + fences[i];

		//return ppp_dp_m1(fences, n - 1, k, sum, memo);
		return ppp_dp_m2(fences, n - 1, k, sum, memo);
	}

	int ppp_dp_m1(vector<int>& fences, int i, int k, vector<int>& sum, vector<vector<int>>& memo) {
		int ans = 0;
		if (memo[i][k] != -1) return memo[i][k];
		else if (k == 1) ans = sum[i];
		else if (i == 0) ans = fences[0];
		else {
			int w1 = sum[i];
			ans = w1;
			for (int j = 0; j < i; j++) {
				w1 = w1 - fences[j - 1];
				int candidate = max(w1, ppp_dp_m1(fences, j, k - 1, sum, memo));
				ans = min(ans, candidate);
			}
		}

		memo[i][k] = ans;
		return memo[i][k];
	}

	int ppp_dp_m2(vector<int>& fences, int i, int k, vector<int>& sum, vector<vector<int>>& memo) {
		if (memo[i][k] != -1) return memo[i][k];
		
		int n = (int)fences.size();
		int ans = sum[n - 1];
		if (k == 1) ans = sum[n - 1] - sum[i];
		else if (i == n - 1) ans = fences[n - 1];
		else {
			for (int j = i + 1; j < n; j++) {
				int w = sum[j] - sum[i];
				int rem = ppp_dp_m2(fences, j, k - 1, sum, memo);
				ans = min(ans, max(w, rem));
			}
		}

		memo[i][k] = ans;
		return memo[i][k];
	}

};

#if 0
int main(void) {
	vector<int> fences = { 10, 20, 60, 50, 30, 40 };
	int k = 3;
	int time = 1;

	PaintersPartitionProblem t;
	int ans = t.ppp_dp_t(fences, k);
	cout << "DP-tab: " << ans << endl;
	ans = t.ppp_dp_t(fences, k);
	cout << "DP-memo: " << ans << endl;
	ans = t.ppp_bs(fences, k, time);
	cout << "DP-bs: " << ans << endl;
	return 0;
}
#endif