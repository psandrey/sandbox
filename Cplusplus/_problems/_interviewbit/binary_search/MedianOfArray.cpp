
/*
 * There are two sorted arrays A and B of size m and n respectively.
 * Find the median of the two sorted arrays.
 * The overall run time complexity should be O(log (m+n)).
 *
 * T(n) = O(log (m+n))
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MedianOfArray {
public:
	// solution 1: O(log(min(m,n)))
	double median_sol1(vector<int>& a, vector<int>& b) {
		int m = (int)a.size();
		int n = (int)b.size();
		if (m > n) return median_sol1(b, a);

		// binary search on vector a (where sizeof(a) <= sizeof(b))
		int lo = 0, hi = m;
		double median = 0.0;
		while (lo <= hi) {
			int mid_a = (lo + hi) / 2;
			int mid_b = (m + n + 1) / 2 - mid_a;

			// a's partition
			int left_a = INT_MIN; // no element on a's left partition
			if (mid_a > 0) left_a = a[mid_a - 1];
			int right_a = INT_MAX; // no element on a's right partition
			if (mid_a < m) right_a = a[mid_a];

			// b's partition
			int left_b = INT_MIN; // no element on b's left partition
			if (mid_b > 0) left_b = b[mid_b - 1];
			int right_b = INT_MAX; // no element on b's right partition
			if (mid_b < n) right_b = b[mid_b];

			if (left_a <= right_b && left_b <= right_a) { // we've found the solution
				if ((m + n) % 2 == 0) // even
					median = (max(left_a, left_b) + min(right_a, right_b)) / 2.0;
				else // odd
					median = max(left_a, left_b);

				break;
			}
			else if (left_b > right_a) lo = mid_a + 1;
			else hi = mid_a - 1;
		}

		return median;
	}


	// solution 2: O(log(m+n))
	// iterative version
	double mediani(vector<int>& a, vector<int>& b) {
		int n = (int)a.size();
		int m = (int)b.size();
		int len = n + m;
		if (len % 2 == 0) {
			int one = findKth(a, b, (len / 2) - 1);
			int two = findKth(a, b, (len / 2));
			long sum = one + two;
			return sum / 2.0;
		}

		return findKth(a, b, (len / 2));
	}

	int findKth(vector<int>& a, vector<int>& b, int k) {
		int n = (int)a.size();
		int m = (int)b.size();
		int ia = 0, ka = 0;
		int ib = 0, kb = 0;
		while (true) {
			int rem_a = n - ia, rem_b = m - ib;
			if (rem_a == 0) return b[ib + k];
			if (rem_b == 0) return a[ia + k];
			if (k == 0) return min(a[ia], b[ib]);

			// Note: Mind the equation: ka + kb = k - 1, because ka and kb both starts from 0
			int ka, kb;
			if (rem_a < rem_b) {
				ka = min(k / 2, rem_a - 1);
				kb = k - ka - 1;
			} else {
				kb = min(k / 2, rem_b - 1);
				ka = k - kb - 1;
			}

			if (a[ia + ka] < b[ib + kb]) { ia = ia + ka + 1; k = kb; }
			else { ib = ib + kb + 1; k = ka; }
		}
	}

	// recursive version
	double medianr(vector<int>& a, vector<int>& b) {
		int n = (int)a.size();
		int m = (int)b.size();
		int len = n + m;
		if (len % 2 == 0) {
			int one = kth(a, 0, b, 0, (len / 2) - 1);
			int two = kth(a, 0, b, 0, (len / 2));
			long sum = one + two;
			return sum / 2.0;
		}

		return kth(a, 0, b, 0, (len / 2));
	}

	int kth(vector<int>& a, int ia, vector<int>& b, int ib, int k) {
		int m = (int)a.size(), n = (int)b.size();
		int rem_a = m - ia, rem_b = n - ib;
		
		if (rem_a == 0) return b[ib + k];
		if (rem_b == 0) return a[ia + k];
		if (k == 0) return min(a[ia], b[ib]);

		// Note: Mind the equation: ka + kb = k - 1, because ka and kb both starts from 0
		// int ka = min(k / 2, m - ia - 1);
		// int kb = k - ka - 1;
		int ka, kb;
		if (rem_a < rem_b) {
			ka = min(k / 2, rem_a - 1);
			kb = k - ka - 1;
		} else {
			kb = min(k / 2, rem_b - 1);
			ka = k - kb - 1;
		}

		if (a[ia + ka] < b[ib + kb])
			return kth(a, ia + ka + 1, b, ib, kb);

		return kth(a, ia, b, ib + kb + 1, ka);
	}
};

#if 0
int main(void) {
	//vector<int> a = {4, 6, 8, 19};
	//vector<int> b = {3, 5, 9, 10, 11, 16};
	
	//vector<int> a = {1};
	//vector<int> b = {2, 3};

	//vector<int> a = {2, 3};
	//vector<int> b = {1};


	//vector<int> a = {0, 23};
	//vector<int> b = {};

	//vector<int> a = {};
	//vector<int> b = { 0, 23 };

	//vector<int> a = {1, 3};
	//vector<int> b = {3, 4};

	vector<int> a = {1};
	vector<int> b = {3, 4, 5, 6, 7, 8};

	MedianOfArray t;
	cout << t.median_sol1(a, b) << endl;
	cout << t.mediani(a, b) << endl;
	cout << t.medianr(a, b) << endl;

	return 0;
}
#endif