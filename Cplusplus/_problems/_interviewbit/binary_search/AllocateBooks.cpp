
/*
 * N number of books are given.
 * The ith book has Pi number of pages.
 * You have to allocate books to M number of students so that maximum number of pages
 *  alloted to a student is minimum. A book will be allocated to exactly one student.
 * Each student has to be allocated at least one book. Allotment should be in contiguous order,
 *  for example: A student cannot be allocated book 1 and book 3, skipping book 2.
 *
 * Note: Return -1 if a valid assignment is not possible
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class AllocateBooks {
public:
	int books(vector<int> &p, int k) {
		if (k == 0) return -1;
		if (p.empty()) return 0;

		int lo = INT_MIN, hi = 0;
		int n = (int)p.size();
		for (int i = 0; i < n; i++) {
			hi += p[i];
			if (lo < p[i]) lo = p[i];
		}

		while (lo < hi) {
			int m = (lo + hi) / 2;
			if (partition(p, m) > k) lo = m + 1; // we cannot, so lets increase the no of pages
			else hi = m;
		}

		return lo;
	}

	int partition(vector<int>& p, int t) {
		int count = 1, sum = 0;
		for (int i = 0; i < (int)p.size(); i++) {
			if (p[i] > t) return INT_MAX;
			if (sum + p[i] <= t) sum += p[i];
			else { sum = p[i]; count++; }
		}

		return count;
	}
};

#if 0
int main(void) {
	vector<int> p = {12, 34, 67, 90};
	int k = 2;

	AllocateBooks t;
	cout << t.books(p, k) << endl;

	return 0;
}
#endif