
/*
 * Given a sorted array of integers, find the starting and ending position of a given target value.
 * Your algorithmís runtime complexity must be in the order of O(log n).
 * If the target is not found in the array, return [-1, -1].
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class SearchForRange {
public:
	vector<int> searchRange(const vector<int> &a, int target) {
		vector<int> ans = { -1, -1 };
		if (a.empty()) return ans;
		int n = (int)a.size();		
		int lo = 0, hi = n - 1;

		// check if target is in a
		while (lo <= hi) {
			int m = (lo + hi) / 2;

			if (a[m] == target) { ans[0] = m; ans[1] = m; break; }
			else if (a[m] < target) lo = m + 1;
			else hi = m - 1;
		}
		if (ans[0] == -1 && ans[1] == -1) return ans;

		// find lower bound
		lo = 0, hi = ans[0];
		while (lo < hi) { // floor
			int m = (lo + hi - 1) / 2;
			if (a[m] < target) lo = m + 1;
			else hi = m;
		}
		ans[0] = lo;

		// find higher bound
		lo = ans[1], hi = n - 1;
		while (lo < hi) { // ceill
			int m = (lo + hi + 1) / 2;
			if (a[m] > target) hi = m - 1;
			else lo = m;
		}
		ans[1] = lo;

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = {5, 7, 7, 8, 8, 10};
	//vector<int> a = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 };
	int target = 8;

	SearchForRange t;
	vector<int> ans = t.searchRange(a, target);
	cout << ans[0] << " , " << ans[1] << endl;

	return 0;
}
#endif