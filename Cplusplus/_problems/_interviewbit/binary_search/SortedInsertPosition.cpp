
/*
 * Given a sorted array and a target value, return the index if the target is found.
 * If not, return the index where it would be if it were inserted in order.
 *
 * You may assume no duplicates in the array.
 *
 * Here are few examples.
 * [1,3,5,6], 5 -> 2
 * [1,3,5,6], 2 -> 1
 * [1,3,5,6], 7 -> 4
 * [1,3,5,6], 0 -> 0
 */

#include <vector>
#include <iostream>
using namespace std;

class SortedInsertPosition {
public:
	// T(n) = O(logn)
	// S(n) = O(1)
	int insert_position(vector<int>& a, int target) {
		int n = (int)a.size();
		if (n == 0) return 0;

		int low = 0, high = n - 1;
		while (low <= high) {
			int m = (low + high) / 2;

			if (target == a[m]) return m;
			else if (target < a[m]) high = m - 1;
			else low = m + 1;
		}

		return low;
	}
};

/*
int main(void) {
	vector<int> a = {1, 3, 5, 6};

	SortedInsertPosition t;
	cout << t.insert_position(a, 0) << endl;

	return 0;
}
*/