
/*
 * Suppose a sorted array is rotated at some pivot unknown to you beforehand.
 * (i.e., 0 1 2 4 5 6 7  might become 4 5 6 7 0 1 2 ).
 * You are given a target value to search. If found in the array, return its index, otherwise return -1.
 *
 * Note: You may assume no duplicate exists in the array.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class RotatedSortedArraySearch {
public:
	int search(const vector<int> &a, int target) {
		if (a.empty()) return -1;

		int lo = 0, hi = (int)a.size() - 1;
		while (lo <= hi) {
			int m = (lo + hi) / 2;
			if (a[m] == target) return m;

			if (a[lo] < a[hi]) { // lo and hi are in the same cadran
				if(target < a[m]) hi = m - 1;
				else lo = m + 1;
			}
			else { // lo and hi are in different cadrans
				if (a[m] > a[lo]) { // cadran 1
					if (target >= a[lo]) { // cadran 1
						if (target < a[m]) hi = m - 1;
						else lo = m + 1;
					}
					else lo = m + 1; // cadran 2
				}
				else { // cadran 2
					if (target < a[lo]) { // cadran 2
						if (target > a[m]) lo = m + 1;
						else hi = m - 1;
					}
					else hi = m - 1; // cadran 1
				}
			}
		}

		return -1;
	}
};

#if 0
int main(void) {
	vector<int> a = { 4, 5, 6, 7, 0, 1, 2 };
	
	RotatedSortedArraySearch t;
	cout << t.search(a, 4) << endl;
	
	return 0;
}
#endif