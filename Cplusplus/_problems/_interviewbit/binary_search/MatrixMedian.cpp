
/*
 * Given a N cross M matrix in which each row is sorted, find the overall median of the matrix.
 * Assume N*M is odd.
 *
 * For example,
 *
 * Matrix=
 *   [1, 3, 5]
 *   [2, 6, 9]
 *   [3, 6, 9]
 *
 * A = [1, 2, 3, 3, 5, 6, 6, 9, 9]
 *
 * Median is 5. So, we return 5.
 * Note: No extra memory is allowed.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MatrixMedian {
public:

	// T(n) = O(log.n)
	int leq_v(vector<int>& a, int t) {
		int n = (int)a.size();
		int lo = 0, hi = n - 1, k = -1;

		while (lo <= hi) {
			int m = (lo + hi) / 2;

			if (a[m] <= t) { k = m; lo = m + 1; }
			else hi = m - 1;
		}

		return (k == -1 ? 0 : (k + 1)); // k is index, return count
	}

	int leq_m(vector<vector<int>>& a, int t) {
		int c = 0;
		for (int i = 0; i < (int)a.size(); i++)
			c += leq_v(a[i], t);

		return c;
	}

	// T(n) = O(32.n.log(m))
	int median(vector<vector<int>>& a) {
		if (a.empty()) return -1;
		int n = a.size(), m = a[0].size();

		int lo = INT_MAX, hi = INT_MIN;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++) 
				{ lo = min(lo, a[i][j]); hi = max(hi, a[i][j]); }

		// mid+1 to include the item it self
		int target = (n * m + 1) / 2;

		// Note: be aware that the matrix can contain duplicates
		//       and the answer might be a duplicate
		int ans = 0;
		while (lo <= hi) {
			int med = (lo + hi) / 2;

			int less_eq = leq_m(a, med);
			if (less_eq < target)lo = med + 1;
			else { ans = med;  hi = med - 1; }
		}

		return ans;
	}
};

#if 0
int main(void) {
#if 0
	vector<vector<int>> a =
	{ {1, 3, 5},
	 {2, 6, 9},
	 {3, 6, 9} };
#else
	//vector<vector<int>> a = { {1, 1, 3, 3, 3, 3, 3}};
	vector<vector<int>> a = { {2}, {1}, {4}, {1}, {2}, {2}, {5}};
#endif

	MatrixMedian t;
	cout << t.median(a);
	return 0;
}
#endif