
/*
 * Write an efficient algorithm that searches for a value in an m x n matrix.
 * This matrix has the following properties:
 *
 * Integers in each row are sorted from left to right.
 * The first integer of each row is greater than or equal to the last integer of the previous row.
 *
 * Example:
 *   Consider the following matrix:
 *  [[1,   3,  5,  7],
 *   [10, 11, 16, 20],
 *   [23, 30, 34, 50]]
 * Given target = 3, return 1 ( 1 corresponds to true )
 *
 * Return 0 / 1 ( 0 if the element is not present, 1 if the element is present ) for this problem
 *
 */

#include <vector>
#include <iostream>
using namespace std;

class MatrixSearch {
public:
	// T(n) = O(logn)
	// S(n) = O(1)
	int search(vector<vector<int>>& a, int target) {
		int n = (int)a.size();
		int m = (int)a[0].size();
		if (n == 0 || m == 0) return 0;

		int low = 0, high = m * n - 1;
		while (low <= high) {
			int middle = (low + high) / 2;
			int i = middle / m;
			int j = middle % m;
			if (target == a[i][j]) return 1;
			else if (target < a[i][j]) high = middle - 1;
			else low = middle + 1;
		}

		return 0;
	}
};

/*
int main(void) {
	vector<vector<int>> a = {
		{1,  3,  5,  7 },
		{10, 11, 16, 20},
		{23, 30, 34, 50} };
	int target = 11;

	//vector<vector<int>> a = {
	//{22,  32,  67}};
	//int target = 93;

	MatrixSearch t;
	cout << t.search(a, target) << endl;
	
	return 0;
}
*/