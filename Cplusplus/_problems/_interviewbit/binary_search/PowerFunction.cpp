
/*
 * Implement pow(x, n) % d.
 *
 * In other words, given x, n and d, find (x^n % d).
 */

#include <algorithm>
#include <iostream>
using namespace std;

class PowerFunction {
public:
	int pow(int x, int n, int d) {
		if (x == 0) return 0;
		if (n == 0) return (d == 1 ? 0 : 1);
		if (n == 1) return ((x%d + d) % d);

		long long xx = x;
		long long ans = 1LL;
		while (n > 0) {
			if ((n & 1) != 0) ans = (ans * xx) % d;
			xx = (xx * xx) % d;

			n = n >> 1;
		}

		return (int)((ans + d) % d);
	}
};

#if 0
int main(void) {
#if 0
	int x = 17,
		n = 7,
		d = 5;
#else
	int x = 71045970,
		n = 41535484,
		d = 64735492;
#endif
	PowerFunction t;
	cout << t.pow(x, n, d) << endl;

	return 0;
}
#endif