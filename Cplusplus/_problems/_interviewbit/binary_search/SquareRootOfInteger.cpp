
/*
 * Implement int sqrt(int x).
 *
 * Compute and return the square root of x.
 * If x is not a perfect square, return floor(sqrt(x))
 *
 * Example :
 * Input : 11
 * Output : 3
 * Note: Do not use sqrt function.
 */

#include <algorithm>
#include <iostream>
using namespace std;

class SquareRootOfInteger {
public:
	// T(n) = O(32), since int has 32 bits
	int sqrt_int(int a) {
		if (a == 0) return 0;

	int lo = 1, hi = a / 2;

#if 0
		while (lo < hi) {
			int m = (lo + hi + 1) / 2; // biased to the right because we need floor
			long long mm = (long long)((long long)m * (long long)m);
			if (mm > (long long)a) hi = m - 1;
			else lo = m;
		}
#else
		while (lo < hi) {
			int m = (lo + hi + 1) / 2;

			if (m < a / m) lo = m;
			else hi = m - 1;
		}
#endif

		return lo;
	}
};

#if 0
int main(void) {
	SquareRootOfInteger t;

	cout << t.sqrt_int(2147483647);
	//cout << t.sqrt_int(6);

	return 0;
}
#endif