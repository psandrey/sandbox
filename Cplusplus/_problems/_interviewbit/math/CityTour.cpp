
/*
 * There are A cities numbered from 1 to A. You have already visited M cities, 
 * the indices of which are given in an array B of M integers.
 *
 * If a city with index i is visited, you can visit either the city with index i-1 (i >= 2) or
 * the city with index i+1 (i < A) if they are not already visited.
 * Eg: if N = 5 and array M consists of [3, 4], then in the first level of moves, you can either visit 2 or 5.
 *
 * You keep visiting cities in this fashion until all the cities are not visited.
 * Find the number of ways in which you can visit all the cities modulo 10^9+7.
 *
 * Constraints:
 * 1 <= A <= 1000
 * 1 <= M <= A
 * 1 <= B[i] <= A
 */

#include <vector>
#include <unordered_map>
#include <map>
#include <algorithm>
#include <iostream>
using namespace std;

class CityTour {
private:
	unsigned int pow_mod_base_2(unsigned int p, unordered_map<unsigned int, unsigned int>& memo) {
		if (p == 0) return 1;
		else if (p == 1) return 2;

		unsigned long long mod = (unsigned long long)1000000007ULL;
		unsigned long long ans = 1, x = 2ul;
		while (p > 0) {
			if ((p & 1) == 1)
				ans = (ans * x) % mod;
			
			x = (x * x) % mod;
			p >>= 1;
		}
		
		return ((unsigned int)ans);
	}

	unsigned int combine(
		unsigned int total_size, unsigned int len, 
		unsigned int prev_comb, unsigned int ways,
		vector<vector<unsigned int>>& binom_coef) {
		
		unsigned long int prev_comb_ull = (unsigned long int) prev_comb;
		unsigned long int ways_ull = (unsigned long int) ways;
		unsigned long int comb = binom_coef[total_size + len][len];

		unsigned long int m = (unsigned long int)1000000007ULL;
		unsigned long int ans = (prev_comb_ull * ways_ull) % m;
		ans = (ans * comb) % m;

		return (unsigned int) ans;
	}

public:
	int solve(int a, vector<unsigned int>& b) {
		unsigned int n = (unsigned int)b.size();
		if (n == 0) return 0;

		sort(b.begin(), b.end());

		vector<vector<unsigned int>> binom_coef(a + 1, vector<unsigned int>(a + 1, 0));
		unsigned int mod = 1000000007U;
		binom_coef[0][0] = 0;
		for (unsigned int i = 0; i <= (unsigned int)a; i++) {
			binom_coef[i][0] = 1;
			for (unsigned int j = 1; j <= i; j++)
				binom_coef[i][j] = (binom_coef[i - 1][j] + binom_coef[i - 1][j - 1]) % mod;
		}

		vector<int> pow_mod_2(a + 1);
		pow_mod_2[0] = 1;
		for (int i = 1; i <= a; i++) pow_mod_2[i] = (pow_mod_2[i - 1] * 2) % mod;

		unsigned int size = b[0] - 1;	// length of the tail
		unsigned int comb = 1;			// tail has only one way
		for (unsigned int i = 1; i < n; i++) {
			if (b[i] == b[i - 1] + 1) continue;

			unsigned int len = b[i] - b[i - 1] - 1;
			unsigned int ways = pow_mod_2[len-1];

			comb = combine(size, len, comb, ways, binom_coef);
			size += len;
		}

		unsigned int head_len = a - b[n - 1];
		if (head_len > 0) comb = combine(size, head_len, comb, 1, binom_coef);

		return comb;
	}
};

#if 0
void main(void) {
	CityTour t;

	unsigned int a = 14;
	vector<unsigned int> b = { 3, 7, 11 };
	// ans = 180

	//unsigned int a = 1000;
	//vector<unsigned int> b = { 6, 537, 796, 542, 214, 289, 230 };
	// ans = 282209890


	//unsigned int a = 7;
	//vector<unsigned int> b = { 1, 7 };

	cout << t.solve(a, b);
}
#endif