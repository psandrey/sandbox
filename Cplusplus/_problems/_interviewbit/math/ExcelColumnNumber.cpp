
/*
 * Given a column title as appears in an Excel sheet, return its corresponding column number.
 *
 * Example:
 *
 * 	A -> 1
 * 	B -> 2
 * 	C -> 3
 * 	...
 * 	Z -> 26
 * 	AA -> 27
 * 	AB -> 28
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class ExcelColumnNumber {
public :
	int excel_column(string s) {
		int n = s.size();
		if (n == 0) return 0;

		int col = 0;
		int digit = 1;
		for (int i = n - 1; i >= 0; i--) {
			int code = s[i] - 'A' + 1;
			col += code * digit;
			digit *= 26;
		}
		
		return col;
	}
};

#if 0
int main(void) {
	ExcelColumnNumber t;

	cout << t.excel_column("BAQTZ");
}
#endif