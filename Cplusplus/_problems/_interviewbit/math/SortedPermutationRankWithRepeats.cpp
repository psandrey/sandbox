
/*
 * Given a string, find the rank of the string amongst its permutations sorted lexicographically.
 * Note: that the characters might be repeated. If the characters are repeated, we need to look at the rank
 * in unique permutations.
 * Look at the example for more details.
 *
 * Example :
 *   Input : 'aba'
 *   Output : 2
 *
 * The order permutations with letters 'a', 'a', and 'b' :
 *   aab
 *   aba
 *   baa
 * The answer might not fit in an integer, so return your answer % 1000003
 *
 *  Note: 1000003 is a prime number
 */

#include <algorithm>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class SortedPermutationRankWithRepeats {
public:

	void prepare_fact_mod_m(vector<int>& f) {
		int n = f.size();
		if (n == 0) return;
		f[0] = 1;
		for (int i = 1; i < n; i++) {
			unsigned long long m = 1000003ULL;
			unsigned long long ii = (unsigned long long)i;
			unsigned long long fi = (unsigned long long)f[i - 1];
			unsigned long long ans = (fi * ii) % m;
			f[i] = (int) ans;
		}
	}

	void prepare_right_smaller(string s, vector<int>& right_smaller, vector<int>& counter) {
		int n = (int)s.size();
		if (n == 0) return;

		for (int i = 0; i < n; i++) { counter[(int)s[i]]++; right_smaller[(int)s[i]]++; }

		int m = (int)right_smaller.size();
		for (int i = 1; i < m; i++) right_smaller[i] += right_smaller[i - 1];
	}

	void update_right_smaller(char c, vector<int>& right_smaller) {
		int n = (int)right_smaller.size();
		for (int i = (int)c; i < n; i++) right_smaller[i]--;
	}

	int get_duplicates(char c, vector<int>& counter, vector<int>& fact) {
		unsigned long long int m = 1000003ULL;
		unsigned long long int ans = 1ULL;
		int n = (int)counter.size();

		for (int i = 0; i < n; i++) {
			if (counter[i] <= 1) continue;
		
			unsigned long long int fact_cnt = (unsigned long long int) fact[counter[i]];
			ans = (ans * fact_cnt) % m;
		}

		return (int)ans;
	}

	int egcd(int a, int b, int* x, int* y) {
		if (b == 0) return -1;
		if (a == 0) return 0;
		if (b > a) swap(a, b);

		int s2 = 1, s1 = 0;
		int t2 = 0, t1 = 1;

		while (b != 0) {
			int q = a / b;
			int r = a % b;

			int s = s2 - q * s1;
			int t = t2 - q * t1;

			s2 = s1; t2 = t1;
			s1 = s; t1 = t;

			a = b; b = r;
		}

		*x = s2; *y = t2;
		return a;
	}

	int modular_multiplicative_inverse(int a, int m) {
		int x, y, gcd;
		gcd = egcd(a, m, &x, &y);
		if (gcd != 1) return -1;

		// wrap around if y > m, and handle negaive values of y
		y = (y % m + m) % m;
		return y;
	}

	int solve(string s) {
		int n = s.size();
		vector<int> fact(n+1);
		prepare_fact_mod_m(fact);

		vector<int> right_smaller(256, 0);
		vector<int> counter(256, 0);
		prepare_right_smaller(s, right_smaller, counter);

		unsigned long long int m = 1000003ULL;
		unsigned long long int ans = 1;
		unsigned long long int mul = 1;
		for (int i = 0; i < n; i++) {
			int ord = (int)s[i];

			unsigned long long int count_smaller_right = (unsigned long long int) right_smaller[ord-1];			
			unsigned long long int mul = (unsigned long long int) fact[n-i-1];
			unsigned long long int a =((count_smaller_right * mul) % m);

			int b = get_duplicates(ord, counter, fact);
			unsigned long long int mod_mul_inv = (unsigned long long int)modular_multiplicative_inverse(b, 1000003);

			unsigned long long int a_div_b_mod_m = (mod_mul_inv * a) % m;
			ans = (ans + a_div_b_mod_m)%m;

			update_right_smaller(s[i], right_smaller);
			counter[ord]--;
		}
		
		return (int)(ans);
	}
};

#if 0
int main(void) {
	SortedPermutationRankWithRepeats t;

	//string s = "IDoAgpWmipGWOUuCIksS";
	string s = "bbdaaa";
	cout << t.solve(s);

	return 0;
}
#endif