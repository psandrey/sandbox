
/*
 * Given a positive integer, return its corresponding column title as appear in an Excel sheet.
 *
 * For example:
 *
 * 	1 -> A
 * 	2 -> B
 * 	3 -> C
 * 	...
 * 	26 -> Z
 * 	27 -> AA
 * 	28 -> AB
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class ExcelColumnTitle {
public:
	string col_title(int a) {
		string ans;
		while (a != 0) {
			//  shit one to the left and make 0(A) ... 25(Z)
			a = a - 1;

			int digit = a % 26;
			char code = (char)('A' + digit);
			ans = code + ans;

			a /= 26;
		}

		return ans;
	}
};

#if 0
int main(void) {
	ExcelColumnTitle t;

	//cout << t.col_title(943566);
	//cout << t.col_title(26);
	cout << t.col_title(474668);

	return 0;
}
#endif
