
/*
 * A robot is located at the top-left corner of an A x B grid.
 *
 * The robot can only move either down or right at any point in time.
 * The robot is trying to reach the bottom-right corner of the grid (marked �Finish�
 * in the diagram below).
 * How many possible unique paths are there?
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class GridUniquePaths {
public:

	int comb(int n, int k) {
		k = min(k, n - k);
		int c = 1;
		for (int j = 1; j <= k; j++)
			c = c * (n + 1 - j) / j;

		return c;
	}

	// T(n) = O(min(n,m))
	int uniquePaths(int n, int m) {
		if (n == 0) return m;
		if (m == 0) return n;

		return comb(n + m - 2, m - 1);
	}

	// backtraking T(n) = O((n+m)Cm)
	int uniquePaths_v2(int n, int m) {
		if (n == 0) return m;
		if (m == 0) return n;

		return grid_path(n, m, 1, 1);
	}

	int grid_path(int n, int m, int i, int j) {
		if (i == n && j == m) return 1;
		if (i > n || j > m) return 0;

		return grid_path(n, m, i, j + 1) + grid_path(n, m, i + 1, j);
	}

	// dynamic programming: T(n) = O(n*m)
	int uniquePaths_v3(int n, int m) {
		if (n == 0) return m;
		if (m == 0) return n;

		vector<vector<int>> memo(n + 1, vector<int>(m + 1, -1));
		return grid_path_memo(n, m, 1, 1, memo);
	}

	int grid_path_memo(int n, int m, int i, int j, vector<vector<int>>& memo) {
		if (memo[i][j] != -1) return memo[i][j];
		else if (i == n && j == m) memo[i][j] = 1;
		else {
			int d = 0, r = 0;
			if (j < m) r = grid_path_memo(n, m, i, j + 1, memo);
			if (i < n) d = grid_path_memo(n, m, i + 1, j, memo);

			memo[i][j] = d + r;
		}

		return memo[i][j];
	}
};

#if 0
int main(void) {

	GridUniquePaths t;
	int n = 7, m = 3;

	cout << t.uniquePaths(n, m) << endl;
	cout << t.uniquePaths_v2(n, m) << endl;
	cout << t.uniquePaths_v3(n, m) << endl;

	return 0;
}
#endif