
/*
 * Given an even number ( greater than 2 ), return two prime numbers whose sum will be equal to given number.
 *
 * Note: A solution will always exist. read Goldbach�s conjecture
 *
 * Example:
 *   Input : 4
 *   Output: 2 + 2 = 4
 *
 * Note: If there are more than one solutions possible, return the lexicographically smaller solution.
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class PrimeSum {
public:
	vector<int> prime_sum(int n) {
		if (n <= 2 || n % 2 != 0) return {};

		// Sieve of Eratosthenes
		vector<bool> p(n + 1, true);
		for (int i = 2; i <= floor(sqrt(n)); i++)
			for (int j = i * i; j <= n; j += i) p[j] = false;

		// Find prime numbers sum (Goldbach conjecture)
		int i = 2;
		for (i = 2; i <= floor(n / 2); i++)
			if (p[i] && p[n - i]) break;

		return { i, n - i };
	}
};

#if 0
int main(void) {
	int n = 16777214;

	PrimeSum t;
	vector<int> ans = t.prime_sum(n);

	cout << ans[0] << "  " << ans[1] << endl;
	return 0;
}
#endif