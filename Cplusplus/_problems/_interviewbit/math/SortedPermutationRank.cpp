
/*
 * Given a string, find the rank of the string amongst its permutations sorted lexicographically.
 * Assume that no characters are repeated.
 *
 * Example :
 *   Input : 'acb'
 *   Output : 2
 *
 * The order permutations with letters 'a', 'c', and 'b' :
 * abc
 * acb
 * bac
 * bca
 * cab
 * cba
 * The answer might not fit in an integer, so return your answer % 1000003
 */

#include <algorithm>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class SortedPermutationRank {
public:
	int rank(string s) {
		if (s.empty()) return 0;
		int n = (int)s.size();

		// how many chars on the right side are smaller
		vector<int> less_right;
		for (int j = 0; j < n - 1; j++) {
			int c = 0;
			for (int i = j + 1; i < n; i++) if (s[i] < s[j]) c++;
			less_right.push_back(c);
		}
		less_right.push_back(0);

		// pre-compute factorial
		vector<int> fact;
		fact.push_back(1);
		for (int j = 1; j < n; j++) fact.push_back( (fact[fact.size() - 1] * j) % 1000003 );

		// compute rank
		int rank = 1;
		for (int j = 0; j < n; j++)
			rank = (rank + (less_right[j] * fact[n - 1 - j]) % 1000003) % 1000003;

		return rank;
	}
};

#if 0
int main(void) {
	string s = "DTNGJPURFHYEW";
	SortedPermutationRank t;

	cout << t.rank(s);

	return 0;
}
#endif