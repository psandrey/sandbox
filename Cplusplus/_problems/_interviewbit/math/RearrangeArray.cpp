
/*
 * Rearrange a given array so that Arr[i] becomes Arr[Arr[i]] with O(1) extra space.
 * Note: let n = size(a), then
 *    All elements in the array are in the range [0, N-1]
 *    N^2 does not overflow for a signed integer
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class RearrangeArray {
public:
	void rearrange(vector<int>& a) {
		int n = (int)a.size();
		if (n <= 1) return;

		// based on the ideea that: f(x, y) = x + y * n
		//  x = f(x,y) % n, because (y*n)%m = 0
		//  y = floor(f(x,y) / n), because x/y < 1
		for (int i = 0; i < n; i++) a[i] += (a[a[i]] % n) * n;
		for (int i = 0; i < n; i++) a[i] /= n;
	}
};

#if 0
int main(void) {
	vector<int> a = { 4, 0, 2, 1, 3 };
	RearrangeArray t;

	t.rearrange(a);
	for (int x : a) cout << x << " ";
	
	return 0;
}
#endif