
/*
 * Given a set of digits (A) in sorted order, find how many numbers of length B are possible
 *  whose value is less than number C.
 *
 * Note: All numbers can only have digits from the given set.
 *
 * Input:
 *	  0 1 2 5  
 *	  2
 *	  21
 *	Output:
 *	  5 (10, 11, 12, 15, 20 are possible)
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class NumbersOfLengthNAndValueLessThanK {

public:
	vector<int> num_to_digit(int a) {
		if (a == 0) return { 0 };

		vector<int> digits;
		while (a != 0) {
			int r = a % 10;
			digits.push_back(r);
			a /= 10;
		}

		reverse(digits.begin(), digits.end());
		return digits;
	}

	int solve(vector<int>& a, int l, int k) {
		int n = (int)a.size();
		if (l == 0 || n == 0) return 0;

		vector<int> digits = num_to_digit(k);
		int m = (int)digits.size();

		// if the required length is bigger than the number of digits of k, then
		//  there is no way we can generate any number
		if (l > m) return 0;

		// if the required length is less than the number of digits  of k, then
		//  is just a k-ary tree (note: 0 can be in front)
		if (l < m) {
			if (a[0] == 0 && l > 1) return (int)((n - 1) * pow(n, l - 1));
			return (int)pow(n, l);
		}

		// if the required length is equal to the number of digits of k, then
		//  vicious problem...
		//
		// first, map the digits in k with the digits in set:
		//   true if the digit is in k and in set, false otherwise.
		vector<bool> map(10, false);
		for (int j = 0; j < m; j++) map[digits[j]] = true;
		int i = 0, j = 0;
		while (i < n || j < 10) {
			if (i < n && a[i] == j) { j++; i++; }
			else if (i < n && a[i] < j) i++;
			else { map[j] = false; j++; }
		}
		
		// this is written a little bit more complicated to avoid two loops
		vector<int> lower(11);
		lower[0] = 0; i = 0;
		for (j = 1; j <= 10; j++) {
			if (i < n && a[i] == j - 1) { lower[j] = 1; i++; }
			lower[j] += lower[j - 1];
		}
		
		vector <int> dp(l + 1);
		dp[0] = 0;
		bool add_rem = true;
		for (int len = 1; len <= l; len++) {
			dp[len] = dp[len - 1] * n;

			int rem = lower[digits[len - 1]];
			if (len == 1 && a[0] == 0 && l > 1) rem --;

			// if (i-1) digit of generated number can be equal to (i - 1) digit of k
			if (add_rem) dp[len] += rem;

			// basically, if there is a digit that is not in the set...
			//  what follows doesn't matter... there is no remaining digits to add
			add_rem = add_rem & (map[digits[len - 1]]);
			//add_rem = add_rem & (lower[digits[len - 1] + 1] == lower[digits[len - 1]] + 1);
		}

		return dp[l];
	}
};

#if 0
int main(void) {
	vector<int> a = { 1, 2, 3 };
	int len = 4;
	int k = 3231;
	

	//vector<int> a = { 2, 3, 5, 6, 7, 9 };
	//int len = 5;
	//int k = 42950;

	//vector<int> a = { 2, 3, 5 };
	//int len = 3;
	//int k = 425;


	NumbersOfLengthNAndValueLessThanK t;
	cout << t.solve(a, len, k) << endl;
}
#endif