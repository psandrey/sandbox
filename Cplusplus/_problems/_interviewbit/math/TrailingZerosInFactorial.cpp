
/*
 * Given an integer n, return the number of trailing zeroes in n!.
 *
 * Example:
 *   n = 5
 *   n! = 120
 *   Number of trailing zeros = 1
 *   So, return 1
 */

#include <algorithm>
#include <iostream>
using namespace std;

class TrailingZerosInFactorial {
public:
	int trailing(int n) {
		if (n < 5) return 0;

		int pow5 = 1, ans = 0;
		for (int k = 1; (pow5 *= 5) <= n; k++) 
			ans += (int)floor(n / pow5);

		return ans;
	}
};

#if 0
int main(void) {
	TrailingZerosInFactorial t;

	cout << t.trailing(30);
	return 0;
}
#endif