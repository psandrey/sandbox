
/*
 * Given a positive integer which fits in a 32 bit signed integer,
 * find if it can be expressed as A^P where P > 1 and A > 0. A and P both should be integers.
 *
 * Example
 *   Input : 4
 *   Output : True
 *   as 2^2 = 4.
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class PowerOfTwoIntegers {
public:
	bool power_of_two_v1(int n) {
		if (n == 1) return true;
		if (n <= 3) return false;

		double logn = log10(n);
		int sq = (int)ceil(sqrt(n));
		for (int i = 2; i <= sq; i++) {
			double logi = log10(i);
			double logn_basei = logn / logi;
#if 1			
			if (floor(logn_basei) != logn_basei) continue;
			return true;
#else
			if (pow(i, logn_basei) == n) return true;
#endif
		}

		return false;
	}

	bool power_of_two_v2(int n) {
		if (n == 1) return true;
		if (n <= 3) return false;

		int r = -1;
		for (int i = 2; i <= n; i++) {
			if (is_prime(i) == false) continue;
			if (n % i != 0) continue;

			pair<int, int> power_div = get_power(n, i);
			if (power_div.first == 1) return false;

			if (r == -1) r = power_div.first;
			else r = gcd(r, power_div.first);

			if (r == 1) return false;

			n /= power_div.second;
		}

		// if r == -1 then n is prime number
		return (r != -1);
	}

	int gcd(int a, int b) {
		if (b == 0) return -1;
		if (a == 0) return 0;

		while (b != 0) {
			int q = a / b;
			int r = a % b;
			a = b; b = r;
		}

		return a;
	}

	bool is_prime(int n) {
		for (int i = 2; i <= floor(sqrt(n)); i++)
			if (n % i == 0) return false;

		return true;
	}

	pair<int, int> get_power(int n, int k) {
		int p = 0;
		int div = 0;
		for (int d = k; d <= n; d *= k) {
			if ((n % d) == 0) { p++; div = d; }
			else return make_pair(p, div);
		}

		return make_pair(p, div);
	}
};

#if 0
int main(void) {
	PowerOfTwoIntegers t;
	//int n = 16808;
	//int n = 24;
	int n = 536870912;

	cout << t.power_of_two_v1(n) << endl;
	cout << t.power_of_two_v2(n) << endl;
	return 0;
}
#endif