
/*
 * Given an array of N non-negative integers, find the sum of hamming distances
 *  of all pairs of integers in the array.
 *
 *  Return the answer modulo 1000000007.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class SumPairwiseHammingDistance {
public:
	int hammingDistance(const vector<int>& a) {
		if (a.empty()) return 0;
		int n = (int)a.size();

		int m = 1000000007;
		int s = 0, mask = 1;
		for (int k = 0; k <= 31; k++) {

			int c = 0;
			for (int j = 0; j < n; j++)
				if ((a[j] & mask) != 0) c++;
			
			s = (s + ((long long)c * (n - c)) % m) % m;
			mask <<= 1;
		}

		return ((2LL * s) % m);
	}

};

#if 0
int main(void) {
	vector<int> a = { 81, 13, 2, 7, 96 };
	
	SumPairwiseHammingDistance t;
	cout << t.hammingDistance(a) << endl;

	return 0;
}
#endif