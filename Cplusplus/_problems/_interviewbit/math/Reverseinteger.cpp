
/*
 * Reverse digits of an integer.
 *
 * Example1:
 *   x = 123,
 *  return 321
 *
 * Note: Return 0 if the result overflows and does not fit in a 32 bit signed integer
*/

#include <algorithm>
#include <iostream>
using namespace std;

class ReverseInteger {
public:
	int reverse(int n) {
		long long x = n;

		if (x < 0) x = -x;
		long long reversed = 0;
		while (x > 0) {
			long long r = x % 10;
			x = x / 10;
			reversed = reversed * 10 + r;
		}
		if (reversed > INT_MAX) return 0;

		if (n < 0) return -((int)(reversed));
		return (int)reversed;
	}
};

#if 0
int main(void) {
	ReverseInteger t;

	cout << t.reverse(-2);
}
#endif