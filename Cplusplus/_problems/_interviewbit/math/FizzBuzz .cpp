
#include <vector>
#include <algorithm>
#include <string>
#include <iostream>
using namespace std;

class FizzBuzz {
public:
	vector<string> fizz_buzz(int n) {
		
		vector<string> ans(n);
		for (int i = 1; i <= n; i++) {
			int r3 = i % 3;
			int r5 = i % 5;
			if (r3 == 0 || r5 == 0) {
				if (r3 == 0) ans[i - 1] = "fizz";
				if (r5 == 0) ans[i - 1] += "buzz";
			} else ans[i - 1] = to_string(i);
		}

		return ans;
	}
};

/*
int main(void) {
	FizzBuzz t;

	vector<string> ans = t.fizz_buzz(20);
	for (string s : ans) cout << s << "  ";
	cout << endl;

	return 0;
}
*/