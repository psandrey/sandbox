
/*
 * Determine whether an integer is a palindrome. Do this without extra space.
 *
 * A palindrome integer is an integer x for which reverse(x) = x where reverse(x) is x with its digit reversed.
 * Negative numbers are not palindromic.
 *
 * Example :
 *   Input : 12121
 *   Output : True
 *
 *   Input : 123
 *   Output : False
 */

#include <algorithm>
#include <iostream>
using namespace std;

class PalindromeInteger {
public:
	bool is_palindrome (int n){
		if (n < 0) return false;
		
		int reversed = 0;
		int x = n;
		while (x > 0) {
			int r = x % 10;
			x = x / 10;
			reversed = reversed * 10 + r;
		}

		return (reversed == n);
	}
};

/*
int main(void) {
	PalindromeInteger t;
	cout << t.is_palindrome(231);

	return 0;
}
*/