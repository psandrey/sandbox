
/*
 * You are given two positive numbers A and B. You need to find the maximum valued integer X such that:
 *
 * X divides A i.e. A % X = 0
 * X and B are co-prime i.e. gcd(X, B) = 1
 * 
 * For example,
 *   A = 30
 *   B = 12
 * We return
 *   X = 5
 */

#include <vector>
#include <iostream>
using namespace std;

class LargestCoprimeDivisor {
public:
	static int gcd(int a, int b) {
		//if (a == 0 && b == 0) is not defined
		if (a == 0) return a;
		if (b == 0) return b;
		
		if (a == b) return a;
		else if (b > a) swap(a, b);

		while (b != 0) {
			int r = a % b;
			a = b; b = r;
		}

		return a;
	}

	int greatest_coprime_div(int a, int b) {
		int div = 1;
		while ((div = gcd(a, b)) != 1) a /= div;

		return a;
	}
};

#if 0
int main(void) {
	int x = 3;
	int y = 7;

	LargestCoprimeDivisor t;
	cout << t.greatest_coprime_div(x, y) << endl;

	return 0;
}
#endif