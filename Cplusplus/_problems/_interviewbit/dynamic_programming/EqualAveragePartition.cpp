/*
 * Given an array with non negative numbers, divide the array into two parts such that the average of both the parts is equal.
 * Return both parts (If exist).
 * If there is no solution. return an empty list.
 *
 * Input:
 *   [1 7 15 29 11 9]
 *
 * Output:
 *   [9 15] [1 7 11 29]
 */

#include <vector>
#include <stack>
#include <iostream>
#include <numeric>
#include <unordered_set>
#include <algorithm>
using namespace std;

using VecSolutionsForSums = vector<bool>;
using VecSolutionsForSizeAndSums = vector<VecSolutionsForSums>;
using Opt = vector<VecSolutionsForSizeAndSums>;

class EqualAveragePartition {
public:
	// dynamic programming version
	vector<vector<int> > avgset(vector<int>& a);

	// brute force version
	vector<vector<int> > avgset_bf(vector<int>& a);
private:
	bool isPossible_memo(const vector<int>& a, size_t idx, int c_sum, size_t c_size,
						 vector<vector<vector<bool>>>& memo, vector<int>& set);

	bool isPossible_bf(const vector<int>& a, size_t idx, int c_sum, size_t c_size,
					   vector<int>& set);
};

/*************************************************************************************************/
/* Dynamic programming version                                                                   */
/*************************************************************************************************/
// T(n) = O(S.n^3)
// S(n) = O(s.n^2)
bool EqualAveragePartition::isPossible_memo(const vector<int>& a, size_t idx, int c_sum, size_t c_size,
											vector<vector<vector<bool>>>& memo, vector<int>& set) {
	const auto n = a.size();

	if (c_size == 0) return (c_sum == 0);
	if (idx == n) return false;
	if (memo[idx][c_size][c_sum] == false) return false; // prune branch

	// case 1: take current element if possible
	const auto x = a[idx];
	if (c_sum - x >= 0) {
		set.emplace_back(x);

		if (isPossible_memo(a, idx + 1, c_sum - x, c_size - 1, memo, set)) return true;

		set.pop_back();
	}

	// case 2: do not take current element
	if (isPossible_memo(a, idx + 1, c_sum, c_size, memo, set)) return true;

	memo[idx][c_size][c_sum] = false;
	return false;
}

static vector<int> get_set2(const vector<int>& a, const vector<int>& set1) {
	int n = a.size();
	int m = set1.size();
	vector<int> set2;
	set2.reserve(n - m);

	int j = 0;
	for (int i = 0; i < n; i++) {
		const auto x = a[i];
		if (j < m && x == set1[j]) { j++; continue; }

		set2.emplace_back(x);
	}

	return set2;
}

vector<vector<int> > EqualAveragePartition::avgset(vector<int>& a) {
	if (a.empty()) return {};
	const auto n = a.size();
	if (n < 2) return {};

	// Note: so that a sorted vector of indices represents a sorted vector of values
	// Thanks: Abhishek
	sort(a.begin(), a.end());

	const int sum = accumulate(a.begin(), a.end(), 0);
	// memo[idx][size][sum]
	vector<vector<vector<bool>>> memo(n + 1, vector <vector<bool>>(n + 1, vector<bool>(sum + 1, true)));

	const auto half = ((n + 1) / 2);
	for (auto k = 1u; k <= half; k++) {
		const auto s = k * sum;
		if (s % n != 0) continue;

		const auto c_sum = s / n;
		vector<int> set1;
		set1.reserve(n);

		if (!isPossible_memo(a, 0, c_sum, k, memo, set1)) continue;

		const auto set2 = get_set2(a, set1);
		if (!set2.empty()) 
			return { set1, set2 };
	}

	return {};
}

/*************************************************************************************************/
/* Brute - force version                                                                         */
/*************************************************************************************************/
// T(n) = O(n.2^n)
// S(n) = O(2^n)
vector<vector<int>> EqualAveragePartition::avgset_bf(vector<int>& a) {
	if (a.empty()) return {};
	const auto n = a.size();
	if (n < 2) return {};

	// Note: so that a sorted vector of indices represents a sorted vector of values
	// Thanks: Abhishek
	sort(a.begin(), a.end());

	const int sum = accumulate(a.begin(), a.end(), 0);
	const auto half = ((n + 1) / 2);
	for (auto k = 1u; k <= half; k++) {
		const auto s = k * sum;
		if (s % n != 0) continue;

		const auto c_sum = s / n;
		vector<int> set1;
		set1.reserve(n);

		if (!isPossible_bf(a, 0, c_sum, k, set1)) continue;

		const auto set2 = get_set2(a, set1);
		if (!set2.empty())
			return { set1, set2 };
	}

	return {};
}

bool EqualAveragePartition::isPossible_bf(const vector<int>& a, size_t idx, int c_sum, size_t c_size,
										  vector<int>& set) {
	const auto n = a.size();

	if (c_size == 0) return (c_sum == 0);
	if (idx == n) return false;

	// case 1: take current element if possible
	const auto x = a[idx];
	if (c_sum - x >= 0) {
		set.emplace_back(x);

		if (isPossible_bf(a, idx + 1, c_sum - x, c_size - 1, set)) return true;

		set.pop_back();
	}

	// case 2: do not take current element
	if (isPossible_bf(a, idx + 1, c_sum, c_size, set)) return true;

	return false;
};

void print_ans(vector<vector<int>> ans) {
	if (ans.empty()) return;

	cout << "[ ";
	for (vector<int>::iterator it = ans[0].begin(); it != ans[0].end(); ++it) cout << *it << ",";
	cout << "] [";
	for (vector<int>::iterator it = ans[1].begin(); it != ans[1].end(); ++it) cout << *it << ",";
	cout << "] " << endl;
}

/*
int main(void) {
	//vector<int> a = { 1, 7, 15, 29, 11, 9 };
	//vector<int> a = { 47, 14, 30, 19, 30, 4, 32, 32, 15, 2, 6, 24 };
	//vector<int> a = { 20, 23, 49, 24, 29, 16, 9, 5, 6, 12, 6, 20, 41, 20 };
	//vector<int> a = { 33, 2, 12, 25, 38, 11, 46, 17, 19, 47, 13, 0, 39, 42, 8, 4 };
	vector<int> a = { 33, 0, 19, 49, 29, 29, 28, 41, 36, 40, 24, 34, 35, 26, 1, 0, 27, 12, 13, 50, 4, 0, 45, 39, 26 };
	//vector<int> a = { 0, 0, 12, 0, 5, 5, 5, 5 };
	//vector<int> a = { 35, 44, 12, 39, 44, 19, 25, 2 };
	//vector<int> a = { 47, 14, 30, 19, 30, 4, 32, 32, 15, 2, 6, 24 };

	EqualAveragePartition t;
	vector < vector<int> > ans = t.avgset(a);
	cout << "Dynamic-prgramming:" << endl;
	print_ans(ans);

	ans = t.avgset_bf(a);
	cout << "Brute-force:" << endl;
	print_ans(ans);

	return 0;
}
*/