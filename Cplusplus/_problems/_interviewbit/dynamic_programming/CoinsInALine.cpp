
/*
 * There are N coins (Assume N is even) in a line.
 * Two players take turns to take a coin from one of the ends of the line until there are no more coins left.
 * The player with the larger amount of money wins. Assume that you go first.
 *
 * Write a program which computes the maximum amount of money you can win.
 *
 * Example: suppose that there are 4 coins which have value: 1 2 3 4
 *  now you are first so you pick 4
 *  then in next term
 *  next person picks 3 then
 *  you pick 2 and
 *  then next person picks 1
 *  so total of your money is 4 + 2 = 6
 *  next/opposite person will get 1 + 3 = 4
 *  so maximum amount of value you can get is 6
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class CoinsInALine {
public:

	int maxcoin(vector<int>& a) {
		int n = (int)a.size();
		// memoization 1 (fails for large test cases)
		vector<vector<int>> memo(n, vector<int>(n, -1));
		//return maxcoin_memo_1(a, 0, n - 1, memo);
		
		// memoization 2 (classic max-min game)
		return maxcoin_memo_2(a, 0, n - 1, memo);
	}

	// memoization 1
	int maxcoin_memo_1(vector<int>& a, int i, int j, vector<vector<int>>& memo) {
		if (i == j) return a[i];
		if (i > j) return 0;
		if (memo[i][j] != -1)
			return memo[i][j];

		int ans = 0;
		if (j - i == 1) ans = max(a[i], a[j]);
		else {
			int candidate_left = 0;
			if (a[j] > a[i + 1]) candidate_left = maxcoin_memo_1(a, i + 1, j - 1, memo);
			else candidate_left = maxcoin_memo_1(a, i + 2, j, memo);

			int candidate_right = 0;
			if (a[j - 1] > a[i]) candidate_right = maxcoin_memo_1(a, i, j - 2, memo);
			else candidate_right = maxcoin_memo_1(a, i + 1, j - 1, memo);

			ans = max(a[i] + candidate_left, a[j] + candidate_right);
		}
		
		memo[i][j] = ans;
		return memo[i][j];
	}


	int maxcoin_memo_2(vector<int>& a, int i, int j, vector<vector<int>>& memo) {
		if (i > j) return 0;
		else if (i == j) return a[i];
		else if (memo[i][j] != -1) return memo[i][j];

		int left = a[i] + min(maxcoin_memo_2(a, i + 2, j, memo), maxcoin_memo_2(a, i + 1, j - 1, memo));
		int right = a[j] + min(maxcoin_memo_2(a, i + 1, j - 1, memo), maxcoin_memo_2(a, i, j - 2, memo));
		memo[i][j] = max(left, right);

		return memo[i][j];
	}
};

#if 0
int main(void) {
	//vector<int> a = { 1, 2, 3, 4 };
	//vector<int> a = { 2, 5, 3, 1 };
	vector<int> a = { 75, 89, 53, 42, 65, 27};

	CoinsInALine t;
	cout << t.maxcoin(a);

	return 0;
}
#endif