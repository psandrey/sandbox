
/*
 * Given a string s1, we may represent it as a binary tree by partitioning it to two non-empty substrings recursively.
 *
 * Below is one possible representation of s1 = �great�:
 *
 *
	great
   /    \
  gr    eat
 / \    /  \
g   r  e   at
		   / \
		  a   t
 *
 * To scramble the string, we may choose any non-leaf node and swap its two children.
 *
 * For example, if we choose the node �gr� and swap its two children, it produces a scrambled string �rgeat�.

	rgeat
   /    \
  rg    eat
 / \    /  \
r   g  e   at
		   / \
		  a   t
 * We say that �rgeat� is a scrambled string of �great�.
 *
 * Similarly, if we continue to swap the children of nodes �eat� and �at�, it produces a scrambled string �rgtae�.
 *
	rgtae
   /    \
  rg    tae
 / \    /  \
r   g  ta  e
	   / \
	  t   a
 * We say that �rgtae� is a scrambled string of �great�.
 *
 * Given two strings s1 and s2 of the same length, determine if s2 is a scrambled string of s1. Return 0/1 for this problem.
 */

#include <algorithm>
#include <map>
#include <vector>
#include <iostream>
using namespace std;

class ScrambleString {
public:


	bool isScramble(string s1, string s2) {
		map<pair<string, string>, bool> memo;
		return is_scrbl(s1, s2, memo);
	}

	bool is_scrbl(string s1, string s2, map<pair<string, string>, bool>& memo) {
		int n = (int)s1.size();
		int m = (int)s2.size();
		if (n != m) return false;

		pair<string, string> key = make_pair(s1, s2);
		if (memo.find(key) != memo.end()) return memo[key];

		if (n == 0 || s1.compare(s2) == 0) {
			memo[key] = true;
			return true;
		}

		string ss1(s1); sort(ss1.begin(), ss1.end());
		string ss2(s2); sort(ss2.begin(), ss2.end());
		if (ss1.compare(ss2) != 0) {
			memo[key] = false;
			return false;
		}

		bool ans = false;
		for (int i = 1; i < n; i++) {
			string s1_0i = s1.substr(0, i);
			string s1_in = s1.substr(i, n - i);

			string s2_0i = s2.substr(0, i);
			string s2_in = s2.substr(i, n - i);

			string s2_0ni = s2.substr(0, n - i);
			string s2_nin = s2.substr(n - i, i);

			// mirrored
			bool r1 = is_scrbl(s1_0i, s2_0i, memo);
			bool r2 = is_scrbl(s1_in, s2_in, memo);
			if (r1 && r2) { ans = true; break; }

			// switched
			r1 = is_scrbl(s1_0i, s2_nin, memo);
			r2 = is_scrbl(s1_in, s2_0ni, memo);
			if (r1 && r2) { ans = true; break; }
		}

		memo[key] = ans;
		return ans;
	}
};

#if 0
int main(void) {
	string s1 = "abbbcbaaccacaacc";
	string s2 = "acaaaccabcabcbcb";

	ScrambleString t;
	cout << t.isScramble(s1, s2);

	return 0;
}
#endif
