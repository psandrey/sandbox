
/*
 * Given a set of strings. Find the length of smallest string which has all the strings
 * in the set as substring.
 *
 * Constraints:
 *  1) 1 <= Number of strings <= 18
 *  2) Length of any string in the set will not exceed 100.
 *
 * Example:
 * Input: [�abcd�, �cdef�, �fgh�, �de�]
 * Output: 8 (Shortest string: �abcdefgh�)
 *
 * Note: The problem reduces to finding the shortest hamiltonian path (i.e. Bellman-Held-Karp algorithm: dp&bit_mask).
 *
 */

#include <unordered_set>
#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class ShortestCommonSuperstring {
public:
	int solve(vector<string>& a) {
		if (a.empty()) return 0;

		// 1. eliminate duplicates and substrings
		int n = (int)a.size();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (j == i) continue;
				while (j < n && i < n && a[i].find(a[j]) != a[i].npos) {
					n--;
					a[j] = a[n];
				}
			}
		}

		// 2. precompute shortest common superstring of pairs
		//    basically, the cost of edge from i to j: w[i][j] (recalling shortest hamiltionian path)
		vector<vector<int>> super_len(n, vector<int>(n, 0));
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++) {
				if (j != i) super_len[i][j] = superLen(a[i], a[j]);
			}

		// 3. Bellman-Held-Karp: find the shortest hamiltonian path
		int all = (1 << n) - 1; // 2^n - 1
		vector<vector<int>> dp(all + 1, vector<int>(n));
		for (int j = 0; j < n; j++) dp[1 << j][j] = (int)a[j].size();

		for (int mask = 3; mask <= all; mask++) {
			for (int i = 0; i < n; i++) {
				if ((mask & (1 << i)) == 0) continue; // if i is not in the mask
				if ((mask ^ (1 << i)) == 0) continue; // if i is the only string in the mask
				dp[mask][i] = INT_MAX;

				for (int j = 0; j < n; j++) {
					if ((mask & (1 << j)) == 0) continue; // if j is not in the mask
					if (j == i) continue;

					int mask_no_i = (mask ^ (1 << i));
					if (dp[mask][i] > dp[mask_no_i][j] + super_len[j][i] - (int)a[j].size())
						dp[mask][i] = dp[mask_no_i][j] + super_len[j][i] - (int)a[j].size();
				}
			}
		}

		int shortest = INT_MAX;
		for (int j = 0; j < n; j++) shortest = min(shortest, dp[all][j]);

		return shortest;
	}

	int superLen(string a, string b) {
		int overlap = 0;
		int n = (int)a.size();
		int m = (int)b.size();
		
		for (int i = n - min(n, m); i < n; i++) {
			string suf = a.substr(i, n - i);
			string pref = b.substr(0, n - i);
			if (suf.compare(pref) == 0) { overlap = n - i; break; }
		}

		return (n + m - overlap);
	}
};

#if 0
int main(void) {
	vector<string> a = { "cdab", "abc", "eab" };
	//vector<string> a = { "ltouujdrbwc", "rcgbflqpottp", "grwvgajcrgwdlp" }; //37

	ShortestCommonSuperstring t;
	cout << t.solve(a);

	return 0;
}
#endif