
/*
 * Given a string s and a dictionary of words dict, add spaces in s to construct a sentence
 * where each word is a valid dictionary word. Return all such possible sentences.
 *
 * Note: Make sure the strings are sorted in your result.
 */

#include <map>
#include <algorithm>
#include <stack>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class WordBreakII {
public:
	vector<string> wordBreak(string a, vector<string>& d) {
		vector<string> ans;
		if (a.empty() || d.empty()) return ans;
		int n = (int)a.size(), m = (int)d.size();

		// remove duplicates from dictionary
		map<string, int> hm;
		for (int j = 0; j < m; j++) hm[d[j]] = j;

		// do the dp
		vector<vector<int>> dp(n, vector<int>());
		for (int k = 0; k < n; k++) {
			string word = "";
			for (int j = k; j < n; j++) {
				word += a[j];
				if (hm.find(word) != hm.end()) {
					int i = hm[word];
					dp[k].push_back(i);
				}
			}
		}

		//build ans
		vector<string> st;
		build_ans(st, dp, 0, n, ans, d);

		return ans;
	}

	void build_ans(vector<string>& st, vector<vector<int>>& dp, int j, int n,
					vector<string>& ans, vector<string>& d) {
		if (j == n) {
			string s = st[0];
			for (int k = 1; k < (int)st.size(); k++) s += " " + st[k];
			ans.push_back(s);
		} else {
			for (int i = 0; i < (int)dp[j].size(); i++) {
				st.push_back(d[dp[j][i]]);
				build_ans(st, dp, j + d[dp[j][i]].size(), n, ans, d);
				st.pop_back();
			}
		}
	}
};

#if 0
int main(void) {
	//string a = "catsanddog";
	//vector<string> d = { "cat", "cats", "and", "sand", "dog" };
	
	//string a = "aaabaaaaab";
	//vector<string> d = { "aabbbaaa", "aaba", "a", "b", "a", "babbbabb", "baaaab" };

	string a = "aabbbabaaabbbabaabaab";
	vector<string> d = { "bababbbb", "bbbabaa", "abbb", "a", "aabbaab", "b", "babaabbbb", "aa", "bb" };

	//string a = "dda";
	//vector<string> d = { "d","dd","dda", "a" };

	WordBreakII t;
	vector<string> ans = t.wordBreak(a, d);

	for (string s : ans) cout << "[" << s << "]" << endl;


	return 0;
}
#endif