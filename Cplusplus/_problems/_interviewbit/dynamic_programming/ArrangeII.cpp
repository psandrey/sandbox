
/*
 * You are given a sequence of black and white horses, and a set of K stables numbered 1 to K.
 * You have to accommodate the horses into the stables in such a way that the following conditions are satisfied:
 *
 * You fill the horses into the stables preserving the relative order of horses.
 * For instance, you cannot put horse 1 into stable 2 and horse 2 into stable 1.
 * You have to preserve the ordering of the horses.
 * No stable should be empty and no horse should be left unaccommodated.
 * Take the product (number of white horses * number of black horses) for each stable and take the sum of all these products.
 * This value should be the minimum among all possible accommodation arrangements
 *
 * Example:
 *   Input: {WWWB} , K = 2
 *   Output: 0
 *
 * Explanation:
 *   We have 3 choices {W, WWB}, {WW, WB}, {WWW, B}
 *   for first choice we will get 1*0 + 2*1 = 2.
 *   for second choice we will get 2*0 + 1*1 = 1.
 *   for third choice we will get 3*0 + 0*1 = 0.
 *
 * Of the 3 choices, the third choice is the best option.
 */

#include <string>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class ArrangeII {
public:
	int arrange(string a, int stables) {
		int n = (int)a.size();
		if (n == 0 || stables == 0 || stables > n) return -1;

		vector<vector<int>> memo(n, vector<int>(stables + 1, -1));
		return helper(a, 0, stables, memo);
	}

private:
	int helper(string a, int i, int stables, vector<vector<int>>& memo) {
		int n = (int)a.size();
		if (memo[i][stables] != -1)	return memo[i][stables];

		// if we have only one stable... put all the horses in it
		if (stables == 1) {
			memo[i][stables] = product(a, i, n - 1);
			return memo[i][stables];
		}

		// the current stable starts at i, so lets see how many horses we can put in the 
		//  current stable to obtain the best score (i.e. starting from horse (i+1) to horse (n-(stables-1))
		//  compute the score and choose the best one).
		int best = INT_MAX;
		for (int j = i + 1; j <= n - stables + 1; j++) {
			int candidate = helper(a, j, stables - 1, memo);
			int score = product(a, i, j - 1);
			best = min(best, (score + candidate));
		}

		memo[i][stables] = best;
		return memo[i][stables];
	}

	int product(string a, int i, int j) {
		int w = 0, b = 0;
		for (int k = i; k <= j; k++) {
			if (a[k] == 'W') w++;
			else b++;
		}

		return (w * b);
	}
};

#if 0
int main(void) {
	string a = "WWWBBWWWBBWBWBWWBBWBWBWWBBBB";
	int k = 4;

	ArrangeII t;
	cout << t.arrange(a, k) << endl;

	return 0;
}
#endif