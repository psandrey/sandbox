
/*
 * Find the contiguous subarray within an array (containing at least one number) which has the largest product.
 * Return an integer corresponding to the maximum product possible.
 *
 * Example :
 *   Input : [2, 3, -2, 4]
 *   Return : 6
 *
 * Possible with [2, 3]
 */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class MaxProductSubarray {
public:
	int max_prod_contig_subarray(vector<int>& a) {
		int n = (int)a.size();
		if (n == 0) return 0;
		if (n == 1) return a[0];

		int max_end_here = a[0];
		int min_end_here = a[0];
		int ans = a[0];
		
		/*
		The idea is to keep track when a negative number appears: 
		1. When a[i]<0 then the min_end_here (which is < 0, otherwise it doesn't matter)
		   multiplied by a[i] will give the max_end_here;
		2. When a[i] > 0 then the max_end_here = max_end_here * a[i] (which is positive).
		Note: there are 2 corner cases: when a[i] = 0 and when we encounter first a[i] < 0.
		      - fist a[i] < 0 sets min_end_here = a[i]
			  - a[i] = 0 cut all previous subarrays
		*/
		for (int i = 1; i < n; i++) {
			if (a[i] > 0) {
				// Note: handles the case when max_end_here = 0 (that's why we need max and min)
				max_end_here = max(a[i], max_end_here * a[i]);
				min_end_here = min(a[i], min_end_here * a[i]);
			} else {
				// switch
				int temp = max_end_here;
				max_end_here = max(a[i], min_end_here * a[i]);
				min_end_here = min(a[i], temp * a[i]);
			}

			ans = max(ans, max_end_here);
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = { -2, 3, 0, -2, -3, -3, 0 };

	MaxProductSubarray t;
	cout << t.max_prod_contig_subarray(a);
	
	return 0;
}
#endif