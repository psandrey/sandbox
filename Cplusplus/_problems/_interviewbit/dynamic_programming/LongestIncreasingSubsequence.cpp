/*
 * Find the longest increasing subsequence of a given sequence / array.
 * In other words, find a subsequence of array in which the subsequence�s elements are in strictly increasing order,
    and in which the subsequence is as long as possible. 
 * This subsequence is not necessarily contiguous, or unique.
 * In this case, we only care about the length of the longest increasing subsequence.
 */

#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class LongestIncreasingSubsequence {
public:
	int longestSubSeq(const vector<int>&);
};

int LongestIncreasingSubsequence::longestSubSeq(const vector<int>& a) {
	if (a.empty()) return 0;

	int n = a.size();
	int *dp = new int[n];
	memset(dp, 0, sizeof(int) * n);
	
	int ans = 1;
	dp[0] = 1;
	for (int i = 1; i < n; i++) {
		dp[i] = 1;
		for (int j = 0; j < i; j++)
			if (a[i] > a[j]) dp[i] = max(dp[i], dp[j] + 1);
		ans = max(ans, dp[i]);
	}

	delete[]dp;
	dp = nullptr;
	return ans;
}

/*
int main(void) {
	vector<int> A = { 0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15 };
	LongestIncreasingSubsequence t;
	cout << "Length of the longest subsequence: " << t.longestSubSeq(A) << endl;
	return 0;
}
*/