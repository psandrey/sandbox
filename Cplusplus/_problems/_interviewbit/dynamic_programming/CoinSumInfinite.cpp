
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class CoinSumInfinite {
public:
	int coins(vector<int>& c, int S) {
		int n = (int)c.size();
		vector<int> dp(S + 1, 0);
		dp[0] = 1;
		for (int i = 0; i < n; i++)
			for (int s = 1; s <= S; s++)
				if (s >= c[i]) dp[s] = (dp[s] + dp[s - c[i]]) % 1000007;

		return dp[S];
	}
};

#if 0
int main(void) {
	vector<int> c = { 18, 24, 23, 10, 16, 19, 2, 9, 5, 12, 17, 3, 28, 29, 4, 13, 15, 8 };
	int S = 458;

	CoinSumInfinite t;
	cout << t.coins(c, S);

	return 0;
}
#endif