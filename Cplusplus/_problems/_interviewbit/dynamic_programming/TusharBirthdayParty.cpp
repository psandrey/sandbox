/*
 * As it is Tushar�s Birthday on March 1st, he decided to throw a party to all his friends at TGI Fridays in Pune.
 * Given are the eating capacity of each friend, filling capacity of each dish and cost of each dish. A friend is satisfied if the sum of the filling capacity of dishes he ate is equal to his capacity. Find the minimum cost such that all of Tushar�s friends are satisfied (reached their eating capacity).
 *
 * NOTE:
 *
 * Each dish is supposed to be eaten by only one person. Sharing is not allowed.
 * Each friend can take any dish unlimited number of times.
 * There always exists a dish with filling capacity 1 so that a solution always exists.
 * Input Format
 *
 * Friends : List of integers denoting eating capacity of friends separated by space.
 * Capacity: List of integers denoting filling capacity of each type of dish.
 * Cost :    List of integers denoting cost of each type of dish.
 * Constraints:
 * 1 <= Capacity of friend <= 1000
 * 1 <= No. of friends <= 1000
 * 1 <= No. of dishes <= 1000
 *
 * Example:
 *
 * Input:
 * 	2 4 6
 * 	2 1 3
 * 	2 5 3
 *
 * Output:
 * 	14
 *
 * Explanation:
 * 	First friend will take 1st and 2nd dish, second friend will take 2nd dish twice.
 *  Thus, total cost = (5+3)+(3*2)= 14
 */
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class Party{
public:
	int party(vector<int> , vector<int>, vector<int>);
};

int Party::party(vector<int> friends, vector<int> caps, vector<int> cost) {
	if (friends.empty() || caps.empty() || cost.empty()) return 0;
	
	int max_friend = 0;
	for (int cap : friends) max_friend = max(max_friend, cap);

	int n = caps.size();
	vector<int> dp(max_friend+1);
	dp[0] = 0;
	for (int cap = 1; cap <= max_friend; cap++) {
		dp[cap] = INT_MAX;
		for (int j = 0; j < n; j++)
			if (cap - caps[j] >= 0)
				dp[cap] = min(dp[cap], dp[cap - caps[j]] + cost[j]);
	}

	int ans = 0;
	for (int i = 0; i < (int)friends.size(); i++) ans += dp[friends[i]];

	return ans;
}

#if 0
int main(void) {
	//vector < int> friends = { 4, 6 };
	//vector < int> caps = { 1, 3 };
	//vector < int> cost = { 5, 3 };
	vector < int> friends = {665, 154, 269, 501, 998, 992, 904, 763, 254, 591, 869, 843, 683, 708, 410, 88, 352, 566, 497, 252, 486, 565, 115, 585, 414, 864, 23, 389, 308};
	vector < int> caps = { 1, 586, 973, 418, 573, 193, 416, 566, 815, 179, 538, 406, 766, 381, 807, 194, 510, 894, 264, 76, 111, 515, 281, 675, 630, 865, 807, 213, 887, 914, 520, 433, 501 };
	vector < int> cost = { 493, 570, 792, 404, 985, 77, 219, 883, 334, 343, 649, 714, 151, 561, 942, 763, 825, 737, 592, 340, 18, 267, 688, 601, 75, 900, 488, 988, 421, 639, 208, 632, 209 };
	Party t;

	cout<< t.party(friends, caps, cost) << endl;

	return 0;
}
#endif