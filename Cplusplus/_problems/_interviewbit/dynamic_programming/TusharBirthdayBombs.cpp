
/*
 * It�s Tushar�s birthday today and he has N friends. Friends are numbered [0, 1, 2, �., N-1] and i-th friend
 * have a positive strength S(i). Today being his birthday, his friends have planned to give him birthday bombs (kicks :P).
 * Tushar�s friends know Tushar�s pain bearing limit and would hit accordingly.
 * If Tushar�s resistance is denoted by R (>=0) then find the lexicographically smallest order of friends to kick Tushar
 * so that the cumulative kick strength (sum of the strengths of friends who kicks) doesn�t exceed his resistance capacity
 * and total no. of kicks hit are maximum. Also note that each friend can kick unlimited number of times
 * (If a friend hits x times, his strength will be counted x times).
 *
 * Note:
 *   Answer format = Vector of indexes of friends in the order in which they will hit.
 *   Answer should have the maximum no. of kicks that can be possibly hit. If two answer have the same no. of kicks,
 *   return one with the lexicographically smaller.
 *   [a1, a2, ..., am] is lexicographically smaller than [b1, b2, .., bm] if a1 < b1 or (a1 = b1 and a2 < b2) ... .
 *   Input cases are such that the length of the answer does not exceed 100000.
 *
 * Example: R = 11, S = [6,8,5,4,7]
 *   ans = [0,2]
 *   Here, [2,3], [2,2] or [3,3] also give the maximum no. kicks.
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class TusharBirthdayBombs {
public:
vector<int> bday(vector<int>& p, int R) {
	int n = (int)p.size();
	vector<int> dp(R + 1, -1);
	vector<int> back(R + 1);

	back[0] = 0;
	for (int r = 0; r <= R; r++) {
		for (int i = 0; i < n; i++) {
			if (r >= p[i] && dp[r] < dp[r - p[i]] + 1) {
				dp[r] = dp[r - p[i]] + 1;
				back[r] = i;
			}
		}
	}

	vector<int> ans;
	int r = R;
	while ( r >= 0 && (r - p[back[r]]) >= 0) {
		ans.push_back(back[r]);
		r = r - p[back[r]];
	}
	return ans;
}
};

#if 0
int main(void) {
	//vector<int> p = { 6,8,5,4,7 };
	//int R = 11;
	
	//vector<int> p = { 11246, 8244, 23802, 15827, 19437, 16152, 6020, 10937, 14538, 18785, 22413, 14452, 5772, 14622, 4547, 18615, 11849, 2913, 19827, 10137, 20697, 22378, 17926, 19926, 17005, 18333, 12752, 9479, 12192 };
	//int R = 4395;

	vector<int> p = { 8, 8, 6, 5, 4 };
	int R = 12;

	TusharBirthdayBombs t;
	vector<int> ans = t.bday(p, R);
	for (int x : ans) cout << x << " ";

	return 0;
}
#endif