/**
 * A message containing letters from A-Z is being encoded to numbers using the following mapping:
 *	'A' -> 1
 *	'B' -> 2
 *		...
 *	Z' -> 26
 * Given an encoded message containing digits, determine the total number of ways to decode it.
 * 
 * Example :
 *	Given encoded message "12", it could be decoded as "AB" (1 2) or "L" (12).
 *	The number of ways decoding "12" is 2.
 */

#include <cstring> 
#include <iostream>
#include <string>
using namespace std;

class WaysToDecode {
public:
	int numDecodings(string s);
};

int WaysToDecode::numDecodings(string s) {
	if (s.empty()) return 0;
	if (s[0] == '0') return 0;

	int n = s.length();
	if (n == 1) return 1;

	int* dp = new int[n + 1];
	memset(dp, 0, sizeof(int) * (n + 1));

	int ans = 0;
	dp[0] = 1; 	dp[1] = 1;
	if ((s[1] != '0') && (s[0] == '1' || (s[0] == '2' && s[1] <= '6')))
		dp[1] = 2;
	else if (s[0] > '2' && s[1] == '0') goto out;

	for (int i = 2; i < n; i++) {
		int len1 = 0;
		if (s[i] != '0') len1 = dp[i - 1];

		int len2 = 0;
		if (s[i - 1] == '1' || (s[i - 1] == '2' && s[i] <= '6'))
			len2 = dp[i - 2];

		if (len1 == 0 && len2 == 0)
			goto out;

		dp[i] = len1 + len2;
	}

	ans = dp[n - 1];
out:
	delete[]dp;
	dp = nullptr;
	return ans;
}

/*
int main(void) {
	WaysToDecode t;
	string s = "1290";

	cout << t.numDecodings(s) << endl;

	return 0;
}
*/