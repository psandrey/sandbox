
/*
 * Given a 3Xn board, find the number of ways to color it using at most 4 colors such that no two adjacent boxes
 * have same color. Diagonal neighbors are not treated as adjacent boxes.
 * Output the ways%1000000007 as the answer grows quickly.
 *
 * 1<= n < 100000
 *
 * Example:
 * Input: n = 1
 * Output: 36
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class WaysToColorA3xNBoard {
public:
	int ways(int n) {
		if (n == 0) return 0;
		if (n == 1) return 36;

		unsigned long long f2 = 12ULL;
		unsigned long long f3 = 24ULL;
		unsigned long long ans = 0;
		unsigned long long m = 1000000007ULL;
		for (int i = 2; i <= n; i++) {
			unsigned long long f2_prev = f2, f3_prev = f3;
			f2 = ((f2_prev * 7) % m + (f3_prev * 5) % m) % m;
			f3 = ((f2_prev * 10) % m + (f3_prev * 11)) % m;
		}

		ans = (f2 + f3) % m;

		return (int)ans;
	}
};

#if 0
int main(void) {

	WaysToColorA3xNBoard t;
	cout << t.ways(500);

	return 0;
}
#endif