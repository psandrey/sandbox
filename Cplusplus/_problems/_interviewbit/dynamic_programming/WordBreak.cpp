
/*
 * Given a string s and a dictionary of words dict, determine if s can be segmented
 *  into a space-separated sequence of one or more dictionary words.
 *
 * Note: This C++ version gives TLE, but Java version passes (same idea).
 * Note: The problem assume you have a very big dictionary, thus they require 
 *        to build a hashmap with all the words in dictionary and lookup only 
 *        for the words between 0..i => T(n) = O(n^2); S(n) = O(n)
 */

#include <unordered_set>
#include <algorithm>
#include <stack>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class WordBreak {
	public:
		// T(n) = O(n^2), S(n) = O(n^2) - memoization
		int wordBreak_v1_memo(string a, vector<string>& d) {
			if (a.empty() || d.empty()) return 0;

			int n = (int)a.size();
			unordered_set<string> hs;
			for (int j = 0; j < (int)d.size(); j++) hs.insert(d[j]);

			vector<vector<int>> memo(n, vector<int>(n, -1));
			return wb_v1_memo(a, 0, n - 1, hs, memo);
		}

		int wb_v1_memo(string& a, int i, int j, unordered_set<string>& hs, vector<vector<int>>& memo) {
			if (j < i) return 0;
			if (memo[i][j] != -1) return memo[i][j];

			if (hs.find(a.substr(i, j - i + 1)) != hs.end()) memo[i][j] = 1;
			else {
				memo[i][j] = 0;
				for (int k = i; k < j; k++) {
					int l = wb_v1_memo(a, i    , k, hs, memo); // [i,k]
					int r = wb_v1_memo(a, k + 1, j, hs, memo); // [k,j]
					if (l == 1 && r == 1) { memo[i][j] = 1; break; }
				}
			}
			return memo[i][j];
		}

		// T(n) = O(n^2), S(n) = O(n^2) - tabulation
		int wordBreak_v1_tab(string a, vector<string>& d) {
			if (a.empty() || d.empty()) return 0;

			int n = (int)a.size();
			unordered_set<string> hs;
			for (int j = 0; j < (int)d.size(); j++) hs.insert(d[j]);

			vector<vector<int>> dp(n + 1, vector<int>(n, 0));
			for (int j = 0; j < n; j++)dp[0][j] = 1; // all of size 0

			for (int sz = 1; sz <= n; sz++) {
				for (int i = 0; i <= n - sz; i++) {
					if (hs.find(a.substr(i, sz)) != hs.end()) dp[sz][i] = 1;
					else {
						dp[sz][i] = 0;
						for (int k = 1; k < sz; k++)
							if (dp[k][i] == 1 && dp[sz - k][i + k] == 1) { dp[sz][i] = 1; break; }
					}
				}
			}

			return dp[n][0];
		}
#if 0
		// T(n) = O(n^2), S(n) = O(n^2)
		int wordBreak_v1_tab(string a, vector<string>& d) {
			if (a.empty() || d.empty()) return 0;

			int n = (int)a.size();
			int m = (int)d.size();

			unordered_set<string> hs;
			for (int j = 0; j < m; j++) hs.insert(d[j]);

			vector<vector<bool>> dp(n, vector<bool>(n));
			for (int sz = 1; sz <= n; sz++) {
				for (int i = 0; i < n; i++) {
					if (sz > n - i) break;
					int j = i + sz - 1;

					if (hs.find(a.substr(i, sz)) != hs.end()) dp[i][j] = true;
					else {
						bool aij = false;
						for (int k = i; k < j; k++) {
							aij = (dp[i][k] && dp[k + 1][j]);
							if (aij == true) break;
						}

						dp[i][j] = aij;
					}
				}
			}

			return dp[0][n - 1];
		}
#endif

		// T(n) = O(n^2), S(n) = O(n) - tabulation
		int wordBreak_v2_tab(string a, vector<string>& d) {
			if (a.empty() || d.empty()) return 0;

			int n = (int)a.size(), m = (int)d.size();

			unordered_set<string> hs;
			for (int j = 0; j < m; j++) hs.insert(d[j]);

			vector<bool> dp(n + 1);
			dp[0] = true;

			for (int k = 1; k <= n; k++) {
				dp[k] = false;

				string word = "";
				for (int j = k; j >= 1; j--) {
					
					word.insert(word.begin(), a[j - 1]);
					if (dp[j - 1] == true && hs.find(word) != hs.end()) {
						dp[k] = true;
						break;
					}
				}
			}

			return dp[n];
		}
};

#if 0
int main(void) {
	//string a = "abcx";
	//vector<string> d = { "a", "abc", "bcx" };
	
	//string a = "aaaacxabcaacxaabc";
	//vector<string> d = { "aa", "a", "abc", "cx" };

	string a = "aaaabaababaaaabaabbabbbbbabaabbbbabbbabaabbabaaaaaabaabbabbbaabaababaabbaaabaababbaabbbaabaaaaabbbbaaaaabaababbbababbabbaabbbbabababaababaaaababbbaaaaaaabbbbaabbbbabbbabbbaaabbaaaaabbbabaaaabbababbbbaababaabaababbbbababbbaaaabbbbaabbbaaaabaababbbaaaaaabbbabbaaabaabaabaaaababbbabbbabbbaabbabaaabaaabbababaabbabaaaabbbbbbabbababaaabbababbabbaaaabbabbbababbbbaabaaabbbaababababaaaaaaaabababaabbabaaabbabaaaaaabbbbbbabaaabbaaaaaaaabbbbabbaaabaabbabbbbbbbbbbbbbbabbbababbbbaabaaabaababbaaabbbbaaabbbbbaabababbaabbabbaaabaababbbbbaaaaabbbabaabaaaabaaaaababbabbababbbbbbaaababbbbbbbabbaabbabaaabbbaabbabaaaabaababb";
	vector<string> d = { "aabababaa", "aaaabaa", "ababaabaa", "aaaa", "b", "aaaaba", "a", "aaba", "bbaaaaaab", "bbb", "aabbaaaaba", "baa", "aabbaba", "abbabb", "bbaaab", "bbbbabbaab", "abbaabbb", "babaa", "b", "bbaaa", "bab", "abaaaaaa", "bbbba", "baababab", "abbaa", "bbaaaaa", "aaaabbbbba" };
	// 1

	WordBreak t;
	cout << "DP v1(memo) : " << t.wordBreak_v1_memo(a, d) << endl;
	cout << "DP v1(tab)  : " << t.wordBreak_v1_tab(a, d) << endl;
	cout << "DP v2(tab)  : " << t.wordBreak_v2_tab(a, d) << endl;

	return 0;
}
#endif