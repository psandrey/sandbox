
/*
 * Given a matrix M of size nxm and an integer K, find the maximum element in the K manhattan distance neighbourhood
 * for all elements in nxm matrix.
 * In other words, for every element M[i][j] find the maximum element M[p][q] such that abs(i-p)+abs(j-q) <= K.
 *
 * Note: Expected time complexity is O(N*N*K)
 *
 * Constraints:
 * 1 <= n <= 300
 * 1 <= m <= 300
 * 1 <= K <= 300
 * 0 <= M[i][j] <= 1000
 *
 * Example:
 * Input: M  = [[1,2,4],[4,5,8]] , K = 2
 * Output: ans = [[5,8,8],[8,8,8]]
 */

#include <algorithm>
#include <queue>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class KthManhattanDistanceNeighbourhood {
public:
	vector<vector<int>> distance(int K, vector<vector<int>>& a) {
		int n = (int)a.size();
		if (n == 0) return vector<vector<int>>();
		int m = (int)a[0].size();
		if (m == 0) return vector<vector<int>>();

		vector<vector<vector<int>>> dp(2, vector<vector<int>>(n, vector<int>(m)));
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++) dp[0][i][j] = a[i][j];

		int toggle = 1;
		for (int k = 1; k <= K; k++) {
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {
					dp[toggle][i][j] = dp[toggle ^ 1][i][j];

					if (i > 0)     dp[toggle][i][j] = max(dp[toggle][i][j], dp[toggle ^ 1][i - 1][j]);
					if (i < n - 1) dp[toggle][i][j] = max(dp[toggle][i][j], dp[toggle ^ 1][i + 1][j]);
					if (j > 0)     dp[toggle][i][j] = max(dp[toggle][i][j], dp[toggle ^ 1][i][j - 1]);
					if (j < m - 1) dp[toggle][i][j] = max(dp[toggle][i][j], dp[toggle ^ 1][i][j + 1]);
				}
			}
			toggle ^= 1;
		}

		return dp[toggle^1];
	}
};

#if 0
int main(void) {
	vector<vector<int>> a = { {1, 2, 4},{4, 5, 8} };
	
	KthManhattanDistanceNeighbourhood t;
	vector<vector<int>> ans = t.distance(2, a);

	for (int i = 0; i < (int)ans.size(); i++) {
		for (int j = 0; j < (int)ans[0].size(); j++) {
			cout << ans[i][j] << " ";
		}
		cout << endl;
	}
	
	return 0;
}
#endif