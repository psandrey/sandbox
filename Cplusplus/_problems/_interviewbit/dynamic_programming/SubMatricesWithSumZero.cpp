
/*
 * Given a 2D matrix, find the number non-empty sub matrices, such that the sum of the elements
 *  inside the sub matrix is equal to 0. (note: elements might be negative).
 */

#include <algorithm>
#include <stack>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class SubMatricesWithSumZero {
public:
	int solve_dp1(vector<vector<int> >& a) {
		if (a.empty()) return 0;
		int n = (int)a.size();
		int m = (int)a[0].size();

		vector<vector<vector<vector<int>>>> dp (n, vector<vector<vector<int>>>(m, 
												vector<vector<int>>(n, vector<int>(m))));
		int count = 0;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				for (int k = i; k < n; k++)
					for (int l = j; l < m; l++) {
						if (i == k && j == l) dp[i][j][k][l] = a[i][j];
						else {
							if (i == k) dp[i][j][k][l] = dp[i][j][k][l - 1] + a[k][l];
							else if (j == l) dp[i][j][k][l] = dp[i][j][k - 1][l] + a[k][l];
							else dp[i][j][k][l] = dp[i][j][k][l - 1] + dp[i][j][k - 1][l] - dp[i][j][k - 1][l - 1]
													+ a[k][l];

							if (dp[i][j][k][l] == 0) {
								cout << i << " " << j << " " << k << " " << l << endl;
								count++;
							}
						}
					}

		return count;
	}
};

#if 0
int main(void) {
	vector<vector<int>> a ={{ 1,  0,  1, 1 },
							{ 1, -8,  8, 1 },
							{ 1,  5, -3, 1 },
							{ 1,  3, -5, 1 }};

	SubMatricesWithSumZero t;
	cout << "DP v1: " << t.solve_dp1(a) << endl;

	return 0;
}
#endif