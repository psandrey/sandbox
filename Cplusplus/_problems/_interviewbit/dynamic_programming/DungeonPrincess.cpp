
/*
 * The demons had captured the princess (P) and imprisoned her in the bottom-right corner of a dungeon.
 * The dungeon consists of M x N rooms laid out in a 2D grid. Our valiant knight (K) was initially positioned
 * in the top-left room and must fight his way through the dungeon to rescue the princess.
 *
 * The knight has an initial health point represented by a positive integer. If at any point his health point drops to 0
 * or below, he dies immediately. Some of the rooms are guarded by demons, so the knight loses health (negative integers)
 * upon entering these rooms; other rooms are either empty (0�s) or contain magic orbs that increase the knight�s health
 * (positive integers).
 *
 * In order to reach the princess as quickly as possible, the knight decides to move only rightward or downward in each step.
 * Write a function to determine the knight�s minimum initial health so that he is able to rescue the princess.
 * For example, given the dungeon below, the initial health of the knight must be at least 7 if he follows the optimal path
 *
 * RIGHT-> RIGHT -> DOWN -> DOWN.
 *
 * Dungeon Princess: Example 1
 *
 * Input arguments to function:
 * Your function will get an M*N matrix (2-D array) as input which represents the 2D grid as described in the question.
 * Your function should return an integer corresponding to the knight�s minimum initial health required.
 *
 * Note:
 *   The knight�s health has no upper bound.
 *   Any room can contain threats or power-ups, even the first room the knight enters and the bottom-right room where
 *   the princess is imprisoned.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class DangeonPrincess {
public:
	int min_health(vector<vector<int>>& a) {
		int n = (int)a.size();
		if (n == 0) return 0;
		int m = (int)a[0].size();
		if (m == 0) return 0;

		vector<vector<int>> dp(n, vector<int>(m));
		dp[n - 1][m - 1] = max(1 - a[n - 1][m - 1], 1);
		for (int j = m-2; j >= 0; j--) dp[n - 1][j] = max(1, dp[n - 1][j + 1] - a[n - 1][j]);
		for (int i = n-2; i >= 0; i--) dp[i][m - 1] = max(1, dp[i + 1][m - 1] - a[i][m - 1]);

		for (int i = n - 2; i >= 0; i--) {
			for (int j = m - 2; j >= 0; j--) {
				int down  = max(1, dp[i + 1][j] - a[i][j]);
				int right = max(1, dp[i][j + 1] - a[i][j]);
				
				// take the best neighbour
				dp[i][j] = min(down, right);
			}
		}

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				cout << dp[i][j] << " ";
			}
			cout << endl;
		}

		return dp[0][0];
	}

	vector<vector<int>> test_matrix() {
		vector<int> b = {
			-15, -59, -4, -94, -18, -67, -93, -92, -50, -7, -95, 0, -62, -1, 2, -2, -96,
			-79, -11, -2, 3, 1, -77, -55, -18, -16, -72, -17, -24, -20, -29, -52, 1, -29,
			-100, -64, -76, -87, -96, -43, -43, -10, -5, 5, -37, -88, -67, -57, -50, -87,
			-14, -85, -78, -7, -70, -44, 4, 9, -91, -99, -12, -13, -83, -93, -43, -6, -23,
			-54, -80, -80, -11, -2, -35, -78, -12, -61, -74, -27, -36, -84, -27, -21, -80,
			-36, -58, -72, -53, -88, -28, -23, -35, -68, -14, -55, -21, -11, -41, -67, -17,
			-30, -72, 9, -41, -89, -87, -31, -66, -30, -78, -14, -94, -69, 5, -33, -26, -26,
			-20, -53, 9, 3, -31, -88, -57, -31, -88, -89, -79, -2, -23, -84, 1, -74, -29,
			-30, -17, -98, 1, -7, 7, -53, -22, -99, -60, -63, -75, -80, -44, -25, -82, -2,
			7, -14, -84, -79, -28, -10, -93, -42, 3, -66, -68, -66, -68, -19, -14, -74, 5,
			-42, -65, -73, -68, -38, -61, -76, -32, -8, 3, 4, -71, -14, -99, -24, -92, -16,
			-34, -87, -70, -12, -25, -75, -80, -24, -59, -70, -90, -19, -9, -21, -81, -26,
			-61, -63, -48, -40, -63, -60, -74, -64, -50, -70, -75, -13, -4, -8, -80, -4, 2,
			-72, -14, -8, -18, -45, -93, -44, -25, 0, -99, -65, -25, -58, -30, -93, -64, 4,
			-35, -8, -34, -69, -3, -28, -78, -43, 8, -37, 1, -51, -100, -24, -22, -76, -13,
			-18, -1, -29, -37, -46, -52, -26, -18, -51, -73, -49, 1, -76, -73, -58, -4, -24,
			-56, -74, -74, -30, -61, 0, -99, -16, -47, -7, -78, -49, -9, -24, -69, -29, -28,
			-100, -11, -40, -80, -82, -57, -80, -10, -84, -42, -36, -81, 1, -16, -49, -19, -76,
			-9, -83, -54, -87, -8, -5, -88, -33, -66, -76, -6, -42, -96, -15, -9, -10, -67,
			-72, -99, 10, -20, -23, -44, -36, -100, -48, -57, -8, -26, -41, -18, -47, -80, -49,
			2, -33, -59, -35, -86, -87, -14, -34, -71, -15, -36, -33, -7, -63, -4, 7, -19, -63,
			-33, -53, -36, -45, -50, 0, -27, -41, -51, -2, -10, -52, -60, -78, -89, 2, -53, 4,
			-66, -8, -77, -63, -65, -67, -67, -67, -8, -24, -63, -89, -61, -69, -89, -68, -100,
			-90, -40, -98, -21, -81, -4, -12, 0, -92, -3, -88 };
		vector<vector<int>> a(20, vector<int>(20));
		int k = 0;
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 20; j++) {
				a[i][j] = b[k];
				k++;
			}
		}
		return a;
	}
};

#if 0
int main(void) {

	vector<vector<int>> a =
	//{ {-2, -3, 3},
	//{-5, -10, 1},
	//{10, 30, -5} };
	
	{ {  1,   1, -3},
	  {100, 100,  2},
	  {100, 100,  0} };


	DangeonPrincess t;
	vector<vector<int>> b = t.test_matrix();
	cout << t.min_health(a) << endl;
	
	return 0;
}
#endif