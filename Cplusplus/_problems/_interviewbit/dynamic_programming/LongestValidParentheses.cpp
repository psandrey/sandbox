
/*
 * Given a string containing just the characters '(' and ')', find the length of the longest valid
 * (well-formed) parentheses substring.
 *
 * For "(()", the longest valid parentheses substring is "()", which has length = 2.
 * Another example is ")()())", where the longest valid parentheses substring is "()()",
 * which has length = 4.
 */

#include <algorithm>
#include <stack>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class LongestValidParentheses {
public:
	int longestValidParentheses(string a) {
		if (a.empty()) return 0;

		int ans = 0;
		stack<int> st;
		for (int j = 0; j < (int)a.size(); j++) {
			if (a[j] == ')') {
				if (!st.empty() && a[st.top()] == '(') {
					st.pop();

					int k = (st.empty() ? -1 : st.top());
					ans = max(ans, j - k);
				} else st.push(j);
			} else st.push(j);
		}

		return ans;
	}
};

#if 0
int main(void) {
	//string s = "))()())(())())";
	string s = ")()))(())((())))))())()(((((())())((()())(())((((())))())((()()))(()(((()()(()((()()))(())()))(((";

	LongestValidParentheses t;
	cout << t.longestValidParentheses(s) << endl;

	return 0;
}
#endif