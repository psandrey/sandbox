
/*
 * Find out the number of N digit numbers, whose digits on being added equals to a given number S.
 * Note that a valid number starts from digits 1-9 except the number 0 itself. i.e. leading zeroes are not allowed.
 *
 * Since the answer can be large, output answer modulo 1000000007.
 *
 * Example: N = 2, S = 4
 * Valid numbers are {22, 31, 13, 40}
 * Hence output 4.
 */

#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class NDigitNumbersWithDigitSumS {
public:
	int solve(int n, int S) {
		int ans = 0;

		vector<vector<unsigned long long int>> dp(2 , vector<unsigned long long int>(S));
		for (int i = 0; i < S; i++) {
			if (i + 1 <= 9) dp[0][i] = 1;
			else dp[0][i] = 0;
		}
		dp[1][0] = 1;

		int toggle = 1;
		for (int len = 1; len < n; len++) {
			for (int s = 1; s < S; s++) {
				dp[toggle][s] = 0;

				// offset with the number of digits
				for (int k = (s >= 9 ? k = s - 9 : 0); k <= s; k++)
					dp[toggle][s] = (dp[toggle][s] + dp[toggle ^ 1][k]) % 1000000007;
			}
			toggle ^= 1;
		}

		toggle ^= 1;
		return (int)(dp[toggle][S - 1]);
	}
};

#if 0
int main(void) {
	int n = 75;
	int S = 22;

	NDigitNumbersWithDigitSumS t;
	cout << t.solve(n, S);

	return 0;
}
#endif