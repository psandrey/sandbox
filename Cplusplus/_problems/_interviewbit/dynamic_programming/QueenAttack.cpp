
/*
 * On a N * M chessboard, where rows are numbered from 1 to N and columns from 1 to M, there are queens at some cells.
 * Return a N * M array A, where A[i][j] is number of queens that can attack cell (i, j).
 * While calculating answer for cell (i, j), assume there is no queen at that cell.
 *
 * Notes:
 * Queen is able to move any number of squares vertically, horizontally or diagonally on a chessboard.
 * A queen cannot jump over another queen to attack a position.
 * You are given an array of N strings, each of size M. Each character is either a 1 or 0 denoting
 * if there is a queen at that position or not, respectively.
 * Expected time complexity is worst case O(N*M).
 * 
 * For example, let chessboard be,
 * [0 1 0]
 * [1 0 0]
 * [0 0 1]
 *
 * where a 1 denotes a queen at that position.
 *
 * Cell (1, 1) is attacked by queens at (2, 1), (1,2) and (3,3).
 * Cell (1, 2) is attacked by queen at (2, 1). Note that while calculating this, we assume that there is no queen at (1, 2).
 * Cell (1, 3) is attacked by queens at (3, 3) and (1, 2).
 * and so on...
 *
 * Finally, we return matrix
 * [3, 1, 2]
 * [1, 3, 3]
 * [2, 3, 0]
 *
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class QueenAttack {
public:
	vector<vector<int>> queen_attack(vector<string>& a) {
		vector<vector<int>> dir = { {-1, -1}, {-1, 0}, {-1, 1}, {0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1} };
		int n = (int)a.size();
		if (n == 0) return vector<vector<int>>();
		int m = (int)a[0].size();
		if (m == 0) return vector<vector<int>>();

		vector<vector<int>> dp(n, vector<int>(m));
		for (int i = 0; i < n; i++) memset(&dp[i][0], 0, sizeof(int) * m);

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if (a[i][j] == '0') continue;

				for (int k = 0; k < (int)dir.size(); k++) {
					int ti = i + dir[k][0];
					int tj = j + dir[k][1];
					while (ti >= 0 && ti < n && tj >= 0 && tj < m) {
						dp[ti][tj]++;
						if (a[ti][tj] == '1') break;
						ti += dir[k][0]; tj += dir[k][1];						
					}
				}
				/*
				for (vector<int> a : dp) {
					for (int x : a) cout << x << " ";
					cout << endl;
				}
				cout << " ending: " << i << " " << j << endl << endl;
				*/
			}
		}

		return dp;
	}
};

#if 0
int main(void) {
	vector<string> a = { "111","111","111" };
	
	QueenAttack t;
	vector<vector<int>> ans = t.queen_attack(a);
	for (vector<int> a : ans) {
		for (int x: a) cout << x << " ";
		cout << endl;
	}

	return 0;
}
#endif