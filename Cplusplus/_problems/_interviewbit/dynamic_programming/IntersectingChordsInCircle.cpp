/* 
 * Given a number N, return number of ways you can draw N chords in a circle with 2*N points such
 * that no 2 chords intersect.
 * Two ways are different if there exists a chord which is present in one way and not in other.
 */

#include <cstring> 
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

class IntersectingChordsInCircle {
public:
	// brute-force version
	int chord_cnt_bf(int n);

	// DP version with memoization
	int chord_cnt_memo(int n);

	// DP version with tabulation
	int chord_cnt_dp(int n);
private:
	int chord_cnt_memo_helper(int n_points, int memo[]);
};

int IntersectingChordsInCircle::chord_cnt_bf(int n_points) {
	if (n_points == 0) return 1;

	int no_chords = n_points / 2;
	int ways = 0;
	for (int i = 0; i < no_chords; i++)
		ways += chord_cnt_bf(2 * i) * chord_cnt_bf(2 * (no_chords - 1 - i));

	return ways;
}

int IntersectingChordsInCircle::chord_cnt_memo(int n_points) {
	int no_chords = n_points / 2;
	int* memo = new int[no_chords + 1];
	memset(memo, 0, sizeof(int) * (no_chords + 1));

	chord_cnt_memo_helper(n_points, memo);
	int ans = memo[no_chords];
	delete[]memo;
	memo = nullptr;
	return ans;
}

int IntersectingChordsInCircle::chord_cnt_memo_helper(int n_points, int memo[]) {
	if (n_points == 0) return 1;
	int no_chords = n_points / 2;
	if (memo[no_chords] != 0) return memo[no_chords];

	int ways = 0;
	for (int i = 0; i < no_chords; i++)
		ways += chord_cnt_bf(2 * i) * chord_cnt_bf(2 * (no_chords - 1 - i));

	memo[no_chords] = ways;
	return ways;
}

int IntersectingChordsInCircle::chord_cnt_dp(int n) {
	if (n == 0 || n == 1) return 1;
	unsigned long long* dp = new unsigned long long[n + 1];
	memset(dp, 0, sizeof(unsigned long long) * (n + 1));

	dp[0] = 1ULL;
	dp[1] = 1ULL;
	//unsigned long long mod = 1000000007ULL;
	for (int i = 2; i <= n; i++)
		for (int j = 0; j < i; j++)
			//dp[i] = (dp[i]+(dp[j] * dp[i - j - 1])%mod)%mod;
			dp[i] = dp[i] + dp[j] * dp[i - j - 1];

	int ans = (int)(dp[n]);
	delete[]dp;
	dp = nullptr;
	return ans;
}

/*
int main(void) {
	IntersectingChordsInCircle t;
	int n = 4;

	cout << "BF:" << t.chord_cnt_bf(2*n) << endl;
	cout << "DP MEMO:" << t.chord_cnt_memo(2 * n) << endl;
	cout << "DP TAB:" << t.chord_cnt_dp(n) << endl;
	return 0;
}
*/