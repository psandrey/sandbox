
/*
 * Given two words A and B, find the minimum number of steps required to convert A to B.
 * (each operation is counted as 1 step.)
 *
 * You have the following 3 operations permitted on a word:
 *
 * Insert a character
 * Delete a character
 * Replace a character
 *
 * Example : edit distance between "Anshuman" and "Antihuman" is 2.
 *
 * Operation 1: Replace s with t.
 * Operation 2: Insert i.
 */

#include <algorithm>
#include <string>
#include <vector>
#include <stdarg.h>
#include <iostream>
using namespace std;

class EditDistance {
public:
	int edit(string a, string b) {
		int n = (int)a.size();
		int m = (int)b.size();
		if (n == 0) return m;
		if (m == 0) return n;
		int ans = 0;
#if 0
		// brute-force
		ans = edit_dist_bf(a, 0, b, 0);
#else
		// memoization
		//vector<vector<int>> memo(n);
		//for (int i = 0; i < n; i++) memo[i] = vector<int>(m, -1);
		//ans = edit_dist_memo(a, 0, b, 0, memo);
		//ans = edit_dist_memo2(a, a.size(), b, b.size(), memo);

		// tabulation
		ans = edit_dist_tab(a, b);
#endif
		return ans;
	}

private:
	int edit_dist_bf(string a, int i, string b, int j) {
		if (i == a.size()) return (b.size() - j);
		if (j == b.size()) return (a.size() - i);

		int ans = 0;
		if (a[i] != b[j]) {
			int del = edit_dist_bf(a, i    , b, j + 1);
			int ins = edit_dist_bf(a, i + 1, b, j);
			int sbt = edit_dist_bf(a, i + 1, b, j + 1);
			ans = min(min(del, ins), sbt) + 1;
		} else { ans = edit_dist_bf(a, i + 1, b, j + 1); }

		return ans;
	}

	int edit_dist_memo(string a, int i, string b, int j, vector<vector<int>>& memo) {
		if (i == a.size()) return (b.size() - j);
		if (j == b.size()) return (a.size() - i);
		if (memo[i][j] != -1)
			return memo[i][j];
		
		int ans = 0;
		if (a[i] != b[j]) {
			int del = edit_dist_memo(a, i    , b, j + 1, memo);
			int ins = edit_dist_memo(a, i + 1, b, j    , memo);
			int sbt = edit_dist_memo(a, i + 1, b, j + 1, memo);
			ans = min(min(del, ins), sbt) + 1;
		} else { ans = edit_dist_memo(a, i + 1, b, j + 1, memo); }

		memo[i][j] = ans;
		return memo[i][j];
	}

	int edit_dist_memo2(string a, int i, string b, int j, vector<vector<int>>& memo) {
		if (i == 0) return j;
		if (j == 0) return i;
		if (memo[i-1][j-1] != -1) return memo[i-1][j-1];

		int ans = 0;
		if (a[i-1] != b[j-1]) {
			int del = edit_dist_memo2(a, i, b, j - 1, memo);
			int ins = edit_dist_memo2(a, i - 1, b, j, memo);
			int sbt = edit_dist_memo2(a, i - 1, b, j - 1, memo);
			ans = min(min(del, ins), sbt) + 1;
		} else { ans = edit_dist_memo2(a, i - 1, b, j - 1, memo); }

		memo[i-1][j-1] = ans;
		return memo[i-1][j-1];
	}

	int minv(int argc, ...) {
		va_list argv;
		va_start(argv, argc);
		int ans = INT_MAX;
		for (int i = 0; i < argc; i++) {
			int x = va_arg(argv, int);
			ans = min(ans, x);
		}
		va_end(argv);

		return ans;
	}

	int edit_dist_tab(string a, string b) {
		int n = (int)a.size();
		int m = (int)b.size();
		vector<vector<int>> dp(n+1);
		for (int i = 0; i <= n; i++) dp[i] = vector<int>(m+1);
		for (int i = 0; i <= n; i++) dp[i][0] = i;
		for (int i = 0; i <= m; i++) dp[0][i] = i;

		for (int i = 1; i <= n; i++) 
			for (int j = 1; j <= m; j++) {
				if (a[i - 1] == b[j - 1]) dp[i][j] = dp[i - 1][j - 1];
				else {
					int min_op = minv(3, dp[i][j - 1], dp[i - 1][j], dp[i - 1][j - 1]);
					dp[i][j] = 1 + min_op;
				}
			}

		return dp[n][m];
	}
};

#if 0
int main(void) {
	string s1 = "aaa";
	string s2 = "aa";

	EditDistance t;
	cout << t.edit(s1, s2);

	return 0;
}
#endif