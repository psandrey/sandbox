
/*
 * Given an array of positive elements, you have to flip the sign of some of its elements such that
 * the resultant sum of the elements of array should be minimum non-negative(as close to zero as possible).
 * Return the minimum no. of elements whose sign needs to be flipped such that the resultant sum is minimum non-negative.
 *
 * Constraints:
 *   1 <= n <= 100
 *   Sum of all the elements will not exceed 10,000.
 *
 * Example:
 *   A = [15, 10, 6]
 *   ans = 1 (Here, we will flip the sign of 15 and the resultant sum will be 1 )
 *
 *   A = [14, 10, 4]
 *   ans = 1 (Here, we will flip the sign of 14 and the resultant sum will be 0)
 *
 * Note that flipping the sign of 10 and 4 also gives the resultant sum 0 but flippings there are not minimu
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class FlipArray {
public:
	int solve(vector<int> a) {
		int n = (int)a.size();
		if (n == 0) return 0;

		int sum = 0;
		for (int x : a) sum += x;
		int S = sum / 2;

		int LIMIT = 10001;
		vector<vector<int>> dp(S + 1, vector<int>(n + 1, LIMIT));

		// for sum = 0 we cannot flip anything
		for (int i = 0; i <= n; i++) dp[0][i] = 0;

		for (int s = 1; s <= S; s++) {
			for (int j = 1; j <= n; j++) {
				dp[s][j] = dp[s][j - 1];

				// can we flip for current sum and not going below 0 ? 
				if (s >= a[j - 1])
					dp[s][j] = min(dp[s][j - 1], dp[s - a[j - 1]][j - 1] + 1);
			}
		}

		/*
		for (int s = 0; s <= S; s++) {
			for (int j = 0; j <= n; j++) {
				cout << dp[s][j] << " ";
			}
			cout << endl;
		}
		*/

		// for the first sum that holds the constraint
		for (int s = S; s >= 0; s--)
			if (dp[s][n] < LIMIT) return dp[s][n];

		return 0;
	}
};

#if 0
int main(void) {
	//vector<int> a = { 18, 18, 20, 4, 3, 4, 22, 17, 9, 10, 19, 9, 19, 19, 17, 4, 17, 21, 17, 22, 11, 19, 1, 21, 11, 21, 22, 15, 20, 22, 22, 13, 15, 19, 14, 15, 1, 14, 9, 7, 1, 4, 16, 19 };
	//vector<int> a = {0, 2, 3};
	vector<int> a = { 3, 3, 7, 10, 2, 1, 5, 3, 8, 5, 1, 4, 3, 9, 1, 4, 8, 1, 1, 4, 5, 10, 3, 8, 5, 3, 6, 3, 5, 5, 4, 9, 7, 1, 9, 10, 3, 3, 4, 2, 9, 4, 5, 3, 3, 5, 6, 2, 8, 6, 8, 2, 7, 10, 9, 2, 4, 4, 4, 8, 10, 9, 7, 8, 1, 5, 9, 5, 9, 2, 7, 9, 6, 3, 2, 10, 10, 7, 1, 7, 5, 10, 10, 1, 9, 10, 4, 2, 5, 9, 10 };
	
	FlipArray t;
	cout << t.solve(a) << endl;

	return 0;
}
#endif