﻿
/*
 * Implement wildcard pattern matching with support for '?' and '*'.
 *
 * '?' : Matches any single character.
 * '*' : Matches any sequence of characters (including the empty sequence).
 * The matching should cover the entire input string (not partial).
 *
 * The function prototype should be: int isMatch(const char *s, const char *p)
 *
 * Examples :
 *
 * isMatch("aa","a") -> 0
 * isMatch("aa","aa") -> 1
 * isMatch("aaa","aa") -> 0
 * isMatch("aa", "*") -> 1
 * isMatch("aa", "a*") -> 1
 * isMatch("ab", "?*") -> 1
 * isMatch("aab", "c*a*b") -> 0
 * Return 1/0 for this problem.
 *
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class RegularExpressionMatch {
public:

	// brute force version
	int isMatch(const string a, const string b) {
		int n = (int)a.size();
		int m = (int)b.size();

		vector<vector<bool>> dp(n + 1, vector<bool>(m + 1));
		dp[0][0] = true;
		for (int j = 1; j <= m; j++) if (b[j - 1] == '*') dp[0][j] = dp[0][j - 1];
		
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				if (b[j - 1] == '*') dp[i][j] = dp[i - 1][j] || dp[i][j - 1];
				else if (b[j - 1] == '?') dp[i][j] = dp[i - 1][j - 1];
				else dp[i][j] = dp[i - 1][j - 1] && (a[i - 1] == b[j - 1]);
			}
		}
#if 0
		for (int j = 0; j <= m; j++) {
			for (int i = 0; i <= n; i++) {
				cout << dp[i][j] << " ";
			}
			cout << endl;
		}
#endif

		return dp[n][m];
	}
};

#if 0
int main(void) {
	//string a = "cac";
	//string b = "??a?";
	string a = "aaaaa";
	string b = "*";
	//string a = "bcaccbabaa";
	//string b = "bb*c?c*?";

	RegularExpressionMatch t;
	cout << t.isMatch(a, b) << endl;

	return 0;
}
#endif