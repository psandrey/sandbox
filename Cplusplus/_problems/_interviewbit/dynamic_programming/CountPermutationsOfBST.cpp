
/*
 * You are given two positive integers A and B. For all permutations of [1, 2, �, A], we create a BST.
 * Count how many of these have height B.
 *
 * Notes:
 *
 * Values of a permutation are sequentially inserted into the BST by general rules i.e in increasing order of indices.
 * Height of BST is maximum number of edges between root and a leaf.
 * Return answer modulo 109 + 7.
 * Expected time complexity is worst case O(N4).
  * 
 * 1 <= N <= 50
 * For example,
 *
 * A = 3, B = 1
 *
 * Two permutations [2, 1, 3] and [2, 3, 1] generate a BST of height 1.
 * In both cases the BST formed is
 *
 *     2
 *    / \
 *   1   3
 *
 * Another example, A = 3, B = 2 -> Return 4.
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class CountPermutationsOfBST {
public:
	int count_perm_bst(int n, int h) {

		// pre-compute binary coeficients
		vector<vector<unsigned long long int>> b_coef(n + 1, vector<unsigned long long int>(n + 1));
		unsigned long long int m = 1000000007ULL;
		b_coef[0][0] = 0;
		for (int i = 0; i <= n; i++) {
			b_coef[i][0] = 1ULL;
			for (int j = 1; j <= i; j++)
				b_coef[i][j] = (b_coef[i - 1][j] + b_coef[i - 1][j - 1]) % m;
		}

		/* Questions:
			1. How many BSTs of a given height are?
			A: T(n, H) = [Left(h=0)  + Left(h=1)  + ... + Left(h=H-1)] *Right(h=H-1) + // left varies, and right gives height H
						 [Right(h=0) + Right(h=1) + ... + Right(h=H-1)]*Left(h=H-1) + // right varies, and left gives height H
						 Left(h=H-1)*Right(h=H-1); // both are of height H
			2. Given a BST structure, how many permutations of a set S = {1, 2, ..., n} preserves the same structure after insertions
			   from index 0 to index n-1?
			A: To preserve the BST structure, the root must be inserted first and the left side and the right side
			must contains the same number of elements, thus the root must be the same. And, one more constraint, the
			left and right elements must be inserted in the same relative order (i.e. the insertions of
			right and left elements can be interleaved, but the relative order must be kept, thus if an element from
			the left is inserted infront of other element, then this element must be inserted infront of the other elements
			in all permutations - same for the elements from the right side). So. how many permutations are?
			We know we have n empty seats and k < n elements on the left side and n-k elements on the right side, thus
			if we put the elements from the left side on k empty seats then the remaining seats will be occupied by the
			elements from the right side, thus kCn = (n-k)Cn.

			Dynamic programming: The main idea: for every BST of height h we multiply with the number of
			combinations, thus we obtain the ans. So, in order to compute T(n,H) we need all the previous T(0<j<n, 0<h<H) trees.	
		*/
		vector<vector<int>> dp(n+1, vector<int>(h+1));
		dp[0][0] = dp[1][0] = 1;
		for (int size = 2; size <= n; size++) { //  tree's number of nodes
			for (int height = 1; height <= h; height++) { // tree's height
				for (int k = 1; k <= size; k++) { // tree's root
					unsigned int left_size_k = k - 1;
					unsigned int right_size_nk = size - left_size_k - 1;

					unsigned long long int t_left = 0, t_right = 0;
					for (int i = 0; i <= height - 2; i++) {
						t_left = (t_left + dp[left_size_k][i]) % m;
						t_right = (t_right + dp[right_size_nk][i]) % m;
					}

					// compute the number of BST structures of size (size) and height (h) with k left elements
					// and size-k right elements
					unsigned long long int L = dp[left_size_k][height - 1];
					unsigned long long int R = dp[right_size_nk][height - 1];
					unsigned long long int t_size_h = ((t_left * R) % m + (t_right * L) % m + (L * R) % m ) % m;

					// now we have the no of BST structures, so for every structure we have the kCn permutations
					//  that preserves the structure, so multiply:
					unsigned long long int permutations_h = (t_size_h * b_coef[size - 1][left_size_k]) % m;

					dp[size][height] = (int)(((unsigned long long int)dp[size][height] + permutations_h) % m);
#if 0
					cout << "size(" << size << "):" << " h(" << height << "): " << left_size_k << " " << right_size_nk << " > " << t_size_h << " (tot: " << dp[size][height] << ")" << endl;
					for (int ii = 0; ii <= size; ii++) {
						for (int jj = 0; jj <= h; jj++) {
							cout << dp[ii][jj] << " ";
						}
						cout << endl;
					}
					cout << endl;
					cout << endl;
#endif
				}
			}
		}


		return dp[n][h];
	}
};

#if 0
int main(void) {

	CountPermutationsOfBST t;
	cout << t.count_perm_bst(72, 34) << endl;

	return 0;
}
#endif
