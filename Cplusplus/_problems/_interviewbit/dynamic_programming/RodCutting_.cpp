
/*
 * There is a rod of length N lying on x-axis with its left end at x = 0 and right end at x = N.
 * Now, there are M weak points on this rod denoted by positive integer values(all less than N)
 * A1, A2, �, AM. You have to cut rod at all these weak points. You can perform these cuts in any order.
 * After a cut, rod gets divided into two smaller sub-rods. Cost of making a cut is the length of
 * the sub-rod in which you are making a cut.
 *
 * Your aim is to minimize this cost. Return an array denoting the sequence in which you will make cuts.
 * If two different sequences of cuts give same cost, return the lexicographically smallest.
 */

#include <map>
#include <algorithm>
#include <stack>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

typedef long long LL;

class RodCutting {
public:
	vector<int> rodCut(int n, vector<int>& a) {
		vector<int> ans;
		if (a.empty()) return ans;

		int m = (int)a.size();
		// i, j, min, cut
		map<pair<int, int>, pair<LL, int>> memo;
		DP(0, n, a, 0, m - 1, memo);

		//build answer using preorder traversal of cuts
		stack<pair<int, int>> st;
		pair<int, int> key = make_pair(0, n);
		st.push(key);
		while (!st.empty()) {
			key = st.top(); st.pop();
			pair<LL, int> cut = memo[key];

			if (cut.second != -1) {
				ans.push_back(cut.second);
				st.push(make_pair(cut.second, key.second));
				st.push(make_pair(key.first, cut.second));
			}
		}

		return ans;
	}

	LL DP(int i, int j, vector<int>& a, int k, int l, map<pair<int, int>, pair<LL, int>>& memo) {
		pair<int, int> key = make_pair(i, j);
		if (memo.find(key) != memo.end())
			return memo[key].first;

		if (k > l) memo[key] = make_pair(0, -1);
		else {
			LL aij = LLONG_MAX;
			int cut = -1;
			for (int t = k; t <= l; t++) {
				LL left  = DP(i, a[t], a, k, t - 1, memo);
				LL right = DP(a[t], j, a, t + 1, l, memo);
				if (aij > left + right + (LL)(j - i)) {
					aij = left + right + (LL)(j - i);
					cut = a[t];
				}
			}

			memo[key] = make_pair(aij, cut);
		}

		return memo[key].first;
	}
};

#if 0
int main(void) {
	int n = 6;
	vector<int> a = { 1, 2, 5 };

	RodCutting t;
	vector<int> ans = t.rodCut(n, a);
	
	for (int c : ans) cout << c << " ";
	cout << endl;

	return 0;
}
#endif