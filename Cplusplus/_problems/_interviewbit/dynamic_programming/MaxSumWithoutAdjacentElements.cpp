
/*
 * Given a 2 * N Grid of numbers, choose numbers such that the sum of the numbers
 * is maximum and no two chosen numbers are adjacent horizontally, vertically or diagonally, and return it.
 *
 * Example:
 * Grid:
 * 	1 2 3 4
 * 	2 3 4 5
 * so we will choose
 * 3 and 5 so sum will be 3 + 5 = 8
 *
 */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class MaxSumWithoutAdjacentElements {
public:
	int max_sum(vector<vector<int>>& a) {
		if (a.empty()) return 0;
		int n = (int)a[0].size();
		if (n == 0) return 0;
		if (n == 1) return max(a[0][0], a[0][1]);
		int ans = 0;
#if 1
		int take = max(a[0][0], a[0][1]), no_take;
		int prev = take, prevprev;
		for (int i = 1; i < n; i++) {
			take = max(a[0][i], a[1][i]);
			if (i > 1) take = prevprev + take;
			no_take = prev;

			prevprev = prev;
			prev = max(take, no_take);
		}

		ans = prev;
#endif
#if 0
		if (n < 2) {
			for (int i = 0; i < 2; i++)
				for (int j = 0; j < n; j++) ans = max(ans, a[i][j]);
		} else {
			vector<int> dp(n);
			dp[0] = max(a[0][0], a[1][0]);
			dp[1] = max(a[0][1], a[1][1]);
			ans = max(dp[0], dp[1]);
			for (int i = 1; i < n; i++) {
				int max_cur = max(a[0][i], a[1][i]);
				int take = max_cur;
				if (i > 1) take = dp[i - 2] + max_cur;
				int no_take = dp[i - 1];
				dp[i] = max(take, no_take);
			}

			ans = dp[n - 1];
		}
#endif
#if 0
		if (n <= 2) {
			for (int i = 0; i < 2; i++)
				for (int j = 0; j < n; j++) ans = max(ans, a[i][j]);
		} else {
			vector<vector<int>> dp(2);
			dp[0] = vector<int>(n);
			dp[1] = vector<int>(n);

			dp[0][0] = a[0][0];
			dp[1][0] = a[1][0];
			ans = max(dp[0][0], dp[1][0]);
			for (int j = 1; j < n; j++)
				for (int i = 0; i < 2; i++) {
					int take = a[i][j];
					if (j > 1) take = max(dp[i][j - 2] + a[i][j], dp[(i + 1) % 2][j - 2] + a[i][j]);
					int no_take = max(max(dp[i][j - 1], dp[(i + 1) % 2][j - 1]), dp[(i + 1) % 2][j]);
					dp[i][j] = max(take, no_take);
				}

			ans = max(dp[0][n - 1], dp[1][n - 1]);
		}
#endif
		return ans;
	}
};

#if 0
int main(void) {
	//vector<vector<int>> a =
	//	{{1, 2, 3, 4},
	//	 {2, 3, 4, 5}};
	vector<vector<int>> a =
//	{ {16, 5, 54, 55, 36, 82, 61, 77, 66, 61},
//	{31, 30, 36, 70, 9, 37, 1, 11, 68, 14} };
	{ {1, 5, 4, 5, 3, 5},
	 {3, 3, 2, 7, 2, 1} };



	MaxSumWithoutAdjacentElements t;
	cout << t.max_sum(a);

	return 0;
}
#endif