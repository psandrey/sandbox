
/*
 * Given expression with operands and operators (OR , AND , XOR) ,
 * in how many ways can you evaluate the expression to true, by grouping in different ways?
 * Operands are only true and false.
 *
 * Input: string :  T|F&T^T
 * here '|' will represent or operator
 * 	 '&' will represent and operator
 * 	 '^' will represent xor operator
 * 	 'T' will represent true operand
 * 	 'F' will false
 *
 * Output: different ways % MOD
 * where MOD = 1003
 *
 * Example: string : T|F = > only 1 way (T|F) => T
 * so output will be 1 % MOD = 1
 */

#include <algorithm>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class EvaluateExpressionToTrue {
public:

	// memoization
	int evaluate(string expr) {
		int n = (int)expr.size();

		vector<vector<vector<int>>> memo(n, vector<vector<int>>(n, vector<int>(2, -1)));
		for (int i = 0; i < n; i++) {
			if (expr[i] == 'T') { memo[i][i][0] = 1; memo[i][i][1] = 0; }
			else { memo[i][i][0] = 0; memo[i][i][1] = 1; }
		}

		vector<int> ans = helper(expr, 0, n - 1, memo);
		return ans[0];
	}

	vector<int> helper(string expr, int i, int j, vector<vector<vector<int>>>& memo) {
		if (memo[i][j][0] != -1 && memo[i][j][1] != -1) return memo[i][j];

		memo[i][j][0] = 0; // evaluations to TRUE
		memo[i][j][1] = 0; // evaluations to FALSE
		for (int k = i + 2; k <= j; k += 2) {
			vector<int> ev_prefix = helper(expr, i, k - 2, memo);
			vector<int> ev_sufix = helper(expr, k, j, memo);
			compose(memo[i][j], ev_prefix, ev_sufix, expr[k - 1]);
		}

		return memo[i][j];
	}

	// tabulation
	int evaluate_v2(string expr) {
		int n = (int)expr.size();
		if (n == 0) return 0;

		int nx = n / 2 + 1;
		vector<vector<vector<int>>> dp(nx, vector<vector<int>>(nx, vector<int>(2)));
		for (int i = 0; i < n; i += 2) {
			if (expr[i] == 'T') { dp[i / 2][i / 2][0] = 1; dp[i / 2][i / 2][1] = 0; }
			else { dp[i / 2][i / 2][0] = 0; dp[i / 2][i / 2][1] = 1; }
		}

		for (int len = 2; len < n; len += 2) {
			for (int i = 0; i + len < n; i += 2) {
				int j = i + len;
				dp[i / 2][j / 2][0] = 0; // evaluations to true
				dp[i / 2][j / 2][1] = 0; // evaluations to false

				for (int k = i; k <= i + len - 2; k += 2) {
					char op = expr[k + 1];
					compose(dp[i / 2][j / 2], dp[i / 2][k / 2], dp[(k + 2) / 2][j / 2], op);
				}
			}
		}
		
		return dp[0][nx - 1][0];
	}

	void compose(vector<int>& ans, vector<int>& left, vector<int>& right, char op) {
		int t = 0, f = 0;
		int m = 1003;
		// xor
		if (op == '^') {
			t = (left[0] * right[1]) % m + (left[1] * right[0]) % m;
			f = (left[0] * right[0]) % m + (left[1] * right[1]) % m;
		}
		// or
		else if (op == '|') {
			t = (left[0] * right[1]) % m + (left[1] * right[0]) % m + (left[0] * right[0]) % m;
			f = (left[1] * right[1]) % m;
		}
		// and
		else {
			t = (left[0] * right[0]) % m;
			f = (left[0] * right[1]) % m + (left[1] * right[0]) % m + (left[1] * right[1]) % m;
		}

		ans[0] = (ans[0] + t % m) % m;
		ans[1] = (ans[1] + f % m) % m;
	}
};

#if 0
int main(void) {

	//string expr = "T|F&T^T";
	//string expr = "T|F^F&T^F";
	string expr = "T^T^T^F|F&F^F|T^F^T";
	EvaluateExpressionToTrue t;
	cout << t.evaluate(expr) << endl;
	cout << t.evaluate_v2(expr) << endl;

	return 0;
}
#endif