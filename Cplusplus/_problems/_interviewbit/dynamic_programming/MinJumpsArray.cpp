
/* Given an array of non-negative integers, A, of length N, you are initially positioned at the first index of the array.
 * Each element in the array represents your maximum jump length at that position.
 * Return the minimum number of jumps required to reach the last index.
 * If it is not possible to reach the last index, return -1.
 * 
 * Note: This is the classic DP version: T(n) = O(n^2), however the greedy version is faster: T(n) = O(n).
 * Note: For greedy version see the java implementation.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MinJumpsArray {
public:
	int jump(vector<int>& a) {
		if (a.empty()) return -1;
		if (a.size() == 1) return 0;
		if (a[0] == 0) return -1;

		int n = a.size();
		vector<int> dp(n, INT_MAX);

		dp[0] = 0;
		for (int i = 0; i < n; i++) {
			if (a[i] == 0 || dp[i] == INT_MAX) continue;

			for (int j = i + 1; j <= min(i + a[i], n - 1); j++) dp[j] = min(dp[j], dp[i] + 1);
		}

		return (dp[n - 1] == INT_MAX ? -1 : dp[n - 1]);
	}
};

#if 0
int main(void) {
	vector<int> a = {15, 23, 13, 20, 16, 2, 4, 45, 24, 28, 0, 37, 23, 24, 27, 0, 31, 0, 0, 0, 2, 28, 0, 20, 44, 43, 22, 27, 45, 11, 31, 21, 0, 8, 0, 33, 17, 24, 47, 0, 43, 21, 22, 23, 23, 11, 0, 8, 23, 5, 27, 10, 40, 37, 0, 13, 0, 14, 47, 3, 25, 16, 0, 6, 5, 0, 45, 23, 2, 34, 20, 43, 30, 24, 48, 0, 4, 28, 19, 42, 0, 41, 13, 25, 39, 1, 43, 13, 33, 27, 29, 47, 2, 44, 5, 33, 42, 20, 34, 1, 30, 28, 37, 0, 0, 0, 28, 39, 10, 49, 0, 11, 0, 24, 0, 21, 36, 29, 26, 21, 0, 0, 27, 5, 41, 22, 34, 0, 0, 7, 38, 49, 3, 39, 0, 44, 25, 22, 21, 0, 2, 3, 8, 0, 0, 41, 49, 28, 21, 0, 9, 12, 12, 15, 20, 0, 35, 31, 10, 0, 0, 10, 12, 48, 0, 8, 16, 1, 9, 0, 32, 49, 39, 39, 28, 0, 30, 4, 0, 6, 27, 29, 12, 25, 17, 0, 0, 19, 21, 31, 0, 0, 46, 0, 11, 0, 9, 17, 0, 14, 9, 41, 31, 40, 0, 0, 23, 41, 50, 17, 0, 0, 32, 4, 38, 5, 27, 19, 45, 9, 26, 37, 35, 24, 10, 5, 13, 4, 40, 12, 25, 10, 2, 12, 13, 0, 10, 14, 1, 49, 5, 1, 49, 46, 0, 14, 45, 0, 1, 46, 46, 27, 0, 9, 23, 19, 0, 0, 0, 0, 46, 18, 41, 0, 12, 22, 28, 49, 15, 0, 49, 35, 34, 0, 43, 0, 0, 0, 0, 6, 30, 29, 0, 25, 18, 0, 1, 2, 14, 0, 42, 27, 26, 27, 2, 50, 21, 48, 10, 27, 24, 0, 34, 0, 0 };
	MinJumpsArray t;
	cout << t.jump(a) << endl;

	return 0;
}
#endif