/*
 * Given A, how many structurally unique BST�s (binary search trees) that store values 1...n?
 */

#include <vector> 
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

class UniqueBinarySearchTreesII {
public:
	int numBSTTrees(int n);

};

int UniqueBinarySearchTreesII::numBSTTrees(int n) {
	if (n == 0 || n == 1) return 1;

	vector<int> dp(n + 1);

	dp[0] = 1;
	dp[1] = 1;
	for (int bt_size = 2; bt_size <= n; bt_size++)
		for (int left = 0; left < bt_size; left++)
			dp[bt_size] += dp[left] * dp[bt_size - left - 1]; // left structure * right structure

	return dp[n];
}

/*
int main(void) {
	UniqueBinarySearchTreesII t;
	int n = 3;
	cout << t.numBSTTrees(n) << endl;
	return 0;
}
*/