
/*
 * Say you have an array for which the ith element is the price of a given stock on day i.
 * Design an algorithm to find the maximum profit. You may complete at most two transactions.
 *
 * Note:
 *   A transaction is a buy & a sell. You may not engage in multiple transactions at the same time
 *   (ie, you must sell the stock before you buy again).
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class BestTimeToBuyAndSellStocks_III {
public:
	int maxProfit(const vector<int>& a) {
		if (a.empty()) return 0;

		int n = (int)a.size();
		unsigned long long profit1 = 0, profit2 = 0, profit = 0;
		for (int j = 1; j < n; j++)
			if (a[j] >= a[j - 1]) profit += (a[j] - a[j - 1]);
			else {
				if (profit > profit1) { profit2 = profit1; profit1 = profit; }
				else if (profit > profit2) profit2 = profit;
				profit = 0;
			}

		if (profit > profit1) { profit2 = profit1; profit1 = profit; }
		else if (profit > profit2) profit2 = profit;

		return (profit1 + profit2);
	}

};

#if 0
int main(void) {
	vector<int> a = { 1, 4, 5, 7, 6, 3, 2, 9 };

	BestTimeToBuyAndSellStocks_III t;
	cout << t.maxProfit(a) << endl;
	
	return 0;
}
#endif