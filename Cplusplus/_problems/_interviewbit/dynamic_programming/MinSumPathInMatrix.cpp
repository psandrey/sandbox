/*
 * Given a m x n grid filled with non-negative numbers,
 * find a path from top left to bottom right which minimizes the sum of all numbers along its path.
 *
 * Note: You can only move either down or right at any point in time.
 * 
 * Example :
 * Input :
 * 	[1 3 2
 * 	 4 3 1
 * 	 5 6 1]
 *
 * Output : 8
 * 	 1 -> 3 -> 2 -> 1 -> 1
 */

#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class MinSumPathInMatrix {
public:
	int min_sum_path(vector<vector<int>> &);
};

int MinSumPathInMatrix::min_sum_path(vector<vector<int>> &a) {
	if (a.empty() || a[0].empty()) return 0;

	int n = a.size();
	int m = a[0].size();

	vector<vector<int>> dp(n, vector<int>(m));
	dp[0][0] = a[0][0];
	for (int i = 1; i < n; i++) dp[i][0] = a[i][0] + dp[i - 1][0];
	for (int j = 1; j < m; j++) dp[0][j] = a[0][j] + dp[0][j - 1];

	for (int i = 1; i < n; i++)
		for (int j = 1; j < m; j++)
			dp[i][j] = min(dp[i][j - 1], dp[i - 1][j]) + a[i][j];

	return dp[n - 1][m - 1];
}

/*
int main(void) {
	vector<vector<int>> a = { { 1, 3, 2 },
							  { 4, 3, 1 },
							  { 5, 6, 1 } };
	MinSumPathInMatrix t;
	cout << t.min_sum_path(a) << endl;
}
*/