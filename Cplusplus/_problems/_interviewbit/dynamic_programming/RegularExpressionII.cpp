
/*
 * Implement regular expression matching with support for '.' and '*'.
 * '.' Matches any single character.
 * '*' Matches zero or more of the preceding element.
 *
 * The matching should cover the entire input string (not partial).
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class RegularExpressionII {
public:
	int isMatch(const string s, const string p) {
		if (s.empty() && p.empty()) return 1;

		int n = (int)s.size();
		int m = (int)p.size();

		vector<vector<bool>> dp(n + 1, vector<bool>(m + 1, false));
		dp[0][0] = true;
		for (int j = 1; j <= m; j++)
			if (p[j - 1] == '*') dp[0][j] = dp[0][j - 2];

		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++) {
				if (s[i - 1] == p[j - 1] || p[j - 1] == '.')		dp[i][j] = dp[i - 1][j - 1];
				else if (p[j - 1] == '*'
					&& s[i - 1] != p[j - 2] && p[j - 2] != '.')		dp[i][j] = dp[i][j - 2];
				else if (p[j - 1] == '*'
					&& (s[i - 1] == p[j - 2] || p[j - 2] == '.'))	dp[i][j] = dp[i][j - 2] || dp[i - 1][j];
				else dp[i][j] = false;
			}
#if 0
		for (int i = 0; i <= n; i++){
			for (int j = 0; j <= m; j++) {
				int x = (dp[i][j] == true ? 1 : 0);
				cout << x << " ";
			}
			cout << endl;
		}
#endif
		return (dp[n][m] ? 1 : 0);
	}
};

#if 0
int main(void) {
	//string  s = "aab";
	//string p = "c*a*b";
	
	//string s = "abbc";
	//string p = "ab*bbc";
	
	string s = "abcbcd";
	string p = "a.*c.*d";

	RegularExpressionII t;
	cout << t.isMatch(s, p) << endl;
		
	return 0;
}
#endif