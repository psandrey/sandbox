/* 
 * Given a binary tree, find the maximum path sum.
 * The path may start and end at any node in the tree.
 */

#include <iostream>
#include <algorithm>
using namespace std;

class MaxSumPathInBinaryTree {
public:
	class TreeNode {
	public:
		TreeNode* left, * right;
		int val;
		TreeNode(int v) {
			left = nullptr;
			right = nullptr;
			val = v;
		}
	};

	int maxPathSum(TreeNode *);
private:
	int helper(TreeNode*, int*);
};

int MaxSumPathInBinaryTree::maxPathSum(TreeNode* root) {
	if (root == nullptr) return 0;
	int maxPath = INT_MIN;
	helper(root, &maxPath);

	return maxPath;
}

int MaxSumPathInBinaryTree::helper(TreeNode *n, int *maxSoFar) {
	if (n == nullptr) return 0;

	// we use max between 0 and whatever left/right is returning to avoid
	//  wrapping the integer value: i.e. INT_MIN - 1 = INT_MAX
	int L = max(0, helper(n->left, maxSoFar));
	int R = max(0, helper(n->right, maxSoFar));

	*maxSoFar = max(*maxSoFar, L + R + n->val);

	return n->val + max(L, R);
}

MaxSumPathInBinaryTree::TreeNode* buildTree(void) {
/*
	TreeNode* root = new TreeNode(10);
	root->left = new TreeNode(2);
	root->right = new TreeNode(10);
	root->left->left = new TreeNode(20);
	root->left->right = new TreeNode(1);
	root->right->right = new TreeNode(-25);
	root->right->right->left = new TreeNode(3);
	root->right->right->right = new TreeNode(4);
*/
	MaxSumPathInBinaryTree::TreeNode *root = new MaxSumPathInBinaryTree::TreeNode(-200);
	root->left = new MaxSumPathInBinaryTree::TreeNode(-100);
	root->left->left = new MaxSumPathInBinaryTree::TreeNode(-300);
	root->left->right = new MaxSumPathInBinaryTree::TreeNode(-400);
	return root;
}

#if 0
int main(void) {
	MaxSumPathInBinaryTree::TreeNode *root = buildTree();
	MaxSumPathInBinaryTree t;
	cout << "Max sum path: " << t.maxPathSum(root) << endl;

	// TODO: memory leak... tree should be freed.
	return 0;
}
#endif