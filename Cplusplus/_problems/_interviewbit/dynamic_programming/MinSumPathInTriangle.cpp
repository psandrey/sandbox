
/*
 * Given a triangle, find the minimum path sum from top to bottom.
 * Each step you may move to adjacent numbers on the row below.
 *
 * For example, given the following triangle
  *
 *     [2],
 *    [3,4],
 *   [6,5,7],
 *  [4,1,8,3]
 * 
 * The minimum path sum from top to bottom is 11 (i.e., 2 + 3 + 5 + 1 = 11).
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class MinSumPathInTriangle {
public:
	int min_sum(vector<vector<int>>& a) {
		int n = (int)a.size();
		if (n == 0) return 0;
		if (n == 1) return a[0][0];

		vector<vector<int>> dp(2, vector<int>(n));
		dp[0][0] = a[0][0];
		int toggle = 1, ans = 0;
		for (int i = 1; i < n; i++) {
			ans = INT_MAX;
			for (int j = 0; j <= i; j++) {

				int d = 0;
				if (j == 0) d = dp[toggle^1][j];
				else if (j == i) d = dp[toggle ^ 1][j - 1];
				else d = min(dp[toggle ^ 1][j - 1], dp[toggle ^ 1][j]);

				dp[toggle][j] = a[i][j] + d;
				ans = min(ans, dp[toggle][j]);
			}

			toggle ^= 1;
		}

		return ans;
	}
};

#if 0
int main(void) {

	vector<vector<int>> a =
	{
		{2},
		{3, 4},
		{6, 5, 7},
		{4, 1, 8, 3}
	};

	MinSumPathInTriangle t;
	cout << t.min_sum(a);

	return 0;
}
#endif