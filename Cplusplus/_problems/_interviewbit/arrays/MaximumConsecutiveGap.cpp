
/*
 * Given an unsorted array, find the maximum difference between the successive elements in its sorted form.
 *
 * Try to solve it in linear time/space.
 *
 * Example :
 * Input : [1, 10, 5]
 * Output : 5
 * Return 0 if the array contains less than 2 elements.
 *
 * You may assume that all the elements in the array are non-negative integers and fit in the 32-bit signed integer range.
 * You may also assume that the difference will not overflow.
 */

#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class MaximumConsecutiveGap {
public:
	int max_gap(vector<int>& a) {
		if (a.empty() || a.size() < 2) return 0;
		int n = (int)a.size();

		int M = INT_MIN, m = INT_MAX;
		for (int j = 0; j < n; j++) { m = min(m, a[j]);  M = max(M, a[j]); }

		/* We use pigeonhole principle that says:
		 *    - If we distribute n numbers into n + 1 buckets, then for sure 1 bucket will remain empty.
		 *      Since the buckets are uniformly distributed, then even if the elements are uniformly
		 *      distributed the empty bucket will reveal the biggest gap. If the elements are not distributed
		 *      uniformly, then they will cluster somewhere and the gap will be bigger (revealed by the
		 *      multiple empty buckets).
		 * Note:
		 *   - M falls into the n + 1 bucket
		 *   - m falls into the 1'st bucket
		 *   - Each bucket have the form: [x, x + d), so
		 *     [m b1)[ b2 )[ b3 )...[ bn )[M  bn+1)
		 */

		double d = ((double)M - m) / n;
		vector<pair<int, int>> bucket(n, make_pair(INT_MAX, INT_MIN));
		bucket[0].first = m;
		bucket[0].second = m;
		for (int i = 0; i < n; i++) {
			if (a[i] == m || a[i] == M) continue;

			int k = (int)floor(((double)a[i] - m) / d);
			bucket[k].first = min(bucket[k].first, a[i]);
			bucket[k].second = max(bucket[k].second, a[i]);
		}
		

		int ans = INT_MIN;
		pair<int, int> prev = bucket[0];
		for (int i = 1; i < n; i++) {
			if (bucket[i].first == INT_MAX || bucket[i].second == INT_MIN) continue;

			ans = max(ans, bucket[i].first - prev.second);
			prev = bucket[i];
		}
		ans = max(ans, M - prev.second);

		return ans;
	}
};

#if 0
int main(void) {
#if 0
	vector<int> a = { 100, 100, 100 };
#else
	vector<int> a = { 46158044, 9306314, 51157916, 93803496, 20512678, 55668109, 488932,
					24018019, 91386538, 68676911, 92581441, 66802896, 10401330, 57053542,
					42836847, 24523157, 50084224, 16223673, 18392448, 61771874, 75040277,
					30393366, 1248593, 71015899, 20545868, 75781058, 2819173, 37183571,
					94307760, 88949450, 9352766, 26990547, 4035684, 57106547, 62393125,
					74101466, 87693129, 84620455, 98589753, 8374427, 59030017, 69501866,
					47507712, 84139250, 97401195, 32307123, 41600232, 52669409, 61249959,
					88263327, 3194185, 10842291, 37741683, 14638221, 61808847, 86673222,
					12380549, 39609235, 98726824, 81436765, 48701855, 42166094, 88595721,
					11566537, 63715832, 21604701, 83321269, 34496410, 48653819, 77422556,
					51748960, 83040347, 12893783, 57429375, 13500426, 49447417, 50826659,
					22709813, 33096541, 55283208, 31924546, 54079534, 38900717, 94495657,
					6472104, 47947703, 50659890, 33719501, 57117161, 20478224, 77975153,
					52822862, 13155282, 6481416, 67356400, 36491447, 4084060, 5884644,
					91621319, 43488994, 71554661, 41611278, 28547265, 26692589, 82826028,
					72214268, 98604736, 60193708, 95417547, 73177938, 50713342, 6283439,
					79043764, 52027740, 17648022, 33730552, 42851318, 13232185, 95479426,
					70580777, 24710823, 48306195, 31248704, 24224431, 99173104, 31216940,
					66551773, 94516629, 67345352, 62715266, 8776225, 18603704, 7611906 };
	// ans = 2835941
#endif

	MaximumConsecutiveGap t;
	cout << t.max_gap(a);

}
#endif