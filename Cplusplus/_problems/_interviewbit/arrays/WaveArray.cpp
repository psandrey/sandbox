
/*
 * Given an array of integers, sort the array into a wave like array and return it.
 * In other words, arrange the elements into a sequence such that a1 >= a2 <= a3 >= a4 <= a5.....
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class WaveArray {
public:
	vector<int> wave(vector<int>& a) {
		if (a.empty() || a.size() <= 1) return a;

		int n = (int)a.size();
		sort(a.begin(), a.end());
		for (int i = 0; i < n - 1; i += 2) {
			int tmp = a[i];
			a[i] = a[i + 1];
			a[i + 1] = tmp;
		}

		return a;
	}
};

#if 0
int main(void) {
	vector<int> a = { 1, 2, 3, 4 };

	WaveArray t;
	vector<int> ans = t.wave(a);
	for (auto x : ans) cout << x << " ";

	return 0;
}
#endif