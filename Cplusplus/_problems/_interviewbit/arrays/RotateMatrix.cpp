
/*
 * You are given an n x n 2D matrix representing an image.
 * Rotate the image by 90 degrees (clockwise).
 *
 * Note: You need to do this in place.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class RotateMatrix {
public:
	void rotate_border(vector<vector<int>>& a, int n, int k) {
		for (int j = 0; j < n - 1; j++) {
			int tmp = a[k][k + j];
			a[k][k + j] = a[k + n - 1 - j][k];
			a[k + n - 1 - j][k] = a[k + n - 1][k + n - 1 - j];
			a[k + n - 1][k + n - 1 - j] = a[k + j][k + n - 1];
			a[k + j][k + n - 1] = tmp;
		}
	}

	void rotate(vector<vector<int> >& a) {
		if (a.empty() || a.size() == 1) return;

		int n = (int)a.size();
		for (int k = 0; k < n / 2; k++)
			rotate_border(a, n - 2 * k, k);
	}
};

#if 0
int main(void) {

	vector<vector<int>> a = {
		{1, 2},
		{3, 4}};

	RotateMatrix t;
	t.rotate(a);
	int n = (int)a.size();

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			cout << a[i][j] << ", ";
		}
		cout << endl;
	}

	return 0;
}
#endif