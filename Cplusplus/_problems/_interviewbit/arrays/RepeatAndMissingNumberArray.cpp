
/*
 * You are given a read only array of n integers from 1 to n.
 * Each integer appears exactly once except A which appears twice and B which is missing.
 * Return A and B.
 *
 * Note: Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
 * Note that in your output A should precede B.
 *
 * Example:
 *   Input:[3 1 2 5 3]
 *   Output:[3, 4]
 * A = 3, B = 4
 *
 * Note: there are 3 solutions.
 */

#include <vector>
#include <iostream>
using namespace std;

class RepeatAndMissingNumberArray {
public:
	// using math
	vector<int> repeat_and_missing_v0(vector<int> a) {
		vector<int> ans(2);

		int n = a.size();
		long long ds = 0, ds2 = 0;
		for (int i = 0; i < n; i++) {
			long long _a = (long long)a[i];
			long long _i = ((long long)i + 1);

			ds += _a;
			ds -= _i;

			ds2 -= _i * _i;
			ds2 += _a * _a;
		}

		ds2 /= ds;
		ans[0] = (int)((ds + ds2) / 2);
		ans[1] = ans[0] - (int)ds;

		return ans;
	}

	// use xor and find duplicate (chagen the input)
	vector<int> repeat_and_missing_v1(vector<int> a) {
		vector<int> ans(2);

		int n = a.size();
		unsigned int x = 0, y = 0, duplicate = 0;
		for (int i = 0; i < n; i++) {
			x ^= (i+1);
			y ^= abs(a[i]);

			int z = abs(a[i]);
			int p = a[z - 1];
			if (p < 0) duplicate = z;
			else a[z - 1] *= -1;
		}

		unsigned int both = x ^ y;
		unsigned int missing = both ^ (unsigned int)duplicate;

		ans[0] = duplicate;
		ans[1] = missing;
		return ans;
	}

	// use xor and divide the input into two subsets:
	//  - one containing the duplicate and one the missing
	//  - find which is which and return the answer
	vector<int> repeat_and_missing_v2(const vector<int>& a) {
		vector<int> ans(2);

		int n = a.size();
		unsigned int x = 0, y = 0;
		for (int i = 0; i < n; i++) {
			x ^= (i + 1);
			y ^= a[i];
		}
		unsigned int both = x ^ y;

		// - find the first bit set (fs) from  missing^duplicate
		// both-1 -> changes the first set bit into 0
		// ~(both-1) -> toggle all 1 bits to 0, but the bit who was set to 0, now will be 1
		// both & ~(both - 1) -> keeps only the first bit set to 1
		unsigned int fs = both & ~(both - 1);

		// first bit set (fs) is used to divide the set of numbers into two sets
		// such that one set will contain the missing number and the other the duplicate one
		x = 0; y = 0;
		int cx = 0, cy = 0;
		for (int i = 0; i < n; i++) {
			if (((unsigned int)a[i] & fs) != 0) x ^= a[i];
			else y ^= a[i];

			if (((unsigned int)(i+1) & fs) != 0) cx ^= (i+1);
			else cy ^= (i+1);
		}

		// the set that contains the duplicate... all numbers will nulify,
		//  except the duplicate one (because, one more xor)
		// similar, for the set that contains the missing number, all numbers
		//  will nulify except the missing one (because, one more xor)
		ans[0] = (int)(x^cx);
		ans[1] = (int)(y^cy);

		// now we know the two numbers, so lets see with one is the duplicate one
		//  and which is the missing one.
		for (int i = 0; i < n; i++) {
			if (ans[1] == a[i]) {
				 int t = ans[0]; ans[0] = ans[1]; ans[1] = t;
				 break;
			}
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = {3, 1, 2, 5, 3};

	RepeatAndMissingNumberArray t;

	vector<int> ans = t.repeat_and_missing_v0(a);
	cout << ans[0] << " " << ans[1] << endl;


	ans = t.repeat_and_missing_v1(a);
	cout << ans[0] << " " << ans[1] << endl;

	ans = t.repeat_and_missing_v2(a);
	cout << ans[0] << " " << ans[1] << endl;
	
	return 0;
}
#endif