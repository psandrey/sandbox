
/*
 * You are in an infinite 2D grid where you can move in any of the 8 directions :
 * You are given a sequence of points and the order in which you need to cover the points. 
 * Give the minimum number of steps in which you can achieve it. You start from the first point.
 *
 * Example: 
 *  Input : [(0, 0), (1, 1), (1, 2)]
 *  Output : 2
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MinStepsInInfiniteGrid {
public:
	int min_steps(vector<int>& a, vector<int>& b) {
		if (a.size() == 0 || b.size() == 0) return 0;

		int steps = 0;
		for (int j = 0; j < (int)a.size() - 1; j++) {
			int dx = abs(a[j] - a[j + 1]);
			int dy = abs(b[j] - b[j + 1]);
			int d = max(dx, dy);

			steps += d;
		}

		return steps;
	}
};

#if 0
int main(void) {
	vector<int> a = { 4, 1, 4, 10 };
	vector<int> b = { 6, 2, 5, 12 };

	MinStepsInInfiniteGrid t;
	cout << t.min_steps(a, b);

	return 0;
}
#endif