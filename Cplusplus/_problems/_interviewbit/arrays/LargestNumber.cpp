
/*
 * Given a list of non negative integers, arrange them such that they form the largest number.
 *
 * For example:
 *  Given [3, 30, 34, 5, 9], the largest formed number is 9534330.
 */

#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

class LargestNumber {
public:

	string largest(const vector<int>& a) {
		if (a.size() == 0) return "";
		int n = (int)a.size();
		vector<string> b(a.size());
		for (int i = 0; i < n; i++) b[i] = to_string(a[i]);

		struct {
			bool operator()(const string& a, const string& b) {
				return ((a + b).compare(b + a) > 0);
			}
		} comp;
		sort(b.begin(), b.end(), comp);

		// cut trailing zeroes
		int s = 0;
		for (s = 0; s < n; s++)
			if (b[s].compare("0") != 0) break;
		if (s == b.size()) return "0";

		// construct number
		string ans;
		for (int j = s; j < n; j++) ans += b[j];

		return ans;
	}
};

#if 0
int main(void) {
	//vector<int> a = { 0, 0, 0 };
	vector<int> a = { 3, 30, 33 };
	LargestNumber t;
	string ans = t.largest(a);
	cout << ans << endl;
}
#endif