
/*
 * Given an integer n, generate a square matrix filled with elements from 1 to n2 in spiral order.
 *
 * Example: given n = 3,
 *
 * You should return the following matrix:
 *   [ 1, 2, 3 ]
 *   [ 8, 9, 4 ]
 *   [ 7, 6, 5 ]
 */

#include <vector>
#include <iostream>
using namespace std;

class SpiralOrderMatrixII {
public:

	int fill_edges(vector<vector<int>> & a, int k, int n, int m) {
			if (n == 1) a[k][k] = m++;
			else {
				// upper line
				for (int j = 0; j < n - 1; j++) a[k][k + j] = m++;
				// right col
				for (int j = 0; j < n - 1; j++) a[k + j][k + n - 1] = m++;
				//bottom line
				for (int j = n - 1; j > 0; j--) a[k + n - 1][k + j] = m++;
				//left col
				for (int j = n - 1; j > 0; j--) a[k + j][k] = m++;
			}

			return m;
		}

	vector<vector<int>> spiral(int n) {
		if (n == 0) return {};
		vector<vector<int>> a(n, vector<int>(n));

		int m = 1;
		for (int k = 0; k < (n + 1) / 2; k++)
			m = fill_edges(a, k, n - 2 * k, m);

		return a;
	}

	// debug
	void print_matrix(vector<vector<int>>& a) {
		int n = (int)a.size();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				cout << a[i][j] << " ";
			}
			cout << endl;
		}
	}
};

#if 0
int main(void) {
	SpiralOrderMatrixII t;
	vector<vector<int>> ans = t.spiral(7);
	t.print_matrix(ans);
	return 0;
}
#endif