
/*
 * Given a positive integer n and a string s consisting only of letters D or I,
 * you have to find any permutation of first n positive integer that satisfy the given input string.
 *
 * D means the next number is smaller, while I means the next number is greater.
 *
 * Notes:
 *  Length of given string s will always equal to n - 1
 *  Your solution should run in linear time and space.
 *  
 * Example :
 * Input 1: n = 3 , s = ID
 * Return: [1, 3, 2]
 *
 */

#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class FindPermutation {
public:
	vector<int> find_perm_v1(const string s, int n) {
		vector<int> ans(n);

		// compute offset for each position and
		//  compute offset of the entire graphic to do the translation
		int ups = 0, downs = 0, off = 0;
		for (int i = 0; i < n - 1; i++) {
			int d = 0;
			if (s[i] == 'D') {
				d = -1 - ups; ups = 0;
				downs += -d;
			} else {
				d = 1 + downs; downs = 0;
				ups += d;
			}

			ans[i + 1] = d;
			off += d;
		}

		// translate graphic
		if (off < 0) { off = -off; off += 1; }
		else off = n - off;
		ans[0] = off;

		// construct graphic
		for (int i = 1; i < n; i++) ans[i] = ans[i] + ans[i - 1];

		return ans;
	}

	vector<int> find_perm_v2(const string s, int n) {
		int off = 1;
		
		// compute offset
		for (int i = 0; i < (int)s.size(); i++)
			if (s[i] == 'D') off += 1;

		// construct permutation
		vector<int> ans;
		int D = off, I = off;
		ans.push_back(off);
		for (int i = 0; i < (int)s.size(); i++)
			if (s[i] == 'D') ans.push_back(--D);
			else ans.push_back(++I);

		return ans;
	}
};

#if 0
int main(void) {
	//string s = "DDIIDIDIIDIDD";
	//int n = 14;
	//string s = "IDIDI";
	//int n = 6;
	string s = "DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD";
	int n = 903;

	FindPermutation t;
	vector<int> ans = t.find_perm_v1(s, n);
	for (int x : ans) cout << x << " ";
	cout << endl;

	ans = t.find_perm_v2(s, n);
	for (int x : ans) cout << x << " ";

	return 0;
}
#endif