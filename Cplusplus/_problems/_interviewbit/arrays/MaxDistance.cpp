
/*
 * Given an array A of integers, find the maximum of j - i subjected to
 *  the constraint of A[i] <= A[j].
 *
 * Note: If there is no solution possible, return -1.
 */

 /*
  * There are 5 versions:
  * - Thumb-up: T(n) = O(n) - find the maximum window based on leftMin-rightMax arrays
  * - T(n) = O(n.logn) - sorting the array
  * - T(n) = O(2^n) - recursive brute-force
  * - T(n) = O(n^2) - brute-force
  * - T(n) = O(n^2/2) = O(n^2) - dynamic programming version
  */

#include <unordered_map>
#include <numeric>
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class MaxDistance {
public:

#if 0
	// version 1: T(n) = O(n.log.n), S(n) = O(n)
	int maximumGap(vector<int>& a) {
		if (a.empty()) return 0;
		int n = (int)a.size();

#if 0
		vector<int> idx(n);
		iota(idx.begin(), idx.end(), 0);
		sort(idx.begin(), idx.end(), [&a](size_t i, size_t j) { return (a[i] < a[j]); });
		
		int min_so_far = idx[0], max_gap = 0;
		for (int i = 1; i < n; i++) {
			min_so_far = min(min_so_far, idx[i]);
			max_gap = max(max_gap, idx[i] - min_so_far);
		}
#else
		vector<pair<int, int>> b(n);
		for (int j = 0; j < n; j++) b[j] = make_pair(a[j], j);
		sort(b.begin(), b.end());

		int min_so_far = INT_MAX, max_gap = INT_MIN;
		for (int j = 0; j < n; j++) {
			int i = b[j].second;
			min_so_far = min(min_so_far, i);
			max_gap = max(max_gap, i - min_so_far);
		}
#endif
		return max_gap;
	}

#else

	// version 1: T(n) = O(n), S(n) = O(n)
	int maximumGap(vector<int>& a) {
		if (a.empty() || a.size() == 1) return 0;
		int n = (int)a.size();

		vector<int> r_max(n);
		r_max[n - 1] = a[n - 1];
		for (int i = n - 2; i >= 0; i--) r_max[i] = max(r_max[i + 1], a[i]);

		vector<int> l_min(n);
		l_min[0] = a[0];
		for (int i = 1; i < n; i++) l_min[i] = min(l_min[i - 1], a[i]);

		int max_gap = 0, l = 0, r = 0;
		while (r < n) {
			if (l_min[l] <= r_max[r]) { max_gap = max(max_gap, r - l); r++; }
			else l++;
		}

		return max_gap;
	}
#endif
};

#if 0
int main(void) {
	vector<int> a = { 4, 2, 6, 5, 1, 3 };
	//vector<int> a = { 3, 2, 1 };

	MaxDistance t;
	int max_gap = t.maximumGap(a);
	cout << max_gap << endl;
	
	return 0;
}
#endif