
/*
 * Find the contiguous subarray within an array (containing at least one number) which has the largest sum.
 *
 * For example:
 *  Given the array [-2,1,-3,4,-1,2,1,-5,4],
 *   the contiguous subarray [4,-1,2,1] has the largest sum = 6.
*/

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MaxSumContiguousSubarray {
public:
	int max_sum_contiguous_subarray(vector<int>& a) {
		if (a.empty()) return INT_MIN;
		if (a.size() == 1) return a[0];

		int s = a[0], ans = a[0];
		for (int i = 1; i < (int)a.size(); i++) {
			s = max(a[i], s + a[i]);
			ans = max(ans, s);
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
//	vector<int> a = { -500 };

	MaxSumContiguousSubarray t;
	cout << t.max_sum_contiguous_subarray(a);

	return 0;
}
#endif