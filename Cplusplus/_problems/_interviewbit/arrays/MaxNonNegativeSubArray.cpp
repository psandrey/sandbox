
/*
 * Find out the maximum sub-array of non negative numbers from an array.
 * The sub-array should be continuous.
 *
 * Maximum sub-array is defined in terms of the sum of the elements in the sub-array.
 * Sub-array A is greater than sub-array B if sum(A) > sum(B).
 *
 * NOTE: If there is a tie, then compare with segment's length and return segment
 *  which has maximum length.
 * NOTE 2: If there is still a tie, then return the segment with minimum starting index.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MaxNonNegativeSubArray {
public:
	vector<int> maxset(vector<int>& a) {
		vector<int> ans;
		if (a.empty()) return ans;

#if 1
		int n = (int)a.size();
		long long int M = -1LL, sum = 0LL;
		int s = -1, e = -1;
		int i = 0;
		for (int j = 0; j <= n; j++) {
			if (j < n && a[j] >= 0) {
				if (a[i] < 0) { i = j; sum = a[j]; }
				else sum += a[j];
			} else {
				if (a[i] >= 0) {
					if (sum > M) { s = i; e = j; M = sum; }
					i = j;
				}
			}
		}

		if (s != -1 && e != -1) ans = vector<int>(a.begin() + s, a.begin() + e);
		return ans;
#else

		int n = (int)a.size();
		int l = 0, r = 0, b = 0, e = 0;
		long long int s = -1LL, cs = 0LL;
		while (r <= n) {
			if (a[l] >= 0 && (r == n || a[r] < 0)) {
				if (s < cs || s == -1) { s = cs; b = l; e = r; }
				if (s == cs && e - b < r - l) { b = l; e = r; }
				cs = 0;
				l = r;
			} else if (r < n) {
				if (a[l] < 0) l = r;
				if (a[r] >= 0) cs += a[r];
			}
			r++;
		}

		if (s == -1) return ans;
		for (int k = b; k < e; k++) ans.push_back(a[k]);

		return ans;
#endif
	}
};

#if 0
int main(void) {
	//vector<int> a = { 1, 2, 5, -7, 2, 3 };
	//vector<int> a = { 10, -1, 2, 3, -4, 100 };
	vector<int> a = { 10, 100 };
	//vector<int> a = { 1967513926, 1540383426, -1303455736, -521595368 };
	//vector<int> a = { 0, 0, -1, 0 };
	//vector<int> a = { 336465782, -278722862, -2145174067, 1101513929, 1315634022, -1369133069,
	//				  1059961393, 628175011, -1131176229, -859484421 };
	
	MaxNonNegativeSubArray t;
	vector<int> ans = t.maxset(a);
	for (int x : ans) cout << x << " ";

	return 0;
}
#endif