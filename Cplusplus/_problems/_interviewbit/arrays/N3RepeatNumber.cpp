
/*
 * You�re given a read only array of n integers.
 * Find out if any integer occurs more than n/3 times in the array in linear time and
 *  constant additional space.
 *
 *  If so, return the integer. If not, return -1.
 *  If there are multiple solutions, return any one.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class N3RepeatNumber {
public:
	int repeatedNumber(const vector<int>& a) {
		if (a.empty()) return -1;
		if (a.size() == 1) return a[0];
		int n = (int)a.size();


		int* m1 = NULL, * m2 = NULL, c1 = 0, c2 = 0;
		for (int i = 0; i < n; i++) {
			if (m1 != NULL && a[i] == (*m1)) c1++;
			else if (m2 != NULL && a[i] == (*m2)) c2++;
			else if (c1 == 0) { m1 = (int*)&a[i]; c1 = 1; }
			else if (c2 == 0) { m2 = (int*)&a[i]; c2 = 1; }
			else { c1--; c2--; }
		}

		c1 = 0; c2 = 0;
		for (int i = 0; i < n; i++) {
			if (m1 != NULL && a[i] == (*m1)) c1++;
			else if (m2 != NULL && a[i] == (*m2)) c2++;
		}

		if (c1 > n / 3) return (*m1);
		else if (c2 > n / 3) return (*m2);

		return -1;
	}
};

#if 0
int main(void) {
	vector<int> a = { 1,2,1,2,1 };
	
	N3RepeatNumber t; 
	cout << t.repeatedNumber(a) << endl;
	return 0;
}
#endif