
/*
 * Given a set of non-overlapping intervals, insert a new interval into the intervals
 * (merge if necessary).
 *
 * Note: You may assume that the intervals were initially sorted according to their start times.
 *
 * Example 1:
 * Given intervals [1,3],[6,9] insert and merge [2,5] would result in [1,5],[6,9].
 *
 * Example 2:
 * Given [1,2],[3,5],[6,7],[8,10],[12,16], insert and merge [4,9] would result in [1,2],[3,10],[12,16].
 * This is because the new interval [4,9] overlaps with [3,5],[6,7],[8,10].
 *
 * Note: Make sure the returned intervals are also sorted.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MergeIntervals {
public:
	struct Interval {
		int start, end;
		Interval() : start(0), end(0) {}
		Interval(int s, int e) : start(s), end(e) {}
		
	};
	
	vector<Interval> insert(vector<Interval>& i, Interval newi) {
		vector<Interval> ans;
		if (i.empty()) ans.push_back(newi);
		else {
			int n = (int)i.size();

			// before
			int j = 0;
			while (j < n && i[j].end < newi.start) {
				ans.push_back(i[j]);
				j++;
			}

			// merge/insert
			while (j < n && i[j].start <= newi.end) {
				newi.start = min(newi.start, i[j].start);
				newi.end = max(newi.end, i[j].end);
				j++;
			}
			ans.push_back(newi);

			// after
			while (j < n) {
				ans.push_back(i[j]);
				j++;
			}
		}

		return ans;
	}

	vector<Interval> build_i(vector<vector<int>>& a) {
		vector<Interval> i;
		for (int j = 0; j < (int)a.size(); j++)
			i.push_back(Interval(a[j][0], a[j][1]));

		return i;
	}
};

#if 0
int main(void) {
	vector<vector<int>> a = { {1,2},{3,5},{6,7},{8,10},{12,16} };
	MergeIntervals t;
	vector<MergeIntervals::Interval> i = t.build_i(a);
	vector<MergeIntervals::Interval> ans = t.insert(i, MergeIntervals::Interval(4, 9));

	for (int j = 0; j < (int) ans.size(); j++) {
		cout << " [ "<< ans[j].start << " , " << ans[j].end << "]  ";
	}
	return 0;
}
#endif