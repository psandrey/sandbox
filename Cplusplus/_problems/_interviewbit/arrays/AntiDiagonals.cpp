
/*
 * Give a N*N square matrix, return an array of its anti-diagonals.
 * Look at the example for more details.
 *
 * Input: 	
 *   1 2 3
 *   4 5 6
 *   7 8 9
 *
 * Return the following :
 *   [1],
 *   [2, 4],
 *   [3, 5, 7],
 *   [6, 8],
 *   [9]
 */

#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class AntiDiagonals {
public:
	vector<vector<int>> anti_diag(vector<vector<int>>& a) {
		if (a.empty()) return vector<vector<int>>();

		int n = (int)a.size();
		vector<vector<int>> ans(2 * n - 1);

#if 1
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				ans[i + j].push_back(a[i][j]);
			}
		}
#else
		// upper diagonals
		int d = 0;
		for (int k = 0; k < n; k++) {
			int i = 0, j = k;
			vector<int>* diag = &ans[d];
			d++;
			while (j >= 0) {
				(*diag).push_back(a[i][j]);
				i++; j--;
			}
		}

		// lower diagonals
		for (int k = 1; k < n; k++) {
			int i = k, j = n - 1;
			vector<int>* diag = &ans[d];
			d++;
			while (i < n) {
				(*diag).push_back(a[i][j]);
				i++; j--;
			}
		}
#endif

		return ans;
	}

	void print_vv(vector<vector<int>>& a) {
		int n = (int)a.size();
		for (int i = 0; i < n; i++) {
			int m = (int)a[i].size();
			for (int j = 0; j < m; j++) {
				cout << a[i][j] << " ";
			}
			cout << endl;
		}
	}
};

#if 0
int main(void) {
	vector<vector<int>> a = { { 1, 2, 3 },
							  { 4, 5, 6 },
							  { 7, 8, 9 } };
	vector<vector<int>> b = { { 1 } };
	AntiDiagonals t;
	vector<vector<int>> ans = t.anti_diag(a);
	t.print_vv(ans);
}
#endif
