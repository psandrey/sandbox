
/*
 * Given n, generate first n rows of Pascal's triangle.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class PascalTriangle {
public:
	vector<vector<int> > solve(int n) {
		vector<vector<int>> ans;

		for (int i = 0; i < n; i++) {
			vector<int> row = vector<int>(i + 1);
			
			row[0] = 1;
			if (i > 0) {
				for (int j = 1; j < i; j++) row[j] = ans[i - 1][j - 1] + ans[i - 1][j];
				row[i] = 1;
			}

			ans.push_back(row);
		}

		return ans;
	}
};

#if 0
int main(void) {
	int n = 5;

	PascalTriangle t;
	vector<vector<int>> ans = t.solve(n);

	if (!ans.empty()) {
		int n = (int)ans.size();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j <= i; j++)
				cout << ans[i][j] << " ";
			cout << endl;
		}
	}
	
	return 0;
}
#endif