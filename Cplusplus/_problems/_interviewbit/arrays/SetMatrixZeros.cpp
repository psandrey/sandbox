
/*
 * Given an m x n matrix of 0s and 1s, if an element is 0, set its entire row and column to 0.
 * Note: Do it in place.
 *
 * Example:
 *  1 0 1
 *  1 1 1
 *  1 1 1
 *
 * On returning, the array A should be :
 *  0 0 0
 *  1 0 1
 *  1 0 1
 */

#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class SetMatrixZeros {
public:
	void set_matrix_zeros(vector<vector<int>>& a) {
		int n = (int)a.size();
		int m = (int)a[0].size();
		if (n == 0 && m == 0) return;

		// check if the first line and the first column contains any zeros
		bool zeroed_first_line = false;
		for (int j = 0; j < m; j++)
			if (a[0][j] == 0) { zeroed_first_line = true; break;}

		bool zeroed_first_col = false;
		for (int i = 0; i < n; i++)
			if (a[i][0] == 0) { zeroed_first_col = true; break; }

		// use first line and first column as markers
		for (int i = 1; i < n; i++)
			for (int j = 1; j < m; j++)
				if (a[i][j] == 0) { a[0][j] = 0; a[i][0] = 0; }

		// set matrix elements acoording to markers
		for (int i = 1; i < n; i++)
			for (int j = 1; j < m; j++)
				if (a[0][j] == 0 || a[i][0] == 0) a[i][j] = 0;

		// if necessary set the firsst line and column to zero
		if (zeroed_first_line == true)
			for (int j = 0; j < m; j++) a[0][j] = 0;

		if (zeroed_first_col == true)
			for (int i = 0; i < n; i++) a[i][0] = 0;
	}

	void print_vv(vector<vector<int>>& a) {
		int n = (int)a.size();
		for (int i = 0; i < n; i++) {
			int m = (int)a[i].size();
			for (int j = 0; j < m; j++) {
				cout << a[i][j] << " ";
			}
			cout << endl;
		}
	}
};

#if 0
int main(void) {
	vector<vector<int>> a = {
						{ 0, 1, 0 },
						{ 1, 1, 1 },
						{ 0, 1, 0 } };

	SetMatrixZeros t;
	t.set_matrix_zeros(a);
	t.print_vv(a);
}
#endif