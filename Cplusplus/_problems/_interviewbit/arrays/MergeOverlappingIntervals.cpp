
/*
 * Given a collection of intervals, merge all overlapping intervals.
 *
 * For example:
 *     Given [1,3],[2,6],[8,10],[15,18],
 *     return [1,6],[8,10],[15,18].
 *
 * Note: Make sure the returned intervals are sorted.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MergeOverlappingIntervals {
public:

	struct Interval {
		int start, end;
		Interval() : start(0), end(0) {}
		Interval(int s, int e) : start(s), end(e) {}

	};

	vector<Interval> merge(vector<Interval>& i) {
		if (i.empty() || i.size() <= 1) return i;

		struct {
			bool operator()(const Interval& a, const Interval& b) {
				return (a.start < b.start);
			}
		} comp;
		sort(i.begin(), i.end(), comp);

#if 1
		int n = (int)i.size(), k = 0;
		for (int j = 1; j < n; j++) {
			if (i[k].end < i[j].start) {
				k++;
				i[k].start = i[j].start;
				i[k].end = i[j].end;
			} else {
				i[k].start = min(i[k].start, i[j].start);
				i[k].end = max(i[k].end, i[j].end);
			}
		}
		i.erase(i.begin() + k + 1, i.end());

		return i;
#else
		int n = (int)i.size(), k = 0;
		vector<Interval> ans;
		for (int j = 1; j < n; j++) {
			if (i[k].end < i[j].start) { ans.push_back(i[k]); k = j; }
			else {
				i[k].start = min(i[k].start, i[j].start);
				i[k].end = max(i[k].end, i[j].end);
			}
		}
		ans.push_back(i[k]);

		return ans;
#endif
	}

	vector<Interval> build_i(vector<vector<int>>& a) {
		vector<Interval> i;
		for (int j = 0; j < (int)a.size(); j++)
			i.push_back(Interval(a[j][0], a[j][1]));

		return i;
	}
};

#if 0
int main(void) {
	vector<vector<int>> a = { {1,3},{2,6},{8,10},{15,18} };

	MergeOverlappingIntervals t;
	vector<MergeOverlappingIntervals::Interval> i = t.build_i(a);
	vector<MergeOverlappingIntervals::Interval> ans = t.merge(i);

	for (int j = 0; j < (int)ans.size(); j++) {
		cout << " [ " << ans[j].start << " , " << ans[j].end << "]  ";
	}

	return 0;
}
#endif