
/*
 * Given an integer array, find if an integer p exists in the array such that the number of integers greater than p
 * in the array equals to p
 * If such an integer is found return 1 else return -1.
*/

#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>
using namespace std;

class NobleInteger {
public:
	// T(n) = O(n.log(n))
	int noble(vector<int>& a) {
		if (a.empty()) return -1;

		sort(a.begin(), a.end());
		int n = (int)a.size(), g = 0;
		for (int i = n - 1; i >= 0; i--) {
			if (i < n - 1 && a[i] != a[i + 1]) g = n - 1 - i;
			if (a[i] == g) return 1;
		}

		return -1;
	}
};

#if 0
int main(void) {
	//vector<int> a = { -4, -2, 0, -1, -6 };
	vector<int> a = {4, -9, 8, 5, -1, 7, 5, 3};
	
	NobleInteger t;
	cout << t.noble(a) << endl;
}
#endif