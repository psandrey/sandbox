
/*
 * Given an index k, return the kth row of the Pascal�s triangle.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class KthRowOfPascalsTriangle {
public:
	vector<int> getRow(int k) {
		vector<int> row;

#if 1
		int x = 1;
		row.push_back(x);
		for (int j = 1; j <= k; j++) {
			x = x * (k - j + 1) / j;
			row.push_back(x);
		}
#else
		// change starting point from 0
		int x = 1;
		for (int j = 0; j <= k; j++) {
			row.push_back(x);
			x = x * (k - j) / (j + 1);
		}
#endif

		return row;
	}

};

#if 0
int main(void) {
	KthRowOfPascalsTriangle t;

	vector<int>row = t.getRow(4);
	for (auto x : row) cout << x << " ";
	
	return 0;
}
#endif