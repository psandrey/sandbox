
/*
 * Given an unsorted integer array, find the first missing positive integer.
 *
 * Example:
 *
 * Given [1,2,0] return 3,
 *   [3,4,-1,1] return 2,
 *   [-8, -7, -6] returns 1
 *
 * Note: Your algorithm should run in O(n) time and use constant space.
 */

#include <vector>
#include <iostream>
using namespace std;

class FirstMissingInteger {
public:
	int first_missing(vector<int>& a) {
		if (a.size() == 0) return 1;
		int n = a.size();

		// move negative numbers to the end of the array
		int j = n;
		for (int i = n - 1; i >= 0; i--) {
			if (a[i] <= 0) { j--; swap(a[i], a[j]); }
		}

		// mark positive numbers as indexes
		for (int i = 0; i < j; i++) {
			int x = abs(a[i]);
			if (x <= j) a[x-1] = -abs(a[x - 1]);
		}

		// find the first missing number
		int i = 0;
		for (i = 0; i < j; i++) if (a[i] >= 0) break;
		return (i+1);
	}
};

#if 0
int main(void) {
	//vector<int> a = { 3, 1, 4, 3, 5, 1, 2, 17, 4 };
	vector<int> a = { -5 };
	FirstMissingInteger t;

	cout << t.first_missing(a);

 	return 0;
}
#endif