
/*
 * Implement the next permutation, which rearranges numbers into the numerically next greater permutation of numbers.
 * If such arrangement is not possible, it must be rearranged as the lowest possible order ie, sorted in an ascending order.
 *
 * Note: The replacement must be in-place, do not allocate extra memory.
 */

#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class NextPermutation {
public:
	void next_permutation(vector<int>& a) {
		int n = (int)a.size();
		if (n == 0 || n == 1) return;

		// find the hump
		int k = n - 2;
		while (k >= 0 && a[k] > a[k + 1]) k--;
		
		if (k < 0) reverse(a.begin(), a.end());
		else {
			// find the replacement
			int j = n - 1;
			while (a[k] > a[j]) j--;
			swap(a[k], a[j]);

			// reverse the tail
			reverse(a.begin() + k + 1, a.end());
		}
	}
};

#if 0
int main(void) {
	vector<int> a = { 198, 2314, 8 };
	NextPermutation t;
	t.next_permutation(a);

	for (int x : a) cout << x << " ";
	cout << endl;

	return 0;
}
#endif