
/*
 * You are given an array A containing N integers. The special product of each ith integer in this array is defined
 *  as the product of the following:<ul>
 *
 * LeftSpecialValue: For an index i, it is defined as the index j such that A[j]>A[i] (i>j).
 *    If multiple A[j]�s are present in multiple positions, the LeftSpecialValue is the maximum value of j. 
 * RightSpecialValue: For an index i, it is defined as the index j such that A[j]>A[i] (j>i).
 *    If multiple A[j]s are present in multiple positions, the RightSpecialValue is the minimum value of j.
 * 
 * Write a program to find the maximum special product of any integer in the array.
 *  Input: You will receive array of integers as argument to function.
 *  Return: Maximum special product of any integer in the array modulo 1000000007.
 * 
 * Note: If j does not exist, the LeftSpecialValue and RightSpecialValue are considered to be 0.
 * Constraints 1 <= N <= 10^5 1 <= A[i] <= 10^9
 *
 * Note: solved based on the algorithm: Next Greater Element (using stack)
 */

#include <vector>
#include <stack>
#include <iostream>
#include <algorithm>
using namespace std;

class MAXSPPROD {
public:
	int maxspprod(vector<int>& a) {
		int n = (int)a.size();
		if (n == 0) return 0;

		vector<int> left(n);
		stack<pair<int,int>> st;
		for (int i = 0; i < n; i++) {
			left[i] = 0;
			while (!st.empty() && st.top().first <= a[i]) st.pop();
			if (!st.empty()) left[i] = st.top().second;

			st.push(make_pair(a[i],i)); 
		}

		// clear stack
		st = stack<pair<int, int>>();
		unsigned long max_prod = 0;
		for (int i = n-1; i >= 0; i--) {
			int right = 0;
			while (!st.empty() && st.top().first <= a[i]) st.pop();
			if (!st.empty()) right = st.top().second;

			st.push(make_pair(a[i], i));

			unsigned long prod = (unsigned long)(left[i]) * (unsigned long)(right);
			if (max_prod < prod) max_prod = prod;
		}

		unsigned long m = 1000000007ULL;
		return (int)(max_prod%m);
	}
};

/*
int main(void) {
	MAXSPPROD t;
	vector<int> a = { 5, 9, 6, 8, 6, 4, 6, 9, 5, 4, 9 };

	int ans = t.maxspprod(a);
	cout << endl << ans << endl;
}
*/