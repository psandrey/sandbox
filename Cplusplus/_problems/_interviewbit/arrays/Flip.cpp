
/*
 * You are given a binary string(i.e. with characters 0 and 1) S consisting of characters S1, S2, ..., SN.
 * In a single operation, you can choose two indices L and R such that 1 <= L <= R <= N and
 * flip the characters SL, SL+1, ..., SR. By flipping, we mean change character 0 to 1 and vice-versa.
 *
 * Your aim is to perform ATMOST one operation such that in final string number of 1s is maximised.
 * If you don�t want to perform the operation, return an empty array. Else, return an array
 * consisting of two elements denoting L and R. If there are multiple solutions,
 * return the lexicographically smallest pair of L and R.
 *
 * Example:
 * S = 010
 *
 * Pair of [L, R] | Final string
 * _______________|_____________
 * [1 1]          | 110
 * [1 2]          | 100
 * [1 3]          | 101
 * [2 2]          | 000
 * [2 3]          | 001
 * [3 3]          | 011
 *
 * We see that [1, 1], [1, 3] and [3, 3] give same number of 1s in final string.
 * So, we return [1, 1].
 *
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class Flip {
public:
	vector<int> flip(string a) {
		if (a.empty()) return vector<int>();
#if 1
		int s = -1, l = -1, r = -1;
		int M = INT_MIN, m = 0, pm = -1;
		for (int i = 0; i < (int)a.size(); i++) {
			int c = (a[i] == '0' ? 1 : -1);

			m = max(c, m + c);
			if (m > 0) {
				if (pm == -1) s = i;
				if (M < m) { l = s; r = i; M = m; }
			}

			pm = m;
		}

		vector<int>ans;
		if (M > 0) { ans.push_back(l + 1); ans.push_back(r + 1); }
		return ans;
#else
		int k = 0, c = -1, b = 0, e = 0, maxc = 0;
		for (int j = 0; j < (int)a.size(); j++) {
			if (c == -1) { k = j; c = 0; }

			if (a[j] == '0') c++;
			else c--;

			if (maxc < c) { b = k; e = j; maxc = c; }
		}


		if (maxc > 0) { return (vector<int>() = {b + 1, e + 1}); }
		return vector<int>();
#endif
	}
};

#if 0
int main(void) {
	string s = "1101010001"; // l = 3, r = 9
	//string s = "111";


	Flip t;
	vector<int> ans = t.flip(s);
	if (ans.size() != 0) cout << ans[0] << "  " << ans[1] << endl;
	return 0;
}
#endif