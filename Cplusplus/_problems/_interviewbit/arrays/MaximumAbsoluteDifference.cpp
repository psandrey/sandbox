
/*
 * You are given an array of N integers, A1, A2 ,..., AN. Return maximum value of f(i, j) for all 1 <= i, j <= N.
 * f(i, j) is defined as |A[i] - A[j]| + |i - j|, where |x| denotes absolute value of x.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MaximumAbsoluteDifference {
public:
	int max_abs_diff(vector<int>& a) {
		if (a.size() == 0) return 0;

		int max_pos = INT_MIN, min_pos = INT_MAX,
			max_neg = INT_MIN, min_neg = INT_MAX;

		for (int i = 0; i < (int)a.size(); i++) {
			max_pos = max(max_pos, a[i] + i);
			min_pos = min(min_pos, a[i] + i);

			max_neg = max(max_neg, a[i] - i);
			min_neg = min(min_neg, a[i] - i);
		}

		return max(max_pos - min_pos, max_neg - min_neg);
	}
};

#if 0
int main(void) {
	vector<int> a = { -39, -24, 82, 95, 91, -65, 16, -76, -56, 70 };
	
	MaximumAbsoluteDifference t;
	cout << t.max_abs_diff(a);

	return 0;
}
#endif