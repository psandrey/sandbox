
/*
 * A hotel manager has to process N advance bookings of rooms for the next season.
 * His hotel has K rooms. Bookings contain an arrival date and a departure date.
 * He wants to find out whether there are enough rooms in the hotel to satisfy the demand.
 * Write a program that solves this problem in time O(N log N).
 *
 * Input:
 *   First list for arrival time of booking.
 *   Second list for departure time of booking.
 *   Third is K which denotes count of rooms.
 *
 * Output:
 *   A boolean which tells whether its possible to make a booking.
 *   Return 0/1 for C programs.
 *   O -> No there are not enough rooms for N booking.
 *   1 -> Yes there are enough rooms for N booking.
 * 
 * Example :
 * Input :
 * 		Arrivals :   [1 3 5]
 * 		Departures : [2 6 8]
 * 		K : 1
 *
 * Return : False / 0
 * At day = 5, there are 2 guests in the hotel. But I have only one room.
 */

#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

bool compare_bookings(int x, int y) {
	return (abs(x) < abs(y));
};

class HotelBookingsPossible {
public:
	bool booking_possible(vector<int>& arrive, vector<int>& depart, int k) {
		int n = (int)arrive.size();
		if (n == 0) return true;

		sort(arrive.begin(), arrive.end());
		sort(depart.begin(), depart.end());

		// each time someone depart (free a room) k++
		// each time someone arrives (occupies a room) k--
		// if the arrival time = departure time, then the rooms remains unchanged
		int i = 0, j = 0;
		while (i < n && j < n) {
			if (arrive[i] < depart[j]) { // occupies a room
				k--; i++;
			} else if (arrive[i] > depart[j]) { // free a room
				k++; j++;
			} else { i++; j++; } // arrives and depart in the same day

			if (k < 0) return false; // not enough rooms
		}

		return true;
	}
};

#if 0
int main(void) {
	vector<int> arrivals = { 1, 3, 5 };
	vector<int> departures = { 2, 6, 8 };

	HotelBookingsPossible t;
	cout << t.booking_possible(arrivals, departures, 1);

	return 0;
}
#endif