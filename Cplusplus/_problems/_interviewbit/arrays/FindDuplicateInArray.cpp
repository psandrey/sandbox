
/*
 * Given a read only array of n + 1 integers between 1 and n, find one number
 *  that repeats in linear time using less than O(n) space and traversing the stream
 *  sequentially O(1) times.
 *
 *  Note: Tortoise and hare algorithm.
 *  See here the explanation: http://keithschwarz.com/interesting/code/?dir=find-duplicate
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class FindDuplicateInArray {
public:
	int repeatedNumber(const vector<int>& a) {
		if (a.empty() || a.size() == 1) return -1;

		int t = 0, h = 0;
		while (t != h || t == 0) { t = a[t]; h = a[a[h]]; }

		h = 0;
		while (t != h) { t = a[t]; h = a[h]; }

		return t;
	}
};

#if 0
int main(void) {
	vector<int> a = { 3, 4, 1, 4, 1 };
	
	FindDuplicateInArray t;
	cout << t.repeatedNumber(a) << endl;

	return 0;
}
#endif