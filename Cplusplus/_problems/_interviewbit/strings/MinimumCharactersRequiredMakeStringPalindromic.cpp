
/*
 * You are given a string. The only operation allowed is to insert characters in the beginning of the string. 
 * How many minimum characters are needed to be inserted to make the string a palindrome string
 *
 * Example:
 *   Input: ABC
 *   Output: 2
 *   Input: AACECAAAA
 *   Output: 2
 */

#include <string>
#include <vector>
#include <iostream>
using namespace std;

class MinimumCharactersRequiredMakeStringPalindromic {
public:
	// T(n) = O(n^2)
	int make_palindrome_brute_force(string s) {
		int n = (int)s.size();
		if (n == 0) return 0;

		int i = 0, end = n - 1;
		int j = end;
		while (i <= j) {
			if (s[i] == s[j]) { i++; j--; }
			else { i = 0; j = --end; }
		}

		return n - (end + 1);
	}

	// KMP:	lpt trick
	// T(n) = O(n)
	int make_palindrome(string s) {
		int n = (int)s.size();
		if (n == 0) return 0;
		vector<int> lpt(n+1);

		// using longest prefix-suffix trick
		int i = 0, j = n;
		while (j > 0) {
			while (i > 0 && s[j - 1] != s[i]) i = lpt[i - 1];
			if (s[j - 1] == s[i]) i++;
			lpt[n - j + 1] = i;
			j--;
		}

		return (n - lpt[n]);
	}
};

/*
int main(void) {
	string s = "aabbaaa";
	//string s = "aabbaa";
	//string s = "abc";

	MinimumCharactersRequiredMakeStringPalindromic t;
	cout << t.make_palindrome_brute_force(s) << endl;
	cout << t.make_palindrome(s) << endl;

	return 0;
}
*/