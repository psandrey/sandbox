
/*
 * Given a roman numeral, convert it to an integer.
 *
 * Input is guaranteed to be within the range from 1 to 3999.
 *
 * Read more details about roman numerals at Roman Numeric System
 */

#include <unordered_map>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class RomanToInteger {
public:
	int roman_to_integer(string s) {
		int n = (int)s.size();
		
		unordered_map<string, int> map{ 
			make_pair("M",1000),
			make_pair("CM",900),
			make_pair("D",500),
			make_pair("CD",400),
			make_pair("C",100),
			make_pair("XC",90),
			make_pair("L",50),
			make_pair("XL",40),
			make_pair("X",10),
			make_pair("IX",9),
			make_pair("V",5),
			make_pair("IV",4),
			make_pair("I",1)};

		int j = 0, ans = 0;
		string prev;
		while (j < n) {
			string sgl(1, s[j]);
			int digit = map[sgl];

			if (!prev.empty()) {
				string dbl = prev + sgl;
				if (map.find(dbl) != map.end()) {
					digit = map[dbl];
					ans -= map[prev];
				}
			}
			ans += digit;

			prev = sgl; j++;
		}

		return ans;
	}
};

#if 0
int main(void) {
	RomanToInteger t;

	string s = "MMDCCCCVIII";
	cout << t.roman_to_integer(s);

	return 0;
}

#endif