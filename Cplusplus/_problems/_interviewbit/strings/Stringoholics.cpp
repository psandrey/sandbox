
/*
 * You are given an array A consisting of strings made up of the letters �a� and �b� only.
 * Each string goes through a number of operations, where:
 *
 * 1.	At time 1, you circularly rotate each string by 1 letter.
 * 2.	At time 2, you circularly rotate the new rotated strings by 2 letters.
 * 3.	At time 3, you circularly rotate the new rotated strings by 3 letters.
 * 4.	At time i, you circularly rotate the new rotated strings by i % length(string) letters.
 *
 * Eg: String is "abaa"
 *
 * 1.	At time 1, string is "baaa", as 1 letter is circularly rotated to the back
 * 2.	At time 2, string is "aaba", as 2 letters of the string "baaa" is circularly rotated to the back
 * 3.	At time 3, string is "aaab", as 3 letters of the string "aaba" is circularly rotated to the back
 * 4.	At time 4, string is again "aaab", as 4 letters of the string "aaab" is circularly rotated to the back
 * 5.	At time 5, string is "aaba", as 1 letters of the string "aaab" is circularly rotated to the back
 * After some units of time, a string becomes equal to it�s original self.
 * Once a string becomes equal to itself, it�s letters start to rotate from the first letter again (process resets).
 * So, if a string takes t time to get back to the original, at time t+1 one letter will be rotated and the string 
 * will be it�s original self at 2t time.
 * You have to find the minimum time, where maximum number of strings are equal to their original self.
 * As this time can be very large, give the answer modulo 109+7.
 *
 * Input:
 *  A: Array of strings.
 * Output:
 *  Minimum time, where maximum number of strings are equal to their original self.
 *
 * Constraints:
 * 1 <= size(A) <= 10^5
 * 1 <= size of each string in A <= 10^5
 * Each string consists of only characters 'a' and 'b'
 * Summation of length of all strings <= 10^7
 *
 * Example:
 *   Input: A: [a,ababa,aba]
 *   Output: 4
 *
 * String 'a' is it's original self at time 1, 2, 3 and 4.
 * String 'ababa' is it's original self only at time 4. (ababa => babaa => baaba => babaa => ababa)
 * String 'aba' is it's original self at time 2 and 4. (aba => baa => aba)
 *
 * Hence, 3 strings are their original self at time 4.
 */

/*
 1. KMP : longest prefix-sufix table
 2. compute periods for every string
 3. compute LCM
 4. compute power modulo M
*/

#include <string>
#include <algorithm>
#include <unordered_map>
#include <vector>
#include <iostream>
using namespace std;

class Stringoholics {
public:
	unsigned long long mod = 1000000007ULL;
	unsigned long long pow_mod(unsigned int a, unsigned int p) {
		if (p == 0) return 1;
		else if (p == 1) return a;

		unsigned long long ans = 1, x = a;
		while (p > 0) {
			if ((p & 1) == 1)
				ans = (ans * x) % mod;

			x = (x * x) % mod;
			p >>= 1;
		}

		return ans;
	}

	void update_lcm_map(unordered_map<int, int>& lcm_map, int num) {
		
		// if num is even
		int prim = 2;
		int pow = 0;
		while (num % 2 == 0) {
			num = num / 2;
			pow++;
		}

		if (pow > 0) {
			int p = 0;
			if (lcm_map.find(prim) != lcm_map.end()) p = lcm_map[prim];
			p = max(p, pow);
			lcm_map[prim] = p;
		}

		// if num is odd
		for (prim = 3; prim <= sqrt(num); prim += 2) {
			pow = 0;
			while (num % prim == 0) {
				num = num / prim;
				pow++;
			}

			if (pow > 0) {
				int p = 0;
				if (lcm_map.find(prim) != lcm_map.end()) p = lcm_map[prim];
				p = max(p, pow);
				lcm_map[prim] = p;
			}
		}

		// if num is prime
		if (num > 2) {
			int p = 0;
			if (lcm_map.find(num) != lcm_map.end()) p = lcm_map[num];
			p = max(p, 1);
			lcm_map[num] = p;
		}
	}

	unsigned long long do_lcm_mod(vector<int>& a) {
		unordered_map<int, int> lcm_map;
		for (int x : a) update_lcm_map(lcm_map, x);

		//for (pair<int, int> x : lcm_map) {
		//	cout << "( " << x.first << " , " << x.second << " ) ";
		//}

		unsigned long long ans = 1;
		for (pair<int, int> x : lcm_map) {

			int base = x.first;
			int pow = x.second;
			//cout << base << " ^ " << pow << endl;
			unsigned long long p = pow_mod(base, pow);

			ans = (ans * p) % mod;
		}

		return ans;
	}

	int build_lpt(string a) {
		int m = (int)a.size();
		vector<int> lpt(m);

		int longest_prefix_suffix = 0;
		int i = 0, j = 0;
		while (j + 1 < m) {
			// find the first good suffix-preffix that we can try to expand for the current substring
			while (i > 0 && a[j + 1] != a[i]) i = lpt[i - 1];

			// can we expand the current suffix-preffix for current substring
			if (a[j + 1] == a[i]) i++;

			// set the current good suffix-preffix for current substring
			lpt[++j] = i;
			longest_prefix_suffix = max(longest_prefix_suffix, i);
		}

		return longest_prefix_suffix;
	}

	int solve(vector<string> a) {
		vector<int> periods;

		// get lengths of all strings
		for (string s : a) {
			int longest_prefix_suffix = build_lpt(s);
			int n = (int)s.size();
			
			// check if the string is periodical: if it is, then cut everything but the first preffix.
			// * this holds because after consuming the first preffix, then for sure
			// * we are back on the original string.
			if (n % (n - longest_prefix_suffix) == 0) n -= longest_prefix_suffix;

			// compute period when string s becomes s again
			// * like a watch: if instead incrementing hour by hour, we increment:
			// * +1 hour, +2 hours, +3hours... until becomes a multiple of n, which basically means
			// * time % n == 0
			unsigned long long total_time = 0;
			int time_unit = 1;
			do {
				total_time += time_unit;
				time_unit++;
			} while (total_time % ((unsigned long long)n) != 0L);

			periods.push_back(time_unit-1); // save the total time units
		}

		//for (int x : periods) cout << x << " ";

		// compute LCM (Least Common Multiple)
		unsigned long long lcm = do_lcm_mod(periods);

		return ((int)lcm);
	}
};

#if 0
int main(void) {
	//vector<string> a = { "a", "ababa", "aba" };
	vector<string> a = {
			"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
			"babaaaabbaba",
			"baaaaababaabbaaaaabbbbbbbaabbaaaababbabaababaabaaabbbaaaaa",
			"abaabb",
			"aababbbbabbaaaabbaaaaaaaababbbabbbbaabbaabaabbaabaababbbababaababaabbababaaabaaaab",
			"babaaaaababbbbbabbbbabbaaabaaaababbabbaabbbabbaaaabbbabaaaabaaaababb",
			"bbbbbbaaaaaabbbbbbababaabaabbbbbaaabbabbaabbbbaaaaaababbabaaabbabbabbbabbaabbabbbbaabbbaaaaabbabaaab",
			"aabaaabaabbbbababbabbabaaaababbababbbabbbbaabbaaaaababbbbbababbbbaaababababab",
			"aaaababbbaabbbabaabb",
			"ababaaaabbabbbbaaabbababbbabbbabaaa",
			"aaabaabbbabaa",
			"baababbababbbbbabbabaabbabbbbbaaaabaaaababaa",
			"babaa",
			"abbabababbbbbbbbbbbaaaaaababbababaaabbbaaaabbbababbabaabbaabbbbaabbbaabbababababaabaaabbaaabbba",
			"ababbaabbaaabbbabaabababbbabaaabbbaababaaa",
			"abbaaaababbbbaaabaaaabaaaaabaababbabbaababbbabbbbbbbbbabbaabaaabbaaababbbaa",
			"bbabababaabbabbabbbabbaababbabaaabbbababab",
			"abbbaaabaab",
			"bbaaabbaaabbaabbabababa",
			"aabaabaaaaaaabaabbbaaababbbbbbababbaabababbaaaaabbabbbabbbaababbaabababbbbbbbbbaabaab",
			"babbaaabbabbaabaaabbb",
			"bbabaabba",
			"baabaaaaabbaaaaaabbbbaaaabababa",
			"babbaababaaba",
			"baabaabaabbababbaabbabbbbbabaaaabbbbbabbabbbbbabbbabaabbbbabbbbaaabbbbabababaaaababbaaabbabb",
			"abbbbaaaabaabbabbaababaabbababbbaaabbabbbbbaaabbabbaabbb",
			"bababaaaaabababbabbaabababbbaabbaabaabaabbabbbababbaaabababbababbbb",
			"abaaabbbabbbaabba",
			"bbbbaaaabbbababaabbbababaaaababbaaaaaabbbabbaababababbba",
			"bababaabaaaabbaabbababbaabbaabaabbaaaaaaaababbaaaaaabbaaabaabaaababbababbbbaabbabbabaabab",
			"aabbbabaaabababaabbbbaabbabaaabbbaaabbabbbbabaabbbbaabbbaaaaabbbabbbbb",
			"aabbbbbbabaabbbabbaababbababaabaaababbbbabbbaababaaaabbaaabaaabaaaabbbabababbab",
			"abaaaaababbabaabbbaaaaabbaaaabaaaaaaaababbaabbbaabbabbbabbaaaaaab",
			"bbbaabbabbbbbbaaaabbabbbbbbbaaabaababbaaaabbbaababbaaabbbbbbbbabbabababbaaabaabaaabaaaabbbbbabaabaaa",
			"bbaaabaaabaaabaabaaabbaabaabbabaabaabbababaaaaabaabbbbaba",
			"abaababaaabbabaabaabbbaaabbaabababbabaaabbbbabbbbbaaaaa",
			"abba",
			"abbbababbaaabbaaabbbabaabbababaaabbbbaaaaababbabbaabbabbbaaabaabbaaaaabaaaabbbaabbbabbbbbbbabb",
			"bbabbaaabaaaabbaaaabbbaaaababbbaabaaaaab",
			"abbaabaabbaaaaaaaabbaabbabbababaaaaaaabbabaabaabbbabbaabbaababbaabbaba",
			"bbbbaababbaaaaaaaaabbbabbbabaabababaababaababa",
			"baaabaabbbbbbaabbabbbabaaaaababaabaababbbaaaaaaaabbbbbabbabaaaaaaaabababaabaababaaabbaaaaaaaaabaa",
			"aababbbabbaaaaababbabaababbbbbbbbaaabbaaaaabbaabbbba",
			"ababababaaaaaabbbabbaaababaabb",
			"bababbaababaabbbabbaab",
			"baababababbaaaaabbbbbbbbbaabababb",
			"bbbbb",
			"aabaabbbaabababbababaaaaabbbbaababaabbabbbbbbaabbaaabbababbbabbabbbaabbbab",
			"bbaabbbbaabbaaaaaabbbaabababbbaabaaabbbbbabaababbbaababbbaaabaaabaaaababbbbaabbaababb",
			"aaababbaaaaabaabababbabaabbbbabbaba" };
		
	Stringoholics t;
	int ans = t.solve(a);
	cout << ans << endl;
	
	//vector<int> aa = {4, 8, 12};
	//cout << t.do_lcm(aa) << endl;
	
	return 0;
}
#endif