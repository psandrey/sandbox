
/*
 * The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this:
 * (you may want to display this pattern in a fixed font for better legibility)
 *
 * P.......A........H.......N
 * ..A..P....L....S....I...I....G
 * ....Y.........I........R
 * And then read line by line: PAHNAPLSIIGYIR
 * Write the code that will take a string and make this conversion given a number of rows:
 *
 * string convert(string text, int nRows);
 * convert("PAYPALISHIRING", 3) should return "PAHNAPLSIIGYIR"
 * Example 2 : ABCD, 2 can be written as
 * A....C
 * ...B....D
 * and hence the answer would be ACBD.
 */

#include <algorithm>
#include <string>
#include <iostream>
using namespace std;

class ZigzagString {
public:
	string zigzag_v1(string a, int n) {
		int m = (int)a.size();
		if (n == 0 || n == 1) return a;
		string ans;

		int above = 0, below = n - 1;
		int start = 0;
		for(int start = 0; start < n; start++) {
			int inc = (below != 0 ? below : above);
			int j = start;
			while (j < m) {
				ans.push_back(a[j]);
				j = j + 2 * inc;
				if (below != 0 && above != 0) inc = (inc == below ? above : below);
			}
			above++; below--;
		}

		return ans;
	}

	string zigzag_v2(string a, int n) {
		if (n <= 1) return a;

		int len = (int)a.length();
		string* str = new string[n];

		int row = 0, step = 1;
		for (int i = 0; i < len; ++i) {
			str[row].push_back(a[i]);

			if (row == 0) step = 1;
			else if (row == n - 1) step = -1;

			row += step;
		}

		a.clear();
		for (int j = 0; j < n; ++j) a.append(str[j]);
		delete[]str;

		return a;
	}
};

#if 0
int main(void) {

	string a = "ABCDEFGHIJ";
	int n = 3;

	ZigzagString t;
	cout << t.zigzag_v1(a, n) << endl;
	cout << t.zigzag_v2(a, n) << endl;

	return 0;
}
#endif