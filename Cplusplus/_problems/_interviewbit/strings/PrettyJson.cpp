
/*
 * Pretty print a json object using proper indentation.
 *
 * Every inner brace should increase one indentation to the following lines.
 * Every close brace should decrease one indentation to the same line and the following lines.
 * The indents can be increased with an additional �\t�
 * 
 * Example 1:
 *   Input : {A:"B",C:{D:"E",F:{G:"H",I:"J"}}}
 *
 * Output :
 *	{
 *		A:"B",
 *		C:
 *		{
 *			D:"E",
 *			F:
 *	 		{
 * 				G:"H",
 * 				I:"J"
 * 			}
 *		}
 *	}
 *
 * [] and {} are only acceptable braces in this case.
 *
 * Assume for this problem that space characters can be done away with.
 * Your solution should return a list of strings, where each entry corresponds to a single line.
 * The strings should not have �\n� character in them.
 *
 */

#include <vector>
#include <algorithm>
#include <string>
#include <iostream>
using namespace std;

class PrettyJson {
public:
	vector<string> pretty_json(string a) {
		vector<string> ans;
		string line, tab;
		int n = (int)a.size();
		for (int i = 0; i < n; i++) {
			if (a[i] == ' ') continue;

			// line delimitators
			if (a[i] == ',') {
				line += a[i];
				ans.push_back(line);
				line = string();
				continue;
			}

			// deal with open brackets and increase tab
			if (a[i] == '{' || a[i] == '[') {
				if (!line.empty()) ans.push_back(line);
				line = tab; line.push_back(a[i]); ans.push_back(line);
				tab.push_back('\t'); line = string();
				continue;
			}

			// deal with close brackets and decrease tab
			if (a[i] == '}' || a[i] == ']') {
				if (!line.empty()) ans.push_back(line);
				tab.pop_back();
				line = tab; line.push_back(a[i]);

				// cut spaces if they are
				while (i < n && a[i] == ' ') i++;
				// add , if present
				if (i < n && a[i + 1] == ',') { line.push_back(a[i + 1]); i++; }
				ans.push_back(line);

				line = string();
				continue;
			}

			if (line.empty()) line = tab;
			line.push_back(a[i]);
		}

		return ans;
	}
};

#if 0
int main(void) {
	PrettyJson t;
	string a = "{A:\"B\",C:{D:\"E\",F:{G:\"H\",I:\"J\"}}}";
	vector<string> ans = t.pretty_json(a);
	for (string line : ans)
		cout << line << endl;
	return 0;
}
#endif