
/*
 * Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.
 *
 * Example: "A man, a plan, a canal: Panama" is a palindrome.
 * "race a car" is not a palindrome.
 *
 * Return 0 / 1 ( 0 for false, 1 for true ) for this problem
 */

#include <algorithm>
#include <string>
#include <iostream>
#include <cctype>
using namespace std;

class PalindromeString {
public:

	// C++ has: isalnum: is alpha numeric char
	bool is_alpha_numeric(char x) {
		if (tolower(x) >= 97 && tolower(x <= 122)
			|| (x >= 48 && x <= 57)) return true;

		return false;
	}

	int is_palindrome(string s) {
		int n = (int)s.size();
		if (n == 0) return 1;

		int i = 0, j = n - 1;
		while (i < j) {
			while (i < j  && is_alpha_numeric(s[i]) == false)i++;
			while (i < j && is_alpha_numeric(s[j]) == false)j--;

			if (tolower(s[i]) != tolower(s[j])) return 0;
			i++; j--;
		}

		return 1;
	}
};

/*
int main(void) {
	string s = "A man, a plan, a canal: Panama";

	PalindromeString t;
	cout << t.is_palindrome(s);

	return 0;
}
*/