
/*
 * Implement strstr: search needle in haystack.
 */

#include <string>
#include <vector>
#include <iostream>
using namespace std;

class StrStr {
public:
	vector<int> build_lpt(string p) {
		int m = (int)p.size();
		vector<int> lpt(m);

		int i = 0, j = 0;
		while (j + 1 < m) {
			// find the first good suffix-preffix that we can try to expand for the current substring
			while (i > 0 && p[j + 1] != p[i]) i = lpt[i - 1];

			// can we expand the current suffix-preffix for current substring
			if (p[j + 1] == p[i]) i++;

			// set the current good suffix-preffix for current substring
			lpt[++j] = i;
		}

		return lpt;
	}

	int strstr(string t, string p) {
		int n = (int)t.size();
		int m = (int)p.size();
		if (n == 0 || m == 0 || m > n) return -1;

		// KMP
		vector<int> lpt = build_lpt(p);
		int i = 0, k = 0;
		while (i < n) {
			// position where we try to expand the current substring-pattern
			while (k > 0 && t[i] != p[k]) k = lpt[k - 1]; // while different, we look at the previouse suffix-preffix
			if (t[i] == p[k]) k++;
			if (k == m) return (i - m + 1);
			i++;
		}

		return -1;
	}
};

/*
int main(void) {

	//string t = "abaababxx";
	//string p = "ababx";

	string p = "b";
	string t = "baba";
	
	StrStr tst;
	cout << tst.strstr(t, p) << endl;
	
	return 0;
}
*/