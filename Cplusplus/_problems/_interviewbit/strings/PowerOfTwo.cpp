
/*
 * Find if Given number is power of 2 or not.
 * More specifically, find if given number can be expressed as 2^k where k >= 1.
 *
 * Input: number length can be more than 64, which mean number can be greater than 2 ^ 64 (out of long long range)
 * Output: return 1 if the number is a power of 2 else return 0
 *
 * Example:
 *   Input : 128
 *   Output : 1
 */

#include <algorithm>
#include <string>
#include <iostream>
using namespace std;

class PowerOfTwo {
public:
	string divide_by2(string a) {
		string ans;

		int n = a.length();
		int carry = 0, i = 0;
		while (i < n) {
			int digit = a[i] - '0';
			digit = digit + (carry * 10);
			carry = digit % 2;
			int temp = digit / 2;
			ans.push_back(temp + '0');
			i++;
		}

		// remove leading zeroes, basically each digit by continuously divide
		// it will become 0, thus zeroes will keep gather, so remove to prevent this.
		while (ans.length() > 0 && ans[0] == '0') ans.erase(ans.begin());
		
		return ans;
	}

	bool is_one(string a) {
		if (a.size() > 1) return 0;
		return (a[0] == '1');
	}
	
	bool is_even(string a) {
		int digit = a[a.size() - 1] - '0';

		return (!(digit & 1));
	}

	int is_power_of_two(string a) {
		int n = (int)a.size();
		if (n == 0) return 1;
		if (n == 1) {
			if (a[0] == '1') return 0;
			if (a[0] == '2' || a[0] == '4' || a[0] == '8') return 1;
		}

		while (is_one(a) == false && is_even(a) == true) {
			a = divide_by2(a);
		}
		
		return is_one(a);
	}
};

#if 0
int main(void) {

	string s = "1125899906842624";

	//string s = "32";
	PowerOfTwo t;
	cout << t.is_power_of_two(s);

	return 0;
}
#endif