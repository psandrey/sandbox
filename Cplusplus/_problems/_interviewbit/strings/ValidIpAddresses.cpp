
/*
 * Given a string containing only digits, restore it by returning all possible valid IP address combinations.
 *
 * A valid IP address must be in the form of A.B.C.D, where A,B,C and D are numbers from 0-255. The numbers cannot be 0 prefixed unless they are 0.
 *
 * Example:
 * Given �25525511135�,
 * return [�255.255.11.135�, �255.255.111.35�]. (Make sure the returned strings are sorted in order)
 */

#include <vector>
#include <string>
#include <iostream>
using namespace std;

class ValidIpAddresses {
public:
	vector<string> valid_ip_addr(string s) {
		vector<string> ans;
		int n = (int)s.size();
		if (n == 0) return ans;
	
		int i, j, k, l;
		for (i = 1; i < 4; i++) {
			for (j = 1; j < 4; j++) {
				for (k = 1; k < 4; k++) {
					for (l = 1; l < 4; l++) {
						int ip_len = i + j + k + l;
						if (ip_len != n) continue;

						string a = s.substr(0        , i);
						string b = s.substr(i        , j);
						string c = s.substr(i + j    , k);
						string d = s.substr(i + j + k, l);
						
						int aa = atoi(a.c_str());
						int bb = atoi(b.c_str());
						int cc = atoi(c.c_str());
						int dd = atoi(d.c_str());
						int len = (int)(to_string(aa) + to_string(bb) + to_string(cc) + to_string(dd)).size();
						
						if (len != n || aa > 255 || bb > 255 || cc > 255 || dd > 255) continue; 
						
						string ip = to_string(aa) + "." + to_string(bb) + "." + to_string(cc) + "." + to_string(dd);
						ans.push_back(ip);
					}
				}
			}
		}

		return ans;
	}
};

/*
int main(void) {
	string s = "0100100";

	ValidIpAddresses t;
	vector<string> ans = t.valid_ip_addr(s);
	for (string ip : ans) cout << ip << endl;

	return 0;
}
*/