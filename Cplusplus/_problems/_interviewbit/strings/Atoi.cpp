
/*
 * Implement atoi to convert a string to an integer.
 *
 * Example :
 *   Input : "9 2704"
 *   Output : 9
 * Note: There might be multiple corner cases here. Clarify all your doubts using �See Expected Output�.
 *
 * Questions:
 *  Q1. Does string contain whitespace characters before the number?
 *    A. Yes
 *  Q2. Can the string have garbage characters after the number?
 *    A. Yes. Ignore it.
 *  Q3. If no numeric character is found before encountering garbage characters, what should I do?
 *    A. Return 0.
 *  Q4. What if the integer overflows?
 *    A. Return INT_MAX if the number is positive, INT_MIN otherwise.
 */

#include <algorithm>
#include <iostream>
using namespace std;

class Atoi {
public:
	void bound_result(long long int x, bool* overflow_n, bool* overflow_p) {
		(*overflow_n) = false;
		(*overflow_p) = false;

		long long int overflow_neg = -(1LL << 31LL);
		long long int overflow_pos = ((1LL << 31LL) - 1LL);
		if (x > overflow_pos) { (*overflow_p) = true;  return; }
		if (x < overflow_neg) { (*overflow_n) = true;  return; }
	}

	bool is_numeric(char x) {
		if (x >= '0' && x <= '9') return true;
		return false;
	}

	int atoi(string s) {
		int n = (int)s.size();
		if (n == 0) return 0;
		int k = 0;

		// cut whitespaces
		while (k < n && s[k] == ' ') k++;

		// if the first char is not numeric or sign then, return 0
		if (k < n && is_numeric(s[k]) == false && s[k] != '-' && s[k] != '+') return 0;

		// get the sign
		long long int sign = 1;
		if (k < n && s[k] == '-') { sign = -1; k++; }
		if (k < n && s[k] == '+') k++;

		// construct number
		long long int ans = 0;
		while (k < n) {
			if (!is_numeric(s[k])) break;
			int digit = s[k] - '0';
			ans = ans * 10 + digit;

			bool overflow_neg = false, overflow_pos = false;
			bound_result(ans*sign, &overflow_neg, &overflow_pos);

			if (overflow_neg) return INT_MIN;
			else if (overflow_pos) return INT_MAX;

			k++;
		}

		return ((int)(ans*sign));
	}
};

/*
int main(void) {

	Atoi t;
	cout << t.atoi("-88297 248252140B12 37239U4622733246I218 9 1303 44 A83793H3G2 1674443R591 4368 7 97");

	return 0;
}
*/