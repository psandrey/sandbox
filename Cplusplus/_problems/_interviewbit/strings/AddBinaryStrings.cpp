
/*
 * Given two binary strings, return their sum (also a binary string).
 *
 * Example:
 *   a = "100"
 *   b = "11"
 *   Return a + b = �111�.
 */

#include <algorithm>
#include <string>
#include <iostream>
using namespace std;

class AddBinaryStrings {
public:
	string add_bin_strings(string a, string b) {
		int n = (int)a.size();
		int m = (int)b.size();
		if (n == 0) return b;
		if (m == 0) return a;
		string ans;

		int i = n - 1, j = m - 1;
		int c = 0, s, d;
		while (i >= 0 || j >= 0) {
			s = 0;

			if (i >= 0) { s += (a[i] - '0'); i--; }
			if (j >= 0) { s += (b[j] - '0'); j--; }
			s += c;

			d = s % 2; c = s / 2;
			ans = to_string(d) + ans;
		}

		if (c == 1) ans = to_string(1) + ans;

		return ans;
	}
};

#if 0
int main(void) {
	string a = "1010110111001101101000";
	string b = "1000011011000000111100110";

	//string a = "111"; // 7
	//string b = "101"; // 5

	AddBinaryStrings t;
	cout << t.add_bin_strings(a, b);
	return 0;
}
#endif