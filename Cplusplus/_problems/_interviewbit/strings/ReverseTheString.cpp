
/*
 * Given an input string, reverse the string word by word.
 *
 * Example:
 *
 * Given s = "the sky is blue", return "blue is sky the".
 *
 * Note: A sequence of non-space characters constitutes a word.
 * Note: Your reversed string should not contain leading or trailing spaces, even if it is present in the input string.
 * Note: If there are multiple spaces between words, reduce them to a single space in the reversed string.
 */

#include <string>
#include <vector>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <iostream>
using namespace std;

class ReverseTheString {
public:
	void reverse_str(string& s) {
		int n = (int)s.size();
		if (n == 0) return;

		string ans;
		int i = 0, j = 0;
		while (j < n) {

			// cut spaces
			while (j < n && s[j] == ' ') j++;
			if (j == n) break;

			// next word
			i = j; while (j < n && s[j] != ' ') j++;

			// add to answer
			if (ans.size() == 0) ans = s.substr(i, j - i);
			else ans = s.substr(i, j - i) + " " + ans;
		}

		s = ans;
	}

	void reverse_str_strem(string& s) {
		int n = (int)s.size();
		if (n == 0) return;

		istringstream iss(s);
		vector<string> v(istream_iterator<string>{iss}, istream_iterator<string>());
		reverse(v.begin(), v.end());
		string ans;
		for (int i = 0; i < (int)v.size(); i++) {
			if (i != 0) ans += " ";
			ans += v[i];
		}
		s = ans;
	}
};

/*
int main(void) {
	string s = "      the sky  is      blue     ";
	
	ReverseTheString t;
	t.reverse_str(s);
	cout << s;

	return 0;
}
*/