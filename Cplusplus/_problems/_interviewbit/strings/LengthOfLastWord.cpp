
/*
 * Given a string s consists of upper/lower-case alphabets and empty space characters ' ',
 *  return the length of last word in the string.
 *
 * If the last word does not exist, return 0.
 *
 * Note: A word is defined as a character sequence consists of non-space characters only.
 *
 * Example: Given s = "Hello World", return 5 as length("World") = 5.
 */

#include <string>
#include <iostream>
using namespace std;

class LengthOfLastWord {
public:
	int len_last_word(string s) {
		int n = (int)s.size();
		if (n == 0) return 0;

		int j = n - 1;
		while (j >= 0 && s[j] == ' ') j--;
		if (j < 0) return 0;

		int end = j;
		while (j >= 0 && s[j] != ' ') j--;
		
		return (end - j);
	}
};

/*
int main(void) {
	//string s = "Hello World";
	string s = "    ";

	LengthOfLastWord t;
	cout << t.len_last_word(s);

	return 0;
}
*/