
/*
 * Validate if a given string is numeric.
 *
 * Examples:
 *   "0" => true
 *   " 0.1 " => true
 *   "abc" => false
 *   "1 a" => false
 *   "2e10" => true
 *   Return 0 / 1 ( 0 for false, 1 for true ) for this problem
 *
 * Notes:
 *  1. Is 1u ( which may be a representation for unsigned integers valid?
 *     For this problem, no.
 *  2. Is 0.1e10 valid?
 *     Yes
 *  3. -01.1e-10?
 *     Yes
 *  4. Hexadecimal numbers like 0xFF?
 *     Not for the purpose of this problem
 *  5. 3. (. not followed by a digit)?
 *     No
 *   6. Can exponent have decimal numbers? 3e0.1?
 *     Not for this problem.
 *   7. Is 1f ( floating point number with f as prefix ) valid?
 *     Not for this problem.
 *   8. How about 1000LL or 1000L ( C++ representation for long and long long numbers )?
 *     Not for this problem.
 *   9. How about integers preceded by 00 or 0? like 008?
 *     Yes for this problem
 */

#include <regex>
#include <iostream>
using namespace std;

class ValidNumber {
public:
	bool is_number(char c) {
		if (c >= '0' && c <= '9') return true;
		return false;
	}

	int is_valid_classic_notation(string s) {
		int n = (int)s.size();
		if (n == 0) return 0;
		int j = 0;

		// leading spaces
		while (j < n && s[j] == ' ') j++;
		if (j == n) return 0;

		// it can have sign followed by a decimal
		if (s[j] == '+' || s[j] == '-') {
			j++;
			if (j == n || is_number(s[j]) == false) return 0;

		}

		// decimal or real number
		while (j < n && is_number(s[j]) == true) j++;

		if (j < n && s[j] == '.') {
			j++;
			if (j == n || is_number(s[j]) == false) return 0;
			while (j < n && is_number(s[j]) == true) j++;
		}
		
		// trailing spaces
		while (j < n && s[j] == ' ') j++;
		if (j != n) return 0;
	}

	int is_valid_scientific_notation(string s) {
		int n = (int)s.size();
		if (n == 0) return 0;
		int j = 0;

		// leading spaces
		while (j < n && s[j] == ' ') j++;
		if (j == n) return 0;

		// it can have sign followed by a decimal
		if (s[j] == '+' || s[j] == '-') {
			j++;
			if (j == n || is_number(s[j]) == false) return 0;

		}

		// decimal or real number
		if (is_number(s[j]) == false) return 0;
		while (j < n && is_number(s[j]) == true) j++;

		if (j < n && s[j] == '.') {
			j++;
			if (j == n || is_number(s[j]) == false) return 0;
			while (j < n && is_number(s[j]) == true) j++;
		}

		// exponent
		if (j == n || (s[j] != 'e' && s[j] != 'E')) return 0;
		j++;

		if (j < n && s[j] == '+' || s[j] == '-') j++;

		if (j == n || is_number(s[j]) == false) return 0;
		while (j < n && is_number(s[j]) == true) j++;

		// trailing spaces
		while (j < n && s[j] == ' ') j++;
		if (j != n) return 0;
	}

	int is_valid_no_v2(string s) {
		int n = (int)s.size();
		if (n == 0) return 0;

		if (is_valid_classic_notation(s) || is_valid_scientific_notation(s)) return 1;
		return 0;
	}

	int is_valid_no(string s) {
		int n = (int)s.size();
		if (n == 0) return 0;

		// regex logic:
		//	1. can have none or more leading spaces
		//  2. can have (or not) one of the signs:-,+
		//  3. can have (or not) one . followed by a number
		//  4. can have none or more trailing spaces
		regex rgx_classic_no_notation("^([ ]*)([-+][0-9]+)?([0-9]*)(.[0-9]+)?([ ]*)$");
		
		// regex logic:
		//	1. can have none or more leading spaces
		//  2. can have (or not) one of the signs:-,+
		//  3. can have (or not) one . followed by a number
		//  4. must have e or E followed by a one or more numbers
		//  5. can have none or more trailing spaces
		regex rgx_scientific_no_notation("^([ ]*)([-+][0-9]+)?([0-9]*)(.[0-9]+)?([eE][+-]?[0-9]+)([ ]*)$");

		if (regex_match(s, rgx_classic_no_notation) || regex_match(s, rgx_scientific_no_notation)) {
			cout << "  ok" << endl;
			return 1;
		}

		cout << "  nok" << endl;
		return  0;
	}
};

/*
int main(void) {
	ValidNumber t;
	cout << t.is_valid_no_v2("+      ") << endl;
	cout << t.is_valid_no_v2("0") << endl;
	cout << t.is_valid_no_v2(".123") << endl;
	cout << t.is_valid_no_v2("01") << endl;
	cout << t.is_valid_no_v2("-2e+10") << endl;
	cout << t.is_valid_no_v2("-.") << endl;
	cout << t.is_valid_no_v2("46.e3") << endl;
	cout << t.is_valid_no_v2(".2e3") << endl;
	cout << t.is_valid_no_v2(".2e3") << endl;
	cout << t.is_valid_no_v2("e3") << endl;
	cout << t.is_valid_no_v2(".e1") << endl;
	cout << t.is_valid_no_v2("+E3") << endl;
	
	return 0;
}
*/