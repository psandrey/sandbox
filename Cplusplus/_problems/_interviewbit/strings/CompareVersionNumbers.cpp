
/*
 * Compare two version numbers version1 and version2.
 *
 * If version1 > version2 return 1,
 * If version1 < version2 return -1,
 *  otherwise return 0.
 * You may assume that the version strings are non-empty and contain only digits and the . character.
 * The . character does not represent a decimal point and is used to separate number sequences.
 * For instance, 2.5 is not "two and a half" or "half way to version three",
 * it is the fifth second-level revision of the second first-level revision.
 *
 * Here is an example of version numbers ordering:  0.1 < 1.1 < 1.2 < 1.13 < 1.13.4
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class CompareVersionNumbers {
public:
	vector<string> parse_version(string v) {
		vector<string> ver;
		int n = (int)v.size();
		int j = 0;
		while (j < n) {
			// cut leading zeroes
			while (j < n && v[j] == '0') j++;

			string x;
			if (j == n || v[j] == '.') x = "0";
			else { while (j < n && v[j] != '.') { x += v[j]; j++; } }
			j++;

			ver.push_back(x);
		}

		// cut trailing zeroes
		j = (int)ver.size() - 1;
		while (j > 0 && ver[j].compare("0") == 0) { ver.pop_back(); j--; }

		return ver;
	}

	int compare_versions(string v1, string v2) {
		vector<string> ver1 = parse_version(v1);
		vector<string> ver2 = parse_version(v2);

		int n = (int)ver1.size();
		int m = (int)ver2.size();
		int i = 0, j = 0;
		int r = 0;
		while (i < min(n, m) && j < min(n, m)) {
			int len1 = (int)ver1[i].size();
			int len2 = (int)ver2[j].size();
			if (len1 > len2) return 1;
			else if (len1 < len2) return -1;

			int compare = ver1[i].compare(ver2[j]);
			if (compare < 0) return -1;
			else if (compare > 0) return 1;
			i++; j++;
		}

		if (n > m) return 1;
		else if (n < m) return -1;

		return 0;
	}
};

/*
int main(void) {
	string v1 = "4444371174137455";
	string v2 = "5.168";

	CompareVersionNumbers t;
	cout << t.compare_versions(v1, v2);

	return 0;
}
*/