
/*
 * Given an integer, convert it to a roman numeral, and return a string corresponding to its roman numeral version.
 *
 * Input is guaranteed to be within the range from 1 to 3999.
 *
 * Example :
 *   Input : 5
 *   Return : "V"
 *
 *   Input : 14
 *   Return : "XIV"
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class IntegerToRoman {
public:

	string get_rom_digit(vector<vector<string>>& map, int pow, int digit) {
		string ans = "";
		if (digit <= 3) {
			int k = digit;
			while (k >= 1) { ans += map[pow][0]; k--; }
		}
		else if (digit == 4) ans = map[pow][0] + map[pow][1];
		else if (digit == 5) ans = map[pow][1];
		else if (digit >= 6 && digit <= 8) {
			ans = map[pow][1];
			int k = digit - 5;
			while (k >= 1) { ans += map[pow][0]; k--; }
		}
		else ans = map[pow][0] + map[pow + 1][0];

		return ans;
	}

	string int_to_rom(int x) {
		if (x == 0) return "";

		vector<vector<string>> map = { {"I", "V"}, {"X","L"}, {"C", "D"}, {"M"} };

		string ans = "";
		int pow = -1;
		while (x > 0) {
			int digit = x % 10;
			x = x / 10;
			pow++;
			if (digit == 0) continue;

			ans = get_rom_digit(map, pow, digit) + ans;
		}
		return ans;
	}
};

# if 0
int main(void) {

	int x = 6;

	IntegerToRoman t;
	cout << t.int_to_rom(x) << endl;
	
	return 0;
}
#endif