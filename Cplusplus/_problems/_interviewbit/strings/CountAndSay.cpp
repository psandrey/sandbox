
/*
 * The count-and-say sequence is the sequence of integers beginning as follows:
 *
 * 1, 11, 21, 1211, 111221, ...
 * 1 is read off as one 1 or 11.
 * 11 is read off as two 1s or 21.
 *
 * 21 is read off as one 2, then one 1 or 1211.
 *
 * Given an integer n, generate the nth sequence.
 */

#include <string>
#include <vector>
#include <iostream>
using namespace std;

class CountAndSay {
public:
	string count_and_say(int n) {
		if (n == 0) return "";

		string ans = "1"; n--;
		while (n > 0) {
			string temp = "";
			char prev = ans[0];
			int cnt = 0;
			for (char c : ans) {
				if (c == prev) cnt++;
				else {
					temp += to_string(cnt) + prev;
					prev = c; cnt = 1;
				}
			}
			temp += to_string(cnt) + prev;

			ans = temp;
			n--;
		}

		return ans;
	}

};

/*
int main(void) {

	CountAndSay t;

	cout << t.count_and_say(20);

	return 0;
}
*/