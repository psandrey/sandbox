
/*
 * Given a string S, find the longest palindromic substring in S.
 *
 * Substring of string S: S[i...j] where 0 <= i <= j < len(S)
 *
 * Palindrome string:  A string which reads the same backwards. More formally, S is palindrome if reverse(S) = S.
 *
 * Incase of conflict, return the substring which occurs first ( with the least starting index ).
 *
 * Example :
 *   Input : "aaaabaaa"
 *   Output : "aaabaaa"
 */

#include <vector>
#include <iostream>
using namespace std;

class LongestPalindromicSubstring {
public:
	string longest_palindrome_substr(string s) {
		int n = (int)s.size();
		if (n == 0) return "";
		if (n == 1) return s;

		string ans;
		int start = 0, max_len = 1;
		for (int i = 0; i < n; i++) {
			int low, hi, len = 0;

			// odd
			low = i - 1; hi = i + 1; len = 1;
			while (low >= 0 && hi < n && s[low] == s[hi]) { low--; hi++; len = hi - (low+1); }
			if (max_len < len) { start = low + 1; max_len = len; }
			//cout << s.substr(low + 1, len) << endl;

			// even
			low = i; hi = i + 1; len = 0;
			while (low >= 0 && hi < n && s[low] == s[hi]) {
				low--; hi++; 
				len = hi - (low+1);
			}
			if (len > 0 && max_len < len) { start = low + 1; max_len = len; }
			//if (len > 0) cout << s.substr(low + 1, len) << endl;
		}

		return s.substr(start, max_len);
	}
};

/*
int main(void) {
	//string s = "abcbabcaba";
	//string s = "yaxxaz";
	string s = "cccaabcbcbccbbcaaaaacccacbcbbaaccacacabcbbbcbbcaacbbababccbacaabbabc";

	LongestPalindromicSubstring t;
	cout << t.longest_palindrome_substr(s) << endl;
	
	return 0;
}
*/