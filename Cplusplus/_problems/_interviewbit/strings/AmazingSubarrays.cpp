
/*
 * You are given a string S, and you have to find all the amazing substrings of S.
 * Amazing Substring is one that starts with a vowel (a, e, i, o, u, A, E, I, O, U).
 * Note: 1 <= length(S) <= 1e6
 * S can have special characters
 *
 * Example:
 *   Input: ABEC
 *   Output: 6
 *
 * Explanation
 * 	Amazing substrings of given string are :
 * 	1. A
 * 	2. AB
 * 	3. ABE
 * 	4. ABEC
 * 	5. E
 * 	6. EC
 * 	here number of substrings are 6 and 6 % 10003 = 6.
 */

#include <string>
#include <vector>
#include <iostream>
#include <unordered_set>
using namespace std;

class AmazingSubarrays {
public:
	int no_amazing_subarrays(string s) {
		int n = (int)s.size();
		if (n == 0) return 0;

		// build vowels hashset
		unordered_set<char> vowels;
		vowels.insert('a');
		vowels.insert('e');
		vowels.insert('i');
		vowels.insert('o');
		vowels.insert('u');

		int j = 0, ans = 0;
		int m = 10003;
		while (j < n) {
			char c = tolower(s[j]);
			if (vowels.find(c) != vowels.end()) ans = (ans + (n - j)) % m;
			j++;
		}

		return ans;
	}
};

/*
int main(void) {
	string s = "ABEC";

	AmazingSubarrays t;
	cout << t.no_amazing_subarrays(s) << endl;

	return 0;
}
*/