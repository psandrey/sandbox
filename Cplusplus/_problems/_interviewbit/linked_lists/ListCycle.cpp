/*
 * Given a linked list, return the node where the cycle begins. If there is no cycle, return null.
 */

#include <iostream>
using namespace std;

class ListCycle {
public:
	class ListNode {
	public:
		ListNode* next;
		int val;
		ListNode(int v) { val = v; next = NULL; }
	};

	ListNode* detectCycle(ListNode* L) {
		if (L == NULL) return NULL;
		if (L->next == L) return L;

		ListNode* h = L, *t = L;
		while (h->next != NULL && h->next->next != NULL) {
			t = t->next;
			h = h->next->next;
			if (t == h) break;
		}
		if (h != t) return NULL;

		h = L;
		while (h != t) {
			h = h->next;
			t = t->next;
		}

		return t;
	}

	ListNode* buildList(void) {
		ListNode* L = new ListNode(1);
		L->next = new ListNode(2);
		L->next->next = new ListNode(3);
		L->next->next->next = new ListNode(4);
		L->next->next->next->next = new ListNode(5);
		L->next->next->next->next->next = L->next->next;
		return L;
	};
};

#if 0
int main(void) {
	ListCycle t;
	ListCycle::ListNode* L = t.buildList();

	ListCycle::ListNode *node = t.detectCycle(L);
	if (node == nullptr) cout << "No cycle detected" << endl;
	else cout << "Cycle detected at:" << node->val << endl;
}
#endif