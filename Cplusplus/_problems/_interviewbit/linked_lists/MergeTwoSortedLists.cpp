/*
 * Merge two sorted linked lists and return it as a new list.
 * The new list should be made by splicing together the nodes of the first two lists, and should also be sorted.
 *
 * For example, given following linked lists :
 *   5 -> 8 -> 20
 *   4 -> 11 -> 15
 *
 * The merged list should be :
 * 4 -> 5 -> 8 -> 11 -> 15 -> 20
 */

#include <iostream>
using namespace std;

class MergeTwoSortedLists {
public:
	class ListNode {
	public:
		ListNode* next;
		int val;
		ListNode(int v) { val = v; next = nullptr; }
	};

	ListNode* buildList1(void) {
		ListNode* L = new ListNode(5);
		L->next = new ListNode(8);
		L->next->next = new ListNode(20);
		return L;
	};

	ListNode* buildList2(void) {
		ListNode* L = new ListNode(4);
		L->next = new ListNode(11);
		L->next->next = new ListNode(15);
		return L;
	};


	void printList(ListNode* L) {
		ListNode* f = L;
		while (f != nullptr) {
			cout << f->val << " ";
			f = f->next;
		}
		cout << endl;
	}

	ListNode* merge(ListNode* L1, ListNode* L2) {
		if (L1 == nullptr && L2 == nullptr) return nullptr;

		// Add sentinel for all lists
		ListNode* p = new ListNode(-1);
		p->next = L1;
		L1 = p;
		p = new ListNode(-1);
		p->next = L2;
		L2 = p;
		ListNode* L = new ListNode(-1);
		p = L;

		// do the merge
		while (L1->next != nullptr || L2->next != nullptr) {
			if (L1->next != nullptr && L2->next != nullptr) {
				if (L1->next->val < L2->next->val) {
					p->next = L1->next;
					L1->next = L1->next->next;
					p = p->next;
					p->next = nullptr;
				} else {
					p->next = L2->next;
					L2->next = L2->next->next;
					p = p->next;
					p->next = nullptr;
				}
			} else if (L1->next != nullptr) {
				p->next = L1->next;
				L1->next = L1->next->next;
				p = p->next;
				p->next = nullptr;
			}
			else {
				p->next = L2->next;
				L2->next = L2->next->next;
				p = p->next;
				p->next = nullptr;
			}
		}

		delete L1, L2;
		p = L;
		L = L->next;
		delete p;
		return L;
	}
};

/*
int main(void) {
	MergeTwoSortedLists t;
	MergeTwoSortedLists::ListNode* L1 = t.buildList1();
	MergeTwoSortedLists::ListNode* L2 = t.buildList2();
	t.printList(L1);
	t.printList(L2);
	MergeTwoSortedLists::ListNode* L = t.merge(L1, L2);
	t.printList(L);
}
*/