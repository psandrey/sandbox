/*
 * Reverse a linked list from position m to n. Do it in-place and in one-pass.
 *
 * For example:
 *   Given 1->2->3->4->5->NULL, m = 2 and n = 4,
 *   return 1->4->3->2->5->NULL.
 * 
 * Note: You may assume that 1<=m<=n<=length of list
 */

#include <iostream>
using namespace std;

class ReverseLinkedListII {
public:
	class ListNode {
	public:
		ListNode* next;
		int val;
		ListNode(int v) { val = v; next = nullptr; }
	};

	ListNode* buildList(void) {
		ListNode* L = new ListNode(1);
		L->next = new ListNode(2);
		L->next->next = new ListNode(3);
		L->next->next->next = new ListNode(4);
		L->next->next->next->next = new ListNode(5);
		return L;
	};

	void printList(ListNode* L) {
		ListNode* f = L;
		while (f != nullptr) {
			cout << f->val << " ";
			f = f->next;
		}
		cout << endl;
	}

	ListNode* reverse(ListNode* L, int m, int n) {
		if (L == nullptr || L->next == nullptr || m == n) return L;

		ListNode* g = new ListNode(-1);
		g->next = L; L = g; n -= m;
		while (g != nullptr && m > 1) {
			g = g->next; m--;
		}

		ListNode* p = g->next;
		while (p->next != nullptr && n > 0) {
			ListNode* t = p->next;
			p->next = p->next->next;
			t->next = g->next;
			g->next = t;
			n--;
		}

		g = L;
		L = L->next;
		delete g;
		return L;
	}
};

/*
int main(void) {
	ReverseLinkedListII t;
	ReverseLinkedListII::ListNode* L = t.buildList();
	t.printList(L);
	L = t.reverse(L, 4, 5);
	t.printList(L);
}
*/