
/*
 * Sort a linked list in O(n.log n) time using constant space complexity.
 */

#include <vector>
#include <stack>
#include <algorithm>
#include <iostream>
using namespace std;

class SortList {
public :
	struct ListNode {
		int val;
		ListNode *next;
		ListNode(int x) : val(x), next(NULL) {}
	};

	ListNode* sortList(ListNode* a) {
		if (a == NULL || a->next == NULL) return a;
		stack<pair<ListNode*, ListNode*>> st;
		ListNode* b = a, *e = a;
		while (e->next != NULL) e = e->next;

		st.push(make_pair(b, e));
		while (!st.empty()) {
			pair<ListNode*, ListNode*> p = st.top(); st.pop();
			b = p.first; e = p.second;
			if (b == e) continue;

			pair<ListNode*, ListNode*> pivot = partition(b, e);

			if (pivot.first != NULL) st.push(make_pair(b, pivot.first));
			if (pivot.second != e) st.push(make_pair(pivot.second->next, e));
		}

		return a;
	}

	pair<ListNode*, ListNode*> partition(ListNode *b, ListNode *e) {
		ListNode* pp = NULL, *p = b, *t = b;
		while (t != e) {
			if (t->val < e->val) {
				swap(p->val, t->val); 
				pp = p;
				p = p->next;
			}
			t = t->next;
		}
		swap(p->val, e->val);

		return make_pair(pp, p);
	}
	ListNode* vector_to_list(vector<int>& a) {
		ListNode* L = new ListNode(0);
		ListNode* t = L;
		for (int i = 0; i < (int)a.size(); i++) {
			ListNode* p = new ListNode(a[i]);
			t->next = p; t = p;
		}

		return L->next;
	}

	void printList(ListNode* b, ListNode* e) {
		if (b == e) cout << b->val << endl;
		else {
			ListNode* n = b;
			while (n != e) {
				cout << n->val << ", ";
				n = n->next;
			}
			if (e != NULL) cout << e->val;
			cout << endl;
		}
	}
};

#if 0
int main(void) {
	vector<int> a = { 5, 25, 84, 20, 8, 99, 92, 2, 24, 6 };
	
	SortList t;
	SortList::ListNode* L = t.vector_to_list(a);
	L = t.sortList(L);
	t.printList(L, NULL);

	return 0;
}
#endif