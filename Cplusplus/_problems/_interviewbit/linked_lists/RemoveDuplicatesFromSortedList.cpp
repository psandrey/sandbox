
/*
 * Given a sorted linked list, delete all duplicates such that each element appear only once.
 *
 * For example,
 *  Given 1->1->2, return 1->2.
 *  Given 1->1->2->3->3, return 1->2->3.
*/

#include <iostream>
using namespace std;

class RemoveDuplicatesFromSortedList {
public:
	class ListNode {
	public:
		ListNode* next;
		int val;
		ListNode(int v) { val = v; next = nullptr; }
	};

	ListNode* buildList(void) {
		ListNode* L = new ListNode(1);
		L->next = new ListNode(2);
		L->next->next = new ListNode(2);
		L->next->next->next = new ListNode(4);
		L->next->next->next->next = new ListNode(5);
		L->next->next->next->next->next = new ListNode(5);
		return L;
	}

	void printList(ListNode* L) {
		ListNode* f = L;
		while (f != nullptr) {
			cout << f->val << " ";
			f = f->next;
		}
		cout << endl;
	}

	ListNode* removeDuplicates(ListNode* L) {
		if (L == nullptr || L->next == nullptr) return L;
		
		ListNode* p = L;
		while (p != nullptr) {
			while (p->next != nullptr && p->val == p->next->val) {
				ListNode* t = p->next;
				p->next = t->next;
				delete t;
			}
			p = p->next;
		}

		return L;
	}
};

/*
int main(void) {
	RemoveDuplicatesFromSortedList t;
	RemoveDuplicatesFromSortedList::ListNode* L = t.buildList();
	t.printList(L);
	L = t.removeDuplicates(L);
	t.printList(L);
}
*/