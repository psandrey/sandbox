/*
 * Given a sorted linked list, delete all nodes that have duplicate numbers, 
 * leaving only distinct numbers from the original list.
 *
 * For example,
 *   Given 1->2->3->3->4->4->5, return 1->2->5.
 *   Given 1->1->1->2->3, return 2->3.
*/

#include <iostream>
using namespace std;

class RemoveDuplicatesFromSortedListII {
public:
	class ListNode {
	public:
		ListNode* next;
		int val;
		ListNode(int v) { val = v; next = nullptr; }
	};

	ListNode* removeDuplicates(ListNode* head) {
		if (head == NULL || head->next == NULL) return head;

		ListNode* L = new ListNode(-1);
		L->next = head;
		ListNode* pp = L, *p = pp->next, *t = p->next;
		while (t != NULL) {
			if (p->val == t->val) {
				while (t != NULL && p->val == t->val) {
					p->next = t->next;
					delete t;
					t = p->next;
				}
				pp->next = t;
				delete p;
				p = t;
				t = (p == NULL ? NULL : p->next);
			}
			else {
				t = t->next; p = p->next; pp = pp->next;
			}
		}

		t = L;
		L = L->next;
		delete t;
		return L;
	}

	ListNode* buildList(void) {
		ListNode* L = new ListNode(1);
		L->next = new ListNode(2);
		L->next->next = new ListNode(3);
		L->next->next->next = new ListNode(3);
		L->next->next->next->next = new ListNode(4);
		L->next->next->next->next->next = new ListNode(4);
		L->next->next->next->next->next->next = new ListNode(5);
		return L;
	};

	void printList(ListNode* L) {
		ListNode* f = L;
		while (f != nullptr) {
			cout << f->val << " ";
			f = f->next;
		}
		cout << endl;
	};
};

#if 0
int main(void) {
	RemoveDuplicatesFromSortedListII t;
	RemoveDuplicatesFromSortedListII::ListNode* L = t.buildList();
	t.printList(L);
	L = t.removeDuplicates(L);
	t.printList(L);
}
#endif