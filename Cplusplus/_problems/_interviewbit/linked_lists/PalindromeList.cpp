
/*
 * Given a singly linked list, determine if its a palindrome. Return 1 or 0 denoting if its a palindrome or not, respectively.
 *
 * Expected solution is linear in time and constant in space.
 * For example,
 *  List 1-->2-->1 is a palindrome.
 *  List 1-->2-->3 is not a palindrome.
*/

#include <iostream>
using namespace std;

class PalindromeList {
public:
	class ListNode {
	public:
		ListNode* next;
		int val;
		ListNode(int v) { val = v; next = nullptr; }
	};

	ListNode* buildList(void) {
		ListNode* L = new ListNode(1);
		L->next = new ListNode(2);
		L->next->next = new ListNode(3);
		L->next->next->next = new ListNode(2);
		L->next->next->next->next = new ListNode(1);
		return L;
	};

	void printList(ListNode* L) {
		ListNode* f = L;
		while (f != nullptr) {
			cout << f->val << " ";
			f = f->next;
		}
		cout << endl;
	}

	int palindrome(ListNode* L) {
		if (L == nullptr || L->next == nullptr) return 1;

		// find middle (hare and tortoise)
		ListNode* p = L, * q = L;
		while (p->next != nullptr && p->next->next != nullptr) {
			p = p->next->next;
			q = q->next;
		}

		// reverse half
		p = q->next;
		while (p != nullptr && p->next != nullptr) {
			ListNode* t = p->next;
			p->next = t->next;
			t->next = q->next;
			q->next = t;
		}

		// check palindrome
		p = q->next; 
		q = L;
		while (p != nullptr) {
			// 2 values are different, so it isn't a palindrome
			if (q->val != p->val) return 0;
			q = q->next;
			p = p->next;
		}

		// is palindrome
		return 1;
	}
};

/*
int main(void) {
	PalindromeList t;
	PalindromeList::ListNode* L = t.buildList();
	t.printList(L);
	int isPalindrome = t.palindrome(L);
	cout << isPalindrome << endl;

}
*/