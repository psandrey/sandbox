/*
 * Given a linked list, remove the nth node from the end of list and return its head.
 *
 * For example,
 * Given linked list: 1->2->3->4->5, and n = 2.
 * After removing the second node from the end, the linked list becomes 1->2->3->5.
 *
 *  Note:
 * If n is greater than the size of the list, remove the first node of the list.
 */

#include <iostream>
using namespace std;

class RemoveNthNodeFromListEnd {
public:
	class ListNode {
	public:
		ListNode *next;
		int val;
		ListNode(int v) { val = v; next = nullptr; }
	};

	ListNode* buildList(void) {
		ListNode* L = new ListNode(0);
		L->next = new ListNode(1);
		L->next->next = new ListNode(2);
		L->next->next->next = new ListNode(3);

		return L;
	};

	ListNode* removeNthFromEnd(ListNode* L, int n) {
		if (L == nullptr) return L;

		ListNode* f = L, * b = nullptr;
		while (f != nullptr && n > 0) {
			f = f->next;
			n--;
		}

		if (f == nullptr) L = L->next;
		else {
			b = L;
			while (f->next != nullptr) {
				f = f->next;
				b = b->next;
			}

			b->next = b->next->next;
		}

		return L;
	}

	void printList(ListNode* L) {
		ListNode *f = L;
		while (f != nullptr) {
			cout << f->val << " ";
			f = f->next;
		}
		cout << endl;
	}
};

/*
int main(void) {
	RemoveNthNodeFromListEnd t;
	RemoveNthNodeFromListEnd::ListNode *L = t.buildList();
	t.printList(L);
	L = t.removeNthFromEnd(L, 2);
	t.printList(L);
}
*/