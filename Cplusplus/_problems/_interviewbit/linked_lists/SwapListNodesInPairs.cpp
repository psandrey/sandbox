
/*
 * Given a linked list, swap every two adjacent nodes and return its head.
 *
 * For example,
 * Given 1->2->3->4, you should return the list as 2->1->4->3.
 *
 * Your algorithm should use only constant space. You may not modify the values in the list, only nodes itself can be changed.
 */

#include <iostream>
using namespace std;

class SwapListNodesInPairs {
public:
	class ListNode {
	public:
		ListNode* next;
		int val;
		ListNode(int v) { val = v; next = nullptr; }
	};

	ListNode* buildList(void) {
		ListNode* L = new ListNode(1);
		L->next = new ListNode(2);
		L->next->next = new ListNode(3);
		L->next->next->next = new ListNode(4);
		L->next->next->next->next = new ListNode(5);
		return L;
	};

	void printList(ListNode* L) {
		ListNode* f = L;
		while (f != nullptr) {
			cout << f->val << " ";
			f = f->next;
		}
		cout << endl;
	}

	ListNode* swapPairs(ListNode* L) {
		if (L == nullptr || L->next == nullptr) return L;

		// add sentinel
		ListNode* p = new ListNode(-1);
		p->next = L;
		L = p;

		// swap in pairs
		while (p->next != nullptr && p->next->next != nullptr) {
			ListNode* t = p->next->next;
			p->next->next = t->next;
			t->next = p->next;
			p->next = t;
			p = p->next->next;
		}

		// remove sentinel
		p = L;
		L = L->next;
		delete p;

		// done
		return L;
	}
};

/*
int main(void) {
	SwapListNodesInPairs t;
	SwapListNodesInPairs::ListNode* L = t.buildList();
	t.printList(L);
	L = t.swapPairs(L);
	t.printList(L);
}
*/