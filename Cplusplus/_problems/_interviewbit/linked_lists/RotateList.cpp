
/*
 * Given a list, rotate the list to the right by k places, where k is non-negative.
 * 
 * For example:
 * Given 1->2->3->4->5->NULL and k = 2,
 *  return 4->5->1->2->3->NULL.
 */

#include <iostream>
using namespace std;

class RotateList {
public:
	class ListNode {
	public:
		ListNode* next;
		int val;
		ListNode(int v) { val = v; next = nullptr; }
	};

	ListNode* rotate(ListNode* head, int k) {
		if (head == NULL || head->next == NULL || k == 0) return head;

		int len = 1;
		ListNode* p = head;
		while (p->next != NULL) { len++; p = p->next; }
		k = k % len;
		if (k == 0) return head;

		k = len - k;
		ListNode* L = new ListNode(-1); L->next = head;
		while (k > 0) {
			ListNode* t = L->next;
			L->next = t->next;
			p->next = t;
			p = t;
			p->next = NULL;
			k--;
		}

		p = L;
		L = L->next;
		delete p;
		return L;
	}

	ListNode* buildList(void) {
		ListNode* L = new ListNode(68);
		L->next = new ListNode(86);
		L->next->next = new ListNode(36);
		L->next->next->next = new ListNode(16);
		L->next->next->next->next = new ListNode(5);
		L->next->next->next->next->next = new ListNode(75);
		return L;
	}

	void printList(ListNode* L) {
		ListNode* f = L;
		while (f != nullptr) {
			cout << f->val << " ";
			f = f->next;
		}
		cout << endl;
	}
};

#if 0
int main(void) {
	RotateList t;
	RotateList::ListNode* L = t.buildList();
	t.printList(L);
	L = t.rotate(L, 90);
	t.printList(L);
}
#endif