
/*
 * Given a singly linked list and an integer K, reverses the nodes of the
 * list K at a time and returns modified linked list.
 * Note : The length of the list is divisible by K
 *
 * Example :
 *   Given linked list 1 -> 2 -> 3 -> 4 -> 5 -> 6 and K=2,
 *   You should return 2 -> 1 -> 4 -> 3 -> 6 -> 5
 *
 * Note: Solve the problem using constant extra space.
*/

#include <iostream>
using namespace std;

class KReverseLinkedList {
public:
	class ListNode {
	public:
		ListNode* next;
		int val;
		ListNode(int v) { val = v; next = nullptr; }
	};

	ListNode * KReverse(ListNode *head, int k) {
		if (head == NULL || k == 1) return head;

		// add sentinel
		ListNode* L = new ListNode(-1);
		L->next = head;
		ListNode* pp = L;
		while (pp->next != NULL) {
			ListNode*p = pp->next, *t = p->next;
			int j = k - 1;
			while (j > 0) {
				j--;
				p->next = t->next;
				t->next = pp->next;
				pp->next = t;
				t = p->next;
			}
			pp = p;
		}

		pp = L;
		L = L->next;
		delete pp;
		return L;
	}

	ListNode* buildList(void) {
		ListNode* L = new ListNode(1);
		L->next = new ListNode(2);
		L->next->next = new ListNode(3);
		L->next->next->next = new ListNode(4);
		L->next->next->next->next = new ListNode(5);
		L->next->next->next->next->next = new ListNode(6);
		return L;
	}

	void printList(ListNode* L) {
		ListNode* f = L;
		while (f != nullptr) {
			cout << f->val << " ";
			f = f->next;
		}
		cout << endl;
	}
};

#if 0
int main(void) {
	KReverseLinkedList t;
	KReverseLinkedList::ListNode* L = t.buildList();
	t.printList(L);
	L = t.KReverse(L, 3);
	t.printList(L);
}
#endif