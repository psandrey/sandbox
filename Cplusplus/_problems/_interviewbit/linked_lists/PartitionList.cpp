
/*
 * Given a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.
 * 
 * Note: You should preserve the original relative order of the nodes in each of the two partitions.
 *
 * For example,
 *   Given 1->4->3->2->5->2 and x = 3,
 *   return 1->2->2->4->3->5.
*/

#include <iostream>
using namespace std;

class PartitionList {
public:
	class ListNode {
	public:
		ListNode* next;
		int val;
		ListNode(int v) { val = v; next = nullptr; }
	};

	ListNode* parition(ListNode* head, int x) {
		if (head == NULL || head->next == NULL) return head;
		ListNode* L = new ListNode(-1);
		L->next = head;

		ListNode* pp = L;
		while (pp->next != NULL && pp->next->val < x) pp = pp->next;

		ListNode* p = pp->next;
		while (p->next != NULL) {
			ListNode* t = p->next;
			if (t->val < x) {
				p->next = t->next;
				t->next = pp->next;
				pp->next = t;
				pp = t;
			}
			else p = p->next;
		}

		p = L;
		L = L->next;
		delete p;
		return L;
	}

	ListNode* buildList(void) {
		ListNode* L = new ListNode(1);
		L->next = new ListNode(4);
		L->next->next = new ListNode(3);
		L->next->next->next = new ListNode(2);
		L->next->next->next->next = new ListNode(5);
		L->next->next->next->next->next = new ListNode(2);
		return L;
	};

	void printList(ListNode* L) {
		ListNode* f = L;
		while (f != nullptr) {
			cout << f->val << " ";
			f = f->next;
		}
		cout << endl;
	}
};

#if 0
int main(void) {
	PartitionList t;
	PartitionList::ListNode* L = t.buildList();
	t.printList(L);
	L = t.parition(L, 3);
	t.printList(L);
}
#endif