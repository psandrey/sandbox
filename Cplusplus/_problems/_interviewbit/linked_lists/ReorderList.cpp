
/*
 * Given a singly linked list L: L0 -> L1 -> � -> Ln-1 -> Ln,
 * reorder it to: L0 -> Ln -> L1 -> Ln-1 -> L2 -> Ln-2 -> �
 * 
 * Note: You must do this in-place without altering the nodes� values.
 * 
 * For example,
 *  Given {1,2,3,4}, reorder it to {1,4,2,3}.
 */

#include <iostream>
using namespace std;

class ReorderList {
public:
	class ListNode {
	public:
		ListNode* next;
		int val;
		ListNode(int v) { val = v; next = nullptr; }
	};

	ListNode* reorderList(ListNode* head) {
		// we need at least 3 items in the list
		if (head == NULL || head->next == NULL
			|| head->next->next == NULL) return head;

		ListNode* pp = head, *t = head;
		while (t->next != NULL && t->next->next != NULL) {
			pp = pp->next;
			t = t->next->next;
		}

		// reverse half
		ListNode *p = pp->next;
		while (p->next != NULL) {
			t = p->next;
			p->next = t->next;
			t->next = pp->next;
			pp->next = t;
		}

		// interleave
		p = head;
		while (p != pp) {
			t = pp->next;
			pp->next = t->next;
			t->next = p->next;
			p->next = t;
			p = t->next;
		}

		return head;
	}

	ListNode* buildList(void) {
		ListNode* L = new ListNode(1);
		L->next = new ListNode(2);
		L->next->next = new ListNode(3);
#if 0
		L->next->next->next = new ListNode(4);
		L->next->next->next->next = new ListNode(5);
		L->next->next->next->next->next = new ListNode(6);
		L->next->next->next->next->next->next = new ListNode(7);
		L->next->next->next->next->next->next->next = new ListNode(8);
#endif
		return L;
	};

	void printList(ListNode* L) {
		ListNode* f = L;
		while (f != nullptr) {
			cout << f->val << " ";
			f = f->next;
		}
		cout << endl;
	}
};

#if 0
int main(void) {
	ReorderList t;
	ReorderList::ListNode* L = t.buildList();
	t.printList(L);
	L = t.reorderList(L);
	t.printList(L);
}
#endif