
/*
 * You are given two linked lists representing two non-negative numbers.
 * The digits are stored in reverse order and each of their nodes contain a single digit.
 * Add the two numbers and return it as a linked list.
 *
 * Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
 * Output: 7 -> 0 -> 8
 */

#include <iostream>
using namespace std;

class AddTwoNumbersAsLists {
public:
	class ListNode {
	public:
		ListNode* next;
		int val;
		ListNode(int v) { val = v; next = nullptr; }
	};

	ListNode* buildList1(void) {
		ListNode* L = new ListNode(2);
		L->next = new ListNode(4);
		L->next->next = new ListNode(3);
		return L;
	}

	ListNode* buildList2(void) {
		ListNode* L = new ListNode(5);
		L->next = new ListNode(6);
		L->next->next = new ListNode(4);
		return L;
	}

	void printList(ListNode* L) {
		ListNode* f = L;
		while (f != nullptr) {
			cout << f->val << " ";
			f = f->next;
		}
		cout << endl;
	}

	ListNode* add(ListNode* L1, ListNode* L2) {
		if (L1 == nullptr && L2 == nullptr) return new ListNode(0);
		// add sentinels
		ListNode* p = new ListNode(-1);
		p->next = L1;
		L1 = p;

		p = new ListNode(-1);
		p->next = L2;
		L2 = p;

		// delete leading zeroes
		p = L1;
		while (p->next != nullptr && p->next->val == 0) {
			ListNode* t = p->next;
			p->next = t->next;
			delete t;
		}

		p = L2;
		while (p->next != nullptr && p->next->val == 0) {
			ListNode* t = p->next;
			p->next = t->next;
			delete t;
		}

		ListNode* L = new ListNode(-1);
		ListNode* r = L;

		int c = 0; // carry
		p = L1->next;
		ListNode* q = L2->next;
		while (p != nullptr || q != nullptr) {
			int s = 0;
			if (p != nullptr && q != nullptr) {
				s = p->val + q->val + c;
				p = p->next; q = q->next;
			} else if (p != nullptr) {
				s = p->val + c;
				p = p->next;
			} else {
				s = q->val + c;
				q = q->next;
			}

			r->next = new ListNode(s % 10);
			r = r->next;
			c = s / 10;
		}

		if (c != 0) r->next = new ListNode(c);
		if (L->next == nullptr) L->next = new ListNode(0);

		// delete sentinels
		p = L; L = L->next; delete p;
		p = L1; L1 = L1->next; delete p;
		p = L2; L2 = L2->next; delete p;

		return L;
	}
};

/*
int main(void) {
	AddTwoNumbersAsLists t;
	AddTwoNumbersAsLists::ListNode* L1 = t.buildList1();
	AddTwoNumbersAsLists::ListNode* L2 = t.buildList2();
	
	t.printList(L1);
	t.printList(L2);

	AddTwoNumbersAsLists::ListNode*  L = t.add(L1, L2);
	t.printList(L);
}
*/