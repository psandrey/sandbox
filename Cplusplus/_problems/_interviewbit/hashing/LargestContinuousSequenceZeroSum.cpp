
/*
 * Find the largest continuous sequence in a array which sums to zero.
 *
 * Example:
 *   Input:  {1 ,2 ,-2 ,4 ,-4}
 *   Output: {2 ,-2 ,4 ,-4}
 */

#include <algorithm>
#include <vector>
#include <unordered_map>
#include <iostream>
using namespace std;

class LargestContinuousSequenceZeroSum {
public:

	// T(n) = O(n)
	vector<int> lszero(vector<int>& a) {
		vector<int> ans;
		if (a.empty()) return ans;

		unordered_map<int, int> hm;
		int n = (int)a.size(), s = 0, b = 0, e = 0;
		for (int j = 0; j < n; j++) {
			s += a[j];

			// 1. if current sum is 0, then the longest sub-sequence is the current one
			//
			// 2. we ask our selves how much can we cut from the beginning
			//  of the sequence to have a sum 0, so for each sum (assuming we encounter
			//  same sum multiple times on the way) we record the earliest, thus we
			//  cut as little as possible and obtain the sub array with maximum length
			if (s == 0) { b = 0; e = j + 1; }
			else if (hm.find(s) != hm.end() && j - hm[s] > e - b) { 
				b = hm[s] + 1; e = j + 1;
			} else hm[s] = j;
		}

		if (e > 0) ans = vector<int>(a.begin() + b, a.begin() + e);
		return ans;
	}


#if 0
	// TLE: T(n) = O(n^2)
	vector<int> lszero(vector<int>& a) {
		vector<int> ans;
		if (a.empty()) return ans;

		int n = (int)a.size(), b = 0, e = 0;
		vector<int> sums(n, 0);
		for (int j = 0; j < n; j++) {
			for (int i = 0; i <= j; i++) {
				sums[i] += a[j];
				if (sums[i] == 0 && e - b < j + 1 - i) { b = i; e = j + 1; }
			}
		}

		if (e > 0) ans = vector<int>(a.begin() + b, a.begin() + e);
		return ans;
 	}
#endif

};

#if 0
int main(void) {
	//vector<int> a = { 1 ,2 ,-2 ,4 ,-4 };
	vector<int> a = { -8, 8, -1, -16, -28, -27, 15, -14, 14, -27, -5, -6, -25,
					 -11, 28, 29, -3, -25, 17, -25, 4, -20, 2, 1, -17, -10, -25 };
	
	LargestContinuousSequenceZeroSum t;
	vector<int> ans = t.lszero(a);
	if (ans.empty() == false) {
		for (auto x : ans) cout << x << ", ";
		cout << endl;
	}

	return 0;
}
#endif