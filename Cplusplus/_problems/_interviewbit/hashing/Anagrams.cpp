
/*
 * Given an array of strings, return all groups of strings that are anagrams.
 * Represent a group by a list of integers representing the index in the original list.
 * Look at the sample case for clarification.
 *
 * Anagram : a word, phrase, or name formed by rearranging the letters of another,
 * such as 'spar', formed from 'rasp'
 *
 * Note: All inputs will be in lower-case.
 * Example :
 *   Input : cat dog god tca
 *   Output : [[1, 4], [2, 3]]
 */

#include <unordered_map>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class Anagrams {
public:
	vector<vector<int> > anagrams(const vector<string>& a) {
		vector<vector<int> > ans;
		if (a.empty()) return ans;

		unordered_map<string, vector<int>> hm;
		for (int i = 0; i < (int)a.size(); i++) {
			string s = a[i];
			sort(s.begin(), s.end());
			if (hm.find(s) == hm.end()) hm[s] = vector<int>();
			hm[s].push_back(i + 1);
		}

		for (auto& it : hm)  ans.push_back(it.second);
		return ans;
	}
};

#if 0
int main(void) {
	vector<string> a =
	{ "abbbaabbbabbbbabababbbbbbbaabaaabbaaababbabbabbaababbbaaabbabaabbaabbabbbbbababbbababbbbaabababba",
	"abaaabbbabaaabbbbabaabbabaaaababbbbabbbaaaabaababbbbaaaabbbaaaabaabbaaabbaabaaabbabbaaaababbabbaa",
	"babbabbaaabbbbabaaaabaabaabbbabaabaaabbbbbbabbabababbbabaabaabbaabaabaabbaabbbabaabbbabaaaabbbbab",
	"bbbabaaabaaaaabaabaaaaaaabbabaaaabbababbabbabbaabbabaaabaabbbabbaabaabaabaaaabbabbabaaababbaababb",
	"abbbbbbbbbbbbabaabbbbabababaabaabbbababbabbabaaaabaabbabbaaabbaaaabbaabbbbbaaaabaaaaababababaabab",
	"aabbbbaaabbaabbbbabbbbbaabbababbbbababbbabaabbbbbbababaaaabbbabaabbbbabbbababbbaaabbabaaaabaaaaba",
	"abbaaababbbabbbbabababbbababbbaaaaabbbbbbaaaabbaaabbbbbbabbabbabbaabbbbaabaabbababbbaabbbaababbaa",
	"aabaaabaaaaaabbbbaabbabaaaabbaababaaabbabbaaaaababaaabaabbbabbababaabababbaabaababbaabbabbbaaabbb" };

	Anagrams t;
	vector<vector<int>> ans = t.anagrams(a);
	for (int i = 0; i < (int)ans.size(); i++) {
		for (int j = 0; j < (int)ans[i].size(); j++) {
			cout << ans[i][j] << " ,";
		}
		cout << endl;
	}

	return 0;
}
#endif