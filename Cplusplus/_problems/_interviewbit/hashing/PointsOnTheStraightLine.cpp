
/*
 * Given n points on a 2D plane, find the maximum number of points that lie on the same straight line.
 */

#include <map>
#include <unordered_map>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class PointsOnTheStraightLine {
public:

#if 1
	int gcd(int a, int b) {
		if (a == 0) return b;
		if (b == 0) return a;

		while (b > 0) {
			int r = a % b;
			a = b; b = r;
		}

		return a;
	}

	int maxPoints(vector<int>& x, vector<int>& y) {
		if (x.empty() || y.empty()) return 0;
		int n = (int)x.size(), ans = 0;

		// for n == 1 the answer is weird
		if (n == 1) return 1;
		else if (n == 2) return 2;

		for (int i = 0; i < n - 1; i++) {

			// compute the maximum number of points on each line that contains point i
			int vertical = 0, overlap = 0, maxP = 0;
			map<pair<int, int>, int> hm;
			for (int j = i + 1; j < n; j++) {
				if (x[j] == x[i] && y[j] == y[i]) { overlap++; continue; }
				if (x[j] == x[i]) { vertical++; continue; }

				int dy = y[j] - y[i], dx = x[j] - x[i];
				if (dx < 0) { dx *= -1; dy *= -1; } // we do this to have the same pair for -,+ and +,-
				int g = gcd(abs(dy), abs(dx));

				pair<int, int> slope = make_pair(dy / g, dx / g);
				if (hm.find(slope) != hm.end()) hm[slope]++;
				else hm[slope] = 1;

				maxP = max(maxP, hm[slope]);
			}
			maxP = max(maxP, vertical); // the vertical line
			maxP += overlap; // add all overlapped points to the maximum line 
			maxP += 1; // until now the point i has not been added to the line, so lets add it

			ans = max(ans, maxP);
		}

		return ans;
	}

#else
	int maxPoints(vector<int>& x, vector<int>& y) {
		if (x.empty() || y.empty()) return 0;
		int n = (int)x.size(), ans = 0;

		// for n == 1 the answer is weird
		if (n == 1) return 1;
		else if (n == 2) return 2;

		for (int i = 0; i < n - 1; i++) {

			// compute the maximum number of points on each line that contains point i
			int vertical = 0, overlap = 0, maxP = 0;
			unordered_map<double, int> hm;
			for (int j = i+1; j < n; j++) {
				if (x[j] == x[i] && y[j] == y[i]) { overlap++; continue; }
				if (x[j] == x[i]) { vertical++; continue; }

				double m = (double)(y[j] - y[i]) / (double)(x[j] - x[i]);
				if (hm.find(m) != hm.end()) hm[m]++;
				else hm[m] = 1;

				cout << i << "," << j << ":" << hm[m] << endl;

				maxP = max(maxP, hm[m]);
			}
			maxP = max(maxP, vertical); // the vertical line

			maxP += overlap; // add all overlapped points to the maximum line 
			maxP += 1; // until now the point i has not been added to the line, so lets add it
			cout << " >> "<< maxP << endl;
			
			ans = max(ans, maxP);
		}

		return ans;
	}
#endif

	void genXY(vector<int>& x, vector<int>& y) {
		vector<int> a = { 14, -2, -17, -3, 2, 3, -13, -5, -18, 6, 9, -10, 15, 5, 2, 11, -3, 18, -7, -1, -10, -13,
			14, -6, -2, 13, -18, -14, -7, -1, 12, -19, 1, -13, -9, 2, -16, -6, -4, -3, 11, 2,
			-4, -2, -16, -11, 17, 5, 5, -13, 8, 18, -16, -15, -8, 12, 5, 5, 4, 14, 16, 2, 14, 8,
			-12, 4, 13, -20, 2, -1, -14, -1, -9, -16, 20, 20, -5, -9, -19, -2, -16, 0, -19, 1, 3,
			-4, 3, -1, 1, -20, -18, -2, 19, -9, 11, 2, -16, 10, 10, -12, 12, 4, -5, 12, -1, -3,
			-5, 2, -17, 3, -20, 4, 18, -1, -15, 10, -3, 19, -5, -17, 17, 14, 14, 7, 0, 17, 0, 5,
			3, -9, 10, -13, 13, 6, 10, 18, 15, 1, 2, 2, 19, 4, -14, 6, -17, 6, 4, -3, 6, 7, 1, 18, 12, 18 };
		
		for (int i = 0; i < (int)a.size(); i+=2) {
			x.push_back(a[i]);
			y.push_back(a[i + 1]);
		}
	}
};

#if 0
int main(void) {
	vector<int> x = { 0, 1, -1 };
	vector<int> y = { 0, 1, -1 };

	//vector<int> x = { -1, 0, 1, 2, 3, 3 };
	//vector<int> y = { 1, 0, 1, 2, 3, 4 };

	//vector<int> x, y;
	
	PointsOnTheStraightLine t;
	//t.genXY(x, y);
	
	int ans = t.maxPoints(x, y);
	cout << ans << endl;

	return 0;
}
#endif