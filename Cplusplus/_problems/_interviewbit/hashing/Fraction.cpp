
/*
 * Given two integers representing the numerator and denominator of a fraction,
 * return the fraction in string format.
 *
 * If the fractional part is repeating, enclose the repeating part in parentheses.
 *
 * Example :
 *   Given numerator = 1, denominator = 2, return "0.5
 *   Given numerator = 2, denominator = 1, return "2"
 *   Given numerator = 2, denominator = 3, return "0.(6)"
 */

#include <algorithm>
#include <string>
#include <unordered_map>
#include <iostream>
using namespace std;

class Fraction {
public:
	string fractionToDecimal(int a, int b) {
		if (b == 0) return "";
		if (a == 0) return "0";

		string ans;

		// do this first because of the (1<<31) test... bastards :)
		long long N = a, D = b;
		if ((N > 0LL && D < 0LL) || (N < 0LL && D > 0LL)) ans = "-";
		if (N < 0LL) N *= -1; // abs expects int, so... we do this
		if (D < 0LL) D *= -1; // same as above

		ans += to_string((N / D));
		N = N % D;
		if (N != 0) {
			unordered_map<long long, int> hm;
			ans += ".";
			while (N != 0L && hm.find(N) == hm.end()) {
				hm[N] = ans.size();
				N *= 10;
				ans += to_string((N / D));
				N = N % D;
			}

			if (N != 0) {
				string p = ans.substr(hm[N], ans.size());
				ans = ans.substr(0, hm[N]) + "(" + p + ")";
			}
		}

		return ans;
	}
};

#if 0
int main(void) {
	int a = 1, b = 24;
	//int a = 1, b = 2;
	//int a = -2147483648, b = -1;
	//int a = -1, b = 2;
	//int a = 12323, b = 10000;
	//int a = -1, b = -2147483648;

	Fraction t;
	cout << t.fractionToDecimal(a, b) << endl;

	return 0;
}
#endif