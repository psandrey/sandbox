
/*
 * Given an array A of integers, find the index of values that satisfy A + B = C + D,
 *  where A,B,C & D are integers values in the array
 *
 * Note:
 *
 * 1) Return the indices `A1 B1 C1 D1`, so that
 *   A[A1] + A[B1] = A[C1] + A[D1]
 *   A1 < B1, C1 < D1
 *   A1 < C1, B1 != D1, B1 != C1
 *
 * 2) If there are more than one solutions,
 *    then return the tuple of values which are lexicographical smallest.
 *
 * Assume we have two solutions
 * S1 : A1 B1 C1 D1 ( these are values of indices int the array )
 * S2 : A2 B2 C2 D2
 *
 * S1 is lexicographically smaller than S2 iff
 *   A1 < A2 OR
 *   A1 = A2 AND B1 < B2 OR
 *   A1 = A2 AND B1 = B2 AND C1 < C2 OR
 *   A1 = A2 AND B1 = B2 AND C1 = C2 AND D1 < D2
 * Example:
 *
 * Input: [3, 4, 7, 1, 2, 9, 8]
 * Output: [0, 2, 3, 5] (O index)
 * If no solution is possible, return an empty list.
 */

#include <unordered_map>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class Equal {
public:
	vector<int> equal(vector<int>& a) {
		vector<int> ans;
		if (a.empty() || a.size() < 4) return ans;
		int n = (int)a.size();

		unordered_map<int, pair<int, int>> hm;
		for (int c1 = 0; c1 < n; c1++) {
			for (int d1 = c1 + 1; d1 < n; d1++) {
				int s = a[c1] + a[d1];
				if (hm.find(s) == hm.end()) { hm[s] = make_pair(c1, d1); continue; }
				
				int a1 = hm[s].first, b1 = hm[s].second;
				if (a1 == c1 || b1 == c1 || b1 == d1) continue;
				if (ans.empty()) {
					ans.push_back(a1);
					ans.push_back(b1);
					ans.push_back(c1);
					ans.push_back(d1);
				} else if ( ans[0] > a1
					|| (ans[0] == a1 && ans[1] > b1)
					|| (ans[0] == a1 && ans[1] == b1 && ans[2] > c1)
					|| (ans[0] == a1 && ans[1] == b1 && ans[2] == c1 && ans[3] > d1)) {
					ans[0] = a1; ans[1] = b1; ans[2] = c1; ans[3] = d1;
				}
			}
		}
		
		return ans;
	}

};

#if 0
int main(void) {
	vector<int> a = {3, 4, 7, 1, 2, 9, 8};
	
	Equal t;
	vector<int> ans = t.equal(a);
	if (ans.empty() == false)
		cout << ans[0] << "," << ans[1] << "," << ans[2] << "," << ans[3] << endl;
	
	return 0;
}
#endif