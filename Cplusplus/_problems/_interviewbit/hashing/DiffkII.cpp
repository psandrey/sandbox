
/*
 * Given an array A of integers and another non negative integer k,
 * find if there exists 2 indices i and j such that A[i] - A[j] = k, i != j.
 */

#include <algorithm>
#include <vector>
#include <unordered_set>
#include <iostream>
using namespace std;

class DiffkII {
public:
	int diffPossible(const vector<int>& a, int k) {
		if (a.empty() == true && k != 0) return 0;
		if (a.empty() == true && k == 0) return 1;
		
		unordered_set<int> hs;
		for (int i = 0; i < (int)a.size(); i++) {
			if (hs.find(a[i]) != hs.end()) return 1;
			hs.insert(a[i] + k);
			hs.insert(a[i] - k);
		}

		return 0;
	}

};

#if 0
int main(void) {

	DiffkII t;
	vector<int> a = { 3, 10, 1 };
	int k = 2;

	cout << t.diffPossible(a, k) << endl;
	
	return 0;
}
#endif