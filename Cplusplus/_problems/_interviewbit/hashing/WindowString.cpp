
/*
 * Given a string S and a string T, find the minimum window in S which will contain all the characters
 *  in T in linear time complexity.
 *
 * Note: that when the count of a character C in T is N, then the count of C in minimum window in S
 *  should be at least N.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class WindowString {
public:
	string minWindow(string s, string t) {
		if (s.empty() || t.empty() || t.size() > s.size()) return "";

		const int FSIZE = 256;
		vector<int> ft(FSIZE, 0), fs(FSIZE, 0);

		int b = 0, wb = 0, len = 0, c = (int)t.size();
		for (int i = 0; i < (int)t.size(); i++) ft[t[i]]++;

		for (int j = 0; j < (int)s.size(); j++) {
			if (ft[s[j]] > 0) {
				if (fs[s[j]] < ft[s[j]]) c--;
				fs[s[j]]++;
			}

			if (c == 0) {
				// close window as much as possible
				while (fs[s[b]] == 0 || fs[s[b]] > ft[s[b]]) {
					if (fs[s[b]] > ft[s[b]]) fs[s[b]]--;
					b++;
				}

				// take the minimum window
				if (len == 0 || len > j - b + 1) { wb = b; len = j - b + 1; }
			}
		}

		return s.substr(wb, len);
	}
};

#if 0
int main(void) {
	//string s = "09lzzxhfhhyhhdzz";
	//string t = "xyzz";
	string s = "xiEjBOGeHIMIlslpQIZ6jERaAVoHUc9Hrjlv7pQpUSY8oHqXoQYWWll8Pumov89wXDe0Qx6bEjsNJQAQ0A6K21Z0BrmM96FWEdRG69M9CYtdBOrDjzVGPf83UdP3kc4gK0uHVKcPN4HPdccm9Qd2VfmmOwYCYeva6BSG6NGqTt1aQw9BbkNsgAjvYzkVJPOYCnz7U4hBeGpcJkrnlTgNIGnluj6L6zPqKo5Ui75tC0jMojhEAlyFqDs7WMCG3dmSyVoan5tXI5uq1IxhAYZvRQVHtuHae0xxwCbRh6S7fCLKfXeSFITfKHnLdT65K36vGC7qOEyyT0Sm3Gwl2iXYSN2ELIoITfGW888GXaUNebAr3fnkuR6VwjcsPTldQSiohMkkps0MH1cBedtaKNoFm5HbH15kKok6aYEVsb6wOH2w096OwEyvtDBTQwoLN87JriLwgCBBavbOAiLwkGGySk8nO8dLHuUhk9q7f0rIjXCsQeAZ1dfFHvVLupPYekXzxtWHd84dARvv4Z5L1Z6j8ur2IXWWbum8lCi7aErEcq41WTo8dRlRykyLRSQxVH70rUTz81oJS3OuZwpI1ifBAmNXoTfznG2MXkLtFu4SMYC0bPHNctW7g5kZRwjSBKnGihTY6BQYItRwLUINApd1qZ8W4yVG9tnjx4WyKcDhK7Ieih7yNl68Qb4nXoQl079Vza3SZoKeWphKef1PedfQ6Hw2rv3DpfmtSkulxpSkd9ee8uTyTvyFlh9G1Xh8tNF8viKgsiuCZgLKva32fNfkvW7TJC654Wmz7tPMIske3RXgBdpPJd5BPpMpPGymdfIw53hnYBNg8NdWAImY3otYHjbl1rqiNQSHVPMbDDvDpwy01sKpEkcZ7R4SLCazPClvrx5oDyYolubdYKcvqtlcyks3UWm2z7kh4SHeiCPKerh83bX0m5xevbTqM2cXC9WxJLrS8q7XF1nh";
	string t = "dO4BRDaT1wd0YBhH88Afu7CI4fwAyXM8pGoGNsO1n8MFMRB7qugS9EPhCauVzj7h";


	WindowString w;
	string ans = w.minWindow(s, t);
	cout << ans << endl;

	return 0;
}
#endif