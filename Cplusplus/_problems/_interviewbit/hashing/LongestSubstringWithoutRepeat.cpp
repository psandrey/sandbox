
/*
 * Given a string, find the length of the longest substring without repeating characters.
 *
 * Example:
 *  The longest substring without repeating letters for "abcabcbb" is "abc", which the length is 3.
 *  For "bbbbb" the longest substring is "b", with the length of 1.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class LongestSubstringWithoutRepeat {
public:
	int lengthOfLongestSubstring(string a) {
		if (a.empty()) return 0;
		else if (a.size() == 1) return 1;

		int n = (int)a.size();
		int i = 0, len = 0;
		vector<bool> hm(256, false);
		for (int j = 0; j < n; j++) {
			if (hm[a[j]] == true) {
				if (len < j - i) len = j - i;
				while (hm[a[j]] == true) {
					hm[a[i]] = false;
					i++;
				}
			}

			hm[a[j]] = true;
		}
		if (len < n - i) len = n - i;

		return len;
	}

};

#if 0
int main(void) {

	string a = "abcabcbb";
	//string a = "dadbc";

	LongestSubstringWithoutRepeat t;
	cout << t.lengthOfLongestSubstring(a) << endl;

	return 0;
}
#endif