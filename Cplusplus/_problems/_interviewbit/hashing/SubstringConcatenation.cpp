
/*
 * You are given a string, S, and a list of words, L, that are all of the same length.
 * Find all starting indices of substring(s) in S that is a concatenation of each word in
 * L exactly once and without any intervening characters.
 *
 * Example :
 *    S: "barfoothefoobarman"
 *    L: ["foo", "bar"]
 *
 *  You should return the indices: [0,9].
 *  Note: the order of indices does not matter.
 */

#include <unordered_map>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class SubstringConcatenation {
public:
	vector<int> findSubstring(string s, const vector<string>& l) {
		vector<int> ans;
		if (s.empty() || l.empty()) return ans;

		int n = (int)s.size(), m = l.size(), nm = l[0].size();
		if (n < m * nm) return ans;

		unordered_map<string, int> fl;
		for (int i = 0; i < m; i++) {
			if (fl.find(l[i]) == fl.end()) fl[l[i]] = 1;
			else fl[l[i]]++;
		}

		for (int b = 0; b < n - m * nm + 1; b++)
			if (concat(s, b, l, fl) == true) ans.push_back(b);
	
		return ans;
	}

	bool concat(string s, int b, const vector<string>& l, unordered_map<string, int>& fl) {
		int n = (int)s.size(), m = l.size(), nm = l[0].size();
		
		unordered_map<string, int> f;
		for (int j = b; j < b + m * nm; j += nm) {
			string c = s.substr(j, nm);

			if (fl.find(c) == fl.end()) return false;
			if (f.find(c) == f.end()) f[c] = 1;
			else f[c]++;

			if (f[c] > fl[c]) return false;
		}
		
		return true;
	}
};

#if 0
int main(void) {

	string s = "aaaaaaaaaaa";
	vector<string> words = { "aaa", "aaa" };

	SubstringConcatenation t;
	vector<int> ans = t.findSubstring(s, words);
	if (ans.empty() == false)
		for (int x : ans) cout << x << ", ";
	cout << endl;

	return 0;
}
#endif