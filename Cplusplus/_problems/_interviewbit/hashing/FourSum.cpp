
/*
 * Given an array S of n integers, are there elements a, b, c, and d in S such that
 * a + b + c + d = target? Find all unique quadruplets in the array which gives the sum of target.
 *
 * Elements in a quadruplet (a,b,c,d) must be in non-descending order. (i.e. a <= b <= c <= d).
 * The solution set must not contain duplicate quadruplets.
 * Also make sure that the solution set is lexicographically sorted.
 */

#include <unordered_map>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class FourSum {
public:
	vector<vector<int> > fourSum(vector<int>& a, int t) {
		vector<vector<int> >ans;
		if (a.empty() || a.size() < 4) return ans;
		sort(a.begin(), a.end());

		int n = (int)a.size();
		for (int k = 0; k < n - 3; k++) {
			if (k > 0 && a[k] == a[k - 1]) continue;

			for (int l = k + 1; l < n - 2; l++) {
				if (l > k + 1 && a[l] == a[l - 1]) continue;

				int i = l + 1, j = n - 1;
				while (i < j) {
					int s = a[k] + a[l] + a[i] + a[j];
					if (s < t) i++;
					else if (s > t) j--;
					else {
						vector<int> qtuple = { a[k], a[l], a[i], a[j] };
						ans.push_back(qtuple);
						i++; j--;

						// jump over duplicates
						while (i < j && a[i] == a[i - 1]) i++;
						while (i < j && a[j] == a[j + 1]) j--;
					}
				}
			}
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = { 1, 1, 1, 1, 0, 0, 0, -1, -1, -1, 0, -2, -2, 2, 0, 2, 2, 2 };
	int target = 0;

	FourSum t;
	vector<vector<int> > ans = t.fourSum(a, target);
	if (ans.empty() == false) {
		for (vector<int> qtuple : ans) {
			cout << qtuple[0] << "," << qtuple[1] << "," << qtuple[2] << "," << qtuple[3] << endl;
		}
	}
	
	return 0;
}
#endif