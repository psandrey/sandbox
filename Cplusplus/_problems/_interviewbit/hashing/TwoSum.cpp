
/*
 * Given an array of integers, find two numbers such that they add up
 *  to a specific target number. The function twoSum should return indices
 *  of the two numbers such that they add up to the target, where index1 < index2.
 *  Please note that your returned answers (both index1 and index2 ) are not zero-based.
 *  Put both these numbers in order in an array and return the array from your function
 *
 *  Note that, if no pair exists, return empty list.
 *  If multiple solutions exist, output the one where index2 is minimum.
 *  If there are multiple solutions with the minimum index2,
 *  choose the one with minimum index1 out of them.
 */

#include <unordered_map>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class TwoSum {
public:
	vector<int> twoSum(const vector<int>& a, int t) {
		vector<int> ans;
		if (a.empty() || a.size() < 2) return ans;

		unordered_map<int, int> hm;
		for (int k = 0; k < (int)a.size(); k++) {
			if (hm.find(a[k]) != hm.end()) {
				ans.push_back(hm[a[k]] + 1); ans.push_back(k + 1);
				break;
			} else {
				int complement = t - a[k];

				if (hm.find(complement) == hm.end()) hm[complement] = k;
			}
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = { 3, 7, 11, 3, 15 };
	int target = 14;

	TwoSum t;
	vector<int> ans = t.twoSum(a, target);
	
	if (!ans.empty())
		for (int x : ans) cout << x << "(" << a[x-1] << "),";
	cout << endl;

	return 0;
}
#endif