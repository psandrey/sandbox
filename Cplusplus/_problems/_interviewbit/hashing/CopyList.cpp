
/*
 * A linked list is given such that each node contains an additional random pointer
 *  which could point to any node in the list or NULL.
 *
 * Return a deep copy of the list.
 */

#include <unordered_map>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class CopyList {
public:
	struct RandomListNode {
		int label;
		RandomListNode* next, * random;
		RandomListNode(int x) : label(x), next(NULL), random(NULL) {}
	};

	RandomListNode* copyRandomList(RandomListNode* L) {
		if (L == NULL) return L;

		unordered_map< RandomListNode*, RandomListNode*> hm;
		RandomListNode* n = L, * nn, * prev, * newL = NULL;
		while (n != NULL) {
			nn = new RandomListNode(n->label); nn->random = n;
			hm[n] = nn;
			if (newL == NULL) newL = nn;
			else prev->next = nn;
			prev = nn;
			n = n->next;
		}

		nn = newL;
		while (nn != NULL) {
			n = nn->random; n = n->random;
			if (n == NULL) nn->random = NULL;
			else nn->random = hm[n];
			nn = nn->next;
		}

		return newL;
	}

	RandomListNode* debugL() {
		RandomListNode* L;
		L = new RandomListNode(1);
		L->next = new RandomListNode(2);
		L->next->next = new RandomListNode(3);
		L->random = L->next->next;
		L->next->random = L;
		L->next->next->random = L;
		return L;
	}
};

#if 0
int main(void) {

	CopyList t;
	CopyList::RandomListNode* L = t.debugL();
	CopyList::RandomListNode* nL = t.copyRandomList(L);

	CopyList::RandomListNode* n = nL;
	while (n != NULL) {
		cout << n->label << "(";
		if (n->random == NULL) cout << "NULL";
		else cout << n->random->label;
		cout << ")" << endl;

		n = n->next;
	}

	return 0;
}
#endif