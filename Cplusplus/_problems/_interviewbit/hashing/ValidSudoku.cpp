
/*
 * Determine if a Sudoku is valid, according to: http://sudoku.com.au/TheRules.aspx
 * The Sudoku board could be partially filled, where empty cells are filled with the character �.�.
 *
 * E.g. ["53..7....", "6..195...", ".98....6.", "8...6...3", "4..8.3..1",
 *       "7...2...6", ".6....28.", "...419..5", "....8..79"]
 *       Answer: 1
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class ValidSudoku {
public:
	int isValidSudoku(const vector<string>& a) {
		vector<vector<bool>> rows(9, vector<bool>(9, false));
		vector<vector<bool>> cols(9, vector<bool>(9, false));
		vector<vector<vector<bool>>> cels(3, vector<vector<bool>>(3, vector<bool>(9, false)));

		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++) {
				if (a[i][j] == '.') continue;

				int x = a[i][j] - '0' - 1;
				if (rows[i][x] == true) return 0;
				rows[i][x] = true;

				if (cols[j][x] == true) return 0;
				cols[j][x] = true;

				if (cels[i / 3][j / 3][x] == true) return 0;
				cels[i / 3][j / 3][x] = true;
			}

		return 1;
	}

};

#if 0
int main(void) {
	vector<string> a = { "53..7....", "6..195...", ".98....6.",
						"8...6...3", "4..8.3..1", "7...2...6",
						".6....28.", "...419..5", "....8..79" };
	ValidSudoku t;
	cout << t.isValidSudoku(a);

	return 0;
}
#endif