
/*
 * For Given Number N find if its COLORFUL number or not.
 *
 * Example:
 * A number can be broken into different contiguous sub-subsequence parts.
 * Suppose, a number 3245 can be broken into parts like 3 2 4 5 32 24 45 324 245 4245.
 * And this number is a COLORFUL number, since product of every digit of a
 * contiguous subsequence is different
 */

#include <algorithm>
#include <string>
#include <unordered_set>
#include <iostream>
using namespace std;

class ColorfulNumber {
public:

	int colorful(int a) {
		string sa = to_string(a);
		int n = (int)sa.size();
		unordered_set<unsigned long long> hs;

		for (int len = 1; len <= n; len++) {
			for (int i = 0; i <= n - len; i++) {
				string sx = sa.substr(i, len);
				
				unsigned long long p = 1;
				for (int k = 0; k < (int)sx.size(); k++)
					p *= (unsigned long long)(sx[k] - '0');
				
				if (hs.find(p) != hs.end())return 0;
				hs.insert(p);
			}
		}

		return 1;
	}

};

#if 0
int main(void) {
	int a = 3245;

	ColorfulNumber t;
	cout << t.colorful(a) << endl;

	return 0;
}
#endif