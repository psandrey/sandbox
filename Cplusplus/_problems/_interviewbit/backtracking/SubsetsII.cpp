
/*
 * Given a collection of integers that might contain duplicates, S, return all possible subsets.
 *
 *  Note:
 * Elements in a subset must be in non-descending order.
 * The solution set must not contain duplicate subsets.
 * The subsets must be sorted lexicographically.
 * Example :
 * If S = [1,2,2], the solution is:
 *
 * [
 * [],
 * [1],
 * [1,2],
 * [1,2,2],
 * [2],
 * [2, 2]
 * ]
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class SubsetII {
public:
	vector<vector<int>> subsetsII(vector<int>& a) {
		vector<int> subset;
		vector<vector<int>> ans;
		if (a.size() == 0) { ans.push_back(subset);  return ans; }
		sort(a.begin(), a.end());
		helper(ans, subset, 0, a, (int)a.size());

		return ans;
	}

	void helper(vector<vector<int>>& ans, vector<int>& subset, int k, vector<int>& a, int n) {
		ans.push_back(subset);

		for (int j = k; j < n; j++) {
			// avoid duplicates
			if (j > k && a[j] == a[j - 1]) continue;

			subset.push_back(a[j]);
			helper(ans, subset, j + 1, a, n);
			subset.pop_back();
		}
	}
};

#if 0
int main(void) {
	//vector<int> a = { 1, 2, 2 };
	vector<int> a = { 1, 2, 1, 2, 1, 2, 1, 2 };
	SubsetII t;

	vector<vector<int>> ans = t.subsetsII(a);
	for (vector<int> v : ans) {
		cout << "[ ";
		for (int x : v) cout << x << " ";
		cout << " ]" << endl;
	}

	return 0;
}
#endif