
#include <unordered_set>
#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class Permutations {
public:
	vector<vector<int> > permute(vector<int>& a) {
		vector<vector<int> > perms;
		if (a.empty()) return perms;
		int n = (int)a.size();
		if (n == 1) {
			vector<int> perm; perm.push_back(a[0]);
			perms.push_back(perm);
			return perms;
		}

		unordered_set<int> lookup;
		vector<int> st;
		build_perms(st, lookup, n, a, perms);

		return perms;
	}

	void build_perms(vector<int>& st, unordered_set<int>& lookup, int n, vector<int>& a, vector<vector<int> >& perms) {
		if (st.size() == n) {
			vector<int> new_perm(st);
			perms.push_back(new_perm);
			return;
		}

		for (int j = 0; j < n; j++) {
			if (lookup.find(a[j]) == lookup.end()) {
				st.push_back(a[j]); lookup.insert(a[j]);

				build_perms(st, lookup, n, a, perms);

				st.pop_back(); lookup.erase(a[j]);
			}
		}
	}
};

#if 0
int main(void) {
	vector<int> a = { 1, 2, 3 };

	Permutations t;
	vector<vector<int> > perms = t.permute(a);
	for (int j = 0; j < (int)perms.size(); j++) {
		for (int x : perms[j])
			cout << x << " ";
		cout << endl;
	}

	return 0;
}
#endif