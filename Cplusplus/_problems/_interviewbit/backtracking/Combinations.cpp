

/*
 * Given two integers n and k, return all possible combinations of k numbers out of 1 2 3 ... n.
 *
 * Make sure the combinations are sorted.
 *
 * To elaborate,
 * Within every entry, elements should be sorted. [1, 4] is a valid entry while [4, 1] is not.
 * Entries should be sorted within themselves.
 * Example :
 * If n = 4 and k = 2, a solution is:
 *
 * [
 *   [1,2],
 *   [1,3],
 *   [1,4],
 *   [2,3],
 *   [2,4],
 *   [3,4],
 * ]
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class Combinations {
public:
	vector<vector<int>> combinations(int n, int k) {
		vector<int> comb;
		vector<vector<int>> ans;
		if (n == 0 || k == 0 || k > n) return ans;

		helper(ans, comb, 0, n, k);
		
		return ans;
	}

	void helper(vector<vector<int>>& ans, vector<int>& comb, int i, int n, int k) {
		if (comb.size() == (size_t)k) { ans.push_back(comb); return; }
		if (i == n) return;

		// take i'th element
		comb.push_back(i+1);
		helper(ans, comb, i + 1, n, k);
		comb.pop_back();

		// don' take i'th element
		helper(ans, comb, i + 1, n, k);
	}
};

#if 0
int main(void) {
	Combinations t;

	vector<vector<int>> ans = t.combinations(4, 2);
	for (vector<int> v : ans) {
		for (int x : v) cout << x << " ";
		cout << endl;
	}


	return 0;
}
#endif