
/*
 * The eight queens problem is the problem of placing eight queens on an 8�8 chessboard
 *  such that none of them attack one another (no two are in the same row,
 *  column, or diagonal). More generally, the n queens problem places n queens on
 *  an n�n chessboard.
 *
 * Solutions:
 *     - 2 iterative backtracking
 *     - 1 recursive backtracking
 */

#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class NQueens {
public:
	vector<vector<string> > solveNQueens(int n) {
		vector<vector<string> > ans;
		if (n == 1) {
			vector<string> row;
			row.push_back("Q");
			ans.push_back(row);
			return ans;
		}
		if (n <= 3) return ans;

		vector<int> st;
		find_solutions(st, ans, n);

		return ans;
	}

	void find_solutions(vector<int>& st, vector<vector<string> >& ans, int n) {
		if (st.size() == n) {
			build_table(st, ans, n);
			return;
		}

		for (int j = 0; j < n; j++) {
			if (valid(st, j, n)) {
				st.push_back(j);
				find_solutions(st, ans, n);
				st.pop_back();
			}
		}
	}

	bool valid(vector<int>& st, int j, int n) {
		if (!st.empty()) {
			int i = st.size();
			for (int row = 0; row < (int)st.size(); row++) {
				int col = st[row];
				int x = abs(row - i);
				int y = abs(col - j);
				if ( (row == i || col == j) ||
					 (abs(row - i) == abs(col - j) ) )return false;
			}
		}

		return true;
	}

	void build_table(vector<int>& st, vector<vector<string> >& ans, int n) {
		vector<string> table;
		int k = 0;
		for (int i = 0; i < n; i++) {
			string row;
			for (int j = 0; j < n; j++) {
				if (st[k] == j) row += "Q";
				else row += ".";
			}
			k++;
			table.push_back(row);
		}

		ans.push_back(table);
	}
};

#if 0
int main(void) {
	int n = 1;

	NQueens t;
	vector<vector<string> > tables = t.solveNQueens(n);
	for (int k = 0; k < (int)tables.size(); k++) {
		for (int row = 0; row < (int)tables[k].size(); row++) {
			cout << tables[k][row] << endl;
		}
		cout << endl;
	}

	return 0;
}
#endif