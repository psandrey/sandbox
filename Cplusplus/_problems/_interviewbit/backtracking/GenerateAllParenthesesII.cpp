
/*
 * Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses of length 2*n.
 *
 * For example, given n = 3, a solution set is:
 *
 * "((()))", "(()())", "(())()", "()(())", "()()()"
 * Make sure the returned list of strings are sorted.
 */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class GenerateAllParenthesesII {
public:
	vector<string> generate_parentheses(int n) {
		vector<string> ans;
		if (n == 0) return ans;
		if (n == 1) ans.push_back("()");
		else {
			string stack;
			helper(ans, stack, n, n);
		}
		//sort(ans.begin(), ans.end());
		return ans;
	}

	void helper(vector<string>& ans, string stack, int remo, int remc) {
		if (remo == 0 && remc == 0)
			ans.push_back(string(stack));
		else {
			if (remo > 0) {
				stack.push_back('(');
				helper(ans, stack, remo - 1, remc);
				stack.pop_back();
			}

			if (remc > remo) {
				stack.push_back(')');
				helper(ans, stack, remo, remc - 1);
				stack.pop_back();
			}
		}
	}
};

#if 0
int main(void) {

	GenerateAllParenthesesII t;

	vector<string> ans = t.generate_parentheses(3);
	for (string x : ans) cout << x << endl;

	return 0;
}
#endif