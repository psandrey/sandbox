
/*
 * Given a set of candidate numbers (C) and a target number (T), find all unique combinations in C where
 * the candidate numbers sums to T.
 *
 * The same repeated number may be chosen from C unlimited number of times.
 *
 *  Note:
 * All numbers (including target) will be positive integers.
 * Elements in a combination (a1, a2, � , ak) must be in non-descending order. (ie, a1 <= a2 <= � <=ak).
 * The combinations themselves must be sorted in ascending order.
 * CombinationA > CombinationB iff (a1 > b1) OR (a1 = b1 AND a2 > b2) OR ... 
 *   (a1 = b1 AND a2 = b2 AND ... ai = bi AND ai+1 > bi+1)
 * The solution set must not contain duplicate combinations.
 * Example,
 * Given candidate set 2,3,6,7 and target 7,
 * A solution set is:
 *
 * [2, 2, 3]
 * [7]
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class CombinationSum {
public:
	vector<vector<int>> combinations_sum(vector<int>& a, int t) {
		int n = (int)a.size();
		vector<int> comb;
		vector<vector<int>> ans;
		if (n == 0) return ans;

		sort(a.begin(), a.end());
		
		// remove duplicates
		int i = 1, j = 1;
		while (i < n) {
			while (i < n && a[i] == a[i - 1]) i++;
			if (i == n) break;
			a[j] = a[i];
			i++; j++;
		}

		helper(ans, comb, t, 0, a, j);

		return ans;
	}

	void helper(vector<vector<int>>& ans, vector<int>& comb, int sum, int i, vector<int>& a, int n) {
		if (sum == 0) { ans.push_back(comb); return; }
		if (sum < 0 || i == n) return;

		if (sum >= a[i]) {
			int no_i = sum / a[i];
			for (int k = no_i; k > 0; k--) comb.push_back(a[i]);
			for (int k = no_i; k > 0; k--) {
				helper(ans, comb, sum - a[i] * k, i + 1, a, n);
				comb.pop_back();
			}
		}

		helper(ans, comb, sum, i + 1, a, n);
	}
};

# if 0
int main(void) {
	vector<int> a = { 1, 1, 1, 1, 1 };
	int target = 5;
	CombinationSum t;

	vector<vector<int>> ans = t.combinations_sum(a, target);
	for (vector<int> v : ans) {
		for (int x : v) cout << x << " ";
		cout << endl;
	}


	return 0;
}
#endif