
/*
 * The gray code is a binary numeral system where two successive values differ in only one bit.
 *
 * Given a non-negative integer n representing the total number of bits in the code, print the sequence of gray code.
 * A gray code sequence must begin with 0.
 *
 * For example, given n = 2, return [0,1,3,2]. Its gray code sequence is:
 * 00 - 0
 * 01 - 1
 * 11 - 3
 * 10 - 2
 * There might be multiple gray code sequences possible for a given n.
 * Return any such sequence.
 */

#include <bitset>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class GrayCode {
public:
#if 1
	vector<int> gray(int n) {
		if (n == 0) return vector<int>();

		int m = pow(2, n);
		vector<int> ans(m);
		ans[0] = 0; ans[1] = 1;

		int len = 2;
		for (int i = 1; i < n; i++) {
			int msb = (1 << i);
			for (int j = 0; j < len; j++)
				ans[len + j] = (ans[len - j - 1] | msb);

			len *= 2;
		}

		return ans;
	}

#else
	vector<int> gray(int n) {
		vector<int> ans;
		if (n == 1) {
			ans.push_back(0);
			ans.push_back(1);
		} else {
			ans = gray(n - 1);
			int m = (int)ans.size();
			int mask = 1 << (n-1);
			for (int i = m - 1; i >= 0; i--)
				ans.push_back(ans[i] | mask);
		}

		return ans;
	}
#endif
};

#if 0
int main(void) {
	GrayCode t;
	
	vector<int> ans = t.gray(4);
	for (int x : ans) {
		cout << bitset<32>(x) << " " << endl;
		//cout << x << endl;
	}
	return 0;
}
#endif