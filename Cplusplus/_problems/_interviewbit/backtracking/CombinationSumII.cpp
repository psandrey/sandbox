
/*
 * Given a collection of candidate numbers (C) and a target number (T),
 * find all unique combinations in C where the candidate numbers sums to T.
 *
 * Each number in C may only be used once in the combination.
 *
 *  Note:
 * All numbers (including target) will be positive integers.
 * Elements in a combination (a1, a2, � , ak) must be in non-descending order. (ie, a1 <= a2 <= ... <= ak).
 * The solution set must not contain duplicate combinations.
 *
 * Example :
 * Given candidate set 10,1,2,7,6,1,5 and target 8,
 * A solution set is:
 * [1, 7]
 * [1, 2, 5]
 * [2, 6]
 * [1, 1, 6]
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class CombinationSumII {
public:
	vector<vector<int>> combinations_sum(vector<int>& a, int t) {
		int n = (int)a.size();
		vector<int> comb;
		vector<vector<int>> ans;
		if (n == 0) return ans;

		sort(a.begin(), a.end());
		helper(ans, comb, t, 0, a, n);

		return ans;
	}

	void helper(vector<vector<int>>& ans, vector<int>& comb, int sum, int i, vector<int>& a, int n) {
		if (sum == 0) { ans.push_back(comb); return; }
		if (sum < 0 || i == n) return;

		if (sum >= a[i]) {
			comb.push_back(a[i]);
			helper(ans, comb, sum - a[i], i + 1, a, n);
			comb.pop_back();
		}

		// avoid duplicate combinations
		i++;
		while (i < n && a[i] == a[i - 1]) i++;
		
		helper(ans, comb, sum, i, a, n);
	}
};

#if 0
int main(void) {
	vector<int> a = { 10, 1, 2, 7, 6, 1, 5 };
	int target = 8;
	CombinationSumII t;

	vector<vector<int>> ans = t.combinations_sum(a, target);
	for (vector<int> v : ans) {
		for (int x : v) cout << x << " ";
		cout << endl;
	}


	return 0;
}
#endif