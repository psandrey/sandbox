
/*
 * The set [1,2,3,...,n] contains a total of n! unique permutations.
 *
 * By listing and labeling all of the permutations in order,
 * We get the following sequence (ie, for n = 3 ) :
 *
 * 1. "123"
 * 2. "132"
 * 3. "213"
 * 4. "231"
 * 5. "312"
 * 6. "321"
 * Given n and k, return the kth permutation sequence.
 *
 * For example, given n = 3, k = 4, ans = "231"
 *
 *  Good questions to ask the interviewer :
 *
 *  What if n is greater than 10. How should multiple digit numbers be represented in string?
 *    In this case, just concatenate the number to the answer.
 *    so if n = 11, k = 1, ans = "1234567891011"
 * 
 *  Whats the maximum value of n and k?
 *    In this case, k will be a positive integer thats less than INT_MAX.
 *    n is reasonable enough to make sure the answer does not bloat up a lot.
*/

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class KthPermutationSequence {
public:
	string getPermutation(int n, int k) {
		vector<int> candidate;
		for (int i = 1; i <= n; i++) candidate.push_back(i);
#if 0
		return build_k_permutation(k - 1, candidate);
#else
		vector<bool> x(n + 1, false);
		return kperm(n, k, x);
#endif
	}

	string kperm(int n, int k, vector<bool>& x) {
		string ans;
		if (k == 0) {
			// remaining items in descending order
			for (int j = (int)x.size() - 1; j >= 1; j--)
				if (x[j] == false) { ans += to_string(j); x[j] = true; }
		} else {
			int f = factorial(n - 1);
			int i = (int)ceill((double)k / f);
			int leading = 1; // find the i-th unused item
			for (int j = 1; j < (int)x.size(); j++) {
				if (x[j] == false) i--;
				if (i == 0) { x[j] = true; leading = j; break;}
			}

			ans = to_string(leading) + kperm(n - 1, k % f, x);
		}

		return ans;
	}

	string build_k_permutation(int k, vector<int>& candidate) {
		int n = candidate.size();
		if (n == 0) return "";

		// how many permutations per group
		int p_per_group = factorial(n-1);

		// get the leading group
		int i = k / p_per_group;
		string leading = to_string(candidate[i]);

		// offset in the group
		k %= p_per_group;

		// remove the leading number
		candidate.erase(candidate.begin() + i);

		// recursively construct the permutation
		return leading + build_k_permutation(k, candidate);
	}

	int factorial(int n) {
		if (n > 12)  return INT_MAX;

		int fact = 1;
		for (int i = 2; i <= n; i++) fact *= i;
		return fact;
	}
};

#if 0
int main(void) {
	KthPermutationSequence t;
	cout << t.getPermutation(4, 17);
	return 0;
}
#endif