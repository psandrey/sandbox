
/*
 * Given a string s, partition s such that every string of the partition is a palindrome.
 * Return all possible palindrome partitioning of s.
 */

#include <stack>
#include <vector>
#include <iostream>
using namespace std;

class PalindromePartitioning {
public:
	vector<vector<string> > partition(string a) {
		vector<vector<string> > ans;
		if (a.empty()) return ans;
		if (a.size() == 1) {
			vector<string> p; p.push_back(a);
			ans.push_back(p);
			return ans;
		}

		vector<int> st;
		build_partitions(st, 0, a, (int)a.size(), ans);

		return ans;
	}

	void build_partitions(vector<int>& st, int j, string a, int n, vector<vector<string> >& ans) {
		if (j == n) {
			vector<string> partition;
			int i = 0;
			for (int k = 0; k < (int)st.size(); k++) {
				string p = a.substr(i, st[k] - i);
				i = st[k];
				partition.push_back(p);
			}
			ans.push_back(partition);
			return;
		}

		for (int i = j + 1; i <= n; i++) {
			if (palindrome(a, j, i)) {
				st.push_back(i);
				build_partitions(st, i, a, n, ans);
				st.pop_back();
			}
		}
	}

	bool palindrome(string a, int s, int e) {
		int i = s, j = e - 1;
		while (i < j) {
			if (a[i] != a[j]) return false;
			i++; j--;
		}

		return true;
	}
};

#if 0
int main(void) {
	string a = "aab";

	PalindromePartitioning t;
	vector<vector<string> > ans = t.partition(a);

	for (int k = 0; k < (int)ans.size(); k++) {
		for (string s : ans[k]) cout << s << " ";
		cout << endl;
	}
	return 0;
}
#endif