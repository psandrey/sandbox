
/*
 * Write a program to solve a Sudoku puzzle by filling the empty cells.
 * Empty cells are indicated by the character '.'
 * You may assume that there will be only one unique solution.
 *
 * Example :
 *  For the above given diagrams, the corresponding input to your program will be
 *  [[53..7....], [6..195...], [.98....6.], [8...6...3], [4..8.3..1], [7...2...6], [.6....28.], [...419..5], [....8..79]]
 *
 *  and we would expect your program to modify the above array of array of characters to
 *  [[534678912], [672195348], [198342567], [859761423], [426853791], [713924856], [961537284], [287419635], [345286179]]
 */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class Sudoku {
public:
	void solveSudoku(vector<vector<char> >& board) {
		vector<vector<bool>> rows(10, vector<bool>(10, false));
		vector<vector<bool>> cols(10, vector<bool>(10, false));
		vector<vector<vector<bool>>> cells(3, vector<vector<bool>>(3, vector<bool>(10, false)));
		init(rows, cols, cells, board);

		solve_sudoku(board, 0, 0, rows, cols, cells);
	}

	bool solve_sudoku(vector<vector<char> >& board, int i, int j,
		vector<vector<bool>>& rows, vector<vector<bool>>& cols, vector<vector<vector<bool>>>& cells) {
		if (i == 9) return true;
		if (j == 9) return solve_sudoku(board, i + 1, 0, rows, cols, cells);
		if (board[i][j] != '.') return solve_sudoku(board, i, j + 1, rows, cols, cells);

		for (int d = 1; d <= 9; d++) {
			if (valid(rows, cols, cells, i, j, d) == true) {
				board[i][j] = '0' + d; set_d(rows, cols, cells, i, j, d);
				if (solve_sudoku(board, i, j + 1, rows, cols, cells)) return true;
				board[i][j] = '.'; clear_d(rows, cols, cells, i, j, d);
			}
		}

		return false;
	}

	void init(vector<vector<bool>>& rows, vector<vector<bool>>& cols, vector<vector<vector<bool>>>& cells,
		vector<vector<char> >& board) {
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++) {
				if (board[i][j] != '.') {
					int d = board[i][j] - '0';
					rows[i][d - 1] = true;
					cols[j][d - 1] = true;
					cells[i / 3][j / 3][d - 1] = true;
				}
			}
	}

	void set_d(vector<vector<bool>>& rows, vector<vector<bool>>& cols, vector<vector<vector<bool>>>& cells,
		int i, int j, int d) {
		rows[i][d - 1] = true;
		cols[j][d - 1] = true;
		cells[i / 3][j / 3][d - 1] = true;
	}

	void clear_d(vector<vector<bool>>& rows, vector<vector<bool>>& cols, vector<vector<vector<bool>>>& cells,
		int i, int j, int d) {
		rows[i][d - 1] = false;
		cols[j][d - 1] = false;
		cells[i / 3][j / 3][d - 1] = false;
	}

	bool valid(vector<vector<bool>>& rows, vector<vector<bool>>& cols, vector<vector<vector<bool>>>& cells,
		int i, int j, int d) {
		if (rows[i][d - 1] == true) return false;
		if (cols[j][d - 1] == true) return false;
		if (cells[i / 3][j / 3][d - 1] == true) return false;

		return true;
	}
};

#if 0
int main(void) {
	vector<vector<char> > board = 
		{ {'5','3','.','.','7','.','.','.','.'},
		{'6','.','.','1','9','5','.','.','.'},
		{'.','9','8','.','.','.','.','6','.'},
		{'8','.','.','.','6','.','.','.','3'},
		{'4','.','.','8','.','3','.','.','1'},
		{'7','.','.','.','2','.','.','.','6'},
		{'.','6','.','.','.','.','2','8','.'},
		{'.','.','.','4','1','9','.','.','5'},
		{'.','.','.','.','8','.','.','7','9'} };

	Sudoku t;
	t.solveSudoku(board);
	for (vector<char> row : board) {
		for (char x : row) cout << x << " ";
		cout << endl;
	}
	return 0;
}
#endif