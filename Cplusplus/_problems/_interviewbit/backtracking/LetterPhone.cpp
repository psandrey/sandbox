
/*
 * Given a digit string, return all possible letter combinations that the number could represent.
 *
 * A mapping of digit to letters (just like on the telephone buttons) is given below.
 * {'0'},{'1'},
 *   2              3             4                 5
 * {'a', 'b', 'c'},{'d','e','f'}, {'g', 'h', 'i'}, {'j','k','l'},
 *   6              7                8               9
 * {'m','n','o'}, {'p','q','r','s'}, {'t','u','v'}, {'w','x','y','z'} };
 *
 * The digit 0 maps to 0 itself.
 * The digit 1 maps to 1 itself.
 *
 * Input: Digit string "23"
 * Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
 * Make sure the returned strings are lexicographically sorted.
 */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class LetterPhone {
public:
	vector<vector<char>> letter_map = { {'0'},{'1'},
				// 2              3             4                 5
				{'a', 'b', 'c'},{'d','e','f'}, {'g', 'h', 'i'}, {'j','k','l'},
				// 6              7                8               9
				{'m','n','o'}, {'p','q','r','s'}, {'t','u','v'}, {'w','x','y','z'} };

	vector<string> letter_phone(string input) {
		int n = (int)input.size();
		vector<string> ans;
		if (n == 0) return ans;

		vector<char> stack;
		cartesian(ans, input, stack, 0, n);
	
		sort(ans.begin(), ans.end());
		return ans;
	}

	void cartesian(vector<string>& ans, string input, vector<char>& stack, int k, int n) {
		if (k == n) ans.push_back(build_string(stack));
		else {
			int key = input[k] - 48;
			int m = (int)letter_map[key].size();
			for (int i = 0; i < m; i++) {
				stack.push_back(letter_map[key][i]);
				cartesian(ans, input, stack, k + 1, n);
				stack.pop_back();
			}
		}
	}

	string build_string(vector<char>& stack) {
		string ans;
		if (!stack.empty()) {
			int m = (int)stack.size();
			for (int i = 0; i < m; i++)
				ans = ans + stack[i];
		}

		return ans;
	}
};

#if 0
int main(void) {
	//string input = "23";
	string input = "2";
	LetterPhone t;
	vector<string> ans = t.letter_phone(input);
	for (string x : ans) cout << x << endl;

	return 0;
}
#endif