
/*
 * Given a set of distinct integers, S, return all possible subsets.
 *
 *  Note:
 * Elements in a subset must be in non-descending order.
 * The solution set must not contain duplicate subsets.
 * Also, the subsets should be sorted in ascending ( lexicographic ) order.
 * The list is not necessarily sorted.
 *
 *  Example : If S = [1,2,3], a solution is:
 *
 * [
 *   [],
 *   [1],
 *   [1, 2],
 *   [1, 2, 3],
 *   [1, 3],
 *   [2],
 *   [2, 3],
 *   [3],
 * ]
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class Subset {
public:
	vector<vector<int>> subsets(vector<int>& a) {
		vector<int> subset;
		vector<vector<int>> ans;
		if (a.size() == 0) { ans.push_back(subset);  return ans; }
		sort(a.begin(), a.end());

		helper(ans, subset, -1, a, (int)a.size());
		
		return ans;
	}

	void helper(vector<vector<int>>& ans, vector<int>& subset, int k, vector<int>& a, int n) {
		if (k == n) return;
		ans.push_back(subset);

		for (int j = k + 1; j < n; j++) {
			subset.push_back(a[j]);
			helper(ans, subset, j, a, n);
			subset.pop_back();
		}
	}


};

#if 0
int main(void) {
	vector<int> a = { 3, 1, 1, 1};
	Subset t;

	vector<vector<int>> ans = t.subsets(a);
	for (vector<int> v : ans){
		for (int x : v) cout << x << " ";
		cout << endl;
	}

	return 0;
}
#endif