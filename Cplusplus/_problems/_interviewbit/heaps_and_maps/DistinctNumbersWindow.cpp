
/*
 * You are given an array of N integers, A1, A2 ,�, AN and an integer K.
 * Return the of count of distinct numbers in all windows of size K.
 */

#include <vector>
#include <iostream>
#include <unordered_map>
using namespace std;

class DistinctNumbersWindow {
public:
	vector<int> dNums(vector<int>& a, int k) {
		vector<int> ans;
		int n = (int)a.size();
		if (k > n) return ans;
#if 1
		unordered_map<int, int> mp;
		for (int j = 0; j < n; j++) {
			mp[a[j]] = j; // update a[j]'s last appearance
			if (j - k + 1 >= 0) { // do we need to count a window ?
				if (j - k >= 0 && mp[a[j - k]] == j - k) // last appearance a[j - k] falls outside of window ?
					mp.erase(mp.find(a[j - k]));

				ans.push_back(mp.size());
			}
		}
#else
		unordered_map<int, int> mp;
		int i = 0, j = 0;
		while (j < n) {
			while (j < n && j - i < k) {
				if (mp.find(a[j]) == mp.end()) mp[a[j]] = 1;
				else mp[a[j]]++;
				j++;
			}
			ans.push_back(mp.size());

			mp[a[i]]--;
			if (mp[a[i]] == 0) mp.erase(a[i]);
			i++;
		}
#endif
		return ans;
	}

};

#if 0
int main(void) {
	vector<int> a = {1, 2, 1, 3, 4, 3};
	int k = 3;
	//vector<int> a = {1 };
	//int k = 1;


	DistinctNumbersWindow t;
	vector<int> ans = t.dNums(a, k);
	for (int x : ans) cout << x << " ";
	cout << endl;

	return 0;
}
#endif