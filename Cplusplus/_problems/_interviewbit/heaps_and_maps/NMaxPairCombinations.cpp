
/*
 * Given two arrays A & B of size N each.
 * Find the maximum n elements from the sum combinations (Ai + Bj) formed from elements
 * in array A and B.
 *
 * For example if A = [1,2], B = [3,4], then possible pair sums can be 1+3 = 4,
 *  1+4=5 , 2+3=5 , 2+4=6 and maximum 2 elements are 6, 5
 *
 *  Example:
 *  N = 4
 *  a[]={1,4,2,3}
 *  b[]={2,5,1,6}
 *
 *  Maximum 4 elements of combinations sum are
 *	10 (4+6), 9 (3+6), 9 (4+5), 8 (2+6)
 */

#include <set>
#include <queue>
#include <vector>
#include <functional>
#include <iostream>
using namespace std;

class NMaxPairCombinations {
public:
	// T(n) = O(n^2), S(n) = O(n^2)
	vector<int> solve_v1(vector<int>& a, vector<int>& b) {
		vector<int> ans;
		if (a.empty()) return ans;
		int n = (int)a.size();

		priority_queue<int> pq;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				pq.push(a[i] + b[j]);

		for (int i = 0; i < n; i++) {
			ans.push_back(pq.top()); pq.pop();
		}

		return ans;
	}

	// T(n) = O(n.logn), S(n) = O(n^2)
	vector<int> solve_v2(vector<int>& a, vector<int>& b) {
		vector<int> ans;
		if (a.empty()) return ans;
		int n = (int)a.size();

		sort(a.begin(), a.end(), greater<int>());
		sort(b.begin(), b.end(), greater<int>());

		struct entry {
			int s;
			int i, j;
			entry(int ii, int jj, int ss): s(ss), i(ii), j(jj) {}
		};

		struct compare {
			bool operator()(const struct entry& x, const struct entry& y) {
				return (x.s < y.s);
			}
		};
		priority_queue<struct entry, vector<struct entry>, compare> pq;
		set<pair<int, int>> hs;

		struct entry e(0, 0, a[0] + b[0]);
		pair<int, int> p = make_pair(0, 0);
		pq.push(e); hs.insert(p);
		for (int j = 0; j < n; j++) {
			e = pq.top(); pq.pop();
			ans.push_back(e.s);
			
			p = make_pair(e.i + 1, e.j);
			if (e.i + 1 < n && hs.find(p) == hs.end()) {
				struct entry newe(e.i + 1, e.j, a[e.i + 1] + b[e.j]);
				pq.push(newe); hs.insert(p);
			}

			p = make_pair(e.i, e.j + 1);
			if (e.j + 1 < n && hs.find(p) == hs.end()) {
				struct entry newe(e.i, e.j + 1, a[e.i] + b[e.j + 1]);
				pq.push(newe); hs.insert(p);
			}
		}

		return ans;
	}
};

#if 0
int main(void) {
	//vector<int> a = { 1,4,2,3 };
	//vector<int> b = { 2,5,1,6 };
	vector<int> a = { 3, 2, 4, 2 };
	vector<int> b = { 4, 3, 1, 2 };

	NMaxPairCombinations t;
	//vector<int> ans = t.solve_v1(a, b);
	//for (int x : ans) cout << x << " ";

	vector<int> ans2 = t.solve_v2(a, b);
	for (int x : ans2) cout << x << " ";
	return 0;
}
#endif