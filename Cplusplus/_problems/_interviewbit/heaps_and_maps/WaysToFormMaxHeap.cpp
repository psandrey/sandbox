
/*
 * Max Heap is a special kind of complete binary tree in which for every node the value
 *  present in that node is greater than the value present in it�s children nodes.
 *
 * So now the problem statement for this question is:
 *  How many distinct Max Heap can be made from n distinct integers
 *  In short, you have to ensure the following properties for the max heap :
 *  1. Heap has to be a complete binary tree (A complete binary tree is a binary tree
 *   in which every level, except possibly the last, is completely filled, and all nodes
 *   are as far left as possible. )
 *  2. Every node is greater than all its children
 */

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class WaysToFormMaxHeap {
public:

	const unsigned long long m = 1000000007ULL;

	int solve(int n) {
		if (n == 0) return 0;
		if (n == 1) return 1;

		// pre-compute combinations (pascal triangle)
		vector<vector<int>> nCk;
		for (int i = 0; i < n + 1; i++) {
			vector<int> row = vector<int>(i + 1);	
			row[0] = 1;
			if (i > 0) {
				for (int j = 1; j < i; j++) {
					unsigned long long n_1Ck_1 = nCk[i - 1][j - 1];
					unsigned long long n_1Ck   = nCk[i - 1][j];
					unsigned long long nCk = (n_1Ck_1 + n_1Ck) % m;
					row[j] = (int)nCk;
				}
				row[i] = 1;
			}
			nCk.push_back(row);
		}

		// memoized computed ways:
		//  for the Right subtrees, the ways were already computed in the Left subtree
		//  thus, memoization make sense.
		vector<int> memo(n + 1, -1); memo[0] = 1; memo[1] = 1;
		return ways(n, nCk, memo);
	}

	int ways(int n, vector<vector<int>>& nCk, vector<int>& memo) {
		if (memo[n] != -1) return memo[n];
	
		int h = (int)floor(log10(n) / log10(2));
		int B = (int)pow(2, h); // no of elements in the bottom layer in the underlying complete BT
		int b = n + 1 - (int)pow(2, h); // acctual no of elements in the bottom layer in the BT
		int l, r; // the no of elements in the left and right subtree

		// is the left subtree complete?
		if (b >= B / 2) l = (int)pow(2, h) - 1;
		else l = n - (int)pow(2, h - 1);
		r = n - 1 - l; // n - 1 = l + r

		unsigned long long left_ways = ways(l, nCk, memo);
		unsigned long long right_ways = ways(r, nCk, memo);
		unsigned long long total_ways = (left_ways * right_ways) % m;
		memo[n] = (int)((nCk[n - 1][l] * total_ways) % m); // there are n_1Cl configuration for each way

		return memo[n];
	}
};

#if 0
int main(void) {
	WaysToFormMaxHeap t;
	cout << t.solve(100) << endl;

	return 0;
}
#endif