
/*
 * Design and implement a data structure for Least Recently Used (LRU) cache.
 * It should support the following operations: get and set.
 * get(key) - Get the value (will always be positive) of the key if the key exists in the cache,
 *  otherwise return -1.
 * set(key, value) - Set or insert the value if the key is not already present.
 *  When the cache reached its capacity, it should invalidate the least recently used item
 *  before inserting a new item.
 *
 * Note: Definition of �least recently used� : An access to an item is defined as a get or a set
 *       operation of the item. �Least recently used� item is the one with the oldest access time.
 */

#include <iostream>
#include <unordered_map>
using namespace std;

class LRUCache {
public:
#if 1
	// this version uses std::list
	list<pair<int, int>> h;
	unordered_map<int, list<pair<int, int>>::iterator> mp;
	int cap = 0;

	LRUCache(int capacity) {
		if (capacity < 1) return; // should throw an exception

		h.clear(); mp.clear();
		cap = capacity;
	}

	int get(int k) {
		if (mp.find(k) == mp.end()) return -1;
		
		list<pair<int, int>>::iterator it = mp[k];
		int v = it->second;

		h.erase(it);
		h.push_front(make_pair(k, v));
		mp[k] = h.begin();

		return v;
	}

	void set(int k, int v) {
		if (mp.find(k) != mp.end()) {
			list<pair<int, int>>::iterator it = mp[k];
			h.erase(it);
		} else if (mp.size() == cap) {
			mp.erase(h.back().first);
			h.pop_back();
		}

		h.push_front(make_pair(k, v));
		mp[k] = h.begin();
	}

#else
	// this version uses custom linked list
	struct LRU_ListNode {
		LRU_ListNode* prev, * next;
		int k, v;
		LRU_ListNode(int key, int val) : prev(NULL), next(NULL), k(key), v(val)
			{ prev = this; next = this; };
	};

	LRU_ListNode* remove(LRU_ListNode** h, LRU_ListNode* m) {
		if (m->next == m && m->prev == m) (*h) = NULL;
		else {
			if ((*h) == m) (*h) = m->next;
			LRU_ListNode* p = m->prev, * n = m->next;
			p->next = n; n->prev = p;
		}

		return m;
	}

	void insert_front(LRU_ListNode** h, LRU_ListNode* m) {
		if (*h == NULL) *h = m;
		else {
			LRU_ListNode* p = (*h)->prev;
			p->next = m;
			m->next = *h; m->prev = p;
			(*h)->prev = m;
			(*h) = m;
		}
	}

	LRU_ListNode* h = NULL;
	unordered_map<int, LRU_ListNode*> mp;
	int cap = 0;
	
	LRUCache(int capacity) {
		if (capacity < 1) return; // should throw an exception
		h = NULL;
		mp = unordered_map<int, LRU_ListNode*>();
		cap = capacity;
	}

	int get(int k) {
		if (mp.find(k) == mp.end()) return -1;

		LRU_ListNode* m = mp[k];
		remove(&h, m);
		insert_front(&h, m);
		return m->v;
	}

	void set(int k, int v) {
		LRU_ListNode* m = NULL;
		if (mp.find(k) != mp.end()) {
			m = mp[k];
			remove(&h, m);
			m->v = v;
		} else {
			if (mp.size() == cap)
				mp.erase(remove(&h, h->prev)->k);

			m = new LRU_ListNode(k, v);
			mp[k] = m;
		}

		insert_front(&h, m);
	}
#endif
};

#if 0
int main(void) {
	int cap = 1;

	LRUCache t(cap);
	t.set(1, 10);
	t.set(5, 12);
	cout << t.get(5) << endl; // returns 12
	cout << t.get(1) << endl; // returns 10
	cout << t.get(10) << endl; // returns - 1
	t.set(6, 14); // this pushes out key = 5 as LRU is full.
	cout << t.get(5) << endl; // returns -1

	return 0;
}
#endif