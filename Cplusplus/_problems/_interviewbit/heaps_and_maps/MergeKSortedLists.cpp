
/*
 * Merge k sorted linked lists and return it as one sorted list.
 */

#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class MergeKSortedLists {
public:
	struct ListNode {
		int val;
		ListNode* next;
		ListNode(int x) : val(x), next(NULL) {}
	};

	ListNode* mergeKLists(vector<ListNode*>& a) {
		struct compare {
			bool operator()(const ListNode* n, const ListNode* m) {
				return (n->val > m->val);
			}
		};
		priority_queue<ListNode*, vector<ListNode*>, compare> pq;
		for (int j = 0; j < (int)a.size(); j++) pq.push(a[j]);

		ListNode* list = NULL, *head = NULL;
		while (!pq.empty()) {
			ListNode* n = pq.top(); pq.pop();
			ListNode* m = n->next;
			if (m != NULL) pq.push(m);
			n->next = NULL;

			if (list == NULL) { list = n; head = n; }
			else { head->next = n; head = n; }
		}

		return list;
	}

	// debug
	vector<ListNode*> build_lists(vector<vector<int>>& arrs) {
		vector<ListNode*> vl;
		for (int i = 0; i < (int)arrs.size(); i++) {
			ListNode* root = NULL, *head = NULL;
			for (int j = 0; j < (int)arrs[i].size(); j++) {
				ListNode* n = new ListNode(arrs[i][j]);
				if (root == NULL) { root = n; head = n; }
				else { head->next = n; head = n; }
			}
			vl.push_back(root);
		}

		return vl;
	}
};

#if 0
int main(void) {
	
	MergeKSortedLists t;
	vector<vector<int>> arrs = {
		{ 1, 3, 5, 7 },
		{ 2, 4, 6, 8 },
		{ 0, 9, 10, 11 }
	};

	vector<MergeKSortedLists::ListNode*> lists = t.build_lists(arrs);
	MergeKSortedLists::ListNode* list = t.mergeKLists(lists);
	while (list != NULL) {
		cout << list->val << "  ";
		list = list->next;
	}
	cout << endl;

	return 0;
}
#endif