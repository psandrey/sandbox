
/*
 * The Longest Increasing Subsequence (LIS) problem is to find the length of the longest subsequence
 * of a given sequence such that all elements of the subsequence are sorted in increasing order.
 * For example, the length of LIS for {10, 22, 9, 33, 21, 50, 41, 60, 80} is 6 and LIS is {10, 22, 33, 50, 60, 80}.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class LIS_LongestIncreasingSubsequence {
public:
	int LIS(vector<int>& a) {
		int ans = 1;
#if 0
		ans = LIS_bf(a, 0, INT_MIN);
#else
		// memoization
		vector<int> memo(a.size(), -1);
		ans = 0;
		for (int n = 0; n < (int)a.size(); n++)
			ans = max(ans, LIS_memo(a, n, memo));

		// tabulation
		//ans = LIS_dp(a);
#endif
		return ans;
	}

private:
	int LIS_bf(vector<int>& a, int i, int prev) {
		if (i == a.size()) return 0;

		int take = 0, notake = 0;
		if (a[i] > prev) take = 1 + LIS_bf(a, i + 1, a[i]);
		notake = LIS_bf(a, i + 1, prev);
		
		return max(take, notake);
	}

	// return the longest increasing substring ending in a[i]
	int LIS_memo(vector<int>& a, int i, vector<int>& memo) {
		if (i == 0) return 1;
		if (memo[i] != -1)
			return memo[i];

		int ans = 1;
		for (int j = 0; j < i; j++)
			if (a[j] < a[i]) ans = max(ans, 1 + LIS_memo(a, j, memo));

		memo[i] = ans;
		return memo[i];
	}

	int LIS_dp(vector<int>& a) {
		vector<int> dp(a.size());

		dp[0] = 1;
		int ans = 1;
		for (int i = 1; i < (int)a.size(); i++) {
			dp[i] = 1;
			for (int j = 0; j < i; j++) {
				if (a[i] > a[j]) dp[i] = max(dp[i], dp[j] + 1);
			}
			ans = max(ans, dp[i]);
		}

		return ans;
	}
};

#if 0
int main(void) {
	vector<int> a = { 10, 22, 9, 33, 21, 50, 41, 80, 60 };
	//vector<int> a = { 3, 10, 2, 4, 20 };


	LIS_LongestIncreasingSubsequence t;
	cout << t.LIS(a);

	return 0;
}
#endif