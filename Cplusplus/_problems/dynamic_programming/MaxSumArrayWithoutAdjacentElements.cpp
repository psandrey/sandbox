
/*
 * Given an array of positive numbers, find the maximum sum of a subsequence with the constraint that
 * no 2 numbers in the sequence should be adjacent in the array.
 * So 3 2 7 10 should return 13 (sum of 3 and 10) or 3 2 5 10 7 should return 15 (sum of 3, 5 and 7).
 */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

class MaxSumArrayWithoutAdjacentElements {
public:
	int max_sum_bf(vector<int>& a) {
		return helper_bf(a, 0, 0);
	}

	int helper_bf(vector<int>& a, int i, int sum) {
		int n = (int)a.size();
		if (i >= n) return sum;

		int max1 = sum;
		if (sum + a[i] >= 0)
			max1 = helper_bf(a, i + 2, sum + a[i]);
		int max2 = helper_bf(a, i + 1, sum);

		return max(max1, max2);
	}

	int max_sum_dp(vector<int>& a) {
		int n = (int)a.size();
		vector<int> dp(n+1);
		dp[0] = a[0]; dp[1] = a[1];
		int ans = max(dp[0], dp[1]);
		for (int i = 2; i < n; i++)
			dp[i] = max(dp[i - 2] + a[i], dp[i - 1]);

		return ans;
	}

};

#if 0
int main(void) {
	vector<int> a = { 12, 9, -12, -33 };
	MaxSumArrayWithoutAdjacentElements t;
	cout << t.max_sum_bf(a) << endl;
	cout << t.max_sum_dp(a) << endl;
	return 0;
}
#endif