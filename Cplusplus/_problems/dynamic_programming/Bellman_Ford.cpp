
/*
 * Single Source Shortest Paths: Bellman-Ford algorithm.
 * Note: memoized and tabulation versions.
 */

#include <queue>
#include <vector>
#include <iostream>
using namespace std;

class Bellman_Ford {
public:
	vector<int> bellman(int n, vector<vector<int>>& w, int s) {
#if 1
		// memoized
		vector<vector<int>> memo(n, vector<int>(n, INT_MIN));

		vector<int> sssp(n, INT_MAX);
		for (int u = 0; u < n; u++)
			sssp[u] = BF_memo(s, u, n - 1, w, memo);

		return sssp;
#else
		// tabulation
		return BF_tab(s, n, w);
#endif
	}

	// memoized version
	int BF_memo(int s, int u, int k, vector<vector<int>>& w, vector<vector<int>>& memo) {
		if (u == s) return 0;
		if (k == 0) return INT_MAX;
		if (memo[u][k] != INT_MIN)
			return memo[u][k];

		int n = (int)w.size();
		int d = INT_MAX;
		for (int v = 0; v < n; v++) {
			if (v == u || w[v][u] == INT_MAX) continue;

			int dv = BF_memo(s, v, k - 1, w, memo);
			if (dv == INT_MAX) continue;
			d = min(d, dv + w[v][u]);
		}

		memo[u][k] = d;
		return memo[u][k];
	}

	// tabulation version
	vector<int> BF_tab(int s, int n, vector<vector<int>>& w) {
		vector<vector<int>> d(n, vector<int>(n, INT_MAX));
		
		d[s][0] = 0;
		for (int k = 1; k < n; k++) {
			for (int u = 0; u < n; u++) {
				d[u][k] = d[u][k - 1];
				for (int v = 0; v < n; v++) {
					if (v == u || w[v][u] == INT_MAX) continue;
					if (d[v][k - 1] == INT_MAX) continue;

					d[u][k] = min(d[u][k], d[v][k - 1] + w[v][u]);
				}
			}
		}

		vector<int> sssp(n);
		for (int u = 0; u < n; u++) sssp[u] = d[u][n - 1];
		return sssp;
	}

	vector<vector<int>> from_edges_to_weights(vector<vector<int>>& edges, int n) {
		vector<vector<int>> w(n, vector<int>(n, INT_MAX));

		int m = (int)edges.size();
		for (int j = 0; j < m; j++) {
			int u = edges[j][0];
			int v = edges[j][1];
			int c = edges[j][2];
			w[u][v] = c;
		}

		return w;
	}
};

#if 0
int main(void) {
	//                             u, v, w
	vector<vector<int>> edges = { { 0, 1, 3 }, { 0, 4, -2 }, { 1, 2, 1 }, { 2, 3, 1 }, {2, 5, 2},
								  { 3, 4, 1 }, { 3, 5, 1 }, {4, 1, 2}, { 4, 2, 4 } };
	int n = 6;
	
	Bellman_Ford t;
	vector<vector<int>> w = t.from_edges_to_weights(edges, n);
	
	vector<int> sssp = t.bellman(n, w, 0);
	for (int u = 0; u < n; u++)
		cout << u << " : " << sssp[u] << endl;

	return 0;
}
#endif