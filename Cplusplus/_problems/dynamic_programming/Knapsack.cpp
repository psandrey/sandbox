
/*
 * Given weights and values of n items, put these items in a knapsack of capacity W to get the maximum total value
 * in the knapsack. In other words, given two integer arrays val[0..n-1] and wt[0..n-1] which represent
 * values and weights associated with n items respectively. Also given an integer W which represents knapsack capacity,
 * find out the maximum value subset of val[] such that sum of the weights of this subset is smaller than or equal to W.
 * You cannot break an item, either pick the complete item, or don�t pick it (0-1 property).
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class Knapsack {

public:
	int knap(vector<int>& w, vector<int>& p, int cap) {
		int ans = 0;
		int n = (int)w.size();
#if 0
		// divide and conquer
		ans = knap_dc(w, p, n, cap);
#else
	#if 0
		// memoization
		vector<vector<int>> memo(cap + 1, vector<int>(n + 1, -1));
		ans = knap_memo(w, p, n, cap, memo);
	#else
		// tabulation
		#if 0
				ans = knap_tab_v1(w, p, cap);
		#else
				ans = knap_tab_v2(w, p, cap);
		#endif
	#endif
#endif
		return ans;
	}

	// The device and conquer version
	// T(n) = O(2^n), S(n) = O(W) - due to recursive calls
	int knap_dc(vector<int>& w, vector<int>& p, int k, int cap) {
		if (cap == 0 || k == 0) return 0;

		int take = 0;
		int notake = knap_dc(w, p, k - 1, cap);
		if (w[k - 1] <= cap) take = knap_dc(w, p, k - 1, cap - w[k - 1]) + p[k - 1];

		return max(take, notake);
	}

	// The dynamic-programming version with memoization
	// T(n) = O(W.n), S(n) = O(W.n)
	int knap_memo(vector<int>& w, vector<int>& p, int k, int cap, vector<vector<int>>& memo) {
		if (memo[cap][k] != -1)	return memo[cap][k];
		if (cap == 0 || k == 0) memo[cap][k] = 0;
		else {
			int take = 0;
			int notake = knap_memo(w, p, k - 1, cap, memo);
			if (w[k - 1] <= cap) take = knap_memo(w, p, k - 1, cap - w[k - 1], memo) + p[k - 1];

			memo[cap][k] = max(take, notake);
		}
		return memo[cap][k];
	}

	// The dynamic-programming version with tabulation (1)
	// T(n) = O(W.n), S(n) = O(W.n)
	int knap_tab_v1(vector<int>& w, vector<int>& p, int W) {
		int n = (int) w.size();

		vector<vector<int>> dp(W + 1, vector<int>(n + 1));
		for (int cap = 0; cap <= W; cap++) dp[cap][0] = 0;

		for (int k = 1; k <= n; k++)
			for (int cap = 0; cap <= W; cap++) {
				if (w[k - 1] > cap) dp[cap][k] = dp[cap][k - 1];
				else dp[cap][k] = max(dp[cap][k - 1], dp[cap - w[k - 1]][k - 1] + p[k - 1]);
			}
	
		//printvv(dp);
		return dp[W][n];
	}

	// The dynamic-programming version with tabulation (1)
	// T(n) = O(W.n), S(n) = O(W)
	int knap_tab_v2(vector<int>& w, vector<int>& p, int W) {
		int n = (int)w.size();

		vector<vector<int>> dp(W + 1, vector<int>(2, 0));

		int j = 0;
		for (int k = 1; k <= n; k++) {
			j ^= 1;
			for (int cap = 0; cap <= W; cap++) {
				if (w[k - 1] > cap) dp[cap][j ^ 1] = dp[cap][j ^ 1];
				else dp[cap][j] = max(dp[cap][j ^ 1], dp[cap - w[k - 1]][j ^ 1] + p[k - 1]);
			}
		}

		return dp[W][j];
	}

	// debug
	void printvv(vector<vector<int>>& dp) {
		int n = (int)dp.size();
		int m = (int)dp[0].size();

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				cout << dp[i][j] << " ";
			}
			cout << endl;
		}
	}
};

#if 0
int main(void) {

	// p: 6
	//vector<int> ww = { 1, 1, 1 };
	//vector<int> pp = { 1, 2, 4 };
	//int W = 2;

	// p: 309
	//                   1   1   1   1   0   1   0   0   0   0
	//vector<int> ww = { 23, 31, 29, 44, 53, 38, 63, 85, 89, 82 };
	//vector<int> pp = { 92, 57, 49, 68, 60, 43, 67, 84, 87, 72 };
	//int W = 165;
	
	
	// p: 9
	//                 0  1  1  0
	vector<int> ww = { 1, 3, 4, 5 };
	vector<int> pp = { 1, 4, 5, 7 };
	int W = 7;

	Knapsack t;
	cout << "value:" << t.knap(ww, pp, W) << endl;

	return 0;
}
#endif