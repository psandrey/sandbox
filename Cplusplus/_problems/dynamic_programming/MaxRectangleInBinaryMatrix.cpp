
/*
 * Given a 2D binary matrix filled with 0�s and 1�s, find the largest rectangle containing
 * all ones and return its area.
 *
 * Note: This is just the editorial version from interviewbit. Improvement over O(n^4) to O(n^3)
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MaxRectangleInBinaryMatrix {
public:
	int maximalRectangle(vector<vector<char> >& matrix) {
		int rows = matrix.size();
		if (rows == 0) return 0;
		int cols = matrix[0].size();
		if (cols == 0) return 0;
		vector<vector<int>> max_x(rows, vector<int>(cols, 0));

		int area = 0;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				if (matrix[i][j] == 1) {
					if (j == 0) max_x[i][j] = 1;
					else max_x[i][j] = max_x[i][j - 1] + 1;
					int y = 1;
					int x = cols;
					while ((i - y + 1 >= 0) && (matrix[i - y + 1][j] == 1)) {
						x = min(x, max_x[i - y + 1][j]);
						area = max(area, x * y);
						y++;
					}
				}
			}
		}

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				cout << max_x[i][j] << " ";
			}
			cout << endl;
		}

		return area;
	}
};

#if 0
int main(void) {
	vector<vector<char>> matrix = { {0, 1, 1, 1, 1},
									{0, 1, 0, 1, 1},
									{0, 1, 0, 0, 0},
									{1, 0, 1, 1, 1},
									{0, 1, 1, 1, 1},
									{0, 1, 1, 0, 0}};

	MaxRectangleInBinaryMatrix t;

	cout << t.maximalRectangle(matrix) << endl;
	return 0;
}
#endif