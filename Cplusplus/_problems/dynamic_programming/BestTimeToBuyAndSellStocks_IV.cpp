
/*
 * Say you have an array for which the ith element is the price of a given stock on day i.
 * Design an algorithm to find the maximum profit. You may complete at most k transactions.
 *
 * Note: You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
 *
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class BestTimeToBuyAndSellStocks_IV {
public:
	int maxProfit(const vector<int>& a, int K) {
		if (a.empty() || a.size() < 2) return 0;
#if 0
		// memoization
		int n = (int)a.size();
		vector<vector<int>> memo(n, vector<int>(K + 1, -1));
		return DP_memo(n - 1, K, a, memo);
#else
		// tabulation
		#if 0
			return DP_tab1(a, K);
		#else
			return DP_tab2(a, K);
		#endif
#endif
	}

	// memoization:
	// T(n) = O(k.n^2)
	int DP_memo(int i, int k, vector<int> a, vector<vector<int>> memo) {
		if (memo[i][k] != -1) return memo[i][k];

		if (i == 0 || k == 0) memo[i][k] = 0;
		else {
			int x = DP_memo(i - 1, k, a, memo);
			int y = 0;
			for (int j = 0; j < i; j++)
				y = max(y, a[i] - a[j] + DP_memo(j, k - 1, a, memo));
			memo[i][k] = max(x, y);
		}

		return memo[i][k];
	}

	// tabulation version 1:
	// T(n) = O(k.n^2)
	int DP_tab1(vector<int> a, int K) {
		int n = (int)a.size();

		vector<vector<int>> dp(K + 1, vector<int>(n));
		for (int i = 0; i < n; i++) dp[0][i] = 0;
		for (int k = 0; k <= K; k++) dp[k][0] = 0;

		for (int k = 1; k <= K; k++)
			for (int i = 1; i < n; i++) {
				int y = 0;
				for (int j = 0; j < i; j++) 
					y = max(y, dp[k - 1][j] + (a[i] - a[j]));
				dp[k][i] = max(dp[k][i - 1], y);
			}

		return dp[K][n - 1];
	}

	// tabulation version 2:
	// T(n) = O(k.n)
	int DP_tab2(vector<int> a, int K) {
		int n = (int)a.size();

		vector<vector<int>> dp(K + 1, vector<int>(n));
		for (int i = 0; i < n; i++) dp[0][i] = 0;
		for (int k = 0; k <= K; k++) dp[k][0] = 0;

		for (int k = 1; k <= K; k++) {
			int maxDiff = -a[0];
			for (int i = 1; i < n; i++) {
				dp[k][i] = max(dp[k][i - 1], maxDiff + a[i]);
				maxDiff = max(maxDiff, dp[k - 1][i] - a[i]);
			}
		}

		return dp[K][n - 1];
	}

};

#if 0
int main(void) {
	vector<int> a = { 2, 5, 7, 1, 4, 3, 1, 3 };
	int k = 3;
	BestTimeToBuyAndSellStocks_IV t;

	cout << "DP: " << t.maxProfit(a, k) << endl;

	return 0;
}
#endif