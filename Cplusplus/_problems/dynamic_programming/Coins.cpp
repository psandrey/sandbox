
/*
 * Given a value S, and an infinite supply of c = { c1, c2, .. , cn} valued coins:
 * 1. In how many ways we can pay the value S with the given coins?
 * 2. What is the minimum number of coins we need to pay value S? 
*/

#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

class Coins {
public:

	// Question 1:
	// ------------------------------------------------------------------------
	int coins_ways(vector<int>& c, int S) {
		int ans = 0;
		if (c.size() == 0) return ans;
#if 0
		ans = coins_ways_bf(c, 0, S);
#else
	#if 0
		// memoization
		int n = (int)c.size();
		vector<vector<int>> memo(S + 1);
		for (int i = 0; i <= S; i++) memo[i] = vector<int>(n, -1);
	
		ans = coins_ways_memo(c, 0, S, memo);
	#else
		// tabulation
		//ans = coins_ways_tab(c, S);

		// tabulation (pile coins)
		ans = coins_ways_tab_pile(c, S);
	#endif
#endif
		return ans;
	}

	// Question 2:
	// ------------------------------------------------------------------------
	int coins_min(vector<int>& c, int S) {
		int ans = 0;
		if (c.size() == 0) return ans;
#if 0
		ans = coins_min_bf(c, S);
#else
	#if 0
		int n = (int)c.size();
		vector<int> memo(S + 1, -1);

		ans = coins_min_memo(c, S, memo);
	#else
		ans = coins_min_tab(c, S);
	#endif
#endif

		return ans;
	}

private:
	int coins_ways_bf(vector<int>& c, int i, int S) {
		if (S == 0) return 1;
		if (i == (int)c.size()) return -1;

		int n_i = S / c[i]; int ans = 0;
		for (int j = 0; j <= n_i; j++) {
			int ways = coins_ways_bf(c, i + 1, S - j * c[i]);
			if (ways != -1) ans = ans + ways;
		}

		return ans;
	}

	int coins_ways_memo(vector<int>& c, int i, int S, vector<vector<int>>& memo) {
		if (S == 0)
			return 1;
		if (i == (int)c.size()) return 0;
		if (memo[S][i] != -1) return memo[S][i];

		int take = 0, notake = 0;
		if (S >= c[i]) take = coins_ways_memo(c, i, S - c[i], memo);
		notake = coins_ways_memo(c, i + 1, S, memo);

		memo[S][i] = take + notake;
		return memo[S][i];
	}

	int coins_ways_tab(vector<int>& c, int S) {
		int n = (int)c.size();
		vector<vector<int>> dp(S + 1);
		for (int i = 0; i <= S; i++) dp[i] = vector<int>(n + 1, 0);
		for (int i = 0; i <= n; i++) dp[0][i] = 1;

		for (int s = 1; s <= S; s++)
			for (int i = 1; i <= n; i++) {
				dp[s][i] = dp[s][i - 1];
				if (s >= c[i - 1]) dp[s][i] += dp[s - c[i - 1]][i];
			}

		return dp[S][n];
	}

	int coins_ways_tab_pile(vector<int>& c, int S) {
		int n = (int)c.size();
		vector<int> dp(S + 1, 0);
		dp[0] = 1;
		for (int i = 0; i < n; i++)
			for (int s = 1; s <= S; s++)
				if (s >= c[i]) dp[s] = dp[s] + dp[s - c[i]];

		return dp[S];
	}

	//----------------------------------------------------------------------------

	// The brute-force version
	// T(n) = O(S^n), S(n) = O(S) - due to recursivity
	int coins_min_bf(vector<int>& c, int S) {
		if (S == 0) return 0;

		int ans = INT_MAX;
		int n = (int)c.size();
		for (int i = 0; i < n; i++) {
			int ans_i = INT_MAX;
			if (c[i] <= S) ans_i = coins_min_bf(c, S - c[i]);
			ans = (ans_i == INT_MAX ? ans : min(ans, ans_i + 1));
		}

		return ans;
	}

	// The dynamic-programming version with memoization
	// T(n) = O(S.n), S(n) = O(S) - due to recursivity
	int coins_min_memo(vector<int>& c, int S, vector<int>& memo) {
		if (S == 0) return 0;
		if (memo[S] != -1) return memo[S];

		int ans = INT_MAX;
		int n = (int)c.size();
		for (int i = 0; i < n; i++) {
			int ans_i = INT_MAX;
			if (c[i] <= S) ans_i = coins_min_memo(c, S - c[i], memo);
			ans = (ans_i == INT_MAX ? ans : min(ans, ans_i + 1));
		}

		memo[S] = ans;
		return ans;
	}

	// The dynamic-programming version with tabualtion
	// T(n) = O(S.n), S(n) = O(S)
	int coins_min_tab(vector<int>& c, int S) {
		int n = (int)c.size();
		vector<int> dp(S + 1, S + 1);
		
		dp[0] = 0;
		for (int s = 1; s <= S; s++) {
			for (int j = 0; j < n; j++) {
				if (s >= c[j]) dp[s] = min(dp[s], dp[s - c[j]] + 1);
			}
		}

		return (dp[S] > S ? -1 : dp[S]);
	}

private:
};

#if 0
int main(void) {
	//vector<int> a = { 1, 2, 3 };
	//int S = 4;

	vector<int> a = { 1, 5, 10, 25 };
	int S = 100;


	Coins t;
	cout << "Ways: " << t.coins_ways(a, S) << endl;

	cout << "Min conins: " << t.coins_min(a, S) << endl;
	return 0;
}
#endif