
/*
 * Same problem as Knapsack, but you can put in the knapsack unlimited number of items of the same type.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class Knapsack_unbound {

public:
	int knap(vector<int>& w, vector<int>& p, int cap) {
		int ans = 0;
#if 0
		// divide and conquer
		ans = knap_dc(w, p, cap);
#else
	#if 0
		// memoization
		int n = (int)w.size();
		vector<int> memo(cap + 1, -1);

		ans = knap_memo(w, p, cap, memo);
	#else
		// tabulation
		ans = knap_tab(w, p, cap);
	#endif
#endif
		return ans;
	}

private:
	int knap_dc(vector<int>& w, vector<int>& p, int cap) {
		if (cap == 0) return 0;

		int best = 0;
		int n = (int)w.size();
		for (int k = 0; k < n; k++) {
			if (w[k] > cap) continue;
			
			best = max(best, knap_dc(w, p, cap - w[k]) + p[k]);
		}

		return best;
	}

	int knap_memo(vector<int>& w, vector<int>& p, int cap, vector<int>& memo) {
		if (memo[cap] != -1) return memo[cap];
		if (cap == 0) memo[cap] = 0;
		else {
			memo[cap] = 0;
			int n = (int)w.size();
			for (int k = 0; k < n; k++) {
				if (w[k] > cap) continue;

				memo[cap] = max(memo[cap], knap_dc(w, p, cap - w[k]) + p[k]);
			}
		}
		return memo[cap];
	}

	int knap_tab(vector<int>& w, vector<int>& p, int W) {
		int n = (int)w.size();
		vector<int> dp(W + 1);

		dp[0] = 0;
		for (int cap = 1; cap <= W; cap++) {
			dp[cap] = dp[cap - 1];

			for (int k = 0; k < n; k++) {
				if (w[k] <= cap) dp[cap] = max(dp[cap], dp[cap - w[k]] + p[k]);
			}
		}

		return dp[W];
	}
};

#if 0
int main(void) {
	// p: 7
	vector<int> w = { 1, 2, 5 };
	vector<int> p = { 1, 3, 4 };
	int W = 5;

	Knapsack_unbound t;
	cout << "value:" << t.knap(w, p, W) << endl;

	return 0;
}
#endif