
/*
 * Write an efficient program to find the sum of contiguous subarray within
 *  a one-dimensional array of numbers which has the largest sum.
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class LargestSumContiguousSubarray {
public:
	int LSCS(vector<int>& a) {
		int ans = INT_MIN;
#if 0
	#if 1
		for (int n = 0; n < (int)a.size(); n++) {
			int ans_n = LSCS_bf_r(a, n);
			ans = max(ans, ans_n);
		}
	#else
		ans = LSCS_bf_i(a);
	#endif
#else
	#if 0
		// memoization
		vector<int> memo(a.size(), -1);
		ans = 0;
		for (int n = 0; n < (int)a.size(); n++)
			ans = max(ans, LSCS_memo(a, n, memo));
	#else
		// tabulation
		ans = LSCS_dp(a);
	#endif
#endif

		return ans;
	}

private:
	int LSCS_bf_i(vector<int>& a) {
		int ans = INT_MIN;
		for (int i = 0; i < (int)a.size(); i++) {
			int sum = 0;
			for (int j = i; j < (int)a.size(); j++) {
				sum = sum + a[j];
				ans = max(ans, sum);
			}
		}

		return ans;
	}

	int LSCS_bf_r(vector<int>& a, int i) {
		if (i == 0) return a[i];

		int ans = a[i];
		int ans_1 = LSCS_bf_r(a, i - 1);
		if (ans_1 > 0) ans += ans_1;

		return ans;
	}

	// return the maximum sum ending in a[i]
	int LSCS_memo(vector<int>& a, int i, vector<int>& memo) {
		if (i == 0) return 1;
		if (memo[i] != -1) return memo[i];

		int ans_prev = LSCS_memo(a, i - 1, memo);
		int ans = a[i];
		if (ans_prev > 0) ans += ans_prev;

		memo[i] = ans;
		return memo[i];
	}

	int LSCS_dp(vector<int>& a) {
		vector<int> dp(a.size());

		int max_end_here = a[0], max_so_far = a[0];
		for (int i = 1; i < (int)a.size(); i++) {
			max_end_here = max(a[i], max_end_here + a[i]);
			max_so_far = max(max_so_far, max_end_here);
		}

		return max_so_far;
	}
};

#if 0
int main(void) {
	vector<int> a = { -2, -3, 4, -1, -2, 1, 5, -3 };

	LargestSumContiguousSubarray t;
	cout << t.LSCS(a);


	return 0;
}
#endif