
/*
 * Given an array of integers where each element represents the max number of steps that can be made
 * forward from that element. Write a function to return the minimum number of jumps to reach the end of the array
 * (starting from the first element). If an element is 0, then cannot move through that element.
 *
 * Example:
 * Input: arr[] = {1, 3, 5, 8, 9, 2, 6, 7, 6, 8, 9}
 * Output: 3 (1-> 3 -> 8 ->9)
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MinimumNumberOfJumpsToReachEnd {
public:
	int min_jumps(vector<int>& a) {
		int n = (int)a.size();
		if (n == 0) return 1;

		vector<int> dp(n);
		dp[n - 1] = 1;
		for (int i = n - 2; i >= 0; i--) {
			int leap = INT_MAX;
			int m = min(n - 1, i + a[i]);
			for (int j = i + 1; j <= m; j++) {
				if (dp[j] == 0) continue;
				leap = min(leap, dp[j] + 1);
			}

			dp[i] = (leap == INT_MAX ? 0 : leap);
		}

		return (dp[0] - 1);
	}
};

#if 0
int main(void) {
	vector<int> a = { 1, 3, 5, 8, 9, 2, 6, 7, 6, 8, 9 };
	
	MinimumNumberOfJumpsToReachEnd t;
	cout << t.min_jumps(a);

	return 0;
}
#endif