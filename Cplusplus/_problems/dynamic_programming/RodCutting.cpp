
/*
 * Suppose you have a rod of length n, and you want to cut up the rod and sell the pieces in
 * a way that maximizes the total amount of money you get. A piece of length i is worth pi
 * dollars.
*/

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class RodCutting {
public:
	int rod_cut_bf(vector<int>& p) {
		int ans = 0;
#if 0
		ans = helper_bf(p, p.size());
#else
		//vector<int> memo(p.size(), -1);
		//ans = helper_memo(p, p.size(), memo);
		ans = helper_tab(p);
#endif

		return ans;
	}

private:
	int helper_bf(vector<int>& p, int len) {
		if (len == 0) return 0;
		
		int ans = INT_MIN;
		for (int i = 0; i < len; i++) {
			int cur = p[i] + helper_bf(p, len - i - 1);
			ans = max(ans, cur);
		}
		
		return ans;
	}

	int helper_memo(vector<int>& p, int len, vector<int>& memo) {
		if (len == 0) return 0;
		if (memo[len-1] != -1) return memo[len];

		int ans = INT_MIN;
		for (int i = 0; i < len; i++) {
			int cur = p[i] + helper_bf(p, len - i - 1);
			ans = max(ans, cur);
		}

		memo[len-1] = ans;
		return memo[len-1];
	}

	int helper_tab(vector<int>& p) {
		vector<int> dp(p.size()+1);

		dp[0] = 0;
		for (int i = 1; i <= (int)p.size(); i++) {
			dp[i] = INT_MIN;
			for (int j = 1; j <= i; j++)
				dp[i] = max(dp[i], dp[i - j] + p[j-1]);
		}

		return dp[p.size()];
	}

};

#if 0
int main(void) {
	//vector<int> p = {1, 5, 8, 9, 10, 17, 17, 20};
	vector<int> p = { 3, 5, 8, 9, 10, 17, 17, 20 };

	RodCutting t;
	cout << t.rod_cut_bf(p);

	return 0;
}
#endif