
/*
 * Given an array of n positive integers.
 * Write a program to find the sum of maximum sum subsequence of the given array such that
 * the integers in the subsequence are sorted in increasing order.
 *
 * For example, if input is {1, 101, 2, 3, 100, 4, 5}, then output should be 106 (1 + 2 + 3 + 100),
 * if the input array is {3, 4, 5, 10}, then output should be 22 (3 + 4 + 5 + 10) and
 * if the input array is {10, 5, 4, 3}, then output should be 10
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class MaximumSumIncreasingSubsequence {
public:
	int MSIS(vector<int>& a) {
	
	int ans = 0;
#if 0
	ans = MSIS_bf(a, 0, INT_MIN);
#else
	#if 0
		// memoization
		vector<int> memo(a.size(), -1);
		ans = 0;
		for (int n = 0; n < (int)a.size(); n++)
			ans = max(ans, MSIS_memo(a, n, memo));
	#else
		// tabulation
		ans = MSIS_dp(a);
	#endif
#endif
	return ans;
	}

private:
	int MSIS_bf(vector<int>& a, int i, int prev) {
		if (i == (int)a.size()) return 0;

		int take = 0, notake;
		if (a[i] > prev) take = a[i] + MSIS_bf(a, i + 1, a[i]);
		notake = MSIS_bf(a, i + 1, prev);

		return max(take, notake);
	}

	// return the maximum sum of the increasing substring ending in a[i]
	int MSIS_memo(vector<int>& a, int i, vector<int>& memo) {
		if (i == 0) return 1;
		if (memo[i] != -1)
			return memo[i];

		int ans = a[i];
		for (int j = 0; j < i; j++)
			if (a[j] < a[i]) ans = max(ans, a[i] + MSIS_memo(a, j, memo));

		memo[i] = ans;
		return memo[i];
	}

	int MSIS_dp(vector<int>& a) {
		vector<int> dp(a.size());

		dp[0] = a[0];
		int ans = a[0];
		for (int i = 1; i < (int)a.size(); i++) {
			dp[i] = a[i];
			for (int j = 0; j < i; j++) {
				if (a[i] > a[j]) dp[i] = max(dp[i], dp[j] + a[i]);
			}
			ans = max(ans, dp[i]);
		}

		return ans;
	}
};

#if 0
int main(void) {

	vector<int> a = { 1, 101, 2, 3, 100, 4, 5 };

	MaximumSumIncreasingSubsequence t;
	cout << t.MSIS(a);

	return 0;
}
#endif