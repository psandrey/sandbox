
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

typedef long long ll;
class LexicographicPartition {
public:
	vector<int> generate (int n, vector<int>& Aprefix, int seed, int Arange){
		vector<int> a(n);
		for (int i = 0; i < n; i++)
			a[i] = Aprefix[i];

		ll state = seed;
		for (int i = (int)Aprefix.size(); i < n; i++) {
			state = (1103515245 * (ll)state + 12345);
			a[i] = state % (2LL * Arange + 1LL);
			a[i] = a[i] - Arange;
			state %= (1ll << 31);
		}

		return a;
	}

	vector <int> positiveSum(int n, vector<int>& Aprefix, int seed, int Arange) {
		vector <int> a = generate(n, Aprefix, seed, Arange);

		ll T = 0;
		for (int i = 0; i < n; i++) T += a[i];
		if (T <= 0) return { -1 };

		int len = 0;
		ll s = 0;
		vector<int> b;
		for (int i = 0; i < n; i++) {
			s += a[i];
			T -= a[i];
			len++;
			if (s > 0 && T > 0) {
				b.push_back(len);
				s = 0; len = 0;
			}
		}
		b.push_back(len);

		b.insert(b.begin(), (int)b.size());
		b.erase(b.begin() + min(200, (int)b.size()), b.end());
		return b;
	}
};

#if 0
int main(void) {
#if 1
	int n = 5;
	vector<int> Aprefix = { 0,1,0,1,2 };
	int seed = 42;
	int Arange = 47;
#else
	int n = 3;
	vector<int> Aprefix = { 3,-7,8 };
	int seed = 1;
	int Arange = 1;
#endif

	LexicographicPartition t;
	vector<int> ans = t.positiveSum(n, Aprefix, seed, Arange);
	for (int x : ans)
		cout << x << endl;

	return 0;
}
#endif