
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

class ATaleOfThreeCities {
public:
	double connect(vector<int>& ax, vector<int>& ay, vector<int>& bx, vector<int>& by, vector<int>& cx, vector<int>& cy) {
		int an = (int)ax.size(), bn = (int)bx.size(), cn = (int)cx.size();

		vector<double> d(3);

		bool f = false;
		for (int i = 0; i < an; i++) {
			for (int j = 0; j < bn; j++) {
				double x = sqrt(pow(ax[i] - bx[j], 2) + pow(ay[i] - by[j], 2));
				if (f == false) { d[0] = x; f = true; }
				else d[0] = min(d[0], x);
			}
		}

		f = false;
		for (int i = 0; i < an; i++) {
			for (int j = 0; j < cn; j++) {
				double x = sqrt(pow(ax[i] - cx[j], 2) + pow(ay[i] - cy[j], 2));
				if (f == false) { d[1] = x; f = true; }
				else d[1] = min(d[1], x);
			}
		}

		f = false;
		for (int i = 0; i < bn; i++) {
			for (int j = 0; j < cn; j++) {
				double x = sqrt(pow(bx[i] - cx[j], 2) + pow(by[i] - cy[j], 2));
				if (f == false) { d[2] = x; f = true; }
				else d[2] = min(d[2], x);
			}
		}

		sort(d.begin(), d.end());
		return (d[0] + d[1]);
	};
};

#if 0
int main(void) {
	vector<int> ax = { 0,0,0 };
	vector<int> ay = { 0, 1, 2 };
	vector<int> bx = { 2, 3 };
	vector<int> by = { 1, 1 };
	vector<int> cx = { 1, 5 };
	vector<int> cy = { 3, 28 };

	ATaleOfThreeCities t;
	cout << t.connect(ax, ay, bx, by, cx, cy) << endl;

	return 0;
}
#endif