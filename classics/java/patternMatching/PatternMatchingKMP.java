package patternMatching;

public class PatternMatchingKMP {

	private static int[] doLPT(String pattern) {
		// the table of the longest proper prefix-suffix for all substrings
		int[] lpt = new int[pattern.length()];
		
		int i = 0;	// end of longest proper prefix-suffix
		int j = 0;	// end of the current substring
		lpt[0] = 0;	// the substring formed by only one character has no proper prefix-suffix
		while (j < pattern.length() - 1) {
			
			// find the previous longest proper prefix-suffix candidate for extension
			while (i > 0 && pattern.charAt(j+1) != pattern.charAt(i))
				i = lpt[i-1];
	
			// can we extend this proper prefix-suffix ?
			if (pattern.charAt(j+1) == pattern.charAt(i))
				i++;

			// set the longest proper prefix-suffix for the pattern[0..j]
			lpt[++j] = i;
		}

		return lpt;
	}

	private static int doKMP(String pattern, String str, int start) {
		int[] lpt = doLPT(pattern);
		// printVector(lpt);

		int k = 0;
		for (int s = start; s < str.length(); s++) {
			while (k > 0 && str.charAt(s) != pattern.charAt(k))
				k = lpt[k-1];
			if (str.charAt(s) == pattern.charAt(k))
				k++;			
			if (k == pattern.length())
				return s - k + 1;
		}

		return -1;
	}

/*
	private static void printVector(int[] vector) {
		System.out.print("Vector:");
		for (int i = 0; i < vector.length; i++)
			System.out.print(vector[i] + ",");
		System.out.println();
	}
*/

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//String pattern = "ababx";
		//String t       = "abaababx";
		//String pattern = "abababca";
		//String t       = "bacbabababcaa";
		//String pattern = "aabaabb";
		//String pattern = "abacx#xcaba";

		String pattern = "aabaabb";
		String t       = "aabaaaabaabb";

		int k = PatternMatchingKMP.doKMP(pattern, t, 0);
		System.out.println("First found at:" + k);
	}
}
