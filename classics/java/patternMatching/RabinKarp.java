package patternMatching;

public class RabinKarp {
	
	private static long q = 1234499471;
	private static int  b = 256;

	private static long hash(String s, int m) {
		long h = 0;
		for (int i = 0; i < m; i++)
			h = (h*b + s.charAt(i)) % q;

		return h;
	}

	private static int rabinKarp(String s, String p) {
		if (s == null || p == null || p.length() == 0 || s.length() < p.length())
			return -1;
		int n = s.length();
		int m = p.length();

		long Bm = 1;
		for (int i = 1; i < m; i++)
			Bm = (Bm*b)%q; // b^m

		long prnH = hash(p, m);
		long h = hash(s, m);
		if (h == prnH)
			return 0;
		
		// Mind the following statement: (x mod q) = (((x mod q) mod q) ... mod q)
		for (int i = m; i < n; i++) {
			h = (h + q - Bm*s.charAt(i-m) % q) % q; // mind: q mod q = 0
			h = (h*b + s.charAt(i)) % q;

			if (prnH == h)
				return i-m+1;
		}
		
		return -1;
		
	}

	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//String p =         "ababx";
		//String s = "abtababyababxxxx";
		String p = "abababca";
		String s = "xyzabababca";

		System.out.println(p + " > " + s + " : " + rabinKarp(s, p));
	}
}
