package patternMatching;

import java.util.HashSet;

public class PatternMatchingBoyerMoore {
	// there are 26 characters in English language, however characters such as:, . ! ? can
	//  appear in searches, so... lets use an alphabet of all 256 ASCII characters
	private final int ALPHABET_LEN = 256;
	private int[][] badChTblV1; // the next occurrence of each character in the given pattern
	private int[]   badChTblV2; // the rightmost occurrence of each character in the given pattern
	private int[]   suffixTbl;  // the good suffix shift table
	private String pattern;

	private PatternMatchingBoyerMoore(String pattern) {
		this.pattern = pattern;

		badChTblV1 = preprocessBadCharTblV1(pattern);
		badChTblV2 = preprocessBadCharTblV2(pattern);
		suffixTbl = preprocessGoodSuffixTbl(pattern);
	}
	
	private int charIndexInAlphabet(char c) {
		return (int) c;
	}

	private int[][] preprocessBadCharTblV1(String pattern) {
		if (pattern == null || pattern.length() == 0)
			return null;

		int[][] tbl = new int[ALPHABET_LEN][pattern.length()];
		for (int i = 0; i < ALPHABET_LEN; i++)
			for (int j = 0; j < pattern.length(); j++)
				tbl[i][j] = -1;

		// compute for each letter of the pattern where is the next occurrence in p
		// e.g. pattern = abacb
		// tbl[a] =  0  0  2 2 2
		// tbl[b] =  0  1  1 1 4
		// tbl[c] = -1 -1 -1 3 3
		HashSet<Character> mp = new HashSet<Character>();
		for (int i = 0; i < pattern.length(); i++) {
			char c = pattern.charAt(i);
			if (!mp.contains(c)) {
				mp.add(c);
				int k = -1;
				for (int j = 0; j < pattern.length(); j++) {
					if (pattern.charAt(j) == c)
						k = j;
					tbl[charIndexInAlphabet(c)][j] = k;
				}
			}
		}

		return tbl;
	}
	
	private int badCharacterRuleShiftV1(char c, int pos) {
		// return the next occurrence of character c from [0..pos),
		//  otherwise shift the entire pattern
		//
		// Note: if tbl=-1, then pos + (-1)*(-1)=pos + 1
		//	   otherwise: shift = pos - tbl
		return pos + (-1)*badChTblV1[charIndexInAlphabet(c)][pos];
	}

	private int[] preprocessBadCharTblV2(String pattern) {
		if (pattern == null || pattern.length() == 0)
			return null;

		int[] tbl = new int[ALPHABET_LEN];
		for (int i = 0; i < ALPHABET_LEN; i++)
			tbl[i] = -1;

		// compute for each letter of the pattern where is the next occurrence in p
		// e.g. pattern = abacb
		// tbl[a] =  0  0  2 2 2 |=> 2
		// tbl[b] =  0  1  1 1 4 |=> 4
		// tbl[c] = -1 -1 -1 3 3 |=> 3
		for (int i = 0; i < pattern.length(); i++)
			tbl[charIndexInAlphabet(pattern.charAt(i))] = i;

		return tbl;
	}

	private int badCharacterRuleShiftV2(char c, int pos) {
		// return the rightmost occurrence of character c from [0..pos),
		//  otherwise shift the entire pattern
		//
		// Note: if tbl=-1, then pos + (-1)*(-1)=pos + 1
		//	   otherwise: shift = pos - tbl
		return pos + (-1)*badChTblV2[charIndexInAlphabet(c)];
	}

	private int[] preprocessGoodSuffixTbl(String pattern) {
		if (pattern == null || pattern.length() == 0)
			return null;

		int[] suffixTbl = new int[pattern.length() + 1];
		for(int i = 0; i < pattern.length() ; i++)
			suffixTbl[i] = 0;

		int[] bpos = preprocessStrongSuffix(pattern, suffixTbl);
		preprocessSuffixPrefix(pattern, suffixTbl, bpos);		
		return suffixTbl;
	}

	private int[] preprocessStrongSuffix(String pattern, int[] suffixTbl) {
		int m = pattern.length();
		int[] bpos = new int[m + 1];
		
		int i = m;
		int j = m + 1;
		bpos[m] = m + 1;

		while (i>0) {
			/* if character at position i-1 != from character at j-1,
			 *  then continue searching to right of the pattern for border */
			while(j <= m && pattern.charAt(i-1) != pattern.charAt(j-1)) {
				
				/* shift to the first occurrence of the suffix
				 * that is not preceded by char at j-1 */
				if (suffixTbl[j] == 0)
					suffixTbl[j] = j - i;

				j = bpos[j];
			}

			//i--;
			//j--;
			bpos[--i] = --j;
		}
		
		return bpos;
	}

	private void preprocessSuffixPrefix(String pattern, int[] suffixTbl, int[] bpos) {
		int m = pattern.length();
		int i, j;
		
		j = bpos[0]; 
		for (i = 0; i <= m; i++) {
			/* set the border position of first character of pattern
			   to all indices in array shift having shift[i] = 0 */
			if(suffixTbl[i] == 0)
				suffixTbl[i] = j;

			/* suffix become shorter than bpos[0], use the position of
			   next widest border as value of j */
			if (i == j)
				j = bpos[j];
		} 
	}

	private int goodSufixRuleShift(int pos) {
		return suffixTbl[pos+1];
	}

	private int doBM(String str, int start, int version) {
		int m = pattern.length();
		int n = str.length();
		int s = start;

		while (s <= n - m) {
			int j = m - 1;
			
			// Keep reducing j of the pattern while matching the string at position j
			while (j >=0 && pattern.charAt(j) == str.charAt(s+j))
				j--;
			
			// we found an occurrence at position s
			if ( j < 0)
				return s;
			// we didn't find an occurrence at position s, so lets shift
			else {
				int shift_bc = 0;
				if (version == 1)
					shift_bc = badCharacterRuleShiftV1(str.charAt(s+j), j);
				else
					shift_bc = badCharacterRuleShiftV2(str.charAt(s+j), j);
				
				int shift_gs = goodSufixRuleShift(j);
				s += Math.max(shift_bc, shift_gs);
			}
		}
		
		return -1;
	}
	
	/* The MAIN ---------------------------------------------------------------------------------*/
	public static void main (String args[]) {
		//String pattern =	"abacb";
		//String t	   = "abbabacbb";
		//String pattern =	 "abababca";
		//String t	   = "bacbabababcaa";
		String t       = "aabababbaba";
		String pattern =      "abbab";
		
		PatternMatchingBoyerMoore bm = new PatternMatchingBoyerMoore(pattern);
		int occurence = bm.doBM(t, 0, 1);
		System.out.println("The pattern(" + pattern + ") first occurred in (" + t +") at:" + occurence);
	}
}
