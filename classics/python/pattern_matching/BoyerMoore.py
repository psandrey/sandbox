"""
------------------------------------------------------------------------------- 
Boyer-Moore string-search algorithm

Time complexity: O(nm)
===============================================================================

Implementation:
1. Bad character rule: DONE
2. Good suffix rule: TODO
3. Galil rule: N.A.
"""

class BM:
	# there are 26 characters in English language, however characters such as:, . ! ? can
	#  appear in searches, so... lets use an alphabet of all 256 ASCII characters
	ALPHABET_LEN = 256

	@staticmethod
	def charIndexInAlphabet(c):
		return ord(c.lower())

	@staticmethod
	def preprocessBadChTable(p:str):
		if len(p) == 0:
			return [[] for _ in range(BM.ALPHABET_LEN)]

		# compute for each letter of the pattern where is the last occurrence in p
		tbl = [[-1] * len(p) for _ in range(BM.ALPHABET_LEN)]
		for i in range(len(tbl)):
			k = -1	
			for j in range(len(p)):
				if BM.charIndexInAlphabet(p[j]) == i:
					k = j
				tbl[i][j] = k
		return tbl	

	@staticmethod
	def badCharacterRuleShift(badChTbl:list, ch, pos):
		chIdx = BM.charIndexInAlphabet(ch)
		patternSize = len(badChTbl[chIdx]) # each line size is equal with pattern size
		return patternSize - min(pos, 1 + badChTbl[chIdx][pos])

	@staticmethod
	def goodSufixRuleShift(j: int):
		# TODO
		return -1

	@staticmethod
	def doBM(t:str, p:str, startIdx:int):
		badChTbl = BM.preprocessBadChTable(p)
		
		i = startIdx + len(p) - 1
		j = len(p) - 1
		while i < len(t):
			if p[j] == t[i]:
				if j == 0:
					return i
				else:
					i -= 1
					j -= 1
			else:
				# bad-chr-rule: advance the pattern in the direction toward 0 such that the current char
				# match with the first appearance of it in the pattern
				# e.g. t=abbcba...  abbcba...
				#      p=abacb       abacb
				#          x          v
				shift_bc = BM.badCharacterRuleShift(badChTbl, t[i], j)
				shift_gs = BM.goodSufixRuleShift(j)
				shift = max(1, shift_bc, shift_gs)

				# advance in text by shift and reset pattern
				i += shift
				j = len(p) - 1
		return -1

class testBM:
	def doUnitTestBM(self):
		p = "abacb"
		tbl = BM.preprocessBadChTable(p)
		for a in tbl:
			print(a)

	def doTestBM_0(self):
		p = "abacb"
		t = "abbcbabab"
		pos = BM.doBM(t, p, 0)
		print("p (", p, ") in (", t, ")")
		print(pos,":", t[pos:len(t)])

	def doTestBM_1(self):
		p = "abaa"
		t = "aabaac"
		pos = BM.doBM(t, p, 0)
		print("p (", p, ") in (", t, ")")
		print(pos,":", t[pos:len(t)])

	def doTestBM_2(self):
		t = "abtababyababx"
		p = "ababx"
		pos = BM.doBM(t, p, 0)
		print("p (", p, ") in (", t, ")")
		print(pos,":", t[pos:len(t)])

	def doTestBM_3(self):
		t = "ala bala portocala"
		p = "ala"
		print("Find all occurrence of p (", p, ") in (", t, ")")
		
		pos = 0
		while pos != -1:
			pos = BM.doBM(t, p, pos)
			if pos != -1:
				print("Found@", pos)
				pos += len(p)

	def doTestBM_4(self):
		t = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."\
		"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."\
		"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."\
		"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."\
		"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."\
		"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."\
		"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."\
		"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."\
		"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."\
		"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."\
		"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."\
		"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."\
		"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."\
		"Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."\
		"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."\
		"Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
		#p = "Lorem"
		p = "reprehenderit"
		print("Find all occurrence of p (", p, ") in (", t, ")")
		
		pos = 0
		while pos != -1:
			pos = BM.doBM(t, p, pos)
			if pos != -1:
				print("Found@", pos)
				pos += len(p)
#t = testBM()
#t.doUnitTestBM()
#t.doTestBM_0()
#t.doTestBM_1()
#t.doTestBM_2()
#t.doTestBM_3()
#t.doTestBM_4()