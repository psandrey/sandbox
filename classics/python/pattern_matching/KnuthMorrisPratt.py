"""
-------------------------------------------------------------------------------
Knuth-Morris-Pratt string search algorithm

Time complexity: O(n)
===============================================================================
"""

class KMP:
	@staticmethod
	def prefixFailureFunction(p:str):
		prfxTbl = [0] * len(p)
		
		k = -1
		prfxTbl[0] = -1
		for i in range(1, len(p)):
			while (k >= 0 and p[k+1] != p[i]):
				k = prfxTbl[k]
			if p[k+1] == p[i]:
				k += 1
			
			# longest prefix for p[i] within p[0:i]
			prfxTbl[i] = k;
		return prfxTbl

	@staticmethod
	def doKMP(t: str, p: str):
		print("search (", p, ":", len(p) ,") in (",t , ":", len(t) ,")")
		
		prfxTbl = KMP.prefixFailureFunction(p)
		k = 0
		for i in range(0, len(t)):
			while(k >= 0 and p[k+1] != t[i]):
				k = prfxTbl[k]
			if p[k+1] == t[i]:
				k += 1
			if k == len(p) - 1:
				return i - len(p) + 1
		return -1

class testPM:
	def doUnitTestKMP(self):
		#p = "AAACAAAA"
		#p = "ababaca"
		p = "ababaca"
		lsp = KMP.prefixFailureFunction(p)
		print(lsp)

	def doUnitTestKMP2(self):
		#p = "AAACAAAA"
		#p = "ababaca"
		p = "aaax"
		lsp = KMP.prefixFailureFunction(p)
		print(lsp)

	def doTestKMP(self):
		t = "abtababyababx"
		p = "ababx"
		pos = KMP.doKMP(t, p)
		print(pos,":", t[pos:len(t)])

#t = testPM()
#t.doUnitTestKMP2()
#t.doUnitTestKMP()
#t.doTestKMP()
